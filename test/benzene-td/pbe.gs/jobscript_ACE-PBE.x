#!/bin/bash

#PBS -N ACE-PBE
#PBS -l nodes=r1i0n7:ppn=24
#PBS -l walltime=500:00:00

date

export INPUT=ACE-PBE.inp
export OUTPUT=ACE-PBE.log
export EXEC=./ace

module purge
module load intel
module load intel_mkl
module load openmpi-1.8.3-intel
module load trilinos-12.8.1_openmp_release_intel_openmpi-1.8.3
module load gsl-1.16_intel_openmpi-1.8.3
module load libxc-trunk-12537

cd $PBS_O_WORKDIR

echo `cat $PBS_NODEFILE`
cat $PBS_NODEFILE
NPROCS=`wc -l < $PBS_NODEFILE`
mpirun -machinefile $PBS_NODEFILE -np $NPROCS $EXEC $PBS_O_WORKDIR/$INPUT > $PBS_O_WORKDIR/$OUTPUT

date