#! /bin/bash
for file in *.log; do
    echo ;
    echo -e '\033[1;37m';
    echo "${file/\.log\///}";
    echo -e '\033[0;0m';
    diff "$file" "log.ref/${file/log/ref}";
done
