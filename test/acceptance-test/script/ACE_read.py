import numpy as np
import sys
import os
import ase.units
from ase.atoms import Atoms
from ase.atom import Atom
from ase.calculators.calculator import FileIOCalculator
from ase.optimize import QuasiNewton

class ACE_reader(FileIOCalculator):
    """
    ACE-Molecule logfile reader
    """
    name = 'ACE'
    implemented_properties = ['energy', 'forces','geometry']
    
    command = 'mpirun -np 20 /home/khs/hs_file/programs/ACE-Molecule/ace PREFIX.inp > PREFIX.log'
    
    def __init__(self, restart=None, ignore_bad_restart_file=False,
                 label='g09', atoms=None, scratch=None, ioplist=list(),
                 basisfile=None, **kwargs):
    
        FileIOCalculator.__init__(self, restart, ignore_bad_restart_file,
                                  label, atoms, **kwargs)   
         
    def set(self, **kwargs):
        changed_parameters = FileIOCalculator.set(self, **kwargs)
        if changed_parameters:
            self.reset()
        return changed_parameters
    
    
    def read_ACE_out(self,filename, quantity= 'energy'):                
        f=open(filename, 'r')                                                               
        lines = f.read()
        '''read energy '''
        energy_list = lines.split("Total energy ")
        energy= float( energy_list[len(energy_list)-1].split('\n')[0].split('=')[1] )
        '''read forces '''
        
        if(quantity == 'energy'):
            f.close()
            #energy *= ase.units.Hartree
            return energy
        if(quantity == 'forces'):
            forces_lines=lines.split("Force:: List of total force in atomic unit.")[1].split("======")[0]
            forces_line = forces_lines.split('\n')
            forces = list()
            for i in range(2,len(forces_line)-1):
                forces += [forces_line[i].split()[-3:]]
            convert = ase.units.Hartree / ase.units.Bohr
            convert = 1
            '''force units change bohr to hartree'''
            forces = np.array(forces, dtype=float) *convert                                                                               
            f.close()
            return forces
    def read(self, label):
        FileIOCalculator.read(self, label)
    def e_to_value(self, value):
        state=str(value).split("e-")
        integer=list()
        check =str(value).split("e+")
        if (len(check)==2):
            return check[0]
        if (len(state[0].split('-'))==2):
            if(len(state)==2):
                if(int(state[1])>7):
                    return '0.0'
                return_value='-0.'
                state[0]=state[0].split('-')[1]
                for i in range(1,int(state[1])):
                    return_value +='0'
                integer = state[0].split('.')
                for i in range (len(integer)):
                    return_value+=integer[i]
                return return_value
            else:
                return value
        else:
            if(len(state)==2):
                if(int(state[1])>7):
                    return '0.0'
                return_value='0.'
                for i in range(1,int(state[1])):
                    return_value +='0'
                integer = state[0].split('.')
                for i in range( len(integer)):
                    return_value+=integer[i]
                return return_value
            else:
                return value


    def make_xyz_file(self, Atoms):
        xyz_file= open(self.label+'_opt.xyz','w')
        xyz_file.write(str(len(Atoms.get_chemical_symbols()))+'\n\n')
        symbol_list = Atoms.get_chemical_symbols()
        position_list = Atoms.get_positions()
        for symbol, position in zip(symbol_list,position_list):
            position =str(position).split('[')[1].split(']')[0].split()
            xyz_file.write(symbol+' '+self.e_to_value(position[0])+' '+self.e_to_value(position[1])+' '+self.e_to_value(position[2])+'\n')
        xyz_file.close()
           
    def write_input(self, atoms, properties=None, system_change=None):
        """Writes the input file"""
        FileIOCalculator.write_input(self, atoms,properties, system_changes)
#        self.initalize(atoms)
        filename = self.label+'.inp'
        inputfile = open(filename, 'w')
        inputtemplate = open('ACEinput.template','r')
        inputtemplate_lines = inputtemplate.read().split('\n\n')
        inputfile.write('## Ignore lines containing "## "\n%% Basic_Information\n')
        inputfile.write('\tLabel\t'+self.label+'\n')
        variable_array = ['Type','Scaling','Basis', 'Grid', 'Radius']
        for x in variable_array:
            inputfile.write('\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[0])
        inputfile.write('\n')
        x='SpinMultiplicity'
        inputfile.write('\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[1])
        inputfile.write('\n')
        x='Polarize'
        inputfile.write('\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[2])
        inputfile.write('\n')
        variable_array = ['Pseudopotential','Format', 'PSFilePath', 'PSFileSuffix']
        for x in variable_array:
            inputfile.write('\t\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[3])
        inputfile.write('\n')
        self.make_xyz_file(atoms)
        x='Geometry_Filename'
        inputfile.write('\t%s\t%s\n' %(x,self.label+'_opt.xyz'))
        inputfile.write(inputtemplate_lines[4])
        inputfile.write('\n')
        x='NumElectrons'
        inputfile.write('\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[5])
        inputfile.write('\n')
        variable_array = ['InitialGuess','InitialFilePath', 'InitialFileSuffix']
        for x in variable_array:
            inputfile.write('\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[6])
        inputfile.write('\n')
        variable_array = ['IterateMaxCycle','ConvergenceType','ConvergenceTolerance']
        for x in variable_array:
            inputfile.write('\t%s\t%s\n' %(x,self.e_to_value(self.parameters[x])))
        inputfile.write(inputtemplate_lines[7])
        inputfile.write('\n')
        variable_array = ['DiagonalizeMaxIter','Tolerance']
        for x in variable_array:
            inputfile.write('\t\t%s\t%s\n' %(x,self.e_to_value(self.parameters[x])))
        inputfile.write(inputtemplate_lines[8])
        inputfile.write('\n')
        # now only pularymixing input availiable
        variable_array = ['MixingMethod','MixingType', 'MixingParameter','PulayMixingParameter']
        for x in variable_array:
            inputfile.write('\t\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[9])
        inputfile.write('\n')
        variable_array = ['Solvent','Area', 'SolverType']
        for x in variable_array:
            inputfile.write('\t\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[10])
        inputfile.write('\n')
        x='NumberOfEigenvalues'
        inputfile.write('\t%s\t%s\n' %(x,self.parameters[x]))
        inputfile.write(inputtemplate_lines[11])
        inputfile.write('\n')
        inputfile.close()

#        input_line = str()
#        for key, val in self.parameters.items():
#            input_line +=("%s   %s\n" %(key,val))

#        inputfile.write(inputtemplet.read())
        
    def read_results(self):
        """Reads the output file using GaussianReader"""
        filename = self.label + '.log'
        self.results['energy'] = self.read_ACE_out(filename, quantity='energy')
        self.results['forces'] = self.read_ACE_out(filename, quantity='forces')
        self.results['geometry'] = self.read_ACE_out(filename, quantity='geometry')

###### This is code test part ####
def make_atom_form_symbol(label):
    f=open(label+'.xyz','r')
    lines = f.read().split("\n")
    symbol_list=str()
    position_list=[]

    for i in range(2,len(lines)):
        if( len(lines[i].split())>3 ):
            symbol_list += lines[i].split()[0]
            x= lines[i].split()[1]
            y= lines[i].split()[2]
            z= lines[i].split()[3]
            position_list.append( (x,y,z) )
    return symbol_list
def make_atom_form_position(label):
    f=open(label+'.xyz','r')
    lines = f.read().split("\n")
    symbol_list=str()
    position_list=[]

    for i in range(2,len(lines)):
        if( len(lines[i].split())>3 ):
            symbol_list += lines[i].split()[0]
            x= lines[i].split()[1]
            y= lines[i].split()[2]
            z= lines[i].split()[3]
            position_list.append( (x,y,z) )
    return position_list

if __name__ == "__main__":
    ##### xyz read and make Atoms above ###
    Label = str(sys.argv[1].split('.xyz')[0])
    symbol_list = make_atom_form_symbol(Label)
    position_list = make_atom_form_position(Label)
    #H2 = Atoms('H2',[(0.0, 0.0, -0.3968829366615649),(0.0, 0.0, 0.3968829366615649)])
    H2 = Atoms(symbol_list,position_list)
    ace = ACE_reader(label=Label,Type='Scaling',Scaling=0.2,Basis='Sinc',Grid='Atoms',
     Radius=2.0,SpinMultiplicity=1.0, Polarize=0, Pseudopotential=1, Format='upf',
     PSFilePath='/home/khs/DATA/UPF',PSFileSuffix='.pbe-theos.UPF',
     NumElectrons=2,InitialGuess=1,InitialFilePath='/home/khs/DATA/UPF',InitialFileSuffix='.pbe-theos.UPF',
     IterateMaxCycle=150,ConvergenceType='Energy',ConvergenceTolerance=0.00001,DiagonalizeMaxIter=10,
     Tolerance= 0.000001, MixingMethod=1, MixingType= 'Density', MixingParameter=0.5, PulayMixingParameter=0.1,
     Solvent= 'water', Area=1.0, SolverType='None', NumberOfEigenvalues=9)
    system_changes=None


    H2.set_calculator(ace)
    e= H2.get_forces()
    g_opt = QuasiNewton(H2)
    g_opt.run(fmax=0.05)


    #print '================================'
    #print e
    #print H2.get_positions()
    #print '================================'
    #ace.read_results()

