#! /bin/env python
# -*- coding:utf-8 -*-

__version__ = "0.1.160112"

class Molecule:
    def __init__(self, label, geometry, elec, eigen, pptyp, ppfiles, spin = 1, exchange = 101, correlation = 130, guessfiles = [], is_dg = False, **kwargs):
        self.label = label
        self.geometry = []
        for pos in geometry:
            self.geometry.append((pos[0], pos[1:]))

        self.electrons = float(elec)
        self.eigenvalues = eigen
        self.pptyp = pptyp
        self.ppfiles = ppfiles
        self.spin = float(spin)
        self.guessfiles = ppfiles
        if len(guessfiles) >= ppfiles:
            self.guessfiles = guessfiles
        self.X = exchange
        self.C = correlation
        self.is_dg = is_dg
        self.kwargs = kwargs

    def write_xyz(self, filename):
        xyz = open(filename, 'w')
        xyz.write(str(len(self.geometry))+"\n")
        xyz.write("KSW testset for ACE-Molecule v."+__version__)
        for pos in self.geometry:
            xyz.write("\n")
            xyz.write("\t".join([pos[0], str(pos[1][0]), str(pos[1][1]), str(pos[1][2])]))
        xyz.close()

    def write_input(self, filename, geomfile):
        input = open(filename, 'w')

        input.write('%% Basic_Information\n')
        input.write('Mode                Sp\n')
        input.write('Label               test\n')
        input.write('Type                Scaling\n')
        input.write('Scaling             0.5\n')
        input.write('Cell                12.0\n')
        input.write('Basis               Sinc\n')
        input.write('Grid                Sphere\n')
        input.write('Geometry_Filename   '+geomfile+'\n')
        input.write('Geometry_Format     xyz\n')
        input.write('NumElectrons        '+str(self.electrons)+'\n')
        input.write('SpinMultiplicity    '+str(self.spin)+'\n')
        if( self.spin == 1 ):
            input.write('Polarize            0\n')
        else:
            input.write('Polarize            1\n')

        input.write('\t%% Pseudopotential\n')
        if self.pptyp == "UPF":
            input.write('\tPseudopotential          1\n')
            input.write('\tFormat                   upf\n')
            for ppfile in self.ppfiles:
                input.write('\tPSFilenames               '+ppfile+'\n')
            if self.is_dg:
                input.write('\tUsingDoubleGrid      Yes\n')
        elif self.pptyp == "HGH":
            input.write('\tPseudopotential          1\n')
            input.write('\tFormat                   Willand\n')
            for ppfile in self.ppfiles:
                input.write('\tPSFilenames               '+ppfile+'\n')
            if self.is_dg:
                input.write('\tUsingDoubleGrid      Yes\n')
        elif self.pptyp == "PAW":
            input.write('\tPseudopotential          3\n')
            input.write('\tFormat                   xml\n')
            for ppfile in self.ppfiles:
                input.write('\tPSFilenames               '+ppfile+'\n')
            if self.is_dg:
                input.write('\tFineProj                 2\n')
                input.write('\tRmax                     1.5\n')
        input.write('\t%% End\n')
        input.write('%% End\n')

        input.write('%% Guess\n')
        if self.pptyp == "UPF":
            input.write('InitialGuess            1\n')
            for ppfile in self.guessfiles:
                input.write('\tInitialFilenames               '+ppfile+'\n')
        elif self.pptyp == "HGH":
            input.write('InitialGuess            1\n')
            for ppfile in self.guessfiles:
                input.write('\tInitialFilenames               '+ppfile+'\n')
        elif self.pptyp == "PAW":
            input.write('InitialGuess            4\n')
            for ppfile in self.guessfiles:
                input.write('\tInitialFilenames               '+ppfile+'\n')
        input.write('%% End\n')
        
        input.write('%% Scf\n')
        input.write('IterateMaxCycle         275\n')
        input.write('ConvergenceType         Energy\n')
        input.write('ConvergenceTolerance    0.000001\n')
        input.write('EnergyDecomposition     2\n')
        input.write('NumberOfEigenvalues     '+str(self.eigenvalues)+'\n')

        input.write('\t%% Exchange_Correlation\n')
        input.write('\tXFunctional         '+str(self.X)+'\n')
        input.write('\tCFunctional         '+str(self.C)+'\n')
        input.write('\t%% End\n')
        input.write('\n')
        input.write('\t%% Diagonalize\n')
        input.write('\tSolver              Pure\n')
        input.write('\tMaxIter             25\n')
        input.write('\tTolerance           0.000000001\n')
        input.write('\tEigenSolver         LOBPCG\n')
        input.write('\t%% End\n')
        input.write('\n')
        input.write('\t%% Mixing\n')
        if "MixingType" in self.kwargs:
            input.write('\tMixingType              '+self.kwargs["MixingType"]+'\n')
        else:
            input.write('\tMixingType              Density\n')
        input.write('\tPulayMixingParameter    0.15\n')
        input.write('\tMixingParameter         0.85\n')
        input.write('\tMixingMethod            1\n')
        input.write('\t%% End\n')

        input.write('%% End\n')
        input.write('\n')
        input.close()

molecules = {
    "Ni":{
        "PAW":Molecule("Ni", [["Ni", 0, 0, 0]], 10, 6, "PAW", ["DATA/Ni.10.PBE"], spin = 3, **{"MixingType":"PAW_Density"}),
        "UPF":Molecule("Ni", [["Ni", 0, 0, 0]], 10, 6, "UPF", ["DATA/Ni.pbe-n-nc.UPF"], spin = 3),
    },
    "Fe":{
        "PAW":Molecule("Fe", [["Fe", 0, 0, 0]], 8, 6, "PAW", ["DATA/Fe.PBE"], spin = 5, **{"MixingType":"PAW_Density"}),
        "UPF":Molecule("Fe", [["Fe", 0, 0, 0]], 8, 6, "UPF", ["DATA/Fe.pbe-n-nc.UPF"], spin = 5),
        #"HGH":Molecule("Fe", [["Fe", 0, 0, 0]], 16, 10, "HGH", ["/home/ksw/KDFT/Data/HGH/Fe-q16.pbe.abinit.HGH"], spin = 5, guessfiles = ["/home/ksw/KDFT/Data/PSEUDOPOTENTIALS_NC/Fe.pbe-n-nc.UPF"]),
    },
    "C":{
        "HGH":Molecule("C", [["C", 0, 0, 0]], 4, 3, "HGH", ["DATA/C.nlcc.hgh"], spin = 3, guessfiles = ["DATA/C.pbe-n-nc.UPF"]),
    },
    "Al":{
        "HGH":Molecule("Al", [["Al", 0, 0, 0]], 3, 2, "HGH", ["DATA/Al.nlcc.hgh"], spin = 2, guessfiles = ["DATA/Al.pbe-n-nc.UPF"]),
    },
    "Si":{
        "HGH":Molecule("Si", [["Si", 0, 0, 0]], 4, 3, "HGH", ["DATA/Si.nlcc.hgh"], spin = 3, guessfiles = ["DATA/Si.pbe-n-nc.UPF"]),
    },
}
