# How to use
1. Add ACE_reader.py to $ASE_PKG_ROOT/io and ACE_calc.py to $ASE_PKG_ROOT/calculator
   Usually $ASE_PKG_ROOT is $PYTHONPATH/site-packages/ase
2. Import and use with
    ```
    from ase.calculators.ACE_cal import ACE
    atom = ase.io.read(...)
    atom.calc = ACE(command = "mpirun -np 20 ./ace PREFIX_opt.inp > PREFIX_opt.log", ACEtemplate = "template", BasicInformation = {"ADDITIONAL": "INPUTS"}, Scf = {"SCF": "INPUTS"})
    ```
