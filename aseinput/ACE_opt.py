from __future__ import print_function
import numpy as np
import sys
import os
from ase.atoms import Atoms
from ase.io import read as ACE_read
from ase.calculators.calculator import FileIOCalculator
from ase.calculators.ACE_cal import ACE
from ase.optimize import QuasiNewton
from ase.optimize import BFGS


##### xyz read and make Atoms above ###
Label = str(sys.argv[1].split('.inp')[0])
system_changes=None
ace = ACE(label=Label,command = 'mpirun -np 8 /home/khs/hs_file/programs/ACE-Molecule/ace PREFIX_opt.inp > PREFIX_opt.log' ,system_changes=None)

filename=Label+'.inp'
ace.set(ACEtemplate = filename)
#print 'befor io'
print ( filename )
mol= ACE_read(filename,format='Acemolecule-input')
ace.write_input(mol)
    #print type(b)
print ( mol.get_positions() )
mol.set_calculator(ace)
print   ( mol.get_forces() )
#g_opt = QuasiNewton(mol)
g_opt = BFGS(mol)
g_opt.run(fmax=0.05)

def e_to_value(value):
    state=str(value).split("e-")
    integer=list()
    check =str(value).split("e+")
    if (len(check)==2):
        return check[0]
    if (len(state[0].split('-'))==2):
        if(len(state)==2):
            if(int(state[1])>7):
                return '0.0'
            return_value='-0.'
            state[0]=state[0].split('-')[1]
            for i in range(1,int(state[1])):
                return_value +='0'
            integer = state[0].split('.')
            for i in range (len(integer)):
                return_value+=integer[i]
            return return_value
        else:
            return value
    else:
        if(len(state)==2):
            if(int(state[1])>7):
                return '0.0'
            return_value='0.'
            for i in range(1,int(state[1])):
                return_value +='0'
            integer = state[0].split('.')
            for i in range( len(integer)):
                return_value+=integer[i]
            return return_value
        else:
            return value

print ('================================')
    #print e
position_list= mol.get_positions()
distance=[]
for position in position_list:
    position =str(position).split('[')[1].split(']')[0].split()
    distance.append(float(e_to_value(position[0])))
    distance.append(float(e_to_value(position[1])))
    distance.append(float(e_to_value(position[2])))
    print ( position[0]+' '+position[1]+' '+position[2] )
value = (distance[0]-distance[3])**2+(distance[1]-distance[4])**2+(distance[2]-distance[5])**2
value2= value**0.5
print ( value2 )
print ('================================')

