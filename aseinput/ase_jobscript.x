#!/bin/bash

#PBS -N H2test
#PBS -l nodes=intel3:ppn=16
#PBS -l walltime=500:00:00

date

export INPUT=~/work/asetest/H2test
export OUTPUT=result.log

module purge
module load anaconda2
module load intel
module load intel_mkl
module load openmpi-1.8.3-intel
module load trilinos-12.8.1_openmp_release_intel_openmpi-1.8.3
module load gsl-1.16_intel_openmpi-1.8.3

cd $PBS_O_WORKDIR

echo `cat $PBS_NODEFILE`
cat $PBS_NODEFILE
NPROCS=`wc -l < $PBS_NODEFILE`
python ACE_opt.py $INPUT $NPROCS  > $OUTPUT

date
