from __future__ import print_function
import numpy as np
import sys
import ase.units
from ase.atoms import Atoms
from ase.data import chemical_symbols


class Acemoleculereader:
    """
    ACE-Molecule logfile reader
    """
    def auto_type(self, data):
        """ tries to determine type"""
        try:
            return float(data)
        except ValueError:
            pass

        try:
            ds = data.split(",")
            array = []

            for d in ds:
                array.append(float(d))

            return array
        except ValueError:
            pass

        return data

    def __init__(self, filename):
        filename = check_filename(filename)
        self.parse(filename)

    def parse(self, filename):
        f = open(filename, 'r')
        lines = f.read()
        geo_lines = lines.split("Warning:: Grid_Atoms ignores input Points and CellDimension")[1].split("Atoms")[1].split("=====================================")[0].split('\n')
        self.data = []
        atoms = []
        positions = []
        new_dict = {}
        self.data.append(new_dict)
        for i in range(1, len(geo_lines) - 1):
            atoms += geo_lines[i].split()[0]
            x = geo_lines[i].split()[1]
            y = geo_lines[i].split()[2]
            z = geo_lines[i].split()[3]
            positions.append((x, y, z))
            new_dict["Atomic_numbers"] = atoms
            new_dict["Positions"] = positions


def check_filename(filename):
    check = str(filename).split("'")
    if(len(check) > 2):
        filename = check[1]
    return str(filename)

    
def read_Acemolecule_out(filename, quantity='atoms'):
    filename = check_filename(filename)
    f = open(filename, 'r')
    lines = f.read()
    '''read energy '''
    energy_list = lines.split("Total energy ")
    energy = float(energy_list[len(energy_list) - 1].split('\n')[0].split('=')[1])
    '''read geometry '''
    geo_lines = lines.split("====================  Atoms  =====================")[-1].split("=================================")[0].split('\n')
    atoms = []
    positions = []
    new_dict = {}
    atom_symbol = str()
    for i in range(1, len(geo_lines) - 1):
        atoms += geo_lines[i].split()[0]
        x = float(geo_lines[i].split()[1]) / 1.8897259886
        y = float(geo_lines[i].split()[2]) / 1.8897259886
        z = float(geo_lines[i].split()[3]) / 1.8897259886
        positions.append((x, y, z))
        new_dict["Atomic_numbers"] = atoms
        new_dict["Positions"] = positions
    for number in new_dict["Atomic_numbers"]:
        atom_symbol += str(chemical_symbols[int(number)])
    positions = new_dict["Positions"]
    atoms = Atoms(atom_symbol, positions=positions)

    geometry = zip(atoms, positions)
        
    if(quantity == 'energy'):
        f.close()
        energy *= ase.units.Hartree
        # energy must be modified, hartree to eV
        return energy
    if(quantity == 'forces'):
        try:
            forces_lines = lines.split("Force:: List of total force in atomic unit.")[1].split("======")[0]
            forces_line = forces_lines.split('\n')
            forces = list()
            for i in range(2, len(forces_line) - 1):
                forces += [[float(forces_line[i].split()[3]),
                            float(forces_line[i].split()[4]),
                            float(forces_line[i].split()[5])]]
            convert = ase.units.Hartree / ase.units.Bohr
            '''force units change bohr to hartree'''
            forces = np.array(forces) * convert
        except:
            forces = None
        f.close()
        return forces
    if(quantity == 'geometry'):
        return geometry
    if(quantity == 'atoms'):
        return atoms


def read_Acemolecule_input(Label):
    filename = check_filename(Label)
    inputtemplate = open(filename, 'r')
    geometryfile = inputtemplate.read().split('GeometryFilename')[1].split('\n')[0].split()[0]
    xyzfile = open(geometryfile, 'r')
    atom_num = int(xyzfile.readline())
    atom_info = xyzfile.read().split('\n')[1:]
    atom_symbols = str()
    positions = []
    for i in range(atom_num):
        atom_symbols += atom_info[i].split()[0]
        x = atom_info[i].split()[1]
        y = atom_info[i].split()[2]
        z = atom_info[i].split()[3]
        positions.append((x, y, z))
    atoms = Atoms(atom_symbols, positions=positions)
    inputtemplate.close()
    xyzfile.close()
    return atoms

if __name__ == "__main__":
    from ase.io import read as ACE_read
    Label = str(sys.argv[1].split('.inp')[0])
    system_changes = None
    a = ACE_read(Label + '.inp', format='ACE-input')
    print(a.get_positions())
    
    filename = Label + '.log'
