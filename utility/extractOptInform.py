import os
import sys

input_filename = sys.argv[1]

f = open(input_filename+'.CARD','w')

### extract energy
os.system("grep \'Final Total Energy\' " + input_filename + " > tmp_energy.txt")

g = open('tmp_energy.txt','r')
contents = g.readlines()
g.close()

#print contents
f.write("Optimization energy data\n")
for i in range(len(contents)):
    tk = contents[i].split()
    #print tk[3]
    f.write(tk[5]+"\n")
f.write("\n")

### extract force elements
os.system("grep \'Before maximum absolute gradient\' " + input_filename + " > tmp_force.txt")

g=open('tmp_force.txt', 'r')
contents = g.readlines()
g.close()

f.write("Optimization force data\n")
for i in range(len(contents)):
    tk = contents[i].split()
    print tk[6]
    f.write(tk[6]+"\n")

os.system("grep \'Maximum absolute gradient\' " + input_filename + " > tmp_force2.txt")
g=open('tmp_force2.txt', 'r')
contents = g.readlines()
g.close()
tk = contents[0].split()
f.write(tk[5]+"\n")
f.write("\n")

### extract dPosition elements
os.system("grep -ir \'Before maximum absolute dPosition\' " + input_filename + " > tmp_dPosition.txt")

g=open('tmp_dPosition.txt', 'r')
contents = g.readlines()
g.close()

f.write("Optimization dPosition data\n")
for i in range(len(contents)):
    tk = contents[i].split()
    print tk[6]
    f.write(tk[6]+"\n")

os.system("grep \'Maximum absolute dPosition\' " + input_filename + " > tmp_dPosition2.txt")
g=open('tmp_dPosition2.txt', 'r')
contents = g.readlines()
g.close()
tk = contents[0].split()
f.write(tk[5]+"\n")
f.write("\n")

### extract final geometry
os.system("grep \'Geometry_Filename\' " + input_filename + "> tmp_xyz.txt")
g = open('tmp_xyz.txt','r') 
contents = g.readlines()
g.close()

#print contents[0].split()[2]

g_tmp = open(contents[0].split()[2],'r')
contents_tmp = g_tmp.readlines()
g_tmp.close()

molecule_size = len(contents_tmp)-2
atom_index = []
for i in range(2,len(contents_tmp)):
    tk=contents_tmp[i].split()
    atom_index.append(tk[0])
#print atom_index

#SUMMARY OF GEO-OPT RESULTS
lines = 8+molecule_size
os.system("grep \'Geometry Optimization converged at\' " + input_filename +" -A "+str(lines) + " > tmp_summary.txt")

g=open('tmp_summary.txt','r')
contents = g.readlines()
g.close()

f.write("Summary of Geometry Optimization Results")
for i in range(0,8):
    f.write(contents[i])

for i in range(9,lines+1):
    tk = contents[i].split()
    
    f.write(atom_index[i-9] + "\t" + str(float(tk[1])*0.52917724882) + "\t" + str(float(tk[2])*0.52917724882) + "\t" + str(float(tk[3])*0.52917724882)+"\n")

os.system("grep \'Total time:\' " + input_filename + " > tmp_time.txt")
g=open('tmp_time.txt','r')
contents=g.readlines()
g.close()

tk=contents[0].split()
f.write("\n")
f.write("Total time = " + str(tk[2]))
os.system('rm tmp_energy.txt')
os.system('rm tmp_force.txt')
os.system('rm tmp_force2.txt')
os.system('rm tmp_dPosition.txt')
os.system('rm tmp_dPosition2.txt')
os.system('rm tmp_xyz.txt')
os.system('rm tmp_summary.txt')
f.close()
