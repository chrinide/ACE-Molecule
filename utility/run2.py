# -*- coding: utf-8 -*-
#from collections import OrderedDict
#from ase.io import read
from numpy import arange
from collections import OrderedDict
import os
import jinja2

do_run = True

template_fname = "ACE.template"
prefix = "../xyz/"
args = OrderedDict()
job_env = OrderedDict()

default_env = {
    'node_name': "intel3",
    'exe_name': "./ace",
    'server': "messi",
    'omp_num_threads': 1,
}

mols = ["PdCl4_2-", "PdMeCN2Br2", "PdMeCN2Cl2", "PdSEt22Cl2", "PdTMEDABr2", "PdTMEDACl2", "PdPPh32Cl2"]
args = OrderedDict([
    ("PdCl4_2-"  , {'elects':  40, 'solvent': 'water',
        'areas': [0.15, 0.16, 0.21, 0.24, 0.30]}),
    ("PdMeCN2Br2", {'elects':  56, 'solvent': 'acetonitrile',
        'areas': [0.21, 0.24, 0.26, 0.28, 0.30]}),
    ("PdMeCN2Cl2", {'elects':  56, 'solvent': 'acetonitrile',
        'areas': [0.20, 0.24, 0.27, 0.28, 0.33]}),
    ("PdSEt22Cl2", {'elects':  88, 'solvent': 'chloroform',
        'areas': [0.22, 0.24, 0.28, 0.30, 0.31]}),
    ("PdTMEDABr2", {'elects':  74, 'solvent': 'methylenechloride',
        'areas': [0.20, 0.21, 0.24, 0.28, 0.30]}),
    ("PdTMEDACl2", {'elects':  74, 'solvent': 'methylenechloride',
        'areas': [0.20, 0.24, 0.26, 0.28, 0.33]}),
    ("PdPPh32Cl2", {'elects': 208, 'solvent': 'chloroform',
        'areas': [0.20, 0.21, 0.24, 0.27, 0.30]}),
])

for mol in mols:
    name = mol
    args[name].update({
        'geom': prefix+mol+'.xyz',
    })
    job_env[name] = dict(default_env)
    job_env[name].update({
        'job_name': name,
        'inp_name': name+".inp",
    })


jobscript_template = """#!/bin/bash
#PBS -N {{ job_name }}
#PBS -l nodes={{ node_name }}:ppn={{ node_procs }}
#PBS -l walltime=500:00:00

date

export INPUT={{ inp_name }}
export OUTPUT={{ job_name }}.log
export EXEC={{ exe_name }}
{% if omp_num_threads != 1 %}
export NPROCS={{ mpi_nprocs }}
{% endif %}

{{ env_args }}

{% if node_name != "login" %}
cd $PBS_O_WORKDIR

echo `cat $PBS_NODEFILE`
cat $PBS_NODEFILE
{%- if omp_num_threads != 1 %}
#NPROCS=`wc -l < $PBS_NODEFILE`
export OMP_NUM_THREADS={{ omp_num_threads }}
{%- else %}
NPROCS=`wc -l < $PBS_NODEFILE`
export OMP_NUM_THREADS=1
{%- endif %}

mpirun -machinefile $PBS_NODEFILE -np $NPROCS $EXEC $PBS_O_WORKDIR/$INPUT > $PBS_O_WORKDIR/$OUTPUT
{%- else %}
export OMP_NUM_THREADS=1

mpirun -np {{ node_procs }} $EXEC ./$INPUT > ./$OUTPUT
{%- endif %}

date

"""
env_args = {
    "messi": """module purge
module load intel
module load intel_mkl
module load openmpi-1.8.3-intel
module load trilinos-12.8.1_openmp_release_intel_openmpi-1.8.3_jaechang
module load gsl-1.16_intel_openmpi-1.8.3
module load pcmsolver-1.2.1""",
    "saraswati": """export LD_LIBRARY_PATH=/home/appl/intel/compilers_and_libraries/linux//lib/intel64/:/home/wykgroup/appl/openmpi-1.10.1_build/lib:/home/wykgroup/appl/trilinos-12.8.1_Build_openmpi/lib:/home/wykgroup/appl/gsl-2.3_build/lib:/appl/intel/compilers_and_libraries/linux/mkl/lib/intel64:/home/appl/intel/compilers_and_libraries/linux/bin/intel64//lib/intel64/:/home/wykgroup/appl/gcc-5.3.0_build/lib:/home/wykgroup/appl/gcc-5.3.0_build/lib64:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/mpi/intel64/lib:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/mpi/mic/lib:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/ipp/lib/intel64:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/compiler/lib/intel64_lin:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/mkl/lib/intel64_lin:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/tbb/lib/intel64/gcc4.4:/home/appl/intel/debugger_2017/iga/lib:/home/appl/intel/debugger_2017/libipt/intel64/lib:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/daal/lib/intel64_lin:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/daal/../tbb/lib/intel64_lin/gcc4.4
export PATH=/home/wykgroup/appl/openmpi-1.10.1_build/bin:/home/wykgroup/appl/gsl-2.3_build:/home/appl/intel/compilers_and_libraries/linux/mkl/bin:/home/appl/intel/compilers_and_libraries/linux/bin/intel64//bin/intel64/:/home/wykgroup/appl/gcc-5.3.0_build/bin:/usr/bin/:/sbin/:/home/wykgroup/bin/:/home/wykgroup/bin/:.:/usr/local/maui-3.3.1/bin/:/usr/local/maui-3.3.1/sbin/:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/bin/intel64:/home/appl/intel/compilers_and_libraries_2017.1.132/linux/mpi/intel64/bin:/home/appl/intel/debugger_2017/gdb/intel64_mic/bin:/home/appl/scheduler/torque-6.1.0/bin:/home/appl/scheduler/torque-6.1.0/sbin:/usr/lib64/qt-3.3/bin:/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/wykgroup/bin""",
}

def initialize_node_procs():
    r1i0n_nodes = range(12)
    intel_nodes = range(15)
    amd_nodes = [0]
    login_nodes = [0]
    node_procs = {}
    for node in r1i0n_nodes:
        node_procs["r1i0n"+str(node)] = "24"
    for node in amd_nodes:
        node_procs["amd"+str(node)] = "16"
    for node in login_nodes:
        node_procs['login'] = "8"
    for node in intel_nodes:
        if int(node) <= 3 or int(node) >= 11:
            node_procs["intel"+str(node)] = "16"
        else:
            node_procs["intel"+str(node)] = "20"
    #if any_node:
    #    node_procs["1"] = str(int(any_node))
    return node_procs

node_procs = initialize_node_procs()
for _, env_ in job_env.items():
    env_['node_procs'] = node_procs[env_['node_name']]
    env_['env_args'] = env_args[env_['server']]

def main():
    inp_template = jinja2.Environment(loader = jinja2.FileSystemLoader('./'), keep_trailing_newline = True).get_template(template_fname)
    job_template = jinja2.Template(jobscript_template, keep_trailing_newline = True)

    i = 1
    for arg_k, arg_v in args.items():
        inp_template.stream(arg_v).dump(arg_k+".inp")
        job_template.stream(job_env[arg_k]).dump("jobscript_{}.x".format(arg_k))
        if do_run:
            print "{} {}: jobscript_{}.x".format(i, arg_k, arg_k)
            if job_env[arg_k]['node_name'] == "login":
                os.system("./jobscript_{}.x".format(arg_k))
            else:
                os.system("qsub jobscript_{}.x".format(arg_k))
        i += 1

if __name__ == "__main__":
    main()
