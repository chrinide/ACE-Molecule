#pragma once
#include <vector>

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_SerialDenseSolver.hpp"
#include "Epetra_CrsMatrix.h"

#include "Compute_Interface.hpp"
#include "Create_Compute.hpp"
#include "Poisson.hpp"
#include "../State/State.hpp"


/**
 * @brief This class provides TDDFT routines. See TDDFT_* (actual implementations) for details.
 * @author Kwangwoo Hong, Jaewook Kim, Sungwoo Kang.
 * @date 2017/01/05
 **/
class TDDFT: public Compute_Interface{
    public:
        TDDFT(Teuchos::RCP<const Basis> mesh, Teuchos::RCP<Teuchos::ParameterList> parameters);
        virtual ~TDDFT();

        /**
         * @brief Interface for compute routine.
         * @details Implementations should:
         * 2. Initialize suitable state.
         * 3. Calculate matrix_dimension and overlap matrix for _compute().
         * 4. Call compute().
         * 5. Process return values of diagonalize, returned by _compute()
         **/
        virtual int compute(Teuchos::RCP<const Basis> mesh, Teuchos::Array< Teuchos::RCP<State> >& states) = 0;

    protected:
        /**
         * @brief Actual computation routine. Should be called within compute(), after called initialize_orbital_numbers.
         * @param state State class.
         * @param matrix_dimension number of virtual orbitals * occupied orbitals, unless ABBA version.
         * @param overlap_matrix Overlap matrix for potential PAW implementation.
         **/
        Teuchos::Array< Teuchos::RCP<Diagonalize> > _compute(
            Teuchos::RCP<State> &state,
            int matrix_dimension,
            Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix = Teuchos::null
        );
        virtual void fill_TD_matrix_restricted(
                std::vector<double> orbital_energies,
                double Hartree_contrib, std::vector<double> xc_contrib,
                int matrix_index_c, int matrix_index_r,
                Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix
        ) = 0;

        Teuchos::RCP<const Basis> mesh;
        Teuchos::RCP<Poisson_Solver> poisson_solver;
        //Teuchos::RCP<const Poisson_Solver> exchange_poisson_solver;
        Teuchos::RCP<Poisson_Solver> exchange_poisson_solver;
        Teuchos::RCP<Exchange_Correlation> exchange_correlation;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > gradient;
        std::vector<int> number_of_electrons; //(i.e. with alpha spin)
        std::vector<int> number_of_orbitals; //(i.e. with alpha spin)
        std::vector<int> index_of_HOMO; //(i.e. of alpha spin)
        double cutoff;

        //void initialize_orbital_numbers(Teuchos::Array< Teuchos::RCP<const Occupation> > occupations);
        void initialize_orbital_numbers(Teuchos::RCP<State> &state);

        void print_contribution(Teuchos::RCP< Epetra_MultiVector> eigen_vectors, int index, std::vector< std::vector<int> > determinant_index, int num_request, double tolerance, bool upper_half = false);

        void get_gradient_ia(Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, int i, int a, Teuchos::RCP<Epetra_MultiVector>& grad_ia);
        void get_gradient_ia_RI(Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, int i, int a, Teuchos::RCP<Epetra_MultiVector>& grad_ia);

        /**
         * @brief Kernel matrix calculation routine, integrated from Kernel_LDA, Kernel_GGA, Kernel_GGA_shchoi, Kernel_KLI_PGG, Kernel_GGA_KLI_PGG.
         * @details Calculate kernel
         * @param functionals Functional types, which is LDA, GGA, or PGG
         * @param portions Portions for each functional types. Ignored in this case.
         * @param state Input state.
         * @param sparse_matrix Output TDDFT kernel matrix.
         * @todo Implement mGGA.
         * @note See also Exchange_Correlation, especially if mGGA should be implemented.
         * @note See also calculate_PGG_kernel_ia for PGG computation routine
         * @note Non-diagonal term is doubled (4.0 for Casida, 2.0 for TammDancoff) because of 2 spins.
         * @note It seems that "Casida" solves \f[ CZ = \Omega^2 Z \f] and "TammDancoff" solves \f[ AX = \Omega X \f].
         **/
        //virtual void get_TD_matrix(
        void get_restricted_TD_matrix(
                std::vector<std::string> functionals,
                std::vector<double> portions,
                int iroot,
                Teuchos::RCP<State> state,
                Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix
        );

        void postprocessing(
                std::vector<double>excitation_energies,
                Teuchos::RCP<Epetra_MultiVector> eigenvectors,
                Teuchos::RCP<State> state,
                int num_excitation
        );
//old comment!
        /**
         * @brief HF X kernel. See J. Chem. Phys. 134, 034120 (2011) equation 44.
         * @param state Input state.
         * @param sparse_matrix Output TDDFT kernel matrix.
         * @param
         **/
//  171113 : old function! the function is merged with Kernel_TDHF_X()
//        virtual void Kernel_HF_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, double scale = 1.0) = 0;

         /**
          * @brief Calculates TDHF kernel
          * @param state Input state.
          * @param sparse_matrix Output TDHF matrix
          * @param kernel_is_not_hybrid False for hybrid kernel, true for TDHF(EXX)-like kernel w/ DFT orbital
          * @param add_delta_term True for TDHF(EXX), False for pristine TDHF
          * @param scale Output matrix is scaled with this factor.
          * @author Sungwoo Kang, Jaewook Kim
          * @date 2017/11/13
          * @note Kernel_KSCI_X() and Kernel_HF_X() is merged (171113)
          * @note delta_term is potential_difference norm. \delta_ij<a|v_GKS-v_KS|b>-\delta_ab<i|v_GKS-v_KS|j>
          * @details
          *          Potential difference norm = <i|v^EXX-v^HF|a>
          *          A = (\epsilon_a - \epsilon_i) \delta_{ab} \delta{ij} \delta{\sigma \sigma^\prime} + (ia\sigma | jb\sigma^\prime)
          *              + f^{xc}_{ia\sigma,jb\sigma^\prime} - c_x (ab\sigma|ij\sigma^\prime) \delta_{\sigma \sigma^\prime}
          *          B =  (ia\sigma | jb\sigma^\prime) + f^{xc}_{ia\sigma,jb\sigma^\prime} - c_x (ja\sigma | ib \sigma^\prime_ \delta_{\sigma \sigma^\prime}
          **/
        virtual void Kernel_TDHF_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, bool kernel_is_not_hybrid, bool add_delta_term, double scale = 1.0) = 0;

        /**
         * @brief Calculates oscillator strength and overlap of the states
         * @param TD_excitation Eigenvector of TDDFT matrix.
         * @param state State. Orbitals and orbital eigenvalues are required.
         * @param start Minimum index of orbital that TDDFT excitations are allowed. (0 always for now)
         * @param ispin Spin index
         * @param num_excitation num_excitation in compute function.
         * @param state_overlap Output. \f[ \langle \Psi_I | r_i \Psi_0 \rangle \f].
         * @param oscillator_strength Output. Oscillator strength for each excitations.
         * @param is_deexcit True for ABBA, false for others.
         * @author Sungwoo Kang
         * @date 2017/01/06
         * @note Casida oscillator strength is fixed 2017/01/06. Reference: DOI: 10.1063/1.471140
         * @note Better equatoin for oscillator strength: 10.1063/1.3517312 eq.19
         **/
        void calculate_oscillator_strength(
                std::vector<double> excitation_energies,
                Teuchos::RCP<Epetra_MultiVector> TD_excitation,
                Teuchos::RCP<State> state,
                int start, int ispin,
                int num_excitation,
                std::vector< std::vector<double> > &state_overlap,
                std::vector<double> &oscillator_strength,
                bool is_deexcit = false
        );

        /**
         * @brief Calculates CT character analysis.
         * @param TD_excitation Eigenvector of TDDFT matrix.
         * @param state State. Orbitals and orbital eigenvalues are required.
         * @param start Minimum index of orbital that TDDFT excitations are allowed. (0 always for now)
         * @param ispin Spin index
         * @param num_excitation num_excitation in compute function.
         * @param ct_character Output. Spatial overlap, r-index, Lambda-index.
         * @param spatial_overlap Output. Spatial overlap of orbital pair.
         * @param is_deexcit True for ABBA, false for others.
         * @author Sungwoo Kang
         * @date 2017/01/06
         * @note Spatial overlap: 10.1063/1.2831900 r-index: 10.1021/ct400337e Lambda-index 10.1063/1.4922780
         **/
        void CT_analysis(
                std::vector<double> excitation_energies,
                Teuchos::RCP<Epetra_MultiVector> TD_excitation,
                Teuchos::RCP<State> state,
                int start, int ispin, int num_excitation,
                std::vector< std::vector<double> > &ct_character,
                std::vector<double> &spatial_overlap,
                bool is_deexcit = false
        );

        /**
         * @brief Calculates PGG kernel_ia.
         * @param ia_over_density density / orb_i orb_j.
         * @param orbitals Orbitals.
         * @param start Index to start.
         * @param end Index to end.
         * @param is_upper_only true for TDA, false for others.
         * @todo Should check is_upper_only is false for ABBA. It is really uncanny situation though.
         **/
        Teuchos::RCP<Epetra_Vector> calculate_PGG_kernel_ia(
                Teuchos::RCP<Epetra_Vector> ia_over_density,
                Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals,
                int start, int end, bool is_upper_only
        );
        void construct_gradient_matrix();

        /**
         * @brief print function of TDDFT excitation.
         **/
        void print_excitation_info(
            double tolerance, int order,
            Teuchos::RCP<Epetra_MultiVector> eigenvectors,
            int iroot, int iHOMO, int num_orbitals,
            std::vector<double> excitation_energies,
            std::vector< std::vector<double> > ct_character,
            std::vector< std::vector<double> > state_overlap,
            std::vector<double> oscillator_strength,
            bool upper_half = false
        );

        Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> > gradient_matrix;

        void get_gradient_ia_RI_using_matrix(Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, int i, int a, Teuchos::RCP<Epetra_MultiVector>& grad_ia);

        void get_gradient_ia_RI_using_matrix(Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, int j, int b, Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > & grads);

        std::string change_exx_kernel(std::string functional_type, std::string exchange_kernel, std::string exx_default = "PGG");

        // Not intended to use in this class, but for derived class.
        // return DFT local potential, which is used to calculate orbitals.
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_dft_potential(Teuchos::RCP<State> state);

        // return potential difference
         /**
          * @brief Calculates potential difference (v_kernel - v_orbital)  relavant to the unitary transformation of casida matrix
          * @param state State. Orbitals and orbital eigenvalues are required.
          * @param without_EXX Parameter used for KSCI scheme [a.k.a. TDHF(EXX)] Option for the calculation of xc_wo_EXX potential
          * @param kernel_is_not_hybrid False for hybrid kernel, true for TDHF(EXX)-like kernel w/ DFT orbital
          * @author Jaewook Kim
          * @date 2017/11/13
          * @note The potential difference for the case that the potential used for the ground state calculation and the TDDFT kenerl is different
          **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > calculate_v_diff(Teuchos::RCP<State> state, bool without_EXX, bool kernel_is_not_hybrid);

        // calculate < i | v_GKS - V_KS | j>
        double potential_difference_norm(Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, int i, int j, Teuchos::RCP<Epetra_Vector> potential_diff);

        //calculate (ia|jb) (two centered integral)
        //i,j = occ, a,b = unocc, uses iroot.
        //output index starts from (iroot,index_of_HOMO|iroot,index_of_HOMO), see the quadruple for statements.
        std::vector<double> get_iajb(Teuchos::RCP<const Epetra_MultiVector> orbital_value, bool use_exchange_poisson);
        std::vector<double> calculate_iajb_highmem(Teuchos::RCP<const Epetra_MultiVector> orbital_value, bool use_exchange_poisson);
        std::vector<double> calculate_iajb_lowmem(Teuchos::RCP<const Epetra_MultiVector> orbital_value, bool use_exchange_poisson);

        /**
         * @brief Get Z vector from eigenvectors and spread over processors.
         * @param input Input eigenvector.
         * @param eigvals GS eigenvalues.
         * @return output Z vector.
         **/
        virtual std::vector< std::vector<double> > gather_z_vector(
            Teuchos::RCP<Epetra_MultiVector> input,
            std::vector<double> eigvals
        ) = 0;

        /**
         * Lowest orbital index to start calculations.
         **/
        int iroot = 0;
        int num1=0;
        int num2=0;
        bool is_tdhf_kli = false;
        bool is_rtriplet = false;
        /**
         * @brief Contains which class is used? C, ABBA, or TDA?
         **/
        std::string type;
        /**
         * @brief Contains which matrix is used? Casida or TammDancoff?
         **/
        std::string theorylevel;
        std::string _exchange_kernel;
        /**
         * @brief Matrix dimension = mat_dim_scale * [# of occupied orbitals] * [# of virtual orbitals]
         **/
        int mat_dim_scale = 1;
        Teuchos::RCP<State> state;
        std::vector<double> eps_diff;
        std::vector<double> spatial_overlap;

        /**
         * @brief Change how to calculate the gradient of orbital pair.
         * @details Orb * grad(orb) if true, grad(orb*orb) if false.
         **/
        bool new_orbital_grad = true;
};
