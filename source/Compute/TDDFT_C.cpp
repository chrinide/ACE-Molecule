#include "TDDFT_C.hpp"
#include <complex>
#include <stdexcept>
#include <cmath>

#include "Epetra_CrsMatrix.h"

#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Basis/Basis_Function/Finite_Difference.hpp"
#include "../Basis/Basis_Function/Sinc.hpp"
#include "../Util/Two_e_integral.hpp"
#include "../Util/Value_Coef.hpp"
#include "../Util/Integration.hpp"
#include "../Util/Parallel_Manager.hpp"
#include "../Util/Parallel_Util.hpp"
#include "../Util/String_Util.hpp"

#define DEBUG (1)// 0 turns off fill-time output.

using std::abs;
using std::vector;
using std::endl;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

TDDFT_C::TDDFT_C(RCP<const Basis> mesh,RCP<Teuchos::ParameterList> parameters): TDDFT::TDDFT(mesh, parameters){
    if(parameters -> sublist("TDDFT").get<string>("TheoryLevel") == "TammDancoff"){
        string errmsg = "TDDFT - Class and TheoryLevel mismatch! (TDDFT_C with TammDancoff)!";
        Verbose::all() << errmsg << std::endl;
        throw std::logic_error(errmsg);
    }
    this -> type = "C";
    this -> parameters -> sublist("TDDFT").sublist("Diagonalize").get<bool>("SymmReal", true);
    if(this -> _exchange_kernel == "HF" or this -> _exchange_kernel == "HF-EXX"){
        string errmsg = "TDDFT::compute - Class and ExchangeKernel mismatch! (TDDFT_C with " + this -> _exchange_kernel + ")!";
        Verbose::all() << errmsg << std::endl;
        throw std::logic_error(errmsg);
    }
}

int TDDFT_C::compute(RCP<const Basis> mesh, Array< RCP<State> >& states){
    this -> state = rcp(new State( *states[states.size()-1]));
    state->timer->start("TDDFT::compute");

    //initialize_orbital_numbers(state->get_occupations());
    initialize_orbital_numbers(state);

    int matrix_dimension = this -> mat_dim_scale * (index_of_HOMO[0] - this -> iroot) * (number_of_orbitals[0] - index_of_HOMO[0]);
    int num_excitation = parameters->sublist("TDDFT").get<int>("NumberOfStates", matrix_dimension);

    Array< RCP<Diagonalize> > diagonalizes = this -> _compute(state, matrix_dimension);

    for(int s = 0; s < diagonalizes.size(); ++s){
        RCP<Diagonalize> diagonalize = diagonalizes[s];
        auto energies = diagonalize->get_eigenvalues();

        if(num_excitation!=energies.size()){
            Verbose::single(Verbose::Simple) << "NumberOfStates(" << num_excitation<< ") is changed to "<< energies.size() << "." <<std::endl;
            num_excitation = energies.size();
        }

        std::vector<double> excitation_energies(energies);
        // Verify excitation energies
        for(int i=0; i<num_excitation; ++i){
            if(excitation_energies[i] < 0.0){
                Verbose::single(Verbose::Simple) << "TDDFT::compute - WARNING: Casida excitation energy " << excitation_energies[i] << " is negative, which should not.\n";
                excitation_energies[i] = -sqrt(-excitation_energies[i]);
            }
            else{
                excitation_energies[i] = sqrt(excitation_energies[i]);
            }
        }
        // end
        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        if(s == 0){
            Verbose::single(Verbose::Simple) << " Singlet excitation information:" << std::endl;
        } else {
            Verbose::single(Verbose::Simple) << " Triplet excitation information:" << std::endl;
        }
        this -> postprocessing(excitation_energies, diagonalize -> get_eigenvectors(), state, num_excitation);
        Verbose::single(Verbose::Simple) <<   "#---------------------------------------------------------------------\n";
    }

    Parallel_Manager::info().all_barrier();
    state->timer->end("TDDFT::compute");
    states.push_back(state);

    return 0;
}

void TDDFT_C::fill_TD_matrix_restricted(
    vector<double> orbital_energies,
    double Hartree_contrib, vector<double> xc_contrib,
    int matrix_index_c, int matrix_index_r,
    Array< RCP<Epetra_CrsMatrix> >& sparse_matrix
){
    const int num_virt_orb = this -> number_of_orbitals[0] - this -> index_of_HOMO[0];
    int i = matrix_index_c / num_virt_orb + this -> iroot;
    int a = matrix_index_c % num_virt_orb + this -> index_of_HOMO[0];
    int j = matrix_index_r / num_virt_orb + this -> iroot;
    int b = matrix_index_r % num_virt_orb + this -> index_of_HOMO[0];
    double eps_ai = orbital_energies[a] - orbital_energies[i];

    // *2 because both up and down spin should be considered.
    double element_tmp = 4.0 * sqrt(eps_ai)
        * sqrt(orbital_energies[b] - orbital_energies[j]) * (Hartree_contrib + xc_contrib[0]);
    if(i==j and a==b){
        element_tmp += eps_ai*eps_ai;
    }
    if(std::fabs(element_tmp) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            if(sparse_matrix[0]->MyGlobalRow(matrix_index_c) ){
                sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
            }
            if(sparse_matrix[0]->MyGlobalRow(matrix_index_r)) {
                sparse_matrix[0]->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
            }
        }
        else{
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
        }
    }
    if(sparse_matrix.size() > 1){
        element_tmp = xc_contrib[1];
        if(i==j and a==b){
            element_tmp += eps_ai*eps_ai;
        }
        element_tmp += 4.0 * sqrt(eps_ai)
            * sqrt(orbital_energies[b] - orbital_energies[j]) * xc_contrib[1];
        if(std::fabs(element_tmp) > this -> cutoff){
            if(matrix_index_c!=matrix_index_r){
                if(sparse_matrix[1]->MyGlobalRow(matrix_index_c) ){
                    sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
                }
                if(sparse_matrix[1]->MyGlobalRow(matrix_index_r)) {
                    sparse_matrix[1]->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
                }
            }
            else{
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
            }
        }
    }
}

void TDDFT_C::Kernel_TDHF_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, bool kernel_is_not_hybrid, bool add_delta_term, double scale/* = 1.0*/){
    throw std::logic_error("Not implemented!");
    return;
}

std::vector< std::vector<double> > TDDFT_C::gather_z_vector(RCP<Epetra_MultiVector> input, std::vector<double> eigvals){
    int num_vec = input -> NumVectors();
    int total_size = input -> Map().NumGlobalElements();
    double ** recv = new double*[num_vec];
    for(int i = 0; i < num_vec; ++i){
        recv[i] = new double[total_size];
    }
    Parallel_Util::group_allgather_multivector(input, recv);
    vector< vector<double> > output(num_vec);
    for(int i = 0; i < num_vec; ++i){
        output[i] = vector<double>(recv[i], recv[i]+total_size);
    }
    for(int i = 0; i < num_vec; ++i){
        delete[] recv[i];
    }
    delete[] recv;
    return output;
}
