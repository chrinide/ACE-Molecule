#include "TDDFT_ABBA.hpp"

#include <cmath>
#include <complex>

#include "Teuchos_Time.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include "Epetra_CrsMatrix.h"

#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Util/Two_e_integral.hpp"
#include "../Util/Value_Coef.hpp"
#include "../Util/Parallel_Manager.hpp"
#include "../Util/Parallel_Util.hpp"
#include "../Util/String_Util.hpp"

#define DEBUG (1)// 0 truns off fill-time output

using std::abs;
using std::vector;
using std::endl;
using std::string;
using std::setw;
using std::setiosflags;
using std::ios;
using Teuchos::SerialDenseMatrix;
using Teuchos::Time;
using Teuchos::TimeMonitor;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

TDDFT_ABBA::TDDFT_ABBA(RCP<const Basis> mesh,RCP<Teuchos::ParameterList> parameters)
: TDDFT::TDDFT(mesh, parameters){
    if(parameters -> sublist("TDDFT").get<string>("TheoryLevel") == "TammDancoff"){
        string errmsg = "TDDFT::compute - Class and TheoryLevel mismatch! (TDDFT_ABBA with TammDancoff)!";
        Verbose::all() << errmsg << std::endl;
        throw std::logic_error(errmsg);
    }
    this -> type = "ABBA";
    parameters->sublist("TDDFT").sublist("Diagonalize").set<string>("Solver","Direct");
    this -> parameters -> sublist("TDDFT").sublist("Diagonalize").get<bool>("SymmReal", false);
    this -> mat_dim_scale = 2;
    //parameters->sublist("TDDFT").sublist("Diagonalize").set<string>("Solver","Pure");
    //parameters -> sublist("TDDFT").sublist("Diagonalize").set<bool>("OverlapPositiveDefinite", false);
}

int TDDFT_ABBA::compute(RCP<const Basis> mesh, Array< RCP<State> >& states){
    this -> state = rcp(new State( *states[states.size()-1]));
    state->timer->start("TDDFT::compute");

    initialize_orbital_numbers(state);

    int matrix_dimension = this -> mat_dim_scale * (index_of_HOMO[0] - this -> iroot) * (number_of_orbitals[0] - index_of_HOMO[0]);

    int num_excitation = parameters->sublist("TDDFT").get<int>("NumberOfStates", matrix_dimension);
    if(num_excitation <= matrix_dimension/2){
        num_excitation += matrix_dimension/2;
        parameters->sublist("TDDFT").set<int>("NumberOfStates", num_excitation);
    }

    Epetra_Map config_map(matrix_dimension,0,this -> mesh -> get_map()->Comm());
    RCP<Epetra_CrsMatrix> overlap_matrix = rcp(new Epetra_CrsMatrix(Copy, config_map, 0));
    for(int i = 0; i < matrix_dimension; ++i){
        double val = (i < matrix_dimension/2)? 1.: -1.;
        if(config_map.MyGID(i)){
            overlap_matrix -> InsertGlobalValues(i, 1, &val, &i);
        }
    }
    overlap_matrix -> FillComplete();

    auto diagonalizes = this -> _compute(state, matrix_dimension, overlap_matrix);

    for(int s = 0; s < diagonalizes.size(); ++s){
        RCP<Diagonalize> diagonalize = diagonalizes[s];
        auto energies = diagonalize->get_eigenvalues();

        // Verify excitation energies
        vector<double> excitation_energies;
        for(int i = 0; i < matrix_dimension/2; ++i){
            if(std::real(energies[i]) > 0.0){
                Verbose::single(Verbose::Simple) << "Eigenvalue of #" << i-matrix_dimension/2 << " is positive, which should not!" << std::endl;
            }
        }
        for(int i = matrix_dimension/2; i < num_excitation; ++i){
            excitation_energies.push_back(std::real(energies[i]));
        }
        RCP<Epetra_MultiVector> eigenvectors = rcp(new Epetra_MultiVector(diagonalize->get_eigenvectors()->Map(), num_excitation - matrix_dimension/2));
        for(int i = matrix_dimension/2; i < num_excitation; ++i){
            eigenvectors -> operator()(i-matrix_dimension/2) -> Update(1.0, *diagonalize -> get_eigenvectors() -> operator()(i), 0.0);
        }
        //num_excitation = excitation_energies.size();
        // end
        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        if(s == 0){
            Verbose::single(Verbose::Simple) << " Singlet excitation information:" << std::endl;
        } else {
            Verbose::single(Verbose::Simple) << " Triplet excitation information:" << std::endl;
        }
        this -> postprocessing(excitation_energies, eigenvectors, state, excitation_energies.size());
        Verbose::single(Verbose::Simple) <<   "#---------------------------------------------------------------------\n";
    }
    Parallel_Manager::info().all_barrier();

    state->timer->end("TDDFT::compute");
    states.push_back(state);

    return 0;
}

void TDDFT_ABBA::fill_TD_matrix_restricted(
    vector<double> orbital_energies,
    double Hartree_contrib, vector<double> xc_contrib,
    int matrix_index_c, int matrix_index_r,
    Array< RCP<Epetra_CrsMatrix> >& sparse_matrix
){
    const int num_virt_orb = this -> number_of_orbitals[0] - this -> index_of_HOMO[0];
    int i = matrix_index_c / num_virt_orb + this -> iroot;
    int a = matrix_index_c % num_virt_orb + this -> index_of_HOMO[0];
    int j = matrix_index_r / num_virt_orb + this -> iroot;
    int b = matrix_index_r % num_virt_orb + this -> index_of_HOMO[0];
    int num_excits = (this -> index_of_HOMO[0] - this -> iroot) * num_virt_orb;

    // A1, B1: A, B. A2, B2: -A, -B.
    // 170807 FIX: No -A -B.
    // *2 because restricted singlet.
    double element_B1 = 2.0 * (Hartree_contrib + xc_contrib[0]);
    //double element_B2 = -element_B1;
    double element_A1 = element_B1;
    if(i==j and a==b){
        element_A1 += std::real(orbital_energies[a]) - std::real(orbital_energies[i]);
    }
    //double element_A2 = -element_A1;

    // A B -> A B
    // B A   -B-A
    // 170807 FIX: A B // B A
    int matrix_index_c2 = matrix_index_c + num_excits;
    int matrix_index_r2 = matrix_index_r + num_excits;
    if(std::fabs(element_B1) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_B1,&matrix_index_r);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_r,1,&element_B1,&matrix_index_c);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_r);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_B2,&matrix_index_r2);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_r,1,&element_B2,&matrix_index_c2);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_c2,1,&element_B2,&matrix_index_r2);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_B2,&matrix_index_c2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c ,1,&element_A1,&matrix_index_r );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c ,1,&element_B1,&matrix_index_r2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_r );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c2,1,&element_A1,&matrix_index_r2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
        }
        else{
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_c);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_A1,&matrix_index_c);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_B2,&matrix_index_c2);
            //sparse_matrix[0]->InsertGlobalValues(matrix_index_c2,1,&element_A2,&matrix_index_c2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
            sparse_matrix[0]->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
        }
    }
    if(sparse_matrix.size() > 1){
        element_B1 = 2.0 * xc_contrib[1];
        //element_B2 = -element_B1;
        element_A1 = element_B1;
        if(i==j and a==b){
            element_A1 += std::real(orbital_energies[a]) - std::real(orbital_energies[i]);
        }
        //double element_A2 = -element_A1;
        // A B -> A B
        // B A   -B-A
        // 170807 FIX: A B // B A
        int matrix_index_c2 = matrix_index_c + num_excits;
        int matrix_index_r2 = matrix_index_r + num_excits;
        if(std::fabs(element_B1) > this -> cutoff){
            if(matrix_index_c!=matrix_index_r){
                /*
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_B1,&matrix_index_r);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r,1,&element_B1,&matrix_index_c);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_r);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_B2,&matrix_index_r2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r,1,&element_B2,&matrix_index_c2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c2,1,&element_B2,&matrix_index_r2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_B2,&matrix_index_c2);
                */
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c ,1,&element_A1,&matrix_index_r );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c ,1,&element_B1,&matrix_index_r2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_r );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c2,1,&element_A1,&matrix_index_r2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
            }
            else{
                //sparse_matrix[1]->InsertGlobalValues(matrix_index_c2,1,&element_B1,&matrix_index_c);
                //sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_A1,&matrix_index_c);
                //sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_B2,&matrix_index_c2);
                //sparse_matrix[1]->InsertGlobalValues(matrix_index_c2,1,&element_A2,&matrix_index_c2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r ,1,&element_A1,&matrix_index_c );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r ,1,&element_B1,&matrix_index_c2);
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_B1,&matrix_index_c );
                sparse_matrix[1]->InsertGlobalValues(matrix_index_r2,1,&element_A1,&matrix_index_c2);
            }
        }
    }
}

//void TDDFT_ABBA::Kernel_KSCI_X(RCP<State> state, Array< RCP<Epetra_CrsMatrix> >& sparse_matrix, bool is_TDHF, double scale/* = 1.0*/){
void TDDFT_ABBA::Kernel_TDHF_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, bool kernel_is_not_hybrid, bool add_delta_term, double scale/* = 1.0*/){
    auto map = mesh->get_map();

    Array< RCP<Epetra_MultiVector> > orbitals;
    for(int i_spin=0; i_spin<state->get_occupations().size(); ++i_spin){
        orbitals.push_back(rcp(new Epetra_MultiVector(*map, state->get_orbitals()[i_spin]->NumVectors())));
        orbitals[i_spin]->Update(1.0, *state->get_orbitals()[i_spin], 0.0);
        Value_Coef::Value_Coef(mesh, orbitals[i_spin], false, true, orbitals[i_spin]);
    }

    // For HF
    RCP<Epetra_Vector> Kib = rcp(new Epetra_Vector(*map)); //occ-vir
    // For HF, LDA & GGA
    RCP<Epetra_Vector> Kia = rcp(new Epetra_Vector(*map)); //occ-vir
    RCP<Epetra_Vector> Kij = rcp(new Epetra_Vector(*map)); //occ-occ
    // For LDA & GGA
    RCP<Epetra_Vector> ia_coeff = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> jb_coeff = rcp(new Epetra_Vector(*map));
    // end

    double EXX_portion = scale;
    // V_GKS - V_KS + a0 * V_HF
    Array< RCP<Epetra_Vector> > potential_diff_wo_EXX;
    if(add_delta_term){
        potential_diff_wo_EXX = calculate_v_diff(state, true, kernel_is_not_hybrid);
        if(potential_diff_wo_EXX.size()==2){
            Verbose::all() << "TDDFT::Kernel_TDHF_X, something wrong ! EXX_portion : " << EXX_portion << "    potential_diff_wo_EXX.size() : " << potential_diff_wo_EXX.size() << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    int number_of_virtual_orbitals = number_of_orbitals[0] - index_of_HOMO[0];
    /*
           int index_i = i * number_of_virtual_orbitals;
           int index_j = j * number_of_virtual_orbitals;
           int index_a = a - index_of_HOMO[0];
           int index_b = b - index_of_HOMO[0];
           matrix_index_c = index_i + index_a = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
           matrix_index_r = index_j + index_b = j * number_of_virtual_orbitals + b - index_of_HOMO[0];
     */
    //* /// KSW TEST // 800% increment (for delta vab)
    //170822 : 1st iajb! (ia|jb)
            // Delta v_ab delta_ij
            //  \delta_ij < \phi_a | vx^NL - vx | \phi_b >
            // Delta v_ab =  -sum_k(ak|kb) + potential_difference_norm(a,b)
    //170822 : 6st iajb! (ib|ja)
            // exchange - Bterm : b-->a, a-->b
    //openmp + mpi, precaclaulate (ia|jb)
    // tnx to jaechang, see TDDFT::get_iajb()
    //old variable name
    int matrix_index_c = 0;
    int matrix_index_r = 0;
    //for delta vij
    int delta_matrix_index_c = 0;
    int delta_matrix_index_r = 0;
    //for exchange-B
    int B_matrix_index_c = 0;
    int B_matrix_index_r = 0;

    Verbose::single(Verbose::Detail) << "TDDFT::get_iajb" << std::endl;
#if DEBUG == 1
        internal_timer -> start("get_iajb");
#endif
    std::vector<double> iajb = get_iajb(orbitals[0], true);
#if DEBUG == 1
        internal_timer -> end("get_iajb");
        Verbose::single(Verbose::Detail) << "TDDFT_ABBA::Kernel_TDHF_X get_iajb time = " << internal_timer -> get_elapsed_time("get_iajb",-1) << std::endl;
#endif
    int index_iajb = 0;

    for(int i=0; i<index_of_HOMO[0]; ++i){
#if DEBUG == 1
        internal_timer -> start("i-loop");
#endif
        for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
            delta_matrix_index_r = delta_matrix_index_c;

//            Kia->PutScalar(0.0);
            ////from B
            //Kib->PutScalar(0.0);
//            Two_e_integral::compute_Kji(mesh, orbitals[0]->operator()(i), orbitals[0]->operator()(a), exchange_poisson_solver, Kia);
            ////from B
            //Two_e_integral::compute_Kji(mesh, orbitals[0]->operator()(i), orbitals[0]->operator()(b), exchange_poisson_solver, Kib);

            for(int j=i; j<index_of_HOMO[0]; ++j){
                for(int b = (j==i)? a: index_of_HOMO[0]; b<number_of_orbitals[0]; ++b){
                    ////from A
//                    double element_iajb = Two_e_integral::compute_two_center_integral(mesh, Kia, orbitals[0]->operator()(b), orbitals[0]->operator()(j)) ;
                    double element_iajb = iajb[index_iajb];
                    int delta_matrix_index_c2 = delta_matrix_index_c + index_of_HOMO[0] * number_of_virtual_orbitals;
                    int delta_matrix_index_r2 = delta_matrix_index_r + index_of_HOMO[0] * number_of_virtual_orbitals;

                    ////from B
                    //B_matrix_index_r = i*number_of_virtual_orbitals + a - index_of_HOMO[0];
                    //B_matrix_index_c = j*number_of_virtual_orbitals + b - index_of_HOMO[0];
                    B_matrix_index_r = i*number_of_virtual_orbitals + b - index_of_HOMO[0];
                    B_matrix_index_c = j*number_of_virtual_orbitals + a - index_of_HOMO[0];
                    //double element_tmp1 = -Two_e_integral::compute_two_center_integral(mesh, Kib, orbitals[0]->operator()(j), orbitals[0]->operator()(a)) * scale;
                    double element_tmp1 = -element_iajb * scale;
                    //double element_tmp3 = -element_tmp1;
                    // A B -> A B
                    // B A   -B-A
                    // only B terms
                    // 170807 FIX: -B removed
                    int B_matrix_index_c2 = B_matrix_index_c + index_of_HOMO[0] * number_of_virtual_orbitals;
                    int B_matrix_index_r2 = B_matrix_index_r + index_of_HOMO[0] * number_of_virtual_orbitals;

                    if(std::fabs(element_tmp1) > this -> cutoff){
                        for(int s = 0; s < sparse_matrix.size(); ++s){
                            if(B_matrix_index_c!=B_matrix_index_r){
                                //sparse_matrix[s]->InsertGlobalValues(B_matrix_index_c2,1,&element_tmp1,&B_matrix_index_r);
                                //sparse_matrix[s]->InsertGlobalValues(B_matrix_index_r2,1,&element_tmp1,&B_matrix_index_c);
                                //sparse_matrix[s]->InsertGlobalValues(B_matrix_index_c,1,&element_tmp3,&B_matrix_index_r2);
                                //sparse_matrix[s]->InsertGlobalValues(B_matrix_index_r,1,&element_tmp3,&B_matrix_index_c2);
                                sparse_matrix[s]->InsertGlobalValues(B_matrix_index_c ,1,&element_tmp1,&B_matrix_index_r2);
                                sparse_matrix[s]->InsertGlobalValues(B_matrix_index_r ,1,&element_tmp1,&B_matrix_index_c2);
                                sparse_matrix[s]->InsertGlobalValues(B_matrix_index_c2,1,&element_tmp1,&B_matrix_index_r );
                                sparse_matrix[s]->InsertGlobalValues(B_matrix_index_r2,1,&element_tmp1,&B_matrix_index_c );
                            }
                            else{
                                //sparse_matrix[s]->InsertGlobalValues(B_matrix_index_c,1,&element_tmp1,&B_matrix_index_c2);
                                //sparse_matrix[s]->InsertGlobalValues(B_matrix_index_c2,1,&element_tmp3,&B_matrix_index_c);
                                sparse_matrix[s]->InsertGlobalValues(B_matrix_index_r ,1,&element_tmp1,&B_matrix_index_c2);
                                sparse_matrix[s]->InsertGlobalValues(B_matrix_index_r2,1,&element_tmp1,&B_matrix_index_c );
                            }
                        }
                    }
                    ////from B end

                    //  \delta_ij < \phi_a | vx^NL - vx | \phi_b >
                    // Delta v_ab =  -sum_k(ak|kb) + potential_difference_norm(a,b)
                    if(add_delta_term){
                        if(i==j){
                            double element_tmp_vdiff = potential_difference_norm(state->get_orbitals()[0],a,b,potential_diff_wo_EXX[0]);
                            //double element_tmp_m_vdiff = - element_tmp_vdiff;
                            for(int s = 0; s < sparse_matrix.size(); ++s){
                                // A
                                //sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c,1,&element_tmp_vdiff,&delta_matrix_index_r);
                                sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r ,1,&element_tmp_vdiff, &delta_matrix_index_c );
                                // -A
                                // 170807 FIX: -A removed
                                //sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c2,1,&element_tmp_m_vdiff,&delta_matrix_index_r2);
                                sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r2 ,1,&element_tmp_vdiff, &delta_matrix_index_c2 );
                                if(delta_matrix_index_r!=delta_matrix_index_c){
                                    //sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r,1,&element_tmp_vdiff,&delta_matrix_index_c);
                                    //sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r2,1,&element_tmp_m_vdiff,&delta_matrix_index_c2);
                                    sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c ,1,&element_tmp_vdiff,&delta_matrix_index_r );
                                    sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r ,1,&element_tmp_vdiff,&delta_matrix_index_c );
                                }

                                //-sum_k(ak|kb) start
                                for(int k=0;k<index_of_HOMO[0];++k){
                                    //ia,jb = ka,kb , (bj|ia) = (bk|ka)
                                    double element_HFx = - element_iajb * scale;
                                    //double element_m_HFx = -element_HFx;
                                    // A
                                    delta_matrix_index_c2 = k * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                    delta_matrix_index_r2 = k * number_of_virtual_orbitals + b - index_of_HOMO[0];
                                    //sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c2,1,&element_HFx,&delta_matrix_index_r2);
                                    sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r2,1,&element_HFx,&delta_matrix_index_c2);
                                    if(a!=b){
                                        //sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r2,1,&element_HFx,&delta_matrix_index_c2);
                                        sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c2,1,&element_HFx,&delta_matrix_index_r2);
                                    }
                                    // -A
                                    delta_matrix_index_c2 += index_of_HOMO[0] * number_of_virtual_orbitals;
                                    delta_matrix_index_r2 += index_of_HOMO[0] * number_of_virtual_orbitals;
                                    //sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c2,1,&element_m_HFx,&delta_matrix_index_r2);
                                    sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r2,1,&element_HFx,&delta_matrix_index_c2);
                                    if(delta_matrix_index_r2!=delta_matrix_index_c2){
                                        //sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r2,1,&element_m_HFx,&delta_matrix_index_c2);
                                        sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c2,1,&element_HFx,&delta_matrix_index_r2);
                                    }
                                }
                                //-sum_k(ak|kb) end
                            }// for(int s = 0; ...)
                        }// if(i == j)
                    }// if(add_delta_term)
                    ++delta_matrix_index_r;
                    ++index_iajb;
                }//for(b)
            }//for(j)
            ++delta_matrix_index_c;
        }//for (a)
#if DEBUG == 1
        internal_timer -> end("i-loop");
        Verbose::single(Verbose::Detail) << "TDDFT_ABBA::Kernel_TDHF_X index for Delta v + B i = " << i << " time = "<< internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
#endif
    }
    // */ /// KSW TEST
    // end
    // exchange - A
    // Delta v_ij =  sum_k(jk|ki) - potential_difference_norm(j,i)
    /*
           int index_i = i * number_of_virtual_orbitals;
           int index_j = j * number_of_virtual_orbitals;
           int index_a = a - index_of_HOMO[0];
           int index_b = b - index_of_HOMO[0];
           matrix_index_c = index_i + index_a = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
           matrix_index_r = index_j + index_b = j * number_of_virtual_orbitals + b - index_of_HOMO[0];
     */
    //170822 : 2st iajb! (ij|ab)
    //170822 : 3st iajb! (ij|ij)
    //170822 : 4st iajb! (ij|ik)
    //170822 : 5st iajb! (ij|jk)
    for(int i=0; i<index_of_HOMO[0]; ++i){
#if DEBUG == 1
        internal_timer -> start("i-loop");
#endif
        int index_i = i * number_of_virtual_orbitals;

        for(int j=i; j<index_of_HOMO[0]; ++j){
            int index_j = j * number_of_virtual_orbitals;

            Kij->PutScalar(0.0);
            Two_e_integral::compute_Kji(mesh, orbitals[0]->operator()(i), orbitals[0]->operator()(j), exchange_poisson_solver, Kij);

            for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
                int index_a = a - index_of_HOMO[0];
                matrix_index_c = index_i + index_a;

                int b = (j==i)? a: index_of_HOMO[0];
                for(; b<number_of_orbitals[0]; ++b){
                    int index_b = b - index_of_HOMO[0];
                    matrix_index_c = index_i + index_a;
                    matrix_index_r = index_j + index_b;
                    double element_ijab = Two_e_integral::compute_two_center_integral(mesh, Kij, orbitals[0]->operator()(a), orbitals[0]->operator()(b));

                    double element_tmp1 = -element_ijab * scale;
                    //double element_tmp3 = -element_tmp1;
                    // A B -> A B
                    // B A   -B-A
                    // only A terms
                    // 170807 FIX: A B // B A
                    int matrix_index_c2 = matrix_index_c + index_of_HOMO[0] * number_of_virtual_orbitals;
                    int matrix_index_r2 = matrix_index_r + index_of_HOMO[0] * number_of_virtual_orbitals;
                    for(int s = 0; s < sparse_matrix.size(); ++s){
                        if(std::fabs(element_tmp1) > this -> cutoff){
                            if(matrix_index_c!=matrix_index_r){
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_tmp1,&matrix_index_r);
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_tmp1,&matrix_index_c);
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_tmp3,&matrix_index_r2);
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_tmp3,&matrix_index_c2);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_c ,1,&element_tmp1,&matrix_index_r );
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r ,1,&element_tmp1,&matrix_index_c );
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_tmp1,&matrix_index_r2);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_tmp1,&matrix_index_c2);
                            }
                            else{
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_tmp1,&matrix_index_c);
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_tmp3,&matrix_index_c2);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r ,1,&element_tmp1,&matrix_index_c );
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_tmp1,&matrix_index_c2);
                            }
                        }

                        //* /// KSW TEST  // Barely changed.
                        //  - \delta_ab < \phi_i | vx^NL - vx | \phi_j >
                        if(add_delta_term){
                            // 이 뒤쪽에서 i랑 j가 vx^NL의 k랑 섞이므로 단순 i==j는 안됨.
                            if(a==b){
                                // Delta v_ij =  sum_k(jk|ki) - potential_difference_norm(j,i)
                                //double element_tmp_vdiff = potential_difference_norm(state->get_orbitals()[0],j,i,potential_diff_wo_EXX[0]);
                                //double element_tmp_m_vdiff = - element_tmp_vdiff;
                                // delta_ij가 없을 때 or delta_ij가 있을 때 ij 같음.
                                if(!this -> is_tdhf_kli or i == j){
                                    double element_tmp_vdiff = -potential_difference_norm(state->get_orbitals()[0],j,i,potential_diff_wo_EXX[0]);
                                    // upper A
                                    //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_tmp_m_vdiff,&matrix_index_r);
                                    sparse_matrix[s]->InsertGlobalValues(matrix_index_r ,1,&element_tmp_vdiff,&matrix_index_c);
                                    // lower A, former -A (<170807)
                                    //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_tmp_vdiff,&matrix_index_r2);
                                    sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_tmp_vdiff,&matrix_index_c2);
                                    if(matrix_index_r!=matrix_index_c){
                                        //sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_tmp_m_vdiff,&matrix_index_c);
                                        //sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_tmp_vdiff,&matrix_index_c2);
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_c ,1,&element_tmp_vdiff, &matrix_index_r );
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_tmp_vdiff, &matrix_index_r2);
                                    }
                                }
                                // sum_k(jk|ki)
                                // 이 부분이 sum_k (jk|ki)를 계산하는 부분이라면, 왜 (ij|ij) = (ji|ji)를 계산하는가?
                                // 여기서는 i가 i==j일 때이고 j가 k기 때문이다. 실제로는 (jk|ik) = (jk|ki) for i==j
                                double element_ijij = Two_e_integral::compute_two_center_integral(mesh, Kij, orbitals[0]->operator()(i), orbitals[0]->operator()(j)) * scale;
                                //double element_m_ijij = - element_ijij;
                                //matrix_index_c = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                //matrix_index_r = j * number_of_virtual_orbitals + b - index_of_HOMO[0];

                                //ia,jb = ia,ia , (ij|ji)
                                // upper A
                                matrix_index_c = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                matrix_index_r = matrix_index_c;
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_ijij,&matrix_index_r);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r, 1,&element_ijij,&matrix_index_c);
                                // lower A, former -A (<170807)
                                matrix_index_c2 = matrix_index_c + index_of_HOMO[0] * number_of_virtual_orbitals;
                                matrix_index_r2 = matrix_index_c2;
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_m_ijij,&matrix_index_r2);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_ijij,&matrix_index_c2);

                                // 이 부분이 sum_k (jk|ki)를 계산하는 부분이라면, 왜 (ij|ij) = (ji|ji)를 계산하는가?
                                // 여기서는 j가 i==j일 때이고 i가 k기 때문이다. 실제로는 (jk|ik) = (jk|ki) for i==j
                                if(i!=j){
                                    //ia,jb = ja,ja , (ij|ji)
                                    //A
                                    matrix_index_c = j * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                    matrix_index_r = matrix_index_c;
                                    //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_ijij,&matrix_index_r);
                                    sparse_matrix[s]->InsertGlobalValues(matrix_index_r ,1,&element_ijij,&matrix_index_c );
                                    //-A
                                    // 170807 FIX: -A removed
                                    matrix_index_c2 = matrix_index_c + index_of_HOMO[0] * number_of_virtual_orbitals;
                                    matrix_index_r2 = matrix_index_c2;
                                    //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_m_ijij,&matrix_index_r2);
                                    sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_ijij  ,&matrix_index_c2);
                                }//if(i != j)
                                // sum_k(jk|ki)를 계산하는 부분이라면, (ki|ij) = (ji|ki)가 무슨 관련인가?
                                // 이 for(k)에서만 k가 i기 때문이다. 2e integral은 (ki|ij)지만 matrix에 들어가는건 (ik|kj)
                                // delta_ij가 없을 때 or delta_ij가 있을 때 ij 같음.
                                if(!this -> is_tdhf_kli or i == j){
                                    for(int k=j+1;k<index_of_HOMO[0];++k){
                                        double element_kiij = Two_e_integral::compute_two_center_integral(mesh, Kij, orbitals[0]->operator()(i), orbitals[0]->operator()(k)) * scale;
                                        //double element_m_kiij = - element_kiij;
                                        //ia,jb = ka,jb, (ki|ij)
                                        // upper A
                                        matrix_index_c = k * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                        matrix_index_r = j * number_of_virtual_orbitals + b - index_of_HOMO[0];
                                        //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_kiij,&matrix_index_r);
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_r ,1,&element_kiij,&matrix_index_c );
                                        if(matrix_index_r!=matrix_index_c){
                                            //sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_kiij,&matrix_index_c);
                                            sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_kiij,&matrix_index_r );
                                        }
                                        // lower A, former -A (<170807)
                                        matrix_index_c2 = matrix_index_c + index_of_HOMO[0] * number_of_virtual_orbitals;
                                        matrix_index_r2 = matrix_index_r + index_of_HOMO[0] * number_of_virtual_orbitals;
                                        //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_m_kiij,&matrix_index_r2);
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_kiij,&matrix_index_c2);
                                        if(matrix_index_r2!=matrix_index_c2){
                                            //sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_m_kiij,&matrix_index_c2);
                                            sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_kiij,&matrix_index_r2);
                                        }
                                    }//for(k)
                                }//if(!tdhf_kli or i==j)
                                // sum_k(jk|ki)를 계산하는 부분이라면, (kj|ij) = (jk|ji)가 무슨 관련인가?
                                // 이 for(k)에서만 k가 j기 때문이다. 2e integral은 (kj|ij)지만 matrix에 들어가는건 (jk|ik) = (jk|ki)
                                // delta_ij가 없을 때 i != j인 경우.
                                if(!this -> is_tdhf_kli and i!=j){
                                    for(int k=i+1;k<index_of_HOMO[0];++k){
                                        double element_kjij = Two_e_integral::compute_two_center_integral(mesh, Kij, orbitals[0]->operator()(j), orbitals[0]->operator()(k)) * scale;
                                        //double element_m_kjij = - element_kjij;
                                        //ia,jb = ia,kb, (kj|ij)
                                        //A
                                        matrix_index_c = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                        matrix_index_r = k * number_of_virtual_orbitals + b - index_of_HOMO[0];
                                        if(matrix_index_r!=matrix_index_c){
                                            //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_kjij,&matrix_index_r);
                                            sparse_matrix[s]->InsertGlobalValues(matrix_index_r ,1,&element_kjij,&matrix_index_c );
                                        }
                                        //sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_kjij,&matrix_index_c);
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_c ,1,&element_kjij,&matrix_index_r );
                                        //-A
                                        // 170807 FIX: -A removed
                                        matrix_index_c2 = matrix_index_c + index_of_HOMO[0] * number_of_virtual_orbitals;
                                        matrix_index_r2 = matrix_index_r + index_of_HOMO[0] * number_of_virtual_orbitals;
                                        //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_m_kjij,&matrix_index_r2);
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_kjij,&matrix_index_c2);
                                        if(matrix_index_r!=matrix_index_c){
                                            //sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_m_kjij,&matrix_index_c2);
                                            sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_kjij,&matrix_index_r2);
                                        }
                                    }
                                }//if(!is_tdhf_kli and i != j)
                            }//if(a == b)
                            // */ /// KSW TEST
                        }//if(add_delta_term)
                    }//for(s)
                }//for(b)
            }//for(j)
        }//for(a)
#if DEBUG == 1
        internal_timer -> end("i-loop");
        Verbose::single(Verbose::Detail) << "TDDFT_ABBA::Kernel_TDHF_X index for rest exchange  i = " << i << " time = " << internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
#endif
    }
    // end
    //
/**************170822********
    // exchange - B term
    //        int number_of_virtual_orbitals = number_of_orbitals[0] - index_of_HOMO[0];
    //170822 : 6st iajb! (ib|ja)
    for(int i=0; i<index_of_HOMO[0]; ++i){
        for(int b=index_of_HOMO[0];b<number_of_orbitals[0];++b){
            Kib->PutScalar(0.0);
            Two_e_integral::compute_Kji(mesh, orbitals[0]->operator()(i), orbitals[0]->operator()(b), exchange_poisson_solver, Kib);
            for(int j=i;j<index_of_HOMO[0];++j){
                int a = (j==i)? b: index_of_HOMO[0];
                for(;a<number_of_orbitals[0];++a){
                    matrix_index_r = i*number_of_virtual_orbitals + a - index_of_HOMO[0];
                    matrix_index_c = j*number_of_virtual_orbitals + b - index_of_HOMO[0];

                    double element_tmp1 = -Two_e_integral::compute_two_center_integral(mesh, Kib, orbitals[0]->operator()(j), orbitals[0]->operator()(a)) * scale;
                    //double element_tmp3 = -element_tmp1;
                    // A B -> A B
                    // B A   -B-A
                    // only B terms
                    // 170807 FIX: -B removed
                    int matrix_index_c2 = matrix_index_c + index_of_HOMO[0] * number_of_virtual_orbitals;
                    int matrix_index_r2 = matrix_index_r + index_of_HOMO[0] * number_of_virtual_orbitals;
                    for(int s = 0; s < sparse_matrix.size(); ++s){
                        if(std::fabs(element_tmp1) > this -> cutoff){
                            if(matrix_index_c!=matrix_index_r){
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_tmp1,&matrix_index_r);
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_tmp1,&matrix_index_c);
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_tmp3,&matrix_index_r2);
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_tmp3,&matrix_index_c2);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_c ,1,&element_tmp1,&matrix_index_r2);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r ,1,&element_tmp1,&matrix_index_c2);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_tmp1,&matrix_index_r );
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_tmp1,&matrix_index_c );
                            }
                            else{
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_tmp1,&matrix_index_c2);
                                //sparse_matrix[s]->InsertGlobalValues(matrix_index_c2,1,&element_tmp3,&matrix_index_c);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r ,1,&element_tmp1,&matrix_index_c2);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r2,1,&element_tmp1,&matrix_index_c );
                            }
                        }
                    }
                }
            }
        }
    }
    ***********************170822******/

    // end
    return;
}

std::vector< std::vector<double> > TDDFT_ABBA::gather_z_vector(RCP<Epetra_MultiVector> input, std::vector<double> eigvals){
    int num_vec = input -> NumVectors();
    int total_size = input->Map().NumGlobalElements();
    double ** recv = new double*[num_vec];
    for(int i = 0; i < num_vec; ++i){
        recv[i] = new double[total_size];
    }
    Parallel_Util::group_allgather_multivector(input, recv);

    total_size /= 2;
    vector<double> eps_diff(total_size);
    int matrix_index_r = 0;
    for(int i=0; i<index_of_HOMO[0]; ++i){
        for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
            eps_diff[matrix_index_r] = sqrt(eigvals[a] - eigvals[i]);
            ++matrix_index_r;
        }
    }
    for(int j = 0; j < num_vec; ++j){
        for(int i = 0; i < total_size; ++i){
            recv[j][i] += recv[j][i+total_size];
            recv[j][i] /= eps_diff[i];
        }
        double norm = 0.0;
        for(int i = 0; i < total_size; ++i){
            norm += recv[j][i]*recv[j][i];
        }
        for(int i = 0; i < total_size; ++i){
            recv[j][i] /= sqrt(norm);
        }
    }

    vector< vector<double> > output(num_vec);
    for(int i = 0; i < num_vec; ++i){
        output[i] = vector<double>(recv[i], recv[i]+total_size);
    }
    for(int i = 0; i < num_vec; ++i){
        delete[] recv[i];
    }
    delete[] recv;
    return output;
}
