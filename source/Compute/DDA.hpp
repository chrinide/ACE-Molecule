#pragma once
#include "Compute_Interface.hpp"
#include "../Io/Atoms.hpp"
#include <array>
class DDA: public Compute_Interface{
    public:
        /**
         * @brief Default constructor.
         * @param mesh Lagrange basis.
         * @param parameters Parameters (DDA).
         **/
        DDA(Teuchos::RCP<const Basis> mesh, Teuchos::RCP<Teuchos::ParameterList> parameters);
        ~DDA();
        /**
         * @brief Calculate Dipole-Dipole interaction.
         * @param mesh Lagrange basis.
         * @param state State to compute
         **/
        int compute(Teuchos::RCP<const Basis> mesh, Teuchos::Array< Teuchos::RCP<State> >& states);
    protected:
        /**
         * @brief Read pseudo potential file
         * @param atomic_density atomic density of each atom
         * @param atoms collection of atom class
         **/
        int read_atomic_density(Teuchos::RCP<Epetra_MultiVector>& atomic_density, Teuchos::RCP<const Atoms> atoms);
       
        int calculate_volume(Teuchos::RCP<Epetra_MultiVector> density, std::vector<double>& volume, std::vector<double>& coverage);
        int calculate_volume2(Teuchos::RCP<Epetra_MultiVector> density, std::vector<double>& volume, std::vector<double>& coverage);

        int calculate_polarizability(Teuchos::RCP<const Atoms> atoms,
                                     std::vector<double> scale_up,
                                     std::vector<std::vector<std::complex<float> > >& polarizability,
                                     std::vector<double>& wavelength);
        int calculate_polarizability(Teuchos::RCP<const Atoms> atoms,
                                     Teuchos::RCP<const Atoms> ref_atoms,
                                     std::vector<std::vector<std::complex<float> > >& polarizability,
                                     std::vector<double>& wavelength);

        //Do not use iterate and inverse, it is much less efficient thatn linearequation
        int iterate(Teuchos::RCP<const Atoms> atoms,
                std::vector<std::vector<std::complex<float> > > polarizability,
                std::vector<std::vector< std::array< std::complex<float> ,9> > >& alpha_cluster);
        
        int inverse(Teuchos::RCP<const Atoms> atoms,
                std::vector<std::vector<std::complex<float> > > polarizability,
                std::vector<std::vector< std::array< std::complex<float> ,9> > >& alpha_cluster);
        
        int linearequation(Teuchos::RCP<const Atoms> atoms,
                std::vector<std::vector<std::complex<float> > > polarizability,
                std::vector<std::vector< std::array< std::complex<float> ,9> > >& alpha_cluster);
        /**
         * @brief make A (Ax=b)
         * @param atoms collection of atom class
         * @param matrix matrix A
         * @param polarizability atomic polarizability of each atom
         **/
        int make_matrix(Teuchos::RCP<const Atoms> atoms,
                        std::vector<std::complex<float> >& matrix,
                        std::vector<std::vector<std::complex<float> > > polarizability);
       
        /**
         * @brief make T matrix
         * @param atoms collection of atom class
         * @param matrix matrix A
         * @param bool_matrix matrix which indicate the entires to be evaluated
         * @param polarizability atomic polarizability of each atom
         **/
        void get_T_matrix(Teuchos::RCP<const Atoms> atoms,
                          std::vector< std::vector< std::vector<std::complex<float> > > > & T_matrix,
                          std::vector< std::vector< bool > > & bool_T_matrix,
                          std::vector<std::vector<std::complex<float> > >& polarizability,
                          int w,
                          bool periodic = false );
        
        void get_T_matrix(Teuchos::RCP<const Atoms> atoms,
                          std::vector< std::vector< std::vector<std::complex<float> > > > & T_matrix,
                          std::vector< std::vector< bool > > & bool_T_matrix,
                          bool periodic = false);

        void dda_mixing(std::vector< std::array< std::complex<float> ,9> > & after,
                    std::vector< std::array< std::complex<float> ,9> > before);        
        /**
         * @brief write dipole moment of each atom
         * @param atoms collection of atom class
         * @param alpha_cluster atomic polarizability with dipole-dipole interaction
         * @param polarizability atomic polarizability without dipole-dipole interaction
         * @param polarizability atomic polarizability of each atom
         **/
        void write_polarizability(Teuchos::RCP<const Atoms> atoms,
                                  std::vector<std::vector< std::array< std::complex<float> ,9> > > alpha_cluster,
                                  std::vector<std::vector<std::complex<float> > > polarizability);
        /**
         * @brief write dipole moment of each atom
         * @param atoms collection of atom class
         * @param alpha_cluster atomic polarizability with dipole-dipole interaction
         * @param polarizability atomic polarizability without dipole-dipole interaction
         * @param polarizability atomic polarizability of each atom
         **/
        void write_dipole(Teuchos::RCP<const Atoms> atoms,
                                  std::vector<std::vector< std::array< std::complex<float> ,9> > > alpha_cluster,
                                  std::vector<std::vector<std::complex<float> > > polarizability);

        /**
         * @brief calculate coordinate of each atom
         * @param atoms collection of atom class
         * @param CN coordinate number of each atom
         **/
        void calculate_CN(Teuchos::RCP<const Atoms> atoms,
                          std::vector<double>& CN);
        /**
         * @brief calculate damping of each atom
         * @param atoms collection of atom class
         * @param damping damping of each atom
         **/
        void calculate_damping(Teuchos::RCP<const Atoms> atoms,
                          std::vector<double>& damping);
        
        int closest(std::vector<double> vec, double value);

        Teuchos::RCP<const Basis> mesh;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        std::vector<std::vector<double> > energy;
        std::vector<std::vector<double> > wavelength;
        std::vector<std::vector<double> > oscillating_strength;
        std::vector<double> wavelength_range;
        double unit = 0.01;
};
