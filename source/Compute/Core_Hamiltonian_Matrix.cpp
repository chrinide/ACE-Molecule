#include "Core_Hamiltonian_Matrix.hpp"
#include "../Util/Parallel_Manager.hpp"
//#include <chrono>

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;

Core_Hamiltonian_Matrix::Core_Hamiltonian_Matrix(
    RCP<const Basis> mesh,
    RCP<const Atoms> atoms,
    RCP<Teuchos::ParameterList> parameters,
    bool is_epetra/* = true*/ ,
    bool store_crs_matrix /*=false*/
): store_crs_matrix(store_crs_matrix)
{
    Verbose::set_numformat(Verbose::Time);
    this -> mesh = mesh;
    this -> kinetic_matrix = rcp( new Kinetic_Matrix( mesh ) );
    double cutoff = parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<double>("HamiltonianCutoff", 1.0E-7);
    int polarize = parameters -> sublist("BasicInformation").get<int>("Polarize");
    this -> np_matrix = rcp( new Nuclear_Potential_Matrix(atoms, polarize+1, cutoff) );

    this -> nuclear_potential = rcp( new Nuclear_Potential( mesh, atoms, parameters ) );

    this -> nuclear_potential -> initialize_matrix( this -> np_matrix );
    if( is_epetra ){
        this -> set_matrix_structure();
    } else {
        this -> epetra_kinetic_matrix_graph = Teuchos::null;
        this -> epetra_ch_matrix_graph = Teuchos::null;
    }
    int value = epetra_kinetic_matrix_graph->NumMyEntries();
    int max; int min; int sum;
    epetra_kinetic_matrix_graph->Comm().MaxAll(&value, &max, 1 );
    epetra_kinetic_matrix_graph->Comm().MinAll(&value, &min, 1 );
    epetra_kinetic_matrix_graph->Comm().SumAll(&value, &sum, 1 );
    Verbose::single(Verbose::Detail)<< "max/min of epetra_kinetic_matrix_graph: " << max << " / " << min <<"  sum=  "<<sum<<std::endl;

    value = epetra_ch_matrix_graph->NumMyEntries();
    epetra_ch_matrix_graph->Comm().MaxAll(&value, &max, 1 );
    epetra_ch_matrix_graph->Comm().MinAll(&value, &min, 1 );
    epetra_ch_matrix_graph->Comm().SumAll(&value, &sum, 1 );
    Verbose::single(Verbose::Detail)<< "max/min of epetra_ch_matrix_graph: " << max << " / " << min <<"  sum=  "<<sum<<std::endl;
    //exit(-1);
}

RCP<Kinetic_Matrix> Core_Hamiltonian_Matrix::get_kinetic_matrix(){
    return this -> kinetic_matrix;
}

RCP<Nuclear_Potential_Matrix> Core_Hamiltonian_Matrix::get_nuclear_potential_matrix(){
    return this -> np_matrix;
}

int Core_Hamiltonian_Matrix::update(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > orbitals
){
    return this -> nuclear_potential -> set_dynamic_nonlocal_pot_coeff_to_matrix( this -> np_matrix );
}

int Core_Hamiltonian_Matrix::multiply(int ispin, RCP<Epetra_MultiVector> input, RCP<Epetra_MultiVector> &output){
    RCP<Epetra_MultiVector> tmp1 = rcp( new Epetra_MultiVector(input -> Map(), input -> NumVectors() ) );
    RCP<Epetra_MultiVector> tmp2 = rcp( new Epetra_MultiVector(input -> Map(), input -> NumVectors() ) );

    this -> kinetic_matrix -> multiply3( input, tmp1 );
    output -> Update( 1.0, *tmp1, 0.0 );
    this -> np_matrix -> multiply( ispin, input, tmp2 );
    output -> Update( 1.0, *tmp2, 1.0 );
    return 0;
}
int Core_Hamiltonian_Matrix::multiply(int ispin, const Anasazi::MultiVec<double>& X,
        Anasazi::MultiVec<double>& Y, bool is_gpu) const{
    if(is_gpu){

    }
    else{

        RCP<Epetra_MultiVector> tmp_X=rcp(dynamic_cast<Epetra_MultiVector* >(&const_cast<Anasazi::MultiVec<double> &>(X)));
        RCP<Epetra_MultiVector> tmp_Y=rcp(dynamic_cast<Epetra_MultiVector* >(&Y));
        RCP<Epetra_MultiVector> tmp1 = rcp( new Epetra_MultiVector(tmp_X -> Map(), tmp_X -> NumVectors() ) );
        RCP<Epetra_MultiVector> tmp2 = rcp( new Epetra_MultiVector(tmp_Y -> Map(), tmp_X -> NumVectors() ) );

        this -> kinetic_matrix -> multiply( tmp_X, tmp1 );
        this -> np_matrix -> multiply( ispin, tmp_X, tmp2 );
        tmp_Y -> Update( 1.0, *tmp1, 0.0 );
        tmp_Y -> Update( 1.0, *tmp2, 1.0 );
    }
    return 0;
}

int Core_Hamiltonian_Matrix::add(int ispin, RCP<Epetra_CrsMatrix> matrix, RCP<Epetra_CrsMatrix> &output, double scalar){
    output = rcp( new Epetra_CrsMatrix(*matrix) );
    output = matrix;
    return this -> add(ispin, output, scalar);
}

int Core_Hamiltonian_Matrix::add(int ispin, RCP<Epetra_CrsMatrix> &matrix, double scalar){
    Verbose::set_numformat(Verbose::Time);
    int ierr;
    //clock_t t1, t2, t3, t4;
    //Parallel_Manager::info().all_barrier();
    //t1 = clock();

    Teuchos::RCP<Time_Measure> timer  = Teuchos::rcp(new Time_Measure() );
    timer->start("kinetic");

    ierr = this -> kinetic_matrix -> add( matrix, scalar );
    timer->end("kinetic");
    Verbose::single(Verbose::Detail) << "kinetic" << timer->get_elapsed_time("kinetic",-1) <<" s" << std::endl;
    //Parallel_Manager::info().all_barrier();
    //t2 = clock();
    timer->start("np");
    ierr += this -> np_matrix -> add( ispin, matrix, scalar );
    timer->end("np");
    Verbose::single(Verbose::Detail) << "np" << timer->get_elapsed_time("np",-1) <<" s" << std::endl;
    // Extreamly slow, even if FillComplete == true
    // 200s for spacing 0.25 Fe box 12 bohr sphere with PAW
    //Parallel_Manager::info().all_barrier();
    //t3 = clock();
    //matrix -> FillComplete();
    //t4 = clock();

    //Verbose::single(Verbose::Detail) << "Core_Hamiltonian_Matrix add kinetic add: \t" << ( (double)(t2-t1) )/CLOCKS_PER_SEC << "s" << std::endl;
    //Verbose::single(Verbose::Detail) << "Core_Hamiltonian_Matrix add npm add: \t" << ( (double)(t3-t2) )/CLOCKS_PER_SEC << "s" << std::endl;
    //Verbose::single(Verbose::Detail) << "Core_Hamiltonian_Matrix add FillComplete: \t" << ( (double)(t4-t3) )/CLOCKS_PER_SEC << "s" << std::endl;
    return ierr;
}


int Core_Hamiltonian_Matrix::set_matrix_structure(){
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Detail) << "Core_Hamiltonian_Matrix kinetic and core hamiltonian matrix Epetra structure calculation" << std::endl;

    timer-> start("Core_Hamiltonian_Matrix::set_matrix_structure Copy");
    RCP<Epetra_CrsMatrix> kinetic = rcp( new Epetra_CrsMatrix(Copy, *this -> mesh -> get_map(), 0) );
    //kinetic->FillComplete();
    this -> kinetic_matrix -> add(kinetic, 1.0);
    timer-> end("Core_Hamiltonian_Matrix::set_matrix_structure Copy");
    Verbose::single(Verbose::Detail) << "Core_Hamiltonian_Matrix set_matrix_structure::set_matrix_structure Copy: \t" << timer->get_elapsed_time("Core_Hamiltonian_Matrix::set_matrix_structure Copy",-1) << "s" << std::endl;

    timer-> start("Core_Hamiltonian_Matrix::FillComplete for kinetic graph");
    kinetic -> FillComplete();

    int value = kinetic->NumMyNonzeros();
    int max; int min; int sum;
    kinetic->Comm().MaxAll(&value, &max, 1 );
    kinetic->Comm().MinAll(&value, &min, 1 );
    kinetic->Comm().SumAll(&value, &sum, 1 );
    Verbose::single(Verbose::Detail)<< "epetra_kinetic_matrix_graph: " << max << " / " << min <<" =  "<<sum<<std::endl;

    //exit(-1);//shchoi 16.10.15
    this -> epetra_kinetic_matrix_graph = rcp( new Epetra_CrsGraph(kinetic -> Graph()) );
    this -> epetra_kinetic_matrix_graph -> OptimizeStorage();
    Parallel_Manager::info().all_barrier();
    timer-> end("Core_Hamiltonian_Matrix::FillComplete for kinetic graph");

    Verbose::single(Verbose::Detail) << "Core_Hamiltonian_Matrix set_matrix_structure:: FillComplete for kinetic graph: \t" << timer->get_elapsed_time("Core_Hamiltonian_Matrix::FillComplete for kinetic graph",-1) << "s" << std::endl;

    timer-> start("Core_Hamiltonian_Matrix::core H matrix FillComplete");
    this -> np_matrix -> get_epetra_structure( kinetic, this -> epetra_ch_matrix_graph );
    this -> epetra_ch_matrix_graph -> OptimizeStorage();
    kinetic = Teuchos::null;
    timer-> end("Core_Hamiltonian_Matrix::core H matrix FillComplete");

    Verbose::single(Verbose::Detail) << "Core_Hamiltonian_Matrix set_matrix_structure:: core H matrix FillComplete: \t" << timer->get_elapsed_time("Core_Hamiltonian_Matrix::core H matrix FillComplete",-1) << "s" << std::endl;

    return 0;
}

int Core_Hamiltonian_Matrix::get_kinetic_matrix(RCP<Epetra_CrsMatrix> &matrix){
    if( this -> epetra_kinetic_matrix_graph == Teuchos::null ){
        this -> set_matrix_structure();
    }
    matrix = rcp( new Epetra_CrsMatrix(Copy, *epetra_kinetic_matrix_graph) );
    int ierr =  this -> kinetic_matrix -> add(matrix, 1.0);
    matrix -> FillComplete();
    return ierr;
}

int Core_Hamiltonian_Matrix::get_nuclear_potential_matrix(int ispin, RCP<Epetra_CrsMatrix> &matrix){
    if( this -> epetra_ch_matrix_graph == Teuchos::null ){
        this -> set_matrix_structure();
    }
    matrix = rcp( new Epetra_CrsMatrix(Copy, *epetra_ch_matrix_graph) );
    int ierr =  this -> np_matrix -> add(ispin, matrix, 1.0);
    matrix -> FillComplete();
    return ierr;
}

int Core_Hamiltonian_Matrix::get(int ispin, RCP<Epetra_CrsMatrix> &matrix){

    if (store_crs_matrix and crs_core_hamiltonian!=Teuchos::null){
        matrix= rcp( new Epetra_CrsMatrix(*crs_core_hamiltonian));
        return 0;
    }

    if( this -> epetra_ch_matrix_graph == Teuchos::null ){
        this -> set_matrix_structure();
    }
    Teuchos::RCP<Time_Measure> timer  = Teuchos::rcp(new Time_Measure() );
    timer->start("Core_Hamiltonian_Matrix::get::construct CrsMatrix with graph");
    matrix = rcp( new Epetra_CrsMatrix(Copy, *epetra_ch_matrix_graph ) );
    timer->end("Core_Hamiltonian_Matrix::get::construct CrsMatrix with graph");
    timer->start("Core_Hamiltonian_Matrix::get::add");
    int ierr =  this -> add(ispin, matrix, 1.0);
    timer->end("Core_Hamiltonian_Matrix::get::add");
    timer->start("Core_Hamiltonian_Matrix::get::FillComplete");
    matrix -> FillComplete();
    timer->end("Core_Hamiltonian_Matrix::get::FillComplete");
    timer->print(Verbose::single(Verbose::Normal));
    if (store_crs_matrix ){
        crs_core_hamiltonian = rcp(new Epetra_CrsMatrix(*matrix));
    }
    //Parallel_Manager::info().all_barrier();
    //Verbose::single(Verbose::Detail)<< "Number of non-zero values" <<std::endl;
    //Verbose::single(Verbose::Detail)<< "Proc. ID / Number of non-zero values" <<std::endl;
    //Verbose::all() << Parallel_Manager::info().get_total_mpi_rank() << "\t" << matrix->NumMyNonzeros() <<std::endl;
    return ierr;
}

RCP<const Basis> Core_Hamiltonian_Matrix::get_mesh(){
    return this -> mesh;
}
