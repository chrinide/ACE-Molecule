#pragma once
#include <time.h>
#include "Guess.hpp"
#include "../Basis/Basis.hpp"
#include "../Io/Atoms.hpp"
#include "Epetra_MultiVector.h"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../State/State.hpp"

class Atomic_Density: public Guess{
    public:
    Atomic_Density( Teuchos::Array< Teuchos::RCP<Occupation> > occupations, Teuchos::RCP<const Atoms> atoms, Teuchos::Array<std::string> upf_filenames );
    int compute(Teuchos::RCP<const Basis> mesh, Teuchos::Array< Teuchos::RCP<State> >& states);

    protected:
    void initialize();

    Teuchos::Array<std::string> upf_filenames;
    std::string upf_filename;
    std::vector<double> radial_mesh;
    std::vector<double> rho;

    Teuchos::RCP<Epetra_Vector> atomic_hartree;
};

