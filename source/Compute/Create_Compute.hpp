#pragma once
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

#include "Poisson.hpp"
#include "Core_Hamiltonian.hpp"
#include "../Basis/Basis.hpp"
#include "../Io/Atoms.hpp"
#include "Exchange_Correlation.hpp"

/**
 * @brief Create Compute-like objects
 */
namespace Create_Compute{
    Teuchos::RCP<Poisson_Solver> Create_Poisson(
        Teuchos::RCP<const Basis> mesh,
        Teuchos::RCP<Teuchos::ParameterList> parameters
    );
    /*
    Teuchos::RCP<Guess> Create_Guess(
        Teuchos::RCP<const Atoms> atoms, 
        Teuchos::RCP<Teuchos::ParameterList> parameters, 
        Teuchos::RCP<const Basis> mesh = Teuchos::null
    );
    */
    Teuchos::RCP<Core_Hamiltonian> Create_Core_Hamiltonian(
        Teuchos::RCP<const Basis> mesh,
        Teuchos::RCP<const Atoms> atoms,
        Teuchos::RCP<Teuchos::ParameterList> parameters
    );
/**
 * @brief Create exchange-correlation class for DFT/TDDFT calculation
 * @details This is wrapper method for creating Exchange_Correlation class. The inputs are processed in here and classified which class should be used. There are five types of functionals
 * - 0: XCFunctional for Libxc
 * - 1: X & CFunctionals for Libxc
 * - 2: KLI
 * - 3: Hybrid (of Libxc & KLI)
 * - 4: HF
 * The variable "functional_type" indicates each type of functional.
 * @return RCP<Exchange_Correlation> : Corresponding object which inherits Exchange_Correlation class.
 * @todo If C++ support is enabled or we can use regex, we should change number/string identification using regex "^(\\-|\\+)?([0-9]+)?".
 */
    Teuchos::RCP<Exchange_Correlation> Create_Exchange_Correlation(
        Teuchos::RCP<const Basis> mesh, ///< Basis data
        Teuchos::RCP<Teuchos::ParameterList> parameters, ///< parameter data
        Teuchos::RCP<Poisson_Solver> poisson ///< Poisson class for KLI, Hybrid, and HF
    );
};

