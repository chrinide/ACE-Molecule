#include "Exchange_Correlation.hpp"
#include <set>

#include "../State/XC_State.hpp"
#include "XC_LC_wPBE_NGau.hpp"

#include "../Util/Value_Coef.hpp"
#include "../Util/Parallel_Util.hpp"
#include "../Util/Lagrange_Derivatives.hpp"
#include "../Util/Density_Orbital_Derivatives.hpp"
#include "../Util/String_Util.hpp"
#include "../Core/Occupation/Occupation_From_Input.hpp"

using std::setw;
using std::string;
using std::iter_swap;
using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

Exchange_Correlation::Exchange_Correlation(
    RCP<const Basis> mesh,
    Array< RCP<Compute_XC> > xc_classes,
    std::vector<double> xc_scale,
    bool is_grad_from_orbital/* = true*/
): mesh(mesh), is_grad_from_orbital(is_grad_from_orbital){
    this->internal_timer = Teuchos::rcp(new Time_Measure());
    //end of sort
    this -> xc_list = xc_classes;
    this -> xc_function_id.clear();
    this -> xc_portion = xc_scale;
    //create xc_list
    this -> EXX_functional_exist = false;
    for(int i = 0; i < xc_scale.size(); ++i){
        this -> xc_function_id.push_back(xc_classes[i] -> get_functional_id());
    }
    for(int i=0;i<xc_function_id.size(); i++){
        this -> xc_data.append(rcp(new XC_State(xc_function_id[i])));
    }

    double exx_portion = 0.0;
    for(int i = 0; i < xc_function_id.size(); ++i){
        if(xc_function_id[i] < 0){
            exx_portion = xc_portion[i];
            this -> EXX_functional_exist = true;
            this -> poisson_solver = this -> xc_list[i] -> get_rsh_poisson_solver();
            break;
        }
    }

    xc_name.clear();
    xc_kind.clear();
    for(int i = 0; i < this -> xc_list.size(); ++i){
        xc_name.push_back(this -> xc_list[i] -> get_info_string(false));
        xc_kind.push_back(this -> xc_list[i] -> get_functional_kind());
    }
    /*
    if(omega > 0.0){
        this -> xc_data[0] -> set_lrc_info(1-global_x_portion, omega);
    }
    */
    print_xc_info();
}

Exchange_Correlation::~Exchange_Correlation(){
    Lagrange_Derivatives::free(mesh->get_points());
    Verbose::set_numformat(Verbose::Time);
    this -> internal_timer -> print(Verbose::single(Verbose::Normal));
}

void Exchange_Correlation::update_xc_info(RCP<State> state, bool force_polarized/* = false*/){
    this -> internal_timer -> start("XC:: update XC info");
    std::map<string, bool> total_necessities = this -> xc_data[0] -> get_necessary_fields();
    vector<string> field_names;
    for(std::map<string, bool>::iterator it = total_necessities.begin(); it != total_necessities.end(); ++it){
        field_names.push_back(it->first);
    }
    for(int i = 1; i < this -> xc_data.size(); ++i){
        std::map<string, bool> current_field_info = this -> xc_data[i] -> get_necessary_fields();
        for(int j = 0; j < field_names.size(); ++j){
            total_necessities[field_names[j]] |= current_field_info[field_names[j]];
        }
    }

    for(int j = 0; j < field_names.size(); ++j){
        if(total_necessities[field_names[j]]){
            Verbose::single(Verbose::Detail) << field_names[j] << " is required" << std::endl;
        } else {
            Verbose::single(Verbose::Detail) << field_names[j] << " is not required" << std::endl;
        }
    }

    Array< RCP<Epetra_Vector> > all_density;
    Array< RCP<Epetra_Vector> > val_density;
    Array< RCP<Epetra_MultiVector> > density_grad;
    Array< RCP<Epetra_Vector> > density_laplacian;
    Array< RCP<Epetra_Vector> > kinetic_energy_density;
    Array< RCP<Epetra_MultiVector> > orbitals;
    Array< RCP<Occupation> > occupations;

    if(total_necessities["all_density"]){
        for(int s = 0; s < state -> get_density().size(); ++s){
            RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*mesh -> get_map()));
            tmp -> Update(1.0, *state -> get_density()[s], 0.0);
            tmp -> Update(1.0, *state -> get_core_density()[s], 1.0);
            all_density.append(tmp);
        }
    }
    if(total_necessities["all_density"] or total_necessities["val_density"]){
        for(int s = 0; s < state -> get_density().size(); ++s){
            RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*mesh -> get_map()));
            tmp -> Update(1.0, *state -> get_density()[s], 0.0);
            val_density.append(tmp);
        }
    }
    //if(total_necessities["orbitals"]){
        orbitals = state -> orbitals;
        occupations = state -> occupations;
    //}

    bool is_same_state = false;
    for(int i = 0; i < this -> xc_data.size(); ++i){
        if(this -> xc_data[i] -> val_density.size() == 0){
            is_same_state = false;
            break;
        }
    }
    if(this -> xc_data[0] -> val_density.size() != 0){
        bool is_same_density = true;
        for(int s = 0; s < val_density.size(); ++s){
            RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*val_density[s]));
            tmp -> Update(-1.0, *this->xc_data[0]->val_density[s], 1.0);
            double norm2;
            tmp -> Norm2(&norm2);
            if(norm2 > 1.0E-30){
                is_same_density = false;
            }
        }
        if(is_same_density){
            is_same_state = (total_necessities["density_gradient"] and this -> xc_data[0] -> density_grad.size() != 0)
                                or (total_necessities["density_laplacian"] and this -> xc_data[0] -> density_laplacian.size() != 0);
            density_grad = this -> xc_data[0] -> density_grad;
            density_laplacian = this -> xc_data[0] -> density_laplacian;
        }
    }

    if(total_necessities["density_gradient"] and density_grad.size() == 0){
        this -> internal_timer -> start("XC::update XC info::density gradient");
        int spin_size = state -> get_density().size();
        for(int s = 0; s < spin_size; ++s){
            RCP<Epetra_MultiVector> tmp = rcp(new Epetra_MultiVector(*mesh -> get_map(), 3));

            if(this -> is_grad_from_orbital and state -> get_orbitals().size() != 0){
                Density_Orbital_Derivatives::density_gradient_from_orbital(mesh, occupations[s], orbitals[s], false, tmp);
            } else {
                Array< RCP<Epetra_MultiVector> > val_rho_grad;// Typecasting problem.
                for(int d = 0; d < 3; ++d){
                    val_rho_grad.append( rcp(new Epetra_MultiVector(*mesh -> get_map(), 1)) );
                }
                Lagrange_Derivatives::gradient(mesh, state -> get_density()[s], val_rho_grad, true);
                for(int d = 0; d < 3; ++d){
                    tmp -> operator()(d) -> Update(1.0, *val_rho_grad[d] -> operator()(0), 1.0);
                }
            }

            tmp -> Update(1.0, *state -> get_core_density_grad()[s], 1.0);
            density_grad.append(tmp);
        }
        this -> internal_timer -> end("XC::update XC info::density gradient");
    }
    if(total_necessities["density_laplacian"] and density_laplacian.size() == 0){
        this -> internal_timer -> start("XC::update XC info::density laplacian");
        int spin_size = state -> get_density().size();
        for(int s = 0; s < spin_size; ++s){
            RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*mesh -> get_map()));
            Lagrange_Derivatives::laplacian(mesh, state -> density[s], tmp, true);

            RCP<Epetra_Vector> core_rho = state -> get_core_density()[s];
            RCP<Epetra_Vector> core_rho_lapl = rcp(new Epetra_Vector(*mesh -> get_map()));
            Lagrange_Derivatives::laplacian(mesh, core_rho, core_rho_lapl, true);
            tmp -> Update(1.0, *core_rho_lapl, 1.0);
            density_laplacian.append(tmp);
        }
        this -> internal_timer -> end("XC::update XC info::density laplacian");
    }
    if(total_necessities["kinetic_energy_density"]){
        std::cout << "NOTE: kinetic energy density equation with nonlinear core correction is not solid." << std::endl;
        std::cout << "One should know that how the core kinetic energy density is treated!" << std::endl;
        this -> internal_timer -> start("XC::update XC info::KE density");
        int spin_size = state -> get_density().size();
        for(int s = 0; s < spin_size; ++s){
            RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*mesh -> get_map()));
            Density_Orbital_Derivatives::kinetic_energy_density(mesh, occupations[s], orbitals[s], false, tmp);
            //tmp -> Update(1.0, *core_KE_density, 1.0);
            kinetic_energy_density.append(tmp);
        this -> internal_timer -> end("XC::update XC info::KE density");
        }
    }
    if(force_polarized){
        if(all_density.size() == 1){
            RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*all_density[0]));
            tmp -> Scale(0.5);
            all_density[0] = tmp;
            all_density.append(rcp(new Epetra_Vector(*tmp)));
        }
        if(val_density.size() == 1){
            RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*val_density[0]));
            tmp -> Scale(0.5);
            val_density[0] = tmp;
            val_density.append(rcp(new Epetra_Vector(*tmp)));
        }
        if(orbitals.size() == 1){
            RCP<Epetra_MultiVector> tmp = rcp(new Epetra_MultiVector(*orbitals[0]));
            tmp -> Scale(0.5);
            orbitals[0] = tmp;
            orbitals.append(rcp(new Epetra_MultiVector(*tmp)));
        }
        if(occupations.size() == 1){
            std::vector<double> occ = occupations[0] -> get_occupation_list();
            for(int j = 0; j < occ.size(); ++j){
                occ[j] *= 0.5;
            }
            RCP<Occupation> tmp = rcp(new Occupation_From_Input(occ, occupations[0] -> get_eigenvalues()));
            //RCP<Occupation> tmp = rcp(new Occupation_From_Input(occupations[0] -> get_size(), occ, occupations[0] -> get_eigenvalues()));
            occupations[0] = tmp;
            occupations.append(rcp(tmp->clone()));
        }
        if(density_grad.size() == 1){
            RCP<Epetra_MultiVector> tmp = rcp(new Epetra_MultiVector(*density_grad[0]));
            tmp -> Scale(0.5);
            density_grad[0] = tmp;
            density_grad.append(rcp(new Epetra_MultiVector(*tmp)));
        }
        if(density_laplacian.size() == 1){
            RCP<Epetra_Vector> tmp = rcp(new Epetra_Vector(*density_laplacian[0]));
            tmp -> Scale(0.5);
            density_laplacian[0] = tmp;
            density_laplacian.append(rcp(new Epetra_Vector(*tmp)));
        }
        if(kinetic_energy_density.size() == 1){
            // I do not know how this will be implemented.
        }
    }
    for(int i = 0; i < this -> xc_data.size(); ++i){
        this -> xc_data[i] -> val_density = val_density;
        this -> xc_data[i] -> density = val_density;
        this -> xc_data[i] -> all_density = all_density;
        this -> xc_data[i] -> density_grad = density_grad;
        this -> xc_data[i] -> density_laplacian = density_laplacian;
        this -> xc_data[i] -> kinetic_energy_density = kinetic_energy_density;
        this -> xc_data[i] -> orbitals = orbitals;
        this -> xc_data[i] -> occupations = occupations;
        this -> xc_data[i] -> spin_size = occupations.size();
        if(!is_same_state){
            Verbose::single(Verbose::Detail) << "XC::update_xc_info clear previous calculations" << std::endl;
            Array< RCP<Epetra_Vector> > empty;
            this -> xc_data[i] -> set_energy(0.0);
            this -> xc_data[i] -> set_potential(empty);
            this -> xc_data[i] -> set_energy_density(Teuchos::null);
            this -> xc_data[i] -> set_lda_kernel(empty);
            this -> xc_data[i] -> set_gga_kernel(empty, empty, empty, empty);
        }
    }
    this -> internal_timer -> end("XC:: update XC info");
    Verbose::single(Verbose::Normal) << "#-------------------------------------- Exchange_Correlation::update_xc_info\n";
    Verbose::single(Verbose::Normal) << " Time to initialize XC data container: " << this -> internal_timer -> get_elapsed_time("XC:: update XC info", -1) << " s\n";
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
}


int Exchange_Correlation::compute(RCP<const Basis> mesh, Array<RCP<State> >& states){
    if(states.size()==0){
        Verbose::all() << "No given density!\n";
        exit(EXIT_FAILURE);
    }
    states.append(rcp(new State(*states[states.size()-1])));

    this->mesh = mesh;

    auto state = states[states.size()-1];
    auto map = mesh->get_map();

    Array< RCP<Epetra_Vector> > exchange_potential;
    Array< RCP<Epetra_Vector> > correlation_potential;
    double exchange_energy = 0.0, correlation_energy = 0.0;

    for(int i_spin=0; i_spin<state->occupations.size(); i_spin++){
        exchange_potential.push_back(rcp(new Epetra_Vector(*map)));
        correlation_potential.push_back(rcp(new Epetra_Vector(*map)));
    }

    compute_vxc(state, exchange_potential, correlation_potential);
    compute_Exc(state, exchange_energy, correlation_energy);

    for(int i_spin=0; i_spin<exchange_potential.size(); i_spin++){
        state->local_potential[i_spin]->Update(1.0,*exchange_potential[i_spin],1.0);
        state->local_potential[i_spin]->Update(1.0,*correlation_potential[i_spin],1.0);
    }
    state->total_energy += exchange_energy + correlation_energy;

    return 0;
}
int Exchange_Correlation::compute(RCP<const Basis> mesh, Array<RCP<Scf_State> >& states){

    if(states.size()==0){
        Verbose::all() << "No given density!\n";
        exit(EXIT_FAILURE);
    }
    states.append(rcp(new Scf_State(*states[states.size()-1])));

    this->mesh = mesh;

    auto state = states[states.size()-1];
    auto map = mesh->get_map();

    Array< RCP<Epetra_Vector> > exchange_potential;
    Array< RCP<Epetra_Vector> > correlation_potential;
    double exchange_energy = 0.0, correlation_energy = 0.0;

    for(int i_spin=0; i_spin<state->occupations.size(); i_spin++){
        exchange_potential.push_back(rcp(new Epetra_Vector(*map)));
        correlation_potential.push_back(rcp(new Epetra_Vector(*map)));
    }
    RCP<State> new_state = Teuchos::rcp_implicit_cast<State>(state);

    this -> compute_vxc(new_state, exchange_potential, correlation_potential);
    this -> compute_Exc(new_state, exchange_energy, correlation_energy);

    state->x_potential = exchange_potential;
    state->c_potential = correlation_potential;
    state->x_energy = exchange_energy;
    state->c_energy = correlation_energy;

    return 0;
}

// This routine CANNOT compute KLI exchange potential.
void Exchange_Correlation::compute_Exc(RCP<State> &state, double &x_energy, double &c_energy){
    this -> internal_timer -> start("XC::XC energy calculation");
    this -> update_xc_info(state);

    double Ex = 0.0, Ec = 0.0;
    for(int i = 0; i < this -> xc_data.size(); ++i){
        this -> xc_list[i] -> compute_Exc(this -> xc_data[i]);

        int kind = this -> xc_kind[i];
        if( kind == 0 or kind == 2 ){
            Ex += this -> xc_data[i] -> get_energy() * this -> xc_portion[i];
        } else if (kind == 1) {
            Ec += this -> xc_data[i] -> get_energy() * this -> xc_portion[i];
        }
    }

    x_energy = Ex;
    c_energy = Ec;

    this -> internal_timer -> end("XC::XC energy calculation");

    Verbose::single(Verbose::Normal) << "\n#----------------------------------------------------- Exchange_Correlation::compute_Exc\n";
    Verbose::single(Verbose::Normal) << " Time to get xc energy: " << this -> internal_timer -> get_elapsed_time("XC::XC energy calculation") << " s\n";
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
    return;
}

void Exchange_Correlation::integrate_rho_vxc(RCP<State> &state, double &int_n_vxc){
    double scaling = mesh->get_scaling()[0] * mesh->get_scaling()[1] * mesh->get_scaling()[2];
    if(this -> xc_data[0] -> get_potential().size() == 0){
        this -> update_xc_info(state);
        for(int i = 0; i < this -> xc_data.size(); ++i){
            this -> xc_list[i] -> compute_vxc(this -> xc_data[i]);
        }
    }
    double n_vxc = 0.0;
    for(int i = 0; i < this -> xc_data.size(); ++i){
        Array< RCP<Epetra_Vector> > potential = this -> xc_data[i] -> potential;
        for(int s = 0; s < potential.size(); ++s){
            double tmp_n_vxc;
            potential[s] -> Dot(*state -> get_density()[s], &tmp_n_vxc);
            n_vxc += scaling * tmp_n_vxc;
            potential[s] -> Dot(*state -> get_core_density()[s], &tmp_n_vxc);
            n_vxc += scaling * tmp_n_vxc;
        }
    }

    int_n_vxc = n_vxc;
    return;
}

// This routine CANNOT compute KLI exchange potential.
void Exchange_Correlation::compute_vxc(
        RCP<State> &state,
        Array< RCP<Epetra_Vector> >& x_potential,
        Array< RCP<Epetra_Vector> >& c_potential)
{
    this -> internal_timer -> start("XC::XC potential calculation");
    this -> internal_timer -> start("XC::XC potential calculation4");
    this -> update_xc_info(state);
    this -> internal_timer -> end("XC::XC potential calculation4");
    this -> internal_timer -> start("XC::XC potential calculation3");
    for(int i = 0; i < this -> xc_data.size(); ++i){
        this -> xc_list[i] -> compute_vxc(this -> xc_data[i]);
    }

    this -> internal_timer -> end("XC::XC potential calculation3");
    this -> internal_timer -> start("XC::XC potential calculation2");
    x_potential.clear();
    c_potential.clear();
    for(int s = 0; s < state -> occupations.size(); ++s){
        x_potential.append(rcp(new Epetra_Vector(*this->mesh->get_map())));
        c_potential.append(rcp(new Epetra_Vector(*this->mesh->get_map())));
    }

    this -> internal_timer -> end("XC::XC potential calculation2");
    this -> internal_timer -> start("XC::XC potential calculation1");
    for(int i=0;i<xc_function_id.size();i++){
        Array< RCP<Epetra_Vector> > potential = this -> xc_data[i] -> potential;
        int kind = this -> xc_kind[i];
        if( kind == 0 or kind == 2 ){
            for(int i_spin=0; i_spin<state -> density.size(); ++i_spin){
                x_potential[i_spin]->Update(xc_portion[i], *potential[i_spin], 1.0);
            }
        } else if (kind == 1){
            for(int i_spin=0; i_spin<state -> density.size(); ++i_spin){
                c_potential[i_spin]->Update(xc_portion[i], *potential[i_spin], 1.0);
            }
        }
    }

    this -> internal_timer -> end("XC::XC potential calculation1");
    this -> internal_timer -> end("XC::XC potential calculation");
    Verbose::single(Verbose::Normal) << "#----------------------------------------------------- Exchange_Correlation::compute_vxc\n";
    Verbose::single(Verbose::Normal) << " Time to get xc potential: " << this -> internal_timer -> get_elapsed_time("XC::XC potential calculation") << " s\n";
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

    return;
}

void Exchange_Correlation::compute_fxc(RCP<State> state){
    this -> update_xc_info(state);
    int spin_size = this -> xc_data[0] -> spin_size*2-1;

    fxc.clear();
    vsigma.clear();
    v2rho2.clear(); v2rhosigma.clear(); v2sigma2.clear();
    for(int s = 0; s < spin_size; ++s){
        fxc.append(rcp(new Epetra_Vector(*this -> mesh ->get_map())));
        vsigma.append(rcp(new Epetra_Vector(*this -> mesh ->get_map())));
        v2rho2.append(rcp(new Epetra_Vector(*this -> mesh ->get_map())));
        v2rhosigma.append(rcp(new Epetra_Vector(*this -> mesh ->get_map())));
        v2sigma2.append(rcp(new Epetra_Vector(*this -> mesh ->get_map())));
    }

    Verbose::single(Verbose::Simple) << "\n#------------------------------------------------- Exchange_Correlation::compute_fxc\n";
    for(int i=0; i<xc_function_id.size(); ++i){
        if(xc_data[i] -> get_xc_id() > 0){
            if(xc_data[i] -> get_xc_id() > 10000){
                Verbose::single(Verbose::Normal) << " Kernel #" << i+1 << ": XC CUSTOM (" << xc_data[i] -> get_xc_id() << ", portion = " << xc_portion[i] << ")\n";
            } else {
                Verbose::single(Verbose::Normal) << " Kernel #" << i+1 << ": LIBXC (" << xc_data[i] -> get_xc_id() << ", portion = " << xc_portion[i] << ")\n";
            }
            xc_list[i]->compute_kernel(this -> xc_data[i]);

            Array< RCP<Epetra_Vector> > ith_f;
            Array< RCP<Epetra_Vector> > ith_vsigma;
            Array< RCP<Epetra_Vector> > ith_v2rho2;
            Array< RCP<Epetra_Vector> > ith_v2rhosigma;
            Array< RCP<Epetra_Vector> > ith_v2sigma2;

            this -> xc_data[i]->get_lda_kernel(ith_f);
            this -> xc_data[i]->get_gga_kernel(ith_vsigma, ith_v2rho2, ith_v2rhosigma, ith_v2sigma2);

            for(int s = 0; s < spin_size; ++s){
                if(ith_f.size() > 0){
                    fxc[s]->Update(xc_portion[i], *ith_f[s], 1.0);
                }
                if(ith_vsigma.size() > 0){
                    vsigma[s]->Update(xc_portion[i], *ith_vsigma[s], 1.0);
                    v2rho2[s]->Update(xc_portion[i], *ith_v2rho2[s], 1.0);
                    v2rhosigma[s]->Update(xc_portion[i], *ith_v2rhosigma[s], 1.0);
                    v2sigma2[s]->Update(xc_portion[i], *ith_v2sigma2[s], 1.0);
                }
            }
        }
        else{
            Verbose::single(Verbose::Normal) << " Kernel #" << i+1 << ": Exact exchange (" << xc_data[i] -> get_xc_id() << ", portion = " << xc_portion[i] << ")\n";
            xc_list[i]->compute_kernel(this -> xc_data[i]);
        }
    }
    return;
}

void Exchange_Correlation::print_xc_info(){
    Verbose::single(Verbose::Simple) << "= XC Functional Information ========================\n";
//    Verbose::single().precision(4);
    Verbose::single() << std::fixed;
    for(int i = 0; i < xc_function_id.size(); i++){
        int type = this -> xc_kind[i];
        switch(type){
            case 0:
                Verbose::single(Verbose::Simple) << "Exchange             : ";
                break;
            case 1:
                Verbose::single(Verbose::Simple) << "Correlation          : ";
                break;
            case 2:
                Verbose::single(Verbose::Simple) << "Exchange+Correlation : ";
                break;
            case 3:
                Verbose::single(Verbose::Simple) << "Kinetic              : ";
                break;
        }
        Verbose::single(Verbose::Simple) << setw(6) << xc_function_id[i] << "    ";
        Verbose::single(Verbose::Simple) << "x" << setw(5) << xc_portion[i] << "\n";
    }
    Verbose::single(Verbose::Simple) << "====================================================\n";
    for(int i = 0; i < xc_function_id.size(); ++i){
        Verbose::single(Verbose::Simple) << xc_function_id[i] << ": " << this -> xc_list[i] -> get_info_string();
        Verbose::single(Verbose::Simple) << "====================================================\n";
    }
    return;
}

double Exchange_Correlation::ia_fxc_jb(
        RCP<Epetra_MultiVector> orbitals,
        RCP<Epetra_Vector> ia_coeff, RCP<Epetra_Vector> jb_coeff,
        RCP<Epetra_MultiVector> grad_ia, RCP<Epetra_MultiVector> grad_jb,
        RCP<Epetra_Vector> grad_rho_dot_grad_ia, RCP<Epetra_Vector> grad_rho_dot_grad_jb,
        int i, int a, int b, int j, int ia_spin, int jb_spin, double total_PGG
){
    return this -> ia_fxc_jb_decoupled(orbitals, ia_coeff, jb_coeff, grad_ia, grad_jb,
                                       grad_rho_dot_grad_ia, grad_rho_dot_grad_jb,
                                       i, a, b, j, total_PGG)[0];
}


vector<double> Exchange_Correlation::ia_fxc_jb_decoupled(
        RCP<Epetra_MultiVector> orbitals,
        RCP<Epetra_Vector> ia_coeff, RCP<Epetra_Vector> jb_coeff,
        RCP<Epetra_MultiVector> grad_ia, RCP<Epetra_MultiVector> grad_jb,
        RCP<Epetra_Vector> grad_rho_dot_grad_ia, RCP<Epetra_Vector> grad_rho_dot_grad_jb,
        int i, int a, int b, int j, double total_PGG
){
    vector<double> normval =
            this -> ia_fxc_jb(Array< RCP<Epetra_MultiVector> >(1, orbitals),
                    Array< RCP<Epetra_Vector> >(1, ia_coeff),
                    Array< RCP<Epetra_Vector> >(1, jb_coeff),
                    Array< RCP<Epetra_MultiVector> >(1, grad_ia),
                    Array< RCP<Epetra_MultiVector> >(1, grad_jb),
                    Array< RCP<Epetra_Vector> >(1, grad_rho_dot_grad_ia),
                    Array< RCP<Epetra_Vector> >(1, grad_rho_dot_grad_jb),
                    i, a, b, j, total_PGG);
    vector<double> retval(2);
    retval[0] = 0.5*(normval[0]+normval[1]);
    retval[1] = 0.5*(normval[0]-normval[1]);
    return retval;
}

vector<double> Exchange_Correlation::ia_fxc_jb(
        Array< RCP<Epetra_MultiVector> > orbitals,
        Array< RCP<Epetra_Vector> > ia_coeff, Array< RCP<Epetra_Vector> > jb_coeff,
        Array< RCP<Epetra_MultiVector> > grad_ia, Array< RCP<Epetra_MultiVector> > grad_jb,
        Array< RCP<Epetra_Vector> > grad_rho_dot_grad_ia, Array< RCP<Epetra_Vector> > grad_rho_dot_grad_jb,
        int i, int a, int b, int j, double total_PGG
){
    //RCP<Time> FTime = TimeMonitor::getNewCounter("Hybrid::integrate_TDDFT");
    //FTime->start();

    int retval_size = (orbitals.size() == 1)? 2: 3;
    vector<double> retval(retval_size);

    auto map = mesh->get_map();
    int NumMyElements = map->NumMyElements();

    double tmp = 0.0, total_tmp = 0.0;

    // LDA contribution
    if(fxc.size() == 1){
        tmp = 0.0, total_tmp = 0.0;

        RCP<Epetra_Vector> iajb = rcp(new Epetra_Vector(*map, false));
        iajb -> Multiply(1.0, *ia_coeff[0], *jb_coeff[0], 0.0);
        fxc[0] -> Dot(*iajb, &total_tmp);

        retval[0] += total_tmp;
        retval[1] += total_tmp;
    } else if(fxc.size() == 3){
        for(int s = 0; s < fxc.size(); ++s){
            tmp = 0.0, total_tmp = 0.0;
            RCP<Epetra_Vector> iajb = rcp(new Epetra_Vector(*map, false));
            iajb -> Multiply(1.0, *ia_coeff[0], *jb_coeff[0], 0.0);
            fxc[0] -> Dot(*iajb, &total_tmp);
            retval[0] += total_tmp;

            iajb -> Multiply(1.0, *ia_coeff[0], *jb_coeff[1], 0.0);
            fxc[1] -> Dot(*iajb, &total_tmp);
            retval[1] += total_tmp;

            iajb -> Multiply(1.0, *ia_coeff[1], *jb_coeff[1], 0.0);
            fxc[2] -> Dot(*iajb, &total_tmp);
            retval[2] += total_tmp;
        }
    }
    // end

    // GGA contribution
    if(v2rho2.size() == 1){
        // 2 * int nabla(ia) dot nabla(jb) vsigma dtau
        RCP<Epetra_Vector> grad_ia_dot_grad_jb = rcp(new Epetra_Vector(*map));
        RCP<Epetra_MultiVector> tmp_mv = rcp(new Epetra_MultiVector(*map, 3));

        tmp_mv->Multiply(1.0, *grad_ia[0], *grad_jb[0], 0.0);

        grad_ia_dot_grad_jb->Update(1.0, *tmp_mv->operator()(0), 1.0);
        grad_ia_dot_grad_jb->Update(1.0, *tmp_mv->operator()(1), 1.0);
        grad_ia_dot_grad_jb->Update(1.0, *tmp_mv->operator()(2), 1.0);

        RCP<Epetra_Vector> grad_ia_dot_grad_jb_coeff = rcp(new Epetra_Vector(*map));
        Value_Coef::Value_Coef(mesh, grad_ia_dot_grad_jb, true, false, grad_ia_dot_grad_jb_coeff);

        RCP<Epetra_MultiVector> vsigma_coeff = rcp(new Epetra_MultiVector(*vsigma[0]));
        Value_Coef::Value_Coef(mesh, vsigma[0], true, false, vsigma_coeff);

        tmp = 0.0; total_tmp = 0.0;
        vsigma_coeff -> Dot(*grad_ia_dot_grad_jb_coeff, &total_tmp);

        retval[0] += 2 * total_tmp;
        retval[1] += 1 * total_tmp;
        // end

        // int (ia) v2rho2 (jb) dtau
        tmp = 0.0; total_tmp = 0.0;
        for(int k=0; k<NumMyElements; ++k){
            tmp += ia_coeff[0]->operator[](k) * v2rho2[0]->operator[](0)[k] * jb_coeff[0]->operator[](k);
        }

        Parallel_Util::group_sum(&tmp, &total_tmp, 1);

        retval[0] += total_tmp;
        retval[1] += total_tmp;
        //end

        // 2 * int { (ia) nabla(rho) dot nabla(jb) + nabla(rho) dot nabla(ia) jb } v2rhosigma dtau
        RCP<Epetra_Vector> ia_grad_rho_dot_grad_jb = rcp(new Epetra_Vector(*map));
        ia_grad_rho_dot_grad_jb->Multiply(1.0, *grad_rho_dot_grad_jb[0], *orbitals[0]->operator()(a), 0.0);
        ia_grad_rho_dot_grad_jb->Multiply(1.0, *ia_grad_rho_dot_grad_jb, *orbitals[0]->operator()(i), 0.0);

        RCP<Epetra_Vector> jb_grad_rho_dot_grad_ia = rcp(new Epetra_Vector(*map));
        jb_grad_rho_dot_grad_ia->Multiply(1.0, *grad_rho_dot_grad_ia[0], *orbitals[0]->operator()(b), 0.0);
        jb_grad_rho_dot_grad_ia->Multiply(1.0, *jb_grad_rho_dot_grad_ia, *orbitals[0]->operator()(j), 0.0);

        RCP<Epetra_Vector> sum = rcp(new Epetra_Vector(*ia_grad_rho_dot_grad_jb));
        sum->Update(1.0, *jb_grad_rho_dot_grad_ia, 1.0);

        Value_Coef::Value_Coef(mesh, sum, true, false, sum);

        RCP<Epetra_MultiVector> v2rhosigma_coeff = rcp(new Epetra_MultiVector(*v2rhosigma[0]));
        Value_Coef::Value_Coef(mesh, v2rhosigma[0], true, false, v2rhosigma_coeff);

        tmp = 0.0; total_tmp = 0.0;
        sum -> Dot(*v2rhosigma_coeff, &total_tmp);

        retval[0] += 3.0 * total_tmp;
        retval[1] += 3.0 * total_tmp;
        // end

        // 4 * int nabla(rho) dot nabla(ia) v2sigma2 nabla(rho) dot nabla(jb) dtau
        sum->Multiply(1.0, *grad_rho_dot_grad_ia[0], *grad_rho_dot_grad_jb[0], 0.0);
        Value_Coef::Value_Coef(mesh, sum, true, false, sum);

        RCP<Epetra_MultiVector> v2sigma2_coeff = rcp(new Epetra_MultiVector(*v2sigma2[0]));
        Value_Coef::Value_Coef(mesh, v2sigma2[0], true, false, v2sigma2_coeff);

        tmp = 0.0; total_tmp = 0.0;
        sum -> Dot(*v2sigma2_coeff, &total_tmp);

        retval[0] += 9.0 * total_tmp;
        retval[1] += 9.0 * total_tmp;
        // end
    } else if( v2rho2.size() == 3){
        Verbose::all() << "XXXXXXXXXXXXXXXXXXX Unrestricted GGA not supported!" << std::endl;
        throw std::invalid_argument("Unrestricted GGA not supported!");
    }
    // end GGA contribution

    // PGG contribution
    for(int k=0; k<xc_function_id.size(); ++k){
        if(this -> xc_data[k] -> get_xc_id() < 0){
            for(int s = 0; s < 1+orbitals.size(); ++s){
                retval[s] += xc_portion[k] * total_PGG;
            }
        }
    }
    // end
    return retval;
}

void Exchange_Correlation::get_saved_grad_density(Array< RCP<Epetra_MultiVector> > &saved_grad_density) const{
    if(this -> xc_data[0] -> density_grad.size() == 0){
        Verbose::all() << "No gradient was ever calculated but get_saved_grad_density is called!" << std::endl;
        throw std::logic_error("No gradient was ever calculated but get_saved_grad_density is called!");
    }
    Array< RCP<Epetra_MultiVector> > grad_density;
    for(int s = 0; s < this -> xc_data[0] -> density_grad.size(); ++s){
        grad_density.append( rcp(new Epetra_MultiVector(*this -> xc_data[0] -> density_grad[s])));
    }
    saved_grad_density = grad_density;
}


string Exchange_Correlation::get_functional_type() const{
    std::set<string> functional_list;
    for(int i = 0; i < this -> xc_data.size(); ++i){
        functional_list.insert(this -> xc_data[i] -> get_functional_type());
    }
    string type = "";
    for(std::set<string>::iterator it = functional_list.begin(); it != functional_list.end(); ++it){
        type += *it;
        if(it != --functional_list.end()) type += "_";
    }
    return String_Util::rtrim(type);
}

double Exchange_Correlation::get_EXX_portion() const{
    double exx_portion = 0.0;
    for(int i = 0; i < xc_function_id.size(); ++i){
        if(this -> xc_data[i] -> get_xc_id() < 0){
            exx_portion += xc_portion[i];
        }
    }
    return exx_portion;
}

RCP<const Poisson_Solver> Exchange_Correlation::get_poisson_solver() const{
    return Teuchos::rcp_const_cast<const Poisson_Solver>(this -> poisson_solver);
}
Array< Teuchos::RCP<Epetra_MultiVector> > Exchange_Correlation::get_vsigma(){
    return vsigma;
}

Array< Teuchos::RCP<Epetra_MultiVector> > Exchange_Correlation::get_v2rho2(){
    return v2rho2;
}

// vsigma_grad_ia spin index: 2*ispin_vsigma + ispin_ia
// v2rho2_ia_coeff spin index: 2*ispin_v2rho2 + ispin_ia
// grad_rho_dot_grad_?? spin index: 2*ispin_grad_rho + ispin_grad_ia/jb
vector<double> Exchange_Correlation::ia_fxc_jb(
        Array< RCP<Epetra_MultiVector> > orbitals,
        Array< RCP<Epetra_Vector> > ia_coeff,
        Array< RCP<Epetra_Vector> > jb_coeff,
        Array< RCP<Epetra_MultiVector> > grad_ia,
        Array< RCP<Epetra_MultiVector> > grad_jb,
        Array< RCP<Epetra_MultiVector> > vsigma_grad_ia,
        Array< RCP<Epetra_Vector> > v2rho2_ia_coeff,
        Array< RCP<Epetra_Vector> > grad_rho_dot_grad_ia,
        Array< RCP<Epetra_Vector> > grad_rho_dot_grad_jb,
        int i, int a, int b, int j, double total_PGG
){
    //RCP<Time> FTime = TimeMonitor::getNewCounter("Hybrid::integrate_TDDFT");
    //FTime->start();

    int retval_size = (orbitals.size() == 1)? 1: 4;
    vector<double> retval(retval_size);

    auto map = mesh->get_map();
    int NumMyElements = map->NumMyElements();

    double tmp = 0.0, total_tmp = 0.0;

    // LDA contribution
    if(fxc.size() == 1){
        tmp = 0.0, total_tmp = 0.0;

        RCP<Epetra_Vector> iajb = rcp(new Epetra_Vector(*map, false));
        iajb -> Multiply(1.0, *ia_coeff[0], *jb_coeff[0], 0.0);
        fxc[0] -> Dot(*iajb, &total_tmp);

        retval[0] += total_tmp;
    } else if(fxc.size() == 3){
        for(int s = 0; s < fxc.size(); ++s){
            tmp = 0.0, total_tmp = 0.0;
            RCP<Epetra_Vector> iajb = rcp(new Epetra_Vector(*map, false));
            iajb -> Multiply(1.0, *ia_coeff[0], *jb_coeff[0], 0.0);
            fxc[0] -> Dot(*iajb, &total_tmp);
            retval[0] += total_tmp;

            iajb -> Multiply(1.0, *ia_coeff[0], *jb_coeff[1], 0.0);
            fxc[1] -> Dot(*iajb, &total_tmp);
            retval[1] += total_tmp;

            iajb -> Multiply(1.0, *ia_coeff[1], *jb_coeff[0], 0.0);
            fxc[1] -> Dot(*iajb, &total_tmp);
            retval[2] += total_tmp;

            iajb -> Multiply(1.0, *ia_coeff[1], *jb_coeff[1], 0.0);
            fxc[2] -> Dot(*iajb, &total_tmp);
            retval[3] += total_tmp;
        }
    }
    // end

    // GGA contribution
    if(v2rho2.size() == 1){
        // 2 * int nabla(ia) dot nabla(jb) vsigma dtau
        RCP<Epetra_MultiVector> vsigma_grad_ia_coeff = rcp(new Epetra_Vector(*map,3));
        Value_Coef::Value_Coef(mesh, vsigma_grad_ia[0], true, false, vsigma_grad_ia_coeff);
        std::vector<double> dot(3);
        vsigma_grad_ia_coeff->Dot(*grad_jb[0], &dot[0]);

        retval[0] += 2 * (dot[0] + dot[1] + dot[2]);
        // end

        // int (ia) v2rho2 (jb) dtau
        tmp = 0.0; total_tmp = 0.0;
        v2rho2_ia_coeff[0]->Dot(*jb_coeff[0], &total_tmp);

        retval[0] += total_tmp;
        //end

        // 2 * int { (ia) nabla(rho) dot nabla(jb) + nabla(rho) dot nabla(ia) jb } v2rhosigma dtau
        RCP<Epetra_Vector> ia_grad_rho_dot_grad_jb = rcp(new Epetra_Vector(*map));
        ia_grad_rho_dot_grad_jb->Multiply(1.0, *grad_rho_dot_grad_jb[0], *orbitals[0]->operator()(a), 0.0);
        ia_grad_rho_dot_grad_jb->Multiply(1.0, *ia_grad_rho_dot_grad_jb, *orbitals[0]->operator()(i), 0.0);

        RCP<Epetra_Vector> jb_grad_rho_dot_grad_ia = rcp(new Epetra_Vector(*map));
        jb_grad_rho_dot_grad_ia->Multiply(1.0, *grad_rho_dot_grad_ia[0], *orbitals[0]->operator()(b), 0.0);
        jb_grad_rho_dot_grad_ia->Multiply(1.0, *jb_grad_rho_dot_grad_ia, *orbitals[0]->operator()(j), 0.0);

        RCP<Epetra_Vector> sum = rcp(new Epetra_Vector(*ia_grad_rho_dot_grad_jb));
        sum->Update(1.0, *jb_grad_rho_dot_grad_ia, 1.0);

        Value_Coef::Value_Coef(mesh, sum, true, false, sum);

        RCP<Epetra_MultiVector> v2rhosigma_coeff = rcp(new Epetra_MultiVector(*v2rhosigma[0]));
        Value_Coef::Value_Coef(mesh, v2rhosigma[0], true, false, v2rhosigma_coeff);

        tmp = 0.0; total_tmp = 0.0;
        sum -> Dot(*v2rhosigma_coeff, &total_tmp);

        retval[0] += 2 * total_tmp;
        // end

        // 4 * int nabla(rho) dot nabla(ia) v2sigma2 nabla(rho) dot nabla(jb) dtau
        sum->Multiply(1.0, *grad_rho_dot_grad_ia[0], *grad_rho_dot_grad_jb[0], 0.0);
        Value_Coef::Value_Coef(mesh, sum, true, false, sum);

        RCP<Epetra_MultiVector> v2sigma2_coeff = rcp(new Epetra_MultiVector(*v2sigma2[0]));
        Value_Coef::Value_Coef(mesh, v2sigma2[0], true, false, v2sigma2_coeff);

        tmp = 0.0; total_tmp = 0.0;
        sum -> Dot(*v2sigma2_coeff, &total_tmp);

        retval[0] += 4 * total_tmp;
        // end
    } else if( v2rho2.size() == 3){
        Array< RCP<Epetra_MultiVector> > vsigma_grad_ia_coeff;
        for(int s = 0; s < 2; ++s){
            vsigma_grad_ia_coeff.append( rcp(new Epetra_Vector(*map,3)) );
            Value_Coef::Value_Coef(mesh, vsigma_grad_ia[s], true, false, vsigma_grad_ia_coeff[s]);
        }

        // (ia_a | fxc_aa | jb_a) part
        // is == is_a is major spin index (alpha in ref. (8))
        // is_b is minor spin index (beta in ref. (8))
        // is_dest is 0 for up spin and 3 for down spin
        for(int is = 0; is < 2; ++is){
            int is_a = is; int is_b = 1-is; int is_dest = (is==0)? 0: 3;
            // 2 * int nabla(ia) dot nabla(jb) vsigma dtau
            std::vector<double> dot(3);
            vsigma_grad_ia_coeff[2*2*is_a+is_a]->Dot(*grad_jb[is_a], &dot[0]);

            retval[is_dest] += 2 * (dot[0] + dot[1] + dot[2]);
            // end

            // int (ia) v2rho2 (jb) dtau
            tmp = 0.0; total_tmp = 0.0;
            v2rho2_ia_coeff[2*2*is_a+is_a]->Dot(*jb_coeff[is_a], &total_tmp);

            retval[is_dest] += total_tmp;
            //end

            // int { (ia) nabla(rho) dot nabla(jb) + nabla(rho) dot nabla(ia) jb } v2rhosigma dtau
            // ia or jb spin index is always is_a
            // coeff i_grad_rho_ia/jb i_v2rhosigma
            // 2 2*is_a+is_a is_a==0? 0: 5 (a_aa: u_uu(0) for is==0 and d_dd(5) for is==1)
            // 1 2*is_b+is_a is_a==0? 1: 4 (a_ab: u_ud(1) for is==0 and d_ud(4) for is==1)
            int coeff_inds_rs[2][3] = {
                {2, 2*is_a+is_a, (is_a==0)? 0: 5},
                {1, 2*is_b+is_a, (is_a==0)? 1: 4}
            };
            for(int j = 0; j < 2; ++j){
                RCP<Epetra_Vector> ia_grad_rho_dot_grad_jb = rcp(new Epetra_Vector(*map, false));
                ia_grad_rho_dot_grad_jb->Multiply(1.0, *grad_rho_dot_grad_jb[coeff_inds_rs[j][1]], *orbitals[is_a]->operator()(a), 0.0);
                ia_grad_rho_dot_grad_jb->Multiply(1.0, *ia_grad_rho_dot_grad_jb, *orbitals[is_a]->operator()(i), 1.0);

                RCP<Epetra_Vector> jb_grad_rho_dot_grad_ia = rcp(new Epetra_Vector(*map, false));
                jb_grad_rho_dot_grad_ia->Multiply(1.0, *grad_rho_dot_grad_ia[coeff_inds_rs[j][1]], *orbitals[is_a]->operator()(b), 0.0);
                jb_grad_rho_dot_grad_ia->Multiply(1.0, *jb_grad_rho_dot_grad_ia, *orbitals[is_a]->operator()(j), 1.0);

                RCP<Epetra_Vector> sum = rcp(new Epetra_Vector(*ia_grad_rho_dot_grad_jb));
                sum->Update(1.0, *jb_grad_rho_dot_grad_ia, 1.0);

                Value_Coef::Value_Coef(mesh, sum, true, false, sum);

                // v2rhosigma index: a_aa: u_uu for is==0 and d_dd for is=1
                int i_v2rhosigma = coeff_inds_rs[j][2];
                RCP<Epetra_MultiVector> v2rhosigma_coeff = rcp(new Epetra_MultiVector(*v2rhosigma[i_v2rhosigma]));
                Value_Coef::Value_Coef(mesh, v2rhosigma[i_v2rhosigma], true, false, v2rhosigma_coeff);

                tmp = 0.0; total_tmp = 0.0;
                sum -> Dot(*v2rhosigma_coeff, &total_tmp);

                retval[is_dest] += coeff_inds_rs[j][0] * total_tmp;
            }
            // end

            // 4 * int nabla(rho) dot nabla(ia) v2sigma2 nabla(rho) dot nabla(jb) dtau
            // coeff i_grad_rho_ia i_grad_rho_jb i_v2sigma2
            // 4 2*is_a+is_a 2*is_a+is_a is_a==0? 0: 5
            // 2 2*is_a+is_a 2*is_b+is_a is_a==0? 1: 4
            // 2 2*is_b+is_a 2*is_a+is_a is_a==0? 1: 4
            // 1 2*is_b+is_a 2*is_b+is_a 3
            int coeff_inds_s2[4][4] = {
                {4, 2*is_a+is_a, 2*is_a+is_a, (is_a==0)? 0: 5},
                {2, 2*is_a+is_a, 2*is_b+is_a, (is_a==0)? 1: 4},
                {2, 2*is_b+is_a, 2*is_a+is_a, (is_a==0)? 1: 4},
                {1, 2*is_b+is_a, 2*is_b+is_a, 3}
            };
            for(int j = 0; j < 4; ++j){
                RCP<Epetra_Vector> sum = rcp(new Epetra_Vector(*grad_rho_dot_grad_ia[0]));
                sum->Multiply(1.0, *grad_rho_dot_grad_ia[coeff_inds_s2[j][1]], *grad_rho_dot_grad_jb[coeff_inds_s2[j][2]], 0.0);
                Value_Coef::Value_Coef(mesh, sum, true, false, sum);

                int i_v2sigma2 = coeff_inds_s2[j][3];
                RCP<Epetra_MultiVector> v2sigma2_coeff = rcp(new Epetra_MultiVector(*v2sigma2[i_v2sigma2]));
                Value_Coef::Value_Coef(mesh, v2sigma2[i_v2sigma2], true, false, v2sigma2_coeff);

                tmp = 0.0; total_tmp = 0.0;
                sum -> Dot(*v2sigma2_coeff, &total_tmp);

                retval[is_dest] += coeff_inds_s2[j][0] * total_tmp;
            }
            // end
        }
        // (ia_a | fxc_aa | jb_a) part end
        // (ia_a | fxc_ab | jb_b) part
        // is_a is alpha in ref. (9)
        // is_b is beta in ref. (9)
        // is_dest is 1 for ud and 2 for du
        for(int is = 0; is < 2; ++is){
            int is_a = is; int is_b = 1-is; int is_dest = (is==0)? 1: 2;
            // 2 * int nabla(ia) dot nabla(jb) vsigma dtau
            std::vector<double> dot(3);
            vsigma_grad_ia_coeff[2*1+is_a]->Dot(*grad_jb[is_b], &dot[0]);

            retval[is_dest] += (dot[0] + dot[1] + dot[2]);
            // end

            // int (ia) v2rho2 (jb) dtau
            tmp = 0.0; total_tmp = 0.0;
            v2rho2_ia_coeff[2*1+is_a]->Dot(*jb_coeff[is_a], &total_tmp);

            retval[is_dest] += total_tmp;
            //end

            // int { (ia) nabla(rho) dot nabla(jb) + nabla(rho) dot nabla(ia) jb } v2rhosigma dtau
            // ia or jb spin index is always is_a
            // coeff i_grad_rho_ia i_grad_rho_jb i_v2rhosigma_a_bb i_v2rhosigma_b_aa
            // 2 2*is_a+is_a is_a==0? 0: 5 (a_aa: u_uu(0) for is==0 and d_dd(5) for is==1)
            // 1 2*is_b+is_a is_a==0? 1: 4 (a_ab: u_ud(1) for is==0 and d_ud(4) for is==1)
            int coeff_inds_rs[2][5] = {
                //{2, is_a, 2*is_a+is_a, is_b, 2*is_b+is_b, (is_a==0)? 2: 3, (is_b==0)? 2: 3},
                //{1, is_a, 2*is_a+is_b, is_b, 2*is_b+is_a, (is_a==0)? 2: 3, (is_b==0)? 2: 3}
                {2, 2*is_a+is_a, 2*is_b+is_b, (is_a==0)? 2: 3, (is_b==0)? 2: 3},
                {1, 2*is_b+is_a, 2*is_a+is_b, (is_a==0)? 1: 4, (is_b==0)? 1: 4}
            };
            // Change following indicies
            for(int j = 0; j < 2; ++j){
                RCP<Epetra_Vector> ia_grad_rho_dot_grad_jb = rcp(new Epetra_Vector(*map, false));
                ia_grad_rho_dot_grad_jb->Multiply(1.0, *grad_rho_dot_grad_jb[coeff_inds_rs[j][2]], *orbitals[is_a]->operator()(a), 0.0);
                ia_grad_rho_dot_grad_jb->Multiply(1.0, *ia_grad_rho_dot_grad_jb, *orbitals[is_a]->operator()(i), 1.0);

                RCP<Epetra_Vector> jb_grad_rho_dot_grad_ia = rcp(new Epetra_Vector(*map, false));
                jb_grad_rho_dot_grad_ia->Multiply(1.0, *grad_rho_dot_grad_ia[coeff_inds_rs[j][1]], *orbitals[is_b]->operator()(b), 0.0);
                jb_grad_rho_dot_grad_ia->Multiply(1.0, *jb_grad_rho_dot_grad_ia, *orbitals[is_b]->operator()(j), 1.0);

                Value_Coef::Value_Coef(mesh, ia_grad_rho_dot_grad_jb, true, false, ia_grad_rho_dot_grad_jb);

                // v2rhosigma index: a_aa: u_uu for is==0 and d_dd for is=1
                int i_v2rhosigma = coeff_inds_rs[j][3];
                RCP<Epetra_MultiVector> v2rhosigma_coeff = rcp(new Epetra_MultiVector(*v2rhosigma[i_v2rhosigma]));
                Value_Coef::Value_Coef(mesh, v2rhosigma[i_v2rhosigma], true, false, v2rhosigma_coeff);

                tmp = 0.0; total_tmp = 0.0;
                ia_grad_rho_dot_grad_jb -> Dot(*v2rhosigma_coeff, &total_tmp);

                Value_Coef::Value_Coef(mesh, jb_grad_rho_dot_grad_ia, true, false, jb_grad_rho_dot_grad_ia);

                i_v2rhosigma = coeff_inds_rs[j][4];
                Value_Coef::Value_Coef(mesh, v2rhosigma[i_v2rhosigma], true, false, v2rhosigma_coeff);
                jb_grad_rho_dot_grad_ia -> Dot(*v2rhosigma_coeff, &tmp);
                total_tmp += tmp;

                retval[is_dest] += coeff_inds_rs[j][0] * total_tmp;
            }
            // end

            // 4 * int nabla(rho) dot nabla(ia) v2sigma2 nabla(rho) dot nabla(jb) dtau
            // coeff i_grad_rho_ia i_grad_rho_jb i_v2sigma2
            // 4 2*is_a+is_a 2*is_a+is_a is_a==0? 0: 5
            // 2 2*is_a+is_a 2*is_b+is_a is_a==0? 1: 4
            // 2 2*is_b+is_a 2*is_a+is_a is_a==0? 1: 4
            // 1 2*is_b+is_a 2*is_b+is_a 3
            int coeff_inds_s2[4][4] = {
                {4, 2*is_a+is_a, 2*is_a+is_a, 2},
                {2, 2*is_a+is_a, 2*is_a+is_b, (is_a==0)? 1: 4},
                {2, 2*is_b+is_a, 2*is_b+is_b, (is_b==0)? 1: 4},
                {1, 2*is_b+is_a, 2*is_a+is_b, 3}
            };
            for(int j = 0; j < 4; ++j){
                RCP<Epetra_Vector> sum = rcp(new Epetra_Vector(*grad_rho_dot_grad_ia[0]));
                sum->Multiply(1.0, *grad_rho_dot_grad_ia[coeff_inds_s2[j][1]], *grad_rho_dot_grad_jb[coeff_inds_s2[j][2]], 0.0);
                Value_Coef::Value_Coef(mesh, sum, true, false, sum);

                int i_v2sigma2 = coeff_inds_s2[j][3];
                RCP<Epetra_MultiVector> v2sigma2_coeff = rcp(new Epetra_MultiVector(*v2sigma2[i_v2sigma2]));
                Value_Coef::Value_Coef(mesh, v2sigma2[i_v2sigma2], true, false, v2sigma2_coeff);

                tmp = 0.0; total_tmp = 0.0;
                sum -> Dot(*v2sigma2_coeff, &total_tmp);

                retval[is_dest] += coeff_inds_s2[j][0] * total_tmp;
            }
            // end
        }
    }
    // end GGA contribution

    // PGG contribution
    for(int k=0; k<xc_function_id.size(); ++k){
        if(this -> xc_data[k] -> get_xc_id() < 0){
            for(int s = 0; s < 1+orbitals.size(); ++s){
                retval[s] += xc_portion[k] * total_PGG;
            }
        }
    }
    // end
    return retval;
}
vector<double> Exchange_Correlation::ia_fxc_jb_decoupled(
        RCP<Epetra_MultiVector> orbitals,
        RCP<Epetra_Vector> ia_coeff,
        RCP<Epetra_Vector> jb_coeff,
        RCP<Epetra_MultiVector> grad_ia,
        RCP<Epetra_MultiVector> grad_jb,
        RCP<Epetra_MultiVector> vsigma_grad_ia,
        RCP<Epetra_Vector> v2rho2_ia_coeff,
        RCP<Epetra_Vector> grad_rho_dot_grad_ia,
        RCP<Epetra_Vector> grad_rho_dot_grad_jb,
        int i, int a, int b, int j, double total_PGG,
        bool force_polarized/* = false*/
){
    vector<double> retval;
    if(force_polarized){
        // Be sure that orbitals, ia/jb_coeff, grad_ia/jb are not modified in ia_fxc_jb!
        Array< RCP<Epetra_MultiVector> > vsigma_grad_ia1;
        Array< RCP<Epetra_Vector> > v2rho2_ia_coeff1;
        Array< RCP<Epetra_Vector> > grad_rho_dot_grad_ia1;
        Array< RCP<Epetra_Vector> > grad_rho_dot_grad_jb1;
        for(int s = 0; s < 2; ++s){
            grad_rho_dot_grad_ia1.append(rcp(new Epetra_Vector(*grad_rho_dot_grad_ia)));
            grad_rho_dot_grad_ia1[s] -> Scale(0.25);
            grad_rho_dot_grad_jb1.append(rcp(new Epetra_Vector(*grad_rho_dot_grad_jb)));
            grad_rho_dot_grad_jb1[s] -> Scale(0.25);
        }
        vector<double> normval =
            this -> ia_fxc_jb(Array< RCP<Epetra_MultiVector> >(2, orbitals),
                    Array< RCP<Epetra_Vector> >(2, ia_coeff),
                    Array< RCP<Epetra_Vector> >(2, jb_coeff),
                    Array< RCP<Epetra_MultiVector> >(2, grad_ia),
                    Array< RCP<Epetra_MultiVector> >(2, grad_jb),
                    vsigma_grad_ia1,
                    v2rho2_ia_coeff1,
                    grad_rho_dot_grad_ia1,
                    grad_rho_dot_grad_jb1,
                    i, a, b, j, total_PGG);
        retval.push_back(0.5*(normval[0]+normval[1]));
        retval.push_back(0.5*(normval[0]-normval[1]));
    } else {
        vector<double> normval =
            this -> ia_fxc_jb(Array< RCP<Epetra_MultiVector> >(1, orbitals),
                    Array< RCP<Epetra_Vector> >(1, ia_coeff),
                    Array< RCP<Epetra_Vector> >(1, jb_coeff),
                    Array< RCP<Epetra_MultiVector> >(1, grad_ia),
                    Array< RCP<Epetra_MultiVector> >(1, grad_jb),
                    Array< RCP<Epetra_MultiVector> >(1, vsigma_grad_ia),
                    Array< RCP<Epetra_Vector> >(1, v2rho2_ia_coeff),
                    Array< RCP<Epetra_Vector> >(1, grad_rho_dot_grad_ia),
                    Array< RCP<Epetra_Vector> >(1, grad_rho_dot_grad_jb),
                    i, a, b, j, total_PGG);
        retval.push_back(normval[0]);
    }
    return retval;
}
