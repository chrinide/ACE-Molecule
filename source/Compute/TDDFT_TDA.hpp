#pragma once
#include <vector>

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_CrsMatrix.h"

#include "TDDFT_ABBA.hpp"


/**
 * @brief This class solve TDDFT Tamm-Dancoff approximation.
 * @author Kwangwoo Hong, Jaewook Kim, Sungwoo Kang.
 * @date 2017/01/05
 **/
class TDDFT_TDA: public TDDFT{
    public:
        TDDFT_TDA(Teuchos::RCP<const Basis> mesh, Teuchos::RCP<Teuchos::ParameterList> parameters);
        virtual int compute(Teuchos::RCP<const Basis> mesh, Teuchos::Array< Teuchos::RCP<State> >& states);
        virtual ~TDDFT_TDA(){};

    protected:
        virtual void fill_TD_matrix_restricted(
                std::vector<double> orbital_energies, 
                double Hartree_contrib, std::vector<double> xc_contrib,
                int matrix_index_c, int matrix_index_r, 
                Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix
        );

        virtual void Kernel_TDHF_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, bool kernel_is_not_hybrid, bool add_delta_term, double scale = 1.0);
        /*
        virtual void Kernel_HF_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, double scale = 1.0);
        virtual void Kernel_KSCI_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, bool is_TDHF, double scale = 1.0);
        */

        /**
         * @brief Get Z vector from eigenvectors and spread over processors.
         * @param input Input eigenvector.
         * @param eigvals GS eigenvalues.
         * @return output Z vector.
         **/
        virtual std::vector< std::vector<double> > gather_z_vector(
            Teuchos::RCP<Epetra_MultiVector> input, 
            std::vector<double> eigvals
        );
};
