#pragma once
#include <vector>
#include <string>
#include "../State/Scf_State.hpp"

#include "xc.h"
#include "Compute_XC.hpp"

/**
 * @brief Libxc library wrapper class for conventinoal exchange-correlation functionals.
 * @details Using xc library of "Libxc"(http://www.tddft.org/programs/octopus/wiki/index.php/Libxc)<br/>
 * The list of available functionals can be found in <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc_functionals">here</a> or the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a>.
 *  Meta-GGA functionals are not implemented.(Energy calculation without NLCC can be computed)<br/>
 *  Reference :  Miguel A. L. Marques, Micael J. T. Oliveira, and Tobias Burnus, <i>Comput. Phys. Commun.</i> <b>183</b>, 2272 (2012)
 */
class Libxc: public Compute_XC{
    public:
        /**
         * @brief Constructor of Exchange_Correlation class
         * @details Constructor for the input parameter "XCFunctional" is exist.
         */
        Libxc(
            Teuchos::RCP<const Basis> mesh, ///< Basis data
            int function_id, ///< XCFunctional id, full list is available in "xc_funcs.h" of Libxc library
            int NGPU = 0,
            double cutoff = 1.0E-9 ///< XC density threshold. Density below this value will make energy/potential/kernel zero.
        );

        /*
        / **
         * @brief Calculate exchange-correlation energy and potential of given state.
         * @details compute function for the object of State class.
         * @return 0 (normal termination) The computed exchange-correlation energy is added to total energy, and the potential is added to state->local_potential[i_spin]
         * /
        int compute(
			Teuchos::RCP<const Basis> mesh,  ///< Basis data
			Teuchos::Array<Teuchos::RCP<State> >& states ///< Teuchos::Array of State object. Last entry is read and updated.
		);
        */
        /**
        * @brief Calculate exchange-correlation energy and potential of given Scf_state.
        * @details compute function for the object of Scf_State class.
        * @return 0 (normal termination) The computed exchange-correlation energy is replaced to exchange_energy and correlation_energy of last Scf_State, and the exchange_potential and correlation_potential of last Scf_State is replaced with calcaulated one.
        */
        int compute(
                Teuchos::RCP<XC_State>& xc_info///< Teuchos::Array of Scf_State object. Last entry is read and updated.
        );

        /**
         * @brief Calculate exchange-correlation energy without orbital
         * @details Note that this routine CANNOT compute KLI and HF exchange potential, since it does not have orbital informations.
         * @return parameter exchange_energy, and correlation_energy will be changed to corresponding values.
         * @param density electron (spin) density
         * @param density_grad gradient of density electron (spin) density
         * @param occupations orbital occupation numbers of the given system
         * @param exchange_energy output, exchange energy of given density
         * @param correlation_energy output, correlation energy of given density
         * @param core_density core electron (spin) density, default: null Epetra_Vector
         * @param core_density_grad core electron (spin) density gradient components, default: null Epetra_MultiVector
         */
        void compute_Exc(
                Teuchos::RCP<XC_State> &xc_info
                );
        /**
         * @brief Calculate integrated value of electric density and exchange-correlation potential with orbital
         * @details This method calculates
         * \f[
               \int d\vec{r} \rho (\vec{r}) v_{xc}(\vec{r}),
           \f]
           where \f$\rho\f$ is electric density and \f$v_{xc}\f$ is exchange-correlation potential
           Note that this routine CAN compute KLI and HF exchange potential, since it has orbital informations.
         * @return parameter int_n_vxc will be changed to corresponding values.
         * @param density electron (spin) density
         * @param density_grad gradient of density electron (spin) density
         * @param occupations orbital occupation numbers of the given system
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         * @param int_n_vxc output, integrated value of electric density times exchange-correlation potential of given system
         * @param exchange_potential exchange potential of given system, default: null Epetra_Vector
         * @param correlation_potential correlation potential of given system, default: null Epetra_Vector
         */
        /*
        void integrate_rho_vxc(
            Teuchos::RCP<XC_State> &xc_info
        );
        */
        /**
         * @brief Calculate exchange-correlation potential with orbitals
           @details Note that this routine CAN compute KLI and HF exchange potential, since it has orbital informations.
         * @return parameter exchange_potential, and correlation_potential will be changed to corresponding values.
         * @param density electron (spin) density
         * @param density_grad gradient of density electron (spin) density
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         * @param exchange_potential exchange potential of given system
         * @param correlation_potential correlation potential of given system
         */
        virtual void compute_vxc(
            Teuchos::RCP<XC_State> &xc_info
        );
        /**
         * @brief Calculate exchange-correlation energy density with orbitals.
           @details Note that this routine CAN compute KLI and HF exchange potential, since it has orbital informations. However KLI of HF energy density is not well defined.(See KLI and HF class)
         * @return parameter energy_density will be changed to corresponding values.
         * @param density electron (spin) density
         * @param density_grad gradient of density electron (spin) density
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         * @param energy_density exchange-correlation energy density of given system
         */
        virtual void compute_exc(
            Teuchos::RCP<XC_State> &xc_info
        );
        /**
         * @brief Calculate exchange-correlation kernel with orbitals
         * @details Note that this routine CAN compute KLI and HF exchange potential, since it has orbital informations.
         * @return protected variables are changed. Use get_fxc functions? (??? ask Kwangwoo)
         * @param density electron (spin) density
         * @param orbital_coeff orbital coeffecients of the given system
         * @param occupations orbital occupation numbers of the given system
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         */
        virtual void compute_kernel(
            Teuchos::RCP<XC_State> &xc_info
        );

        virtual std::string get_info_string(bool verbose = true);
        virtual int get_functional_kind();
    protected:
        int rel_flag;
        double omega;
        const int NGPU;
        /**
         * @brief Functional types. Libxc parameter.
         * Available xc_func_type : XC_FAMILY_LDA, XC_FAMILY_GGA, XC_FAMILY_MGGA
         */
        xc_func_type func;
        void contract_gradient(
                Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > density_grad,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > &cont_grad
        );

        /**
         * @brief XC density threshold.
         **/
        double cutoff;
};
