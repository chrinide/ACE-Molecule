#include "Force_Paw.hpp"
#include <vector>
#include <complex>

#include "Epetra_MultiVector.h"
#include "Teuchos_SerialDenseMatrix.hpp"

#include "../Core/Pseudo-potential/Paw.hpp"
#include "../Util/Trilinear_Interpolation.hpp"
#include "../Util/Verbose.hpp"

using std::vector;
using Teuchos::Array;
using Teuchos::RCP;

Force_Paw::Force_Paw(
        RCP<const Basis> mesh,
        RCP<const Atoms> atoms,
        RCP<Nuclear_Potential> nuclear_potential,
        RCP<External_Field> external_field,
        RCP<Teuchos::ParameterList> parameters
):Force(mesh, atoms, nuclear_potential, external_field, parameters){}

int Force_Paw::compute(
        RCP<const Basis> mesh,
        Array< Teuchos::RCP<State> > &states
){
    Array< RCP<const Occupation> > occupations = states[states.size()-1] -> get_occupations();
    int spin_size = occupations.size();
    Array< RCP<const Epetra_MultiVector> > orbitals = states[states.size()-1] -> get_orbitals();
    Array< RCP<const Epetra_Vector> > ps_density = states[states.size()-1] -> get_density();
    Array< RCP<Epetra_Vector> > ps_core_density = states[states.size()-1] -> get_core_density();

    Array< RCP<Epetra_Vector> > eff_potential = states[states.size()-1] -> get_local_potential();
    //RCP<Epetra_Vector> hartree_potential = states[states.size()-1] -> get_hartree_potential();
    RCP<Epetra_Vector> hartree_potential = this -> nuclear_potential -> get_paw() -> get_fine_hartree_potential();

    RCP<const Basis> fine_mesh = this -> nuclear_potential -> get_paw() -> get_fine_basis();
    if(hartree_potential == Teuchos::null){
        Verbose::all() << "Hartree potential is required for the PAW force calculations!" << std::endl;
        exit(EXIT_FAILURE);
    }
    Array< vector<double> > eigenvalues = states[states.size()-1] -> get_orbital_energies();
    double scaling = this -> mesh -> get_scaling()[0]*this -> mesh -> get_scaling()[1]*this -> mesh -> get_scaling()[2];
    double fine_scaling = fine_mesh -> get_scaling()[0]*fine_mesh -> get_scaling()[1]*fine_mesh -> get_scaling()[2];

    this -> minus_F_tot.clear();
    for(int ia = 0; ia < this -> atoms -> get_size(); ++ia){
        RCP<Paw_Atom> paw_atom = this -> nuclear_potential -> get_paw() -> get_paw_atom(ia);
        Array< Teuchos::SerialDenseMatrix<int,double> > sD_matrix = this -> nuclear_potential -> get_paw() -> get_density_matrixes()[ia];
        vector<double> retval(3);

        Array< RCP<Epetra_MultiVector> > core_ps_density_grad = paw_atom -> get_core_density_grad_vector(false);
        RCP<Epetra_MultiVector> comp_charge_grad = paw_atom -> get_compensation_charge_grad(sD_matrix, true);

        for(int i = 0; i < 3; ++i){
            for(int s = 0; s < occupations.size(); ++s){
                double tmp;
                // Core density contribution.
                eff_potential[s] -> Dot( *core_ps_density_grad[s] -> operator()(i), &tmp );
                retval[i] = tmp*scaling;
            }
            double tmp;
            // Core compensation charge contribution.
            hartree_potential -> Dot( *comp_charge_grad -> operator()(i), &tmp );
            retval[i] += tmp * fine_scaling;
            Verbose::single() << "Atom " << ia << " direction " << i << " core contribution = " << retval[i] << std::endl;
        }

        // Zero potential contribution.
        Array< RCP<Epetra_Vector> > zero_potential_gradient = paw_atom -> get_zero_potential_gradient();
        vector<double> zeroval(3, 0.0);
        for(int i = 0; i < 3; ++i){
            for(int s = 0; s < occupations.size(); ++s){
                double tmp;
                ps_density[s] -> Dot( *zero_potential_gradient[i], &tmp );
                zeroval[i] += tmp;
                ps_core_density[s] -> Dot( *zero_potential_gradient[i], &tmp );
                zeroval[i] += tmp;
            }
            zeroval[i] *= scaling;
            retval[i] += zeroval[i];
            Verbose::single() << "Atom " << ia << " direction " << i << " zero potential contribution = " << zeroval[i] << std::endl;
        }

        // Smooth orbital contribution.
        vector< vector< vector<double> > > hamiltonian_correction = paw_atom -> get_Hamiltonian_correction_matrix(hartree_potential, sD_matrix);
        Teuchos::SerialDenseMatrix<int,double> overlap_mat = paw_atom -> get_overlap_matrix();
        vector< vector< vector<double> > > proj_dot_orb = paw_atom -> projector_dot_orbitals(orbitals);
        Array< RCP<Epetra_MultiVector> > projector_grad = paw_atom -> get_projector_gradients();
        vector< vector< vector< vector<double> > > > proj_grad_dot_orb(3);
        for(int d = 0; d < 3; ++d){
            proj_grad_dot_orb[d].resize(spin_size);
            for(int alpha = 0; alpha < spin_size; ++alpha){
                proj_grad_dot_orb[d][alpha].resize(orbitals[alpha] -> NumVectors());
                for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n){
                    proj_grad_dot_orb[d][alpha][n].resize(hamiltonian_correction[alpha].size(), 0.0);
                }
            }
        }
        for(int d = 0; d <3; ++d){
            for(int alpha = 0; alpha < spin_size; ++alpha ){
                for(int n = 0; n < orbitals[alpha] -> NumVectors(); ++n ){
                    for(int i = 0; i < hamiltonian_correction[alpha].size(); ++i){
                        projector_grad[d] -> operator()(i) -> Dot( *(orbitals[alpha]->operator()(n)), &proj_grad_dot_orb[d][alpha][n][i] );
                    }
                }
            }
        }
        vector<double> valval(3, 0.0);
        for(int s = 0; s < spin_size; ++s){
            for(int n = 0; n < occupations[s] -> get_size(); ++n){
                if( occupations[s] -> operator[](n) > 1.0E-6 ){
                    for(int i1 = 0; i1 < hamiltonian_correction[s].size(); ++i1){
                        for(int i2 = 0; i2 < hamiltonian_correction[s].size(); ++i2){
                            double matrix_part = hamiltonian_correction[s][i1][i2]-eigenvalues[s][n]*overlap_mat(i1,i2);
                            matrix_part *= occupations[s] -> operator[](n);
                            for(int d = 0; d < 3; ++d){
                                valval[d] += matrix_part * (proj_dot_orb[s][n][i1]*proj_grad_dot_orb[d][s][n][i2] + proj_grad_dot_orb[d][s][n][i1]*proj_dot_orb[s][n][i2]);
                                retval[d] += matrix_part * (proj_dot_orb[s][n][i1]*proj_grad_dot_orb[d][s][n][i2] + proj_grad_dot_orb[d][s][n][i1]*proj_dot_orb[s][n][i2]);
                            }
                        }
                    }
                }
            }
        }
        for(int d = 0; d < 3; ++d){
            Verbose::single() << "Atom " << ia << " direction " << d << " valence contribution = " << valval[d] << std::endl;
            this -> minus_F_tot.push_back(-retval[d]);
        }
    }
    Force::print_force();
    return 3;
}
