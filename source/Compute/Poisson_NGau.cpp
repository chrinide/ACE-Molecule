#include "Poisson_NGau.hpp"

//#include "Teuchos_TimeMonitor.hpp"
#include "../Util/Time_Measure.hpp"

//#include "../Util/Faddeeva.hpp"
#include "../Util/Verbose.hpp"
//#include "../Util/Parallel_Manager.hpp"
//#include "../Util/Parallel_Util.hpp"

//#include "../Util/Value_Coef.hpp"
#include "../Util/String_Util.hpp"

using std::abs;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::SerialDenseMatrix;

Poisson_NGau_Solver::Poisson_NGau_Solver(
        RCP<const Basis> mesh,
        std::vector<double> exponent, std::vector<double> coeff, int ngpus)
: Poisson_Solver::Poisson_Solver(mesh, 0.0, 0.0, 0.0, 0, 0, false, ngpus){
//: Kernel_Integral::Kernel_Integral(mesh, 0.0, 0.0, 0.0, 0, 0, false, ngpus){
    // t_f = sqrt(M_PI) to remove Sundholm asymptotic correction.
    if(exponent.size() != coeff.size()){
        Verbose::all() << "N-Gaussian kernel:: exponent and coefficient have different size of "
                       << exponent.size() << " and " << coeff.size() << std::endl;
        exit(EXIT_FAILURE);
    }
    this->t_total.resize(exponent.size());
    this->w_total = coeff;
    // Exploit Poisson solver that uses 1/r = 2/\sqrt(\pi) \int_0^\infty e^{-t^2r^2} dt
    for(int i = 0; i < this -> w_total.size(); ++i){
        this -> w_total[i] *= sqrt(M_PI)/2;
        this -> t_total[i] = sqrt(exponent[i]);
    }

    this -> type = "NGAU";
    internal_timer = rcp(new Time_Measure() );
    this -> t_sampling();// This hangs if called later.
}

int Poisson_NGau_Solver::compute(RCP<const Basis> mesh, Array<RCP<State> >& states){
    RCP<Epetra_Vector> hartree_potential  = rcp(new Epetra_Vector(*mesh->get_map() ) );
    if (*mesh!=(*this->mesh)){
        Verbose::all() << " mesh in Poisson_NGau_Solver is different to given mesh" << std::endl
                       << this->mesh.get() << std::endl
                       << mesh.get() << std::endl;
        exit(-1);
    }
    const int spin_size = states[states.size()-1]->density.size();
    double hartree_energy;

    //Verbose::single()<< "~~~~~~~~" <<std::endl;
    if (spin_size==0){
        Verbose::all() << "Poisson_NGau_Solver:: No density!!" << std::endl;
        exit(-1);
    }
    this -> compute(states[states.size()-1]->density, hartree_potential,hartree_energy);

    RCP<State> new_state = rcp(new State(*states[states.size()-1]) );
    new_state->local_potential.clear();
    new_state->local_potential.append(hartree_potential );

    new_state->total_energy = hartree_energy;

    states.append(new_state);
    return 0;
}


void Poisson_NGau_Solver::t_sampling(){
    Verbose::single(Verbose::Detail) << "NGAU VERSION OF t-sampling is called!!" << std::endl;
    int NumProc = Parallel_Manager::info().get_mpi_size();
    is_t_sampled = true;

    t_proc.clear(); i_proc.clear(); w_proc.clear();
    for(int alpha=0; alpha<t_total.size(); alpha++){
        int iproc = alpha % NumProc;
        if(Parallel_Manager::info().get_mpi_rank() == iproc){
            t_proc.push_back(t_total[alpha]);
            i_proc.push_back(alpha);
            w_proc.push_back(w_total[alpha]);
        }
    }

    this -> which_t_sampling_used = "NGAU";
    return;
}

std::string Poisson_NGau_Solver::get_info_string() const{
    std::string info = "Hartree potential computer (Poisson_NGau_Solver) info:\n";
    info += String_Util::to_string((long long)w_total.size()) + " points quadrature\n";
    info += "Number of GPUs: " + String_Util::to_string(ngpus) + "\n";
    for(int i = 0; i < t_total.size(); ++i){
        info += "Gaussian coefficients, exponent pair " + String_Util::to_string(i+1) + ": [" + String_Util::to_string(this -> w_total[i]*2/std::sqrt(M_PI)) + ", " + String_Util::to_string(t_total[i]*t_total[i]) + "]\n";
    }

    info += string("Asymtotic correction: ") + (asymtotic_correction? "on": "off") + "\n";
    return info;
}
