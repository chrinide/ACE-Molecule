#include "Poisson.hpp"
#include <unistd.h>
#include <cmath>

//#include "../Basis/Basis_Function/Legendre.hpp"
//#include "Teuchos_Time.hpp"
//#include "Teuchos_TimeMonitor.hpp"
#include "../Util/Time_Measure.hpp"
#include "../Basis/Basis_Function/Legendre.hpp"

//#include "../Util/Verbose.hpp"

#include "../Util/String_Util.hpp"
#include "../Util/Value_Coef.hpp"

using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::SerialDenseMatrix;

Poisson_Solver::Poisson_Solver(RCP<const Basis> mesh,
                                double t_i, double t_l, double t_f,
                                int num_points1, int num_points2,
                                bool asymtotic_correction/* = true*/, int ngpus /*= 0*/ )
                                :Kernel_Integral(mesh,t_i,t_l,t_f,num_points1,num_points2,asymtotic_correction,ngpus)
{
    this -> type = "NORMAL";
    gpu_enable = (0<ngpus);
    internal_timer = rcp(new Time_Measure() );
}

int Poisson_Solver::compute(RCP<const Basis> mesh, Array<RCP<State> >& states){
    RCP<Epetra_Vector> hartree_potential  = rcp(new Epetra_Vector(*mesh->get_map() ) );
    if (*mesh!=(*this->mesh)){
        Verbose::all() << " mesh in Poisson_Solver is different to given mesh" << std::endl;
        Verbose::all() << this->mesh.get()<< std::endl;
        Verbose::all() << mesh.get() << std::endl;
        exit(-1);
    }
    const int spin_size = states[states.size()-1]->density.size();
    double hartree_energy;

    if (spin_size==0){
        Verbose::all() << "Poisson_Solver:: Problem!!" << std::endl;
        exit(-1);
    }
    this -> compute(states[states.size()-1]->density, hartree_potential,hartree_energy);
    RCP<State> new_state = rcp(new State(*states[states.size()-1]) );
    new_state->local_potential.clear();
    new_state->local_potential.append(hartree_potential );
    new_state->total_energy = hartree_energy;

    states.append(new_state);
    return 0;
}

void Poisson_Solver::t_sampling(){
    // New method by Sundholm (JCP 132, 024102, 2010).
    // Within two-divided regions ([t_i,t_l], [t_l,t_f]),
    // num_points1, num_points2 quadrature points are made, respectively.
    Verbose::single(Verbose::Detail) << "t_sampling is called" << std::endl;
    Verbose::single(Verbose::Detail) << "XXXXXXXXXXXXXXX NON NGAU VERSION OF t-sampling is called!" << std::endl;
    // Linear coord region:  [t_i, t_l]
    is_t_sampled = true;
    int* legendre_points = new int [3];
    legendre_points[0] = num_points1;
    legendre_points[1] = 1;
    legendre_points[2] = 1;

    double* legendre_scaling = new double [3];
    legendre_scaling[0] = 1;
    legendre_scaling[1] = 1;
    legendre_scaling[2] = 1;

    Legendre* legendre = new Legendre(legendre_points, legendre_scaling);
    double* x_leg = new double [num_points1];
    double* w_leg = new double [num_points1];
    legendre->gauleg(t_i, t_l, x_leg, w_leg, num_points1);

    for(int j=0; j<num_points1; j++){
        t_total.push_back(x_leg[j]);
        w_total.push_back(w_leg[j]);
    }

    delete[] w_leg;
    delete[] x_leg;
    delete legendre;

    // Logarithmic coord region: [t_l, t_f]
    legendre_points[0] = num_points2;
    legendre = new Legendre(legendre_points, legendre_scaling);
    x_leg = new double [num_points2];
    w_leg = new double [num_points2];
    legendre->gauleg(log(t_l), log(t_f), x_leg, w_leg, num_points2);

    // Return the log-coord-partitioned points back to linear t-space.
    double s_p, w_p;
    for(int j=0; j<num_points2; j++){
        s_p = x_leg[j];
        w_p = w_leg[j];
        x_leg[j] = exp(s_p);
        w_leg[j] = w_p * exp(s_p);
    }

    for(int j=0; j<num_points2; j++){
        t_total.push_back(x_leg[j]);
        w_total.push_back(w_leg[j]);
    }
    delete[] w_leg;
    delete[] x_leg;
    delete legendre;
    delete[] legendre_scaling;
    delete[] legendre_points;

    //shchoi
/*
    t_total.push_back(1.0);
    w_t.push_back(1.0);
*/
    t_proc.clear(); i_proc.clear(); w_proc.clear();
    int NumProc = Parallel_Manager::info().get_mpi_size();
    for(int alpha=0; alpha<t_total.size(); alpha++){
        int iproc = alpha % NumProc;
        if(Parallel_Manager::info().get_mpi_rank() == iproc){
            t_proc.push_back(t_total[alpha]);
            i_proc.push_back(alpha);
            w_proc.push_back(w_total[alpha]);
        }
    }

    this -> which_t_sampling_used = "NORMAL";
    return;
}

std::string Poisson_Solver::get_info_string() const{
    std::string info = "Hartree potential computer (Poisson) info:\n";
    info += String_Util::to_string(num_points1+num_points2) + " points quadrature\n";
    info += "Number of GPUs: " + String_Util::to_string(ngpus) + "\n";
    info += "Linear sampling region:      [" + String_Util::to_string(t_i) + ", " + String_Util::to_string(t_l) + "], " + String_Util::to_string(num_points1) + "\n";
    info += "Logarithmic sampling region: [" + String_Util::to_string(t_l) + ", " + String_Util::to_string(t_f) + "], " + String_Util::to_string(num_points2) + "\n";
    info += std::string("Asymtotic correction: ") + (asymtotic_correction? "on": "off") + "\n";
    return info;
}
