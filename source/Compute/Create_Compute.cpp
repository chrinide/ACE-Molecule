#include "Create_Compute.hpp"
#include <string>
#include <array>
#include <cctype>

#include "Libxc.hpp"
#include "KLI.hpp"
#include "XC_LC_wPBE_NGau.hpp"
#include "Compute_XC.hpp"
#include "../State/XC_State.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../Core/Occupation/Occupation_Zero_Temp.hpp"

#include "../Core/Occupation/Create_Occupation.hpp"
#include "../Util/String_Util.hpp"
#include "Exchange_Correlation.hpp"
#include "Poisson_NGau.hpp"

using std::string;
using std::vector;
using Teuchos::null;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;
using Teuchos::ParameterList;

RCP<Poisson_Solver> Create_Compute::Create_Poisson(RCP<const Basis> mesh, RCP<ParameterList> parameters){
    // Default values of t-integration parameters.
//    if(!parameters->sublist("ISF").isParameter("Initial_Integrate_NumGrid")){
//        parameters->sublist("ISF").set<int>("Initial_Integrate_NumGrid", 800);
//    }
//    if(!parameters->sublist("ISF").isParameter("Integrate_Max")){
//        parameters->sublist("ISF").set<double>("Integrate_Max", 10.0);
//    }

    double t_i = parameters->sublist("ISF").get<double>("Ti", 0.0);
    double t_l = parameters->sublist("ISF").get<double>("Tl", 2.0);
    double t_f = parameters->sublist("ISF").get<double>("Tf", 1000.0);
    int num_points1 = parameters->sublist("ISF").get<int>("Num_Points1", 25);
    int num_points2 = parameters->sublist("ISF").get<int>("Num_Points2", 10);
    int ngpus = parameters->sublist("ISF").get<int>("NGPU", 0);

    Verbose::single(Verbose::Detail) <<"Create_Poisson" << std::endl;
    Verbose::single(Verbose::Detail) << "t_i : " << t_i << std::endl;
    Verbose::single(Verbose::Detail) << "t_l : " << t_l << std::endl;
    Verbose::single(Verbose::Detail) << "t_f : " << t_f << std::endl;
    Verbose::single(Verbose::Detail) << "num_points1 : " << num_points1 << std::endl;
    Verbose::single(Verbose::Detail) << "num_points2 : " << num_points2 << std::endl;
    Verbose::single(Verbose::Detail) << "gpu:  " << ngpus <<std::endl;
    return rcp( new Poisson_Solver(mesh,t_i,t_l,t_f,num_points1,num_points2,true,ngpus  ) );
}

RCP<Core_Hamiltonian> Create_Compute::Create_Core_Hamiltonian(RCP<const Basis> mesh,RCP<const Atoms> atoms,RCP<ParameterList> parameters){

    Teuchos::RCP<Teuchos::ParameterList> occ_param;
    //obtain occupation method from Scf or Guess procedure
    if(parameters->isSublist("Scf")){
        if(parameters->sublist("Scf").isSublist("Occupation")){
            occ_param = sublist(sublist(parameters,"Scf"),"Occupation");
        }
    }
    else if(parameters->isSublist("Guess")){
        if(parameters->sublist("Guess").isSublist("Occupation")){
            occ_param = sublist(sublist(parameters,"Scf"),"Occupation");
        }
        if(parameters -> sublist("Guess").isParameter("NumberOfEigenvalues")){
            occ_param -> set("OccupationSize", parameters->sublist("Guess").get<int>("NumberOfEigenvalues"));
        }
    }
    // if nothing found, set default occ_param
    if (occ_param==Teuchos::null){
        occ_param = Teuchos::rcp(new ParameterList() );
    }

    occ_param->set("Polarize",parameters->sublist("BasicInformation").get<int>("Polarize") );
    occ_param->set("NumElectrons",parameters->sublist("BasicInformation").get<double>("NumElectrons") );
    occ_param->set("SpinMultiplicity",parameters->sublist("BasicInformation").get<double>("SpinMultiplicity") );
    bool store_crs_matrix =  parameters->sublist("BasicInformation").get<int> ("StoreCoreHamiltonian", 0)==0? false:true;

    auto occupations = Create_Occupation::Create_Occupation(occ_param );
    return rcp( new Core_Hamiltonian(mesh,occupations,atoms,store_crs_matrix,parameters)  );
}

void split_xc_param(std::string id_and_portion, int &id, double &portion){
    vector<string> id_portion = String_Util::strSplit(id_and_portion, " ");
    string ids_and_portions;
    if(id_portion[0].find_first_not_of("-0123456789") != std::string::npos){
        // This line can be changed to regex using "^(\\-|\\+)?([0-9]+)?" with C++11. (Counts whole integer.)
        id_portion[0] = String_Util::to_string( (long long) XC(functional_get_number)(id_portion[0].c_str()) );
    }
    if(id_portion.size() > 1){
        ids_and_portions = id_portion[0]+" "+id_portion[1];
        id = std::stoi(id_portion[0]);
        portion = std::stod(id_portion[1]);
    } else {
        ids_and_portions = id_portion[0]+" 1.0";
        id = std::stoi(id_portion[0]);
        portion = 1.0;
    }
}

RCP<Exchange_Correlation> Create_Compute::Create_Exchange_Correlation(
        RCP<const Basis> mesh, RCP<ParameterList> parameters,
        RCP<Poisson_Solver> poisson)
{
    int functional_type = 0;
    Array<string> x_ids_and_portions;
    Array<string> c_ids_and_portions;

    RCP<ParameterList> xc_param = Teuchos::sublist(parameters, "ExchangeCorrelation");

    if(xc_param -> isParameter("XCLibrary")){
        Verbose::single(Verbose::Simple) << "Exchange_Correlation::XCLibrary option is removed since XCFun is not supported now." << std::endl;
        xc_param -> remove("XCLibrary");
    }
    if(!xc_param -> isParameter("XCFunctional") and (!xc_param -> isParameter("CFunctional") and !xc_param -> isParameter("XFunctional")) ){
        if( xc_param -> isParameter("FunctionalName") ){
            if( xc_param -> get<string>("FunctionalName") == "PZ" ){
                xc_param -> set("XFunctional", Array<string>(1,string("1")));
                xc_param -> set("CFunctional", Array<string>(1,string("9")));
            } else
            if( xc_param -> get<string>("FunctionalName") == "PBE" ){
                xc_param -> set("XFunctional", Array<string>(1,string("101")));
                xc_param -> set("CFunctional", Array<string>(1,string("130")));
            } else
            if( xc_param -> get<string>("FunctionalName") == "EXX" ){
                xc_param -> set("XFunctional", Array<string>(1,string("-12")));
            }
        }
    }

    vector<int> function_ids;
    vector<double> xc_portions;
    std::array<string,3> inputs;
    inputs[0] = "XCFunctional";
    inputs[1] = "XFunctional";
    inputs[2] = "CFunctional";
    for(int j = 0; j < 3; ++j){
        if (xc_param -> isParameter(inputs[j])){
            Array<string> xc_ids_and_portions = xc_param -> get< Array<string> >(inputs[j]);
            for(int i = 0; i < xc_ids_and_portions.size(); ++i){
                int id = -1; double portion = 0.0;
                split_xc_param(xc_ids_and_portions[i], id, portion);
                if(id == -1){
                    Verbose::all() << "XC functional name unrecognizable!:" << xc_param -> get< Array<string> >(inputs[j])[i] << std::endl;
                    throw std::invalid_argument("Unrecognizable Functional " + xc_param -> get< Array<string> >(inputs[j])[i]);
                }
                if(id == -30){
                    Verbose::single(Verbose::Simple) << "Correlation functional -30 (does nothing) is removed!" << std::endl;
                } else {
                    function_ids.push_back(id);
                    xc_portions.push_back(portion);
                }
            }
        }
    }
    if(function_ids.size() == 0){
        Verbose::all() << "No XC functional is set!" << std::endl;
        Verbose::all() << "XC parameter:" << std::endl << *xc_param << std::endl;
        Verbose::all() << "XC ids and portions:" << std::endl;
        for(int i = 0; i < x_ids_and_portions.size(); ++i){
            Verbose::all() << x_ids_and_portions[i] << std::endl;
        }
        for(int i = 0; i< c_ids_and_portions.size(); ++i){
            Verbose::all() << c_ids_and_portions[i] << std::endl;
        }
        throw std::invalid_argument("No XC functional is set!");
        return null;
    }

    bool is_cal_grad_using_density = false;
    if( xc_param -> isParameter("CalGradientUsingDensity") ){
        if( xc_param -> get<int>("CalGradientUsingDensity") != 0){
            is_cal_grad_using_density = true;
        }
    }
    int NGPU = xc_param -> get<int>("NGPU",0);
    double PD_cutoff = parameters -> sublist("ExchangeCorrelation").get<double>("PDcutoff", -1.0); // pair density cutoff

    bool autoexx = true;
    if(xc_param -> get<int>("AutoEXXPortion", 1) == 0){
        Verbose::single(Verbose::Simple) << "NOTICE: Not using predefined EXX portion!" << std::endl;
        autoexx = false;
    }

    vector<double> gau_hf_coeff, gau_hf_exponent;
    if( xc_param -> isParameter("ErfNGaussianPreset")){
        vector<string> erf_coeff = String_Util::Split_ws(parameters -> sublist("ExchangeCorrelation").get<string>("ErfNGaussianPreset"));
        int num_coeff = String_Util::stoi(erf_coeff[1]);
        vector<double> exponent(num_coeff), coeff(num_coeff);
        if(num_coeff == 2){
            double omega = String_Util::stod(erf_coeff[0]);
            exponent[0] = 0.075/0.4/0.4 * omega*omega; exponent[1] = 0.006/0.4/0.4 * omega*omega;
            coeff[0] = 0.27/0.4 * omega; coeff[1] = 0.18/0.4 * omega;
        } else {
            Verbose::all() << "ErfIntoNGaussian should be \"[float] 2\" instead of " << erf_coeff[0] << " " << num_coeff << std::endl;
            throw std::invalid_argument("ErfIntoNGaussian should be \"[float] 2\"");
        }
        Array<string> ngau_spec;
        for(int i = 0; i < num_coeff; ++i){
            ngau_spec.append(String_Util::to_string(coeff[i])+" "+String_Util::to_string(exponent[i]));
        }
        xc_param -> get< Array<string> >("GaussianPotential", ngau_spec);
    }
    if( xc_param -> isParameter("GaussianPotential")){
        Array<string> ngau_spec = xc_param -> get< Array<string> >("GaussianPotential");
        for(int i = 0; i < ngau_spec.size(); ++i){
            vector<string> gau_spec = String_Util::strSplit(ngau_spec[i], " ");
            //vector<string> gau_spec = String_Util::Split_ws(ngau_spec[i]);// XXX test and replace to this!
            gau_hf_coeff.push_back(String_Util::stod(gau_spec[0]));
            gau_hf_exponent.push_back(String_Util::stod(gau_spec[1]));
        }
    }
    RCP<Poisson_Solver> new_poisson = poisson;
    if(gau_hf_exponent.size() > 0){
        new_poisson = rcp( new Poisson_NGau_Solver(mesh, gau_hf_exponent, gau_hf_coeff,poisson->get_ngpus()) );
    }

    Array< RCP<Compute_XC> > xc_classes;
    double extra_exx_portion = 0.0;
    for(int i = 0; i < function_ids.size(); ++i){
        if(function_ids[i] < 0){
            xc_classes.append( rcp(new KLI(mesh, function_ids[i], new_poisson, PD_cutoff)) );
        } else if(function_ids[i] < 10001){
            xc_classes.append(rcp(new Libxc(mesh, function_ids[i], NGPU)));
            if(autoexx){
                RCP<XC_State> exx_test = rcp(new XC_State(function_ids[i]));
                if(exx_test -> get_EXX_portion() > 0.0){
                    extra_exx_portion += exx_test -> get_EXX_portion() * xc_portions[i];
                }
            }
        } else if(function_ids[i] == 10478){
            xc_classes.append(rcp(new XC_LC_wPBE_NGau(mesh, function_ids[i], gau_hf_exponent, gau_hf_coeff,NGPU)));
            if(autoexx){
                xc_classes.append(rcp(new KLI(mesh, -12, new_poisson, PD_cutoff)));
                xc_portions.push_back(1.0);
            }
            xc_classes.append(rcp(new Libxc(mesh, 130, NGPU)));
            xc_portions.push_back(1.0);
        }
    }
    if(extra_exx_portion > 0.0){
        xc_classes.append(rcp(new KLI(mesh, -12, new_poisson, PD_cutoff)));
        xc_portions.push_back(extra_exx_portion);
    }

    return rcp(new Exchange_Correlation(mesh, xc_classes, xc_portions, !is_cal_grad_using_density));
}
