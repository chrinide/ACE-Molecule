#pragma once
#include "Teuchos_RCP.hpp"
#include "Epetra_CrsMatrix.h"
#include "Epetra_FECrsMatrix.h"
#include "Epetra_MultiVector.h"

#include "../Core/Pseudo-potential/Nuclear_Potential.hpp"
#include "../Basis/Kinetic_Matrix.hpp"
#include "../Core/Pseudo-potential/Nuclear_Potential_Matrix.hpp"
//#include "AnasaziConfigDefs.hpp"
#include "AnasaziOperator.hpp"
#include "AnasaziTypes.hpp"
#include "../Util/Time_Measure.hpp"
/**
 * @brief Contains almost every hamiltonian matrix information, except XC and hartree potential.
 * @author Sungwoo Kang
 * @date 2016.1
 **/
class Core_Hamiltonian_Matrix{
    public:
        /**
         * @brief Constructor.
         * @param mesh Basis information.
         * @param atoms Atoms information.
         * @param parameters Input parameters.
         * @param is_epetra Decides Epetra_CrsMatrix support.
         * @param store_crs_matrix if true, store crsmatrix itself during the calculation
         **/
        Core_Hamiltonian_Matrix(
            Teuchos::RCP<const Basis> mesh,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Teuchos::ParameterList> parameters,
            bool is_epetra = true,
            bool store_crs_matrix = false
        );

        /**
         * @brief Returns kinetic matrix class.
         * @return Kinetic matrix class.
         **/
        Teuchos::RCP<Kinetic_Matrix> get_kinetic_matrix();
        /**
         * @brief Returns nuclear potential matrix class.
         * @return Nuclear potential matrix class.
         **/
        Teuchos::RCP<Nuclear_Potential_Matrix> get_nuclear_potential_matrix();

        // From now, SCF calculation methods
        /**
         * @brief Update Hamiltonian, mainly nuclear potential part. Note that this does not call Nuclear_Potential::update.
         * @param occupations Occupations of the current state.
         * @param orbitals Orbitals of the current state.
         * @return Same as Nuclear_Potential::set_dynamic_nonlocal_pot_coeff_to_matrix().
         * @todo Probably set Core_Hamiltonian as friend and move this to protected
         *       to avoid direct call and confusion with Nuclear_Potential::update.
         **/
        int update(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals
        );

        // From now, matrix calculation methods
        /**
         * @brief Multiply method.
         * @param ispin Spin index.
         * @param input Vector to multiply
         * @param output Multiplied vector. Output.
         * @return Always 0.
         * @note I do not know that this function is tested by LJC.
         **/
        int multiply(
            int ispin,
            Teuchos::RCP<Epetra_MultiVector> input,
            Teuchos::RCP<Epetra_MultiVector> &output
        );
        int multiply(int ispin, const Anasazi::MultiVec<double>& X,
        Anasazi::MultiVec<double>& Y, bool is_gpu=false) const;
        /**
         * @brief Add method.
         * @details [Core Hamiltonian Matrix]  \f$\times\f$ [scalar] + [matrix].
         * @param ispin Spin index.
         * @param matrix Matrix to add.
         * @param output Resulting matrix. Output.
         * @param scalar Scaling factor for matrix.
         * @note KSW does not think that putting input variable after output variable is good...
         **/
        int add(
            int ispin,
            Teuchos::RCP<Epetra_CrsMatrix> matrix,
            Teuchos::RCP<Epetra_CrsMatrix> &output,
            double scalar
        );
        /**
         * @brief Add method.
         * @details [Core Hamiltonian Matrix]  \f$\times\f$ [scalar] + [matrix].
         * @param ispin Spin index.
         * @param matrix Matrix to add this matrix. Input and output.
         * @param scalar Scaling factor for matrix.
         * @note KSW does not think that putting input variable after output variable is good...
         **/
        int add(
            int ispin,
            Teuchos::RCP<Epetra_CrsMatrix> &matrix,
            double scalar
        );

        // From now, Epetra_CrsMatrix compatability
        /**
         * @brief Calculate Epetra_CrsGraph for kinetic matrix and this matrix.
         * @details Introducing this function will reduce matrix construction time, if matrix should be constructed more than once for a run.
         * @return Always 0.
         * @note Exists for Epetra_CrsMatrix support.
         **/
        int set_matrix_structure();
        /**
         * @brief Returns kinetic matrix as Epetra_CrsMatrix.
         * @param output Epetra_CrsMatrix form of kinetic matrix. Output. Need not to be initialized.
         * @return Always 0.
         * @note Exists for Epetra_CrsMatrix support.
         **/
        int get_kinetic_matrix( Teuchos::RCP<Epetra_CrsMatrix> &output );
        /**
         * @brief Returns nuclear potential matrix as Epetra_CrsMatrix.
         * @param ispin Spin index.
         * @param output Epetra_CrsMatrix form of nuclear potential matrix. Output. Need not to be initialized.
         * @return Always 0.
         * @note Exists for Epetra_CrsMatrix support.
         **/
        int get_nuclear_potential_matrix( int ispin, Teuchos::RCP<Epetra_CrsMatrix> &output );
        /**
         * @brief Returns this class information as Epetra_CrsMatrix.
         * @param ispin Spin index.
         * @param output Epetra_CrsMatrix form of this class. Output. Need not to be initialized.
         * @return Always 0.
         * @note Exists for Epetra_CrsMatrix support.
         **/
        int get(int ispin, Teuchos::RCP<Epetra_CrsMatrix> &output );

        Teuchos::RCP<Nuclear_Potential> nuclear_potential;

        Teuchos::RCP<const Basis> get_mesh();
    protected:

//        virtual int SetUseTranspose(bool UseTranspose) {return 0;};
//        virtual int ApplyInverse(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const{Warning::throw_error("Hamiltonian_Matrix::ApplyInverse"); return 0;} ;
//        virtual double NormInf() const{Warning::throw_error("Hamiltonian_Matrix::NormInf"); return 0;} ;
//        virtual const char * Label() const{Warning::throw_error("Hamiltonian_Matrix::Label"); return NULL; } ;
//        virtual bool UseTranspose() const{Warning::throw_error("Hamiltonian_Matrix::UseTranspose"); return false;} ;
//        virtual bool HasNormInf() const{Warning::throw_error("Hamiltonian_Matrix::HasNormInf"); return false;} ;
//        virtual const Epetra_Comm & Comm() const{return kinetic_matrix->Comm(); } ;
//        virtual const Epetra_Map & OperatorDomainMap() const {return kinetic_matrix->OperatorDomainMap();};
//        virtual const Epetra_Map & OperatorRangeMap() const {return kinetic_matrix->OperatorRangeMap();};
//
//        virtual int Apply(const Epetra_MultiVector& X, Epetra_MultiVector& Y) const{Warning::throw_error("Hamiltonian_Matrix::Apply"); return 0;}

    private:
        Teuchos::RCP<Kinetic_Matrix> kinetic_matrix;
        Teuchos::RCP<Nuclear_Potential_Matrix> np_matrix;

        Teuchos::RCP<const Basis> mesh;
        bool store_crs_matrix;
        Teuchos::RCP<Epetra_CrsMatrix> crs_core_hamiltonian =Teuchos::null;
        Teuchos::RCP<Epetra_CrsGraph> epetra_kinetic_matrix_graph;
        Teuchos::RCP<Epetra_CrsGraph> epetra_ch_matrix_graph;
        Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure() );
};
