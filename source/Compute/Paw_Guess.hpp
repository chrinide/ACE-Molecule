#pragma once
#include "Epetra_MultiVector.h"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Guess.hpp"
#include "../Basis/Basis.hpp"
#include "../Io/Atoms.hpp"
#include "../Core/Occupation/Occupation.hpp"

/**
 * @brief Construct initial atomic density and orbitals from PAW dataset.
 * @author Sungwoo Kang
 * @date 2015
 * @todo It seems that the guess for the many atom case is bad and needs LCAO-like treatment.
 **/
class Paw_Guess: public Guess{
    public:
        /**
         * @brief Constructor.
         * @param occupations occupations class
         * @param atoms Atom informations
         * @param paw_filenames PAW dataset paths.
         **/
        Paw_Guess( 
            Teuchos::Array< Teuchos::RCP<Occupation> > occupations, 
            Teuchos::RCP<const Atoms> atoms, 
            Teuchos::Array<std::string> paw_filenames 
        );

        /**
         * @brief Computation routine. Inherited from Compute.
         * @param mesh Basis informations.
         * @param states Update states here.
         * @return Always zero.
         * @note See one_atom_initialize, many_atom_initialize.
         **/
        int compute( 
            Teuchos::RCP<const Basis> mesh, 
            Teuchos::Array< Teuchos::RCP<State> > &states
        );

    protected:
        /**
         * @brief Actual computation routine for one atom.
         * @details Orbitals are ordered by its energy.
         * @param mesh Basis informations.
         * @param state Update states here.
         * @return Always zero.
         **/
        int one_atom_initialize( 
            Teuchos::RCP<const Basis> mesh, 
            Teuchos::RCP<State> &state 
        );
        /**
         * @brief Actual computation routine for many atom.
         * @details Orbitals are derived equally from total density.
         * @param mesh Basis informations.
         * @param state Update states here.
         * @return Always zero.
         **/
        int many_atom_initialize( 
            Teuchos::RCP<const Basis> mesh, 
            Teuchos::RCP<State> &state 
        );

        Teuchos::Array<std::string> paw_filenames;

        //Teuchos::RCP<Epetra_Vector> atomic_hartree;
};

