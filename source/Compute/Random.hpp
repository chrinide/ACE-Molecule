#pragma once
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Guess.hpp"
#include "../Basis/Basis.hpp"
#include "../Io/Atoms.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../State/State.hpp"

//class State;

class Random: public Guess{
    public:
        Random(
            Teuchos::Array< Teuchos::RCP<Occupation> > occupations, 
            Teuchos::RCP<const Atoms> atoms
        );
        int compute(
            Teuchos::RCP<const Basis> mesh, 
            Teuchos::Array< Teuchos::RCP<State> >& states
        );

    protected:
};

