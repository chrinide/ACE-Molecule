#pragma once
#include <vector>
#include <string>
#include "../State/Scf_State.hpp"

#include "Libxc.hpp"

/**
 * @brief Libxc library wrapper class for conventinoal exchange-correlation functionals.
 * @details Using xc library of "Libxc"(http://www.tddft.org/programs/octopus/wiki/index.php/Libxc)<br/>
 * The list of available functionals can be found in <a href="http://www.tddft.org/programs/octopus/wiki/index.php/Libxc_functionals">here</a> or the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a>.
 *  Meta-GGA functionals are not implemented.(Energy calculation without NLCC can be computed)<br/>
 *  Reference :  Miguel A. L. Marques, Micael J. T. Oliveira, and Tobias Burnus, <i>Comput. Phys. Commun.</i> <b>183</b>, 2272 (2012)
 */
class XC_Custom: public Libxc{
    public:
        /**
         * @brief Constructor of Exchange_Correlation class
         * @details Constructor for the input parameter "XCFunctional" is exist.
         */
        /*
        XC_Custom(
            Teuchos::RCP<const Basis> mesh, ///< Basis data
            int function_id, ///< XCFunctional id, only 478 allowed
            std::vector<double> alpha, std::vector<double> beta,
            int NGPU = 0
        );
        */

        XC_Custom(Teuchos::RCP<const Basis> mesh, int function_id, int NGPU = 0): Libxc(mesh, function_id, NGPU){};

        virtual void compute_exc(
            Teuchos::RCP<XC_State> &xc_info
        );
        virtual void compute_vxc(
            Teuchos::RCP<XC_State> &xc_info
        );
        virtual void compute_kernel(
                Teuchos::RCP<XC_State> &xc_info
        );

        virtual int get_functional_kind()=0;
        virtual std::string get_info_string(bool verbose = true)=0;
    protected:
        void xc_gga_ex(void *dummy, int np, double *rho, double *sigma, double *ex);
        void xc_gga_vx(void *dummy, int np, double *rho, double *sigma, double *vrho, double *vsigma);
        virtual void xc_gga_x(void *dummy, int np, double *rho, double *sigma,
            double *ex, double *vrho, double *vsigma,
            double *v2rho2, double *v2rhosigma, double *v2sigma2,
            double *v3rho3, double *v3rho2sigma, double *v3rhosigma2, double *v3sigma3) = 0;

        void xc_lda_ex(void *dummy, int np, double *rho, double *ex);
        void xc_lda_vx(void *dummy, int np, double *rho, double *vrho);
        void xc_lda_fx(void *dummy, int np, double *rho, double *v2rho2);
        virtual void xc_lda_x(void *dummy, int np, double*rho, double *ex, double *vrho, double *v2rho2, double *v3rho3) = 0;

        int spin_size;
};
