#pragma once
#include "Epetra_CrsMatrix.h"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_MultiVector.h"
//#include "Teuchos_ArrayRCP.hpp"

#include "../Core/Occupation/Occupation.hpp"
#include "Compute_Interface.hpp"
#include "../State/State.hpp"
#include "../Io/Atoms.hpp"
#include "../Util/Time_Measure.hpp"

//class Atoms;

class Guess: public Compute_Interface{
    public:
        //int set_occupations(Array<RCP<Occupation> > occupations);
        //int set_atoms(RCP<Atoms> atoms);
        //int set_map(RCP<Epetra_Map> map);
//        virtual Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > compute(Teuchos::Array<Occupation* > occupations, Teuchos::RCP<Epetra_MultiVector> density)=0;
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Guess(){};
    protected:
        int initialize_states(
            Teuchos::RCP<const Basis> mesh, 
            Teuchos::Array< Teuchos::RCP<State> >& states
        );
        Teuchos::Array< Teuchos::RCP<Occupation> > occupations;
        //RCP<Basis> mesh;
        Teuchos::RCP<const Atoms> atoms;
        double num_electron;
        double spin_multiplicity;
};
