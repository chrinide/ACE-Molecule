#pragma once
#include <vector>
#include <array>
#include "Epetra_Map.h"
#include "Epetra_Vector.h"
#include "Teuchos_RCP.hpp"
#include "Epetra_CrsMatrix.h"
#include "Teuchos_ParameterList.hpp"
//#include "../../Util/Linear_Interpolation.hpp"
#include "../Util/Verbose.hpp"
#include "Poisson.hpp"
#include "Core_Hamiltonian.hpp"
#include "Exchange_Correlation.hpp"

class CI_Occupation{
    public:
        //routines for calculating element of CI matrix
        double cal_sorted_CI_element(int index1, int index2);
        double cal_CI_element_value1(std::vector<int> beta_occ1, std::vector<int> alpha_occ2, std::vector<int> same_alpha_occ,std::vector<int> diff_alpha_occ, std::vector<int> diff_alpha_index,std::vector<int> same_alpha_index);
        double cal_CI_element_value1_hcore(std::vector<int> beta_occ1, std::vector<int> alpha_occ2, std::vector<int> same_alpha_occ,std::vector<int> diff_alpha_occ, std::vector<int> diff_alpha_index,std::vector<int> same_alpha_index);
        double cal_CI_element_value1_exchange(std::vector<int> beta_occ1, std::vector<int> alpha_occ2, std::vector<int> same_alpha_occ,std::vector<int> diff_alpha_occ, std::vector<int> diff_alpha_index,std::vector<int> same_alpha_index);
        double cal_CI_element_value1_repulsion(std::vector<int> beta_occ1, std::vector<int> alpha_occ2, std::vector<int> same_alpha_occ,std::vector<int> diff_alpha_occ, std::vector<int> diff_alpha_index,std::vector<int> same_alpha_index);
        double cal_CI_element_value2(std::vector<int> alpha_occ1 ,std::vector<int> beta_occ2, std::vector<int> same_beta_occ,std::vector<int> diff_beta_occ,std::vector<int> diff_beta_index ,std::vector<int> same_beta_index);
        double cal_CI_element_value2_hcore(std::vector<int> alpha_occ1 ,std::vector<int> beta_occ2, std::vector<int> same_beta_occ,std::vector<int> diff_beta_occ,std::vector<int> diff_beta_index ,std::vector<int> same_beta_index);
        double cal_CI_element_value2_exchange(std::vector<int> alpha_occ1 ,std::vector<int> beta_occ2, std::vector<int> same_beta_occ,std::vector<int> diff_beta_occ,std::vector<int> diff_beta_index ,std::vector<int> same_beta_index);
        double cal_CI_element_value2_repulsion(std::vector<int> alpha_occ1 ,std::vector<int> beta_occ2, std::vector<int> same_beta_occ,std::vector<int> diff_beta_occ,std::vector<int> diff_beta_index ,std::vector<int> same_beta_index);
        double cal_CI_element_value3(std::vector<int> alpha_occ2,std::vector<int> beta_occ2,std::vector<int> diff_alpha_occ,std::vector<int> diff_beta_occ,std::vector<int> diff_alpha_index,std::vector<int> diff_beta_index);
        double cal_CI_element_value4(std::vector<int> alpha_occ2 ,std::vector<int> diff_alpha_occ,std::vector<int> diff_alpha_index);
        double cal_CI_element_value5(std::vector<int> beta_occ2 ,std::vector<int> diff_beta_occ,std::vector<int> diff_beta_index);


        //print electronic configuration of given index
        void print_configuration(int configuration);
        void print_configuration(std::vector<int> occupation1, std::vector<int> occupation2, int alpha1, int beta1);

        //delete unnecessary memory
        void reduce_memory();

        //return number of occupied orbitals
        int get_num_occupied_orbitals();

        //return total number of orbitals
        int get_num_orbitals();

        //return string number of excitation
        int get_string_number(int excitation);

        //calculate pure Hcore
        void cal_pure_Hcore();

        //calculate two centered integral (ijkl)
        void cal_two_centered_integ(
                Teuchos::Array< Teuchos::RCP< const Epetra_MultiVector> > eigenvectors,
                Teuchos::RCP<Poisson_Solver> poisson_solver,
                Teuchos::RCP<const Basis> mesh );

        //calculate Hcore (Hij)
        void cal_CI_Hcore(
                Teuchos::Array< Teuchos::RCP< const Epetra_MultiVector> > eigenvectors,
                Teuchos::Array< std::vector<double> > orbital_energies,
                Teuchos::RCP<const Basis> mesh,
                bool is_DFT,
                Teuchos::Array< Teuchos::RCP<State> >& states,
                Teuchos::RCP<Exchange_Correlation> exchange_correlation,
                Teuchos::RCP<Poisson_Solver> poisson_solver,
                Teuchos::RCP<Core_Hamiltonian> core_hamiltonian);

        //modify H_core using two_centered_integral
        void cal_CI_modified_Hcore();

        //these routines are used to calculate which configuration is same and which configuration is different
        void check_same_diff_occupation(std::vector<int> alpha_occu1,std::vector<int> beta_occu1,std::vector<int> alpha_occu2,std::vector<int> beta_occu2, std::vector<int>& same_alpha_occ, std::vector<int>& diff_alpha_occ, std::vector<int>& same_beta_occ,std::vector<int>& diff_beta_occ, std::vector<int>& diff_alpha_index,std::vector<int>& diff_beta_index, std::vector<int>& same_alpha_index,std::vector<int>& same_beta_index);
        void check_same_diff_occupation(int index1, int index2, std::vector<int>& alpha_occu1, std::vector<int>& beta_occu1,std::vector<int>& alpha_occu2,std::vector<int>& beta_occu2, std::vector<int>& same_alpha_occ, std::vector<int>& diff_alpha_occ, std::vector<int>& same_beta_occ,std::vector<int>& diff_beta_occ, std::vector<int>& diff_alpha_index,std::vector<int>& diff_beta_index, std::vector<int>& same_alpha_index,std::vector<int>& same_beta_index);
        void check_same_diff_occupation(int index1, int index2, int& alpha1, int& alpha2, int& beta1, int& beta2, int& alpha_address1, int& alpha_address2, int& beta_address1, int& beta_address2, std::vector<int>& alpha_occu1, std::vector<int>& beta_occu1,std::vector<int>& alpha_occu2,std::vector<int>& beta_occu2, std::vector<int>& same_alpha_occ, std::vector<int>& diff_alpha_occ, std::vector<int>& same_beta_occ,std::vector<int>& diff_beta_occ, std::vector<int>& diff_alpha_index,std::vector<int>& diff_beta_index, std::vector<int>& same_alpha_index,std::vector<int>& same_beta_index);

        //get sign of given configuration
        int get_sign(int excitation, int address);

        //these routine are calculate H0 value of given alpha and beta occupation
        double cal_H0_value(std::vector<int> alpha_occupation,std::vector<int> beta_occupation);
        void cal_H0(Teuchos::RCP<Epetra_Vector> H0);
        double cal_H0_value_hcore(std::vector<int> alpha_occupation,std::vector<int> beta_occupation);
        double cal_H0_value_exchange(std::vector<int> alpha_occupation,std::vector<int> beta_occupation);
        double cal_H0_value_repulsion(std::vector<int> alpha_occupation,std::vector<int> beta_occupation);

        //return check address
        std::vector< std::vector<int> > get_check_address();

        //find index of pair spin
        int find_pair_spin(int index);

        //get address of given occupation
        int get_address(int excitation,std::vector<int> occupation);

        //get total number of string (number of total configuration)
        int get_total_num_string();

        //calculate energy of Hcore
        virtual std::vector<double> cal_Hcore_energy(Teuchos::RCP< Epetra_MultiVector > CI_coefficient, int num_eigenvalues) = 0;

        //calculate density of CI eigenvectors
        virtual Teuchos::RCP<Epetra_Vector> cal_CI_density(
                Teuchos::RCP<const Basis> mesh,
                Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_of_orbital,
                Teuchos::RCP<Epetra_Vector> CI_coefficient,
                Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital) = 0;

        //calculate sigma
        virtual Teuchos::Array< Teuchos::RCP<Epetra_Vector> > cal_sigma(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > CI_coefficient) = 0;

        //make single and triple excitation list
        virtual void make_ST_list(std::vector< std::vector< std::vector<int> > > & ST_list) = 0;
        virtual std::vector< std::vector<int> > make_GD_list() = 0;
        virtual int get_ST_size() = 0;
        virtual int get_GD_size() = 0;

        //check cartegory of given configuration
        virtual void check_category(int coordinate, int* alpha, int* beta, int* alpha_address, int* beta_address) = 0; //input : coordinate in total_H, output : cartegory in which alpha string and beta string
        virtual void check_category(int coordinate, int* alpha, int* beta, int* alpha_address, int* beta_address, std::vector<int>& alpha_occ, std::vector<int>& beta_occ); //input : coordinate in total_H, output : cartegory in which alpha string and beta string
        virtual void address_to_excitation(int* excitation, int* ret_address, int address) = 0;
    protected:
        int distinguish_excitation(std::vector<int> occupation);
        std::vector< std::vector<int> > cal_Y(int nelectrons, int norbitals, int exciation);
        std::vector< std::vector<int> > cal_W(int nelectrons, int norbitals, int exciation);
        //sort configuration
        void Sorting(std::vector<int>& occupation);

        //calculate gamma value
        int cal_gamma(int p, int q, std::vector<int> occupation);
        int Eij(std::vector<int> occupation, int p, int q);

        // cal sign of single excitation
        int cal_single_sign(int address);

        //make possible occupations of given number of orbitals and electrons
        std::vector< std::vector<int> > make_occupation(int num_electrons, int num_orbitals, int start_orbitals);

        //make possible occupations of given number of orbitals and electrons with restrictions
        //max_excitation is 1 for CIS and 2 for CISD.
        void make_restricted_occupation(int max_excitation);

        std::vector<int> spin_check_address;
        std::vector< std::vector< std::vector< std::vector<int> > > > total_occupation;

        //two centered integral (ijkl)
        std::vector< std::vector< std::vector< std::vector<double> > > > two_centered_integ;

        //Hcore
        std::vector< std::vector<double> > Hcore;

        //pure Hcore
        std::vector< std::vector<double> > pure_Hcore;

        //number of orbitals
        int num_orbitals;

        //number of electrons
        int num_electrons;

        //number of alpha electrons
        int num_alpha_electrons;

        //number of beta electrons
        int num_beta_electrons;

        //string list
        std::array<int, 3> string_number;

        //sign list
        std::vector<int> sign_list;
        std::vector< std::vector<int> > check_address;

        //total number of string which as same as total number of configuration
        int total_num_string;

        //parameters
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        std::vector< std::vector<int> > SY1;
        std::vector< std::vector<int> > DY1;
};
