#pragma once
#include "../Basis/Basis.hpp"
#include "../State/State.hpp"
#include "../Util/Time_Measure.hpp"
#include "../Util/Verbose.hpp"
//class State;
//class Basis;

/**
This is pure abstract class to be inherited by all Modes 
Every Compute Mode should interit this class.
*/
class Compute{
    public:
        /**
         * @brief compute method for every Compute-driven classes.
         * @param mesh Basis for the system.
         * @param states Array of states. Computed new state will be appended. Input and Output.
         **/
        virtual int compute( Teuchos::RCP<const Basis> mesh, Teuchos::Array< Teuchos::RCP<State> >& states)=0;

        Compute(){internal_timer = Teuchos::rcp(new Time_Measure());}
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Compute(){};

        Teuchos::RCP<Time_Measure> internal_timer;
        Verbose::Tag verb_level = Verbose::Simple;
};
