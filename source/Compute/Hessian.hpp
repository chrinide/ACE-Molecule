#pragma once
#include <vector>

namespace Hessian{
        std::vector< std::vector<double> > initial_guess(
            std::vector<double> force, 
            double tolerance
        );

        std::vector< std::vector<double> > update_hessian(
            std::vector< std::vector<double> > old_Hessian, 
            std::vector<double> old_force, 
            std::vector<double> force, 
            std::vector<double> old_position, 
            std::vector<double> position, 
            double tolerance
        );

        std::vector< std::vector<double> > bfgs(
            std::vector< std::vector<double> > old_Hessian, 
            std::vector<double> old_Force, 
            std::vector<double> new_Force, 
            std::vector<double> old_Position, 
            std::vector<double> new_Position, 
            double tolerance
        );

};
