#include "Sp.hpp"

Sp::Sp(RCP<const Basis> mesh, RCP<const Atoms> atoms, RCP<ParameterList> parameters){
    this->mesh = mesh;
    this->parameters = parameters;
    int guess_type = parameters->sublist("Sp").get<int>("");
    if(guess_type==0){
        guess= Create_Compute::Create_Core_Hamiltonian(mesh, atoms, parameters);
    }
    else if(guess_type==1){
        guess = Create_Compute::Create_Atomic_Density(atoms,"sublist",parameters);
    }
    else{
        exit(-1);
//        guess = Create_Core_Hamiltnoian("",parameter);
    }

    if(guess_type==0){
        scf = rcp(new Scf(mesh, parameters, rcp_dynamic_cast<Core_Hamiltonian>(guess) ) );    
    }
    else{
        scf = rcp(new Scf(mesh, parameters) );    
    }
}

int Sp::compute(RCP<const Basis> mesh,Array<RCP<State> >& states){
    Array<RCP<State> > tmp_states;
    
    tmp_states.append( rcp( new State( *states[states.size()-1] )  ) );
    
    guess->compute(mesh, tmp_states);
    scf->compute(mesh, tmp_states);

    states.append(rcp(new State(*tmp_states[tmp_states.size()-1]) ) );
    
    return 0;
}

