#pragma once
#include <vector>

#include "Epetra_MultiVector.h"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Compute_Interface.hpp"
#include "Core_Hamiltonian.hpp"
#include "Poisson.hpp"
#include "Exchange_Correlation.hpp"
#include "CI_Occupation.hpp"
#include "../Util/Time_Measure.hpp"

class CISD : public Compute_Interface{
    public:
        CISD(
                Teuchos::RCP<const Basis> mesh,
                Teuchos::RCP<Teuchos::ParameterList> parameters,
                Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvector,
                Teuchos::Array< std::vector<double > > orbital_energies,
                Teuchos::RCP<Poisson_Solver> poisson_solver = Teuchos::null,
                Teuchos::RCP<Exchange_Correlation> exchange_correlation = Teuchos::null);
        int compute(Teuchos::RCP<const Basis> mesh, Teuchos::Array<Teuchos::RCP<State> >& states);
        ~CISD();
    protected:
        //initializer
        void initializer();

        //Davidson diagonalize
        std::vector<double> Davidson();

        //Gram shmidt
        void Gram_Schmidt(Teuchos::RCP<Epetra_Vector> new_vector);

        //diagonlize
        void diagonalize(Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > Dmatrix, double* eval, double** evec, int matrix_size);

        //print eigenvalue and eigenvectors
        void print_eval(Teuchos::Array< Teuchos::RCP<Epetra_Vector > > eigenvectors);

        //CI_occupation
        Teuchos::RCP<CI_Occupation> cisd_occupation;

        //mesh
        Teuchos::RCP<const Basis> mesh;

        //Core_hamiltonian
        Teuchos::RCP<Core_Hamiltonian> core_hamiltonian;

        //Poisson solver
        Teuchos::RCP<Poisson_Solver> poisson_solver;

        //Exchange correlation
        Teuchos::RCP<Exchange_Correlation> exchange_correlation;

        //parameters
        Teuchos::RCP<Teuchos::ParameterList> parameters;

        //two centered integral ijkl
        //double**** two_centered_integ;

        //core hamiltonian <i|H|j>
        //double** Hcore;

        //CI energy
        double CI_energy;

        //Number of CI eigenvalues to be calculated
        int num_eigenvalues;

        //Block size to be used for diagonalization
        int num_blocks;

        //max iteration
        int max_iteration;

        //number of total string. It is same as number of configuration
        int total_num_string;

        //It will be used to gather distributed coefficient
        //double* combine_coefficient;

        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > sigma;
        Teuchos::RCP<Epetra_Vector> H0;

        //Orbital information
        Teuchos::Array< Teuchos::RCP<  const Epetra_MultiVector> > orbital;

        //CI coefficient (distributed acros MPI processor)
        Teuchos::Array< Teuchos::RCP<  Epetra_Vector> > CI_coefficient;

        //Eigenvectors obtained by diagonalization of CI matrix
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > eigenvectors;

        //Davison correction
        std::vector<double> Davidson_correction(Teuchos::Array< Teuchos::RCP<Epetra_Vector > > eigenvectors, std::vector<double> _CI_energy);

        //number of electrons
        int num_electrons;
        Teuchos::RCP<Time_Measure> timer;
};
