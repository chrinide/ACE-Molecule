#pragma once
#include "Epetra_MultiVector.h"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_LAPACK.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"
#ifdef HAVE_MPI
#include "Epetra_MpiComm.h"
#include "mpi.h"
#else
#include "Epetra_SerialComm.h"
#endif

#include "CIS_Occupation.hpp"
#include "Core_Hamiltonian.hpp"
#include "Compute_Interface.hpp"
#include "Poisson.hpp"
#include "Exchange_Correlation.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../Util/Value_Coef.hpp"
#include "../Util/Time_Measure.hpp"

class CIS: public Compute_Interface{
    public:
        CIS(Teuchos::RCP<const Basis> mesh,
                Teuchos::RCP<Teuchos::ParameterList> parameters,
                Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > eigenvector,
                Teuchos::Array< std::vector<double> > orbital_energies,
                Teuchos::RCP<Poisson_Solver> poisson_solver=Teuchos::null,
                Teuchos::RCP<Exchange_Correlation> exchange_correlation = Teuchos::null);
        int compute(Teuchos::RCP<const Basis> mesh, Teuchos::Array< Teuchos::RCP<State> >& states);
        ~CIS();
    protected:
        //initializer
        void initializer();

        //Davidson diagonalization
        std::vector<double> Davidson();

        //Gram schmidt
        void Gram_Schmidt(Teuchos::RCP<Epetra_Vector> new_vector);
        //void diagonalize(Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > Dmatrix, double* eval, double** evec, int matrix_size);

        //print eigenvalue and eigenvectors
        void print_eval(Teuchos::Array< Teuchos::RCP<Epetra_Vector > > eigenvectors);

        //CIS occupation
        Teuchos::RCP<CI_Occupation> cis_occupation;

        //mesh
        Teuchos::RCP<const Basis> mesh;

        //core hamiltonian
        Teuchos::RCP<Core_Hamiltonian> core_hamiltonian;

        //poisson solver
        Teuchos::RCP<Poisson_Solver> poisson_solver;

        //exchange correlation
        Teuchos::RCP<Exchange_Correlation> exchange_correlation;

        //parameters
        Teuchos::RCP<Teuchos::ParameterList> parameters;

        //two centered integral (ijkl)
        //double**** two_centered_integ;

        //Hcore
        //double** Hcore;

        //CI energy
        double CI_energy;

        //number of eigenvalues
        int num_eigenvalues;

        //block size for diagonlization of CI matrix
        int num_blocks;

        //max iteration
        int max_iteration;

        //total number of string which is same as number of configuration
        int total_num_string;

        //collected coefficient of CI eigenvector
        //double* combine_coefficient;

        //sigma
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > sigma;

        //Diagonal of core hamiltonian
        Teuchos::RCP<Epetra_Vector> H0;

        //orbitals
        Teuchos::Array< Teuchos::RCP< const Epetra_MultiVector> > orbital;

        //distributed CI coefficient
        Teuchos::Array< Teuchos::RCP< Epetra_Vector> > CI_coefficient;

        //diagonalizing full matrix with MPI parallelization
        std::vector<double> Full_matrix_diagonalize();
        std::vector<double> direct_matrix_diagonalize();
        void direct_diagonalize(Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > Dmatrix, double* eval, double** evec, int matrix_size);
        Teuchos::RCP<Diagonalize> diagonalize;

        //make CI matrix
        Teuchos::RCP<Epetra_CrsMatrix> make_CI_matrix_crs(Teuchos::RCP<Epetra_Vector> H0);
        Teuchos::RCP< Teuchos::SerialDenseMatrix<int,double> > make_CI_matrix_dense(Teuchos::RCP<Epetra_Vector> H0);

        //number of electrons
        int num_electrons;

        Teuchos::RCP<Time_Measure> timer;
};
