#pragma once 
#include "Scf.hpp"
#include "Create_Compute.hpp"
#include "Compute_Interface.hpp"

class Sp: public Compute_Interface{
    public:
        Sp(RCP<const Basis> mesh, RCP<Atoms> atoms, RCP<ParameterList> parameters);
        int compute(RCP<const Basis> mesh,Array<RCP<State> >& states);

    protected:
        RCP<Guess> guess;
        RCP<Scf> scf;
        RCP<const Atoms> atoms;

};

