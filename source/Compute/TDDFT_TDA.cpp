#include "TDDFT_TDA.hpp"
#include <cmath>
#include <complex>

#include "Teuchos_Time.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include "Epetra_CrsMatrix.h"

#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Util/Two_e_integral.hpp"
#include "../Util/Value_Coef.hpp"
#include "../Util/Parallel_Manager.hpp"
#include "../Util/Parallel_Util.hpp"
#include "../Util/String_Util.hpp"

#define DEBUG (1)// 0 truns off fill-time output

using std::abs;
using std::vector;
using std::endl;
using std::string;
using std::setw;
using std::setiosflags;
using std::ios;
using Teuchos::SerialDenseMatrix;
using Teuchos::Time;
using Teuchos::TimeMonitor;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

TDDFT_TDA::TDDFT_TDA(RCP<const Basis> mesh, RCP<Teuchos::ParameterList> parameters)
:TDDFT(mesh, parameters){
    if(parameters -> sublist("TDDFT").get<string>("TheoryLevel") != "TammDancoff"){
        string errmsg = "TDDFT::compute - Class and TheoryLevel mismatch! (TDDFT_TDA with " + this -> theorylevel + ")!";
        Verbose::all() << errmsg << std::endl;
        throw std::logic_error(errmsg);
    }
    this -> type = "TDA";
}

int TDDFT_TDA::compute(RCP<const Basis> mesh, Array< RCP<State> >& states){
    this -> state = rcp(new State( *states[states.size()-1]));
    state->timer->start("TDDFT::compute");

    initialize_orbital_numbers(state);

    int matrix_dimension = this -> mat_dim_scale * (index_of_HOMO[0] - this -> iroot) * (number_of_orbitals[0] - index_of_HOMO[0]);

    int num_excitation = parameters->sublist("TDDFT").get<int>("NumberOfStates", matrix_dimension);
    Array< RCP<Diagonalize> > diagonalizes = this -> _compute(state, matrix_dimension);

    for(int s = 0; s < diagonalizes.size(); ++s){
        RCP<Diagonalize> diagonalize = diagonalizes[s];
        auto energies = diagonalize->get_eigenvalues();

        if(num_excitation!=energies.size()){
            Verbose::single(Verbose::Simple) << "Original NumberOfStates(" << num_excitation<< ") is changed to " << energies.size() << "." << std::endl;
            num_excitation = energies.size();
        }

        std::vector<double> excitation_energies(energies);
        excitation_energies.resize(num_excitation);
        // Verify excitation energies
        double min_element = *std::min(excitation_energies.begin(), excitation_energies.end());
        if(min_element < 0.0){
            Verbose::single(Verbose::Simple) << "TDDFT::compute - WARNING: For whatever reason, excitation energy " << min_element << " is negative.\n";
            Verbose::single(Verbose::Simple) << "TDDFT::compute - WARNING: This should not happen.\n";
        }
        // end

        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        if(s == 0){
            Verbose::single(Verbose::Simple) << " Singlet excitation information:" << std::endl;
        } else {
            Verbose::single(Verbose::Simple) << " Triplet excitation information:" << std::endl;
        }
        this -> postprocessing(excitation_energies, diagonalize -> get_eigenvectors(), state, num_excitation);
        Verbose::single(Verbose::Simple) <<   "#---------------------------------------------------------------------\n";
    }
    Parallel_Manager::info().all_barrier();

    state->timer->end("TDDFT::compute");
    states.push_back(state);

    return 0;
}

void TDDFT_TDA::fill_TD_matrix_restricted(
        vector<double> orbital_energies,
        double Hartree_contrib, vector<double> xc_contrib,
        int matrix_index_c, int matrix_index_r,
        Array< RCP<Epetra_CrsMatrix> >& sparse_matrix
){
    const int num_virt_orb = this -> number_of_orbitals[0] - this -> index_of_HOMO[0];
    int i = matrix_index_c / num_virt_orb + this -> iroot;
    int a = matrix_index_c % num_virt_orb + this -> index_of_HOMO[0];
    int j = matrix_index_r / num_virt_orb + this -> iroot;
    int b = matrix_index_r % num_virt_orb + this -> index_of_HOMO[0];
    double eps_ai = orbital_energies[a] - orbital_energies[i];

    // *2 because both up and down spin should be considered.
    double element_tmp = 2.0 * (Hartree_contrib + xc_contrib[0]);
    if(i==j and a==b){
        element_tmp += eps_ai;
    }
    if(std::fabs(element_tmp) > this -> cutoff){
        if(matrix_index_c!=matrix_index_r){
            if(sparse_matrix[0]->MyGlobalRow(matrix_index_c) ){
                sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
            }
            if(sparse_matrix[0]->MyGlobalRow(matrix_index_r)) {
                sparse_matrix[0]->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
            }
        }
        else{
            sparse_matrix[0]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
        }
    }
    if(sparse_matrix.size() > 1){
        throw std::invalid_argument("Not implemented");
        double element_tmp = 2.0 * xc_contrib[1];
        if(i==j and a==b){
            element_tmp += eps_ai;
        }
        if(std::fabs(element_tmp) > this -> cutoff){
            if(matrix_index_c!=matrix_index_r){
                if(sparse_matrix[1]->MyGlobalRow(matrix_index_c) ){
                    sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_r);
                }
                if(sparse_matrix[1]->MyGlobalRow(matrix_index_r)) {
                    sparse_matrix[1]->InsertGlobalValues(matrix_index_r,1,&element_tmp,&matrix_index_c);
                }
            }
            else{
                sparse_matrix[1]->InsertGlobalValues(matrix_index_c,1,&element_tmp,&matrix_index_c);
            }
        }
    }
}

void TDDFT_TDA::Kernel_TDHF_X(Teuchos::RCP<State> state, Teuchos::Array< Teuchos::RCP<Epetra_CrsMatrix> >& sparse_matrix, bool kernel_is_not_hybrid, bool add_delta_term, double scale/* = 1.0*/){
    auto map = mesh->get_map();
    Array< RCP<Epetra_MultiVector> > orbitals;
    for(int i_spin=0; i_spin<state->get_occupations().size(); ++i_spin){
        orbitals.push_back(rcp(new Epetra_MultiVector(*map, state->get_orbitals()[i_spin]->NumVectors())));
        orbitals[i_spin]->Update(1.0, *state->get_orbitals()[i_spin], 0.0);
        Value_Coef::Value_Coef(mesh, orbitals[i_spin], false, true, orbitals[i_spin]);
    }

    // For HF
    RCP<Epetra_Vector> Kib = rcp(new Epetra_Vector(*map)); //occ-vir
    // end

    // For LDA & GGA
    RCP<Epetra_Vector> Kia = rcp(new Epetra_Vector(*map)); //occ-vir
    RCP<Epetra_Vector> Kij = rcp(new Epetra_Vector(*map)); //occ-occ

    RCP<Epetra_Vector> ia_coeff = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> jb_coeff = rcp(new Epetra_Vector(*map));
    // end

    double EXX_portion = scale;
    // V_GKS - V_KS + a0 * V_HF
    Array< RCP<Epetra_Vector> > potential_diff_wo_EXX;
    if(add_delta_term){
        potential_diff_wo_EXX = calculate_v_diff(state, true, kernel_is_not_hybrid);
        if(potential_diff_wo_EXX.size()==2){
            Verbose::all() << "TDDFT::Kernel_KSCI, something wrong ! EXX_portion : " << EXX_portion << "    potential_diff_wo_EXX.size() : " << potential_diff_wo_EXX.size() << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    // Hartree<-fill td matrix restricted()
    // -sum_k(ak|kb)
    int number_of_virtual_orbitals = number_of_orbitals[0] - index_of_HOMO[0];
    /*
       int index_i = i * number_of_virtual_orbitals;
       int index_j = j * number_of_virtual_orbitals;
       int index_a = a - index_of_HOMO[0];
       int index_b = b - index_of_HOMO[0];
       matrix_index_c = index_i + index_a = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
       matrix_index_r = index_j + index_b = j * number_of_virtual_orbitals + b - index_of_HOMO[0];
     */

    int matrix_index_c = 0;
    int matrix_index_r = 0;

    int delta_matrix_index_c = 0;
    int delta_matrix_index_r = 0;


/***** 171114, get iajb is waste of time
    Verbose::single(Verbose::Detail) << "TDDFT::get_iajb" << std::endl;
#if DEBUG == 1
    internal_timer -> start("get_iajb");
#endif
    std::vector<double> iajb = get_iajb(orbitals[0], true);
#if DEBUG == 1
    internal_timer -> end("get_iajb");
    Verbose::single(Verbose::Detail) << "TDDFT_TDA::Kernel_TDHF_X get_iajb time = " << internal_timer -> get_elapsed_time("get_iajb",-1) << std::endl;
#endif
    int index_iajb = 0;
****/
    if(add_delta_term){
        for(int i=iroot; i<index_of_HOMO[0]; ++i){
#if DEBUG == 1
            internal_timer -> start("i-loop");
#endif
            for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
                delta_matrix_index_r = delta_matrix_index_c;

                Kia->PutScalar(0.0);
                Two_e_integral::compute_Kji(mesh, orbitals[0]->operator()(i), orbitals[0]->operator()(a), exchange_poisson_solver, Kia);

                for(int j=i; j<index_of_HOMO[0]; ++j){
                    for(int b = (j==i)? a: index_of_HOMO[0]; b<number_of_orbitals[0]; ++b){
                        //double element_iajb = iajb[index_iajb];

                        // Delta v_ab =  -sum_k(ak|kb) + potential_difference_norm(a,b)

                        if(i==j){
                            double element_iajb = Two_e_integral::compute_two_center_integral(mesh, Kia, orbitals[0]->operator()(b), orbitals[0]->operator()(j));
                            double element_tmp_vdiff = potential_difference_norm(state->get_orbitals()[0],a,b,potential_diff_wo_EXX[0]);
                            for(int s = 0; s < sparse_matrix.size(); ++s){
                                // A
                                sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r,1,&element_tmp_vdiff,&delta_matrix_index_c);
                                if(delta_matrix_index_r!=delta_matrix_index_c){
                                    sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c,1,&element_tmp_vdiff,&delta_matrix_index_r);
                                }

                                for(int k=0;k<index_of_HOMO[0];++k){
                                    //ia,jb = ka,kb , (bj|ia) = (bk|ka)
                                    // A
                                    double element_HFx = - element_iajb * scale;
                                    int delta_matrix_index_c2 = k * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                    int delta_matrix_index_r2 = k * number_of_virtual_orbitals + b - index_of_HOMO[0];
                                    sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_r2,1,&element_HFx,&delta_matrix_index_c2);
                                    if(delta_matrix_index_r2!=delta_matrix_index_c2){
                                        sparse_matrix[s]->InsertGlobalValues(delta_matrix_index_c2,1,&element_HFx,&delta_matrix_index_r2);
                                    }
                                }
                                //-sum_k(ak|kb) end
                            }
                        }
                        ++delta_matrix_index_r;
                        //++index_iajb;
                    }
                }
                ++delta_matrix_index_c;
            }
#if DEBUG == 1
            internal_timer -> end("i-loop");
            Verbose::single(Verbose::Detail) << "TDDFT_TDA::Kernel_TDHF_X index for Delta v + B i = " << i << " time = "<< internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
#endif
        }

        // end
    }
    // exchange - A
    // sum_k(jk|ki)
    /*
       int index_i = i * number_of_virtual_orbitals;
       int index_j = j * number_of_virtual_orbitals;
       int index_a = a - index_of_HOMO[0];
       int index_b = b - index_of_HOMO[0];
       matrix_index_c = index_i + index_a = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
       matrix_index_r = index_j + index_b = j * number_of_virtual_orbitals + b - index_of_HOMO[0];
     */

    for(int i=0; i<index_of_HOMO[0]; ++i){
#if DEBUG == 1
        internal_timer -> start("i-loop");
#endif
        int index_i = i * number_of_virtual_orbitals;

        for(int j=i; j<index_of_HOMO[0]; ++j){
            int index_j = j * number_of_virtual_orbitals;

            Kij->PutScalar(0.0);
            Two_e_integral::compute_Kji(mesh, orbitals[0]->operator()(i), orbitals[0]->operator()(j), exchange_poisson_solver, Kij);

            for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
                int index_a = a - index_of_HOMO[0];
                matrix_index_c = index_i + index_a;

                for(int b = (j==i)? a: index_of_HOMO[0]; b<number_of_orbitals[0]; ++b){
                    int index_b = b - index_of_HOMO[0];
                    matrix_index_c = index_i + index_a;
                    matrix_index_r = index_j + index_b;
                    double element_ijab = Two_e_integral::compute_two_center_integral(mesh, Kij, orbitals[0]->operator()(a), orbitals[0]->operator()(b));

                    double element_tmp1 = - element_ijab * scale;
                    for(int s = 0; s < sparse_matrix.size(); ++s){
                        if(std::fabs(element_tmp1) > this -> cutoff){
                            if(matrix_index_c!=matrix_index_r){
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_tmp1,&matrix_index_r);
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_tmp1,&matrix_index_c);
                            }
                            else{
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_tmp1,&matrix_index_c);
                            }
                        }//for(s)

                        if(add_delta_term){
                            if(a==b){
                                // Delta v_ij =  sum_k(jk|ki) - potential_difference_norm(j,i)
                                // delta_ij가 없을 때 or delta_ij가 있을 때 ij 같음.
                                if(!this -> is_tdhf_kli or i == j){
                                    double element_tmp_m_vdiff = - potential_difference_norm(state->get_orbitals()[0],j,i,potential_diff_wo_EXX[0]);
                                    // A
                                    sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_tmp_m_vdiff,&matrix_index_c);
                                    if(matrix_index_r!=matrix_index_c){
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_tmp_m_vdiff,&matrix_index_r);
                                    }
                                }
                                // sum_k(jk|ki)
                                double element_ijij = Two_e_integral::compute_two_center_integral(mesh, Kij, orbitals[0]->operator()(i), orbitals[0]->operator()(j)) * scale;
                                //matrix_index_c = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                //matrix_index_r = j * number_of_virtual_orbitals + b - index_of_HOMO[0];

                                //ia,jb = ia,ia , (ij|ji)
                                //A
                                matrix_index_c = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                matrix_index_r = matrix_index_c;
                                sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_ijij,&matrix_index_r);

                                if(i!=j){
                                    //ia,jb = ja,ja , (ij|ji)
                                    //A
                                    matrix_index_c = j * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                    matrix_index_r = matrix_index_c;
                                    sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_ijij,&matrix_index_r);
                                }

                                if(!this -> is_tdhf_kli or i == j){
                                    for(int k=j+1;k<index_of_HOMO[0];++k){
                                        double element_kiij = Two_e_integral::compute_two_center_integral(mesh, Kij, orbitals[0]->operator()(i), orbitals[0]->operator()(k)) * scale ;
                                        //ia,jb = ka,jb, (ki|ij)
                                        //A
                                        matrix_index_c = k * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                        matrix_index_r = j * number_of_virtual_orbitals + b - index_of_HOMO[0];
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_kiij,&matrix_index_r);
                                        if(matrix_index_r!=matrix_index_c){
                                            sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_kiij,&matrix_index_c);
                                        }
                                    }//for(k)
                                }//if(!is_tdhf_kli or i==j)
                                if(!this -> is_tdhf_kli and i!=j){
                                    for(int k=i+1;k<index_of_HOMO[0];k++){
                                        double element_kjij = Two_e_integral::compute_two_center_integral(mesh, Kij, orbitals[0]->operator()(j), orbitals[0]->operator()(k)) * scale;
                                        //ia,jb = ia,kb, (kj|ij)
                                        //A
                                        matrix_index_c = i * number_of_virtual_orbitals + a - index_of_HOMO[0];
                                        matrix_index_r = k * number_of_virtual_orbitals + b - index_of_HOMO[0];
                                        sparse_matrix[s]->InsertGlobalValues(matrix_index_c,1,&element_kjij,&matrix_index_r);
                                        if(matrix_index_r!=matrix_index_c){
                                            sparse_matrix[s]->InsertGlobalValues(matrix_index_r,1,&element_kjij,&matrix_index_c);
                                        }
                                    }//for(k)
                                }//if(!is_tdhf_kli and i!=j)
                            }//if(a==b)
                        }//if(add_delta_term)
                    }//for(s)
                }//for(b)
            }//for(j)
        }//for(a)
#if DEBUG == 1
        internal_timer -> end("i-loop");
        Verbose::single(Verbose::Detail) << "TDDFT_TDA::Kernel_TDHF_X index for rest exchange  i = " << i << " time = " << internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
#endif
    }
    // end
    return;
}

std::vector< std::vector<double> > TDDFT_TDA::gather_z_vector(RCP<Epetra_MultiVector> input, std::vector<double> eigvals){
    int num_vec = input -> NumVectors();
    int total_size = input->Map().NumGlobalElements();
    double ** recv = new double*[num_vec];
    for(int i = 0; i < num_vec; ++i){
        recv[i] = new double[total_size];
    }
    Parallel_Util::group_allgather_multivector(input, recv);

    vector<double> eps_diff(total_size);
    int matrix_index_r = 0;
    for(int i=0; i<index_of_HOMO[0]; ++i){
        for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
            eps_diff[matrix_index_r] = sqrt(eigvals[a] - eigvals[i]);
            ++matrix_index_r;
        }
    }
    for(int j = 0; j < num_vec; ++j){
        for(int i = 0; i < total_size; ++i){
            recv[j][i] /= eps_diff[i];
        }
        double norm = 0.0;
        for(int i = 0; i < total_size; ++i){
            norm += recv[j][i]*recv[j][i];
        }
        for(int i = 0; i < total_size; ++i){
            recv[j][i] /= sqrt(norm);
        }
    }

    vector< vector<double> > output(num_vec);
    for(int i = 0; i < num_vec; ++i){
        output[i] = vector<double>(recv[i], recv[i]+total_size);
    }
    for(int i = 0; i < num_vec; ++i){
        delete[] recv[i];
    }
    delete[] recv;
    return output;
}
