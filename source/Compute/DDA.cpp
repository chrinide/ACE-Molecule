#include "DDA.hpp"
#include "../Util/Parallel_Util.hpp"

#include <iostream>
#include <array>

#include "../Core/Mixing/Create_Mixing.hpp"
#include "../Core/Mixing/Mixing.hpp"
#include "../Util/Linear_Interpolation.hpp"
#include "../Util/Read_Upf.hpp"
#include "../Util/Read_Output.hpp"
#include "../Util/HB.hpp"
#include "../Util/String_Util.hpp"
#include "../Io/Periodic_table.hpp"
#include "../Io/Io.hpp"
#include "Teuchos_BLAS.hpp"
#include "mkl.h"
#include "../Util/Faddeeva.hpp"
//#include "mkl_lapacke.h"
//#include "mkl_lapacke_config.h."

using std::ios;
using std::abs;
using std::vector;
using std::string;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
using std::complex;
using std::array;

DDA::DDA(Teuchos::RCP<const Basis> mesh, Teuchos::RCP<Teuchos::ParameterList> parameters){
    this->parameters = parameters;
    this->mesh = mesh;
    /*
       string unit = parameters -> sublist("DDA").isParameter("Unit", "um");
       if(unit = "um")
       unit_conversion = 1E-6;
       else if(unit = "nm")
       unit_conversion = 1E-6;
       else{
       std::cout << "Unit is wrong" << std::endl;
       exit(-1);
       }
       */
}

DDA::~DDA(){

}

int DDA::compute(Teuchos::RCP<const Basis> mesh, Teuchos::Array< Teuchos::RCP<State> >& states){
    Verbose::single() << "Start DDA" << std::endl;
    Teuchos::RCP<const Atoms> atoms = states[states.size()-1]->get_atoms();
    RCP<Epetra_MultiVector> atomic_density = rcp(new Epetra_MultiVector(*mesh->get_map(), atoms->get_size()));;
    RCP<Epetra_MultiVector> HB_charge_density = rcp(new Epetra_MultiVector(*mesh->get_map(), atoms->get_size()));;

    double scaleup = parameters -> sublist("DDA").get<double>("ScaleUp", 1.0);
    std::vector<double> scale_up;
    std::vector<double> V_eff;
    std::vector<double> V_atomic;
    std::vector<double> coverage_eff;
    std::vector<double> coverage_atomic;
    //calculate atomic volume of each atom using Hirshfel method
    if(scaleup<0){
        read_atomic_density(atomic_density, atoms);
        HB::HB(states[states.size()-1]->get_density(), atomic_density, HB_charge_density );
        //mesh->write_cube("HB",atoms, HB_charge_density);
        //mesh->write_cube("Atomic",atoms, atomic_density);
        Verbose::single() << "HB is end" << std::endl;
        const double* scaling = mesh->get_scaling();
        calculate_volume2(atomic_density, V_atomic, coverage_atomic);
        calculate_volume2(HB_charge_density, V_eff, coverage_eff);
        Verbose::single() << std::endl << "Atom\tEffective Volume\tAtomic Volume\t Ratio\tCoverage eff\tCoverage atom" << std::endl;
        for(int i=0; i<V_atomic.size(); i++){
            Verbose::single() << i << "\t" << V_eff[i] << "\t" <<V_atomic[i] << "\t" << V_eff[i]/V_atomic[i] << "\t" << coverage_eff[i] << "\t" << coverage_atomic[i] << std::endl;
            scale_up.push_back(V_eff[i]/V_atomic[i]);
        }
        double sum = 0.0;
        double sum2 = 0.0;
        for(int i=0; i<V_eff.size(); i++)
            sum+=V_eff[i];
        for(int i=0; i<V_eff.size(); i++)
            sum2+=V_atomic[i];
        Verbose::single() << "Total density : " << sum << " " << sum*scaling[0]*scaling[1]*scaling[2] << std::endl;
        Verbose::single() << "Atomic Total density : " << sum2 << " " << sum2*scaling[0]*scaling[1]*scaling[2] << std::endl;
    }
    else{
        for(int i=0; i<atoms->get_size(); i++)
            scale_up.push_back(scaleup);
    }


    //read polarizability
    std::vector<std::vector<std::complex<float> > > polarizability;
    if(parameters -> sublist("DDA").isParameter("Gaussian") ){
        Array<std::string> gaussian_filename = parameters -> sublist("DDA").get<Array<std::string> >("Gaussian");
        if(gaussian_filename.size()!=atoms->get_num_types()){
            std::cout << "wrong number of Gaussian files " << gaussian_filename.size() << " " << atoms->get_size() << std::endl;
            exit(-1);
        }
        energy.resize(gaussian_filename.size());
        wavelength.resize(gaussian_filename.size());
        oscillating_strength.resize(gaussian_filename.size());
        for(int i=0; i<gaussian_filename.size(); i++){
            Read::Gaussian::read_gaussian(gaussian_filename[i], energy[i], wavelength[i], oscillating_strength[i]);
            Verbose::single() << i << " th atom gaussian information" << std::endl;
            Verbose::single() << "energy\twavelength\toscillarting_strength" << std::endl;
            double max_wavelength = parameters -> sublist("DDA").get<double>("GaussianMaxWavelength", 10000.0);
            double min_wavelength = parameters -> sublist("DDA").get<double>("GaussianMinWavelength", 0.0);
            int j=0;
            while(j<energy[i].size()){

                if(wavelength[i][j]>max_wavelength or wavelength[i][j]<min_wavelength){
                    Verbose::single() << wavelength[i][j] << std::endl;
                    wavelength[i].erase (wavelength[i].begin()+j);
                    energy[i].erase (energy[i].begin()+j);
                    oscillating_strength[i].erase (oscillating_strength[i].begin()+j);
                }
                else
                    j++;
            }
            for(int j=0; j<energy[i].size(); j++)
                if(oscillating_strength[i][j]>0.0001)
                    Verbose::single() << energy[i][j] << "\t" << wavelength[i][j] << "\t" << oscillating_strength[i][j] << std::endl;

        }
        if(energy[0].size()!=wavelength[0].size() and wavelength[0].size()!=oscillating_strength[0].size()){
            std::cout << "Error in result of reading gaussian " << energy[0].size() << " " << wavelength[0].size() << " " << oscillating_strength[0].size() << std::endl;
            exit(-1);
        }
        //double unit_conversion = 1/(5.29E-9)/(5.29E-9)/(5.29E-9);
        double unit_conversion = 1E24;
        calculate_polarizability(atoms, scale_up, polarizability, wavelength_range);
    }
    else if(parameters -> sublist("DDA").isParameter("ACE") ){
        Array<std::string> ace_filename = parameters -> sublist("DDA").get<Array<std::string> >("ACE");
        if(ace_filename.size()!=atoms->get_num_types()){
            std::cout << "wrong number of ACE files " << ace_filename.size() << " " << atoms->get_size() << std::endl;
            exit(-1);
        }
        energy.resize(ace_filename.size());
        wavelength.resize(ace_filename.size());
        oscillating_strength.resize(ace_filename.size());
        for(int i=0; i<ace_filename.size(); i++){
            Read::ACE::read_ACE(ace_filename[i], energy[i], wavelength[i], oscillating_strength[i]);
            Verbose::single() << i << " th atom gaussian information" << std::endl;
            Verbose::single() << "energy\twavelength\toscillarting_strength" << std::endl;
            for(int j=0; j<energy[i].size(); j++)
                Verbose::single() << energy[i][j] << "\t" << wavelength[i][j] << "\t" << oscillating_strength[i][j] << std::endl;

        }
        if(energy[0].size()!=wavelength[0].size() and wavelength[0].size()!=oscillating_strength[0].size()){
            std::cout << "Error in result of reading ACE " << energy[0].size() << " " << wavelength[0].size() << " " << oscillating_strength[0].size() << std::endl;
            exit(-1);
        }
        //double unit_conversion = 1/(5.29E-9)/(5.29E-9)/(5.29E-9);
        double unit_conversion = 1E24;
        calculate_polarizability(atoms, scale_up, polarizability, wavelength_range);
    }
    else if(parameters -> sublist("DDA").isParameter("Txt")){

        Array<std::string> txt_filename = parameters -> sublist("DDA").get<Array<std::string> >("Txt");
        if(txt_filename.size()!=atoms->get_num_types()){
            std::cout << "wrong number of Txt files " << txt_filename.size() << " " << atoms->get_size() << std::endl;
            exit(-1);
        }
        polarizability.resize(atoms->get_size());
        for(int i=0; i<txt_filename.size(); i++){
            std::vector<std::complex<double> > tmp_polarizability;
            wavelength_range.clear();
            Read::Txt::read_txt(txt_filename[i], wavelength_range, tmp_polarizability);
            std::vector<std::complex<float> > tmp_polarizability2(tmp_polarizability.begin(), tmp_polarizability.end());
            for(int j=0; j<tmp_polarizability2.size(); j++)
                tmp_polarizability2[j]/=(float)1E24;

            for(int iatom=0;iatom<atoms->get_size();iatom++){
                if(atoms->get_atomic_numbers()[iatom] == atoms->get_atom_types()[i]){
                    polarizability[iatom].resize(tmp_polarizability2.size());
                    for(int k=0; k<tmp_polarizability2.size(); k++){
                        //polarizability[iatom][k] = tmp_polarizability2[k]*scale_up[iatom];
                    }
                }
            }

        }
    }
    else if(parameters -> sublist("DDA").isParameter("AssignPolarizability")){
        string xyz_filename = parameters -> sublist("DDA").get<std::string>("AssignPolarizability");
        RCP<Atoms> ref_atoms = Teuchos::rcp(new Atoms() );
        RCP<Io> io = Teuchos::rcp(new Io());
        ref_atoms->operator=(io->read_atoms(xyz_filename,"xyz"));
        calculate_polarizability(atoms, ref_atoms, polarizability, wavelength_range);
    }
    else{
        std::cout << "Wrong input polarizability file " << std::endl;
        exit(-1);
    }
    Parallel_Manager::info().all_barrier();
    //actual computation (solve self consistent screening eqaution using several method)
    std::vector<std::vector< std::array< std::complex<float> ,9> > > alpha_cluster;
    double st = MPI_Wtime();
    string method = parameters -> sublist("DDA").get<string>("SolvingMethod","LinearEquation");
    if(method=="Iteration"){
        iterate(atoms, polarizability, alpha_cluster);
    }
    else if(method=="LinearEquation"){
        linearequation(atoms, polarizability, alpha_cluster);
    }
    else{
        std::cout << "WRONG SOLVING METHOD : " << method << std::endl;
        exit(-1);
    }
    double end = MPI_Wtime();
    Verbose::single() << "ITERATE TIME : " << end-st << " s" << std::endl;

    //polarizability
    write_polarizability(atoms, alpha_cluster, polarizability);
    write_dipole(atoms, alpha_cluster, polarizability);

    //exit(-1);
    return 0;
}

int DDA::read_atomic_density(Teuchos::RCP<Epetra_MultiVector>& atomic_density, Teuchos::RCP<const Atoms> atoms){
    Verbose::single()<< "#-------------------------------------------------------- DDA::read atomic density" << std::endl;
    Verbose::single()<< "Linear interpolation will be used " << std::endl;
    Array<string> initial_filenames;
    if(parameters -> sublist("DDA").isParameter("InitialFilePath") and parameters -> sublist("DDA").isParameter("InitialFileSuffix") ){
        vector<int> atomic_numbers = atoms -> get_atom_types();
        vector<string> atomic_symbols;
        for(int ia = 0; ia < atomic_numbers.size(); ++ia){
            string atom_symbol = Periodic_atom::periodic_table[atomic_numbers[ia]-1];
            transform(atom_symbol.begin()+1, atom_symbol.end(), atom_symbol.begin()+1, ::tolower);
            initial_filenames.push_back( parameters -> sublist("Guess").get<string>("InitialFilePath")+"/"+atom_symbol+parameters->sublist("Guess").get<string>("InitialFileSuffix") );
        }
        parameters -> sublist("DDA").set("InitialFilenames", initial_filenames);
        Verbose::single() << "InitialFilenames inferred are:" << std::endl;
        Verbose::single() << parameters -> sublist("DDA").get< Array<string> >("InitialFilenames") << std::endl;
    }
    else{
        initial_filenames = parameters->sublist("DDA").get<Array<string> >("InitialFilenames");
    }

    vector<std::array<double,3> > position=atoms->get_positions();
    atomic_density = rcp(new Epetra_MultiVector(*mesh->get_map(), position.size()));
    RCP<const Epetra_Map> map= mesh->get_map();
    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements= map->MyGlobalElements();
    double* density=new double [NumMyElements];
    for(int i=0;i<NumMyElements;i++){
        density[i]=0.0;
    }
    double total_Z =0.0;
    double xi,yi,zi,x,y,z,rr;
    const double** scaled_grid=mesh->get_scaled_grid();
    std::string upf_filename;
    std::vector<double> radial_mesh;
    std::vector<double> rho;

    for(int iatom=0;iatom<atoms->get_size();iatom++){
        double Zval_tmp = 0.0;
        int number_vps = 0, number_pao = 0, mesh_size_tmp = 0;  // dummy value
        bool core_correction = false;                           // dummy value

        // Find corresponding pseudopotential file
        for(int i=0;i<atoms->get_num_types();i++){
            if(atoms->get_atomic_numbers()[iatom] == atoms->get_atom_types()[i]){
                upf_filename=initial_filenames[i];
                break;
            }
        }
        // end

        // Get number of valence electron
        Read::Upf::read_header(upf_filename, &Zval_tmp, &number_vps, &number_pao, &mesh_size_tmp, &core_correction);
        total_Z+=Zval_tmp;
        //end

        // Read radial_mesh points
        radial_mesh.clear();
        radial_mesh = Read::Upf::read_upf(upf_filename, "PP_R");

        while(abs(radial_mesh[radial_mesh.size()-1]) < 1.0E-10){
            radial_mesh.pop_back();
        }

        radial_mesh.insert(radial_mesh.begin(),0.0);
        // end

        // Read density
        rho.clear();
        rho = Read::Upf::read_upf(upf_filename, "PP_RHOATOM");

        while(abs(rho[rho.size()-1]) < 1.0E-10){
            rho.pop_back();
        }

        // From the manual of Quantum Espresso, PP_RHOATOM in *.upf is
        // radial atomic (pseudo-)charge. This is 4*pi*r^2 times the true charge.
        // Therefore, this code saves the value divided by 4*pi*r^2.
        for(int i=0;i<rho.size();i++){
            rho[i]=rho[i]/(4*M_PI*radial_mesh[i+1]*radial_mesh[i+1]);
        }

        // linear extrapolation at r=0
        double tmp;
        tmp=(rho[1]-rho[0])/(radial_mesh[2]-radial_mesh[1])*(radial_mesh[0]-radial_mesh[1])+rho[0];
        rho.insert(rho.begin(),tmp);

        Verbose::single()<<"RHOATOM cutoff= " << radial_mesh[rho.size()-1]<<"\n";
#ifdef ACE_HAVE_OMP
#pragma omp parallel for private(xi,yi,zi,x,y,z,rr)
#endif
        for(int i=0;i<NumMyElements;i++){
            int i_x=0,i_y=0,i_z=0;
            mesh->decompose(MyGlobalElements[i],&i_x,&i_y,&i_z);

            // Position of the grid point
            xi=scaled_grid[0][i_x];
            yi=scaled_grid[1][i_y];
            zi=scaled_grid[2][i_z];

            // Distance between the grid point & atom
            x=xi-position[iatom][0];
            y=yi-position[iatom][1];
            z=zi-position[iatom][2];
            rr=sqrt(x*x+y*y+z*z);
            if(rr<=radial_mesh[rho.size()-1]){
                //double tmp = Spline_Interpolation::splint(radial_mesh, rho, y2, rho_size, rr);
                double tmp = Interpolation::Linear::linear_interpolate(rr, radial_mesh, rho);
                atomic_density->operator[](iatom)[i] = tmp;
            }
            else{
                atomic_density->operator[](iatom)[i] = 1.0E-60;
            }
        }

    }

    Verbose::single()<< "#--------------------------------------------------------------------------------" << std::endl;
    return 0;

}
int DDA::calculate_volume(Teuchos::RCP<Epetra_MultiVector> density, std::vector<double>& volume, std::vector<double>& coverage){
    int NumMyElements = density->Map().NumMyElements();
    double cutoff = parameters -> sublist("DDA").get<double>("CutOff",0.001);
    std::vector<double> volume_list;
    std::vector<double> volume_in_list;
    std::vector<double> volume_out_list;
    std::vector<double> combine_volume_in_list;
    std::vector<double> combine_volume_out_list;
    const double* scaling = mesh->get_scaling();
    for(int i=0; i<density->NumVectors(); i++){
        int count = 0.0;
        double volume_in = 0.0;
        double volume_out = 0.0;
        for(int j=0; j<NumMyElements; j++){
            if(density->operator[](i)[j]>cutoff){
                count++;
                volume_in+=density->operator[](i)[j];
            }
            volume_out+=density->operator[](i)[j];
        }
        double tmp_volume = count*scaling[0]*scaling[1]*scaling[2];
        volume_list.push_back(tmp_volume);
        volume_in_list.push_back(volume_in);
        volume_out_list.push_back(volume_out);
    }
    volume.resize(volume_list.size());
    combine_volume_in_list.resize(volume_list.size());
    combine_volume_out_list.resize(volume_list.size());
    Parallel_Util::group_sum(&volume_list[0], &volume[0], volume.size());
    Parallel_Util::group_sum(&volume_in_list[0], &combine_volume_in_list[0], volume_in_list.size());
    Parallel_Util::group_sum(&volume_out_list[0], &combine_volume_out_list[0], volume_out_list.size());
    for(int i=0; i<combine_volume_in_list.size();i++)
        coverage.push_back(combine_volume_in_list[i]/combine_volume_out_list[i]);
    return 0;
}

int DDA::calculate_volume2(Teuchos::RCP<Epetra_MultiVector> density, std::vector<double>& volume, std::vector<double>& coverage){
    int NumMyElements = density->Map().NumMyElements();
    double cutoff = parameters -> sublist("DDA").get<double>("CutOff",0.001);
    std::vector<double> volume_list;
    std::vector<double> volume_in_list;
    std::vector<double> volume_out_list;
    std::vector<double> combine_volume_in_list;
    std::vector<double> combine_volume_out_list;
    const double* scaling = mesh->get_scaling();
    for(int i=0; i<density->NumVectors(); i++){
        int count = 0.0;
        double volume_in = 0.0;
        double volume_out = 0.0;
        for(int j=0; j<NumMyElements; j++){
            if(density->operator[](i)[j]>cutoff){
                count++;
                volume_in+=density->operator[](i)[j];
            }
            volume_out+=density->operator[](i)[j];
        }
        double tmp_volume = count*scaling[0]*scaling[1]*scaling[2];
        //volume_list.push_back(tmp_volume);
        volume_list.push_back(volume_in);
        volume_in_list.push_back(volume_in);
        volume_out_list.push_back(volume_out);
    }
    volume.resize(volume_list.size());
    combine_volume_in_list.resize(volume_list.size());
    combine_volume_out_list.resize(volume_list.size());
    Parallel_Util::group_sum(&volume_list[0], &volume[0], volume.size());
    Parallel_Util::group_sum(&volume_in_list[0], &combine_volume_in_list[0], volume_in_list.size());
    Parallel_Util::group_sum(&volume_out_list[0], &combine_volume_out_list[0], volume_out_list.size());
    for(int i=0; i<combine_volume_in_list.size();i++)
        coverage.push_back(combine_volume_in_list[i]/combine_volume_out_list[i]);
    return 0;
}

int DDA::calculate_polarizability(Teuchos::RCP<const Atoms> atoms,
        std::vector<double> scale_up,
        std::vector<std::vector<std::complex<float> > >& polarizability,
        std::vector<double>& output_wavelength){
    double start_wavelength = parameters -> sublist("DDA").get<double>("MinWavelength",300.0);
    double end_wavelength = parameters -> sublist("DDA").get<double>("MaxWavelength",700.0);
    double resolution = parameters -> sublist("DDA").get<double>("DeltaWavelength",1.0);
    std::vector<double> damping ;
    calculate_damping(atoms, damping);
    double light_speed = 299792458.0;
    double electron_charge = 1.60217662E-19;
    double electron_mass = 9.10938356E-31;
    double pi = 3.14159265359;
    /*
       for(int i=0; i<wavelength.size(); i++)
       for(int j=0; j<wavelength[i].size();j++)
       wavelength[i][j]*=1.0E-9;
       */
    Verbose::single() << std::endl;
    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        double lamda = start_wavelength;
        std::vector<complex<float> > iatom_polarizability;
        std::vector<double> iatom_energy;
        std::vector<double> iatom_wavelength;
        std::vector<double> iatom_oscillating_strength;
        for(int i=0;i<atoms->get_num_types();i++){
            if(atoms->get_atomic_numbers()[iatom] == atoms->get_atom_types()[i]){
                iatom_energy = energy[i];
                iatom_wavelength = wavelength[i];
                iatom_oscillating_strength = oscillating_strength[i];
                break;
            }
        }
        while(lamda<end_wavelength){
            if(iatom==0)
                output_wavelength.push_back(lamda);
            double real = 0.0;
            double imag = 0.0;
            for(int i=0; i<iatom_energy.size();i++){
                double w1 = 2*pi*light_speed/iatom_wavelength[i]*1E9;
                double w2 = 2*pi*light_speed/lamda*1E9;
                //double w1 = 2*pi/iatom_wavelength[i]*1000;
                //double w2 = 2*pi/lamda*1000;
                double A = w1*w1-w2*w2;
                double B = w2*damping[iatom];
                double C = iatom_oscillating_strength[i];
                real+=A*C/(A*A+B*B);
                imag+=B*C/(A*A+B*B);
            }
            //exit(-1);
            real*=electron_charge*electron_charge/electron_mass*scale_up[iatom]*8.898E15;
            imag*=electron_charge*electron_charge/electron_mass*scale_up[iatom]*8.898E15;
            std::complex<float> tmp = std::complex<float> (real, imag);
            iatom_polarizability.push_back(tmp);
            lamda+=resolution;
        }
        double static_polarizability = 0.0;
        for(int i=0; i<iatom_energy.size();i++){
            double w1 = 2*pi*light_speed/iatom_wavelength[i]*1E9;
            //double w1 = 2*pi/iatom_wavelength[i]*1000;
            //double w2 = 2*pi/lamda*1000;
            double A = w1*w1;
            double C = iatom_oscillating_strength[i];
            static_polarizability+=C/A;
        }
        //exit(-1);
        static_polarizability*=8.898E15/(5.29E-9)/(5.29E-9)/(5.29E-9);
        static_polarizability*=electron_charge*electron_charge/electron_mass*scale_up[iatom];
        Verbose::single() << "Static polarizability of " << iatom << "th atom is " << static_polarizability << " (bohr^3)" << std::endl;

        polarizability.push_back(iatom_polarizability);
    }
    return 0;
}



void DDA::get_T_matrix(Teuchos::RCP<const Atoms> atoms,
        std::vector< std::vector<std::vector<std::complex<float> > > > & T_matrix,
        std::vector< std::vector< bool > > & bool_T_matrix,
        std::vector< std::vector<std::complex<float> > >& polarizability,
        int w,
        bool periodic){
    T_matrix.resize(atoms->get_size());
    bool_T_matrix.resize(atoms->get_size());
    int index = 0;
    vector<std::array<double,3> > positions = atoms->get_positions();
    float x,y,z,r,r5;
    complex<float> R_pp;
    complex<float> tmp1, tmp2, tmp3;
    complex<double> complex_i (0.0,1.0);
    float float_3 = (float)3.0;
    float sqrt_2 = (float)sqrt(2.0);
    float sqrt_pi = (float)3.14159265359;
    double cutoff_radius = parameters -> sublist("DDA").get<double>("CutOffRadius",10000.0);
#ifdef ACE_HAVE_OMP
//#pragma omp parallel for
#endif
    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        T_matrix[iatom].resize(atoms->get_size());
        bool_T_matrix[iatom].resize(atoms->get_size());
        for(int jatom=0; jatom<atoms->get_size(); jatom++){
            if(iatom==jatom){
                bool_T_matrix[iatom][jatom] = false;
                continue;
            }

            if(periodic){
                x = (positions[0][0]-positions[jatom][0])*0.529177249;
                y = (positions[0][1]-positions[jatom][1])*0.529177249;
                z = (positions[0][2]-positions[jatom][2])*0.529177249;
            }
            else{
                x = (positions[iatom][0]-positions[jatom][0])*0.529177249;
                y = (positions[iatom][1]-positions[jatom][1])*0.529177249;
                z = (positions[iatom][2]-positions[jatom][2])*0.529177249;
            }
            r = sqrt(x*x+y*y+z*z);

            //R_pp = sqrt(polarizability);
            if(r>cutoff_radius){
                bool_T_matrix[iatom][jatom] = false;
                continue;
            }
            else{
                bool_T_matrix[iatom][jatom] = true;
            }
            complex<float> R1 = (complex<float>) pow(polarizability[iatom][w]*(float)1E24*sqrt_2/sqrt_pi/float_3,1/3);
            complex<float> R2 = (complex<float>) pow(polarizability[jatom][w]*(float)1E24*sqrt_2/sqrt_pi/float_3,1/3);
            R_pp = sqrt(R1*R1+R2*R2);
            tmp3 = (complex<float>)(Faddeeva::erfi(-complex_i*((complex<double>)(r/R_pp)))*complex_i);
            tmp1 = tmp3-2/sqrt(sqrt_pi)*r/R_pp*exp(-r*r/R_pp/R_pp);
            tmp2 = -4/sqrt_pi/R_pp/R_pp/R_pp/r/r*exp(-r*r/R_pp/R_pp);
            //std::cout << iatom << " " << jatom << " " << tmp1 << " " << tmp2 << " " << tmp3 << " " << R_pp << " " << r/R_pp << " " << polarizability[iatom][w] << " " << polarizability[jatom][w] << std::endl;
            T_matrix[iatom][jatom].resize(9);
            r5 = r*r*r*r*r;
            //tmp1 = float(1.0);
            //tmp2 = float(0.0);
            //std::cout << iatom << " " << jatom << " " << tmp1 << " " << tmp2 << " " << tmp3 <<std::endl;
            T_matrix[iatom][jatom][0] = (3*x*x-r*r)/r5*tmp1+tmp2*x*x;
            T_matrix[iatom][jatom][1] = (3*x*y)/r5*tmp1+tmp2*x*y;
            T_matrix[iatom][jatom][2] = (3*x*z)/r5*tmp1+tmp2*x*z;
            T_matrix[iatom][jatom][3] = (3*y*x)/r5*tmp1+tmp2*y*x;
            T_matrix[iatom][jatom][4] = (3*y*y-r*r)/r5*tmp1+tmp2*y*y;
            T_matrix[iatom][jatom][5] = (3*y*z)/r5*tmp1+tmp2*y*z;
            T_matrix[iatom][jatom][6] = (3*z*x)/r5*tmp1+tmp2+x*z;
            T_matrix[iatom][jatom][7] = (3*z*y)/r5*tmp1+tmp2*z*y;
            T_matrix[iatom][jatom][8] = (3*z*z-r*r)/r5*tmp1+tmp2*z*z;
            index++;
            /*
            T_matrix[iatom][jatom][0] = (3*x*x-r*r)/r5;
            T_matrix[iatom][jatom][1] = (3*x*y)/r5;
            T_matrix[iatom][jatom][2] = (3*x*z)/r5;
            T_matrix[iatom][jatom][3] = (3*y*x)/r5;
            T_matrix[iatom][jatom][4] = (3*y*y-r*r)/r5;
            T_matrix[iatom][jatom][5] = (3*y*z)/r5;
            T_matrix[iatom][jatom][6] = (3*z*x)/r5;
            T_matrix[iatom][jatom][7] = (3*z*y)/r5;
            T_matrix[iatom][jatom][8] = (3*z*z-r*r)/r5;
            */

        }
    }
    return;
}
void DDA::get_T_matrix(Teuchos::RCP<const Atoms> atoms,
        std::vector< std::vector<std::vector<std::complex<float> > > > & T_matrix,
        std::vector< std::vector< bool > > & bool_T_matrix,
        bool periodic){
    T_matrix.resize(atoms->get_size());
    bool_T_matrix.resize(atoms->get_size());
    int index = 0;
    vector<std::array<double,3> > positions = atoms->get_positions();
    double x,y,z,r,r5, R_pp;
    double cutoff_radius = parameters -> sublist("DDA").get<double>("CutOffRadius",10000.0);

    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        T_matrix[iatom].resize(atoms->get_size());
        bool_T_matrix[iatom].resize(atoms->get_size());
        for(int jatom=0; jatom<atoms->get_size(); jatom++){
            if(iatom==jatom){
                bool_T_matrix[iatom][jatom] = false;
                continue;
            }
            if(periodic){
                x = (positions[0][0]-positions[jatom][0])*0.529177249;
                y = (positions[0][1]-positions[jatom][1])*0.529177249;
                z = (positions[0][2]-positions[jatom][2])*0.529177249;
            }
            else{
                x = (positions[iatom][0]-positions[jatom][0])*0.529177249;
                y = (positions[iatom][1]-positions[jatom][1])*0.529177249;
                z = (positions[iatom][2]-positions[jatom][2])*0.529177249;
            }
            r = sqrt(x*x+y*y+z*z);
            if(r>cutoff_radius){
                bool_T_matrix[iatom][jatom] = false;
                continue;
            }
            else{
                bool_T_matrix[iatom][jatom] = true;
            }

            T_matrix[iatom][jatom].resize(9);
            r5 = r*r*r*r*r;
            T_matrix[iatom][jatom][0] = (3*x*x-r*r)/r5;
            T_matrix[iatom][jatom][1] = (3*x*y)/r5;
            T_matrix[iatom][jatom][2] = (3*x*z)/r5;
            T_matrix[iatom][jatom][3] = (3*y*x)/r5;
            T_matrix[iatom][jatom][4] = (3*y*y-r*r)/r5;
            T_matrix[iatom][jatom][5] = (3*y*z)/r5;
            T_matrix[iatom][jatom][6] = (3*z*x)/r5;
            T_matrix[iatom][jatom][7] = (3*z*y)/r5;
            T_matrix[iatom][jatom][8] = (3*z*z-r*r)/r5;
            index++;

        }
    }
    return;
}

int DDA::iterate(Teuchos::RCP<const Atoms> atoms,
        std::vector<std::vector<std::complex<float> > > polarizability,
        std::vector<std::vector< std::array<complex<float> ,9> > >& alpha_cluster){
    Verbose::single() << "Start DDA iteartion " << std::endl;
    int max_iter = parameters -> sublist("DDA").get<int>("MaxIter", 1000);
    double convergence = parameters -> sublist("DDA").get<double>("Convergence", 0.001);
    std::vector<std::vector< std::vector<double> > > T_matrix;
    std::vector<std::vector< bool > > bool_T_matrix;
    std::vector<std::vector< std::vector< std::complex<float> > > > complex_T_matrix(T_matrix.size());
    get_T_matrix(atoms, complex_T_matrix, bool_T_matrix);
    alpha_cluster.resize(wavelength_range.size());

    complex<float> complex_one = complex<float>(1.0,0.0);
    complex<float> complex_minus_one = -1.0;
    complex<float> complex_zero = 0.0;
    Teuchos::RCP<Basis> tmp_mesh;
#ifdef ACE_HAVE_OMP
#pragma omp parallel for schedule(dynamic)
#endif
    for(int w=0; w<wavelength_range.size();w++){
        std::array<std::complex<float>,9> tmp_matrix;
        std::vector< std::array< complex<float>,9 > > tmp_alpha (atoms->get_size());
        std::vector<complex<float> > iatom_alpha(atoms->get_size());
        std::vector< array< complex<float>, 9 > > before_alpha(atoms->get_size());

        Teuchos::RCP<Teuchos::ParameterList> tmp_parameters = rcp(new Teuchos::ParameterList(*parameters));
        Teuchos::RCP<Mixing> mixing = Create_Mixing::Create_Mixing(Teuchos::sublist(tmp_parameters,"DDA"));
        Teuchos::Array< Teuchos::RCP<Scf_State> > dda_states;
        dda_states.append(rcp(new Scf_State()));
        double st = MPI_Wtime();
        alpha_cluster[w].resize(atoms->get_size());
        for(int iatom=0; iatom< atoms->get_size(); iatom++){
            //        alpha_imag[iatom] = new double[9];
            for(int i=0;i<atoms->get_num_types();i++){
                /*
                if(atoms->get_atomic_numbers()[iatom] == atoms->get_atom_types()[i]){
                    iatom_alpha[iatom] = polarizability[i][w]*(float)1E24;
                }
                */
                iatom_alpha[iatom] = polarizability[iatom][w]*(float)1E24;
            }
            if(true){
                alpha_cluster[w][iatom][0] = iatom_alpha[iatom];
                alpha_cluster[w][iatom][4] = iatom_alpha[iatom];
                alpha_cluster[w][iatom][8] = iatom_alpha[iatom];
            }
            else{
                alpha_cluster[w][iatom] = alpha_cluster[w-1][iatom];
            }
        }
        dda_states[0]->polarizability.resize(wavelength_range.size());
        dda_states[0]->polarizability[w] = alpha_cluster[w];

        for(int iatom=0; iatom < atoms->get_size(); iatom++)
            memcpy(before_alpha[iatom].data(), alpha_cluster[w][iatom].data(), sizeof(complex<float>)*9);
        int iter = 0;
        double conv = 0.0;

        bool converged = true;
        for(iter=0; iter<max_iter; iter++){

            for(int iatom=0; iatom < atoms->get_size(); iatom++){
                std::memset(tmp_matrix.data(), 0.0, sizeof(complex<float>)*9);
                for(int jatom=0; jatom< atoms->get_size(); jatom++){

                    if(iatom!=jatom and bool_T_matrix[iatom][jatom]==true){
                        //cblas_zgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 3,3,3, &complex_one,
                        //        T_matrix[iatom][jatom], 3, alpha_cluster[w][iatom],3, &complex_one, tmp_matrix, 3);
                        cblas_cgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 3,3,3, &complex_one,
                                &complex_T_matrix[iatom][jatom][0], 3, alpha_cluster[w][jatom].data(),3, &complex_one, tmp_matrix.data(), 3);

                    }

                }

                //complex<float> tmp_complex = iatom_alpha[iatom];
                //complex<float> tmp_complex = -1.0*iatom_alpha[iatom];
                cblas_cscal(9,&iatom_alpha[iatom],tmp_matrix.data(),1 );
                tmp_matrix[0]+=iatom_alpha[iatom];
                tmp_matrix[4]+=iatom_alpha[iatom];
                tmp_matrix[8]+=iatom_alpha[iatom];
                //std::memcpy(alpha_cluster[w][iatom].data(),tmp_matrix, sizeof(complex<float>)*9);
                std::memcpy(tmp_alpha[iatom].data(), tmp_matrix.data(), sizeof(complex<float>)*9);

            }
            for(int iatom=0; iatom < atoms->get_size(); iatom++)
                std::memcpy(alpha_cluster[w][iatom].data(),tmp_alpha[iatom].data(), sizeof(complex<float>)*9);
            array< complex<float>,9 > diff ;

            converged = true;
            for(int iatom=0; iatom < atoms->get_size(); iatom++){
                memcpy(diff.data(), alpha_cluster[w][iatom].data(), sizeof(complex<float>)*9);
                cblas_caxpy(9,&complex_minus_one, before_alpha[iatom].data(), 1,diff.data(), 1);
                complex<float> ref = (alpha_cluster[w][iatom][0] + alpha_cluster[w][iatom][4] + alpha_cluster[w][iatom][8])/((float) 3);
                float conv1 = cblas_scnrm2(9,diff.data(), 1);
                //conv = cblas_scnrm2(9,diff, 1)/std::norm(ref);
                conv = cblas_scnrm2(9,diff.data(), 1);
                //std::cout << "convergence : " << iter << " " << iatom << " " << conv << " " << std::endl;
                if(conv>convergence){
                    converged = false;
                    //        complex<float> sum = alpha_cluster[w][iatom][0] + alpha_cluster[w][iatom][4] + alpha_cluster[w][iatom][8];
                    //        std::cout << "convergence : " << iter << " " << iatom << " " << conv << std::endl;
                    break;
                }


            }

            if(converged){
                double end = MPI_Wtime();
                Verbose::single() << wavelength_range[w] << "nm is converged ( iteration : " << iter << ", conv : " << conv << " ) ";
                Verbose::single() << "Time : " << end-st << " s " << std::endl;
                break;
            }
            else{
                RCP<Scf_State> new_state = rcp(new Scf_State());
                new_state->polarizability.resize(wavelength_range.size());
                new_state->polarizability[w] = alpha_cluster[w];
                dda_states.append(new_state);
                if( dda_states.size() > mixing -> get_used_step_number() + 1 ){
                    //                    dda_states[dda_states.size()-mixing -> get_used_step_number() - 2] = Teuchos::null;
                    //dda_states.erase(dda_states.begin(), dda_states.end()-(dda_states.size()-mixing -> get_used_step_number() - 2));
                    //dda_states.erase(dda_states.begin());
                }

                mixing->update(tmp_mesh, dda_states, w);
                alpha_cluster[w] = dda_states[dda_states.size()-1]->polarizability[w];


                /*
                //dda_mixing(alpha_cluster[w], before_alpha);
                for(int iatom=0; iatom < atoms->get_size(); iatom++){
                complex<float> tmp = (alpha_cluster[w][iatom][0] + alpha_cluster[w][iatom][4]+ alpha_cluster[w][iatom][8])/((float) 3);
                complex<float> diff = tmp - iatom_alpha[iatom];
                complex<float> one_over_ten = 0.1;
                //    if(std::norm(tmp)/std::norm(iatom_alpha[iatom])>10.0)
                //            cblas_zscal(9,&one_over_ten,alpha_cluster[w][iatom].data(),1 );
                }
                */
                for(int iatom=0; iatom < atoms->get_size(); iatom++){
                    memcpy(before_alpha[iatom].data(), alpha_cluster[w][iatom].data(), sizeof(complex<float>)*9);
                }
                //std::cout.precision(5);
                //std::cout << "iter : " << iter << " " << conv << " " <<alpha_cluster[w][13][0] << " " << alpha_cluster[w][13][0] <<alpha_cluster[w][13][4] <<alpha_cluster[w][13][8] <<std::endl;
            }
        }
        //exit(-1);
        if(!converged){
            double end = MPI_Wtime();
            Verbose::single() << wavelength_range[w] << "nm is not converged ( iteration : " << iter << ", conv : " << conv << " ) ";
            Verbose::single() << "Time : " << end-st << " s " << std::endl;
            //            exit(-1);
        }

    }
    /*
       for(int w = 0; w < alpha_cluster.size(); w++){
       complex<float> sum = 0.0;
       for(int iatom=0; iatom<atoms->get_size(); iatom++){
       sum+=alpha_cluster[w][iatom][0]+alpha_cluster[w][iatom][4]+alpha_cluster[w][iatom][8];
       }
       sum/=3.0;
       std::cout << wavelength_range[w] << "\t" << sum << "\t" << sum.imag()/wavelength_range[w]*100000.0 <<std::endl;

       }
       */
    return 0;
}
void DDA::dda_mixing(std::vector< std::array< std::complex<float> ,9> > & after,
        std::vector< std::array< std::complex<float> ,9> > before){

    if(after.size()!=before.size()){
        std::cout << "Mixing:: after size != before size" << std::endl;
        exit(-1);
    }
    int mixing_method = parameters -> sublist("DDA").sublist("Mixing").get<int>("MixingMethod", 0);
    std::complex<float> mixing_parameter = parameters -> sublist("DDA").sublist("Mixing").get<float>("MixingParameter", 0.1);
    if(mixing_method==0){
        for(int iatom=0; iatom<after.size(); iatom++){
            for(int i=0; i<9; i++){
                after[iatom][i] = after[iatom][i]*mixing_parameter + before[iatom][i]*((float)1.0-mixing_parameter);
            }
        }
    }
    else{
        std::cout << "Wrong mixing method" << std::endl;
        exit(-1);
    }

}


int DDA::inverse(Teuchos::RCP<const Atoms> atoms,
        std::vector<std::vector<std::complex<float> > > polarizability,
        std::vector<std::vector< std::array<complex<float> ,9> > >& alpha_cluster){
    Verbose::single() << "Start DDA inverse " << std::endl;
    std::vector<std::complex<float> > matrix;
    make_matrix(atoms, matrix, polarizability);
    return 0;
}

int DDA::make_matrix(Teuchos::RCP<const Atoms> atoms,
        std::vector<std::complex<float> >& matrix,
        std::vector<std::vector<std::complex<float> > > polarizability){

    /*
       std::vector<std::vector< std::vector<double> > > T_matrix;
       get_T_matrix(atoms, T_matrix);
       std::vector<std::vector< std::vector< std::complex<float> > > > complex_T_matrix(T_matrix.size());
       for(int i=0; i<T_matrix.size(); i++){
       complex_T_matrix[i].resize(T_matrix[i].size());
       for(int j=0; j<T_matrix[i].size(); j++){
       if(T_matrix[i][j].size()>0){
       complex_T_matrix[i][j].resize(T_matrix[i][j].size());
       for(int k=0; k<T_matrix[i][j].size(); k++)
       complex_T_matrix[i][j][k] = T_matrix[i][j][k];
       }

       }
       }
       T_matrix.clear();
       matrix.resize(100);
       */
    return 0;
}

int DDA::linearequation(Teuchos::RCP<const Atoms> atoms,
        std::vector<std::vector<std::complex<float> > > polarizability,
        std::vector<std::vector< std::array<complex<float> ,9> > >& alpha_cluster){
    Verbose::single() << "Start DDA iteration by solving linear equation" << std::endl;
    int max_iter = parameters -> sublist("DDA").get<int>("MaxIter", 1000);
    double convergence = parameters -> sublist("DDA").get<double>("Convergence", 0.001);


    int num_atoms = atoms->get_size();
//    std::vector<complex<float> >  A (9*num_atoms*num_atoms, 0.0);

    int LWORK;
    std::vector<std::vector< std::vector<complex<float> > > > complex_T_matrix;
    std::vector<std::vector< bool > > bool_T_matrix;
    bool periodic = false;
    if(parameters->sublist("DDA").get<int>("PBC",0)>0)
        periodic = true;
    get_T_matrix(atoms, complex_T_matrix, bool_T_matrix, periodic);
    parameters->sublist("DDA").get<string>("ChargeDistribution","Gaussian");
    if (parameters->sublist("DDA").get<string>("ChargeDistribution","Gaussian")=="Gaussian")
        std::cout << "Gaussian charge distribution is used" << std::endl;
    else if (parameters->sublist("DDA").get<string>("ChargeDistribution","Gaussian")=="Point")
        std::cout << "Point charge distribution is used" << std::endl;
    else{
        std::cout << "Wrong Charge distribution!!!!" << std::endl;
        exit(-1);
    }

    std::vector<int> my_work;
    int MyPID = Parallel_Manager::info().get_mpi_rank();
    int NumProcs = Parallel_Manager::info().get_mpi_size();
    alpha_cluster.resize(wavelength_range.size());

    for(int w=0; w<alpha_cluster.size(); w++)
        alpha_cluster[w].resize(num_atoms);
//#ifdef ACE_HAVE_OMP
//#pragma omp parallel for schedule(dynamic) firstprivate(complex_T_matrix, bool_T_matrix)
//#pragma omp parallel for schedule(dynamic)
//#endif
    for(int w=0; w<wavelength_range.size(); w++){
        double st = MPI_Wtime();
        if(w%NumProcs!=MyPID){
            continue;
        }
        int* IPIV = new int[3*num_atoms];
        std::vector<complex<float> >  new_A (9*num_atoms*num_atoms, 0.0);
        if(parameters->sublist("DDA").get<string>("ChargeDistribution")=="Gaussian")
            get_T_matrix(atoms, complex_T_matrix, bool_T_matrix, polarizability,w, periodic);
        for(int iatom=0; iatom < num_atoms; iatom++){
            for(int jatom=0; jatom < num_atoms; jatom++){
                for(int x=0; x<3; x++){
                    for(int y=0; y<3; y++){
                        if(bool_T_matrix[iatom][jatom]==false)
                            continue;
                        int x_position = 3*iatom+x;
                        int y_position = 3*jatom+y;
                        new_A[x_position*3*num_atoms+y_position] = -complex_T_matrix[iatom][jatom][x*3+y]*polarizability[jatom][w]*(float)1E24;
                    }
                }
            }
        }
        std::vector<complex<float> > B (9*num_atoms, 0.0);
        for(int iatom=0; iatom<num_atoms; iatom++){
            B[iatom*3] = polarizability[iatom][w]*(float)1E24;
            B[iatom*3+num_atoms*3+1] = polarizability[iatom][w]*(float)1E24;
            B[iatom*3+num_atoms*6+2] = polarizability[iatom][w]*(float)1E24;
            new_A[3*num_atoms*3*iatom+3*iatom] = (float) 1.0;
            new_A[3*num_atoms*(3*iatom+1)+3*iatom+1] = (float) 1.0;
            new_A[3*num_atoms*(3*iatom+2)+3*iatom+2] = (float) 1.0;
        }

        //int info = LAPACKE_cposv(LAPACK_COL_MAJOR, 'U', num_atoms*3, 3, reinterpret_cast <lapack_complex_float*> (&A[0]),
        //             3*num_atoms,reinterpret_cast <lapack_complex_float*>( &B[0]), 3*num_atoms);
        int info;
        /*
           LAPACKE_csysv(LAPACK_COL_MAJOR, 'U', num_atoms*3, 3, reinterpret_cast <lapack_complex_float*> (&new_A[0]),
           3*num_atoms, IPIV, reinterpret_cast <lapack_complex_float*>( &B[0]), 3*num_atoms);
           */
        info = LAPACKE_cgesv(LAPACK_COL_MAJOR, num_atoms*3, 3, reinterpret_cast <lapack_complex_float*> (&new_A[0]),
                3*num_atoms, IPIV, reinterpret_cast <lapack_complex_float*>( &B[0]), 3*num_atoms);
        if(info!=0){
            std::cout << "ERROR for get LWORK!!!" << std::endl;
            std::cout << "info : " << info << std::endl;
            std::cout << "wavelength : " << wavelength_range[w] << std::endl;
            //exit(-1);
        }
        else{
            double end = MPI_Wtime();
            std::cout << wavelength_range[w] << " nm is success, time : " << end-st << " s" << std::endl;
        }

        for(int iatom=0; iatom<num_atoms; iatom++){
            alpha_cluster[w][iatom][0] = B[3*iatom];
            alpha_cluster[w][iatom][1] = B[3*iatom+3*num_atoms];
            alpha_cluster[w][iatom][2] = B[3*iatom+6*num_atoms];
            alpha_cluster[w][iatom][3] = B[3*iatom+1];
            alpha_cluster[w][iatom][4] = B[3*iatom+3*num_atoms+1];
            alpha_cluster[w][iatom][5] = B[3*iatom+6*num_atoms+1];
            alpha_cluster[w][iatom][6] = B[3*iatom+2];
            alpha_cluster[w][iatom][7] = B[3*iatom+3*num_atoms+2];
            alpha_cluster[w][iatom][8] = B[3*iatom+6*num_atoms+2];
        }
        delete [] IPIV;
    }
    bool_T_matrix.clear();
    complex_T_matrix.clear();
    if(NumProcs>1){
        double st = MPI_Wtime();
        int index = 0;
        std::complex<float>* conti = new complex<float>[9*alpha_cluster.size()*num_atoms];
        std::complex<float>* combine = new complex<float>[9*alpha_cluster.size()*num_atoms];
        for(int w=0; w<alpha_cluster.size(); w++){
            for(int iatom=0; iatom < alpha_cluster[w].size(); iatom++){
                memcpy(&conti[index], alpha_cluster[w][iatom].data(), sizeof(complex<float>)*9);
                index+=9;
            }
        }
        Parallel_Manager::info().group_allreduce(conti, combine,  9*alpha_cluster.size()*num_atoms , Parallel_Manager::Sum);
        index = 0;
        for(int w=0; w<alpha_cluster.size(); w++){
            for(int iatom=0; iatom < alpha_cluster[w].size(); iatom++){
                //memcpy(&conti[index], alpha_cluster[w][iatom].data(), sizeof(complex<float>)*9);
                memcpy(alpha_cluster[w][iatom].data(), &combine[index], sizeof(complex<float>)*9);
                index+=9;
            }
        }
        double end = MPI_Wtime();
        Verbose::single() << "DDA communication time : " << end - st << " s " << std::endl;
    }
    return 0;
}

void DDA::write_polarizability(Teuchos::RCP<const Atoms> atoms,
                               std::vector<std::vector< std::array< std::complex<float> ,9> > > alpha_cluster,
                               std::vector<std::vector<std::complex<float> > > polarizability){
    std::vector<std::complex<float> > before ;
    std::vector<std::complex<float> > after ;
    std::vector<std::complex<float> > total (wavelength_range.size());

    int jatom = 0;
    string prefix = parameters -> sublist("DDA").get<string>("OutputPrefix","");
    int output_before = parameters -> sublist("DDA").get<int>("OutputBefore",0);
    int output_after = parameters -> sublist("DDA").get<int>("OutputAfter",0);


    if(prefix!="")
        prefix+="_";
    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        for(int w=0; w<wavelength_range.size(); w++){
            if(output_before>0)
                before.push_back(polarizability[iatom][w]*(float)1E24);
            if(output_after>0)
                after.push_back((alpha_cluster[w][iatom][0]+alpha_cluster[w][iatom][4]+alpha_cluster[w][iatom][8])/(float)3.0);
            total[w]+=(alpha_cluster[w][iatom][0]+alpha_cluster[w][iatom][4]+alpha_cluster[w][iatom][8])/(float)3.0;

            //total[w]+=(alpha_cluster[w][iatom][0])/(float)3.0;
        }
        std::string name1 = prefix+"BEFORE_"+String_Util::to_string(iatom);
        std::string name2 = prefix+"AFTER_"+String_Util::to_string(iatom);
        if(output_before>0)
            mesh->write_polarizability(name1, atoms, wavelength_range, before);
        if(output_after>0)
            mesh->write_polarizability(name2, atoms, wavelength_range, after);

        before.clear();
        after.clear();
    }
    int output_dipole = parameters -> sublist("DDA").get<int>("OutputDipole",0);
    if(output_dipole>0){
        std::vector<std::vector< std::vector< std::complex<float> > > > dipole (wavelength_range.size());
        for(int w=0; w<wavelength_range.size(); w++){
            dipole[w].resize(atoms->get_size());
            for(int iatom=0; iatom<atoms->get_size(); iatom++){
                dipole[w][iatom].resize(3);
                complex<float> dipole_x = alpha_cluster[w][iatom][0] + alpha_cluster[w][iatom][2] + alpha_cluster[w][iatom][2];
                complex<float> dipole_y = alpha_cluster[w][iatom][3] + alpha_cluster[w][iatom][4] + alpha_cluster[w][iatom][5];
                complex<float> dipole_z = alpha_cluster[w][iatom][6] + alpha_cluster[w][iatom][7] + alpha_cluster[w][iatom][8];
                dipole[w][iatom][0] = dipole_x;
                dipole[w][iatom][1] = dipole_y;
                dipole[w][iatom][2] = dipole_z;
            }
        }
    }
    string name = prefix+"Total";
    mesh->write_polarizability(name, atoms, wavelength_range, total);
}

void DDA::write_dipole(Teuchos::RCP<const Atoms> atoms,
                               std::vector<std::vector< std::array< std::complex<float> ,9> > > alpha_cluster,
                               std::vector<std::vector<std::complex<float> > > polarizability){

    int output_dipole = parameters -> sublist("DDA").get<int>("OutputDipole",0);
    if(output_dipole>0){
        string prefix = parameters -> sublist("DDA").get<string>("OutputPrefix","");
        if(prefix!="")
            prefix+="_";
        std::vector<std::vector< std::vector< std::complex<float> > > > dipole (wavelength_range.size());
        for(int w=0; w<wavelength_range.size(); w++){
            dipole[w].resize(atoms->get_size());
            for(int iatom=0; iatom<atoms->get_size(); iatom++){
                dipole[w][iatom].resize(3);
                complex<float> dipole_x = alpha_cluster[w][iatom][2];
                complex<float> dipole_y = alpha_cluster[w][iatom][5];
                complex<float> dipole_z = alpha_cluster[w][iatom][8];
                dipole[w][iatom][0] = dipole_x;
                dipole[w][iatom][1] = dipole_y;
                dipole[w][iatom][2] = dipole_z;
            }
        }
        mesh->write_dipole(prefix, atoms, wavelength_range, dipole);
    }

}

void DDA::calculate_CN(Teuchos::RCP<const Atoms> atoms,
        std::vector<double>& CN){
    double rmin = parameters -> sublist("DDA").get<double>("CNRmin",3.0);
    double rmax = parameters -> sublist("DDA").get<double>("CNRmax",5.0);
    CN.resize(atoms->get_size());
    double x,y,z,r;
    vector<std::array<double,3> > positions=atoms->get_positions();
#ifdef ACE_HAVE_OMP
#pragma omp parallel for private(x,y,z,r)
#endif
    for(int iatom=0; iatom< atoms->get_size(); iatom++){
        double cn = 0.0;
        for(int jatom=0; jatom< atoms->get_size(); jatom++){
            if(iatom==jatom)
                continue;
            x = (positions[iatom][0]-positions[jatom][0])*0.529177249;
            y = (positions[iatom][1]-positions[jatom][1])*0.529177249;
            z = (positions[iatom][2]-positions[jatom][2])*0.529177249;
            r = sqrt(x*x+y*y+z*z);
            if(r <= rmin)
                cn+=1.0;
            else if(r>rmin and r<rmax)
                cn+=0.5*(1+cos(3.141592*r/(rmax-rmin)));
        }
        CN[iatom] = cn;
    }
    for(int i=0; i<CN.size(); i++){
        Verbose::single() << "CN of " << i << " th atom is " << CN[i] << std::endl;
    }
}


void DDA::calculate_damping(Teuchos::RCP<const Atoms> atoms,
        std::vector<double>& damping){
    double light_speed = 299792458.0;
    double pi = 3.14159265359;
    if(parameters -> sublist("DDA").get<int>("PDDamping",0)>0){
        std::vector<double> CN;
        calculate_CN(atoms, CN);
        double dmax = parameters -> sublist("DDA").get<double>("PDDampingMax",10000.0);
        double dmin = parameters -> sublist("DDA").get<double>("PDDampingMin",1000.0);
        double CNmax = parameters -> sublist("DDA").get<double>("CNmax",12.0);
        for(int iatom=0; iatom<atoms->get_size(); iatom++){

            double X = std::min(CN[iatom]/CNmax, 1.0);
            Verbose::single() << "X of " << iatom << " th atom : " << X << std::endl;
            double d = (2*pi*light_speed/dmin*1E9)*(1-X)+(2*pi*light_speed/dmax*1E9)*X;
            damping.push_back(d);
        }
    }
    else{
        double d = parameters -> sublist("DDA").get<double>("Damping",1000);
        damping.clear();
        for(int iatom=0; iatom<atoms->get_size(); iatom++)
            damping.push_back(2*pi*light_speed/d*1E9);

    }
}

int DDA::calculate_polarizability(Teuchos::RCP<const Atoms> atoms,
                             Teuchos::RCP<const Atoms> ref_atoms,
                             std::vector<std::vector<std::complex<float> > >& polarizability,
                             std::vector<double>& wavelength){

    std::vector<double> CN, ref_CN;
    calculate_CN(atoms, CN);
    calculate_CN(ref_atoms, ref_CN);
    Array<std::string> AP_filenames = parameters -> sublist("DDA").get<Array<std::string> >("AtomicPolarizability");

    if(AP_filenames.size()!=ref_atoms->get_size()){
        std::cout << "wrong number of Txt files " << AP_filenames.size() << " " << ref_atoms->get_size() << std::endl;
        exit(-1);
    }
    std::vector<std::vector<std::complex<float> > > ref_polarizability;
    ref_polarizability.resize(ref_atoms->get_size());
    for(int i=0; i<AP_filenames.size(); i++){
        std::vector<std::complex<double> > tmp_polarizability;
        wavelength_range.clear();
        Read::Txt::read_txt(AP_filenames[i], wavelength_range, tmp_polarizability);
        std::vector<std::complex<float> > tmp_polarizability2(tmp_polarizability.begin(), tmp_polarizability.end());
        for(int j=0; j<tmp_polarizability2.size(); j++)
            tmp_polarizability2[j]/=(float)1E24;
        ref_polarizability[i] = tmp_polarizability2;

    }
    polarizability.resize(atoms->get_size());
    for(int iatom=0; iatom< atoms->get_size(); iatom++){
        int index = closest(ref_CN, CN[iatom]);
        polarizability[iatom] = ref_polarizability[index];
        Verbose::single() << iatom << "th atom is assigned to " << index << " th atom " << CN[iatom] << " " << ref_CN[index] << std::endl;
    }
    return 0;
}

int DDA::closest(std::vector<double> vec, double value) {
    std::vector<double> tmp (vec.size());
    for(int i=0; i<tmp.size(); i++){
        tmp[i] = std::fabs(vec[i]-value);
    }
    double min = tmp[0];
    int min_index = 0;
    for(int i=0; i<tmp.size(); i++){
        if(tmp[i]<min){
            min_index=i;
            min = tmp[i];
        }
    }
    return min_index;
}



/*

   int DDA::iterate(Teuchos::RCP<const Atoms> atoms, std::vector<std::vector<double > > polarizability_real){
   std::cout << "start iteartion " << std::endl;
   int max_iter = parameters -> sublist("DDA").get<int>("MaxIter", 1000);
   double convergence = parameters -> sublist("DDA").get<double>("Convergence", 0.001);
   std::vector<std::vector< std::array< double,9 > > > T_matrix;
   get_T_matrix(atoms, T_matrix);
   std::vector<std::vector<double*> >alpha_cluster_real(wavelength_range.size());
   double* tmp_matrix = new double[9];
   std::vector<double > iatom_alpha(atoms->get_size());
   std::vector<std::array<double,9> > before_alpha(atoms->get_size());

   for(int w=0; w<wavelength_range.size();w++){
   alpha_cluster_real[w].resize(atoms->get_size());
   for(int iatom=0; iatom< atoms->get_size(); iatom++){
   alpha_cluster_real[w][iatom] = new double[9];
//        alpha_imag[iatom] = new double[9];
for(int i=0;i<atoms->get_num_types();i++){
if(atoms->get_atomic_numbers()[iatom] == atoms->get_atom_types()[i]){
iatom_alpha[iatom] = polarizability_real[i][w]*1E24;
}
}
}

for(int iatom=0; iatom < atoms->get_size(); iatom++)
memset(before_alpha[iatom].data(), 0.0, sizeof(double)*9);
//for(int iter=0; iter<max_iter; iter++){
bool converged = true;
for(int iter=0; iter<10; iter++){
//std::cout << std::endl << "before : " << iter << " " << alpha_real[w][0][0] << std::endl;

for(int iatom=0; iatom < atoms->get_size(); iatom++){

std::memset(tmp_matrix, 0.0, sizeof(double)*9);
//std::cout << "1" << std::endl;
for(int jatom=0; jatom< atoms->get_size(); jatom++){
if(iatom!=jatom)
cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 3,3,3, 1.0,
T_matrix[iatom][jatom].data(), 3, alpha_cluster_real[w][iatom],3, 1.0, tmp_matrix, 3);
}

//std::cout << "2" << std::endl;
//std::cout << "alpha fixed " << iatom_alpha_fixed.size() << " " << alpha_fixed[iatom][w] << std::endl;
//std::cout << "tmp_matrix1 : " << tmp_matrix[0] << " " << iatom_alpha_fixed[iatom] << std::endl;
cblas_dscal(9,-iatom_alpha[iatom],tmp_matrix,1 );
//std::cout << "tmp_matrix2 : " << tmp_matrix[0] << " " << iatom_alpha_fixed[iatom] << std::endl;
//cblas_dscal(9,-alpha_fixed[w][iatom],tmp,1 );
tmp_matrix[0]+=iatom_alpha[iatom];
tmp_matrix[4]+=iatom_alpha[iatom];
tmp_matrix[8]+=iatom_alpha[iatom];
//std::cout << "3" << std::endl;
//std::cout << "before alpha assign " << alpha_real[w][iatom][0] << std::endl;
//std::cout << "tmp_matrix3 : " << tmp_matrix[0] << " " << iatom_alpha_fixed[iatom] << std::endl;
std::memcpy(alpha_cluster_real[w][iatom],tmp_matrix, sizeof(double)*9);
//std::cout << "after alpha assign " << alpha_real[w][iatom][0] << std::endl;
//std::cout << "4" << std::endl;
}

double* diff = new double[9];
double norm = 0.0;
converged = true;
for(int iatom=0; iatom < atoms->get_size(); iatom++){
memcpy(diff, alpha_cluster_real[w][iatom], sizeof(double)*9);
cblas_daxpy(9,-1.0, before_alpha[iatom].data(), 1,diff, 1);
norm = cblas_dnrm2(9,diff, 1);
//std::cout << iter << " " << iatom << " " << norm << std::endl;
//memcpy(before_alpha_real[iatom], alpha_real[w][iatom], sizeof(double)*9);
if(norm>convergence){
converged = false;
break;
}

}
delete [] diff;
if(converged){
    Verbose::single() << wavelength_range[w] << "nm is converged ( iteration : " << iter << ", norm : " << norm << " )" << std::endl;
    break;
}
else{
    for(int iatom=0; iatom < atoms->get_size(); iatom++)
        memcpy(before_alpha[iatom].data(), alpha_cluster_real[w][iatom], sizeof(double)*9);
}

}
if(!converged)
    Verbose::single() << wavelength_range[w] << "nm is not converged" << std::endl;
    //exit(-1);

    }
delete [] tmp_matrix;
for(int iatom=0; iatom < atoms->get_size(); iatom++)
delete [] before_alpha[iatom];
for(int w = 0; w < alpha_cluster_real.size(); w++){
    double real_sum = 0.0;
    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        real_sum+=alpha_cluster_real[w][iatom][0]+alpha_cluster_real[w][iatom][4]+alpha_cluster_real[w][iatom][8];
    }
    real_sum/=3.0;
    //        std::cout << wavelength_range[w] << " " << real_sum << std::endl;

}
for(int w = 0; w < alpha_cluster_real.size(); w++){
    double sum0 = alpha_cluster_real[w][0][0]+alpha_cluster_real[w][0][4]+alpha_cluster_real[w][0][8];
    double sum1 = alpha_cluster_real[w][1][0]+alpha_cluster_real[w][1][4]+alpha_cluster_real[w][1][8];
    double sum2 = alpha_cluster_real[w][2][0]+alpha_cluster_real[w][2][4]+alpha_cluster_real[w][2][8];
    double average = (sum0+sum1+sum2) / 3.0;
    //std::cout << wavelength_range[w] << " " << sum0 << " " << sum1 << " " <<sum2 <<  " " << average << std::endl;
}
return 0;
}
*/
