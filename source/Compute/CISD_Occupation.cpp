#include <vector>
#include <iostream>
#include <algorithm>
#include "CISD_Occupation.hpp"
#include "../Util/Density_From_Orbitals.hpp"

using std::string;
using std::vector;
using std::endl;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

CISD_Occupation::CISD_Occupation(RCP<Teuchos::ParameterList> parameters, int num_orbitals, int num_alpha_electrons, int num_beta_electrons){
    this->num_orbitals = num_orbitals;
    this->num_alpha_electrons = num_alpha_electrons;
    this->num_beta_electrons = num_beta_electrons;
    this->parameters = parameters;
    if((num_orbitals < num_alpha_electrons+2 or num_orbitals < num_beta_electrons+2) and num_alpha_electrons==!1){
        Verbose::all() << "too small number of orbitals, number of orbitals should be large than 2+num_alapha_electrons and 2+beta_electrons" << endl;
        exit(-1);
    }
    initializer();
}
void CISD_Occupation::initializer(){
    //Verbose::single() << "SY1" << endl;
    this -> SY1 = cal_Y(num_alpha_electrons,num_orbitals, 1);
    if(num_alpha_electrons>1){
        //Verbose::single() << "SY2" << endl;
        this -> DY1 = cal_Y(num_alpha_electrons,num_orbitals, 2);
    }
    Verbose::single(Verbose::Detail) << "start make occupation" << endl;
    make_restricted_occupation(2);
    /*
    Verbose::single() << "single" << endl;
    for(int i=0; i<string_number[1];i++){
        for(int j = 0; j<num_alpha_electrons; j++){
            //Verbose::single() << total_occupation[0][1][i][j] << " ";
        }
        //Verbose::single() << "  ";
        //Verbose::single() << get_address(1,total_occupation[0][1][i]) << endl;
    }

    if(num_alpha_electrons>1){
        Verbose::single() << "double" << endl;

        for(int i=0; i<string_number[2];i++){
            for(int j = 0; j<num_alpha_electrons; j++){
                Verbose::single() << total_occupation[0][2][i][j] << " ";
            }
            Verbose::single() << "  ";
            Verbose::single() << get_address(2,total_occupation[0][2][i]) << endl;
        }
    }
    */
    check_address.clear();
    check_address.resize(3);
    for(int i = 0; i < 3; ++i){
        check_address.at(i).resize(3);
    }

    check_address[0][0] = 0;
    check_address[1][0] = check_address[0][0] + string_number[0]*string_number[0];
    check_address[0][1] = check_address[1][0] + string_number[1]*string_number[0];
    check_address[1][1] = check_address[0][1] + string_number[0]*string_number[1];
    check_address[2][0] = check_address[1][1] + string_number[1]*string_number[1];
    check_address[0][2] = check_address[2][0] + string_number[2]*string_number[0];
    spin_check_address = vector<int>(string_number[0] + string_number[1] +string_number[2]);
    spin_check_address[0] = 0;
    spin_check_address[1] = 1;
    spin_check_address[2] = 1 + string_number[1];

    total_num_string = string_number[0]*string_number[0]
        + string_number[1]*string_number[0]
        + string_number[0]*string_number[1]
        + string_number[1]*string_number[1]
        + string_number[2]*string_number[0]
        + string_number[0]*string_number[2];
    Verbose::single(Verbose::Detail) << "total num string : " << total_num_string << endl;
    Verbose::single(Verbose::Detail) << "make cisd occupation end" << endl;
    make_sign_list();
    Verbose::single(Verbose::Detail) << "make sign list end" << endl;
    Verbose::single(Verbose::Detail) << "gamma test" << endl;

//    cout << "find spin pair test" << endl;
   /*
    for(int i=0; i<total_num_string;i++){
        cout << "===========" << endl;
        print_configuration(i);
        int temp = find_pair_spin(i);
        print_configuration(temp);
        cout << "===========" << endl;
    }
    */
    /*
    vector<int> same_alpha_occ;
    vector<int> same_beta_occ;
    vector<int> diff_alpha_occ;
    vector<int> diff_beta_occ;
    same_alpha_occ.clear();
    same_beta_occ.clear();
    diff_alpha_occ.clear();
    diff_beta_occ.clear();
    cout << "asdfasdfadf" << endl;
    check_same_diff_occupation(0, 736, same_alpha_occ,  diff_alpha_occ, same_beta_occ,diff_beta_occ);
    cout << endl << "asdfasdfadf" << endl;
    same_alpha_occ.clear();
    same_beta_occ.clear();
    diff_alpha_occ.clear();
    diff_beta_occ.clear();
    check_same_diff_occupation(199, 200, same_alpha_occ,  diff_alpha_occ, same_beta_occ,diff_beta_occ);
    cout << endl << "asdfasdfadf" << endl;
    same_alpha_occ.clear();
    same_beta_occ.clear();
    diff_alpha_occ.clear();
    diff_beta_occ.clear();
    check_same_diff_occupation(73, 400, same_alpha_occ,  diff_alpha_occ, same_beta_occ,diff_beta_occ);
    */
}

int CISD_Occupation::cal_double_sign(int address)
{
    vector<int>  occupation;
    vector<int>  ground_occupation;

    occupation = total_occupation[0][2][address];
    ground_occupation = total_occupation[0][0][0];
    int i=0;
    int index=0;
    int different[2];
    int k = 0;
    for(int i=0; i<num_alpha_electrons; i++){
        if(occupation[index]!=i){
            different[k] = i;
            k++;
        }
        if(k==2){
            break;
        }
        index++;
    }
    if(different[0]+different[1] -1 ==1)
        return -1;
    return 1;
}

Teuchos::Array< Teuchos::RCP<Epetra_Vector> > CISD_Occupation::cal_sigma(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > CI_coefficient){
    Teuchos::Array< Teuchos::RCP< Epetra_Vector> > sigma;
    for(int i=0; i<CI_coefficient.size(); i++)
        sigma.append(Teuchos::rcp(new Epetra_Vector(CI_coefficient[0]->Map())));
    int* MyGlobalElements = CI_coefficient[0]->Map().MyGlobalElements();

    combine_coefficient = new double*[CI_coefficient.size()];
    for(int k=0; k<CI_coefficient.size(); k++){
        combine_coefficient[k] = new double[total_num_string];
        double* tmp_combine_coefficient = new double[total_num_string];
        int NumMyElements = CI_coefficient[0]->Map().NumMyElements();
        for(int i=0; i<NumMyElements; i++)
            tmp_combine_coefficient[MyGlobalElements[i]] = CI_coefficient[k]->operator[](i);
        CI_coefficient[0]->Map().Comm().SumAll(tmp_combine_coefficient, combine_coefficient[k], total_num_string);
        delete [] tmp_combine_coefficient;
    }
    cal_sigma_alpha_alpha(sigma, CI_coefficient.size());
    cal_sigma_beta_beta(sigma, CI_coefficient.size());

    int alpha1, beta1,alpha_address1, beta_address1;
    for(int i=0; i<total_num_string;i++){
        if(CI_coefficient[0]->Map().LID(i)>=0){
            check_category(i, &alpha1, &beta1, &alpha_address1, &beta_address1);
            vector<double> temp(CI_coefficient.size());
            /*
            temp = cal_sigma1_alpha_alpha(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp[j];
            temp = cal_sigma2_alpha_alpha(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp[j];
            temp = cal_sigma1_beta_beta(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp[j];

            temp = cal_sigma2_beta_beta(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++)
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp[j];
            */
            temp = cal_sigma2_alpha_beta(alpha1, alpha_address1, beta1, beta_address1, CI_coefficient.size());
            for(int j=0; j<CI_coefficient.size();j++){
                sigma[j]->operator[](CI_coefficient[0]->Map().LID(i)) += temp.at(j);
            }
        }
    }

   // cout << "sigma check" << endl;
   // cout << *sigma[0] << endl;
   // cout << *sigma[1] << endl;
   // cout << *sigma[2] << endl;
    //exit(-1);
    for(int i=0; i<CI_coefficient.size(); i++)
        delete [] combine_coefficient[i];
    delete combine_coefficient;
    return sigma;

}
int CISD_Occupation::cal_sign(int excitation, int address){
    if(excitation==1)
        return cal_single_sign(address);
    if(excitation==2)
        return cal_double_sign(address);
    else
        return 1;
}

void CISD_Occupation::cal_sigma_alpha_alpha(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > sigma, int num_blocks)
{
    clock_t start_time, end_time;
    start_time = clock();
    double time1 = 0.0;
    double time2 = 0.0;
    int num_alpha_string = string_number[0] + string_number[1] + string_number[2];
    int MyPID = sigma[0]->Map().Comm().MyPID();
    int NumProcs = sigma[0]->Map().Comm().NumProc();
    int start_index;
    int end_index;
    if(num_orbitals < NumProcs){
        if(MyPID <num_orbitals){
            start_index = MyPID;
            end_index = MyPID+1;
        }
        else{
            start_index = 0;
            end_index = -1;
        }
    }
    else{
            start_index = (int(num_orbitals/NumProcs))*MyPID;
            end_index = start_index + int(num_orbitals/NumProcs);
            if(MyPID==NumProcs-1)
                end_index = num_orbitals;
    }

    Epetra_Map sigma_alpha_alpha_map (num_alpha_string, 0, sigma[0]->Map().Comm());
    for(int index = 0; index<num_alpha_string; index++){
        double* F = new double[num_alpha_string];
        clock_t st,end;
        for(int i=0; i<num_alpha_string; i++)
            F[i] = 0.0;
        //Verbose::single() << "num_alpha_string : " << num_alpha_string << endl;
        //Verbose::single() << "index : " << index << endl;
        int alpha_address;
        int alpha_excitation;
        address_to_excitation(&alpha_excitation, &alpha_address, index);
        int alpha_sign = cal_sign(alpha_excitation, alpha_address);
        vector<int> alpha_occupation = total_occupation[0][alpha_excitation][alpha_address];
        for(int p = start_index; p < end_index; p++){//loop over number of orbitals
            for(int q=0; q<num_alpha_electrons; q++){//loop over number of alpha_electrons
                int epq = Eij(alpha_occupation,p,q);
                double value =0.0;
                if(epq!=0){
                    vector<int> new_alpha_occupation = total_occupation[0][alpha_excitation][alpha_address];
                    new_alpha_occupation[q] = p;
                    //st = clock();
                    Sorting(new_alpha_occupation);
                    //sort(new_alpha_occupation.begin(), new_alpha_occupation.end());
                    //end =clock();
                    //time2 = time2 + double((end-st))/CLOCKS_PER_SEC;
                    int new_alpha_excitation = distinguish_excitation(new_alpha_occupation);
                    if (new_alpha_excitation < 3){
                        int new_alpha_address = get_address(new_alpha_excitation, new_alpha_occupation);
                        int new_alpha_sign = cal_sign(new_alpha_excitation, new_alpha_address);
                        F[new_alpha_address + spin_check_address[new_alpha_excitation]] += alpha_sign*new_alpha_sign*Hcore[p][alpha_occupation[q]]*epq;
                    }
                    for(int r=0; r<num_orbitals; r++){
                        for(int s = 0;s<num_alpha_electrons; s++){
                            int ers = Eij(new_alpha_occupation,r,s);

                            if(ers!=0){

                                vector<int> new_new_alpha_occupation = total_occupation[0][alpha_excitation][alpha_address];
                                new_new_alpha_occupation[q] = p;
                                //st = clock();
                                Sorting(new_new_alpha_occupation);
                                //sort(new_new_alpha_occupation.begin(), new_new_alpha_occupation.end());
                                //end =clock();
                                //time2 = time2 + double((end-st))/CLOCKS_PER_SEC;
                                new_new_alpha_occupation[s] = r;
                                //st = clock();
                                Sorting(new_new_alpha_occupation);
                                //sort(new_new_alpha_occupation.begin(), new_new_alpha_occupation.end());
                                //end =clock();
                                //time2 = time2 + double((end-st))/CLOCKS_PER_SEC;
                                int new_new_alpha_excitation = distinguish_excitation(new_new_alpha_occupation);

                                if (new_new_alpha_excitation < 3){
                                    int new_new_alpha_address = get_address(new_new_alpha_excitation, new_new_alpha_occupation);
                                    int new_new_alpha_sign = cal_sign(new_new_alpha_excitation, new_new_alpha_address);
                                    F[new_new_alpha_address + spin_check_address[new_new_alpha_excitation]] += alpha_sign*new_new_alpha_sign*two_centered_integ[p][alpha_occupation[q]][r][new_alpha_occupation[s]]*epq*ers*0.5;
                                }
                            }
                        }
                    }
                }
            }
        }
//        st = clock();
        double* combine_F = new double[num_alpha_string];
        sigma[0]->Map().Comm().SumAll(F, combine_F, num_alpha_string);
        for(int beta_excitation = 0; beta_excitation<3 - alpha_excitation; beta_excitation++){
            if(num_alpha_electrons==1 and beta_excitation==2)
                continue;
            else{
                for(int beta_address = 0; beta_address < string_number[beta_excitation]; beta_address++){
                    int coordinate = check_address[alpha_excitation][beta_excitation]+alpha_address+string_number[alpha_excitation]*beta_address;
                    if(sigma[0]->Map().LID(coordinate)>=0){
                        for(int j=0; j<num_alpha_string; j++){
                            int excitation = 0;
                            int address = 0;
                            address_to_excitation(&excitation, &address, j);
                            int new_coordinate = check_address[excitation][beta_excitation] + address + string_number[excitation]*beta_address;
                            if(excitation+beta_excitation <3){
                                for(int i=0; i<num_blocks; i++){
                                    sigma[i]->operator[](sigma[0]->Map().LID(coordinate)) = sigma[i]->operator[](sigma[0]->Map().LID(coordinate)) + combine_F[j]*combine_coefficient[i][new_coordinate];

                                }
                            }
                        }
                    }
                }
            }
        }
        delete [] combine_F;
        delete [] F;
//        end = clock();
//        time1 = time1 + double((end-st))/CLOCKS_PER_SEC;
    }
    end_time = clock();
//    Verbose::single() << "cal alpha_alpha_time : " << double((end_time-start_time))/CLOCKS_PER_SEC << " s" << endl;
//    Verbose::single() << "multiplication time : " << time1 << " s" << endl;
//    Verbose::single() << "sorting time : " << time2 << " s" << endl;
    return;
}

void CISD_Occupation::cal_sigma_beta_beta(Teuchos::Array< Teuchos::RCP< Epetra_Vector > > sigma, int num_blocks)
{
    int num_beta_string = string_number[0] + string_number[1] + string_number[2];
    Epetra_Map sigma_beta_beta_map (num_beta_string, 0, sigma[0]->Map().Comm());
    Teuchos::Array< Teuchos::RCP< Epetra_Vector > > F;
    int MyPID = sigma[0]->Map().Comm().MyPID();
    int NumProcs = sigma[0]->Map().Comm().NumProc();
    int start_index;
    int end_index;
    if(num_orbitals < NumProcs){
        if(MyPID <num_orbitals){
            start_index = MyPID;
            end_index = MyPID+1;
        }
        else{
            start_index = 0;
            end_index = -1;
        }
    }
    else{
        start_index = (int(num_orbitals/NumProcs))*MyPID;
        end_index = start_index + int(num_orbitals/NumProcs);
        if(MyPID==NumProcs-1)
            end_index = num_orbitals;
    }
    for(int index = 0; index<num_beta_string; index++){
        double* F = new double[num_beta_string]();

        int beta_address;
        int beta_excitation;
        address_to_excitation(&beta_excitation, &beta_address, index);
        int beta_sign = cal_sign(beta_excitation, beta_address);
        vector<int> beta_occupation = total_occupation[0][beta_excitation][beta_address];
        for(int p = start_index; p < end_index; p++){
            for(int q=0; q<num_beta_electrons; q++){
                int epq = Eij(beta_occupation,p,q);
                if(epq!=0){
                    vector<int> new_beta_occupation = total_occupation[0][beta_excitation][beta_address];
                    new_beta_occupation[q] = p;
                    Sorting(new_beta_occupation);
                    //sort(new_beta_occupation.begin(), new_beta_occupation.end());
                    int new_beta_excitation = distinguish_excitation(new_beta_occupation);
                    if (new_beta_excitation < 3){
                        int new_beta_address = get_address(new_beta_excitation, new_beta_occupation);
                        int new_beta_sign = cal_sign(new_beta_excitation, new_beta_address);
                        F[new_beta_address + spin_check_address[new_beta_excitation]] += beta_sign*new_beta_sign*Hcore[p][beta_occupation[q]]*epq;
                    }
                    for(int r=0; r<num_orbitals; r++){
                        for(int s = 0;s<num_beta_electrons; s++){
                            int ers = Eij(new_beta_occupation,r,s);
                            if(ers!=0){
                                vector<int> new_new_beta_occupation = total_occupation[0][beta_excitation][beta_address];
                                new_new_beta_occupation[q] = p;
                                Sorting(new_new_beta_occupation);
                                //sort(new_new_beta_occupation.begin(), new_new_beta_occupation.end());
                                new_new_beta_occupation[s] = r;
                                Sorting(new_new_beta_occupation);
                                //sort(new_new_beta_occupation.begin(), new_new_beta_occupation.end());
                                int new_new_beta_excitation = distinguish_excitation(new_new_beta_occupation);
                                if (new_new_beta_excitation < 3){
                                    int new_new_beta_address = get_address(new_new_beta_excitation, new_new_beta_occupation);
                                    int new_new_beta_sign = cal_sign(new_new_beta_excitation, new_new_beta_address);
                                    F[new_new_beta_address + spin_check_address[new_new_beta_excitation]]+= beta_sign*new_new_beta_sign*two_centered_integ[p][beta_occupation[q]][r][new_beta_occupation[s]]*epq*ers*0.5;
                                }
                            }
                        }
                    }
                }
            }
        }

        double* combine_F = new double[num_beta_string];
        sigma[0]->Map().Comm().SumAll(F, combine_F, num_beta_string);
        for(int alpha_excitation = 0; alpha_excitation<3 - beta_excitation; alpha_excitation++){
            if(num_beta_electrons==1 and alpha_excitation==2)
                continue;
            else{
                for(int alpha_address = 0; alpha_address < string_number[alpha_excitation]; alpha_address++){
                    int coordinate = check_address[alpha_excitation][beta_excitation]+alpha_address+string_number[alpha_excitation]*beta_address;
                    if(sigma[0]->Map().LID(coordinate)>=0){
                        for(int j=0; j<num_beta_string; j++){
                            int excitation = 0;
                            int address = 0;
                            address_to_excitation(&excitation, &address, j);
                            int new_coordinate = check_address[alpha_excitation][excitation] + alpha_address + string_number[alpha_excitation]*address;
                            if(alpha_excitation +excitation <3){
                                for(int i=0; i<num_blocks; i++){
                                    sigma[i]->operator[](sigma[0]->Map().LID(coordinate)) = sigma[i]->operator[](sigma[0]->Map().LID(coordinate)) + combine_F[j]*combine_coefficient[i][new_coordinate];
                                }
                            }
                        }
                    }
                }
            }
        }
        delete [] combine_F;
        delete [] F;

    }
    return;
}
std::vector<double> CISD_Occupation::cal_sigma2_alpha_beta(int excitation1, int address1, int excitation2, int address2, int num_blocks){
    vector<int> occupation1 = total_occupation[0][excitation1][address1];
    vector<int> occupation2 = total_occupation[0][excitation2][address2];
    vector<double> retval(num_blocks);

    for(int p=0; p<num_orbitals;p++){
        for(int q=0; q<num_alpha_electrons;q++){
            for(int r=0; r<num_orbitals;r++){
                for(int s=0; s<num_alpha_electrons;s++){
                    int ers = Eij(occupation2,r,s);
                    if(ers!=0){
                        int epq = Eij(occupation1,p,q);
                        if(epq!=0){
                            vector<int> temp_occupation2 = total_occupation[0][excitation2][address2];
                            vector<int> temp_occupation1 = total_occupation[0][excitation1][address1];
                            temp_occupation2[s] = r;
                            temp_occupation1[q] = p;
                            Sorting(temp_occupation1);
                            Sorting(temp_occupation2);
                            //sort(temp_occupation1.begin(), temp_occupation1.end());
                            //sort(temp_occupation2.begin(), temp_occupation2.end());
                            int new_excitation1 = distinguish_excitation(temp_occupation1);
                            int new_excitation2 = distinguish_excitation(temp_occupation2);
                                if(new_excitation1 + new_excitation2<3 ){
                                    int temp_address1 = get_address(new_excitation1, temp_occupation1);
                                    int temp_address2 = get_address(new_excitation2, temp_occupation2);
                                    for(int l=0; l<num_blocks; l++){
                                        retval[l] += cal_sign(new_excitation1,temp_address1)*cal_sign(new_excitation2,temp_address2)*two_centered_integ[p][occupation1[q]][r][occupation2[s]]*combine_coefficient[l][check_address[new_excitation1][new_excitation2]+temp_address1+string_number[new_excitation1]*temp_address2]*ers*epq;

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    for(int l=0; l<num_blocks; l++){
        retval[l] = retval[l]*cal_sign(excitation2,address2)*cal_sign(excitation1,address1);
    }
    for(int l=0; l<num_blocks; l++){
        retval[l] = retval[l];
    }
    return retval;
}
void CISD_Occupation::address_to_excitation(int* excitation, int* ret_address, int address){
    if(address >=spin_check_address[2])
        *excitation = 2, *ret_address = address - spin_check_address[2];
    else if(address >=spin_check_address[1])
        *excitation = 1, *ret_address = address - spin_check_address[1];
    else
        *excitation = 0, *ret_address = 0;
    return;
}

std::vector<double> CISD_Occupation::cal_Hcore_energy(Teuchos::RCP< Epetra_MultiVector > CI_coefficient, int num_eigenvalues){
    Teuchos::Array< Teuchos::RCP< Epetra_Vector > > Hcore_sigma ;
    for(int i=0; i<CI_coefficient->NumVectors(); i++)
        Hcore_sigma.append(Teuchos::rcp(new Epetra_Vector(CI_coefficient->Map())));
    int num_alpha_string = string_number[0] + string_number[1] + string_number[2];
    int MyPID = Hcore_sigma[0]->Map().Comm().MyPID();
    int NumProcs = Hcore_sigma[0]->Map().Comm().NumProc();
    int start_index;
    int end_index;
    if(num_orbitals < NumProcs){
        if(MyPID <num_orbitals){
            start_index = MyPID;
            end_index = MyPID+1;
        }
        else{
            start_index = 0;
            end_index = -1;
        }
    }
    else{
        start_index = (int(num_orbitals/NumProcs))*MyPID;
        end_index = start_index + int(num_orbitals/NumProcs);
        if(MyPID==NumProcs-1)
            end_index = num_orbitals;
    }
    Epetra_Map sigma_alpha_alpha_map (num_alpha_string, 0, Hcore_sigma[0]->Map().Comm());
    int* MyGlobalElements = CI_coefficient->Map().MyGlobalElements();

    combine_coefficient = new double*[CI_coefficient->NumVectors()];
    for(int k=0; k<CI_coefficient->NumVectors(); k++){
        combine_coefficient[k] = new double[total_num_string]();
        double* tmp_combine_coefficient = new double[total_num_string]();
        int NumMyElements = CI_coefficient->Map().NumMyElements();
        for(int i=0; i<NumMyElements; i++)
            tmp_combine_coefficient[MyGlobalElements[i]] = CI_coefficient->operator[](k)[i];
        CI_coefficient->Map().Comm().SumAll(tmp_combine_coefficient, combine_coefficient[k], total_num_string);
        delete [] tmp_combine_coefficient;
    }

    for(int index = 0; index<num_alpha_string; index++){
        double* F = new double[num_alpha_string];
        for(int i=0; i<num_alpha_string; i++)
            F[i] = 0.0;
        //Verbose::single() << "num_alpha_string : " << num_alpha_string << endl;
        //Verbose::single() << "index : " << index << endl;
        int alpha_address;
        int alpha_excitation;
        address_to_excitation(&alpha_excitation, &alpha_address, index);
        int alpha_sign = cal_sign(alpha_excitation, alpha_address);
        vector<int> alpha_occupation = total_occupation[0][alpha_excitation][alpha_address];
        for(int p = start_index; p < end_index; p++){
            for(int q=0; q<num_alpha_electrons; q++){
                int epq = Eij(alpha_occupation,p,q);
                if(epq!=0){
                    vector<int> new_alpha_occupation = total_occupation[0][alpha_excitation][alpha_address];
                    new_alpha_occupation[q] = p;
                    Sorting(new_alpha_occupation);
                    //sort(new_alpha_occupation.begin(), new_alpha_occupation.end());
                    int new_alpha_excitation = distinguish_excitation(new_alpha_occupation);
                    if (new_alpha_excitation < 3){
                        int new_alpha_address = get_address(new_alpha_excitation, new_alpha_occupation);
                        int new_alpha_sign = cal_sign(new_alpha_excitation, new_alpha_address);
                        F[new_alpha_address + spin_check_address[new_alpha_excitation]] += alpha_sign*new_alpha_sign*pure_Hcore[p][alpha_occupation[q]]*epq;
                    }
                }
            }
        }
        double* combine_F = new double[num_alpha_string];
        Hcore_sigma[0]->Map().Comm().SumAll(F, combine_F, num_alpha_string);
        for(int beta_excitation = 0; beta_excitation<3 - alpha_excitation; beta_excitation++){
            if(num_alpha_electrons==1 and beta_excitation==2)
                continue;
            else{
                for(int beta_address = 0; beta_address < string_number[beta_excitation]; beta_address++){
                    int coordinate = check_address[alpha_excitation][beta_excitation]+alpha_address+string_number[alpha_excitation]*beta_address;
                    if(Hcore_sigma[0]->Map().LID(coordinate)>=0){
                        for(int j=0; j<num_alpha_string; j++){
                            int excitation = 0;
                            int address = 0;
                            address_to_excitation(&excitation, &address, j);
                            int new_coordinate = check_address[excitation][beta_excitation] + address + string_number[excitation]*beta_address;
                            if(excitation+beta_excitation <3){
                                for(int i=0; i<CI_coefficient->NumVectors(); i++){
                                    Hcore_sigma[i]->operator[](Hcore_sigma[0]->Map().LID(coordinate)) = Hcore_sigma[i]->operator[](Hcore_sigma[0]->Map().LID(coordinate)) + combine_F[j]*combine_coefficient[i][new_coordinate];
                                }
                            }
                        }
                    }
                }
            }
        }
        delete [] combine_F;
        delete [] F;
    }
    for(int i=0; i<CI_coefficient->NumVectors(); i++)
        delete [] combine_coefficient[i];
    delete[] combine_coefficient;
    vector<double> Hcore_energy(num_eigenvalues);
    for(int i=0; i<num_eigenvalues; i++){
        double temp = 0;
        CI_coefficient->operator()(i)->Dot(*Hcore_sigma[i], &temp);
        Hcore_energy[i] = temp*2; //2 is because there are same number of alpha and beta electrons
    }

    return Hcore_energy;
}

Teuchos::RCP<Epetra_Vector> CISD_Occupation::cal_CI_density(RCP<const Basis> mesh, Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_of_orbital,Teuchos::RCP<Epetra_Vector> CI_coefficient, Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital){
    /*
    CI_coefficient->PutScalar(0.0);
    if(CI_coefficient->Map().LID(0)>=0)
        CI_coefficient->operator[](0) = 1.0;
    */
    Verbose::single(Verbose::Detail) << "start make ci density" << endl;
    int time = 0;
    Teuchos::RCP<Epetra_Vector> CI_density = Teuchos::rcp(new Epetra_Vector(density_of_orbital[0]->Map()));
    int num_alpha_string = string_number[0] + string_number[1] + string_number[2];
    double* combine_coefficient = new double[total_num_string]();
    double* tmp_combine_coefficient = new double[total_num_string]();

    int NumMyElements = CI_coefficient->Map().NumMyElements();
    int* MyGlobalElements = CI_coefficient->Map().MyGlobalElements();
    for(int i=0; i<NumMyElements; i++)
        tmp_combine_coefficient[MyGlobalElements[i]] = CI_coefficient->operator[](i);
    CI_coefficient->Map().Comm().SumAll(tmp_combine_coefficient, combine_coefficient, total_num_string);
    delete [] tmp_combine_coefficient;

    for(int i=0; i< num_alpha_string; i++){
        int alpha_address;
        int alpha_excitation;
        address_to_excitation(&alpha_excitation, &alpha_address, i);
        for(int beta_excitation = 0; beta_excitation<3 - alpha_excitation; beta_excitation++){
            if(num_alpha_electrons==1 and beta_excitation==2)
                continue;
            else{
                for(int beta_address = 0; beta_address < string_number[beta_excitation]; beta_address++){
                    int coordinate = check_address[alpha_excitation][beta_excitation]+alpha_address+string_number[alpha_excitation]*beta_address;
                    for(int j=0; j<num_alpha_electrons; j++){
                        CI_density->Update(combine_coefficient[coordinate]*combine_coefficient[coordinate], *density_of_orbital[total_occupation[0][alpha_excitation][alpha_address][j]], 1.0);
                        CI_density->Update(combine_coefficient[coordinate]*combine_coefficient[coordinate], *density_of_orbital[total_occupation[0][beta_excitation][beta_address][j]], 1.0);
                    }
                }
            }
        }
    }

    vector< vector< vector<int> > > alpha_occupation;
    vector< vector< vector<int> > > alpha_occupation2; //this occupation include double excitation
    vector<int> temp_merge;
    vector< vector<int> > temp_occupation;
    vector< vector<int> > temp_occupation2;
    temp_occupation = make_occupation(num_alpha_electrons-1, num_alpha_electrons-1, 0);
    alpha_occupation.push_back(temp_occupation);
    alpha_occupation2.push_back(temp_occupation);
    vector< vector<int> > middle_occupation;
    int excite = 1;
    for(int excite =1; excite <3; excite++){
        if(num_alpha_electrons-1 < excite){
            string_number[excite] = 0;
        }
        else{
            temp_occupation.clear();
            temp_occupation2.clear();
            temp_occupation = make_occupation(num_alpha_electrons-excite-1, num_alpha_electrons-1, 0);
            temp_occupation2 = make_occupation(excite, num_orbitals-2-num_alpha_electrons+1, num_alpha_electrons-1);

            for(int j=0; j<temp_occupation2.size(); j++)
            {
                for(int i=0; i<temp_occupation.size(); i++)
                {
                    temp_merge.clear();
                    temp_merge = temp_occupation[i];
                    temp_merge.insert(temp_merge.end(), temp_occupation2[j].begin(), temp_occupation2[j].end());
                    middle_occupation.push_back(temp_merge);
                }
            }
        }
        alpha_occupation2.push_back(middle_occupation);
        middle_occupation.clear();
    }
    alpha_occupation.push_back(alpha_occupation2[1]);
    /*
    for(int i=0; i<alpha_occupation2.size(); i++){
        for(int j=0; j<alpha_occupation2[i].size(); j++){
            for(int k=0; k<alpha_occupation2[i][j].size(); k++){
                Verbose::single() << alpha_occupation2[i][j][k] << " ";
            }
            Verbose::single() << endl;
        }
    }
    */
    for(int i=0; i<num_orbitals; i++){
        for(int j=i+1; j<num_orbitals; j++){
            Teuchos::RCP<Epetra_Vector> overlap_density = Teuchos::rcp(new Epetra_Vector(orbital[0]->Map()));
            overlap_density->Multiply( 1.0, *orbital[0]->operator()(i), *orbital[0]->operator()(j), 0.0);
            vector<vector<vector<int>>> occupation1 ;
            vector<vector<vector<int>>> occupation2 ;
            if(i>=num_alpha_electrons and j>=num_alpha_electrons){
                occupation1 = alpha_occupation2;
                occupation2 = alpha_occupation2;
            }
            else{
                occupation1 = alpha_occupation;
                occupation2 = alpha_occupation;
            }

      //      Verbose::single() << i << " " << j << endl;
            for(int k=0; k < occupation1.size(); k++){
                for(int l=0; l<occupation1[k].size(); l++){
                    for(int n=0; n<occupation1[k][l].size(); n++){
                        if(occupation1[k][l][n] >= i){
                            for(int x=n; x< occupation1[k][l].size(); x++){
                                occupation1[k][l][x] += 1;
                            }
                            occupation1[k][l].insert(occupation1[k][l].begin()+n,i);
                            break;
                        }

                    }
                    for(int n=0; n<occupation1[k][l].size(); n++){
                        if(occupation1[k][l][n]>=j)
                            occupation1[k][l][n] += 1;
                    }
                    if(occupation1[k][l][occupation1[k][l].size()-1]<i){
                        occupation1[k][l].push_back(i);
                    }

                    for(int n=0; n<occupation2[k][l].size(); n++){
                        if(occupation2[k][l][n] >= i)
                            occupation2[k][l][n] +=1;
                    }
                    for(int n=0; n<occupation2[k][l].size(); n++){
                        if(occupation2[k][l][n]>=j){
                            for(int x=n; x< alpha_occupation2[k][l].size(); x++){
                                occupation2[k][l][x] += 1;
                            }
                            occupation2[k][l].insert(occupation2[k][l].begin()+n,j);
                            break;
                        }
                    }
                    if(occupation2[k][l][occupation2[k][l].size()-1]<j){
                        occupation2[k][l].push_back(j);
                    }

                }
            }
            /*
            if(i==0 and j==1){
                Verbose::single() << "check1" << endl;
                for(int x=0; x<occupation1.size(); x++){
                    for(int y=0; y<occupation1[x].size(); y++){
                        for(int z=0; z<occupation1[x][y].size(); z++){
                            //if(occupation1[x][y][z]!=i)
                                Verbose::single() << occupation1[x][y][z] << " ";
                        }
                        Verbose::single() << endl;
                    }
                }
                Verbose::single() << "check2" << endl;
                for(int x=0; x<occupation2.size(); x++){
                    for(int y=0; y<occupation2[x].size(); y++){
                        for(int z=0; z<occupation2[x][y].size(); z++){

                            //if(occupation2[x][y][z]!=j)
                                Verbose::single() << occupation2[x][y][z] << " ";
                        }
                        Verbose::single() << endl;
                    }
                }

            //    exit(-1);
            }
            */
            for(int k=0; k<occupation1.size(); k++){
                for(int l=0; l<occupation1[k].size(); l++){
                    int beta_excitation;
                    int alpha_excitation1 = distinguish_excitation(occupation1[k][l]);
                    int alpha_excitation2 = distinguish_excitation(occupation2[k][l]);
                    if(alpha_excitation1>alpha_excitation2){
                        beta_excitation = 2- distinguish_excitation(occupation1[k][l]);
                    }
                    else{
                        beta_excitation = 2- distinguish_excitation(occupation2[k][l]);
                    }
                    for(int n=0; n<beta_excitation+1;n++){
                        for(int beta_address=0; beta_address<string_number[n]; beta_address++){
                            int coordinate1 = check_address[alpha_excitation1][n] + get_address(alpha_excitation1, occupation1[k][l]) + string_number[alpha_excitation1]*beta_address;
                            int coordinate2 = check_address[alpha_excitation2][n] + get_address(alpha_excitation2, occupation2[k][l]) + string_number[alpha_excitation2]*beta_address;
                            /*
                            cout << "a" << endl;
                            cout << "address" << endl;
                            cout << get_address(alpha_excitation1, occupation1[k][l]) << endl;
                            cout << get_address(alpha_excitation2, occupation2[k][l]) << endl;
                            cout << "check address " << endl;
                            cout << get_address(alpha_excitation1, occupation1[k][l]) << endl;
                            cout << get_address(alpha_excitation2, occupation2[k][l]) << endl;
                            cout << "string number" << endl;
                            cout << string_number[beta_excitation] << endl;
                            cout << string_number[0] << endl;
                            cout << string_number[1] << endl;
                            cout << string_number[2] << endl;

                            cout << "beta excitation " << endl;
                            cout << beta_excitation << endl;
                            cout << coordinate1 << endl;
                            cout << coordinate2 << endl;
                            cout << i << " " << j << endl;
                            */
                            int alpha_address1 = get_address(alpha_excitation1, occupation1[k][l]);
                            int alpha_address2 = get_address(alpha_excitation2, occupation2[k][l]);
                            CI_density->Update(4*combine_coefficient[coordinate1]*combine_coefficient[coordinate2]*get_sign(alpha_excitation1, alpha_address1)*get_sign(alpha_excitation2, alpha_address2), *overlap_density, 1.0);
                            time++;
                        }
                    }
                }
            }

        }
    }

    delete[] combine_coefficient;
    //CI_density->Scale(2.0);//bcause alpha density and beta density is same;
    return CI_density;
}


void CISD_Occupation::cal_combine_H0(Teuchos::RCP<Epetra_Vector> H0){
    int NumMyElements = H0->Map().NumMyElements();
    int* MyGlobalElements = H0->Map().MyGlobalElements();
    combine_H0 = new double[total_num_string]();
    double* temp = new double[total_num_string]();
    for(int j=0; j<NumMyElements; j++)
        temp[MyGlobalElements[j]] = H0->operator[](j);
    H0->Map().Comm().SumAll(temp, combine_H0, total_num_string);
    delete[] temp;
    return ;
}

void CISD_Occupation::make_sign_list(){
    sign_list = vector<int>(string_number[0]+string_number[1] + string_number[2]);
    sign_list[0] = 1;
    for(int i=0; i<string_number[1]; i++){
        sign_list[i+1] = cal_single_sign(i);
    }
    for(int i=0; i<string_number[2]; i++){
        sign_list[i+1 + string_number[1]] = cal_double_sign(i);
    }
}

int CISD_Occupation::get_sign(int excitation, int address){
    int abs_address = address;
    for(int i=0; i<excitation;i++){
        abs_address += string_number[i];
    }
    return sign_list[abs_address];
}

void CISD_Occupation::make_CI_matrix_row(int occupation, vector<double>& value, vector<int>& index, Epetra_Map* CISD_map ){
    int alpha1, beta1, alpha2, beta2, alpha_address1, beta_address1,alpha_address2, beta_address2;
    check_category(occupation, &alpha1, &beta1, &alpha_address1, &beta_address1);
}
void CISD_Occupation::same_spin_interaction(vector<int> occupation1, vector<int> occupation2, vector<double>& value, vector<int>& index, Epetra_Map* CISD_map){
    int num_beta_string = string_number[0] + string_number[1] + string_number[2];
    Epetra_Map sigma_beta_beta_map (num_beta_string, 0, CISD_map->Comm());
    int MyPID = CISD_map->Comm().MyPID();
    int NumProcs = CISD_map->Comm().NumProc();
    int start_index;
    int end_index;
    if(num_orbitals < NumProcs){
        if(MyPID <num_orbitals){
            start_index = MyPID;
            end_index = MyPID+1;
        }
        else{
            start_index = 0;
            end_index = -1;
        }
    }
    else{
        start_index = (int(num_orbitals/NumProcs))*MyPID;
        end_index = start_index + int(num_orbitals/NumProcs);
        if(MyPID==NumProcs-1)
            end_index = num_orbitals;
    }
}



vector< vector<int> > CISD_Occupation::make_GD_list(){
    vector< vector<int> > GD_list;
    GD_list.push_back(total_occupation[0][0][0]);
    for(int i=0; i< string_number[1]; i++)
        GD_list.push_back(total_occupation[0][1][i]);
    return GD_list;
}
int CISD_Occupation::get_GD_size(){
    return (1+string_number[1]);
}
void CISD_Occupation::make_ST_list(vector< vector< vector<int> > > & ST_list){
    ST_list.push_back(total_occupation[0][2]);
    vector<int>  occupation;
    vector<int>  ground_occupation;
    vector<int>  temp_occ;
    vector< vector<int> > tmp_ST_list;

    ground_occupation = total_occupation[0][0][0];
    for(int l=0; l<string_number[2]; l++){
        int i=0;
        int index=0;
        int different[2];
        int k = 0;
        occupation = ST_list[0][l];
        for(int i=0; i<num_alpha_electrons; i++){
            if(occupation[index]!=i){
                different[k] = i;
                k++;
            }
            if(k==2){
                break;
            }
            index++;
        }
        temp_occ = ground_occupation;
        temp_occ.erase(temp_occ.begin()+different[0]);
        temp_occ.push_back(occupation[num_alpha_electrons-2]);
        tmp_ST_list.push_back(temp_occ);
        temp_occ = ground_occupation;
        temp_occ.erase(temp_occ.begin()+different[1]);
        temp_occ.push_back(occupation[num_alpha_electrons-2]);
        tmp_ST_list.push_back(temp_occ);
        temp_occ = ground_occupation;
        temp_occ.erase(temp_occ.begin()+different[0]);
        temp_occ.push_back(occupation[num_alpha_electrons-1]);
        tmp_ST_list.push_back(temp_occ);
        temp_occ = ground_occupation;
        temp_occ.erase(temp_occ.begin()+different[1]);
        temp_occ.push_back(occupation[num_alpha_electrons-1]);
        tmp_ST_list.push_back(temp_occ);
    }
    ST_list.push_back(tmp_ST_list);
}
int CISD_Occupation::get_ST_size(){
    return string_number[2]*8 + 2*string_number[1];
}

void CISD_Occupation::check_category(int coordinate, int* alpha, int* beta, int* alpha_address, int* beta_address){

    if(coordinate >= check_address[0][2] && string_number[2]!=0){
        int temp = coordinate - check_address[0][2];
        *alpha = 0;
        *beta = 2;
        *alpha_address = temp%(string_number[0]);
        *beta_address = temp/(string_number[0]);
    }

    else if(coordinate >= check_address[2][0] && string_number[2]!=0){
        int temp = coordinate - check_address[2][0];
        *alpha = 2;
        *beta = 0;
        *alpha_address = temp%(string_number[2]);
        *beta_address = temp/(string_number[2]);
    }
    else if(coordinate >= check_address[1][1]){
        int temp = coordinate - check_address[1][1];

        *alpha_address = temp%(string_number[1]);
        *beta_address = temp/(string_number[1]);
        *alpha = 1;
        *beta = 1;
    }
    else if(coordinate >= check_address[0][1]){


        int temp = coordinate - check_address[0][1];

        *alpha_address = temp%(string_number[0]);
        *beta_address = temp/(string_number[0]);
        *alpha = 0;
        *beta = 1;
    }
    else if(coordinate >= check_address[1][0]){

        int temp = coordinate - check_address[1][0];
        *alpha_address = temp%(string_number[1]);
        *beta_address = temp/(string_number[1]);
        *alpha = 1;
        *beta = 0;
    }
    else if(coordinate == check_address[0][0]){

        *alpha_address = 0;
        *beta_address = 0;
        *alpha = 0;
        *beta = 0;
    }
}
void CISD_Occupation::write_CI_density(RCP<const Basis> mesh, Array<RCP<State> >& states, Teuchos::RCP<Epetra_MultiVector> CI_coefficient, Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital){
    Verbose::single(Verbose::Detail) << "start writing CI density" << endl;
    clock_t st, end;
    st = clock();
    Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_of_orbital;
    for(int i=0; i<orbital[0]->NumVectors(); i++){
        //Verbose::single() << i << endl;
        Teuchos::RCP<Epetra_Vector> density = rcp(new Epetra_Vector(*mesh->get_map()));
        Teuchos::RCP<Epetra_Vector> temp = rcp(new Epetra_Vector(*orbital[0]->operator()(i)));
        Density_From_Orbitals::compute_orbital_density(mesh, temp, false, density);
        density_of_orbital.append(Teuchos::rcp(new Epetra_Vector(*density)));
    }
    Teuchos::Array< Teuchos::RCP< Epetra_Vector > > CI_density;
    for(int i=0; i<CI_coefficient->NumVectors(); i++){
        Teuchos::RCP<Epetra_Vector> temp = Teuchos::rcp(new Epetra_Vector(*CI_coefficient->operator()(i)));
        CI_density.append(Teuchos::rcp(new Epetra_Vector(*(cal_CI_density(mesh, density_of_orbital, temp, orbital)))));
        string name = parameters->sublist("BasicInformation").get<string>("Label");
        std::stringstream out;
        string tmp_string;
        out << i;
        tmp_string = out.str();
        name = name + "_CISD_density_" + tmp_string;
        mesh->write_cube(name, states[states.size()-1]->get_atoms(), CI_density[i]);
        temp = Teuchos::null;

    }
    for(int i=1; i < CI_coefficient->NumVectors(); i++){
        string name = parameters->sublist("BasicInformation").get<string>("Label");
        Teuchos::RCP<Epetra_Vector> diff = rcp(new Epetra_Vector(*mesh->get_map()));
        diff->Update(1.0, *CI_density[0], -1.0, *CI_density[i], 0.0);
        std::stringstream out;
        string tmp_string;
        out << i;
        tmp_string = out.str();
        name = name + "_CISD_diff_density_" + tmp_string;
        mesh->write_cube(name, states[states.size()-1]->get_atoms(), diff);
        diff = Teuchos::null;
    }
    CI_density.clear();

    end = clock();
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "CI density write time: " << double((end-st))/CLOCKS_PER_SEC << " s" << endl;
    Verbose::single(Verbose::Detail) << "end of writing CI density" << endl;
}
