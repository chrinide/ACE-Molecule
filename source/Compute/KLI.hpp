#pragma once
#include <string>
#include "Compute_XC.hpp"
#include "../State/XC_State.hpp"
#include "Poisson.hpp"

/**
 * @brief The class actually performs the calculation of KLI-approximated exact-exchange functionals.
 * @details KLI approximation is implemented.<br/>
 * Reference for KLI exchange functional : J. B. Krieger, Y. Li, and G. J. Iafrate, <i>Phys. Lett. A</i> <b>148</b>, 470–474 (1990).<br/>
 */


class KLI: public Compute_XC{
    public:
        /**
         * @brief Constructor of KLI class
         * @param mesh Basis data
         * @param function_id Functional ID of KLI
         * @param poisson_solver Poisson_Solver that perform two-electron intergral.
         */
        KLI(
            Teuchos::RCP<const Basis> mesh,
            int function_id,
            Teuchos::RCP<Poisson_Solver> poisson_solver,
            double PD_cutoff/*=-1.0*/
        );

        /**
         * @brief Calculate exchange-correlation energy and potential of given XC_State.
         * @return 0 (normal termination) The computed exchange energy is replaced to exchange_energy, and the exchange_potential is replaced for the last Scf_State.
         * @param mesh Basis data
         * @param states Teuchos::Array of Scf_State object. Last entity is read and updated.
         */
        int compute(
            Teuchos::RCP<XC_State> &xc_info
        );

        /**
         * @details Note that this routine DOES NOT WORK! Without orbital informaiton, KLI exchange cannot be calculated.
         * @return Error termination
         * @param density electron (spin) density
         * @param exchange_energy output,
            exchange energy of given density
         * @param correlation_energy output, correlation energy of given density
         * @param core_density core electron (spin) density, default: null Epetra_Vector
         * @param core_density_grad core electron (spin) density gradient components, default: null Epetra_MultiVector
         */
        void compute_Exc(
            Teuchos::RCP<XC_State> &xc_info
        );

        /**
         * @brief Calculate integrated value of electric density and exchange-correlation potential without orbital
         * @details Note that this routine DOES NOT WORK! Without orbital informaiton, KLI exchange cannot be calculated.
         * @return Error termination
         * @param density electron (spin) density
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         * @param int_n_vxc output, integrated value of electric density times exchange-correlation potential of given system
         * @param exchange_potential exchange potential of given system, default: null Epetra_Vector
         * @param correlation_potential correlation potential of given system, default: null Epetra_Vector
         */
        /*
        void integrate_rho_vxc(
            Teuchos::RCP<XC_State> &xc_info
        );
        */

        /**
         * @brief Calculate exchange-correlation potential without orbitals.
         * @details Note that this routine DOES NOT WORK! Without orbital informaiton, KLI exchange cannot be calculated.
         * @return Error termination
         * @param density electron (spin) density
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         * @param exchange_potential exchange potential of given system
         * @param correlation_potential correlation potential of given system
         */
        void compute_vxc(
            Teuchos::RCP<XC_State> &xc_info
        );

        /**
         * @brief Calculate exchange-correlation energy density without orbitals.
         * @details Note that this routine DOES NOT WORK! Without orbital informaiton, KLI exchange cannot be calculated.
         * @return Error termination
         * @param density electron (spin) density
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         * @param energy_density exchange-correlation energy density of given system
         */
        /*
        void compute_exc(
            Teuchos::RCP<XC_State> &xc_info
        );
		*/
        /**
         * @brief Calculate exchange-correlation kernel with orbitals
         * @details It gives nothing but print that "KLI-PGG exchange kernel will be used."
         * @return Nothing.
         * @param density electron (spin) density
         * @param orbital_coeff orbital coeffecients of the given system
         * @param occupations orbital occupation numbers of the given system
         * @param core_density core electron (spin) density
         * @param core_density_grad core electron (spin) density gradient components
         */
        void compute_kernel(
            Teuchos::RCP<XC_State> &xc_info
        );

        std::string get_functional_type();
        virtual std::string get_info_string(bool verbose = true);
        virtual int get_functional_kind();

    private:
        /**
         * @brief Functional ID of Libxc.
         * @details
         * - -10 : no exchange potential(only energy)
         * - -11 : Slater potential
         * - -12 : OEP-EXX potential with KLI approximation
         * - -13 : OEP-EXX potential(not implemented)
         */
        int function_id;
        double PD_cutoff;

        /**
         * @brief Number of electorns in the system (i.e. with alpha spin)
         * @details ROHF H2 -> number of orbitals (i.e.) 12, index of homo = 1, number of electrons = 1
         */
        std::vector<int> number_of_electrons; ///< Number of electorns in the system (i.e. with alpha spin)
        /**
         * @brief Number of orbitals in State (i.e. with alpha spin)
         * @details ROHF H2 -> number of orbitals (i.e.) 12, index of homo = 1, number of electrons = 1
         */
        std::vector<int> number_of_orbitals;
        /**
         * @brief index of HOMO (i.e. of alpha spin)
         * @details ROHF H2 -> number of orbitals (i.e.) 12, index of homo = 1, number of electrons = 1
         */
        std::vector<int> index_of_HOMO;

        /**
         * @brief Orbital index that will start KLI computation from here.
         **/
        std::vector<int> ind_start;
        std::array<int, 2> orbital_range_mod;

/////////////////////////////////////////////////////

        /**
         * @brief initialize three variables: number_of_electrons, number_of_orbitals, and index_of_HOMO
         * @param occupations occupation number
         * @details ROHF H2 -> number of orbitals (i.e.) 12, index of homo = 1, number of electrons = 1
         */
        void initialize_orbital_numbers(
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations
        );

        /**
         * @brief calculate exchange energy
         * @param orbital_coeff orbital written as coefficient form
         * @param occupations occupation number
         */
        //EXX : require waveftnprod, occupation, K
        double obtain_exchange_energy(
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations
        );

        double obtain_exchange_energy2(
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations
        );

        /**
         * @brief calculate u*psi value
         * @param orbital_coeff orbital written as coefficient form
         * @param occupations occupation number
         * @details
         * \f[
         *     u\psi_{\sigma,i} (\vec{r}) = -\sum_j^{N_\sigma} f_{\sigma,j} \psi_{\sigma,j}(\vec{r})K_{\sigma,ji}(\vec{r})
         * \f]
         */
        //upsi_sigma,i(r) = -sum ( occ_j * psi_sigma,j(r) * K_sigma,ji(r) , j=1..N_sigma)
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > obtain_u_psi(
            Teuchos::Array<Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations
        );
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > obtain_u_psi2(
            Teuchos::Array<Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations
        );
///        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > obtain_u_psi(Teuchos::Teuchos::Array< Teuchos::Teuchos::RCP<Epetra_MultiVector> > eigenvectors, Teuchos::Teuchos::Array< Teuchos::Teuchos::RCP<Epetra_MultiVector> > wavefunction_products, Teuchos::Teuchos::Array<Occupation*> occupations);


        /**
         * @brief calculate slater potential, XPotential = -11
         * @param density electric (spin) density
         * @param orbital_coeff orbital written as coefficient form
         * @param occupations occupation number
         * @param exchange_potential output, slater type exchange potential
         * @details
         * \f[
         *     v_{x,\sigma}^{\mathrm slater} = \sum_i^{N_\sigma} \frac{\rho_{\sigma, i}(\vec{r})  u_{\sigma, i}(\vec{r})}{ \rho_{\sigma}(\vec{r})}
         * \f]
         */
        //slater_potential : require waveftnprod, u, density, occpuations
        //slater potential_sigma(r) = sum(rho_sigma,i(r) * u_sigma,i(r) / rho_sigma(r), i=1..N_sigma)
        //XPotential = -11
        void slater_potential(
            Teuchos::Array<Teuchos::RCP<const Epetra_Vector> > density,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array<Teuchos::RCP<Epetra_Vector> > & exchange_potential
        );
        /**
         * @brief calculate slater potential for KLI calculation, XPotential = -11
         * @param density electric (spin) density
         * @param orbital_coeff orbital written as coefficient form
         * @param u_psi u times psi value. See @ref obtain_u_psi
         * @param occupations occupation number
         * @return exchange_potential output, slater type exchange potential
         * @details
         * \f[
         *     v_{x,\sigma}^{\mathrm slater} = \sum_i^{N_\sigma} \frac{\rho_{\sigma, i}(\vec{r})  u_{\sigma, i}(\vec{r})}{ \rho_{\sigma}(\vec{r})}
         * \f]
         */
        //slater potential_sigma(r) = sum(rho_sigma,i(r) * u_sigma,i(r) / rho_sigma(r), i=1..N_sigma)
        //inside KLI
        Teuchos::Array<Teuchos::RCP<Epetra_Vector> > slater_potential_internal(
            Teuchos::Array<Teuchos::RCP<const Epetra_Vector> > density,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > u_psi,
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations
        );
        /**
         * @brief calculate KLI potential for KLI calculation, XPotential = -12
         * @param density electric (spin) density
         * @param orbital_coeff orbital written as coefficient form
         * @param occupations occupation number
         * @param exchange_potential output, KLI type exchange potential
         */
        //kli_potential : require slater, kli_norm, waveftnprod, density, occupuations
        void kli_potential(
            Teuchos::Array<Teuchos::RCP<const Epetra_Vector> > density,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array<Teuchos::RCP<Epetra_Vector> > & exchange_potential
        );
        /**
         * @brief calculate delta M matrix for KLI calculation
         * @param density electric (spin) density
         * @param orbital_density electric density corresponds to one given orbital
         * @param occupations occupation number
         * @param alpha spin number
         * @return Identity matrix - M matrix. See the details
         * @details
         * delta_M method calculate
         * \f[
         *     \delta_{ji}-M_{\sigma, ji}
         * \f]
         * where \f$M_{\sigma, ji}\f$ is
         * \f[
         *      \int d\vec{r} \frac{\rho_{\sigma, j}(\vec{r}) \rho_{\sigma, i}(\vec{r})}{\rho_{\sigma}(\vec{r})}
         * \f]
         * Note that i,j should be smaller than index_of_HOMO, which means \f$i,j\leq N_\sigma\f$.
         */
        //delta_M = delta_ji - M_sigma,ji
        //M_sigma_ji = int(rho_sigma,j(r) * rho_sigma,i(r) / rho_sigma(r), dr)
        //i, j = 1..N_sigma-1(HOMO-1)
        Teuchos::RCP< Teuchos::SerialDenseMatrix<int, double> > obtain_delta_M(
            Teuchos::RCP<const Epetra_Vector> density,
            Teuchos::RCP<Epetra_MultiVector> orbital_density,
            Teuchos::Array<Teuchos::RCP<const Occupation> > occupations,
            int alpha
        );

        /**
         * @brief calculate slater norm for KLI calculation
         * @param orbital_coeff orbital written as coefficient form
         * @param slater slater potential
         * @param u_psi u_psi function
         * @param alpha spin number
         * @return slater_norm vector
         * @details
         slater_norm method calculate
         * \f[
         *     (\mathrm{(slater norm)}_{\sigma,i} = \left<\sigma,i | v_{x,\sigma}^{\mathrm{slater}} - u_{\sigma,i} | \sigma,i\right>
         * \f]
         * Note that i should be smaller than index_of_HOMO, which means \f$i\leq N_\sigma\f$.
         */
        //slater_norm_sigma,i = <sigma,i|slater_sigma - u_sigma,i|sigma,i>
        //i = 1..N_sigma-1(HOMO-1)
        Teuchos::RCP< Teuchos::SerialDenseMatrix<int, double> > obtain_slater_norm(
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbital_coeff,
            Teuchos::Array<Teuchos::RCP<Epetra_Vector> > slater,
            Teuchos::Array<Teuchos::RCP<Epetra_MultiVector> > u_psi,
            int alpha
        );

        /**
         * @brief calculate kli norm for KLI calculation
         * @param delta_M delta_M (see obtain_delta_M)
         * @param slater_norm slater norm (see obtain_slater_norm)
         * @return kli_norm vector
         * @details
         kli_norm method calculate
         * \f[
         *     (\mathrm{(kli norm)}_{\sigma,i} = \left<\sigma,i | v_{x,\sigma}^{\mathrm{KLI}} - u_{\sigma,i} | \sigma,i\right>
         * \f]
         * Note that i should be smaller than index_of_HOMO, which means \f$i\leq N_\sigma\f$.
         */
        //kli_norm_sigma,i = <sigma,i|kli_sigma - u_sigma,i|sigma,i>
        //i = 1..N_sigma-1(HOMO-1)
        Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > obtain_kli_norm(
            Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > delta_M,
            Teuchos::RCP<Teuchos::SerialDenseMatrix<int, double> > slater_norm
        );

        /**
         * @brief check whether norm of ij pair density is large enough or not
         * @param index_list of orbital pair i,j whose pair density is large enough
         * @param orbital values
         */
        void check_PD_norm(std::vector< std::vector<int> >& index_list, Teuchos::RCP<Epetra_MultiVector> orbitals, int i_spin);
};
