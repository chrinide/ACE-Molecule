#pragma once
#include "Guess.hpp"
#include "../Basis/Basis.hpp"
#include "../Io/Atoms.hpp"
#include "Epetra_MultiVector.h"

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "../Core/Occupation/Occupation.hpp"
#include "../State/State.hpp"


//class State;
class Grid_Cutting: public Guess{
    public:
        Grid_Cutting(Teuchos::Array<Teuchos::RCP<Occupation> > occupations, Teuchos::RCP<Teuchos::ParameterList> parameters, Teuchos::RCP<const Atoms> atoms);
        int compute(Teuchos::RCP<const Basis> mesh, Teuchos::Array<Teuchos::RCP<State> >& states);
    protected:
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        void Size_Up( Teuchos::RCP<const Basis> mesh1, Teuchos::RCP<const Basis> mesh2,Teuchos::Array<Teuchos::RCP<State> >& states );

};
