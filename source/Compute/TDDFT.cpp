#include "TDDFT.hpp"
#include <complex>
#include <stdexcept>
#include <cmath>
#include "Epetra_CrsMatrix.h"
#include "Teuchos_Time.hpp"
#include "Teuchos_TimeMonitor.hpp"

#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Basis/Basis_Function/Finite_Difference.hpp"
#include "../Basis/Basis_Function/Sinc.hpp"
#include "../Util/Two_e_integral.hpp"
#include "../Util/Value_Coef.hpp"
#include "../Util/Integration.hpp"
#include "../Util/Parallel_Manager.hpp"
#include "../Util/Parallel_Util.hpp"
#include "../Util/String_Util.hpp"
#include "../Util/Density_From_Orbitals.hpp"
#include "../Core/Occupation/Occupation_From_Input.hpp"

#define DEBUG (1)// 0 turns off fill-time output.
#define GGA_OUT (1)// 0 turns off GGA_fill_output.
#define _ACE_NEW_CONTRIB_ (1)
#define _HA_TO_EV_ (27.211396132)

using std::abs;
using std::vector;
using std::endl;
using std::string;
using std::setw;
using std::setiosflags;
using std::ios;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::Time;
using Teuchos::TimeMonitor;

TDDFT::TDDFT(RCP<const Basis> mesh, RCP<Teuchos::ParameterList> parameters){
    this -> cutoff = parameters -> sublist("TDDFT").get<double>("TruncationCriteria", 1E-15);
    if(parameters -> sublist("TDDFT").get<int>("Root", 1) != 1){
        Verbose::all() << "Root option is not supported!" << std::endl;
        throw std::invalid_argument("Root option is not supported");
    }
    this -> new_orbital_grad = parameters->sublist("TDDFT").get<int>("Gradient",1)==1;

    string sort_orbital = parameters -> sublist("TDDFT").get<string>("SortOrbital");
    if(sort_orbital != "Order" or sort_orbital != "Tolerance"){
        //Verbose::single() << "TDDFT::compute - The option \"SortOrbital\" is wrong.\n";
        //Verbose::single() << "TDDFT::compute - Set \"SortOrbital\" as \"Order\"\n";
        parameters->sublist("TDDFT").set<string>("SortOrbital", "Order");
    }
    if(sort_orbital == "Tolerance"){
        parameters->sublist("TDDFT").get<double>("OrbitalTolerance", 0.1);
    }

    this->mesh = mesh;
    auto tddft_param = Teuchos::sublist(parameters, "TDDFT");
    this->poisson_solver = Create_Compute::Create_Poisson(mesh, tddft_param);
    Verbose::single(Verbose::Normal) << "TDDFT exchange correlation:" << std::endl;
    this->exchange_correlation = Create_Compute::Create_Exchange_Correlation(mesh, tddft_param, poisson_solver);
    this -> exchange_poisson_solver = Teuchos::rcp_const_cast<Poisson_Solver>(this -> exchange_correlation -> get_poisson_solver());
    this->parameters = parameters;
    this->internal_timer = Teuchos::rcp(new Time_Measure() );
    this -> _exchange_kernel = parameters -> sublist("TDDFT").get<string>("ExchangeKernel");
    if(this -> _exchange_kernel == "HF-EXX"){
       this -> is_tdhf_kli = (parameters -> sublist("TDDFT").get<int>("DeltaCorrection") == 2);
    }

    this -> is_rtriplet = parameters -> sublist("TDDFT").get<int>("RestrictedTriplet", 0) == 1;
    this -> theorylevel = parameters -> sublist("TDDFT").get<string>("TheoryLevel");
    Verbose::single(Verbose::Normal) << "TDDFT: poisson solver is:" << std::endl << this -> poisson_solver -> get_info_string() << std::endl;
    if(this -> exchange_poisson_solver != Teuchos::null){
        Verbose::single(Verbose::Simple) << "TDDFT: exchange_poisson solver is:" << std::endl << this -> exchange_poisson_solver -> get_info_string() << std::endl;
    }
}

TDDFT::~TDDFT(){
    Verbose::single(Verbose::Simple) << "TDDFT Time Profile" << std::endl;
    Verbose::set_numformat(Verbose::Time);
    this -> internal_timer -> print(Verbose::single(Verbose::Simple));
}

Array< RCP<Diagonalize> > TDDFT::_compute(
    RCP<State> &state,
    int matrix_dimension,
    RCP<Epetra_CrsMatrix> overlap_matrix/* = Teuchos::null*/
){
    if(*mesh != (*this->mesh)){
        Verbose::all() << "TDDFT::compute - \"mesh\" in TDDFT is different to given mesh\n";
        Verbose::all() << this->mesh.get() << std::endl << mesh.get() << std::endl;
        throw std::logic_error("TDDFT::compute - \"mesh\" in TDDFT is different to given mesh\n");
    }
    Verbose::single(Verbose::Simple) << "TDDFT::compute - Preparing for calculation......\n";
    if(state->orbitals.size() == 0){
        Verbose::all() << "TDDFT::compute - Orbitals must exist for TDDFT calculation.\n";
        throw std::runtime_error("No orbitals for TDDFT calculations");
    }

    Verbose::set_numformat(Verbose::Occupation);
    for(int s = 0; s < state -> get_occupations().size(); ++s){
        double * norm2 = new double[state -> get_occupations()[s]->get_size()];
        state -> get_orbitals()[s] -> Norm2(norm2);
        for(int i = 0; i < state -> get_occupations()[s]->get_size(); ++i){
            if(norm2[i] < 0.9 or norm2[i] > 1.1){
                Verbose::all() << "2-norm of spin " << s << " orbital #" << i+1 << " is " << norm2[i] << "!" << std::endl;
                throw std::runtime_error("2-norm of spin " + String_Util::to_string(s) + " orbital #" + String_Util::to_string(i+1) + " is " + String_Util::to_string(norm2[i]) + "!");
            }
        }
        delete[] norm2;
    }
    Verbose::set_numformat(Verbose::Energy);
    Verbose::single(Verbose::Normal) << "Orbital energy check" << std::endl;
    for(int s = 0; s < state -> get_occupations().size(); ++s){
        Verbose::single(Verbose::Normal) << "Spin " << s << std::endl;
        for(int i = 0; i < state -> get_occupations()[s]->get_size(); ++i){
            Verbose::single(Verbose::Normal) << state -> get_orbital_energies()[s][i] << "\t";
        }
        Verbose::single(Verbose::Normal) << std::endl;
    }

    // Computing DFT kernel
    exchange_correlation->compute_fxc(state);
    // end
    //RCP< SerialDenseMatrix<int, double> > matrix;
    Epetra_Map config_map(matrix_dimension,0,this -> mesh -> get_map()->Comm());

    string functional_type = exchange_correlation->get_functional_type();
    functional_type = this -> change_exx_kernel(functional_type, this -> _exchange_kernel);
    string theory = parameters -> sublist("TDDFT").get<string>("TheoryLevel");
    Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
    Verbose::single(Verbose::Simple) << " " + functional_type + "-type kernel is used.\n";
    Verbose::single(Verbose::Simple) << " Verbose::SimpleTheory level     : " << theory << "\n";
    Verbose::single(Verbose::Simple) << " Matrix dimension : " << config_map.NumGlobalElements() << "\n";
    std::size_t found = functional_type.find("HF-EXX");
    if(found != string::npos){
        Verbose::single(Verbose::Simple) << " Exchange kernel : HF-EXX\n";
        Verbose::single(Verbose::Simple) << "   [1] CIS : J. Kim, K. Hong, S. Choi, S.-Y. Hwang, and W.Y. Kim, Phys. Chem. Chem. Phys. 17, 31434 (2015).\n";
        Verbose::single(Verbose::Simple) << "   [2] TDDFT : unpublished.\n";
        if(!parameters -> sublist("TDDFT").sublist("OrbitalInfo").isSublist("ExchangeCorrelation")){
            throw std::invalid_argument("HF-EXX kernel requires TDDFT.OrbitalInfo.Exchange_Correlation variables!");
        }
    }
    Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";

    // Compute coupling matrix
    vector<string> functionals = String_Util::strSplit(functional_type, "_");
    vector<double> portions(functionals.size(), 1.0);
    for(vector<string>::iterator it = functionals.begin(); it != functionals.end(); ++it){
        if(*it == "HF" or *it == "HF-EXX"){
            portions[it-functionals.begin()] = this -> exchange_correlation -> get_EXX_portion();
        }
    }

    Array< RCP<Diagonalize> > diagonalizes;
    if(state -> occupations.size() == 1){
        internal_timer->start("TDDFT::construct_matrix");
        Array< RCP<Epetra_CrsMatrix> > sparse_matrices;
        int num_mat = (this -> is_rtriplet)? 2: 1;
        for(int s = 0; s < num_mat; ++s){
            sparse_matrices.append( rcp(new Epetra_CrsMatrix(Copy,config_map,0)) );
            diagonalizes.append(Create_Diagonalize::Create_Diagonalize(Teuchos::sublist(Teuchos::sublist(parameters,"TDDFT"), "Diagonalize")));
        }
        this -> get_restricted_TD_matrix(functionals, portions, iroot, state, sparse_matrices);
        // end

        for(int s = 0; s < num_mat; ++s){
            sparse_matrices[s]->FillComplete(); //shchoi sparse
        }
        internal_timer -> end("TDDFT::construct_matrix");

        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT::construct_matrix");
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";

        internal_timer -> start("TDDFT::compute diagonalize");
        int num_excitation = Teuchos::sublist(parameters, "TDDFT")->get<int>("NumberOfStates");
        for(int s = 0; s < num_mat; ++s){
            auto initial_eigenvectors = rcp(new Epetra_MultiVector( config_map, num_excitation ) );
            initial_eigenvectors->Random();
            diagonalizes[s]->diagonalize(sparse_matrices[s], num_excitation, initial_eigenvectors, overlap_matrix);
        }
        internal_timer -> end("TDDFT::compute diagonalize");
    }

    return diagonalizes;
}

void TDDFT::postprocessing(
        vector<double> excitation_energies,
        RCP<Epetra_MultiVector> eigenvectors,
        RCP<State> state,
        int num_excitation
){
    bool is_abba = (this -> type == "ABBA")? true: false;
    internal_timer->start("TDDFT postprocessing"); //postprocessing
    // Compute oscillator strength
    vector<double> oscillator_strength(num_excitation);
    vector< vector<double> > state_overlap(3);
    Verbose::single(Verbose::Simple) << "TDDFT::compute - Compute oscillator strength\n";
    internal_timer->start("TDDFT::compute oscillator strength");
    this -> calculate_oscillator_strength(excitation_energies,
            eigenvectors, state,
            this -> iroot, 0, num_excitation,
            state_overlap, oscillator_strength, is_abba);
    internal_timer->end("TDDFT::compute oscillator strength");
    internal_timer-> print(Verbose::single(Verbose::Simple), "TDDFT::compute oscillator strength");
    vector< vector<double> > ct_character;

    this -> CT_analysis(excitation_energies,
            eigenvectors, state,
            this -> iroot, 0, num_excitation,
            ct_character, this -> spatial_overlap, is_abba);
    // end

    double tolerance = 0.0;
    int default_max_order = (is_abba)? eigenvectors -> GlobalLength()/2: eigenvectors -> GlobalLength();
    default_max_order = std::min(10, default_max_order);
    int order = default_max_order;
    internal_timer -> start("TDDFT_print");
    if(parameters->sublist("TDDFT").get<string>("SortOrbital") == "Tolerance"){
        tolerance = parameters->sublist("TDDFT").get<double>("OrbitalTolerance");
    }
    else if(parameters->sublist("TDDFT").get<string>("SortOrbital") == "Order"){
        order = parameters->sublist("TDDFT").get<int>("MaximumOrder", default_max_order);
    }
    internal_timer -> start("print_excitation");
    this -> print_excitation_info(tolerance, order,
            eigenvectors,
            this -> iroot, index_of_HOMO[0], number_of_orbitals[0],
            excitation_energies, ct_character,
            state_overlap, oscillator_strength, is_abba);

    Verbose::set_numformat(Verbose::Time);
    internal_timer -> end("print_excitation");
    internal_timer -> print(Verbose::single(Verbose::Simple), "print_excitation");
    internal_timer -> end("TDDFT_print");
    internal_timer-> print(Verbose::single(Verbose::Simple), "TDDFT_print");
    internal_timer-> end("TDDFT postprocessing");
    internal_timer-> print(Verbose::single(Verbose::Simple), "TDDFT postprocessing");
}

void TDDFT::print_excitation_info(
    double tolerance, int order,
    RCP<Epetra_MultiVector> eigenvectors,
    int iroot, int iHOMO, int num_orbitals,
    std::vector<double> excitation_energies,
    std::vector< std::vector<double> > ct_character,
    std::vector< std::vector<double> > state_overlap,
    std::vector<double> oscillator_strength,
    bool upper_half/* = false*/
){
    int num_excitation = excitation_energies.size();
    vector< vector<int> > determinant_index;
    for(int i=iroot; i<iHOMO; ++i){
        for(int a=iHOMO; a<num_orbitals; ++a){
            vector<int> tmp;
            tmp.push_back(i+1);
            tmp.push_back(a+1);
            determinant_index.push_back(tmp);
        }
    }
    for(int i = 0; i < num_excitation; ++i){
        Verbose::set_numformat(Verbose::Energy);
        Verbose::single(Verbose::Simple) << " #" << i+1 << ": Excitation energy = " << excitation_energies[i] << " Hartree = " << excitation_energies[i] * 27.211396132 << " eV\n";
        Verbose::set_numformat(Verbose::Scientific);
        Verbose::single(Verbose::Simple) << "     <x> = " << std::scientific << state_overlap[0][i] << "\n";
        Verbose::single(Verbose::Simple) << "     <y> = " << std::scientific << state_overlap[1][i] << "\n";
        Verbose::single(Verbose::Simple) << "     <z> = " << std::scientific << state_overlap[2][i] << "\n";
        Verbose::single(Verbose::Simple) << "     Oscillator strength = " << std::scientific << oscillator_strength[i] << "\n";
        Verbose::single(Verbose::Simple) << "     Orbital overlap = " << std::scientific << ct_character.at(0)[i] << "\n";
        Verbose::single(Verbose::Simple) << "     r-index = " << std::scientific << ct_character.at(1)[i] << "\n";
        Verbose::single(Verbose::Simple) << "     (Gamma-r)-index = " << std::scientific << ct_character[2][i]-ct_character[1][i] << "\n";
        Verbose::single(Verbose::Simple) << "     Gamma-index = " << std::scientific << ct_character.at(2)[i] << "\n";
        print_contribution(eigenvectors, i, determinant_index, order, tolerance, upper_half);
    }
}

string TDDFT::change_exx_kernel(string functional_type, string exchange_kernel, string exx_default/* = "PGG"*/){
    if(exchange_kernel.size() == 0){
        return functional_type;
    }
    std::size_t found = functional_type.find("EXX");
    if(found != string::npos){
        if(exchange_kernel == "HF"){
            functional_type.replace(found, 3, "HF");
        } else if(exchange_kernel == "KLI-PGG" or exchange_kernel == "PGG"){
            functional_type.replace(found, 3, "PGG");
        } else if(exchange_kernel == "HF-EXX"){
            functional_type.replace(found, 3, "HF-EXX");
        } else {
            Verbose::single() << "Defaulting the EXX kernel to "+exx_default+"." << std::endl;
            functional_type.replace(found, 3, exx_default);
        }
    }
    found = functional_type.find("KLI-PGG");
    if(found != string::npos){
        if(exchange_kernel == "HF"){
            functional_type.replace(found, 7, "HF");
        } else if(exchange_kernel == "KLI-PGG" or exchange_kernel == "PGG"){
            functional_type.replace(found, 7, "PGG");
        } else if(exchange_kernel == "HF-EXX"){
            functional_type.replace(found, 7, "HF-EXX");
        } else {
            Verbose::single(Verbose::Normal) << "Defaulting the EXX kernel to PGG." << std::endl;
            functional_type.replace(found, 7, "PGG");
        }
    }
    found = functional_type.find("KLI");
    if(found != string::npos){
        if(exchange_kernel == "HF"){
            functional_type.replace(found, 3, "HF");
        } else if(exchange_kernel == "KLI-PGG" or exchange_kernel == "PGG"){
            functional_type.replace(found, 3, "PGG");
        } else if(exchange_kernel == "HF-EXX"){
            functional_type.replace(found, 3, "HF-EXX");
        } else {
            Verbose::single(Verbose::Normal) << "Defaulting the KLI kernel to PGG." << std::endl;
            functional_type.replace(found, 3, "PGG");
        }
    }
    return functional_type;
}

void TDDFT::calculate_oscillator_strength(
        vector<double> excitation_energies,
        RCP<Epetra_MultiVector> TD_excitation,
        RCP<State> state,
        int start, int ispin,
        int num_excitation,
        vector< vector<double> > &state_overlap,
        vector<double> &oscillator_strength,
        bool is_deexcit/* = false*/
){
    auto map = mesh->get_map();
    auto config_map = TD_excitation -> Map();
    int matrix_dimension = config_map.NumGlobalElements();
    state_overlap.resize(3);
    oscillator_strength = vector<double>(num_excitation);
    for(int d = 0; d < 3; ++d){
        state_overlap[d] = vector<double>(num_excitation);
    }
    // R F_I = Omega_I^2 F_I
    auto F_I = TD_excitation; // RCP<MV>
    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();
    const double** scaled_grid = mesh->get_scaled_grid();

    int config_NumMyElements = config_map.NumMyElements();
    int* config_MyGlobalElements = config_map.MyGlobalElements();

    const int mat_size = is_deexcit? matrix_dimension/2: matrix_dimension;

    double* orbital_overlap_x = new double [mat_size]();
    double* orbital_overlap_y = new double [mat_size]();
    double* orbital_overlap_z = new double [mat_size]();
    double* overlap_x = new double [mat_size]();
    double* overlap_y = new double [mat_size]();
    double* overlap_z = new double [mat_size]();

    int matrix_index_r = 0;
    this -> eps_diff = vector<double>(mat_size);

    for(int i=start; i<index_of_HOMO[ispin]; ++i){
        for(int a=index_of_HOMO[ispin]; a<number_of_orbitals[ispin]; ++a){
            for(int k=0; k<NumMyElements; ++k){
                int k_x=0, k_y=0, k_z=0;
                mesh->decompose(MyGlobalElements[k], &k_x, &k_y, &k_z);
                double x = scaled_grid[0][k_x];
                double y = scaled_grid[1][k_y];
                double z = scaled_grid[2][k_z];

                overlap_x[matrix_index_r] += state->get_orbitals()[ispin]->operator[](i)[k] * state->get_orbitals()[ispin]->operator[](a)[k] * x;
                overlap_y[matrix_index_r] += state->get_orbitals()[ispin]->operator[](i)[k] * state->get_orbitals()[ispin]->operator[](a)[k] * y;
                overlap_z[matrix_index_r] += state->get_orbitals()[ispin]->operator[](i)[k] * state->get_orbitals()[ispin]->operator[](a)[k] * z;
            }
            this -> eps_diff[matrix_index_r] = sqrt(state -> orbital_energies[ispin][a] - state -> orbital_energies[ispin][i]);
            ++matrix_index_r;
        }
    }

    Parallel_Util::group_sum(overlap_x, orbital_overlap_x, mat_size);
    Parallel_Util::group_sum(overlap_y, orbital_overlap_y, mat_size);
    Parallel_Util::group_sum(overlap_z, orbital_overlap_z, mat_size);

    delete[] overlap_x;
    delete[] overlap_y;
    delete[] overlap_z;

    vector< vector<double> > z_vec = this -> gather_z_vector(F_I, state -> orbital_energies[ispin]);
    for(int i=0; i<num_excitation; ++i){
        double r_portion[3] = {0.0, 0.0, 0.0};
        for(int j = 0; j < mat_size; ++j){
            r_portion[0] += z_vec[i][j] * orbital_overlap_x[j];
            r_portion[1] += z_vec[i][j] * orbital_overlap_y[j];
            r_portion[2] += z_vec[i][j] * orbital_overlap_z[j];
        }

        for(int d = 0; d < 3; ++d){
            r_portion[d] *= eps_diff[i];
            state_overlap[d][i] = sqrt(2.0) * r_portion[d];
        }

        oscillator_strength[i] = 2.0 / 3.0 * (r_portion[0]*r_portion[0] + r_portion[1]*r_portion[1] + r_portion[2]*r_portion[2]);
        if(state -> get_orbitals().size() == 1){
            oscillator_strength[i] *= 2;
        }
    }

    delete[] orbital_overlap_x;
    delete[] orbital_overlap_y;
    delete[] orbital_overlap_z;
}

void TDDFT::CT_analysis(
        vector<double> excitation_energies,
        RCP<Epetra_MultiVector> TD_excitation,
        RCP<State> state,
        int start, int ispin,
        int num_excitation,
        vector< vector<double> > &ct_character,
        vector<double> &spatial_overlap,
        bool is_deexcit/* = false*/
){
    internal_timer->start("TDDFT CTcal");
    start = 0;
    int NumMyElements = this -> mesh -> get_map()->NumMyElements();
    int* MyGlobalElements = this -> mesh -> get_map()->MyGlobalElements();
    const double** scaled_grid = mesh->get_scaled_grid();
    const int matrix_dimension = TD_excitation -> Map().NumGlobalElements();

    const int mat_size = is_deexcit? matrix_dimension/2: matrix_dimension;
    const int num_virt_orb = this -> number_of_orbitals[ispin] - this -> index_of_HOMO[ispin];
    const int num_occ_orb = this -> index_of_HOMO[ispin] - start;
    vector< vector<double> > z_vec = this -> gather_z_vector(TD_excitation, state -> orbital_energies[ispin]);

    double* orbital_overlap = new double [mat_size]();
    double* overlap = new double [mat_size]();

    int matrix_index_r = 0;
    if(this -> eps_diff.size() != mat_size){
        this -> eps_diff = vector<double>(mat_size);
        for(int i=start; i<index_of_HOMO[ispin]; ++i){
            for(int a=index_of_HOMO[ispin]; a<number_of_orbitals[ispin]; ++a){
                this -> eps_diff[matrix_index_r] = sqrt(state -> orbital_energies[ispin][a] - state -> orbital_energies[ispin][i]);
                ++matrix_index_r;
            }
        }
    }
    matrix_index_r = 0;
    for(int i=start; i<index_of_HOMO[ispin]; ++i){
        for(int a=index_of_HOMO[ispin]; a<number_of_orbitals[ispin]; ++a){
            for(int k=0; k<NumMyElements; ++k){
                overlap[matrix_index_r] += std::abs(state->get_orbitals()[ispin]->operator[](i)[k]) * std::abs(state->get_orbitals()[ispin]->operator[](a)[k]);
            }
            ++matrix_index_r;
        }
    }

    Parallel_Util::group_sum(overlap, orbital_overlap, mat_size);
    delete[] overlap;

    double* orbital_centoid_x = new double [number_of_orbitals[ispin]]();
    double* orbital_centoid_y = new double [number_of_orbitals[ispin]]();
    double* orbital_centoid_z = new double [number_of_orbitals[ispin]]();
    double* centoid_x = new double [number_of_orbitals[ispin]]();
    double* centoid_y = new double [number_of_orbitals[ispin]]();
    double* centoid_z = new double [number_of_orbitals[ispin]]();

    double* orbital_centoid_x2 = new double [number_of_orbitals[ispin]]();
    double* orbital_centoid_y2 = new double [number_of_orbitals[ispin]]();
    double* orbital_centoid_z2 = new double [number_of_orbitals[ispin]]();
    double* centoid_x2 = new double [number_of_orbitals[ispin]]();
    double* centoid_y2 = new double [number_of_orbitals[ispin]]();
    double* centoid_z2 = new double [number_of_orbitals[ispin]]();
    for(int k=0; k<NumMyElements; ++k){
        int k_x=0, k_y=0, k_z=0;
        mesh->decompose(MyGlobalElements[k], &k_x, &k_y, &k_z);
        double x = scaled_grid[0][k_x];
        double y = scaled_grid[1][k_y];
        double z = scaled_grid[2][k_z];
        for(int i = start; i < number_of_orbitals[ispin]; ++i){
            centoid_x[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * x;
            centoid_y[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * y;
            centoid_z[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * z;

            centoid_x2[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * x*x;
            centoid_y2[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * y*y;
            centoid_z2[i] += pow(state->get_orbitals()[ispin]->operator[](i)[k],2) * z*z;
        }
    }

    Parallel_Util::group_sum(centoid_x, orbital_centoid_x, number_of_orbitals[ispin]-start);
    Parallel_Util::group_sum(centoid_y, orbital_centoid_y, number_of_orbitals[ispin]-start);
    Parallel_Util::group_sum(centoid_z, orbital_centoid_z, number_of_orbitals[ispin]-start);
    delete[] centoid_x; delete[] centoid_y; delete[] centoid_z;

    Parallel_Util::group_sum(centoid_x2, orbital_centoid_x2, number_of_orbitals[ispin]-start);
    Parallel_Util::group_sum(centoid_y2, orbital_centoid_y2, number_of_orbitals[ispin]-start);
    Parallel_Util::group_sum(centoid_z2, orbital_centoid_z2, number_of_orbitals[ispin]-start);
    delete[] centoid_x2; delete[] centoid_y2; delete[] centoid_z2;


    // Orbital spatial overlap calculation from DOI: 10.1063/1.2831900
    vector< vector<double> > kappa(num_excitation);
    vector<double> kappa2(num_excitation);
    for(int i = 0; i < num_excitation; ++i){
        kappa[i] = vector<double>(z_vec[i].begin(), z_vec[i].end());
        for(int j = 0; j < mat_size; ++j){
            kappa[i][j] *= eps_diff[j];
            kappa2[i] += kappa[i][j]*kappa[i][j];
        }
    }

    vector<double> lambda(num_excitation);
    // CT excitation distance (Delta-r index) calculation from DOI: 10.1021/ct400337e
    vector<double> r_ind(num_excitation);

    for(int i = 0; i < num_excitation; ++i){
        for(int j = 0; j < mat_size; ++j){
            int ji = j / num_virt_orb;
            int ja = j % num_virt_orb + this -> index_of_HOMO[ispin];
            double dx2 = pow(orbital_centoid_x[ji]-orbital_centoid_x[ja],2);
            double dy2 = pow(orbital_centoid_y[ji]-orbital_centoid_y[ja],2);
            double dz2 = pow(orbital_centoid_z[ji]-orbital_centoid_z[ja],2);
            r_ind[i] += kappa[i][j]*kappa[i][j]*std::sqrt(dx2+dy2+dz2);
            lambda[i] += kappa[i][j]*kappa[i][j]*orbital_overlap[j];
        }
    }

    for(int i = 0; i < num_excitation; ++i){
        r_ind[i] /= kappa2[i];
        lambda[i] /= kappa2[i];
    }

    vector<double> var_x(number_of_orbitals[ispin]), var_y(number_of_orbitals[ispin]), var_z(number_of_orbitals[ispin]);
    for(int i = 0; i < number_of_orbitals[ispin]; ++i){
        var_x[i] = orbital_centoid_x2[i]-pow(orbital_centoid_x[i],2);
        var_y[i] = orbital_centoid_y2[i]-pow(orbital_centoid_y[i],2);
        var_z[i] = orbital_centoid_z2[i]-pow(orbital_centoid_z[i],2);
    }

    spatial_overlap = vector<double>(orbital_overlap, orbital_overlap+mat_size);
    ct_character.push_back(lambda);
    delete[] orbital_overlap;
    // delete[] orbital_centoid_r at next section.
    ct_character.push_back(r_ind);
    // END CT excitation distance (Delta-r index) calculation from DOI: 10.1021/ct400337e
    delete[] orbital_centoid_x; delete[] orbital_centoid_y; delete[] orbital_centoid_z;
    delete[] orbital_centoid_x2; delete[] orbital_centoid_y2; delete[] orbital_centoid_z2;
    // END Orbital spatial overlap calculation from DOI: 10.1063/1.2831900

    vector<double> sigma_ind(num_excitation);
    vector<double> lambda_ind(r_ind.begin(), r_ind.end());

    for(int i = 0; i < num_excitation; ++i){
        for(int j = 0; j < mat_size; ++j){
            int ji = j / num_virt_orb;
            int ja = j % num_virt_orb + this -> index_of_HOMO[ispin];
            sigma_ind[i] += kappa[i][j]*kappa[i][j]*std::abs(std::sqrt(var_x[ja]+var_y[ja]+var_z[ja])-std::sqrt(var_x[ji]+var_y[ji]+var_z[ji]));
        }
        sigma_ind[i] /= kappa2[i];
        lambda_ind[i] += sigma_ind[i];
    }
    ct_character.push_back(lambda_ind);
    internal_timer->end("TDDFT CTcal");
    Verbose::set_numformat(Verbose::Time);
    internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT CTcal");
    Verbose::single(Verbose::Normal) << "Gammaa index calculation end" << std::endl;
    // END CT excitation distance (Gamma index) calculation from DOI: 10.1063/1.4867007
}

void TDDFT::print_contribution(RCP< Epetra_MultiVector > eigen_vectors, int index, vector< vector<int> > determinant_index, int num_request, double tolerance, bool upper_half/* = false*/){
    double* evec = eigen_vectors->operator[](index);
    ///////////////// Gather values on root CPU /////////////
    int total_size = eigen_vectors->GlobalLength();
    double* total_evec = new double[total_size];

    if(upper_half){
        total_size /= 2;
    }
    int* total_index = new int[total_size];
    for(int i =0; i<total_size;++i){
        total_index[i]=i;
    }
    vector< vector<double> > z_vec = this -> gather_z_vector(eigen_vectors, this -> state -> orbital_energies[0]);
    memcpy(total_evec, z_vec[index].data(), total_size*sizeof(double));
    ///////////////////////////// Sort & Print /////////////////////////////////
    Verbose::set_numformat(Verbose::Scientific);
    if(Parallel_Manager::info().get_mpi_rank()==0){
        std::sort(total_index, total_index+total_size, [&total_evec](int i1, int i2) {return total_evec[i1]*total_evec[i1] > total_evec[i2]*total_evec[i2];});

#ifdef _ACE_NEW_CONTRIB_
        double total_eps = 0.0;
        for(int i = 0; i < total_size; ++i){
            total_eps += pow(total_evec[i]*this->eps_diff[i],2);
        }
        Verbose::single(Verbose::Simple) << "     Eigenvalue contribution = " << std::scientific << total_eps << "\n";
#endif
        for(int i=0; i<num_request; ++i){
            if(fabs(total_evec[total_index[i]])<tolerance) break;
            int i2 = total_index[i];
#ifdef _ACE_NEW_CONTRIB_
            Verbose::single(Verbose::Simple) << "      " << setw(4) << setiosflags(ios::right) << i2 << ": " << setw(15) << setiosflags(ios::right) << total_evec[i2]
                << " (" << setw(4) << setiosflags(ios::right) << determinant_index[i2][0] << " to " << setw(4) << setiosflags(ios::right) << determinant_index[i2][1] << ")"
                << ", (Eigval = " << this -> eps_diff[i2]*this -> eps_diff[i2] * _HA_TO_EV_ << " eV, overlap = " << this -> spatial_overlap[i2] << ")"
                << "\n";
#else
            Verbose::single(Verbose::Simple) << "      " << setw(4) << setiosflags(ios::right) << i2 << ": " << setw(15) << setiosflags(ios::right) << total_evec[i2]
                << " (" << setw(4) << setiosflags(ios::right) << determinant_index[i2][0] << " to " << setw(4) << setiosflags(ios::right) << determinant_index[i2][1] << ")"
                <<"\n";
#endif
        }
    }
    Verbose::set_numformat();

    delete[] total_evec;
    delete[] total_index;

    return;
}

//void TDDFT::initialize_orbital_numbers(Array< RCP<const Occupation> > occupations){
void TDDFT::initialize_orbital_numbers(RCP<State> &state){
    /*
    if(this -> parameters -> sublist("TDDFT").isParameter("Root")){
        int iroot2 = this -> parameters -> sublist("TDDFT").get<int>("Root", 1)-1;
        //if(iroot2 < 1) break;
        Array< RCP<Epetra_MultiVector> > orbitals2;
        Array< RCP<Occupation> > occupations2;
        Array< vector<double> > orbital_energies2;
        for(int is = 0; is < state -> get_occupations().size(); ++is){
            int orbsize2 = state -> get_occupations()[is]->get_size()-iroot2;
            RCP<const Occupation> orig_occ = state -> get_occupations()[is];
            orbitals2.append(rcp(new Epetra_MultiVector(Copy, *state->get_orbitals()[is], iroot2, orbsize2)));
            vector<double> old_occ_list = orig_occ -> get_occupation_list();
            vector<double> old_eigvals = orig_occ -> get_eigenvalues();
            vector<double> new_occ_list = vector<double>(old_occ_list.begin()+iroot2, old_occ_list.end());
            vector<double> new_eigvals;
            if(orig_occ->get_eigenvalues().size() > iroot2){
                new_eigvals = vector<double>(old_eigvals.begin()+iroot2, old_eigvals.end());
            }
            occupations2.append(rcp(new Occupation_From_Input(orig_occ->get_num_occ_orbitals()-iroot2, new_occ_list, new_eigvals)));
            vector<double> old_orb_energies = state -> get_orbital_energies()[is];
            orbital_energies2.append(vector<double>(old_orb_energies.begin()+iroot2, old_orb_energies.end()));
        }
        state -> orbitals = orbitals2;
        state -> occupations = occupations2;
        state -> orbital_energies = orbital_energies2;
    }
    */
    auto occupations = state -> get_occupations();
    number_of_orbitals = vector<int>(occupations.size());
    number_of_electrons = vector<int>(occupations.size());
    index_of_HOMO = vector<int>(occupations.size());

    //this -> iroot = this -> parameters -> sublist("TDDFT").get<int>("Root", 1)-1;
    this -> iroot = 0;
    if(this -> iroot < 0) this -> iroot = 0;

    for(int i_spin=0; i_spin<occupations.size(); ++i_spin){
        number_of_orbitals[i_spin] = occupations[i_spin]->get_size();
        number_of_electrons[i_spin] = ceil(occupations[i_spin]->get_total_occupation());
        index_of_HOMO[i_spin] = number_of_orbitals[i_spin];

        if(occupations.size() == 1){
            number_of_electrons[0] = ceil(0.5*(occupations[i_spin]->get_total_occupation()));
        }

        if(number_of_electrons[i_spin] < index_of_HOMO[i_spin]){
            for(int i=number_of_orbitals[i_spin]-1; i>=0; --i){
                if(occupations[i_spin]->operator[](i) < 1.0E-30){
                    --index_of_HOMO[i_spin];
                }
                else{
                    break;
                }
            }
        }
        Verbose::single(Verbose::Simple) << "\n#----------------------------------------- TDDFT::initialize_orbital_numbers" << endl;
        Verbose::single(Verbose::Simple) << " starting orbital: " << iroot << "\tnumber_of_orbitals[" << i_spin << "] : " << number_of_orbitals[i_spin] << "\tindex_of_HOMO: " << index_of_HOMO[i_spin] << "\tnumber_of_electrons: " << number_of_electrons[i_spin] << endl;
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" << endl;

        if(this -> iroot > index_of_HOMO[i_spin]){
            Verbose::all() << "No occupied orbitals included for calculations!" << std::endl;
            throw std::invalid_argument("No occupied orbitals included for calculations!");
        }

        if(number_of_orbitals[i_spin] <= index_of_HOMO[i_spin]){
            Verbose::all() << "No virtual orbitals included for calculations!" << std::endl;
            throw std::invalid_argument("No virtual orbitals included for calculations!");
        }
    }
    return;
}

void TDDFT::get_restricted_TD_matrix(
        vector<string> functionals,
        vector<double> portions,
        int iroot,
        RCP<State> state,
        Array< RCP<Epetra_CrsMatrix> >& sparse_matrix
){
    bool multiply_orbitals = false;
    bool calculate_grad = false;
    bool calculate_pgg = false;
    double hf_portion = 0.0;
    double ks_ci_portion = 0.0;
    if(iroot >= index_of_HOMO[0]) iroot = 0;
    for(vector<string>::iterator it = functionals.begin(); it != functionals.end(); ++it){
        if(*it == "LDA"){
            multiply_orbitals = true;
        } else if(*it == "GGA"){
            multiply_orbitals = true;
            calculate_grad = true;
        } else if(*it == "PGG"){
            calculate_pgg = true;
        } else if(*it == "HF"){
            hf_portion = portions.at(it-functionals.begin());
        } else if(*it == "HF-EXX"){
            ks_ci_portion = portions.at(it-functionals.begin());
        } else {
            throw std::invalid_argument("Unsupported functional (" + *it + ") found.");
        }
    }
    bool is_pgg_upper_only = (this -> type == "TDA")? true: false;

    auto map = mesh->get_map();

    int matrix_index_c = 0;
    int matrix_index_r = 0;

    Array< RCP<Epetra_MultiVector> > orbitals;
    Array< RCP<Epetra_MultiVector> > saved_grad_density;
    for(int i_spin=0; i_spin<state->get_occupations().size(); ++i_spin){
        orbitals.push_back(rcp(new Epetra_MultiVector(*map, state->get_orbitals()[i_spin]->NumVectors())));
        orbitals[i_spin]->Update(1.0, *state->get_orbitals()[i_spin], 0.0);
        Value_Coef::Value_Coef(mesh, orbitals[i_spin], false, true, orbitals[i_spin]);
        if(calculate_grad){
            saved_grad_density.push_back(rcp(new Epetra_MultiVector(*map, 3)));
        }
    }

    RCP<Epetra_Vector> Kia = rcp(new Epetra_Vector(*map));

    // For LDA & GGA
    RCP<Epetra_Vector> ia_coeff;
    RCP<Epetra_Vector> jb_coeff;
    if(multiply_orbitals){
        ia_coeff = rcp(new Epetra_Vector(*map));
        jb_coeff = rcp(new Epetra_Vector(*map));
    }
    // end

    // For GGA
    RCP<Epetra_MultiVector> grad_ia;
    RCP<Epetra_MultiVector> grad_jb;
    RCP<Epetra_MultiVector> vsigma_grad_ia;
    RCP<Epetra_Vector> v2rho2_ia_coeff;
    RCP<Epetra_Vector> grad_rho_dot_grad_ia;
    RCP<Epetra_Vector> grad_rho_dot_grad_jb;
    RCP<Epetra_MultiVector> tmp_mv;
    if(calculate_grad){
        this -> exchange_correlation -> get_saved_grad_density(saved_grad_density);
        this -> construct_gradient_matrix();
        grad_ia = rcp(new Epetra_MultiVector(*map, 3));
        grad_jb = rcp(new Epetra_MultiVector(*map, 3));
        v2rho2_ia_coeff = rcp(new Epetra_Vector(*map));
        vsigma_grad_ia = rcp(new Epetra_MultiVector(*map, 3));
        grad_rho_dot_grad_ia = rcp(new Epetra_Vector(*map));
        grad_rho_dot_grad_jb = rcp(new Epetra_Vector(*map));
        tmp_mv = rcp(new Epetra_MultiVector(*map, 3));
    }
    // end
    // fxc^PGG contribution
    RCP<Epetra_Vector> i_over_density;
    RCP<Epetra_Vector> ia_over_density;
    RCP<Epetra_Vector> kernel_ia;
    RCP<Epetra_Vector> j_over_density;
    RCP<Epetra_Vector> jb_over_density;
    Array< Teuchos::RCP<Epetra_MultiVector> > vsigma ;
    Array< Teuchos::RCP<Epetra_MultiVector> > v2rho2 ;
    RCP<Epetra_MultiVector> vsigma_coeff ;
    if(calculate_pgg){
        i_over_density = rcp(new Epetra_Vector(*map));
        ia_over_density = rcp(new Epetra_Vector(*map));
        kernel_ia = rcp(new Epetra_Vector(*map));
        j_over_density = rcp(new Epetra_Vector(*map));
        jb_over_density = rcp(new Epetra_Vector(*map));
    }
    // end

    std::vector<double> iajb = get_iajb(orbitals[0], false);
    int index_iajb = 0;
    //std::vector<double> iajb = get_iajb(orbitals[0]);
    if(calculate_grad){
        if(this -> new_orbital_grad){
            Verbose::single(Verbose::Detail) << "Gradient of pair density will be calculated using saved density gradient" << std::endl;
            for(int axis=0; axis<3; axis++){
                gradient.push_back(rcp(new Epetra_MultiVector(*mesh->get_map(), number_of_orbitals[0] )));
            }
            RCP<Epetra_MultiVector> tmp = rcp(new Epetra_MultiVector(*orbitals[0]));
            //Value_Coef::Value_Coef(mesh, tmp, false, true, tmp);
            for(int axis=0; axis<3; axis++){
                gradient_matrix[axis]->Multiply(false, *tmp, *gradient[axis]);
            }
        }
        else{
            Verbose::single(Verbose::Detail) << "Gradient of pair density will be calculated using gradient matrix" << std::endl;
        }
        vsigma = exchange_correlation->get_vsigma();
        v2rho2 = exchange_correlation->get_v2rho2();
        vsigma_coeff = rcp(new Epetra_MultiVector(*vsigma[0]));
        Value_Coef::Value_Coef(mesh, vsigma[0], true, false, vsigma_coeff);
    }


    for(int i=iroot; i<index_of_HOMO[0]; ++i){
#if DEBUG == 1
        internal_timer -> start("i-loop");
#endif
        // fxc^PGG contribution
        if(calculate_pgg){
            i_over_density->ReciprocalMultiply(1.0, *state->get_density()[0], *orbitals[0]->operator()(i), 0.0);
        }
        // end
        for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
#if DEBUG == 1
//        internal_timer -> start("ia-loop");
#endif
            matrix_index_r = matrix_index_c;

            // Hartree contribution
            // 170822
            /*
            Kia->PutScalar(0.0);
            Two_e_integral::compute_Kji(mesh, orbitals[0]->operator()(i), orbitals[0]->operator()(a), poisson_solver, Kia);
            */
            // end

            if(multiply_orbitals){
                //ia_coeff->Multiply(1.0, *state->get_orbitals()[0]->operator()(i), *state->get_orbitals()[0]->operator()(a), 0.0);
                //Value_Coef::Value_Coef(mesh, ia_coeff, false, true, ia_coeff);
                ia_coeff->Multiply(1.0, *state->get_orbitals()[0]->operator()(i), *orbitals()[0]->operator()(a), 0.0);
            }

            if(calculate_grad){
                //grad_ia->PutScalar(0.0);
                get_gradient_ia_RI_using_matrix(state->get_orbitals()[0], i, a, grad_ia);
                //get_gradient_ia_RI(state->get_orbitals()[0], i, a, grad_ia);
                tmp_mv->Multiply(1.0, *saved_grad_density[0], *grad_ia, 0.0);
                grad_rho_dot_grad_ia->Update(1.0, *tmp_mv->operator()(0), 0.0);
                grad_rho_dot_grad_ia->Update(1.0, *tmp_mv->operator()(1), 1.0);
                grad_rho_dot_grad_ia->Update(1.0, *tmp_mv->operator()(2), 1.0);

                vsigma_grad_ia->operator()(0)->Multiply(1.0, *grad_ia->operator()(0), *vsigma_coeff->operator()(0), 0.0);
                vsigma_grad_ia->operator()(1)->Multiply(1.0, *grad_ia->operator()(1), *vsigma_coeff->operator()(0), 0.0);
                vsigma_grad_ia->operator()(2)->Multiply(1.0, *grad_ia->operator()(2), *vsigma_coeff->operator()(0), 0.0);
                v2rho2_ia_coeff->Multiply(1.0, *ia_coeff, *v2rho2[0], 0.0);
            }

            if(calculate_pgg){
                RCP<Time> time = TimeMonitor::getNewCounter("TDDFT::Kernel_PGG");
                time->start();
                ia_over_density->Multiply(1.0, *i_over_density, *orbitals[0]->operator()(a), 0.0);
                kernel_ia = this -> calculate_PGG_kernel_ia(ia_over_density, orbitals, 0, index_of_HOMO[0], is_pgg_upper_only);
                Value_Coef::Value_Coef(mesh, kernel_ia, true, false, kernel_ia); // becomes coeff
                Verbose::single(Verbose::Simple) << " TDDFT::Kernel_PGG - Time to construct KLI-PGG kernel for (i,a) = (" << i+1 << "," << a+1 << ") : " << time->stop() << " s\n";
            }

            for(int j=i; j<index_of_HOMO[0]; ++j){
                // fxc^PGG contribution
                if(calculate_pgg){
                    j_over_density->ReciprocalMultiply(1.0, *state->get_density()[0], *orbitals[0]->operator()(j), 0.0);
                }
                // end

                //170822
                /*
                RCP<Epetra_Vector> Kiaj = rcp(new Epetra_Vector(*mesh->get_map()));
                Kiaj->Multiply(1.0, *Kia, *orbitals[0]->operator()(j), 0.0);
                Value_Coef::Value_Coef(mesh, Kiaj, true, false, Kiaj); // becomes coeff
                */
                for(int b = (j==i)? a: index_of_HOMO[0]; b<number_of_orbitals[0]; ++b){
                    if(multiply_orbitals){
                        jb_coeff->Multiply(1.0, *state->get_orbitals()[0]->operator()(j), *orbitals()[0]->operator()(b), 0.0);
                    }

                    if(calculate_grad){
                        //grad_jb->PutScalar(0.0);
                        //if(b!=a){
                        get_gradient_ia_RI_using_matrix(state->get_orbitals()[0], j, b, grad_jb);

                        tmp_mv->Multiply(1.0, *saved_grad_density[0], *grad_jb, 0.0);
                        //}
                        grad_rho_dot_grad_jb->Update(1.0, *tmp_mv->operator()(0), 0.0);
                        grad_rho_dot_grad_jb->Update(1.0, *tmp_mv->operator()(1), 1.0);
                        grad_rho_dot_grad_jb->Update(1.0, *tmp_mv->operator()(2), 1.0);
                    }

                    double total_PGG = 0.0;
                    // fxc^PGG contribution
                    if(calculate_pgg){
                        //jb_over_density->Multiply(1.0, *j_over_density, *orbitals[0]->operator()(b), 0.0);
                        //Value_Coef::Value_Coef(mesh, jb_over_density, true, false, jb_over_density); // becomes coeff
                        jb_over_density->Multiply(1.0, *j_over_density, *state->get_orbitals()[0]->operator()(b), 0.0);
                        kernel_ia -> Dot(*jb_over_density, &total_PGG);
                        total_PGG *= -2.0;
                    }
                    // end

                    // Hartree_contrib is (ia|jb)
                    double Hartree_contrib = iajb[index_iajb];
                    //170822
//                    state->get_orbitals()[0]->operator()(b)->Dot(*Kiaj, &Hartree_contrib);
                    internal_timer -> start("kernel");
                    vector<double> xc_contrib = exchange_correlation->ia_fxc_jb_decoupled(orbitals[0], ia_coeff, jb_coeff, grad_ia, grad_jb, vsigma_grad_ia, v2rho2_ia_coeff, grad_rho_dot_grad_ia, grad_rho_dot_grad_jb,  i, a, b, j, total_PGG);
                    internal_timer -> end("kernel");
#if DEBUG == 1
                    internal_timer -> start("fill time");
#endif
                    this -> fill_TD_matrix_restricted(state -> orbital_energies[0], Hartree_contrib, xc_contrib, matrix_index_c, matrix_index_r, sparse_matrix);
#if DEBUG == 1
                    internal_timer -> end("fill time");

#endif
                    ++matrix_index_r;
                    ++index_iajb;
                }
            }
            ++matrix_index_c;
#if DEBUG == 1
//        internal_timer -> end("ia-loop");
//        Verbose::single() << "TDDFT::get_TD_matrix index for i = " << i << ", a = " << a << " time = " << internal_timer -> get_elapsed_time("ia-loop",-1) << std::endl;
#endif
        }
#if DEBUG == 1
        internal_timer -> end("i-loop");
        Verbose::single(Verbose::Simple) << "TDDFT::get_TD_matrix index for i = " << i << " time = " << internal_timer -> get_elapsed_time("i-loop",-1) << std::endl;
        //internal_timer -> print(Verbose::single());
#endif
    }
    if(hf_portion > this -> cutoff){
        Verbose::set_numformat(Verbose::Normal);
        Verbose::single(Verbose::Detail) << "HF portion: " << hf_portion << std::endl;
        internal_timer -> start("TDDFT::HF exchange");
//        this -> Kernel_HF_X(state, sparse_matrix, hf_portion);
        this -> Kernel_TDHF_X(state, sparse_matrix, true, false, hf_portion); // kernel is not hybrid, and do NOT add delta term
        internal_timer -> end("TDDFT::HF exchange");

        Verbose::set_numformat(Verbose::Time);
        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT::HF exchange");
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";
    }
    if(ks_ci_portion > this -> cutoff){
        bool kernel_is_not_hybrid = false;
//        bool is_tdhf = false;
        if(this -> exchange_correlation -> get_functional_type() == "KLI-PGG"){
//            is_tdhf = true;
            kernel_is_not_hybrid = true;
        }
        Verbose::set_numformat(Verbose::Normal);
        Verbose::single(Verbose::Detail) << "HF-EXX portion: " << ks_ci_portion << std::endl;
        internal_timer -> start("TDDFT::HF-EXX exchange");
//        this -> Kernel_KSCI_X(state, sparse_matrix, is_tdhf, ks_ci_portion);
        this -> Kernel_TDHF_X(state, sparse_matrix, kernel_is_not_hybrid, true, ks_ci_portion); // ADD delta term
        internal_timer -> end("TDDFT::HF-EXX exchange");

        Verbose::set_numformat(Verbose::Time);
        Verbose::single(Verbose::Simple) << "\n#------------------------------------------------------ TDDFT::compute\n";
        internal_timer->print(Verbose::single(Verbose::Simple), "TDDFT::HF-EXX exchange");
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";
    }
    return;
}

RCP<Epetra_Vector> TDDFT::calculate_PGG_kernel_ia(
        RCP<Epetra_Vector> ia_over_density,
        Array< RCP<Epetra_MultiVector> > orbitals,
        int start, int end, bool is_upper_only
){
    auto map = mesh->get_map();
    RCP<Epetra_Vector> kernel_ia = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> kernel_ia_tmp = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> iak_over_density = rcp(new Epetra_Vector(*map));
    Array< RCP<Epetra_Vector> > iakl_over_density;

    for(int k=start; k<end; ++k){
        iak_over_density->Multiply(1.0, *ia_over_density, *orbitals[0]->operator()(k), 0.0);
        for(int l=k; l<end; ++l){
            iakl_over_density.clear();
            iakl_over_density.push_back(rcp(new Epetra_Vector(*map)));
            iakl_over_density[0]->Multiply(1.0, *iak_over_density, *orbitals[0]->operator()(l), 0.0);

            double tmp_val = 0.0;
            kernel_ia_tmp->PutScalar(0.0);
            poisson_solver->compute(iakl_over_density, kernel_ia_tmp, tmp_val);

            kernel_ia_tmp->Multiply(1.0, *orbitals[0]->operator()(k), *kernel_ia_tmp, 0.0);
            kernel_ia_tmp->Multiply(1.0, *orbitals[0]->operator()(l), *kernel_ia_tmp, 0.0);

            double coeff = 1.0;
            if(l != k and !is_upper_only){
                coeff = 2.0;
            }
            kernel_ia->Update(coeff, *kernel_ia_tmp, 1.0);
        }
    }
    return kernel_ia;
}

std::vector< std::vector<double> > TDDFT::gather_z_vector(RCP<Epetra_MultiVector> input, std::vector<double> eigvals){
    int num_vec = input -> NumVectors();
    int total_size = input -> Map().NumGlobalElements();
    double ** recv = new double*[num_vec];
    for(int i = 0; i < num_vec; ++i){
        recv[i] = new double[total_size];
    }
    Parallel_Util::group_allgather_multivector(input, recv);
    vector< vector<double> > output(num_vec);
    for(int i = 0; i < num_vec; ++i){
        output[i] = vector<double>(recv[i], recv[i]+total_size);
    }
    for(int i = 0; i < num_vec; ++i){
        delete[] recv[i];
    }
    delete[] recv;
    return output;
}

void TDDFT::get_gradient_ia_RI_using_matrix(RCP<const Epetra_MultiVector> orbital_coeff, int i, int a, RCP<Epetra_MultiVector>& grad_ia){
    auto map = mesh->get_map();
    //RCP<Epetra_MultiVector> tmp = rcp(new Epetra_MultiVector(*grad_ia));
    if(parameters->sublist("TDDFT").get<int>("Gradient",0)==1){
        grad_ia->operator()(0)->Multiply(1.0, *gradient[0]->operator()(i), *orbital_coeff->operator()(a), 0.0);
        grad_ia->operator()(0)->Multiply(1.0, *gradient[0]->operator()(a), *orbital_coeff->operator()(i), 1.0);

        grad_ia->operator()(1)->Multiply(1.0, *gradient[1]->operator()(i), *orbital_coeff->operator()(a), 0.0);
        grad_ia->operator()(1)->Multiply(1.0, *gradient[1]->operator()(a), *orbital_coeff->operator()(i), 1.0);

        grad_ia->operator()(2)->Multiply(1.0, *gradient[2]->operator()(i), *orbital_coeff->operator()(a), 0.0);
        grad_ia->operator()(2)->Multiply(1.0, *gradient[2]->operator()(a), *orbital_coeff->operator()(i), 1.0);
    }
    else
    {
        RCP<Epetra_Vector> ia = rcp(new Epetra_Vector(*map, false));
        ia->Multiply(1.0, *orbital_coeff->operator()(i), *orbital_coeff->operator()(a), 0.0);
        Value_Coef::Value_Coef(mesh, ia, false, true, ia);
        for(int axis=0; axis<3; axis++){
            gradient_matrix[axis]->Multiply(false, *ia, *grad_ia->operator()(axis));
        }

    }
    num2++;
    return;
}
/*
void TDDFT::get_gradient_ia_RI_using_matrix(RCP<const Epetra_MultiVector> orbital_coeff, int j, int b, Teuchos::Array< RCP<Epetra_MultiVector> > & grads){
    RCP<Epetra_MultiVector> jb = rcp(new Epetra_MultiVector(*mesh->get_map(), number_of_orbitals[0]-b));
    auto map = mesh->get_map();

    for(int i=0; i<3; i++)
        grads.push_back(rcp(new Epetra_MultiVector(*mesh->get_map(), number_of_orbitals[0]-b)));
    for(int i=b; i<number_of_orbitals[0]; i++)
        jb->operator()(i-b)->Multiply(1.0, *orbital_coeff->operator()(j), *orbital_coeff->operator()(i), 0.0);
    for(int i=b; i<number_of_orbitals[0]; i++)
        Value_Coef::Value_Coef(mesh, jb, false, true, jb);
    internal_timer->start("TDDFT::jaechang1");
    for(int axis=0; axis<3; axis++){
        //for(int k=0; k<number_of_orbitals[0]-b; k++)
        //    gradient_matrix[axis]->Multiply(false, *jb->operator()(k), *grads[axis]->operator()(k));
        gradient_matrix[axis]->Multiply(false, *jb, *grads[axis]);
    }


    internal_timer->end("TDDFT::jaechang1");
    internal_timer->start("TDDFT::jaechang3");
    for(int axis=0; axis<3; axis++){
        for(int k=0; k<number_of_orbitals[0]-b; k++)
            gradient_matrix[axis]->Multiply(false, *jb->operator()(k), *grads[axis]->operator()(k));
        //gradient_matrix[axis]->Multiply(false, *jb, *grads[axis]);
    }
    internal_timer->end("TDDFT::jaechang3");
        //gradient_matrix[axis]->Multiply(false, *ia, *grad_ia->operator()(axis));
        //gradient_matrix[axis]->PutScalar(1.0);
    num1+=number_of_orbitals[0]-b;
    return;
}
*/

void TDDFT::get_gradient_ia_RI(RCP<const Epetra_MultiVector> orbital_coeff, int i, int a, RCP<Epetra_MultiVector>& grad_ia){
    auto map = mesh->get_map();

    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();

    const double** scaled_grid = mesh->get_scaled_grid();
    int Col = mesh->get_original_size();

    RCP<Epetra_Vector> ia = rcp(new Epetra_Vector(*map));
    ia->Multiply(1.0, *orbital_coeff->operator()(i), *orbital_coeff->operator()(a), 0.0);
    Value_Coef::Value_Coef(mesh, ia, false, true, ia);


    double* tmpcoef = new double [Col];
    double* totcoef = new double [Col];
    memset(tmpcoef, 0.0, sizeof(double)*Col);
    memset(totcoef, 0.0, sizeof(double)*Col);

    for(int k=0; k<NumMyElements; ++k){
        tmpcoef[MyGlobalElements[k]] = ia->operator[](k);
    }
    Parallel_Util::group_sum(tmpcoef, totcoef, Col);

    int k_x=0, k_y=0, k_z=0;
    for(int k=0; k<NumMyElements; ++k){
        mesh->decompose(MyGlobalElements[k], &k_x, &k_y, &k_z);
        double x = scaled_grid[0][k_x];
        double y = scaled_grid[1][k_y];
        double z = scaled_grid[2][k_z];

        double bx = mesh->compute_basis(k_x,x,0);
        double by = mesh->compute_basis(k_y,y,1);
        double bz = mesh->compute_basis(k_z,z,2);

        double grad_x = 0.0;
        double grad_y = 0.0;
        double grad_z = 0.0;

        for(int a=0; a<mesh->get_points()[0]; ++a){
            int ind = mesh->combine(a, k_y, k_z);
            if(ind >= 0){
                grad_x += totcoef[ind] * mesh->compute_first_der(a,x,0);
            }
        }
        grad_x *= by * bz;

        for(int b=0; b<mesh->get_points()[1]; ++b){
            int ind = mesh->combine(k_x, b, k_z);
            if(ind >= 0){
                grad_y += totcoef[ind] * mesh->compute_first_der(b,y,1);
            }
        }
        grad_y *= bx * bz;

        for(int c=0; c<mesh->get_points()[2]; ++c){
            int ind = mesh->combine(k_x, k_y, c);
            if(ind >= 0){
                grad_z += totcoef[ind] * mesh->compute_first_der(c,z,2);
            }
        }
        grad_z *= bx * by;

        grad_ia->ReplaceMyValue(k, 0, grad_x);
        grad_ia->ReplaceMyValue(k, 1, grad_y);
        grad_ia->ReplaceMyValue(k, 2, grad_z);
    }

    delete[] tmpcoef;
    delete[] totcoef;

    return;
}

void TDDFT::get_gradient_ia(RCP<const Epetra_MultiVector> orbital_coeff, int i, int a, RCP<Epetra_MultiVector>& grad_ia){
    auto map = mesh->get_map();

    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();

    const double** scaled_grid = mesh->get_scaled_grid();
    int Col = mesh->get_original_size();

    double* tmpcoef_i = new double [Col];
    double* tmpcoef_a = new double [Col];
    double* totcoef_i = new double [Col];
    double* totcoef_a = new double [Col];
    memset(tmpcoef_i, 0.0, sizeof(double)*Col);
    memset(tmpcoef_a, 0.0, sizeof(double)*Col);
    memset(totcoef_i, 0.0, sizeof(double)*Col);
    memset(totcoef_a, 0.0, sizeof(double)*Col);

    for(int k=0; k<NumMyElements; ++k){
        tmpcoef_i[MyGlobalElements[k]] = orbital_coeff->operator[](i)[k];
        tmpcoef_a[MyGlobalElements[k]] = orbital_coeff->operator[](a)[k];
    }
    Parallel_Util::group_sum(tmpcoef_i, totcoef_i, Col);
    Parallel_Util::group_sum(tmpcoef_a, totcoef_a, Col);

    int k_x=0, k_y=0, k_z=0;
    for(int k=0; k<NumMyElements; ++k){
        mesh->decompose(MyGlobalElements[k], &k_x, &k_y, &k_z);
        double x = scaled_grid[0][k_x];
        double y = scaled_grid[1][k_y];
        double z = scaled_grid[2][k_z];

        double bx = mesh->compute_basis(k_x,x,0);
        double by = mesh->compute_basis(k_y,y,1);
        double bz = mesh->compute_basis(k_z,z,2);

        double grad_x = 0.0;
        double grad_y = 0.0;
        double grad_z = 0.0;

        double tmp_grad_i = 0.0;
        double tmp_grad_a = 0.0;

        for(int a=0; a<mesh->get_points()[0]; ++a){
            int ind = mesh->combine(a, k_y, k_z);
            if(ind >= 0){
                tmp_grad_i += totcoef_i[ind] * mesh->compute_first_der(a,x,0);
                tmp_grad_a += totcoef_a[ind] * mesh->compute_first_der(a,x,0);
            }
        }
        tmp_grad_i *= totcoef_a[MyGlobalElements[k]] * bx * by * by * bz * bz;
        tmp_grad_a *= totcoef_i[MyGlobalElements[k]] * bx * by * by * bz * bz;

        grad_x = tmp_grad_i + tmp_grad_a;

        tmp_grad_i = 0.0;
        tmp_grad_a = 0.0;

        for(int b=0; b<mesh->get_points()[1]; ++b){
            int ind = mesh->combine(k_x, b, k_z);
            if(ind >= 0){
                tmp_grad_i += totcoef_i[ind] * mesh->compute_first_der(b,y,1);
                tmp_grad_a += totcoef_a[ind] * mesh->compute_first_der(b,y,1);
            }
        }
        tmp_grad_i *= totcoef_a[MyGlobalElements[k]] * bx * bx * by * bz * bz;
        tmp_grad_a *= totcoef_i[MyGlobalElements[k]] * bx * bx * by * bz * bz;

        grad_y = tmp_grad_i + tmp_grad_a;

        tmp_grad_i = 0.0;
        tmp_grad_a = 0.0;

        for(int c=0; c<mesh->get_points()[2]; ++c){
            int ind = mesh->combine(k_x, k_y, c);
            if(ind >= 0){
                tmp_grad_i += totcoef_i[ind] * mesh->compute_first_der(c,z,2);
                tmp_grad_a += totcoef_a[ind] * mesh->compute_first_der(c,z,2);
            }
        }
        tmp_grad_i *= totcoef_a[MyGlobalElements[k]] * bx * bx * by * by * bz;
        tmp_grad_a *= totcoef_i[MyGlobalElements[k]] * bx * bx * by * by * bz;

        grad_z = tmp_grad_i + tmp_grad_a;

        grad_ia->ReplaceMyValue(k, 0, grad_x);
        grad_ia->ReplaceMyValue(k, 1, grad_y);
        grad_ia->ReplaceMyValue(k, 2, grad_z);
    }

    delete[] tmpcoef_i;
    delete[] tmpcoef_a;
    delete[] totcoef_i;
    delete[] totcoef_a;

    return;
}

void TDDFT::construct_gradient_matrix(){
    RCP<Time> time = TimeMonitor::getNewCounter("TDDFT::construct_gradient_matrix");
    time->start();

    auto map = mesh->get_map();
    int NumMyElements = map->NumMyElements();
    int* MyGlobalElements = map->MyGlobalElements();
    const double** scaled_grid = mesh->get_scaled_grid();

    int* points = new int[3];
    double* scaling = new double[3];

    points[0] = parameters->sublist("BasicInformation").get<int>("PointX");
    points[1] = parameters->sublist("BasicInformation").get<int>("PointY");
    points[2] = parameters->sublist("BasicInformation").get<int>("PointZ");

    scaling[0] = parameters->sublist("BasicInformation").get<double>("ScalingX");
    scaling[1] = parameters->sublist("BasicInformation").get<double>("ScalingX");
    scaling[2] = parameters->sublist("BasicInformation").get<double>("ScalingX");

    Basis_Function* basis;

    Verbose::single(Verbose::Simple) << "\n#------------------------------------ TDDFT::construct_gradient_matrix\n";

    if(parameters->sublist("TDDFT").isParameter("GradientMatrix")){
        if(parameters->sublist("TDDFT").get<string>("GradientMatrix") == "Finite_Difference"){
            if(parameters->sublist("TDDFT").isParameter("DerivativesOrder")){
                Verbose::single(Verbose::Normal) << " Gradient from finite difference: " << parameters->sublist("TDDFT").get<int>("DerivativesOrder") << "-point stencil.\n";
                basis = new Finite_Difference(points, scaling, parameters->sublist("TDDFT").get<int>("DerivativesOrder"));
            }
            else{
                Verbose::single(Verbose::Normal) << " Gradient from finite difference: 9-point stencil.\n";
                basis = new Finite_Difference(points, scaling);
            }
        }
        else{
            Verbose::single(Verbose::Normal) << " Gradient from sinc function.\n";
            basis = new Sinc(points, scaling);
        }
    }
    else{
        Verbose::single(Verbose::Normal) << " Gradient from sinc function.\n";
        basis = new Sinc(points, scaling);
    }

    delete[] points;
    delete[] scaling;

    gradient_matrix.clear();
    for(int axis=0; axis<3; axis++){
        gradient_matrix.push_back(rcp(new Epetra_CrsMatrix(Copy, *map, 0)));
    }

    int i_x=0, i_y=0, i_z=0;
    for(int i=0; i<NumMyElements; ++i){
        mesh->decompose(MyGlobalElements[i], &i_x, &i_y, &i_z);

        double x = scaled_grid[0][i_x];
        double y = scaled_grid[1][i_y];
        double z = scaled_grid[2][i_z];

        double bx = mesh->compute_basis(i_x,x,0);
        double by = mesh->compute_basis(i_y,y,1);
        double bz = mesh->compute_basis(i_z,z,2);

        vector<int> index;
        vector<double> value;
        int index_size;

        for(int a=0; a<mesh->get_points()[0]; ++a){
            int ind = mesh->combine(a, i_y, i_z);
            if(ind >= 0){
                double tmp = basis->compute_first_der(a,x,0) * by * bz;
                if(abs(tmp) > 1.0E-20){
                    index.push_back(ind);
                    value.push_back(tmp);
                }
            }
        }
        index_size = index.size();
        gradient_matrix[0]->InsertGlobalValues(MyGlobalElements[i], index_size, &value[0], &index[0]);

        index.clear();
        value.clear();

        for(int b=0; b<mesh->get_points()[1]; ++b){
            int ind = mesh->combine(i_x, b, i_z);
            if(ind >= 0){
                double tmp = basis->compute_first_der(b,y,1) * bx * bz;
                if(abs(tmp) > 1.0E-20){
                    index.push_back(ind);
                    value.push_back(tmp);
                }
            }
        }
        index_size = index.size();
        gradient_matrix[1]->InsertGlobalValues(MyGlobalElements[i], index_size, &value[0], &index[0]);

        index.clear();
        value.clear();

        for(int c=0; c<mesh->get_points()[2]; ++c){
            int ind = mesh->combine(i_x, i_y, c);
            if(ind >= 0){
                double tmp = basis->compute_first_der(c,z,2) * bx * by;
                if(abs(tmp) > 1.0E-20){
                    index.push_back(ind);
                    value.push_back(tmp);
                }
            }
        }
        index_size = index.size();
        gradient_matrix[2]->InsertGlobalValues(MyGlobalElements[i], index_size, &value[0], &index[0]);
    }

    gradient_matrix[0]->FillComplete();
    gradient_matrix[1]->FillComplete();
    gradient_matrix[2]->FillComplete();

    delete basis;

    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << " Total time = " << time->stop() << " s\n";
    Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------\n";

    return;
}

Array< RCP<Epetra_Vector> > TDDFT::get_dft_potential(RCP<State> state){
    auto map = mesh->get_map();
    Array< RCP<Epetra_Vector> > xc_potential;
//    RCP<Epetra_Vector> local_potential = rcp(new Epetra_Vector(*map));
    if(state->local_potential.size()!=0){
        for(int i_spin=0;i_spin<state->local_potential.size();++i_spin){
            xc_potential.push_back(state->get_local_potential()[i_spin]);
        }

        if (state->density.size()==0){
            if (state->orbitals.size()==0){
                Verbose::all() << "TDDFT::get_dft_potential - Something wrong, no guess orbitals.\n";
                exit(EXIT_FAILURE);
            }
            else{
                Density_From_Orbitals::compute_total_density(mesh,state->occupations,state->orbitals,state->density);
            }
        }

        RCP<Epetra_Vector> hartree_potential;
        double hartree_energy;
        poisson_solver->compute(state->density, hartree_potential, hartree_energy);
        Verbose::single(Verbose::Detail)<< "TDDFT::get_dft_potential Hartree potential calculation is done" <<std::endl;

        for(int i_spin=0;i_spin<state->local_potential.size();++i_spin){
            xc_potential[i_spin]->Update(-1.0,*hartree_potential, 1.0);
        }
    }
    else{
        Array< RCP<Epetra_Vector> > x_potential;
        Array< RCP<Epetra_Vector> > c_potential;
        for(int i_spin=0; i_spin<state->occupations.size(); ++i_spin){
            xc_potential.push_back(rcp(new Epetra_Vector(*map)));
            x_potential.push_back(rcp(new Epetra_Vector(*map)));
            c_potential.push_back(rcp(new Epetra_Vector(*map)));
        }
        auto tddft_xc_param = Teuchos::sublist(Teuchos::sublist(this -> parameters, "TDDFT", true), "OrbitalInfo", true);

        RCP<Exchange_Correlation> xc_class_for_orbitals = Create_Compute::Create_Exchange_Correlation(mesh, tddft_xc_param, poisson_solver);

        xc_class_for_orbitals->compute_vxc(state, x_potential, c_potential);

        for(int i_spin=0; i_spin<state->occupations.size(); ++i_spin){
            xc_potential[i_spin]->Update(1.0, *x_potential[i_spin], 1.0, *c_potential[i_spin], 0.0);
        }
    }

    return xc_potential;
}

//xc_wo_EXX - get_dft_potential(from gs calc)
Teuchos::Array< Teuchos::RCP<Epetra_Vector> > TDDFT::calculate_v_diff(Teuchos::RCP<State> state, bool without_EXX, bool kernel_is_not_hybrid){
    if(kernel_is_not_hybrid){
        // TDHF(EXX)-like pure kernel!
        // Verbose::single() << "Pure TDHF" << std::endl;
        // - get_dft_potential(from gs calc)  (EXX part is in Kernel_X)
        Array< RCP<Epetra_Vector> > xc_potential_KS = get_dft_potential(state);
        for(int i_spin=0; i_spin<state->occupations.size(); ++i_spin){
            xc_potential_KS[i_spin]->Scale(-1.0);
        }
        return xc_potential_KS;
    }
    else{
        //xc_wo_EXX - get_dft_potential(from gs calc)
        //Verbose::single() << "Hybrid TDHF" << std::endl;
        Teuchos::RCP<Exchange_Correlation> xc_GKS;
        if(without_EXX){
            auto xcwoEXX_param = Teuchos::sublist(Teuchos::sublist(this -> parameters, "TDDFT", true), "OrbitalInfo", true);
            //auto xcwoEXX_param = Teuchos::sublist(this -> parameters, "TDDFT", true);
            if( xcwoEXX_param -> sublist("ExchangeCorrelation").isParameter("XCFunctional") ){
                xcwoEXX_param -> sublist("ExchangeCorrelation").set<int>("AutoEXXPortion", 0);
            } else if(xcwoEXX_param -> sublist("ExchangeCorrelation").isParameter("XFunctional")){
                Array<string> x_functional = xcwoEXX_param -> sublist("ExchangeCorrelation").get< Array<string> >("XFunctional");
                Array<string> x_functional_new;
                for(int i = 0; i < x_functional.size(); ++i){
                    vector<string> tok = String_Util::Split_ws(x_functional[i]);
                    if(tok[0][0] != '-'){
                        x_functional_new.push_back(x_functional[i]);
                    }
                }
                xcwoEXX_param -> sublist("ExchangeCorrelation").set("XFunctional", x_functional_new);
            }
            xc_GKS = Create_Compute::Create_Exchange_Correlation(mesh, xcwoEXX_param, poisson_solver);
        }
        else{
            xc_GKS = exchange_correlation;
        }

        Array< RCP<Epetra_Vector> > xc_potential_KS = get_dft_potential(state);
        Array< RCP<Epetra_Vector> > x_potential_GKS;
        Array< RCP<Epetra_Vector> > c_potential_GKS;
        xc_GKS->compute_vxc(state, x_potential_GKS, c_potential_GKS);
        for(int i_spin=0; i_spin<state->occupations.size(); ++i_spin){
            //        xc_potential_GKS[i_spin]->Update(1.0, *xc_potential_KS[i_spin], -1.0);
            x_potential_GKS[i_spin]->Update(1.0, *c_potential_GKS[i_spin], 1.0);
            xc_potential_KS[i_spin]->Update(1.0, *x_potential_GKS[i_spin], -1.0);
        }
        //    return xc_potential_GKS;
        return xc_potential_KS;
    }
}

double TDDFT::potential_difference_norm(Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, int i, int j, RCP<Epetra_Vector> potential_diff){
    RCP<Epetra_Vector> tmp_vec = Teuchos::rcp(new Epetra_Vector(potential_diff->Map()));
    tmp_vec->Multiply(1.0, *(orbital_coeff->operator()(i)), *potential_diff, 0.0);
    double retval = 0.0;
    tmp_vec->Dot(*(orbital_coeff->operator()(j)), &retval);
    return retval;
}

std::vector<double> TDDFT::get_iajb(Teuchos::RCP<const Epetra_MultiVector> orbital_value, bool use_exchange_poisson){
    Verbose::single(Verbose::Detail) << "TDDFT::get_iajb" << std::endl;
#if DEBUG == 1
    internal_timer -> start("get_iajb");
#endif
    vector<double> iajb;
    if(parameters -> sublist("TDDFT").get("MemorySaveMode", 0) == 0){
        iajb = this -> calculate_iajb_highmem(orbital_value, use_exchange_poisson);
    } else {
        iajb = this -> calculate_iajb_lowmem(orbital_value, use_exchange_poisson);
    }
#if DEBUG == 1
    internal_timer -> end("get_iajb");
    Verbose::set_numformat(Verbose::Time);
    Verbose::single(Verbose::Simple) << "TDDFT::get_iajb, time = " << internal_timer -> get_elapsed_time("get_iajb",-1) << std::endl;
#endif
    return iajb;
}

std::vector<double> TDDFT::calculate_iajb_highmem(Teuchos::RCP<const Epetra_MultiVector> orbital_value, bool use_exchange_poisson){
    std::vector<double> tmp_iajb;
    RCP<Epetra_MultiVector> orbital = rcp(new Epetra_MultiVector(*orbital_value));
    int size = orbital_value->operator()(0)->GlobalLength();
    double** combine_orbital = new double*[orbital->NumVectors()];
    for(int i=0; i<orbital->NumVectors(); i++)
        combine_orbital[i] = new double[size]();
    double* Kia = new double[size];
    Parallel_Util::group_allgather_multivector(orbital, combine_orbital);
    //RCP<Epetra_Vector> Kia2 = rcp(new Epetra_Vector(*mesh->get_map()));
    int index=0;
    int index2=0;
    for(int i=iroot; i<index_of_HOMO[0]; ++i){
        Verbose::single(Verbose::Normal) << "TDDFT::get_iajb, i = " << i << std::endl;
        for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
            bool check = false;
            if(index2%Parallel_Manager::info().get_mpi_size()==Parallel_Manager::info().get_mpi_rank()){
                //for(int k=0; k<size; k++) Kia[k] = 0.0;
                memset(Kia, 0, size*sizeof(double));
                if(use_exchange_poisson){
                    Two_e_integral::compute_Kji(combine_orbital[i], combine_orbital[a], exchange_poisson_solver, Kia, size);
                }
                else{
                    Two_e_integral::compute_Kji(combine_orbital[i], combine_orbital[a], poisson_solver, Kia, size);
                }
                check = true;
            }
            ++index2;
            for(int j=i; j<index_of_HOMO[0]; ++j){
                int b = (j==i)? a: index_of_HOMO[0];
                for(; b<number_of_orbitals[0]; ++b){
                    //double Hartree_contrib2 = Two_e_integral::compute_two_center_integral(mesh, Kia2, orbital->operator()(b), orbital->operator()(j));
                    if(check){
                        double Hartree_contrib = Two_e_integral::compute_two_center_integral(mesh, Kia, combine_orbital[b], combine_orbital[j], size);
                        tmp_iajb.push_back(Hartree_contrib);
                        //index++;
                    }
                    else{
                        //index++;
                        tmp_iajb.push_back(0.0);
                    }
                    ++index;
                }
            }
        }
    }
    for(int i=0; i<orbital->NumVectors(); ++i){
        delete [] combine_orbital[i];
    }
    delete [] combine_orbital;
    delete [] Kia;
    std::vector<double> iajb(index);
    Parallel_Util::group_sum(&tmp_iajb[0],&iajb[0], index);
    return iajb;
}

std::vector<double> TDDFT::calculate_iajb_lowmem(Teuchos::RCP<const Epetra_MultiVector> orbital_value, bool use_exchange_poisson){
    std::vector<double> iajb;
    RCP<Epetra_MultiVector> orbital_value2 = Teuchos::rcp_const_cast<Epetra_MultiVector>(orbital_value);
    RCP<Epetra_Vector> Kia = rcp(new Epetra_Vector(*mesh->get_map()));
    int index=0;
    for(int i=iroot; i<index_of_HOMO[0]; ++i){
        Verbose::single(Verbose::Normal) << "TDDFT::get_iajb, i = " << i << std::endl;
        for(int a=index_of_HOMO[0]; a<number_of_orbitals[0]; ++a){
            Kia -> PutScalar(0.0);
            if(use_exchange_poisson){
                Two_e_integral::compute_Kji(mesh, orbital_value2 -> operator()(i), orbital_value2 -> operator()(a), exchange_poisson_solver, Kia);
            }
            else{
                Two_e_integral::compute_Kji(mesh, orbital_value2 -> operator()(i), orbital_value2 -> operator()(a), poisson_solver, Kia);
            }
            for(int j=i; j<index_of_HOMO[0]; ++j){
                int b = (j==i)? a: index_of_HOMO[0];
                for(; b<number_of_orbitals[0]; ++b){
                    double Hartree_contrib = Two_e_integral::compute_two_center_integral(mesh, Kia, orbital_value2->operator()(b), orbital_value2->operator()(j));
                    iajb.push_back(Hartree_contrib);
                    //index++;
                }
            }
        }
    }
    return iajb;
}
