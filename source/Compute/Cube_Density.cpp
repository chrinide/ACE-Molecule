#include "Cube.hpp"
#include "Cube_Density.hpp"
#include "../Util/Density_Orbital.hpp"
#include "../Util/Read_Output.hpp"
#include "../Util/Value_Coef.hpp"

using Teuchos::Array;
using Teuchos::ParameterList;
using Teuchos::rcp;
using Teuchos::RCP;

Cube_Density::Cube_Density(Array< RCP<Occupation> > occupations, RCP<const Atoms> atoms, Array<std::string> cube_filenames, bool is_bohr){
    this->occupations = occupations;
    this->atoms = atoms;
    this->cube_filenames = cube_filenames;
    this -> is_bohr = is_bohr;
}

int Cube_Density::compute(RCP<const Basis> mesh,Array< RCP<State> >& states){
    initialize_states(mesh,states);
    auto state = states[states.size()-1]; //< pick last state

    Verbose::single(Verbose::Normal) << "\n#------------------------------------------------------- Cube_Density::compute"<<std::endl;

    for(int i_spin=0; i_spin<occupations.size(); i_spin++){
        state->density.append(rcp(new Epetra_Vector(*mesh->get_map())));
        Verbose::single(Verbose::Normal) << " Read " << cube_filenames[i_spin] << std::endl;
        mesh->read_cube(cube_filenames[i_spin], state->density[i_spin], is_bohr);
    }

    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------\n";

    state->orbitals = Density_Orbital::compute_density_orbitals(mesh,state->density,occupations); //< generate orbitals
    return 0;
}
