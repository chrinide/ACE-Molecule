#include "Core_Hamiltonian.hpp"
#include <iostream>
#include <string>
#include <ctime>

#include "../Core/Diagonalize/Create_Diagonalize.hpp"
#include "../Basis/Basis_Function/Finite_Difference.hpp"
#include "../Util/Density_From_Orbitals.hpp"
#include "../Util/Verbose.hpp"
#include "../Util/Parallel_Manager.hpp"

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;

// static member
Teuchos::RCP<Core_Hamiltonian_Matrix> Core_Hamiltonian::core_H_matrix = Teuchos::null;

Core_Hamiltonian::Core_Hamiltonian(RCP<const Basis> mesh, Array<RCP<Occupation> > occupations,RCP<const Atoms> atoms,bool store_crs_matrix, RCP<Teuchos::ParameterList> &parameters){
    this->mesh = mesh ;
    this->atoms = atoms;
    this->parameters= parameters;
    this->occupations = occupations;
    Verbose::single(Verbose::Normal)<< "Core_Hamiltonian:: occupation " << occupations[0]->get_type() <<std::endl;
    if(this -> core_H_matrix == Teuchos::null){
        this -> core_H_matrix = rcp( new Core_Hamiltonian_Matrix(mesh, atoms, parameters, true, store_crs_matrix) );
    } else if(*this -> core_H_matrix -> get_mesh() != *this -> mesh){
        this -> core_H_matrix = rcp( new Core_Hamiltonian_Matrix(mesh, atoms, parameters, true, store_crs_matrix) );
    }
}

int Core_Hamiltonian::compute(RCP<const Basis> mesh, Array<RCP<State> >& states){
    initialize_states(mesh, states);
    RCP<State> state = states[states.size()-1];

    diagonalize = Create_Diagonalize::Create_Diagonalize(Teuchos::sublist(Teuchos::sublist(parameters,"Guess"),"Diagonalize"));
    /*
    if(parameters->sublist("Guess").sublist("Diagonalize").get<std::string>("Solver") == "Pure"){
        diagonalize = Create_Diagonalize::Create_Diagonalize( Teuchos::rcp( new Teuchos::ParameterList(parameters->sublist("Guess").sublist("Diagonalize")) ));
    }
    else{
        Verbose::all() << "Unsupported diagonalize solver" << std::endl;
        exit(EXIT_FAILURE);
    }*/
    // clear existing orbital information
    state->orbitals.clear();
    state->orbital_energies.clear();

    // solve eigenproblem and update orbital informaiton of stae
    for(int i=0; i<occupations.size(); i++){
        RCP<Epetra_CrsMatrix> core_hamiltonian = rcp(new Epetra_FECrsMatrix (Copy,*mesh->get_map(),0 ));
        this -> core_H_matrix -> get(i, core_hamiltonian);
        Verbose::single(Verbose::Normal)<< i << "th core hamiltonian diagonalize" <<std::endl;
        diagonalize->diagonalize(core_hamiltonian, parameters->sublist("Guess").sublist("Diagonalize").get<int>("NumberOfEigenvalues", occupations[i]->get_total_occupation()));
        state->orbitals.push_back(diagonalize->get_eigenvectors());
        state->orbital_energies.push_back(diagonalize->get_eigenvalues());
    }
    Density_From_Orbitals::compute_total_density(mesh, occupations, state->orbitals, state->density);
    Verbose::single(Verbose::Detail)<< "Core_Hamiltonian:: End "<<std::endl;
    return 0;
}

int Core_Hamiltonian::get_core_hamiltonian(int i_spin,RCP<Epetra_CrsMatrix>& return_val){
    /*
    RCP<Epetra_CrsMatrix> kinetic_matrix2 = rcp(new Epetra_FECrsMatrix(Copy,*mesh->get_map(),0));
    clock_t t1, t2, t3, t4, t5, t6;
    t1 = clock();
    kinetic_matrix->add(kinetic_matrix2, 1.0);
    t2 = clock();
    kinetic_matrix2 -> FillComplete();
    t3 = clock();
    this -> nuclear_potential -> get_nuclear_potential_matrix() -> add(i_spin, return_val, 1.0);
    t4 = clock();
    int a = EpetraExt::MatrixMatrix::Add(*kinetic_matrix2,false,1.0,*return_val,1.0);
    if( a != 0 ){
        Verbose::all() << "Core_Hamiltonian:: kinetic_matrix + nuclear_potential_matrix = " << a << std::endl;
        exit(EXIT_FAILURE);
    }
    t5 = clock();
    return_val->FillComplete();
    t6 = clock();
    Verbose::single(Verbose::Detail) << "Core_Hamiltonian kinetic matrix add: \t" << ( (double)(t2-t1) )/CLOCKS_PER_SEC << "s" << std::endl;
    Verbose::single(Verbose::Detail) << "Core_Hamiltonian kinetic matrix Fill: \t" << ( (double)(t3-t2) )/CLOCKS_PER_SEC << "s" << std::endl;
    Verbose::single(Verbose::Detail) << "Core_Hamiltonian NPM add: \t" << ( (double)(t4-t3) )/CLOCKS_PER_SEC << "s" << std::endl;
    Verbose::single(Verbose::Detail) << "Core_Hamiltonian KM+NPM : \t" << ( (double)(t5-t4) )/CLOCKS_PER_SEC << "s" << std::endl;
    Verbose::single(Verbose::Detail) << "Core_Hamiltonian total Fill: \t" << ( (double)(t6-t5) )/CLOCKS_PER_SEC << "s" << std::endl;
    */
    this -> core_H_matrix -> get(i_spin, return_val);

    return 0;
}

/*
int Core_Hamiltonian::get_pp_matrix(int i_spin,RCP<Epetra_CrsMatrix>& return_val){

    return_val = rcp(new Epetra_CrsMatrix (Copy,*mesh->get_map(),0 ));
    this -> nuclear_potential -> get_nuclear_potential_matrix() -> add(i_spin, return_val, 1.0);
    return_val->FillComplete();

    return 0;
}
*/

int Core_Hamiltonian::get_kinetic_energy(RCP<State> state, std::vector<double>& kinetic_energies){
    kinetic_energies.clear();
    Array< RCP<Occupation> > occupations =state->occupations;
    Array< RCP<Epetra_MultiVector> > orbitals = state->orbitals;

    const int spin_size = state->occupations.size();
    //clock_t st, end;
    for(int i_spin=0; i_spin < spin_size; i_spin++){
        //double time1, time2;
        kinetic_energies.push_back(0.0);

        RCP<Epetra_MultiVector> kinetic_tmp = rcp( new Epetra_MultiVector( orbitals[i_spin] -> Map(), orbitals[i_spin] -> NumVectors() ) );
        //st = clock();

        //std::cout << *orbitals[i_spin] <<std::endl;
        //exit(-1); //shchoishchoi
        if(parameters->sublist("Scf").isParameter("ConstructKineticMatrix")){
            if(parameters->sublist("Scf").get<int>("ConstructKineticMatrix")!=0){
                RCP<Epetra_CrsMatrix> kinetic_matrix = rcp(new Epetra_CrsMatrix(Copy, *mesh -> get_map(), 0) );
                this -> core_H_matrix -> get_kinetic_matrix(kinetic_matrix);
                kinetic_matrix->Multiply(false, *orbitals[i_spin], *kinetic_tmp);
                //this -> core_H_matrix -> get_kinetic_matrix() -> multiply2(orbitals[i_spin], kinetic_tmp);
            }
            else{
                this -> core_H_matrix -> get_kinetic_matrix() -> multiply(orbitals[i_spin], kinetic_tmp);

            }
        }
        else{
            RCP<Epetra_CrsMatrix> kinetic_matrix = rcp(new Epetra_CrsMatrix(Copy, *mesh -> get_map(), 0) );
            this -> core_H_matrix -> get_kinetic_matrix(kinetic_matrix);
            kinetic_matrix->Multiply(false, *orbitals[i_spin], *kinetic_tmp);
        }

        //end = clock();
        //time1 = ((double)(end-st))/CLOCKS_PER_SEC;
        //st = clock();
        double* Result=new double [orbitals[i_spin]->NumVectors()];
        //const int size_loop = std::min(orbitals[i_spin] -> NumVectors(), occupations[i_spin]->get_size());
        //double* Result=new double [size_loop];
        kinetic_tmp->Dot(*orbitals[i_spin],Result);
        for(int i=0;i<orbitals[i_spin]->NumVectors();i++){
            kinetic_energies[i_spin] += Result[i] * occupations[i_spin]->operator[](i);
        }
        //end = clock();
        //time2 = ((double)(end-st))/CLOCKS_PER_SEC;
        delete[] Result;
        //Verbose::single(Verbose::Detail) << "time1 : " << time1 << std::endl;
        //Verbose::single(Verbose::Detail) << "time2 : " << time2 << std::endl;
    }
    return 0;
}

int Core_Hamiltonian::get_core_electron_info(Array< RCP<Epetra_Vector> >& density, Array< RCP<Epetra_MultiVector> >& density_grad){
    return this -> core_H_matrix -> nuclear_potential->get_pseudo_core_electron_info(density, density_grad);
}

int Core_Hamiltonian::calculate_nuclear_density_interaction(RCP<State> state,double& energy){
    if (state->orbitals.size() !=0){
        this -> core_H_matrix -> nuclear_potential -> calculate_external_energy( state -> occupations, state -> orbitals, this -> core_H_matrix -> get_nuclear_potential_matrix(), energy );
    }
    else{
        Verbose::all() << "Core_Hamiltonian::calculate_nuclear_density_interaction::  state->orbitals.size() ==0" <<std::endl;
        exit(-1);
    }
    return 0;
}

// Calculate ion-ion interaction
int Core_Hamiltonian::calculate_ion_ion(double& nuclear_nuclear_repulsion){
    int ierr = this -> core_H_matrix -> nuclear_potential -> calculate_nuclear_repulsion(nuclear_nuclear_repulsion);
    return ierr;
}

RCP<Core_Hamiltonian_Matrix> Core_Hamiltonian::get_core_hamiltonian_matrix(){
    return this -> core_H_matrix;
}

RCP<Nuclear_Potential> Core_Hamiltonian::get_nuclear_potential(){
    return this -> core_H_matrix -> nuclear_potential;
}

void Core_Hamiltonian::update_hamiltonian(
    Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
    Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals
){
    this -> core_H_matrix -> update(occupations, orbitals);
}

void Core_Hamiltonian::update_orbital_info(
    Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
    Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals
){
    this -> core_H_matrix -> nuclear_potential -> update(occupations, orbitals);
}

void Core_Hamiltonian::free_matrix(){
    if(Core_Hamiltonian::core_H_matrix != Teuchos::null){
        Core_Hamiltonian::core_H_matrix.release();
    }
}
