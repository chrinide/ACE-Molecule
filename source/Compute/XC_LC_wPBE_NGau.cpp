#include "XC_LC_wPBE_NGau.hpp"
#include "xc.h"

#include "../Util/Parallel_Util.hpp"
#include <stdexcept>
#include <cmath>

#include "../Util/Faddeeva.hpp"
#include "../Util/Lagrange_Derivatives.hpp"
#include "../Util/Value_Coef.hpp"
#include "../Util/String_Util.hpp"

//#define DEBUG 1

using std::abs;
using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

#define _XC_RHO_CUTOFF (1.0E-10)
#define _XC_SIGMA_CUTOFF (1.0E-10)

/*
 * Guide to read LIBXC
 * 1. XC(X) is either xc_ or XC_ in double precision (xc_s_ or XC_S_ in single precision)
 * 2. LC_WPBE implemented in hyb_gga_xc_hse.c
 *    XC_GGA_X_WPBEH 1.0, XC_GGA_C_PBE 1.0
 *    cam_omega = 0.4 (RS \mu), cam_alpha = 1.0 (LR EXX portion), cam_beta = -1.0 (SR EXX portion: DISABLED)
 * 3. See hyb_gga_x_ityh.c to see 10.1063/1.1383587 which talks about making LDA/GGA X into ERF-type range-separated functional.
 * 4. See lda_x.c lda_x_attenuation_function for LDA screened coulomb interaction implementations.
 * 5. For GGA, change 3(3/4PI)^(1/3) to K^GGA. If K^GGA depend on density or aa !~ \rho^(1/3).
 *    Since K^PBE = K^LDA * (1 + kappa - kappa/(1+mu s^2/kappa)) where s = |\grad n|/(2 k_F n).
 */

// mGGA exchange-correlation potential CANNOT be computed.
// mGGA exchange-correlation energy can be computed.
// mGGA exchange-correlation energy with NLCC CANNOT be computed.

/*
 * NOTE on LIBXC index:
 * For UNPOLARIZED case, it is simply rho[i], sigma[i] where i is point index, and so on.
 * For POLARIZED case, rho[2*i+is], sigma[3*i+is] where is is spin index, and so on.
 * POLARIZED spin index list:
 * - GGA
 *   rho, vrho(2)     = (u, d)
 *   sigma, vsigma(3) = (uu, ud, dd)
 *
 *   v2rho2(3)        = (u_u, u_d, d_d)
 *   v2rhosigma(6)    = (u_uu, u_ud, u_dd, d_uu, d_ud, d_dd)
 *   v2sigma2(6)      = (uu_uu, uu_ud, uu_dd, ud_ud, ud_dd, dd_dd)
 *
 *   v3rho3(4)        = (u_u_u, u_u_d, u_d_d, d_d_d)
 *   v3rho2sigma(9)   = (u_u_uu, u_u_ud, u_u_dd, u_d_uu, u_d_ud, u_d_dd, d_d_uu, d_d_ud, d_d_dd)
 *   v3rhosigma2(12)  = (u_uu_uu, u_uu_ud, u_uu_dd, u_ud_ud, u_ud_dd, u_dd_dd, d_uu_uu, d_uu_ud, d_uu_dd, d_ud_ud, d_ud_dd, d_dd_dd)
 *   v3sigma(10)      = (uu_uu_uu, uu_uu_ud, uu_uu_dd, uu_ud_ud, uu_ud_dd, uu_dd_dd, ud_ud_ud, ud_ud_dd, ud_dd_dd, dd_dd_dd)
 *
 */

XC_LC_wPBE_NGau::XC_LC_wPBE_NGau(RCP<const Basis> mesh, int function_id,
    std::vector<double> alpha, std::vector<double> beta, int NGPU):XC_Custom(mesh, function_id, NGPU), alpha(alpha), beta(beta){
    if(this -> function_id != 10478){
        throw std::invalid_argument("Only LC-wPBE-2Gau is supported!");
    }
#ifdef DEBUG
    test();
#endif
}

std::vector<double> Fx_pbeh_gau(std::vector<double> _alpha, std::vector<double> _beta, double s, double kF, int ds = 0, int dk = 0);
double H(double s, int dorder = 0);
double sqrt_H(double s, int dorder = 0);
double s_dd_sqrt_H(double s);
double sqrt_s2H(double s, int dorder = 0);
double F(double s, double dorder = 0);
double EGss(double s, double dorder = 0);
const double A =  1.01611440000000000;
const double B = -0.37170836000000000;
const double C = -0.07721546100000000;
const double D =  0.57786348000000000;
const double E = -0.05195573100000000;
const double a[] = {0.00979681, 0.04108340, 0.18744000, 0.00120824, 0.03471880};
#ifdef DEBUG
void test();
#endif

std::string XC_LC_wPBE_NGau::get_info_string(bool verbose/* = true*/){
    if(!verbose) return "LC-wPBE(NGau)";
    std::string info("Long-range corrected PBE (NGau) (LC-wPBE(NGau)) by Vydrov and Scuseria and NGau (N=2) modified by Hirao\n\
Reference 1: O. A. Vydrov and G. E. Scuseria, J. Chem. Phys. 125, 234109 (2006), http://dx.doi.org/10.1063/1.2409292\n\
Reference 2: J.-W. Song and K. Hirao, J. Chem. Phys. 143, 144112 (2015), http://dx.doi.org/10.1063/1.4932687\n\
Reference 3: M. Wenzerhof and J. Perdew, J. Chem. Phys. 109, 3313 (1998), http://dx.doi.org/10.1063/1.476928\n\
Hybrid GGA functional\n\
XC_FLAGS_HAVE_EXC XC_FLAGS_HAVE_VXC XC_FLAGS_HYB_CAM\n\
Exact exchange portion required for this functional: 1\n\
Range-separation parameter: (depends on gaussian functions, originally 0.4)\n\
Range-separation exact exchange portion: 1\n");
    return info;
}

int XC_LC_wPBE_NGau::get_functional_kind(){
    return 0;
}

void XC_LC_wPBE_NGau::xc_gga_x(void *dummy, int np, double *rho, double *sigma,
        double *ex, double *vrho, double *vsigma,
    double *v2rho2, double *v2rhosigma, double *v2sigma2,
    double *v3rho3, double *v3rho2sigma, double *v3rhosigma2, double *v3sigma3)
{
    if(v3rho3 != NULL or v3rho2sigma != NULL or v3rhosigma2 != NULL or v3sigma3 != NULL){
        throw std::runtime_error("kxc not implemented!");
    }
    if(np < 1) return;
    bool deriv = (vrho == NULL or vsigma == NULL)? false: true;
    bool dderiv = (v2rho2 == NULL or v2rhosigma == NULL or v2sigma2 == NULL)? false: true;

    //double *lda_ex, *pbe_ex;
    //double *lda_vx, *pbe_vxrho, *pbe_vxsigma;
    //double *lda_fx, *pbe_vx2rho2, *pbe_vx2rhosigma, *pbe_vx2sigma2;

    const int rho_size = this -> spin_size;
    const int sigma_size = this -> spin_size * 2 -1;
    const int rho2_size = (this -> spin_size == 1)? 1: 3;
    const int sigma2_size = (this -> spin_size == 1)? 1: 6;

    xc_func_type pbe_func;
    if(this -> spin_size == 1){
        xc_func_init(&pbe_func, 101, XC_UNPOLARIZED);
    } else {
        xc_func_init(&pbe_func, 101, XC_POLARIZED);
    }

    //std::cout << "pbe_ex = " << pbe_ex << std::endl;
    if(ex != NULL){
        double *pbe_ex = new double[np];
        xc_gga_exc(&pbe_func, np, rho, sigma, pbe_ex);
        memcpy(ex, pbe_ex, np*sizeof(double));
        delete[] pbe_ex;
    }
    if(deriv or dderiv){
        //std::cout << "pbe_vxrho = " << pbe_vxrho << std::endl;
        //std::cout << "pbe_vxsigma = " << pbe_vxsigma << std::endl;
        double *pbe_vxrho = new double[rho_size*np];
        double *pbe_vxsigma = new double[sigma_size*np];
        xc_gga_vxc(&pbe_func, np, rho, sigma, pbe_vxrho, pbe_vxsigma);
        memcpy(vrho, pbe_vxrho, rho_size*np*sizeof(double));
        memcpy(vsigma, pbe_vxsigma, sigma_size*np*sizeof(double));
        delete[] pbe_vxrho; delete[] pbe_vxsigma;
    }
    if(dderiv){
        //std::cout << "pbe_vx2rho2 = " << pbe_vx2rho2 << std::endl;
        //std::cout << "pbe_vx2rhosigma = " << pbe_vx2rhosigma << std::endl;
        //std::cout << "pbe_vx2sigma2 = " << pbe_vx2sigma2 << std::endl;
        double *pbe_vx2rho2 = new double[rho2_size*np];
        double *pbe_vx2rhosigma = new double[rho_size*sigma_size*np];
        double *pbe_vx2sigma2 = new double[sigma2_size*np];
        xc_gga_fxc(&pbe_func, np, rho, sigma, pbe_vx2rho2, pbe_vx2rhosigma, pbe_vx2sigma2);
        memcpy(v2rho2, pbe_vx2rho2, rho2_size*np*sizeof(double));
        memcpy(v2rhosigma, pbe_vx2rhosigma, rho_size*sigma_size*np*sizeof(double));
        memcpy(v2sigma2, pbe_vx2sigma2, sigma2_size*np*sizeof(double));
        delete[] pbe_vx2rho2; delete[] pbe_vx2rhosigma; delete[] pbe_vx2sigma2;
    }
    xc_func_end(&pbe_func);
    /* It seems only uu, dd, u_uu, d_dd, uu_uu, dd_dd terms are nonzero for X. */
    //if(this -> spin_size > 1){
    //    throw std::invalid_argument("Spin unrestricted calculation should be tested!");
    //}
    for(int i = 0; i < np; ++i){
        double rho_tot = (this->spin_size==1)? rho[rho_size*i]: rho[rho_size*i]+rho[rho_size*i+1];
        for(int is = 0; is < this -> spin_size; ++is){
            double srho = this -> spin_size*rho[rho_size*i+is];
            double ssigma = this -> spin_size*this -> spin_size*sigma[sigma_size*i+2*is];
            //double ssigma = sigma[sigma_size*i+2*is];
            double kF = pow(3*M_PI*M_PI*srho, 1./3.);
            double s = (ssigma < _XC_SIGMA_CUTOFF)? 0.0: sqrt(ssigma)/2/kF/srho;
            //double s = (sigma[i] < _XC_SIGMA_CUTOFF)? sqrt(_XC_SIGMA_CUTOFF)/2/kF/rho[i]: sqrt(sigma[i])/2/kF/rho[i];
            double lda_ex = -0.75*kF/M_PI;
            vector<double> Fx = Fx_pbeh_gau(this -> alpha, this -> beta, s, kF, 0, 0);
            double Fx_tot = 0.0;
            for(int j = 0; j < Fx.size(); ++j){
                Fx_tot += Fx[j];
            }
            if(ex != NULL){
                if(rho_tot < _XC_RHO_CUTOFF){
                    ex[i] = 0.0;
                } else if(srho > _XC_RHO_CUTOFF){
                    if(this -> spin_size == 1){
                        ex[i] -= (lda_ex * Fx_tot);
                    } else {
                        ex[i] -= (lda_ex * Fx_tot)*srho/rho_tot/2.;
                    }
                }
            }
            double dFx_s_tot = 0.0;
            double dFx_k_tot = 0.0;
            if(deriv or dderiv){
                //std::cout << "before dFx_s" << std::endl;
                vector<double> dFx_s = Fx_pbeh_gau(this -> alpha, this -> beta, s, kF, 1, 0);
                for(int j = 0; j < dFx_s.size(); ++j){
                    dFx_s_tot += dFx_s[j];
                }
                //std::cout << "before dFx_k" << std::endl;
                vector<double> dFx_k = Fx_pbeh_gau(this -> alpha, this -> beta, s, kF, 0, 1);
                for(int j = 0; j < dFx_k.size(); ++j){
                    dFx_k_tot += dFx_k[j];
                }
            }
            if(deriv){
                if(srho < _XC_RHO_CUTOFF){
                    vrho[i*rho_size+is] = 0.0;
                } else {
                    // lda_ex = lda_Ex/rho.
                    // By using srho, 1/2-ed.
                    //vrho[i*rho_size+is] -= (lda_ex * (dFx_k_tot*kF - 4.*dFx_s_tot*s)/3. + 4./3.*lda_ex * Fx_tot)/this -> spin_size;
                    vrho[i*rho_size+is] -= (lda_ex * (dFx_k_tot*kF - 4.*dFx_s_tot*s)/3. + 4./3.*lda_ex * Fx_tot);
                }
                if(ssigma < _XC_SIGMA_CUTOFF){
                    vsigma[i*sigma_size+2*is] = 0.0;
                } else {
                    // By using ssigma, 1/4-ed.
                    //vsigma[i*sigma_size+2*is] -= lda_ex*srho/ssigma * dFx_s_tot*s/2.0 / this -> spin_size;
                    vsigma[i*sigma_size+2*is] -= lda_ex*srho/ssigma * dFx_s_tot*s/2.0 * this -> spin_size;
                }
            }
            double d2Fx_s2_tot = 0.0; double d2Fx_k2_tot = 0.0; double d2Fx_sk_tot = 0.0;
            if(dderiv){
                //std::cout << "before d2Fx_s2" << std::endl;
                vector<double> d2Fx_s2 = Fx_pbeh_gau(this -> alpha, this -> beta, s, kF, 2, 0);
                for(int j = 0; j < d2Fx_s2.size(); ++j){
                    d2Fx_s2_tot += d2Fx_s2[j];
                }
                //std::cout << "before d2Fx_sk" << std::endl;
                vector<double> d2Fx_sk = Fx_pbeh_gau(this -> alpha, this -> beta, s, kF, 1, 1);
                for(int j = 0; j < d2Fx_sk.size(); ++j){
                    d2Fx_sk_tot += d2Fx_sk[j];
                }
                //std::cout << "before d2Fx_k2" << std::endl;
                vector<double> d2Fx_k2 = Fx_pbeh_gau(this -> alpha, this -> beta, s, kF, 0, 2);
                for(int j = 0; j < d2Fx_k2.size(); ++j){
                    d2Fx_k2_tot += d2Fx_k2[j];
                }
                double dsdrho = -4./3.*s/srho; double dsdsigma = 0.5*s/ssigma; double dkdrho = 1./3.*kF/srho;
                double lda_vx = 4./3.*lda_ex; double lda_fx = 4./9.*lda_ex/srho;
                if(srho < _XC_RHO_CUTOFF){
                    v2rho2[i*rho2_size+2*is] = 0.0;
                } else {
                    // By using srho, *1/4-ed. Polarized requires *1/2 only.
                    v2rho2[i*rho2_size+2*is] -= (lda_fx*Fx_tot + lda_vx * (dFx_k_tot*dkdrho + dFx_s_tot*dsdrho)
                            + lda_ex*srho * (d2Fx_s2_tot*dsdrho*dsdrho + dFx_s_tot*(dsdrho*-7/3./srho)
                                + d2Fx_k2_tot*dkdrho*dkdrho + dFx_k_tot*(dkdrho*-2./3./srho) + 2.*d2Fx_sk_tot*dsdrho*dkdrho))*this ->spin_size;
                                //+ d2Fx_k2_tot*dkdrho*dkdrho + dFx_k_tot*(dkdrho*-2./3./srho) + 2.*d2Fx_sk_tot*dsdrho*dkdrho))/this ->spin_size;
                }
                if(srho < _XC_RHO_CUTOFF and ssigma < _XC_SIGMA_CUTOFF){
                    v2rhosigma[i*rho_size*sigma_size+5*is] = 0.0;
                } else {
                    // By using srho and ssigma, *1/8-ed. Polarized requires *1/2 only.
                    // lda_vx + dF/ds*ds/dsigma + lda_ex*rho*(d2F/ds2 ds/drho ds/dsigma + d2F/dsdk dk/drho ds/dsigma)
                    v2rhosigma[i*rho_size*sigma_size+5*is] -= (lda_vx * dFx_s_tot*dsdsigma
                            + lda_ex*srho * (d2Fx_s2_tot*dsdrho*dsdsigma+d2Fx_sk_tot*dkdrho*dsdsigma))*this -> spin_size*this->spin_size;
                            //+ lda_ex*srho * (d2Fx_s2_tot*dsdrho*dsdsigma+d2Fx_sk_tot*dkdrho*dsdsigma))/this -> spin_size;
                }
                if(ssigma < _XC_SIGMA_CUTOFF){
                    v2sigma2[i*sigma2_size+5*is] = 0.0;
                } else {
                    // By using ssigma, *1/16-ed. Polarized requires *1/2 only.
                    v2sigma2[i*sigma2_size+5*is] -= lda_ex*srho * (d2Fx_s2_tot * dsdsigma*dsdsigma + dFx_s_tot * dsdsigma*-.5/ssigma)*pow(this -> spin_size,3);
                    //v2sigma2[i*sigma2_size+5*is] -= lda_ex*srho * (d2Fx_s2_tot * dsdsigma*dsdsigma + dFx_s_tot * dsdsigma*-.5/ssigma)/this -> spin_size;
                }
                /*
                if(std::abs(v2rho2[i]) > 1.E10){
                    std::cout << "v2rho2 big: " << v2rho2[i] << std::endl;
                }
                if(std::abs(v2rhosigma[i]) > 1.E10){
                    std::cout << "v2rhosigma big: " << v2rhosigma[i] << std::endl;
                    std::cout << d2Fx_s2_tot << ", " << d2Fx_sk_tot << ",, " << dsdrho << ", " << dsdsigma << ", " << dkdrho << ",, " << s << ", " << kF << std::endl;
                    std::cout << srho << ", " << ssigma << "/" << _XC_RHO_CUTOFF << ", " << _XC_SIGMA_CUTOFF << std::endl;
                }
                if(std::abs(v2sigma2[i]) > 1.E10){
                    //std::cout << "v2sigma2 big: " << v2sigma2[i] << std::endl;
                }
                // */
            }
        }
    }
    return;
}

/*
 * The Hirao article says that Fx is function of alpha, beta, rho, s, y.
 * However s = sqrt(contracted_grad)/2/k_F/rho, y = k_Fu where u = r1 - r2.
 * Actually Hirao article Fx does not depend on y.
 * Integrating Hirao J seems that F omitted -8/9 factor, erfc argument denominator is alpha instead of A, and alpha should be alpha/kF^2 and beta should be beta/kF
 */
std::vector<double> Fx_pbeh_gau(std::vector<double> _alpha, std::vector<double> _beta, double s, double kF, int ds/* = 0*/, int dk/* = 0*/){
    //std::cout << "Fx start" << std::endl;
    const double c1 = -8./9.*A*sqrt(M_PI)/2;
    const double c2 = -8./9.*-0.75*sqrt(A)*M_PI;
    const double c3 = -8./9.*sqrt(M_PI)/16;
    //const double s_cutoff = sqrt(_XC_SIGMA_CUTOFF)/2/pow(3*M_PI*M_PI*_XC_RHO_CUTOFF, 1./3.);

    //s = std::max(sqrt(_XC_SIGMA_CUTOFF)/2/pow(3*M_PI*M_PI*_XC_RHO_CUTOFF, 1./3.)/_XC_RHO_CUTOFF, s);
    //if(s < s_cutoff) s = 0.0;
    double ss = s*s;
    double ph = H(s)*ss; double dph = D+ph;
    double epg = E+EGss(s); double pf = 1+F(s)*ss;

    //std::cout << "before alpha, beta init" << std::endl;
    vector<double> alpha, beta;
    for(int i = 0; i < _alpha.size(); ++i){
        alpha.push_back(_alpha.at(i)/kF/kF);
        beta.push_back(_beta.at(i)/kF);
        //std::cout << alpha[i] << ", " << beta[i] << std::endl;
    }
    vector<double> retval;
    if(ds == 0 and dk == 0){
        for(int i = 0; i < alpha.size(); ++i){
            double adph = alpha[i] + dph;
            double term1 = 1./sqrt(adph);
            //double term2 = exp(2.25/A*(alpha[i]+ph))*Faddeeva::erfc(1.5*sqrt((alpha[i]+ph)/A));
            double term2 = Faddeeva::erfcx(1.5*sqrt((alpha[i]+ph)/A));
            double term3 = (15*epg+2*adph*(3*C*pf+2*B*adph))/pow(adph, 3.5);
            //Verbose::single() << "coeff = " << alpha[i] << ", " << beta[i] << std::endl;
            //Verbose::single() << adph << ", " << term1 << ", " << term2 << ", " << term3 << std::endl;
            //Verbose::single() << 2.25*(alpha[i]+ph)/A << std::endl;
            retval.push_back(beta[i]*(c1*term1+c2*term2+c3*term3));
        }
        return retval;
    }
    double _dph = H(s, 1)*ss+2*H(s)*s;
    double _dpf = F(s, 1)*ss+2*F(s)*s;
    // dF/d(beta/kF) d(beta/kF)/dkF = -F/kF
    if(ds == 1 and dk == 0){
        for(int i = 0; i < alpha.size(); ++i){
            double adph = alpha[i] + dph;
            double term1 = -0.5*pow(adph, -1.5)*_dph;
            //double term2 = _dph*( 2.25/A*exp(2.25*(alpha[i]+ph)/A)*Faddeeva::erfc(1.5*sqrt((alpha[i]+ph)/A)) - 1.5/sqrt(A*M_PI*(alpha[i]+ph)) );
            double term2 = _dph*( 2.25/A*Faddeeva::erfcx(1.5*sqrt((alpha[i]+ph)/A)) - 1.5/sqrt(A*M_PI*(alpha[i]+ph)) );
            double term3_2 = 3*C*pf+2*B*adph;
            double term3_0 = 15*epg+2*adph*term3_2;
            double term3 = ( 15*EGss(s,1) + 2*_dph*term3_2 + 2*adph*(3*C*_dpf+2*B*_dph) - 3.5*term3_0*_dph/adph )/pow(adph, 3.5);
            retval.push_back(beta[i]*(c1*term1+c2*term2+c3*term3));
        }
        return retval;
    }
    if(ds == 0 and dk == 1){
        vector<double> Fx = Fx_pbeh_gau(_alpha, _beta, s, kF, 0, 0);
        for(int i = 0; i < alpha.size(); ++i){
            double adph = alpha[i] + dph;
            double term1 = -0.5*pow(adph, -1.5);
            //double term2 = 2.25/A*exp(2.25*(alpha[i]+ph)/A)*Faddeeva::erfc(1.5*sqrt((alpha[i]+ph)/A)) - 1.5/sqrt(A*M_PI*(alpha[i]+ph));
            double term2 = 2.25/A*Faddeeva::erfcx(1.5*sqrt((alpha[i]+ph)/A)) - 1.5/sqrt(A*M_PI*(alpha[i]+ph));
            //double term3 = -(6*B*adph*adph+15*C*(1+F(s)*ss)*adph+52.5*EGss(s))/pow(adph, 4.5);
            double term3_2 = 3*C*pf+2*B*adph;
            double term3_0 = 15*epg+2*adph*term3_2;
            double term3 = (2*term3_2 + 4*B*adph - 3.5*term3_0/adph)/pow(adph, 3.5);
            retval.push_back(-Fx[i]/kF - 2.*alpha[i]/kF*beta[i]*(c1*term1+c2*term2+c3*term3));
        }
        return retval;
    }
    if(ds == 0 and dk == 2){
        vector<double> Fx = Fx_pbeh_gau(_alpha, _beta, s, kF, 0, 0);
        vector<double> dFx_k = Fx_pbeh_gau(_alpha, _beta, s, kF, 0, 1);
        //double ddbeta = 0 * 1./kF/kF;
        for(int i = 0; i < alpha.size(); ++i){
            double adph = alpha[i] + dph;
            double term1 = 0.75 * pow(adph, -2.5);
            //double term2 = 81./16./A/A*exp(2.25*(alpha[i]+ph)/A)*Faddeeva::erfc(1.5*sqrt((alpha[i]+ph)/A)) - 3./4. * (9./2./A- 1./(alpha[i]+ph))/sqrt(M_PI*(alpha[i]+ph)*A);
            double term2 = 81./16./A/A*Faddeeva::erfcx(1.5*sqrt((alpha[i]+ph)/A)) - 3./4. * (9./2./A- 1./(alpha[i]+ph))/sqrt(M_PI*(alpha[i]+ph)*A);
            //double term3 = 15./4. * (4*B*adph*adph + 14*C*(1+F(s)*ss)*adph + 63*EGss(s))/pow(adph, 5.5);
            double term3 = (63./4. * (15*epg+4*B*adph*adph + 6*C*pf*adph) - 7.*(8*B*adph+6*C*pf)*adph + 8*B*adph*adph)/pow(adph, 5.5);
            double dFdalpha = -.5/alpha[i]*kF*(dFx_k[i] + Fx[i]/kF);
            double d2beta_term = Fx[i] * 2./kF/kF;// dF/d(beta/kF) d2(beta/kF)/dkF2
            double d2alpha_term = dFdalpha*6*alpha[i]/kF/kF;// dF/d(alpha/kF**2) d2(alpha/kF**2)/dkF2
            double dalpha_beta_term = 2 * dFdalpha/kF*2*alpha[i]/kF;// 2 d2F/d(beta/kF)d(alpha/kF**2) d(beta/kF)/dkF d(alpha/kF/kF)/dkF
            double dalpha2_term = beta[i] * 4*alpha[i]*alpha[i]/kF/kF*(c1*term1+c2*term2+c3*term3);// d2F/d(alpha/kF**2)**2 (d(alpha/kF/kF)/dkF)
            retval.push_back(d2alpha_term + dalpha2_term + dalpha_beta_term + d2beta_term);
        }
        return retval;
    }
    double _ddph = H(s, 2)*ss+4*H(s, 1)*s + 2*H(s, 0);
    if(ds == 2 and dk == 0){
        for(int i = 0; i < alpha.size(); ++i){
            double adph = alpha[i] + dph;
            double term1 = 0.75 * _dph*_dph*pow(adph, -2.5) - 0.5*pow(adph, -1.5)*_ddph;
            //double term2_coeff = 5.0625/A/A*_dph*_dph+2.25/A*_ddph;
            //double term2 = term2_coeff*exp(2.25*(alpha[i]+ph)/A)*Faddeeva::erfc(1.5*sqrt((alpha[i]+ph)/A)) - 2./3.*(term2_coeff - 1.125/A/(alpha[i]+ph)*_dph*_dph)/sqrt(M_PI*(alpha[i]+ph)*A);
            //double term2 = term2_coeff*Faddeeva::erfcx(1.5*sqrt((alpha[i]+ph)/A)) - 2./3.*(term2_coeff - 1.125/A/(alpha[i]+ph)*_dph*_dph)/sqrt(M_PI*(alpha[i]+ph)*A);
            //double term2 = term2_coeff*(Faddeeva::erfcx(1.5*sqrt((alpha[i]+ph)/A)) - 1.5/sqrt(A*M_PI*(alpha[i]+ph))) -1.5/sqrt(A*M_PI*(alpha[i]+ph))*(_ddph-0.5/(alpha[i]+dph)*_dph*_dph);
            double term2 = (5.0625/A/A*_dph*_dph+2.25/A*_ddph)*Faddeeva::erfcx(1.5*sqrt((alpha[i]+ph)/A)) + .75/sqrt(M_PI*A*(alpha[i]+ph))*(_dph*_dph*(1./(alpha[i]+ph)-4.5/A)-2*_ddph);
            double term3_7_1 = 15*EGss(s,2)+2*_ddph*(3*C*pf+2*B*adph);// d2 E(1+Gs2)/ds2 + 2*d2(alpha+D+Hss)/ds2*(-B-+-C-)
            double term3_7_2 = 2*adph*(2*B*_ddph + 3*C*(F(s,2)*ss+4*F(s,1)*s+2*F(s,0)));// 2*(alpha+D+Hss)*d2(-B-+-C-)/ds2
            double term3_7_3 = 4*_dph*(2*B*_dph+3*C*_dpf);// 2*d(alpha+D+Hss)/ds*d(-B-+-C-)/ds
            //double term3_9_1 =_ddph*(2*adph*(2*B*adph+3*C*(1+F(s)*ss))+15*EGss(s));
            double term3_9_1 = _ddph*(2*adph*(2*B*adph+3*C*pf)+15*epg);
            //double term3_9_2 = 2*_dph2*(adph*(2*B*(H(s,1)*ss+2*s*H(s,0))+3*C*_dpf) + 2*B*adph+3*C*(1+F(s)*ss)) + 15*EGss(s,1);
            double term3_9_2 = _dph*(2*adph*(2*B*_dph+3*C*_dpf) + 2*_dph*(2*B*adph+3*C*pf) + 15*EGss(s,1));
            //double term3_11 = _dph*_dph*(2*adph*(2*B*adph+3*C*(1+F(s)*ss))+15*EGss(s));
            double term3_11 = _dph*_dph*(2*adph*(2*B*adph+3*C*pf)+15*epg);
            double term3 = (term3_7_1+term3_7_2+term3_7_3)/pow(adph, 3.5) - 3.5 * (term3_9_1+2*term3_9_2)/pow(adph, 4.5) + 63.0/4*term3_11/pow(adph, 5.5);
            retval.push_back(beta[i]*(c1*term1+c2*term2+c3*term3));
    //Verbose::single() << "term = " << term1 << ", " << term2 << ", " << term3 << std::endl;
    //Verbose::single() << "term3 = " << (term3_7_1+term3_7_2+term3_7_3)/pow(adph, 3.5) << "+" << - 7 * (.5*term3_9_1+term3_9_2)/pow(adph, 4.5) <<"+"<< 63.0/4*term3_11/pow(adph, 5.5)<<std::endl;
    //Verbose::single() << "term3' = " << term3_7_1<<"+"<<term3_7_2<<"+"<<term3_7_3 << "," << term3_9_1<<"+2*"<<term3_9_2 <<","<< 63.0/4*term3_11<<std::endl;
        }
        return retval;
    }
    if(ds == 1 and dk == 1){
        vector<double> dsFx = Fx_pbeh_gau(_alpha, _beta, s, kF, 1, 0);
        for(int i = 0; i < alpha.size(); ++i){
            double adph = alpha[i] + dph;
            double term1 = .75*pow(adph, -2.5)*_dph;
            //double term2 = _dph*( 2.25/A*2.25/A*exp(2.25*(alpha[i]+ph)/A)*Faddeeva::erfc(1.5*sqrt((alpha[i]+ph)/A)) - 1.5/sqrt(A*M_PI*(alpha[i]+ph)) * (1.5/A - 1./(alpha[i]+ph)) );
            double term2 = _dph*( 5.0625/A/A*Faddeeva::erfcx(1.5*sqrt((alpha[i]+ph)/A)) + .75/sqrt(A*M_PI*(alpha[i]+ph)) * (-4.5/A + 1./(alpha[i]+ph)) );
            double term3_7 = 8*B*_dph + 6*C*_dpf;
            //double term3_9 = (6*C*_dpf + 8*B*_dph)*adph + 12*_dph*(B*adph+C*pf)+15*EGss(s, 1);
            double term3_9 = (6*C*_dpf + 4*B*_dph)*adph + 12*_dph*(B*adph+C*pf)+15*EGss(s, 1);
            double term3_11 = _dph*(adph*(4*B*adph+6*C*pf)+15*epg);
            double term3 = (term3_7 - 3.5*term3_9/adph + 15.75*term3_11/adph/adph)/pow(adph, 3.5);
            retval.push_back(-dsFx[i]/kF - 2.*alpha[i]/kF * beta[i]*(c1*term1+c2*term2+c3*term3));
        }
        return retval;
    }
    throw std::runtime_error("Third or higher derivatives are not implemented!");
    return retval;
}

/*
 * SR: 10.1063/1.4932687 10.1063/1.1564060
 * LDA SR Ex^SR = - 3/2 (3/4*PI)^(1/3) \sum_s \int \rho_s^(4/3) * [ 1 - 8/3 a_s ( PI^0.5 erf(1/2a_s) + (2a_s-4a_s^3) exp(-1/4a_s^2) - 3a_s +4a_s^3) ] d^3 R
 * exc: to be multiplied and integrated with \rho_s
 * All exc, vxc, fxc, kxc are implemented in libxc lda_x_attenuation_function_erf
 */

double H(double s, int dorder/* = 0*/){
    const double a[] = {0.00979681,
                        0.04108340,
                        0.18744000,
                        0.00120824,
                        0.03471880};

    double ss = s*s; double s4 = ss*ss;
    double f = a[0]*ss+a[1]*ss*ss;// numerator
    double g = 1.+a[2]*s4+a[3]*s4*s+a[4]*s4*ss;// denominator

    if(dorder == 0){
        return f/g;
    }

    double df = 2*a[0]*s+4*a[1]*ss*s;
    double dg = 4*a[2]*ss*s+5*a[3]*s4+6*a[4]*s4*s;
    if(dorder == 1){
        return df/g - f*dg/(g*g);
    }
    double ddf = 2*a[0]+12*a[1]*ss;
    double ddg = 12*a[2]*ss+20*a[3]*ss*s+30*a[4]*s4;
    if(dorder == 2){
        return ddf/g - (2*df*dg+f*ddg)/(g*g) + 2*f*dg*dg/pow(g,3);
    }
    double d3f = 24*a[1]*s;
    double d3g = 24*a[2]*s+60*a[3]*ss+120*a[4]*ss*s;
    if(dorder == 3){
        return d3f/g - (3*ddf*dg+3*df*ddg+f*d3g)/(g*g) + 6*(df*dg*dg+f*dg*ddg)/pow(g,3) - 6*f*pow(dg,3)/pow(g,4);
    }
    throw std::logic_error("dnH/dsn for n>3 not implemented!");
    return 0.;
}

double sqrt_H(double s, int dorder/* = 0*/){
    const double a[] = {0.00979681,
                        0.04108340,
                        0.18744000,
                        0.00120824,
                        0.03471880};

    double ss = s*s; double s4 = ss*ss;
    double f = a[0]*ss+a[1]*ss*ss;// numerator
    double g = 1.+a[2]*s4+a[3]*s4*s+a[4]*s4*ss;// denominator

    if(dorder == 0){
        return sqrt(f/g);
    }

    double df_over_sqrt_f = (std::abs(s) > 1.E-8)? (2*a[0]*s+4*a[1]*ss*s)/sqrt(f): 2*(a[0]+2*a[1]*ss)/sqrt(a[0])*(1-0.5*a[1]/a[0]*ss);
    double dg = 4*a[2]*ss*s+5*a[3]*s4+6*a[4]*s4*s;
    if(dorder == 1){
        return 0.5*(df_over_sqrt_f/sqrt(g) - sqrt(f)*dg/pow(g, 1.5));
    }
    //double ddf_over_sqrt_f = (std::abs(s) > 0.01)? (2*a[0]+12*a[1]*ss)/sqrt(f): 2*(a[0]);
    double ddg = 12*a[2]*ss+20*a[3]*ss*s+30*a[4]*s4;
    if(dorder == 2){
        //return 0.5*(ddf_over_sqrt_f/g - 2*df_over_sqrt_f/sqrt(g)*dg/g - sqrt(f)*ddg/pow(g,1.5) + 2*sqrt(f)*dg*dg/pow(g, 2.5))
        //       - 0.25/g/g*(pow(df_over_sqrt_f,2)-2*df*dg+f*dg*dg/g);
    }
    throw std::logic_error("dnsqrt(H)/dsn for n>1 not implemented!");
    return 0.;
}

double sqrt_s2H(double s, int dorder/* = 0*/){
    double ss = s*s; double s4 = ss*ss;
    double f = a[0]*ss+a[1]*ss*ss;// numerator
    double g = 1.+a[2]*s4+a[3]*s4*s+a[4]*s4*ss;// denominator

    if(dorder == 0){
        return s*sqrt(f/g);
    }

    double df_over_sqrt_f = (2*a[0]+4*a[1]*ss)/sqrt(a[0]+a[1]*ss);
    double dg = 4*a[2]*ss*s+5*a[3]*s4+6*a[4]*s4*s;
    if(dorder == 1){
        return 0.5*(s*df_over_sqrt_f/sqrt(g) - s*sqrt(f)*dg/pow(g, 1.5))+sqrt(f/g);
    }
    double sddf_over_sqrt_f = (2*a[0]+12*a[1]*ss)/sqrt(a[0]+a[1]*ss);
    double ddg = 12*a[2]*ss+20*a[3]*ss*s+30*a[4]*s4;
    if(dorder == 2){
        double d_sqrt_H_ds = 0.5*(df_over_sqrt_f/sqrt(g) - sqrt(f)*dg/pow(g, 1.5));
        //return 2*d_sqrt_H_ds + 0.5*(sddf_over_sqrt_f/sqrt(g) - 0.5*s*dg/pow(g, 1.5)*df_over_sqrt_f - 0.5*s*pow(df_over_sqrt_f, 2)/sqrt(g)/sqrt(a[0]+a[1]*ss) - s*(sqrt(f)*ddg + 0.5*df_over_sqrt_f*dg)/pow(g, 1.5) + 1.5*s*sqrt(f)*dg*dg/pow(g, 2.5));
        //return 2*d_sqrt_H_ds - s*sqrt(f)*ddg/2/pow(g,1.5)+s*sqrt(f)*dg*dg/pow(g,2.5)-s*sqrt(f)*dg*dg/4/pow(g,2.5)-s*df_over_sqrt_f*dg/pow(g,1.5)+s*df_over_sqrt_f*dg/2/pow(g,1.5)+sddf_over_sqrt_f/2/sqrt(g)-pow(df_over_sqrt_f,2)/sqrt(a[0]+a[1]*ss)/4/sqrt(g(s));
        //Verbose::single() << d_sqrt_H_ds << ", " << 0.75*s*sqrt(f)*dg*dg/pow(g,2.5) << ", " << s/2*(sqrt(f)*ddg+df_over_sqrt_f*dg)/pow(g,1.5) << ", " << (sddf_over_sqrt_f/2-pow(df_over_sqrt_f,2)/sqrt(a[0]+a[1]*ss)/4)/sqrt(g) << std::endl;
        //Verbose::single() << sddf_over_sqrt_f << ", " << pow(df_over_sqrt_f,2)/sqrt(a[0]) << std::endl;
        return 2*d_sqrt_H_ds + 0.75*s*sqrt(f)*dg*dg/pow(g,2.5) - s/2*(sqrt(f)*ddg+df_over_sqrt_f*dg)/pow(g,1.5)+(sddf_over_sqrt_f/2-pow(df_over_sqrt_f,2)/sqrt(a[0]+a[1]*ss)/4)/sqrt(g);
    }
    throw std::logic_error("dnsqrt(s^2*H)/dsn for n>2 not implemented!");
    return 0.;
}

double s_dd_sqrt_H(double s){
    double ss = s*s; double s4 = ss*ss;
    double f = a[0]*ss+a[1]*ss*ss;// numerator
    double g = 1.+a[2]*s4+a[3]*s4*s+a[4]*s4*ss;// denominator

    double df_over_sqrt_f = (std::abs(s) > 0.01)? (2*a[0]*s+4*a[1]*ss*s)/sqrt(f): 2*(a[0]+2*a[1]*ss)/sqrt(a[0])*(1-0.5*a[1]/a[0]*ss);
    double df = 2*a[0]*s+4*a[1]*ss*s;
    double dg = 4*a[2]*ss*s+5*a[3]*s4+6*a[4]*s4*s;

    double sddf_over_sqrt_f = (std::abs(s) > 1.E-8)? (2*a[0]*s+12*a[1]*ss*s)/sqrt(f): 2*(a[0]+6*a[1]*ss)/sqrt(a[0])*(1-0.5*a[1]/a[0]*ss);
    double ddg = 12*a[2]*ss+20*a[3]*ss*s+30*a[4]*s4;
    return 0.5*(sddf_over_sqrt_f/g - 2*s*df_over_sqrt_f/sqrt(g)*dg/g - s*sqrt(f)*ddg/pow(g,1.5) + 2*s*sqrt(f)*dg*dg/pow(g, 2.5))
            - 0.25*s/g/g*(pow(df_over_sqrt_f,2)-2*df*dg+f*dg*dg/g);
}

double F(double s, double dorder/* = 0*/){
    // (16*A*A+36*(B-AD))/36C * H(s) + 9k/36C. k = -4/27
    if(dorder == 0){
        return 6.475387061*H(s) + 0.479658304;
    }
    return 6.475387061*H(s, dorder);
}

double EGss(double s, double dorder/* = 0*/){
    const double s_cutoff = sqrt(_XC_SIGMA_CUTOFF)/2/pow(3*M_PI*M_PI*_XC_RHO_CUTOFF, 1./3.)/_XC_RHO_CUTOFF;
    double ss = s*s;
    double t = D+H(s)*ss;
    //double exp_erfc = exp(2.25*H(s,0)*ss/A)*erfc(1.5*s*sqrt(H(s,0)/A));
    double exp_erfc = Faddeeva::erfcx(1.5*s*sqrt(H(s,0)/A));
    if(dorder == 0){
        double non_a = -0.8*sqrt(M_PI)*pow(t, 3.5);
        double m_polym_a = E + 1./15*(6*C*(1+F(s)*ss)*t + 4*B*t*t + 8*A*pow(t,3));
        double exp_a = 0.8*sqrt(A*M_PI)*exp_erfc*pow(t, 3.5);
        return non_a - m_polym_a + exp_a;
    }
    double dt = H(s, 1)*ss+2*H(s, 0)*s;
    if(dorder == 1){
        double non_a = -2.8*sqrt(M_PI)*pow(t, 2.5)*dt;
        double m_polym_a = 1./15*(6*C*(F(s,1)*ss+2*F(s,0)*s)*t + 6*C*(1+F(s,0)*ss)*dt + 8*B*t*dt + 24*A*t*t*dt);
        //double exp_a = 0.8*sqrt(A*M_PI) * (2.25/A*exp_erfc*t*dt - 1.5/sqrt(M_PI*A)*2*(sqrt(H(s,0))+s*sqrt_H(s, 1))*t + 3.5*exp_erfc*dt)*pow(t, 2.5);
        double exp_a = 0.8*sqrt(A*M_PI) * (2.25/A*exp_erfc*t*dt - 1.5/sqrt(M_PI*A)*2*(sqrt_s2H(s, 1))*t + 3.5*exp_erfc*dt)*pow(t, 2.5);
        return non_a - m_polym_a + exp_a;
    } else if(dorder == 2){
        double ddt = H(s, 2)*ss + 4*H(s, 1)*s + 2*H(s, 0);

        double non_a = -2.8*sqrt(M_PI)*(2.5*pow(t, 1.5)*dt*dt + pow(t, 2.5)*ddt);
        double dFss = F(s,1)*ss+2*F(s,0)*s;
        double m_polym_a = 1./15*(6*C*((F(s,2)*ss+4*F(s,1)*s+2*F(s,0))*t + 2*dFss*dt + (1+F(s,0)*ss)*ddt) + 8*B*(dt*dt+t*ddt) + 24*A*(2*t*dt*dt+t*t*ddt));
        //double exp_a_7 = 2.25/A/A*exp_erfc*(ddt+dt*dt) - .75/sqrt(M_PI*A)*(-4*A*s*pow(sqrt_H(s,1),2) + 4*H(s,1)+5*H(s,2) + 4.5/A*s*pow(2*H(s)+s*H(s,1),2));
        //double exp_a_5 = 7*exp_erfc*(9./4.*dt*dt+ddt) - 21./2*sqrt(M_PI)*dt*(2*sqrt(H(s)/A)+2*s*sqrt_H(s,1));
        //double exp_a_7 = exp_erfc*(9./4./A*ddt+81./16./A/A*dt*dt) - 3/sqrt(M_PI*A)*( sqrt_H(s,1)+s_dd_sqrt_H(s) + 1.125/A*s*dt*(sqrt(H(s))+s*sqrt_H(s,1)) );
        double exp_a_7 = exp_erfc*(9./4./A*ddt+81./16./A/A*dt*dt) - 3/sqrt(M_PI*A)*( sqrt_s2H(s,2) + 2.25/A*dt*(sqrt_s2H(s,1)) );
        //double exp_a_7 = exp_erfc*(9./4./A*ddt+81./16./A/A*dt*dt) - 3/sqrt(M_PI*A)*( 2*sqrt_H(s,1)+s_dd_sqrt_H(s) + 2.25/A*dt*(sqrt(H(s))+s*sqrt_H(s,1)) );
        // exp_a_7 exp_erfc portion good.
        // Rational term portion should be 3/sqrt(M_PI*A)* 9.897981787827707b-2 for s = 0.0001
        //double exp_a_5 = 7*exp_erfc*(9./4./A*dt*dt+ddt/2) - 21/sqrt(M_PI*A)*dt*(sqrt(H(s))+s*sqrt_H(s,1));
        double exp_a_5 = 7*exp_erfc*(9./4./A*dt*dt+ddt/2) - 21/sqrt(M_PI*A)*dt*(sqrt_s2H(s,1));
        double exp_a_3 = 35./4.*exp_erfc*dt*dt;
        double exp_a = 0.8*sqrt(A*M_PI)*(exp_a_7*t*t+exp_a_5*t+exp_a_3)*pow(t, 1.5);
        //Verbose::single() << "EGss(s,2) = " << non_a << ", " << -m_polym_a << ", " << exp_a << std::endl;
        //Verbose::single() << "EGss(s,2) erfcx = " << exp_a_7 << ", " << exp_a_5 << ", " << exp_a_3 << std::endl;
        //Verbose::single() << "EGss(s,2) erfcx 7-term = " << exp_erfc*(9./4./A*ddt+81./16./A/A*dt*dt) << ", " << sqrt_H(s,1) << "+" << s_dd_sqrt_H(s) << "+" <<  1.125/A*s*dt*(sqrt(H(s))+s*sqrt_H(s,1)) << std::endl;
        return non_a - m_polym_a + exp_a;
    } else if(dorder == 3){
        double dt = H(s, 1)*ss + 2*H(s, 0)*s;
        double ddt = H(s, 2)*ss + 4*H(s, 1)*s + 2*H(s, 0);
        double d3t = H(s, 3)*ss + 6*H(s, 2)*s + 6*H(s, 1);

        double non_a = -2.8*sqrt(M_PI)*(3.75*pow(t, .5)*dt+2.5*pow(t, 1.5)*ddt + 2.5*pow(t, 1.5)*ddt+pow(t, 2.5)*d3t);
        double dFss_s = F(s,1)*s+2*F(s,0);
        //double m_polym_a = 1./15*(6*C*((F(s,2)*ss+2*dFss_s)*t+dFss_s*s*dt) + dFss_s*s*dt+(1+F(s,0)*ss)*ddt + 8*B*(dt*dt+t*ddt) + 24*A*(2*t*dt*dt+t*t*ddt));// TODO
        //double exp_a = 0.8*sqrt(A*M_PI) * (2.25/A*(exponential*(2.25/A*dDHss+ddDHss)*erfc(1.5*s*sqrt(H/A)) - 3*sqrt(H/A/M_PI)));// TODO
        //return non_a - m_polym_a + exp_a;
        throw std::logic_error("dn(Gs**2)/dsn for n==3 not implemented!");
    }
    throw std::logic_error("dn(Gs**2)/dsn for n>3 not implemented!");
    return 0.;
}

//*
#ifdef DEBUG
void test(){
    //double s = 0.;//0001;
    double s = 1;
    double kF = 1.0;
    vector<double> alpha(1.0, 1), beta(1.0, 1);
    Verbose::single() << "TESTESTESTESTESTESTESTESTESTESTESTESTESTESTSETESTESTEST" << std::endl;
    //Verbose::single() << "s, kF, alpha, beta all 1.0" << std::endl;
    Verbose::single() << "s = 0, kF, alpha, beta all 1.0" << std::endl;

    Verbose::single() << "(0) F, EGss, H = " << F(s) << ", " << EGss(s) << ", " << H(s) << std::endl;
    Verbose::single() << "(1) F, EGss, H = " << F(s, 1) << ", " << EGss(s, 1) << ", " << H(s, 1) << std::endl;
    Verbose::single() << "(2) F, EGss, H = " << F(s, 2) << ", " << EGss(s, 2) << ", " << H(s, 2) << std::endl;
    Verbose::single() << "EGss(0.0001, 2) should be = - 5.256772326207221E-2" << std::endl;
    Verbose::single() << "EGss(1., 2) should be = - 8.534727293572471E-1" << std::endl;
    Verbose::single() << "sqrt_s2H(0.,0) = " << sqrt_s2H(0.,0) << std::endl;
    Verbose::single() << "sqrt_s2H(0.,1) = " << sqrt_s2H(0.,1) << std::endl;
    Verbose::single() << "sqrt_s2H(0.,2) = " << sqrt_s2H(0.,2) << std::endl;
    Verbose::single() << "sqrt_s2H(1.,0) = " << sqrt_s2H(1.,0) << std::endl;
    Verbose::single() << "sqrt_s2H(1.,1) = " << sqrt_s2H(1.,1) << std::endl;
    Verbose::single() << "sqrt_s2H(1.,2) = " << sqrt_s2H(1.,2) << std::endl;
    Verbose::single() << "Fx = " << Fx_pbeh_gau(alpha, beta, s, kF)[0] << std::endl;
    Verbose::single() << "dFx_ds = " << Fx_pbeh_gau(alpha, beta, s, kF, 1, 0)[0] << std::endl;
    Verbose::single() << "dFx_dk = " << Fx_pbeh_gau(alpha, beta, s, kF, 0, 1)[0] << std::endl;
    Verbose::single() << "d2Fx_ds2 = " << Fx_pbeh_gau(alpha, beta, s, kF, 2, 0)[0] << std::endl;
    Verbose::single() << "d2Fx_dsk = " << Fx_pbeh_gau(alpha, beta, s, kF, 1, 1)[0] << std::endl;
    Verbose::single() << "d2Fx_dk2 = " << Fx_pbeh_gau(alpha, beta, s, kF, 0, 2)[0] << std::endl;
    //Verbose::single() << "d2Fx_ds2 should be = 8.869817392092126E-3" << std::endl;
    const double c1 = -8./9.*A*sqrt(M_PI)/2;
    const double c2 = -8./9.*-0.75*sqrt(A)*M_PI;
    const double c3 = -8./9.*sqrt(M_PI)/16;
    double ss = s*s;
    double ph = H(s)*ss; double dph = D+ph;
    double epg = E+EGss(s); double pf = 1+F(s)*ss;

    double adph = alpha[0] + dph;
    /*
    double term1 = 1./sqrt(adph);
    double term2 = Faddeeva::erfcx(1.5*sqrt((alpha[0]+ph)/A));
    double term3 = (15*epg+2*adph*(3*C*pf+2*B*adph))/pow(adph, 3.5);
    Verbose::single() << beta[0]*c1*term1 << ", " << beta[0]*c2*term2 << ", " << beta[0]*c3*term3 << ", " << term3 << std::endl;
    Verbose::single() << "polym term = " << 15*epg << ", " << 2*adph*(3*C*pf+2*B*adph) << ", " << pow(adph, 3.5) << std::endl;
    */

    double _ddph = H(s, 2)*ss+4*H(s, 1)*s + 2*H(s, 0);
    double _dph = H(s, 1)*ss+2*H(s)*s;
    double _dpf = F(s, 1)*ss+2*F(s)*s;
    double term1 = 0.75 * _dph*_dph*pow(adph, -2.5) - 0.5*pow(adph, -1.5)*_ddph;
    double term2_coeff = 5.0625/A/A*_dph*_dph+2.25/A*_ddph;
    double term2 = term2_coeff*(Faddeeva::erfcx(1.5*sqrt((alpha[0]+ph)/A)) - 1.5/sqrt(A*M_PI*(alpha[0]+ph))) -1.5/sqrt(A*M_PI*(alpha[0]+ph))*(_ddph-0.5/(alpha[0]+dph)*_dph*_dph);
    double term3_7_1 = 15*EGss(s,2)+2*_ddph*(3*C*(1+F(s)*ss)+2*B*adph);// d2 E(1+Gs2)/ds2 + 2*d2(alpha+D+Hss)/ds2*(-B-+-C-)
    double term3_7_2 = (4*B*_ddph + 6*C*(F(s,2)*ss+4*F(s,1)*s+2*F(s,0)))*adph;// 2*(alpha+D+Hss)*d2(-B-+-C-)/ds2
    double term3_7_3 = 4*_dph*(2*B*(2*s*H(s,0)+H(s,1)*ss)+3*C*(2*s*F(s,0)+ss*F(s,1)));// 2*d(alpha+D+Hss)/ds*d(-B-+-C-)/ds
    //double term3_9_1 =_ddph*(2*adph*(2*B*adph+3*C*(1+F(s)*ss))+15*EGss(s));
    double term3_9_1 =_ddph*(2*adph*(2*B*adph+3*C*(1+F(s)*ss))+15*epg);
    //double term3_9_2 = 2*_dph2*(adph*(2*B*(H(s,1)*ss+2*s*H(s,0))+3*C*_dpf) + 2*B*adph+3*C*(1+F(s)*ss)) + 15*EGss(s,1);
    double term3_9_2 = _dph*(2*adph*(2*B*_dph+3*C*_dpf) + 2*_dph*(2*B*adph+3*C*(1+F(s)*ss)) + 15*EGss(s,1));
    //double term3_11 = _dph*_dph*(2*adph*(2*B*adph+3*C*(1+F(s)*ss))+15*EGss(s));
    double term3_11 = _dph*_dph*(2*adph*(2*B*adph+3*C*(1+F(s)*ss))+15*epg);
    double term3 = (term3_7_1+term3_7_2+term3_7_3)/pow(adph, 3.5) - 7 * (term3_9_1+term3_9_2)/pow(adph, 4.5) + 63.0/4*term3_11/pow(adph, 5.5);
    Verbose::single() << "term = " << c1*term1 << ", " << c2*term2_coeff*term2 << ", " << c3*term3 << std::endl;
    Verbose::single() << "term3 = " << term3_7_1 << "+" << term3_7_2 << "+" << term3_7_3 << ", " << term3_9_1 << "+" << term3_9_2 << ", " << term3_11 << std::endl;
    //Verbose::single() << "c3*term3 should be = - 5.629790903162742E-3" << std::endl;
    Verbose::single() << "c3*term3 should be = - 2.973021851092979E-2 (s=0 gives prob maxima bug)" << std::endl;
    //Verbose::single() << "term3 should be = - 9.007665445060387E-2" << std::endl;
    //Verbose::single() << "EGss term = " << c3*(15*EGss(s,2)/pow(adph, 3.5)-7*(15*EGss(s)*_ddph+15*EGss(s,1)*_dph)/pow(adph, 4.5)+63./4*_dph*_dph*15*EGss(s)/pow(adph, 5.5)) << std::endl;
    //Verbose::single() << "EGss term = " << c3*(15*EGss(s,2)/pow(adph, 3.5)-7*(15*epg*_ddph+15*EGss(s,1)*_dph)/pow(adph, 4.5)+63./4*_dph*_dph*15*epg/pow(adph, 5.5)) << std::endl;
    //Verbose::single() << "EGss term should be (coeff included)= 1.573486008975906E-2" << std::endl;
    Parallel_Manager::info().all_barrier();
    exit(0);
}
#endif
//    */

/*
 * For debug:
 * A: 1.0161144; B: -0.37170836; C: -0.077215461; D: 0.57786348; E: -0.051955731;
 * H(s):= (0.00979681*s*s+0.0410834*s**4)/(1+0.18744*s**4+0.00120824*s**5+0.0347188*s**6);
 * //F(s):= 6.475*H(s)+0.4797;
 * F(s):= 6.475387061*H(s)+0.479658304;
 * EGss(s):=-(3*%pi/4+%pi**.5*(15*E+6*C*(1+F(s)*s**2)*(D+H(s)*s**2)+4*B*(D+H(s)*s**2)**2+8*A*(D+H(s)*s**2)**3)/16/(D+H(s)*s**2)**(7/2) - 3*%pi*A**0.5/4*exp(9*H(s)*s**2/4/A)*erfc(3*s/2*sqrt(H(s)/A)))/15/sqrt(%pi)*16*(D+H(s)*s**2)**(7/2);
 * Fx(alpha, beta, s):= -8/9*beta*(A*(sqrt(%pi)/(2*sqrt(alpha+D+H(s)*s**2)) - 3*%pi/(4*sqrt(A))*exp(9/(4*A)*(alpha+H(s)*s**2))*erfc(1.5*sqrt((alpha+H(s)*s*s)/A)))+sqrt(%pi)*(15*(E+EGss(s))+2*(alpha+D+H(s)*s**2)*(3*C*(1+F(s)*s**2)+2*B*(alpha+D+H(s)*s**2)))/(16*(alpha+D+H(s)*s**2)**(7/2)));
 *
 * For MAXIMA: bfloat to get numerical value, bfloat(at(diff(Fx(a,b,s),s),[a=1,b=1,s=0])) for diff value.
 */


 void XC_LC_wPBE_NGau::xc_lda_x(void *dummy, int np, double *rho, double *ex, double *vrho, double *v2rho2, double *v3rho3){
     Verbose::all() << "XC_LC_wPBE_NGau::xc_lda_x called" << std::endl;
     throw std::logic_error("XC_LC_wPBE_NGau::xc_lda_x called");
 }
