#include "Force_KB.hpp"

#include <string>

#include "Create_Compute.hpp"

#include "../Core/Occupation/Occupation.hpp"
#include "../Core/Occupation/Occupation_Zero_Temp.hpp"
#include "../Core/ExternalField/External_Field.hpp"

#include "../Io/Atoms.hpp"
#include "../Util/String_Util.hpp"
#include "../Util/Lagrange_Derivatives.hpp"
#include "../Util/Parallel_Manager.hpp"
#include "../Util/Parallel_Util.hpp"
using std::string;
using std::vector;

using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;

/*
Force_KB::Force_KB(RCP<const Basis> mesh, RCP<const Atoms> atoms, RCP<Core_Hamiltonian> core_hamiltonian, RCP<Teuchos::ParameterList> parameters){
    this->mesh = mesh;
    this->atoms = atoms;
    //this->states = states;
    this->core_hamiltonian = core_hamiltonian;
    this->parameters = parameters;
}
*/
Force_KB::Force_KB(
        RCP<const Basis> mesh, RCP<const Atoms> atoms,
        RCP<Nuclear_Potential> nuclear_potential,
        RCP<External_Field> external_field,
        RCP<Teuchos::ParameterList> parameters):Force(mesh, atoms, nuclear_potential,external_field, parameters){
    this -> force_derivative_method = parameters->sublist("Force").get<string>("ForceDerivative", "Potential");
};

vector<double> Force_KB::compute_NN(){
    Verbose::single(Verbose::Detail) << "----- Force::Start Computing Force-NN Part" << std::endl;
    vector<double> F_NN;
    vector<double> Zval = this->nuclear_potential->get_kbprojector()->get_Zvals();
    for(int i=0;i<atoms->get_size();i++){
        int itype = atoms->get_atom_type(i);
        double tmp_x=0.0, tmp_y=0.0, tmp_z=0.0;

        for(int j=0 ;j<atoms->get_size();j++){
            int jtype = atoms->get_atom_type(j);
            if(i != j){
                double tmp = -Zval[itype] * Zval[jtype] / pow(atoms->distance(i,j),3);
                tmp_x += tmp * (atoms->get_positions()[i][0]-atoms->get_positions()[j][0]);
                tmp_y += tmp * (atoms->get_positions()[i][1]-atoms->get_positions()[j][1]);
                tmp_z += tmp * (atoms->get_positions()[i][2]-atoms->get_positions()[j][2]);
            }
        }
        F_NN.push_back(tmp_x);
        F_NN.push_back(tmp_y);
        F_NN.push_back(tmp_z);
    }
    Verbose::single(Verbose::Detail) << "----- Force::Finalize Computing Force-NN Part" << std::endl;
    return F_NN;
}

vector<double> Force_KB::compute_local(Array< RCP<const Epetra_Vector> > density, Array< RCP<const Occupation > > occupation, Array< RCP<const Epetra_MultiVector> > orbitals, Array< Array< RCP< Epetra_MultiVector> > > grad_orbital){
    vector<double> F_loc;
    vector<std::array<double,3> > atoms_positions = atoms->get_positions();
     int size = mesh->get_original_size();
    int i_x=0, i_y=0, i_z=0;
    const double** scaled_grid = mesh->get_scaled_grid();
    double scale = mesh->get_scaling()[0]*mesh->get_scaling()[1]*mesh->get_scaling()[2];

    //Using Pseudopotential as external potential
    int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements = mesh->get_map()->NumMyElements();


    vector< vector<double > > local_pp_dev = this->nuclear_potential->get_kbprojector()->get_V_local_dev();
    vector< vector<double > > local_pp = this->nuclear_potential->get_kbprojector()->get_V_local();
    vector<double > local_cutoff = this->nuclear_potential->get_kbprojector()->get_local_cutoff();

    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);
        double tmp_x=0, tmp_y=0, tmp_z=0;

        if(this -> force_derivative_method == "Potential"){
            for(int j=0;j<NumMyElements;j++){
                double x,y,z,r;
                mesh -> decompose(MyGlobalElements[j], &i_x, &i_y, &i_z);

                x = (scaled_grid[0][i_x] - atoms_positions[iatom][0]);
                y = (scaled_grid[1][i_y] - atoms_positions[iatom][1]);
                z = (scaled_grid[2][i_z] - atoms_positions[iatom][2]);
                r = sqrt(x*x + y*y + z*z);

                if(r <= local_cutoff[itype]){
                    for(int t=0; t< density.size(); t++){
                        double tmp=0.0;

                        if(r>=1.0E-5){
                            tmp = -scale*density[t]->operator[](j)*local_pp_dev[iatom][MyGlobalElements[j]]/r;
                            tmp_x += tmp * x;
                            tmp_y += tmp * y;
                            tmp_z += tmp * z;
                        }
                        else{
                            tmp_x += 0.0;
                            tmp_y += 0.0;
                            tmp_z += 0.0;
                        }
                    }
                }
            }
        } else if(this -> force_derivative_method == "Orbital"){
            for(int i=0; i<NumMyElements; i++){
                double x,y,z,r;
                mesh -> decompose(MyGlobalElements[i], &i_x, &i_y, &i_z);

                x = (scaled_grid[0][i_x] - atoms_positions[iatom][0]);
                y = (scaled_grid[1][i_y] - atoms_positions[iatom][1]);
                z = (scaled_grid[2][i_z] - atoms_positions[iatom][2]);
                r = sqrt(x*x + y*y + z*z);
                if(r<=local_cutoff[itype]){
                    for(int alpha=0; alpha<orbitals.size(); alpha++){ //alpha : spin index
                        for(int t=0; t<orbitals[alpha]->NumVectors(); t++){ // t : orbital index
                            tmp_x += 2*occupation[alpha]->operator[](t)*local_pp[iatom][MyGlobalElements[i]]*orbitals[alpha]->operator[](t)[i]*grad_orbital[alpha][0]->operator[](t)[i];
                            tmp_y += 2*occupation[alpha]->operator[](t)*local_pp[iatom][MyGlobalElements[i]]*orbitals[alpha]->operator[](t)[i]*grad_orbital[alpha][1]->operator[](t)[i];
                            tmp_z += 2*occupation[alpha]->operator[](t)*local_pp[iatom][MyGlobalElements[i]]*orbitals[alpha]->operator[](t)[i]*grad_orbital[alpha][2]->operator[](t)[i];
                        }
                    }
                }
            }
        }

        double total_tmp_x=0, total_tmp_y=0, total_tmp_z=0;

        //mesh->get_map()->Comm().SumAll(&tmp_x, &total_tmp_x, 1);
        //mesh->get_map()->Comm().SumAll(&tmp_y, &total_tmp_y, 1);
        //mesh->get_map()->Comm().SumAll(&tmp_z, &total_tmp_z, 1);
        Parallel_Util::group_sum(&tmp_x, &total_tmp_x, 1);
        Parallel_Util::group_sum(&tmp_y, &total_tmp_y, 1);
        Parallel_Util::group_sum(&tmp_z, &total_tmp_z, 1);

        F_loc.push_back(total_tmp_x);
        F_loc.push_back(total_tmp_y);
        F_loc.push_back(total_tmp_z);
    }
    //}

/*
//Using modelpotential as external potential
else if(parameters->sublist("Pseudopotential").get<int>("Pseudopotential") == 2){
if(MyPID == 0) cout << "Not implemented for model potential yet" << endl;
exit(-1);
}

//Using all-electron potential as external potential
else{
for(int i=0;i<atoms->get_size();i++){
double tmp_x = 0, tmp_y = 0, tmp_z = 0;
for(int j=0;j<NumMyElements;j++){
double x,y,z,r;
grid_setting -> decompose(MyGlobalElements[j], basis->get_points(), &i_x, &i_y, &i_z);
x = (scaled_grid[0][i_x] - atoms_positions[i][0]);
y = (scaled_grid[1][i_y] - atoms_positions[i][1]);
z = (scaled_grid[2][i_z] - atoms_positions[i][2]);
r = sqrt(x*x + y*y + z*z);

for(int k=0; k<orbitals[0]->NumVectors(); k++){
for(int t=0; t<orbitals.size(); t++){
double tmp = -scale*density->operator[](t)[j]*atoms->get_atomic_numbers()[i]/pow(r,3);
tmp_x += tmp * x ;
tmp_y += tmp * y ;
tmp_z += tmp * z ;
}
}
}

double total_tmp_x=0, total_tmp_y=0, total_tmp_z=0;
map->Comm().SumAll(&tmp_x, &total_tmp_x, 1);
map->Comm().SumAll(&tmp_y, &total_tmp_y, 1);
map->Comm().SumAll(&tmp_z, &total_tmp_z, 1);

F_loc.push_back(total_tmp_x);
F_loc.push_back(total_tmp_y);
F_loc.push_back(total_tmp_z);
}
}
 */
    Verbose::single(Verbose::Detail) << "----- Force::Finalize Computing Force-Local Part" << std::endl;
    return F_loc;
}

vector<double> Force_KB::compute_nonlocal(Array< RCP<const Epetra_MultiVector> > orbitals, Array< RCP<const Occupation> > occupations, Array< Array< RCP< Epetra_MultiVector> > > grad_orbital){
    Verbose::single(Verbose::Detail) << "----- Force::Start Computing Force-Nonlocal Part" << std::endl;
    int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements =mesh->get_map()->NumMyElements();
    vector<double> F_NL;
    vector<std::array<double,3> > atoms_positions = atoms->get_positions();
     int size = mesh->get_original_size();
    const double** scaled_grid = mesh->get_scaled_grid();

    vector< vector<double> > local_pp;
    //vector<double> local_pp;
    vector< vector<int> > oamom;
    vector<int> number_vps_file;
    vector< vector<double> > EKB;
    vector< vector< vector< vector < vector<double> > > > > V_vector;
    vector< vector< vector< vector < vector<int> > > > > V_index;

    this->nuclear_potential->get_kbprojector()->get_vector_address(local_pp, V_vector, V_index, EKB, oamom, number_vps_file);

    //vector< vector<int> > oamom = core_hamiltonian->get_core_hamiltonian_matrix()->nuclear_potential->get_kbprojector()->get_oamom();
    //vector<int> number_vps_file = core_hamiltonian->get_core_hamiltonian_matrix()->nuclear_potential->get_kbprojector()->get_number_vps_file();
    //vector< vector<double> > EKB = core_hamiltonian->get_core_hamiltonian_matrix()->nuclear_potential->get_kbprojector()->get_input_EKB();
    //vector< vector< vector< vector < vector<double> > > > > V_vector = core_hamiltonian->get_core_hamiltonian_matrix()->nuclear_potential->get_kbprojector()->get_V_nl();
    //vector< vector< vector< vector < vector<int> > > > > V_index = core_hamiltonian->get_core_hamiltonian_matrix()->nuclear_potential->get_kbprojector()->get_V_index();
    vector< vector< vector< vector < vector<double> > > > > V_dev_x = this->nuclear_potential->get_kbprojector()->get_V_nl_dev_x();
    vector< vector< vector< vector < vector<double> > > > > V_dev_y = this->nuclear_potential->get_kbprojector()->get_V_nl_dev_y();
    vector< vector< vector< vector < vector<double> > > > > V_dev_z = this->nuclear_potential->get_kbprojector()->get_V_nl_dev_z();

    double scale = mesh->get_scaling()[0]*mesh->get_scaling()[1]*mesh->get_scaling()[2];

    //if(parameters->sublist("BasicInformation").sublist("Pseudopotential").get<int>("Pseudopotential") == 1){
    int number_atom=atoms->get_size();
    for(int iatom=0;iatom<number_atom;iatom++){
        double tmp_x=0, tmp_y=0, tmp_z=0;
        int itype = atoms->get_atom_type(iatom);
        for(int alpha=0; alpha<orbitals.size(); alpha++){

            int Row = orbitals[alpha]->NumVectors();
            int Col = mesh->get_original_size();

            //Re-make orbitals to compute parallely
            double** totcoef = new double* [Row];
            //double** tmpcoef = new double* [Row];
            for(int i=0; i<Row; i++){
                //tmpcoef[i] = new double [Col];
                totcoef[i] = new double [Col];
                //memset(tmpcoef[i], 0.0, sizeof(double)*Col);
                memset(totcoef[i], 0.0, sizeof(double)*Col);
            }
            /*

            for(int t=0; t<Row; t++){
                for(int i=0; i<NumMyElements; i++){
                    tmpcoef[t][MyGlobalElements[i]] = orbitals[alpha]->operator[](t)[i];
                }
                mesh->get_map()->Comm().SumAll(tmpcoef[t], totcoef[t], Col);
            }
            */
            Parallel_Util::group_allgather_multivector(orbitals[alpha], totcoef);

            if(this -> force_derivative_method == "Potential"){
                for(int k=0;k<number_vps_file[itype];k++){
                    int l = oamom[itype][k];
                    int EKB_index = k*(number_vps_file[itype]+1);
                    double EKB_value = EKB[itype][EKB_index];
                    for(int m=0;m<2*l+1;m++){
                        for (int p=0;p< V_vector[iatom][k][m].size();p++){
                            int index_size = V_index[iatom][k][m][p].size();
                            for(int i=0;i<index_size;i++){
                                int i_index = V_index[iatom][k][m][p][i];
                                if(mesh->get_map()->MyGID(i_index)){
                                    for(int j=0;j<index_size;j++){
                                        int j_index = V_index[iatom][k][m][p][j];
                                        for(int s=0; s<orbitals[alpha]->NumVectors(); s++){
                                            tmp_x += -occupations[alpha]->operator[](s)*totcoef[s][j_index]*totcoef[s][i_index] * EKB_value * ( V_dev_x[iatom][k][m][p][i] * V_vector[iatom][k][m][p][j] + V_vector[iatom][k][m][p][i] * V_dev_x[iatom][k][m][p][j] );
                                            tmp_y += -occupations[alpha]->operator[](s)*totcoef[s][j_index]*totcoef[s][i_index] * EKB_value * ( V_dev_y[iatom][k][m][p][i] * V_vector[iatom][k][m][p][j] + V_vector[iatom][k][m][p][i] * V_dev_y[iatom][k][m][p][j] );
                                            tmp_z += -occupations[alpha]->operator[](s)*totcoef[s][j_index]*totcoef[s][i_index] * EKB_value * ( V_dev_z[iatom][k][m][p][i] * V_vector[iatom][k][m][p][j] + V_vector[iatom][k][m][p][i] * V_dev_z[iatom][k][m][p][j] );
                                            //tmp_energy += occupations[alpha]->operator[](s)*totcoef[s][j_index]*totcoef[s][i_index] * EKB_value * V_vector[iatom][k][m][p][j] * V_vector[iatom][k][m][p][i];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(this -> force_derivative_method == "Orbital"){
                /*
                   double*** grad_totcoef = new double** [3];
                //double** grad_tmp_tmpcoef = new double* [Row];
                for(int k=0; k<3; k++){
                double** grad_tmp_tmpcoef = new double* [Row];
                double** grad_tmp_totcoef = new double* [Row];
                for(int i=0; i<Row; i++){
                grad_tmp_tmpcoef[i] = new double [Col];
                grad_tmp_totcoef[i] = new double [Col];
                memset(grad_tmp_tmpcoef[i], 0.0, sizeof(double)*Col);
                memset(grad_tmp_totcoef[i], 0.0, sizeof(double)*Col);
                }


                for(int t=0; t<Row; t++){
                for(int i=0; i<NumMyElements; i++){
                grad_tmp_tmpcoef[t][MyGlobalElements[i]] = grad_orbital[alpha][k]->operator[](t)[i];
                }
                mesh->get_map()->Comm().SumAll(grad_tmp_tmpcoef[t], grad_tmp_totcoef[t], Col);
                }
                grad_totcoef[k] = grad_tmp_totcoef;
                }
                 */
                double*** grad_totcoef = new double** [3];
                //double*** grad_tmpcoef = new double** [3];
                for(int k=0; k<3; k++){
                    //double** grad_tmpcoef[k] = new double* [Row];
                    //double** grad_tmpcoef[k] = new double* [Row];
                    //grad_tmpcoef[k] = new double* [Row];
                    grad_totcoef[k] = new double* [Row];
                    //memset(grad_tmpcoef[k], 0.0, sizeof(double)*Row);
                    //memset(grad_totcoef[k], 0.0, sizeof(double)*Row);
                    for(int i=0; i<Row; i++){
                        //grad_tmpcoef[k][i] = new double [Col];
                        grad_totcoef[k][i] = new double [Col];
                        //memset(grad_tmpcoef[k][i], 0.0, sizeof(double)*Col);
                        memset(grad_totcoef[k][i], 0.0, sizeof(double)*Col);
                    }

                    /*
                    for(int t=0; t<Row; t++){
                        for(int i=0; i<NumMyElements; i++){
                            grad_tmpcoef[k][t][MyGlobalElements[i]] = grad_orbital[alpha][k]->operator[](t)[i];
                        }
                        mesh->get_map()->Comm().SumAll(grad_tmpcoef[k][t], grad_totcoef[k][t], Col);
                    }
                    */
                    Parallel_Util::group_allgather_multivector(grad_orbital[alpha][k], grad_totcoef[k]);
                    //grad_totcoef[k] = grad_tmp_totcoef;
                }

                for(int k=0;k<number_vps_file[itype];k++){
                    int l = oamom[itype][k];
                    int EKB_index = k*(number_vps_file[itype]+1);
                    double EKB_value = EKB[itype][EKB_index];
                    for(int m=0;m<2*l+1;m++){
                        for (int p=0;p< V_vector[iatom][k][m].size();p++){
                            int index_size = V_index[iatom][k][m][p].size();
                            for(int i=0;i<index_size;i++){
                                int i_index = V_index[iatom][k][m][p][i];
                                if(mesh->get_map()->MyGID(i_index)){
                                    for(int j=0;j<index_size;j++){
                                        int j_index = V_index[iatom][k][m][p][j];
                                        for(int s=0; s < orbitals[alpha]->NumVectors(); s++){
                                            tmp_x += 2*occupations[alpha]->operator[](s)*totcoef[s][j_index]*grad_totcoef[0][s][i_index] * EKB_value * V_vector[iatom][k][m][p][j] * V_vector[iatom][k][m][p][i];
                                            tmp_y += 2*occupations[alpha]->operator[](s)*totcoef[s][j_index]*grad_totcoef[1][s][i_index] * EKB_value * V_vector[iatom][k][m][p][j] * V_vector[iatom][k][m][p][i];
                                            tmp_z += 2*occupations[alpha]->operator[](s)*totcoef[s][j_index]*grad_totcoef[2][s][i_index] * EKB_value * V_vector[iatom][k][m][p][j] * V_vector[iatom][k][m][p][i];

                                        }
                                    }
                                }
                            }
                        }
                    }
                }


                // Remove memory allocated for saving orbital-gradients
                /*
                   for(int i=0; i<Row; i++){
                   delete[] grad_tmp_tmpcoef[i];
                   }
                   for(int k=0; k<3; k++){
                   for(int i=0; i<Row; i++){
                   delete[] grad_totcoef[k][i];
                   }
                   delete[] grad_totcoef[k];
                   delete[] grad_tmp_tmpcoef;
                   }
                //delete[] grad_tmp_tmpcoef;
                delete[] grad_totcoef;
                 */
                for(int k=0; k<3; k++){
                    for(int i=0; i<Row; i++){
                        delete[] grad_totcoef[k][i];
                        //delete[] grad_tmpcoef[k][i];
                    }
                    delete[] grad_totcoef[k];
                    //delete[] grad_tmpcoef[k];
                }
                //delete[] grad_tmpcoef;
                delete[] grad_totcoef;
            }


            // Remove memory allocated for saving orbitals //
            for(int i=0; i<Row; i++){
                //delete[] tmpcoef[i];
                delete[] totcoef[i];
            }
            //delete[] tmpcoef;
            delete[] totcoef;
        }


        double total_tmp_x=0.0, total_tmp_y=0.0, total_tmp_z=0.0;
        Parallel_Util::group_sum(&tmp_x, &total_tmp_x, 1);
        Parallel_Util::group_sum(&tmp_y, &total_tmp_y, 1);
        Parallel_Util::group_sum(&tmp_z, &total_tmp_z, 1);

        F_NL.push_back(total_tmp_x);
        F_NL.push_back(total_tmp_y);
        F_NL.push_back(total_tmp_z);
    }
    //}

/*
    //Using modelpotential as external potential
    else if(parameters->sublist("Pseudopotential").get<int>("Pseudopotential") == 2){
        if(MyPID == 0) cout << "Not implemented for model potential yet" << endl;
        exit(-1);
    }

    //Using all-electron potential as external potential
    else{
        if(MyPID == 0) cout << "All electron - nonlocal !!! " << endl;
    }
*/

    Verbose::single(Verbose::Detail) << "----- Force::Finalize Computing Force-Nonlocal Part" << std::endl;
    return F_NL;
}

int Force_KB::compute(RCP<const Basis> mesh,Array<RCP<State> >& states){
    Verbose::single(Verbose::Detail) << "----- Force::Start Computing Force-Total" << std::endl;
    clock_t st,et;

    minus_F_tot.clear();

    //Using Hellmann-Feynman theorem
    //dE/dR = dE_(loc)/dR + dE_(NL)/dR + dE_(NN)/dR
    //F = F_loc + F_NL + F_NN
    RCP<State> state = states[states.size()-1];

    Array< RCP<const Epetra_Vector> > density = state->get_density();
    //Array< RCP<Epetra_MultiVector> > orbitals = state->get_orbitals();
    Array< RCP<const Epetra_MultiVector> > orbitals = state->get_orbitals();
    Array< RCP<const Occupation > > occupation = state->get_occupations();
    Array< Array< RCP<Epetra_MultiVector> > > grad_orbital;
    //Using Pseudopotential as external potential
    //if(parameters->sublist("BasicInformation").sublist("Pseudopotential").get<int>("Pseudopotential") == 1){
        st = clock();
         int size = mesh->get_original_size();
        if(this -> force_derivative_method == "Orbital"){
            Verbose::single(Verbose::Detail) << "----- Force::Computing Orbital Gradient to Get Force" << std::endl;
            Parallel_Manager::info().all_barrier();
            clock_t st_grad = clock();
            for(int alpha=0; alpha<orbitals.size(); alpha++){
                Array< RCP<Epetra_MultiVector> > grad_orbital_tmp;
                for(int i=0; i<3; i++){
                    grad_orbital_tmp.push_back(rcp(new Epetra_MultiVector(orbitals[alpha]->Map(), orbitals[alpha]->NumVectors(), true))); ///map?? How to use this?
                }
                Lagrange_Derivatives::gradient(mesh, orbitals[alpha], grad_orbital_tmp, true);
                grad_orbital.push_back(grad_orbital_tmp);
            }
            Parallel_Manager::info().all_barrier();
            clock_t et_grad = clock();
            Verbose::single(Verbose::Detail) << "----- Force::Time to Compute Orbital Gradient : " << double(et_grad-st_grad)/CLOCKS_PER_SEC << " s" << std::endl;
        }

        vector<double> F_loc = this->compute_local(density, occupation, orbitals, grad_orbital);
        vector<double> F_NL = this->compute_nonlocal(orbitals, occupation, grad_orbital);
        vector<double> F_NN = this->compute_NN();
        et = clock();

        Verbose::single(Verbose::Simple) << std::endl << std::endl;
        if(F_loc.size() == F_NL.size() & F_loc.size() == F_NN.size()){
            for(int i=0; i<F_loc.size(); i++){
                int a = i/3;
                int b = i-a*3;
                char coord;
                if ( b==0 ) coord = 'x';
                else if ( b==1 ) coord = 'y';
                else if ( b==2 ) coord = 'z';

                double tmp=0.0;
                //tmp = -(F_loc.at(i) + F_NL.at(i) + F_NN.at(i));
                tmp = F_loc.at(i) + F_NL.at(i) + F_NN.at(i);
                /*if(abs(tmp)<=parameters->sublist("Opt").get<double>("ConvergenceTolerance")){
                    tmp=0.0;
                }*/

                Verbose::single(Verbose::Normal) << coord << "-direction force on " << a << "th atom " << std::endl;
                Verbose::single(Verbose::Normal) << " F_L : " << F_loc.at(i) << "\tF_NL : " << F_NL.at(i) << "\tF_NN : " << F_NN.at(i) << std::endl;
                Verbose::single(Verbose::Normal) << " minus_F_tot : " << tmp << std::endl;
                Verbose::single(Verbose::Normal) << "=============================================" << std::endl;

                minus_F_tot.push_back(tmp);
            }
            Verbose::single(Verbose::Simple) << "Time to compute Force : " << double(et-st)/CLOCKS_PER_SEC << " s" << std::endl;;
        }
    //}

    /*
    //Using modelpotential as external potential
    else if(parameters->sublist("Pseudopotential").get<int>("Pseudopotential") == 2){
        cout << "Not implemented for model potential yet" << endl;
//        exit(-1);
    }
    //Using all-electron potential as external potential
    else{
        st = clock();
        vector<double> F_loc = this->compute_local(density, orbitals, occupation, grad_orbital);
        vector<double> F_NN = this->compute_NN();
        et = clock();
        if(F_loc.size() == F_NN.size()){
            for(int i=0; i<F_loc.size(); i++){
                int a = i/3;
                int b = i-a*3;
                char coord;
                if ( b==0 ) coord = 'x';
                else if ( b==1 ) coord = 'y';
                else if ( b==2 ) coord = 'z';
                double tmp=0;
                tmp = F_loc.at(i) + F_NN.at(i);
                if(MyPID == 0){
                    cout << coord << "direction force on " << a << "th atom " << endl;
                    cout << " minus_F_tot : " << tmp << endl;
                    cout << "=============================================" << endl;
                }
                minus_F_tot.push_back(tmp);
            }
        }
        if(MyPID==0){
            cout << "time to compute Force_KB : " << double(et-st)/CLOCKS_PER_SEC << " s" << endl;
            cout << endl;
        }
    }
    */

    // system is under external field
    if(external_field!=Teuchos::null){
        Verbose::single(Verbose::Detail) << "----- Force::Start External Field Part" << std::endl;

        auto force_from_external_field = external_field->get_force(state);
        Verbose::single(Verbose::Detail) <<"1" <<std::endl;
        if(force_from_external_field.size()!=minus_F_tot.size() ){
            Verbose::all() << "[Force_KB]  something wrong!! " << std::endl;
            exit(-1);
        }
        for (int i =0; i< minus_F_tot.size(); i++){
            minus_F_tot[i]-=force_from_external_field[i];
        }
        Verbose::single(Verbose::Simple) << "[Force_KB]  force from external field is now added " << std::endl;
        Verbose::single(Verbose::Simple) << "[Force_KB]  final results are following " << std::endl;
    }
    Force::print_force();
    Verbose::single(Verbose::Detail) << "----- Force::Finalize Computing Force-Total" << std::endl;
    return 0;
}
