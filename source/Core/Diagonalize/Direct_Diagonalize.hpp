#pragma once
#include "Teuchos_LAPACK.hpp"
#include "Diagonalize.hpp"


/**
 * @author Jaewook Kim, Sungwoo Kang
 **/
class Direct_Diagonalize: public Diagonalize{
    public:
        Direct_Diagonalize(bool is_sym_real_only = true);
        ~Direct_Diagonalize();
        bool diagonalize(Teuchos::RCP<Epetra_CrsMatrix> matrix, int numev, Teuchos::RCP<Epetra_MultiVector> initial_eigenvector=Teuchos::null, Teuchos::RCP<Epetra_CrsMatrix> overlap_matrix = Teuchos::null);
        bool diagonalize(
            Teuchos::RCP<Core_Hamiltonian_Matrix> core_hamiltonian_matrix, int ispin, int numev, Teuchos::RCP<Epetra_Vector> local_potential, 
            Teuchos::RCP<Epetra_MultiVector> initial_eigenvector = Teuchos::null 
        );

        bool diagonalize(const int matrix_dimension, const int numev, double* matrix);

        Teuchos::RCP<Epetra_MultiVector> get_eigenvectors();
        std::vector<double > get_eigenvalues();

    private:
        Teuchos::RCP<Epetra_MultiVector> eigenVectors;
        Teuchos::RCP<Epetra_MultiVector> eigenVectors_imag;
        std::vector< double > eigenValues;
        std::vector<double> eigenValues_imag;
        bool is_sym_real;
        //bool is_positive_overlap;
};
