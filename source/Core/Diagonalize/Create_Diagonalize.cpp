#include "Create_Diagonalize.hpp"
#include "../../Util/Verbose.hpp"
#include "../../Util/Parallel_Manager.hpp"
#include "Pure_Diagonalize.hpp"

#include "Direct_Diagonalize.hpp"

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime_api.h>
#endif

using std::string;
using Teuchos::RCP;

RCP<Diagonalize> Create_Diagonalize::Create_Diagonalize(RCP<Teuchos::ParameterList> parameters, Teuchos::Array< std::vector<double> > eigvals/* = Teuchos::Array< std::vector<double> >()*/){
    RCP<Diagonalize> retVal;
    string solver = parameters -> get<string>("Solver", "Pure");
    string eigen_solver = parameters -> get<string>("EigenSolver", "LOBPCG");
    int max_iter = parameters -> get<int>("DiagonalizeMaxIter", 40);

    double tolerance = parameters -> get<double>("Tolerance", 1.0e-6);

    if(solver=="Direct"){
        bool is_symm_real = parameters -> get<bool>("SymmReal", true);
        //bool is_positive_overlap = parameters -> get<bool>("OverlapPositiveDefinite", true);
        retVal = Teuchos::rcp(new Direct_Diagonalize(is_symm_real));
    }
    else{
        int block_size = parameters->get<int>("BlockSize", 0);
        int prec_overlap_level = parameters->get<int>("PrecOverlapLevel", 1);
        string verb = parameters->get<string>("DiagonalizeVerbosity", "Simple");
        double locking_tolerance = 0.00000001;
        int max_locked = 0;
        if (parameters->get<int>("Locking",0)!=0){
            Verbose::single(Verbose::Normal) << "Locking is turned on" << std::endl;
            locking_tolerance = parameters->get<double>("LockingTolerance", 0.00000001);
            max_locked = parameters -> get<int>("MaxLocked", block_size);
            Verbose::single(Verbose::Normal) << "Locking Tolerenace: " << locking_tolerance << std::endl;
            Verbose::single(Verbose::Normal) << "Max. locked vector: " << max_locked << std::endl;
        }

        bool full_ortho = (parameters -> get<int>("FullOrthogonalize", 0) == 0)? false: true;
        bool redistribution = (parameters -> get<int>("Redistribution", 0) == 0)? false: true;

        std::string redistribute_method = parameters->get<std::string>("PartitioningMethod", "block");
        double balancing_tol = std::fabs(parameters->get<double>("Balancing_Tolerance", 2.0));
        string precond_type = parameters->get<string>("DiagonalizeSolverPrec", "None");

        //string precond_type = "None";
        if(parameters->isParameter("DiagonalizeSolverPrec") ){
            precond_type = parameters->get<string>("DiagonalizeSolverPrec");
        }

        bool gpu_enable = false;
        int NGPU = 1;
#ifdef USE_CUDA
        if(parameters->get<int>("NGPU",0)>0 ){
            Parallel_Manager::info().assign_gpus(parameters->get<int>("NGPU"),Parallel_Manager::Diag);
            gpu_enable = true;
        }
#else
        if(parameters->isParameter("GPUEnable") and parameters->get<int>("GPUEnable")==0 ){
            std::cout << "ACE-Molecule is not compiled with GPU enable" << std::endl;
            exit(-1);
        }
#endif

        retVal = Teuchos::rcp(new Pure_Diagonalize(block_size, prec_overlap_level, max_iter, verb,  locking_tolerance, max_locked, full_ortho, tolerance, precond_type, eigen_solver, redistribution,balancing_tol,redistribute_method, gpu_enable, NGPU ) );
        //string precond_type = "None";
        //if(parameters->isParameter("DiagonalizeSolverPrec") ){
        //    precond_type = parameters->get<string>("DiagonalizeSolverPrec");
        //}
        //retVal = Teuchos::rcp(new Pure_Diagonalize(block_size, prec_overlap_level, max_iter, verb,  locking_tolerance, max_locked, full_ortho, tolerance, precond_type, eigen_solver, redistribution,balancing_tol,redistribute_method ) );
    }
    if(retVal == Teuchos::null){
        Verbose::all() << "Unsupported Diagonalize solver" << std::endl;
        exit(-1);
    }
    return retVal;
}
