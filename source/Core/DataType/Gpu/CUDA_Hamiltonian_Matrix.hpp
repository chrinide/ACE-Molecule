#pragma once 
#ifdef USE_CUDA

#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "AnasaziOperator.hpp"
#include "AnasaziMultiVec.hpp"
#include "Epetra_Operator.h"
#include "cusparse.h"
#include "CUDA_MultiVec.hpp"
#include "driver_types.h"
#include "Epetra_CrsMatrix.h"

namespace CUDA{

template<class ScalarType>
class Hamiltonian_Matrix  {
//class Hamiltonian_Matrix : public Anasazi::Operator<ScalarType> {
    public:
        Hamiltonian_Matrix(Teuchos::RCP<Epetra_CrsMatrix> hamiltonian_matrix, bool gpu_enable ):hamiltonian_matrix(hamiltonian_matrix),gpu_enable(gpu_enable){};

    //    std::cout << "Hamiltonian_Matrix_Multiplier, destructor is called" << std::endl;
        void set_spin(int spin);
        void Apply(const CUDA::MultiVec<ScalarType>& X, CUDA::MultiVec<ScalarType>& Y) const ;
        //void Apply(const Anasazi::MultiVec<ScalarType>& X, Anasazi::MultiVec<ScalarType>& Y) const ;
        ~Hamiltonian_Matrix();
        
    private:
        Teuchos::RCP<Epetra_CrsMatrix> hamiltonian_matrix;
        int ispin;
        bool gpu_enable;


        mutable int* d_row_index;
        mutable int* d_col_index;
        mutable double* d_values;
        mutable cusparseHandle_t cusparse_handle;

        mutable int nnz;
        mutable int num_row;
        mutable int num_vec = 0;
        mutable int global_num_row;


        mutable double* alpha;
        mutable double* beta ;

        mutable bool hamiltonian_matrix_in_gpu = false;
        mutable bool is_cuMallocHost = false;

        mutable double* host_combine_Mv_X;
        mutable double* host_Mv_X;
        mutable Teuchos::RCP<Epetra_Map> ColMap;
        mutable Teuchos::RCP<Epetra_Import> Importer;
        mutable double* combine_Mv_X;
//        mutable Teuchos::RCP<Epetra_CrsMatrix> hamiltonian_matrix;// = rcp(new Epetra_CrsMatrix(Copy, *mesh -> get_map(), 0) );

};

};
template class CUDA::Hamiltonian_Matrix<double>;
namespace Anasazi{

template < class ScalarType>
class OperatorTraits<ScalarType, CUDA::MultiVec<ScalarType>, CUDA::Hamiltonian_Matrix<ScalarType> >{
    public:
        static void Apply ( const CUDA::Hamiltonian_Matrix<ScalarType>& Op,
                      const CUDA::MultiVec<ScalarType>& x,
                      CUDA::MultiVec<ScalarType>& y )
        { Op.Apply( x, y ); }

};
};
#endif
