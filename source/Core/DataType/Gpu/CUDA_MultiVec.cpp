#ifdef USE_CUDA
#include "CUDA_MultiVec.hpp"
#include "nvToolsExt.h"
#include "cuda_runtime_api.h"
#include "../../../Util/Parallel_Manager.hpp"
#include "../../../Util/Parallel_Util.hpp"
#include "../../../Util/Verbose.hpp"
#include "device_launch_parameters.h"
#include <curand.h>
#include "../../../Util/Time_Measure.hpp"

#ifdef USE_MAGMA
#include "magma.h"
#include "magma_lapack.h"
#endif 

extern const char* cublasGetErrorString(cublasStatus_t status);//shchoi
namespace CUDA{

cublasHandle_t Global_handle=0;
//timer is defined in CUDA_Hamiltonian_Matrix.cpp
extern Teuchos::RCP<Time_Measure> timer;
using std::endl;
using std::cout;

template <class ScalarType>
//! Constructor for a \c numVecs vectors of length \c Length.
MultiVec<ScalarType>::MultiVec(const int _length, const int _numVecs, bool gpu_enable) :
    length(_length),
    numVecs(_numVecs)
{
#ifdef USE_MAGMA
//    magma_init();
#endif 
    //Verbose::single() << "constructor 1 " << std::endl;
    this->gpu_enable = gpu_enable;
    check();
    data_.resize(numVecs);
    ownership_.resize(numVecs);
    // Allocates memory to store the vectors.
    for (int v = 0 ; v < numVecs ; ++v)
    {
        data_[v] = new ScalarType[length]();
        ownership_[v] = true;
    }
    // Allocates memory in gpu
    d_data_.resize(numVecs);
    cudaMalloc((void**)& d_data_[0], sizeof(ScalarType)*length*numVecs);
    for (int v = 0; v < numVecs ; ++v){
        d_data_[v] = &d_data_[0][length*v];
    }
// Initializes all elements to zero.
    MvInit(0.0);

}


template <class ScalarType>
MultiVec<ScalarType>::MultiVec(const int _length, const int _numVecs, ScalarType** pointer, bool gpu_enable):
    length(_length),
    numVecs(_numVecs)
{
    //Verbose::single() << "constructor 2 " << std::endl;
    this->gpu_enable = gpu_enable;
    data_.resize(numVecs);
    ownership_.resize(numVecs);
    for(int v=0; v<numVecs; v++)
    {
        data_[v] = new ScalarType[length]();
    }
    is_data_in_gpu = true;
    d_data_.resize(numVecs);
    cudaMalloc((void**)& d_data_[0], sizeof(ScalarType)*length*numVecs);
    for (int v = 0 ; v < numVecs ; ++v){
        ownership_[v] = true;
        d_data_[v] = &d_data_[0][v*length];
    }
    for (int v = 0 ; v < numVecs ; ++v){
        cudaMemcpy(d_data_[v], pointer[v], sizeof(ScalarType)*length, cudaMemcpyHostToDevice);
    }
}
template <class ScalarType>
MultiVec<ScalarType>::MultiVec(const int _length, const int _numVecs, std::vector<ScalarType*> new_d_data,std::vector<ScalarType*> pointer, bool gpu_enable ):
    length(_length),
    numVecs(_numVecs)
{
    //Verbose::single() << "constructor 3 " << std::endl;
    this->gpu_enable = gpu_enable;
    check();
    data_.resize(numVecs);
    ownership_.resize(numVecs);
    // Allocates memory to store the vectors.
    for (int v = 0 ; v < _numVecs ; ++v)
    {
        data_[v] = pointer[v];
        ownership_[v] = false;
    }
    is_data_in_gpu = true;
    d_data_.resize(_numVecs);
    for (int v = 0 ; v < _numVecs ; ++v)
    {
        d_data_[v] = new_d_data[v];
    }
}


template <class ScalarType>
void MultiVec<ScalarType>::host_to_device()
{
    //Verbose::single() << "void MultiVec<ScalarType>::host_to_device()" << endl;
    for(int i=0; i<numVecs; i++)
        cudaMemcpy(d_data_[i], data_[i], sizeof(ScalarType)*length, cudaMemcpyHostToDevice);

}

//data transfer from gpu to cpu
template <class ScalarType>
void MultiVec<ScalarType>::device_to_host(){
    //Verbose::single() << "device to host " << std::endl;
    for(int i=0; i<numVecs; i++)
        cudaMemcpy(data_[i], d_data_[i], sizeof(ScalarType)*length, cudaMemcpyDeviceToHost);
}
//! Destructor
template <class ScalarType>
MultiVec<ScalarType>::~MultiVec()
{
    //Verbose::single() << "destructor " << std::endl;
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif 
    for (int v = 0 ; v < numVecs ; ++v)
        if (ownership_[v]) 
            delete[] data_[v];

    bool check = true;
    for (int v = 0 ; v < numVecs ; ++v){
        if (!ownership_[v]) 
            check=false;
    }
    if(check)
        cudaFree(d_data_[0]);
    
    //cudaDeviceSynchronize();
    //timer->print(Verbose::single());
}



//get address of data in gpu
template <class ScalarType>
void MultiVec<ScalarType>::get_address(std::vector<ScalarType*>& address) const{
    //cout << "ScalarType* MultiVec<ScalarType>::get_address() const" << endl;
    for(int i=0; i<d_data_.size(); i++)
        address.push_back(d_data_[i]);
}

template <class ScalarType>
void MultiVec<ScalarType>::set_address(std::vector<ScalarType*> address){
    //cout << "ScalarType* MultiVec<ScalarType>::set_address() const" << endl;
    d_data_.clear();
    for(int i=0; i<address.size(); i++){
        d_data_.push_back(address[i]);
    }
}

//! Returns a view of current vector (shallow copy)
    template <class ScalarType>
MultiVec<ScalarType>* MultiVec<ScalarType>::CloneViewNonConst(const std::vector< int > &index) 
{
    int size = index.size();
    std::vector<ScalarType*> values(size);
    MultiVec* tmp;  
    ScalarType* tmp_h_check;
    ScalarType* h_check;
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif 
    for (unsigned int v = 0 ; v < index.size() ; ++v)
        values[v] = data_[index[v]];
    std::vector<ScalarType*> d_values(size);
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif 
    for (unsigned int v = 0 ; v < index.size() ; ++v)
        d_values[v] = d_data_[index[v]];
    tmp = new MultiVec<ScalarType>(length, index.size(), d_values, values);
    return tmp;
}

//! Returns a view of current vector (shallow copy), const version.
template <class ScalarType>
const MultiVec<ScalarType>* MultiVec<ScalarType>::CloneView(const std::vector< int > &index) const
{
    int size = index.size();
    std::vector<ScalarType*> values(size);
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif 
    for (unsigned int v = 0 ; v < size ; ++v)
        values[v] = data_[index[v]];
    MultiVec<ScalarType>* tmp;
    ScalarType* tmp_d_data;
    std::vector<ScalarType*> d_values(size);
#ifdef ACE_HAVE_OMP
#pragma omp parallel for
#endif
    for (unsigned int v = 0 ; v < size ; ++v)
        d_values[v] = d_data_[index[v]];
    tmp = new MultiVec<ScalarType>(length, index.size(), d_values, values);
    return tmp;
}

template <class ScalarType>
int MultiVec<ScalarType>::GetVecLength () const
{
    return(length);
}

template <class ScalarType>
int MultiVec<ScalarType>::GetNumberVecs () const
{
    return(numVecs);
}

// Update *this with alpha * A * B + beta * (*this). 
    template <class ScalarType>
void MultiVec<ScalarType>::MvTimesMatAddMv (ScalarType alpha, const CUDA::MultiVec<ScalarType> &A, 
        const Teuchos::SerialDenseMatrix<int, ScalarType> &B, 
        ScalarType beta)
{
    std::cout << "ScalarType is not double" << endl;
    exit(-1);
}
// Update *this with alpha * A * B + beta * (*this). 
template <>
void MultiVec<double>::MvTimesMatAddMv (double alpha, const CUDA::MultiVec<double> &A, 
        const Teuchos::SerialDenseMatrix<int, double> &B, 
        double beta)
{
    timer->start("MvTimesMatAddMv");
    nvtxRangePushA("MvTimesMatAddMv");

    //Verbose::single() << "MvTimesMatAddMv " << std::endl;
    assert (length == A.GetVecLength());
    assert (B.numRows() == A.GetNumberVecs());
    assert (B.numCols() <= numVecs);

    MultiVec* MyA;
    MyA = dynamic_cast<MultiVec*>(&const_cast<CUDA::MultiVec<double> &>(A)); 
    assert(MyA!=NULL);
    if ((*this)[0] == (*MyA)[0]) {
        if(gpu_enable){
            std::cout << "this part is not complete, jaechang   MvTimesMatAddMv" << endl;

            exit(-1);
        }
    }
    else {
        const size_t B_numCols = B.numCols();
        const size_t B_numRows = B.numRows();
        double* d_B;
/*
        double* h_B = new double[B.numRows()*B.numCols()];
        #ifdef ACE_HAVE_OMP
        #pragma omp parallel for collapse(2)
        #endif 
        for(int i=0; i<B_numCols; i++){
            for(int j=0; j<B_numRows; j++)
                h_B[i*B_numRows+j] = B(j,i);
        }
*/
        cudaMalloc((void**)& d_B, sizeof(double)*B_numRows*B_numCols);
//        cudaMemcpy(d_B, h_B, sizeof(double)*B_numRows*B_numCols, cudaMemcpyHostToDevice);
        cudaMemcpy(d_B, B.values(), sizeof(double)*B_numRows*B_numCols, cudaMemcpyHostToDevice);
        std::vector<double* > address_A;
        MyA->get_address(address_A);
        cublasStatus_t  check = cublasDgemm(Global_handle, CUBLAS_OP_N, CUBLAS_OP_N, length, B.numCols(),B.numRows() , &alpha, address_A[0], length, d_B, B.numRows(),  &beta, d_data_[0], length);
        if(check!=CUBLAS_STATUS_SUCCESS){
            printf("error on MvTimesMatAddMv\n");
        }
        cudaDeviceSynchronize();
//        delete [] h_B;
        cudaFree(d_B);
    }
    nvtxRangePop();
    timer->end("MvTimesMatAddMv");
}

// Replace *this with alpha * A + beta * B. 
template <class ScalarType>
void MultiVec<ScalarType>::MvAddMv (ScalarType alpha, const CUDA::MultiVec<ScalarType>& A, 
        ScalarType beta,  const CUDA::MultiVec<ScalarType>& B)
{
    std::cout << "ScalarType is not double" << endl;
    exit(-1);
}
    template <>
void MultiVec<double>::MvAddMv (double alpha, const CUDA::MultiVec<double>& A, 
        double beta,  const CUDA::MultiVec<double>& B)
{
    timer->start("MvAddMv");
    //Verbose::single() << "MvAddMv " << std::endl;
    MultiVec* MyA;
    MyA = dynamic_cast<MultiVec*>(&const_cast<CUDA::MultiVec<double> &>(A)); 
    assert (MyA != 0);

    MultiVec* MyB;
    MyB = dynamic_cast<MultiVec*>(&const_cast<CUDA::MultiVec<double> &>(B)); 
    assert (MyB != 0);

    assert (numVecs == A.GetNumberVecs());
    assert (numVecs == B.GetNumberVecs());

    assert (length == A.GetVecLength());
    assert (length == B.GetVecLength());
    std::vector<double*> address_A;
    std::vector<double*> address_B;
    MyA->get_address(address_A);
    MyB->get_address(address_B);
    MvInit(0.0);
    cublasStatus_t check = cublasDaxpy(Global_handle, length*numVecs, &alpha, address_A[0], 1, d_data_[0], 1);
    if(check!=CUBLAS_STATUS_SUCCESS){
        printf("error   MvAddMv1\n");
    }
    check = cublasDaxpy(Global_handle, length*numVecs, &beta, address_B[0], 1, d_data_[0], 1);
    if(check!=CUBLAS_STATUS_SUCCESS){
        printf("error   MvAddMv2\n");
    }
    cudaDeviceSynchronize(); //shchoi

    timer->end("MvAddMv");
    
}
// Compute a dense matrix B through the matrix-matrix multiply alpha * A^H * (*this). 
template <class ScalarType>
void MultiVec<ScalarType>::MvTransMv (ScalarType alpha, const CUDA::MultiVec<ScalarType>& A, 
        Teuchos::SerialDenseMatrix< int, ScalarType >& B
#ifdef HAVE_ANASAZI_EXPERIMENTAL
        , CUDA::ConjType conj
#endif
        ) const
{
    cout << "ScalarType is not double " << endl;
    exit(-1);
}


template <>
void MultiVec<double>::MvTransMv (double alpha, const CUDA::MultiVec<double>& A, 
        Teuchos::SerialDenseMatrix< int, double >& B
#ifdef HAVE_ANASAZI_EXPERIMENTAL
        , CUDA::ConjType conj
#endif
        ) const
{
    timer->start("MvTransMv");
    nvtxRangePushA("MvTransMv");
//    Verbose::single() << "MvTransMv " << std::endl;
    MultiVec* MyA;
    MyA = dynamic_cast<MultiVec*>(&const_cast<CUDA::MultiVec<double> &>(A)); 
    assert (MyA != 0);
    assert (A.GetVecLength() == length);
    assert (numVecs <= B.numCols());
    assert (A.GetNumberVecs() <= B.numRows());

    const size_t A_numVecs = A.GetNumberVecs();
    cublasStatus_t  check;
#ifdef HAVE_ANASAZI_EXPERIMENTAL
    if (conj == CUDA::CONJ) {
#endif
            double* h_result = new double[A_numVecs*numVecs];
            double beta = 0.0;
            double* d_result;
		size_t total_byte, free_byte ;

		cudaError_t cuda_status = cudaMemGetInfo( &free_byte, &total_byte ) ;

		if ( cudaSuccess != cuda_status ){
			printf("Error: cudaMemGetInfo fails, %s \n", cudaGetErrorString(cuda_status) );
			exit(1);
		}

		double free_db = (double)free_byte ;
		double total_db = (double)total_byte ;
		double used_db = total_db - free_db ;

		int rank = Parallel_Manager::info().get_total_mpi_rank();
//		std::cout << "rank : " << rank <<", ";
//		printf("GPU memory usage: used = %f, free = %f MB, total = %f MB\n",
//				used_db/1024.0/1024.0, free_db/1024.0/1024.0, total_db/1024.0/1024.0);

            cudaMalloc((void**)& d_result, sizeof(double)*A_numVecs*numVecs);
            std::vector<double* > address_A;
            MyA->get_address(address_A);
            nvtxRangePushA("Computation");
            timer->start("MvTransMvDGEMM");

		cuda_status = cudaMemGetInfo( &free_byte, &total_byte ) ;

		if ( cudaSuccess != cuda_status ){
			printf("Error: cudaMemGetInfo fails, %s \n", cudaGetErrorString(cuda_status) );
			exit(1);
		}

		free_db = (double)free_byte ;
		total_db = (double)total_byte ;
		used_db = total_db - free_db ;

//		rank = Parallel_Manager::info().get_total_mpi_rank();
//		std::cout << "rank : " << rank <<", ";
//		printf("GPU memory usage: used = %f, free = %f MB, total = %f MB\n",
//				used_db/1024.0/1024.0, free_db/1024.0/1024.0, total_db/1024.0/1024.0);

            check = cublasDgemm(Global_handle, CUBLAS_OP_T, CUBLAS_OP_N, A_numVecs, numVecs, length, &alpha, address_A[0], length, d_data_[0], length, &beta, d_result, A_numVecs);
            cudaDeviceSynchronize();
            if(check!=CUBLAS_STATUS_SUCCESS){
                cout << "MvTransMv error: " << cublasGetErrorString(check) << endl;
            }

            timer->end("MvTransMvDGEMM");
            nvtxRangePop();
            timer->start("MvTransMvComm");
            cudaMemcpy(h_result, d_result, sizeof(double)*A_numVecs*numVecs, cudaMemcpyDeviceToHost);
            timer->end("MvTransMvComm");
            double* total_h_result = new double[A_numVecs*numVecs];
            nvtxRangePushA(" mvtransmv HtoH communication");
            Parallel_Util::all_sum(h_result, total_h_result, A_numVecs*numVecs);
            nvtxRangePop();

            timer->start("MvTransMvRearrange");
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for collapse(2)
            #endif 
            for (int w = 0 ; w < numVecs ; w++) {
                for (int v = 0 ; v < A_numVecs ; v++) {
                    B(v, w) = total_h_result[w*A_numVecs+v]; //shchoi
//                    B(v, w) = total_h_result[w*A_numVecs+v]*alpha;
                }
            }
            timer->end("MvTransMvRearrange");
            nvtxRangePushA("deallocation");
            delete [] h_result;
            delete [] total_h_result;
            cudaFree(d_result);
            nvtxRangePop();
#ifdef HAVE_ANASAZI_EXPERIMENTAL
    } else {
        if(gpu_enable){
            cout <<"This part is not complete ... jaechang " << endl;   
            exit(-1);
        }
    }
#endif
    nvtxRangePop();
    timer->end("MvTransMv");

//    Verbose::single() << "MvTransMvSize"<<A_numVecs <<"\t"<<  numVecs<< "\t" <<length<<std::endl;
//    timer->print(Verbose::single());
}




template <class ScalarType>
void MultiVec<ScalarType>::MvNorm ( std::vector<typename Teuchos::ScalarTraits<ScalarType>::magnitudeType> &normvec ) const 
{

    cout << "ScalarType is not double....MvNorm" << endl;
    exit(-1);
}
template <>
void MultiVec<double>::MvNorm ( std::vector<typename Teuchos::ScalarTraits<double>::magnitudeType> &normvec ) const 
{
    timer->start("MvNorm");
    double* h_norm_list = new double[numVecs];
    assert (numVecs <= (int)normvec.size());
    double* d_norm_list;
    for(int i=0; i<numVecs; i++){
        cublasStatus_t  check = cublasDnrm2(Global_handle, length, d_data_[i], 1, &h_norm_list[i]);
        if(check!=CUBLAS_STATUS_SUCCESS){
            cout << "MvNorm error   " << i << endl;
        }

    }
    cudaDeviceSynchronize(); //shchoi

    double* total_h_norm_list = new double[numVecs];
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif 
    for(int i=0; i<numVecs; i++)
        h_norm_list[i] = h_norm_list[i]*h_norm_list[i];
    Parallel_Util::group_sum(h_norm_list, total_h_norm_list, numVecs);
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif 
    for(int i=0; i<numVecs; i++)
        total_h_norm_list[i] = std::sqrt(total_h_norm_list[i]);
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif 
    for(int i=0; i<numVecs; i++)
        normvec[i] = total_h_norm_list[i];    
    delete [] h_norm_list;
    delete [] total_h_norm_list;
    timer->end("MvNorm");
    
}



template <class ScalarType>
void MultiVec<ScalarType>::SetBlock (const CUDA::MultiVec<ScalarType>& A, 
        const std::vector<int> &index)
{
    //Verbose::single() << "SetBlock " << std::endl;
    timer->start("SetBlock");
    MultiVec* MyA;
    MyA = dynamic_cast<MultiVec*>(&const_cast<CUDA::MultiVec<ScalarType> &>(A)); 

    std::vector<ScalarType* > address_A;
    MyA->get_address(address_A);
    cudaMemcpy(d_data_[index[0]],address_A[0] , sizeof(ScalarType)*length*index.size(),cudaMemcpyDeviceToDevice);
    timer->end("SetBlock");
}


// Scale the i-th vector by alpha[i]
    template <class ScalarType>
void MultiVec<ScalarType>::MvScale( const std::vector<ScalarType>& alpha )
{
    std::cout << "Type is not double " << std::endl;
    exit(-1);
}
template<> void MultiVec<double>::MvScale( const std::vector<double>& alpha )
{

    //Verbose::single() << "MvScale " << std::endl;
    for(int i=0; i<numVecs; i++){
        cublasStatus_t check = cublasDscal(Global_handle, length, &alpha[i], d_data_[i], 1); 
        if(check!=CUBLAS_STATUS_SUCCESS){
            printf("error   MvScale2\n");
        }

    }
    cudaDeviceSynchronize(); //shchoi

}


// Fill the vectors in *this with random numbers.
    template <class ScalarType>
void  MultiVec<ScalarType>::MvRandom ()
{
    cout << "ScalarType is not double ....MvRandom" << endl;
    exit(-1);
}
template <>
void  MultiVec<double>::MvRandom ()
{
    //Verbose::single() << "Random " << std::endl;
    timer->start("MvRandom");
    curandGenerator_t prng;
    curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT);
    curandSetPseudoRandomGeneratorSeed(prng, (unsigned long long) clock());
    for(int i=0; i<numVecs; i++){
        curandGenerateUniformDouble(prng, d_data_[i], length);
    }
    curandDestroyGenerator(prng);
    timer->end("MvRandom");
}

// Replace each element of the vectors in *this with alpha.
    template <class ScalarType>
void  MultiVec<ScalarType>::MvInit (ScalarType alpha)
{
    //Verbose::single() << "MvInit" << std::endl;
    timer->start("MvInit");
    for (int v = 0 ; v < numVecs ; ++v) 
        cudaMemset(d_data_[v], alpha, sizeof(ScalarType)*length);
    cudaDeviceSynchronize();
    timer->end("MvInit");
}

template <class ScalarType>
void MultiVec<ScalarType>::MvPrint (std::ostream &os) const
{
    os << "Object MultiVec" << std::endl;
    os << "Number of rows = " << length << std::endl;
    os << "Number of vecs = " << numVecs << std::endl;

    for (int i = 0 ; i < length ; ++i)
    {
        for (int v = 0 ; v < numVecs ; ++v)
            os << (*this)(i, v) << " ";
        os << std::endl;
    }
}

template <class ScalarType>
inline ScalarType& MultiVec<ScalarType>::operator()(const int i, const int j)
{
    if (j < 0 || j >= numVecs) throw(-1);
    if (i < 0 || i >= length) throw(-2);
    return(data_[j][i]);
}

template <class ScalarType>
inline const ScalarType& MultiVec<ScalarType>::operator()(const int i, const int j) const
{
    if (j < 0 || j >= numVecs) throw(-1);
    if (i < 0 || i >= length) throw(-2);
    return(data_[j][i]);
}

template <class ScalarType>
ScalarType* MultiVec<ScalarType>::operator[](int v)
{
    //cout << "operator[]" << endl;
    if (v < 0 || v >= numVecs) throw(-1);
    return(d_data_[v]);
}

template <class ScalarType>
ScalarType* MultiVec<ScalarType>::operator[](int v) const
{
    //cout << "operator[] const" << endl;
    return(d_data_[v]);
}
    template <class ScalarType>
void MultiVec<ScalarType>::check()
{
    if (length <= 0)
        throw("Length must be positive");

    if (numVecs <= 0)
        throw("Number of vectors must be positive");
}

/*! \relates MultiVec
  Output stream operator for handling the printing of MultiVec.
  */
    template <typename ScalarType>
std::ostream& operator<<(std::ostream& os, const MultiVec<ScalarType>& Obj)
{
    Obj.MvPrint(os);
    return os;
}
/*
template <class ScalarType>
void MultiVec<ScalarType>::Allocate_Gpu_Memory(){
    if(is_data_in_gpu==false and gpu_enable==true){
        is_data_in_gpu = true;
        if(gpu_enable){
            d_data_.resize(numVecs);
            for (int v = 0 ; v < numVecs ; ++v){
                cudaMalloc((void**)&d_data_[v], sizeof(ScalarType)*length);
            }
        }
        host_to_device();
    }
}
*/


//! Returns a clone copy of specified vectors.
template <class ScalarType>
MultiVec<ScalarType>* MultiVec<ScalarType>::CloneCopy(const std::vector< int > &index) const
{
    timer->start("CloneCopy");
    int size = index.size();
    MultiVec* tmp = new MultiVec<ScalarType>(length, size);
        std::vector<ScalarType*> address_tmp;
        tmp->get_address(address_tmp);
        cudaMemcpy(address_tmp[0],d_data_[index[0]] , sizeof(ScalarType)*length*index.size(),cudaMemcpyDeviceToDevice);
        /*
        for(int i=0; i<index.size(); i++){
            //cudaMemcpyAsync(address_tmp[i], d_data_[index[i]], length, cudaMemcpyDeviceToDevice, Global_stream[i%NStream]);
            cudaMemcpy(address_tmp[i], d_data_[index[i]], length, cudaMemcpyDeviceToDevice);
        }
        cudaDeviceSynchronize();
        */

//        tmp->const_copy_to_continuous_d_data();
    
    timer->end("CloneCopy");
    return(tmp);
}
//! Returns a clone of the current vector.
template <class ScalarType>
MultiVec<ScalarType>* MultiVec<ScalarType>::Clone(const int numVecs) const
{
    //cout << "Clone" << endl;
    // FIXME
    MultiVec* tmp = new MultiVec(length, numVecs);

    //   for (int v = 0 ; v < numVecs ; ++v)
    //         for (int i = 0 ; i < length ; ++i)
    //           (*tmp)(i, v) = (*this)(i, v);

    return(tmp);
}

template <class ScalarType>
void MultiVec<ScalarType>::copy_to_continuous_d_data()
{
    /*
    if(true){
        for(int i=0; i<numVecs; i++){
            cudaMemcpyAsync(&continuous_d_data[i*length], d_data_[i], sizeof(ScalarType)*length, cudaMemcpyDeviceToDevice, Global_stream[i%NStream]);
        }
    }
    is_continuous_data_copied=true;
    cudaDeviceSynchronize();
    Parallel_Manager::info().all_barrier();
    */
    return ;
}


template <class ScalarType>
void MultiVec<ScalarType>::const_copy_to_continuous_d_data() const
{
    /*
    if(true){
        for(int i=0; i<numVecs; i++){
            cudaMemcpyAsync(&continuous_d_data[i*length], d_data_[i], sizeof(ScalarType)*length, cudaMemcpyDeviceToDevice, Global_stream[i%NStream]);
        }
    }
    is_continuous_data_copied=true;
    cudaDeviceSynchronize();
    Parallel_Manager::info().all_barrier();
    */
    return ;
}

template <class ScalarType>
void MultiVec<ScalarType>::copy_to_d_data()
{
    /*
    for(int i=0; i<numVecs; i++){
        cudaMemcpyAsync(d_data_[i], &continuous_d_data[i*length], sizeof(ScalarType)*length, cudaMemcpyDeviceToDevice, Global_stream[i%NStream]);
    }

    cudaDeviceSynchronize();
    Parallel_Manager::info().all_barrier();
    */
    return ;
}


template <class ScalarType>
void MultiVec<ScalarType>::const_copy_to_d_data() const
{
    /*
    for(int i=0; i<numVecs; i++){
        cudaMemcpyAsync(d_data_[i], &continuous_d_data[i*length], sizeof(ScalarType)*length, cudaMemcpyDeviceToDevice, Global_stream[i%NStream]);
    }
    cudaDeviceSynchronize();
    Parallel_Manager::info().all_barrier();
    */
    return ;
}

template <class ScalarType>
bool MultiVec<ScalarType>::check_ownership() const{
    for(int i=0; i<numVecs; i++){
        if(!ownership_[i])
            return false;
    }
    return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////below part is not used during diagonalization of gpu////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 

// Returns a clone of the corrent multi-vector.
template <class ScalarType>
MultiVec<ScalarType>* MultiVec<ScalarType>::CloneCopy() const
{
    //cout << "CloneCopy" << endl;
    return(new MultiVec(*this));
}



template <class ScalarType>
void MultiVec<ScalarType>::MvDot (const CUDA::MultiVec<ScalarType>& A, std::vector<ScalarType> &b
#ifdef HAVE_ANASAZI_EXPERIMENTAL
        , CUDA::ConjType conj
#endif
        ) const
{
//    cout << "MvDot" << endl;
    MultiVec* MyA;
    MyA = dynamic_cast<MultiVec*>(&const_cast<CUDA::MultiVec<ScalarType> &>(A)); 
    assert (MyA != 0);

    assert (numVecs <= (int)b.size());
    assert (numVecs == A.GetNumberVecs());
    assert (length == A.GetVecLength());

#ifdef HAVE_ANASAZI_EXPERIMENTAL
    if (conj == CUDA::CONJ) {
#endif
#pragma omp parallel for
        for (int v = 0 ; v < numVecs ; ++v) {
            ScalarType value = 0.0;
            for (int i = 0 ; i < length ; ++i) {
                value += (*this)(i, v) * Teuchos::ScalarTraits<ScalarType>::conjugate((*MyA)(i, v));
            }
            b[v] = value;
        }
#ifdef HAVE_ANASAZI_EXPERIMENTAL
    } else {
#pragma omp parallel for
        for (int v = 0 ; v < numVecs ; ++v) {
            ScalarType value = 0.0;
            for (int i = 0 ; i < length ; ++i) {
                value += (*this)(i, v) * (*MyA)(i, v);
            }
            b[v] = value;
        }
    }
#endif
}  


// Scale the vectors by alpha
    template <class ScalarType>
void MultiVec<ScalarType>::MvScale( ScalarType alpha )
{
    //cout << "MvScale" << endl;
#pragma omp parallel for
    for (int v = 0 ; v < numVecs ; ++v) {
        for (int i = 0 ; i < length ; ++i) {
            (*this)(i, v) *= alpha;
        }
    }
}

// constructor for non-owner object
template <class ScalarType>
MultiVec<ScalarType>::MultiVec(const int _length, std::vector<ScalarType*> pointer, bool gpu_enable) :
    length(_length),
    numVecs(pointer.size())
{
    //cout << "constructor 2" << endl;
    this->gpu_enable = gpu_enable;
    check();

    data_.resize(pointer.size());
    ownership_.resize(numVecs);

    for (int v = 0 ; v < numVecs ; ++v)
    {
        data_[v] = pointer[v];
        ownership_[v] = false;
    }
    if(gpu_enable){
        is_data_in_gpu = true;
        d_data_.resize(numVecs);
        for (int v = 0 ; v < numVecs ; ++v){
            cudaMalloc((void**)&d_data_[v], sizeof(ScalarType)*_length);
        }

        //host_to_device();    
    }
    //    MvInit(0.0);
}

//! Copy constructor, performs a deep copy.
template <class ScalarType>
MultiVec<ScalarType>::MultiVec(const MultiVec& rhs, bool gpu_enable) :
    length(rhs.GetVecLength()),
    numVecs(rhs.numVecs)
{
    this->gpu_enable = gpu_enable;
    //cout << "loneViewNonConst
    check();

    data_.resize(numVecs);
    ownership_.resize(numVecs);

    for (int v = 0 ; v < numVecs ; ++v)
    {
        data_[v] = new ScalarType[length]();
        ownership_[v] = true;
    }

    for (int v = 0 ; v < numVecs ; ++v)
    {
        for (int i = 0 ; i < length ; ++i)
            (*this)(i, v) = rhs(i, v);
    }
}

template <class ScalarType>
const double* MultiVec<ScalarType>::view(const int iVec) const
{
    //cout << "view" << endl;
    return (const double*) data_[iVec];
} 

// Copy the vectors in A to a set of vectors in *this. The numvecs vectors in 
// A are copied to a subset of vectors in *this indicated by the indices given 
// in index.
// FIXME: not so clear what the size of A and index.size() are...
//
    template <class ScalarType>
void MultiVec<ScalarType>::set(const int iVec, const int size, const int* indices, const ScalarType* values)
{
    cout << "set" << endl;
#pragma omp parallel for
    for(int i=0; i<size; i++){
        //this->values[iVec][indices[i]] = values[i];
        data_[iVec][indices[i]]=values[i];
    }
    //host_to_device();
}
 */
 
 
//template class MultiVec<double>;
//template class MultiVec<float>;
//template class FDOperator< std::complex<double> >;
}
#endif

