#pragma once
#include "../../State/Dynamic_Accessor.hpp"
#include "Convergence.hpp"

class Convergence_Energy: public Convergence{
    public:
        virtual bool converge(
            Teuchos::RCP<const Basis> mesh,
            Teuchos::Array< Teuchos::RCP<Scf_State> > states
        );
        Convergence_Energy(double tolerance);
    protected:
};
