#include "Convergence_Density.hpp"

using Teuchos::RCP;

Convergence_Density::Convergence_Density(double tolerance,int norm){
    this->tolerance = tolerance;
    this->convergence_type = "Density";
    this->norm = norm;
}

bool Convergence_Density::converge(RCP<const Basis> mesh, Teuchos::Array<RCP<Scf_State> > states){
    //bool return_val=true;
    if (states.size()<=1){
        return false;
    }
    auto last_state1 = states[states.size()-1];
    auto last_state2 = states[states.size()-2];
    double max_difference=0.0;
    for(int i_spin=0; i_spin < last_state1->occupations.size() ; i_spin++ ){
        if(states[states.size()-1]->occupations[i_spin]->get_total_occupation()==0){
            continue;
            //max_difference=std::max(max_difference,0.0);
        }
        else{
            RCP<Epetra_Vector> val1 = last_state1->density[i_spin];
            RCP<Epetra_Vector> val2 = last_state2->density[i_spin];
            
            RCP<Epetra_Vector> dif_vector = Teuchos::rcp( new Epetra_Vector ( *val1 ) );
            dif_vector->Update(-1.0,*val2,1.0);
            //std::cout << *dif_vector<<std::endl;
            double* norms = new double[dif_vector->NumVectors()];
            if(norm==-1){
                dif_vector->NormInf(norms);
            }
            else if (norm ==2){
                dif_vector->Norm2(norms);
            }
            else{
                dif_vector->Norm1(norms);
            }
            double tmp_max = 0.0;
            for (int i=0; i<dif_vector->NumVectors();i++){
//            Verbose::single() << "Linear_Mixing_Density \t"<< norms[i] <<std::endl;
                tmp_max = std::max(tmp_max,norms[i]);
            }
            tmp_max *= mesh->get_scaling()[0] * mesh->get_scaling()[1] * mesh->get_scaling()[2] / last_state1->occupations[i_spin]->get_total_occupation();
            max_difference=std::max(max_difference,tmp_max);
            delete[] norms;
        }
    }
    Verbose::single() << "Convergence:: convergence type: " << convergence_type << std::endl; 
    Verbose::single(Verbose::Detail) << "Convergence:: max difference / num_elec" <<std::endl;
    print(max_difference);
    //Verbose::single() << "Convergence:: current deviation (max. / num_elec): " << max_difference << std::endl; 
    if ( max_difference>tolerance){
        Verbose::single() << "Convergence:: not converged yet (tolerance = "<< tolerance <<")"  << std::endl;
        return false;
    }
    Verbose::single() << "Convergence:: converged!"  << std::endl;
    return true;
/*
    for (int i =0 ; i<convergences.size();i++){
        return_val = return_val and convergences[i]->converge(states);
    }
    return return_val;
*/
}
