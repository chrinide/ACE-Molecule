#include "Occupation_Zero_Temp.hpp"
/*
#include "Occupation.hpp"
class Occupation_Zero_Temp: public Occupation{
    public:
        Occupation_Zero_Temp(double default_occupation=2.0);
        void set_eigenvalues(vector<double > eigenvalues,double number_of_occupied_states);
    protected:
};
*/


Occupation_Zero_Temp::Occupation_Zero_Temp(bool is_polar, double spin_multiplicity, int spin_index, double num_elec,int size){
    this->is_polar=is_polar;
    this->spin_index=spin_index;
    this->num_elec=num_elec;
    this->temp = 0.0;
    this->spin_multiplicity = spin_multiplicity;
    this->size = size;
    type = "Zero_Temp";
    occupation.clear();
    int int_num_elec = floor(num_elec+0.5);

    if (is_polar==false){
        for (int i=0; i<int_num_elec/2; i++){
            occupation.push_back(2.0);
        }
        if(int_num_elec%2==1){
            occupation.push_back(1.0); // restricted open shell
        }
    }
    else{ // polarized

        int diff_elec = floor( (spin_multiplicity-1.0)+0.5 );
        int_num_elec -=diff_elec;
        for (int i=0; i<int_num_elec/2; i++){
            occupation.push_back(1.0);
        }
        if (spin_index==0){
            for (int i=0; i<diff_elec; i++){
                occupation.push_back(1.0);
            }
        }
        else{
            for (int i=0; i<diff_elec; i++){
                occupation.push_back(0.0);
            }
        }
    }
    if (size==0){
        this->size = occupation.size();
    }

    while(occupation.size()<size){
        occupation.push_back(0.0);
    }
}
Occupation* Occupation_Zero_Temp::clone() const {
    return new Occupation_Zero_Temp(is_polar, spin_multiplicity, spin_index, num_elec, size);
}
