#pragma once
#include "Occupation.hpp"
#include "Occupation_Zero_Temp.hpp"
#include "Teuchos_RCP.hpp"

class Occupation_Fermi: public Occupation{
    public:
        double operator[](int index) const;
        Occupation_Fermi(bool is_polar, double spin_multiplicity, int spin_index, double num_elec, double temp,int size);
        ~Occupation_Fermi(){};
        void set_eigenvalues(std::vector<double> eigenvalues);
        Occupation* clone() const;
        double get_total_occupation() const;
    private:
        Teuchos::RCP<Occupation_Zero_Temp> basic_occupation;
        double fermi_energy;

};
