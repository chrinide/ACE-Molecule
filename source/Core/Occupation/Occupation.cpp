#include "Occupation.hpp"
#include <cmath>
#include "../../Util/String_Util.hpp"

using std::ostream;
/*
ostream &operator << (ostream &c, Occupation &occupation) {
    c<< "====== Occupations"<< occupation.type<<" ========" <<std::endl;
    for(int i=0; i<occupation.get_size(); i++){
        //Verbose::all() << std::setw(7) << occupation.operator[](i);
        c << i<<": " <<std::setw(7) << std::setprecision(4) << occupation.operator[](i) <<std::endl;
    }
    c << "============================" <<std::endl;


    return c;
}

ostream &operator << (ostream &c, Occupation* poccupation)  {
    //Verbose::all() << *poccupation;
    c << *poccupation;

    return c;
}
*/
ostream &operator << (ostream &c, const Occupation &occupation) {
    int max_occ = occupation.get_size();
    const int entry_per_line = 5;
    int lines = ceil(static_cast<float>(max_occ)/entry_per_line);
    int width = String_Util::to_string(max_occ).size();
    c << "====== Occupations (" << std::setw(10) << occupation.type <<") ========" <<std::endl;
    c << " Occupations:" << std::endl;
    /*
    for(int i=0; i<occupation.get_size(); ++i){
        //Verbose::all() << std::setw(7) << occupation.operator[](i);
        c << i <<": " <<std::setw(7) << std::setprecision(4) << occupation.operator[](i) <<std::endl;
    }
    */
    for(int i = 0; i < lines; ++i){
        const int num_entry = std::min(entry_per_line, max_occ-entry_per_line*i);
        c << std::setw(width) << entry_per_line*i+1 << " - " << std::setw(width) << entry_per_line*i+num_entry << ": ";
        for(int j = 0; j < num_entry; ++j){
            c << std::setw(7) << std::setprecision(4) << occupation.operator[](entry_per_line*i+j) << "  ";
        }
        c << std::endl;
    }

    auto eigenvalues = occupation.get_eigenvalues();
    if(eigenvalues.size() > 0){
        max_occ = eigenvalues.size();
        lines = max_occ/entry_per_line+1;
        lines = ceil(static_cast<float>(max_occ)/entry_per_line);
        width = String_Util::to_string(max_occ).size();
        c << " Eigenvalues:" << std::endl;
        /*
        for(int i=0; i<eigenvalues.size(); ++i){
            c << i << ": " << std::setw(7) << std::setprecision(4) << eigenvalues[i] <<std::endl;
        }
        */
        for(int i = 0; i < lines; ++i){
            const int num_entry = std::min(entry_per_line, max_occ-entry_per_line*i);
            c << std::setw(width) << entry_per_line*i+1 << " - " << std::setw(width) << entry_per_line*i+num_entry << ": ";
            for(int j = 0; j < num_entry; ++j){
                c << std::setw(7) << std::setprecision(4) << eigenvalues[entry_per_line*i+j] << "  ";
            }
            c << std::endl;
        }
    }
    c << "=======================================" <<std::endl;

    return c;
}

ostream &operator << (ostream &c, const Occupation* &poccupation)  {
    //Verbose::all() << *poccupation;
    c << *poccupation;

    return c;
}


double Occupation::operator[](int index) const {
    if (occupation.size()<=index){
        return 0.0;
    }
    return occupation[index];
}


Occupation& Occupation::operator=(const Occupation& original_occupation){
    this->set_eigenvalues(original_occupation.eigenvalues);
    this->size = original_occupation.size;
    this->temp = original_occupation.temp;

    occupation.clear();
    for(int i =0;i<original_occupation.get_size(); i++){
        occupation.push_back(original_occupation[i]);
    }
    return *this;
}

int Occupation::get_size()const {
    return size;
}

void Occupation::print(std::vector<double > eigenvalues) const {
    Verbose::single() << "eigenvalue \t occupation"<<std::endl;
    for (int i = 0; i < occupation.size(); i++){
        Verbose::single() << std::real(eigenvalues[i]) << "\t" << this->operator[](i) <<std::endl;
    }
    return;
}

double Occupation::get_total_occupation()const {
    double value = 0.0;

    int size = occupation.size();
    for(int i=0; i<size; i++){
        value += occupation[i];
    }

    return value;
}

void Occupation::resize(int new_size){
    int old_size = this -> occupation.size();

    if( old_size < new_size ){
        this -> occupation.resize(new_size,0.0);
        this -> size = new_size;
    } else if( old_size > new_size ) {
        double removing_occupation = 0.0;
        for(int i = new_size; i < old_size; ++i){
            removing_occupation += this -> occupation[i];
        }
        if( removing_occupation > 0.0 ){
            Verbose::single() << "Cannot remove positive occupation" << std::endl;
        } else {
            this -> occupation.resize(new_size);
            this -> size = new_size;
        }
    }
}

/*
void Occupation::compute(vector<double> input_occupation){
    occupation.clear();

    for(int i=0; i<input_occupation.size(); i++){
        occupation.push_back(input_occupation[i]);
    }

    return;
}
*/
