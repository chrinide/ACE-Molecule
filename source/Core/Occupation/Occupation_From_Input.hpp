#pragma once
#include "Occupation.hpp"

class Occupation_From_Input: public Occupation{
    public:
        Occupation_From_Input();
        ~Occupation_From_Input(){};

        Occupation_From_Input(std::vector<double> occ, std::vector<double> eigval = std::vector<double>());
        void set_eigenvalues(std::vector<double> eigenvalues);
        Occupation* clone() const;
};
