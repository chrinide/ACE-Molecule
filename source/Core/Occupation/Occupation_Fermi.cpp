#include "Occupation_Fermi.hpp"
#include "../../Util/Verbose.hpp"

Occupation_Fermi::Occupation_Fermi(bool is_polar, double spin_multiplicity, int spin_index, double num_elec, double temp,int size){
    this->is_polar=is_polar;
    this->spin_index=spin_index;
    this->num_elec=num_elec;
    this->temp = temp;
    this->size = size;
    this->spin_multiplicity = spin_multiplicity;

    type = "Fermi";
    basic_occupation = Teuchos::rcp(new Occupation_Zero_Temp(is_polar,spin_multiplicity,spin_index,num_elec,size));
    occupation.clear();
}

void Occupation_Fermi::set_eigenvalues(std::vector<double> eigenvalues){
    //
    assert(size == eigenvalues.size());

    if (eigenvalues.size()==0){
        occupation.clear();
        return;
    }
    if(is_polar == true){
        Verbose::all() <<  "fermi occupation is not yet implemented for polarized systems" <<std::endl;
        exit(-1);
    }
    double max_occupation = is_polar? 1.0: 2.0;
    double sum=0.0;
    const double boltzman_factor = 3.1668114*1e-6;

    double total_occupation = basic_occupation->get_total_occupation();
    const int max_iter = 200;
    int i=0;
    double min_val = eigenvalues[0];
    double max_val = eigenvalues[size-1];

    for (i=0; i<max_iter; i++){
        fermi_energy = (min_val+max_val)/2;
        sum =0.0;

        occupation.clear();
        for (int i =0; i<size; i++){
            double e = (eigenvalues[i]-fermi_energy)/boltzman_factor;
            occupation.push_back( max_occupation/(exp(e/temp)+1));
            sum+=occupation[i];
        }
        if (std::fabs(total_occupation-sum) < 0.00001 ){
            break;  // converged
        }
        else if (sum < total_occupation){
                min_val = fermi_energy; // up-shift fermi-level
        }
        else{
                max_val = fermi_energy; // down-shift fermi-level
        }

    }
    if (i==max_iter){
        Verbose::all() << "Occupation_Fermi::problem on setting fermi-level" <<std::endl;
        for (i =0; i< eigenvalues.size(); i++){
            Verbose::all() << i << "th eigen/occ. : " << eigenvalues[i] << "/" <<occupation[i]  <<std::endl;
        }
        exit(-1);
    }
    this->eigenvalues.clear();
    this->eigenvalues.assign(eigenvalues.begin(), eigenvalues.end());
    Verbose::single() << "Occupation_Fermi:: fermi energy   "  << fermi_energy << " Hartree ( iteration: " << i << ") "<<std::endl;
    return ;
}
double Occupation_Fermi::operator[](int index) const {
    if (occupation.size()==0){
        return basic_occupation->operator[](index);
    }
    return Occupation::operator[](index);
}

Occupation* Occupation_Fermi::clone() const{
    auto return_val =  new Occupation_Fermi( is_polar,spin_multiplicity, spin_index, num_elec, temp, size);
    return_val->set_eigenvalues(this->eigenvalues);
    return return_val;
}

double Occupation_Fermi::get_total_occupation() const{
    return basic_occupation->get_total_occupation();
}
