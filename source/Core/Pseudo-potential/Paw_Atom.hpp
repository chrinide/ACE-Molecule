#pragma once
#include <vector>
#include <string>

#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"
#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"

#include "../../Basis/Basis.hpp"
#include "../Occupation/Occupation.hpp"
#include "../Pseudo-potential/Paw_Species.hpp"
#include "../Pseudo-potential/Paw_XC.hpp"

/**
 * @brief Atomic PAW calculations for each atoms. This part concerns position-dependent part, mostly.
 * @details For detailed descriptions about PAW method, see arXiv:0910.1921.
 * @author Sungwoo Kang
 * @date 2015
 **/
class Paw_Atom{
    public:
        /**
         * @brief Constructor. Get PAW element informations from Paw_Species class.
         * @param[in] paw_species Paw_Species class that contains informations about the element.
         * @param[in] atom_center Center of atom.
         * @param[in] spin_size Spin size. 1 for unpolarized, 2 for polarized.
         * @param[in] mesh Basis information.
         * @param[in] fine_mesh Fine mesh information that some quantities, usually hartree potential, will be computed on.
         * @param[in] addi_params Additional parameters, like angular momentum cutoff of supersampling information.
         **/
        Paw_Atom(
            Teuchos::RCP<Paw_Species> paw_species,
            std::array<double,3> atom_center,
            int spin_size,
            Teuchos::RCP<const Basis> mesh,
            Teuchos::RCP<const Basis> fine_mesh,
            Teuchos::RCP<Teuchos::ParameterList> addi_params
        );
        ~Paw_Atom();

        /**
         * @brief Return Paw_Species class.
         * @return Paw_Species class, that are currently holding.
         **/
        Teuchos::RCP<Paw_Species> get_paw_species();

        // Density matrix
        // wavefucntion = Scf::get_orbitals();
        /**
         * @brief Calculates PAW atom-centered density matrix.
         * @param[in] occupations Occupations for the density matrix calculations.
         * @param[in] wavefunctions Orbitals for the density matrix calculations.
         * @return PAW atom-centered density matrix. Index: [spin index](matrix row, column).
         **/
        Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > get_one_center_density_matrix(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > wavefunctions
        );
        /**
         * @brief Calculates inner product between PAW projector function and orbitals.
         * @param[in] occupations Occupations for the density matrix calculations.
         * @param[in] orbitals Orbitals for the density matrix calculations.
         * @return Inner product between PAW projector function and orbitals. Index: [spin index][orbital index][PAW partial wave index].
         **/
        std::vector< std::vector< std::vector<double> > > projector_dot_orbitals(
                Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals
        );

        /**
         * @brief Returns initial PAW atom-centered density matrix, inferred from the dataset.
         * @param[in] charge Charge to change initial occupation.
         * @return PAW atom-centered density matrix. Index: [spin index](matrix row, column).
         **/
        Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > get_one_center_initial_density_matrix(double charge = 0.0);

        /**
         * @brief Prints dot product between PAW projector function and orbitals.
         * @param[in] occupations Occupations for calculations.
         * @param[in] orbitals Orbitals for calculations.
         **/
        void print_proj_orbital_dot(
                Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
                Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitals
        );

        /**
         * @brief Calculates PAW energy correction.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @param[in,out] hartree_energy Hartree energy.
         * @param[in,out] int_n_vxc \f$ \int n \times v_{xc} \f$.
         * @param[in,out] x_energy Exchange energy.
         * @param[in,out] c_energy Correlation energy.
         * @param[in,out] kinetic_energy Kinetic energy.
         * @param[in,out] zero_energy Zero correction energy.
         * @param[in,out] external_energy External energy.
         * @note Currently, int_n_vxc is not updated since it is calculation-intensive and does not affect total energy.
         **/
        void get_energy_correction(
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix,
            double &hartree_energy,
            double &int_n_vxc,
            double &x_energy,
            double &c_energy,
            std::vector<double> &kinetic_energies,
            double &zero_energy,
            double &external_energy
        );

        // Hartree_potential = Scf::get_hartree_potential()
        /**
         * @brief Calculates PAW hamiltonian correction matrix.
         * @details This is a coefficient for outer product of projector functions.
         * @param[in] Hartree_potential Hartree potential on the fine mesh.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @return PAW hamiltonian correction matrix. Index: [spin index][matrix row][matrix column].
         **/
        std::vector< std::vector< std::vector<double> > > get_Hamiltonian_correction_matrix(
            Teuchos::RCP<Epetra_Vector> Hartree_potential,
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix
        );

        // Density correction
        /**
         * @brief Returns valence density correction that will turn pseudo valence density into all-electron valence density.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @return Density correction. Always on the ordinary meth.
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_density_correction( Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix );

        /**
         * @brief Returns all-electron orbitals from pseudo orbitals.
         * @param[in] ps_orbitals Smooth psuedo orbitals.
         * @param[out] ae_orbitals Ouput all-electron orbitals.
         **/
        void get_orbital_correction(
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > ps_orbitals,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > &ae_orbitals
        );

        // Core Density
        /**
         * @brief Returns core AE/PS density.
         * @param[in] is_ae If true, all-electron core density will be returned. If false, it will be pseudo core density.
         * @return AE/PS core density. Always on the ordinary mesh (this -> mesh).
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_core_density_vector( bool is_ae );

        /**
         * @brief Returns gradient of AE/PS core density.
         * @param[in] is_ae If true, all-electron core density gradient will be returned. If false, it will be pseudo core density gradient.
         * @return AE/PS core density gradient. Index: [spin index](axis; x:0,y:1,z:2). Always on the ordinary mesh (this -> mesh).
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > get_core_density_grad_vector( bool is_ae );
        // Related with zero potential
        /**
         * @brief Returns zero potential.
         * @return Zero potential. Always on the ordinary mesh (this -> mesh).
         **/
        Teuchos::RCP<Epetra_Vector> get_zero_potential();
        /**
         * @brief Returns zero potential gradient.
         * @param [inis_fine If true, return value will be on fine mesh. If false, it will be on ordinary mesh (this -> mesh).
         * @return Gradient of zero potential.
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_zero_potential_gradient(bool is_fine = false);

        // Compensation Charge
        /**
         * @brief Returns compensation charge.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @param[in] is_fine If true, return value will be on fine mesh. If false, it will be on ordinary mesh (this -> mesh).
         * @return Compensation charge.
         * @note This routine uses trilinear interpolation, but it is actually undersampling points, so interpolation quality does not matter.
         **/
        Teuchos::RCP<Epetra_Vector> get_compensation_charge(
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix,
            bool is_fine = false
        );

        /**
         * @brief Returns the gradient of compensation charge.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @param[in] is_fine If true, return value will be on fine mesh. If false, it will be on ordinary mesh (this -> mesh).
         * @return Gradient of compensation charge.
         * @note This routine uses trilinear interpolation, but it is actually undersampling points, so interpolation quality does not matter.
         **/
        Teuchos::RCP<Epetra_MultiVector> get_compensation_charge_grad(
              Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix,
            bool is_fine = false
        );

        /**
         * @brief Returns analytic integration of compensation charge.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @return Analytic integration of compensation charge. \f$ \sqrt{4\pi} Q^a_{00} \f$.
         **/
        double get_total_compensation_charge(
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix
        );

        // Hartree potential
        /**
         * @brief Returns hartree potential of compensation charge.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @param[in] is_fine If true, return value will be on fine mesh. If false, it will be on ordinary mesh (this -> mesh).
         * @return Hartree potential of compensation charge.
         **/
        Teuchos::RCP<Epetra_Vector> get_compensation_charge_Hartree_potential(
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix,
            bool is_fine = false
        );

        /**
         * @brief Returns hartree potential of smooth core density.
         * @param[in] is_fine If true, return value will be on fine mesh. If false, it will be on ordinary mesh (this -> mesh).
         * @return Hartree potential of smooth core density.
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_smooth_core_potential( bool is_fine = false );

        /**
         * @brief Returns initial hartree potential estimate.
         * @details Initial hartree potential is estimated from the smooth core density, initial density (from partial waves), and compensation charge (from initial PAW atom-centered density matrix).
         * @param[in] is_fine If true, return value will be on fine mesh. If false, it will be on ordinary mesh (this -> mesh).
         * @return Initial guess of hartree potential.
         **/
        Teuchos::RCP<Epetra_Vector> get_initial_hartree_potential( bool is_fine = false );

        // Overlap matrix
        /**
         * @brief Returns overlap matrix.
         * @return Overlap matrix \f$ \sqrt{4\pi} \Delta^a_{00i_1i_2} \f$.
         **/
        Teuchos::SerialDenseMatrix<int,double> get_overlap_matrix();

        /**
         * @brief Returns projector function coefficients.
         * @param[out] proj_coeffs Projector function coefficients.
         * @param[out] proj_inds Index for projector function.
         **/
        void get_projector_coeffs_and_inds(
            std::vector< std::vector<double> > & proj_coeffs,
            std::vector< std::vector<int> > & proj_inds
        );

        /**
         * @brief Returns the gradient of the projector functions.
         * @return Projector function gradients. (Coefficients).
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > get_projector_gradients();

        /**
         * @brief Update external potential informations.
         * @details From potential v_ext, creates and internally store core and valence correction calculated from Paw_Species.
         * @param[in] v_ext External potential input.
         * @note Although the presence of this function, external potential is not supported.
         **/
        void update_external_potential(
            Teuchos::RCP<Epetra_Vector> v_ext
        );

        // Density correction
        /**
         * @param[in] is_ae If true, all-electron core density will be returned. If false, it will be pseudo core density.
         * @param[out] pw_vectors Output.
         * @param[in] is_value If true, value will be returned. If false, mesh coefficient will be returned.
         * @todo change pw_vectors to last.
         **/
        void get_atomcenter_orbitals(
            bool is_ae,
            Teuchos::RCP<Epetra_MultiVector> &pw_vectors,
            bool is_value
        );

        /**
         * @brief Returns atom-centered valence density.
         * @param[in] is_ae If true, all-electron core density will be returned. If false, it will be pseudo core density.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @param[in] omesh Output mesh. If Teuchos::null, it is ordinary mesh (this -> mesh).
         * @return Atom-centered valence density.
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > get_atomcenter_density_vector(
            bool is_ae,
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix,
            Teuchos::RCP<const Basis> omesh = Teuchos::null
        );

        // Density gradient
        /**
         * @brief Returns the gradient of atom-centered valence density.
         * @param[in] is_ae If true, all-electron core density will be returned. If false, it will be pseudo core density.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @param[in] omesh Output mesh. If Teuchos::null, it is ordinary mesh (this -> mesh).
         * @return Gradient of atom-centered valence density. Index: [spin index](axis;x:0,y:1,z:2)
         **/
        Teuchos::Array< Teuchos::RCP< Epetra_MultiVector > > get_atomcenter_density_grad_vector(
            bool is_ae,
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix,
            Teuchos::RCP<const Basis> omesh = Teuchos::null
        );

        /**
         * @brief Print the contribution of each orbitals to the PAW partial wave.
         */
        void print_orbital_occupancy(
            Teuchos::Array< Teuchos::RCP<const Occupation> > occupations,
            Teuchos::Array< Teuchos::RCP<const Epetra_MultiVector> > orbitlas
        );

    private:
        // read
        /**
         * @brief Initialize atom-centered grid for supersampling of projector function.
         * @details This constructs Basis with Grid_Atoms that has center on the atom center.
         **/
        void paw_finegrid_init();
        /**
         * @brief Pre-interpolates some values from atom-centered mesh.
         * @details Interpolated values are:
         *          hartree potential of compensation charge (fine version: this -> compensation_potential, coarse version: this -> coarse_compensation_potential),
         *          compensation charge (this -> compensation_charge, fine version only),
         *          and hartree potential of smooth core density (fine version: this -> fine_core_density_potential, this -> coarse_core_density_potential).
         *          Also, this interpolates projector function and distributes it. Supersampling is called.
         * @note See get_projector_coeffs(): Projector function supersampling.
         **/
        void paw_function_interpolation();

        // related with compensation charge
        /**
         * @brief Returns multipole expansion coefficient of compensation charge.
         * @param[in] sD_matrix PAW atom-centered density matrix.
         * @return \f$ \Delta^a \delta_{l,0} + \Delta^a_{i_1i_2} \times D^a_{i_1i_2} \f$.
         **/
        std::vector< std::vector<double> > get_compensation_charge_expansion_coeff(
            Teuchos::Array< Teuchos::SerialDenseMatrix<int,double> > &sD_matrix
        );

        // Projector functions
        /**
         * @brief Returns projector function coefficients, supersampling applied.
         * @param[in] center Atom center.
         * @param[in] omesh Output mesh. If Teuchos::null, it is ordinary mesh (this -> mesh).
         * @return Projector function coefficients, supersampling applied.
         **/
        Teuchos::RCP<Epetra_MultiVector> calculate_fine_projector_coeffs(
            std::array<double,3> center,
            Teuchos::RCP<const Basis> omesh = Teuchos::null
        );
        /**
         * @brief Returns the gradient of projector function coefficients, supersampling applied.
         * @param[in] center Atom center.
         * @param[in] omesh Output mesh. If Teuchos::null, it is ordinary mesh (this -> mesh).
         * @return Projector function coefficients, supersampling applied.
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > calculate_fine_projector_grads(
            std::array<double,3> center,
            Teuchos::RCP<const Basis> omesh = Teuchos::null
        );

        /**
         * @brief Returns compensation potential, supersampling applied.
         * @param[in] center Atom center.
         * @param[in] omesh Output mesh. If Teuchos::null, it is ordinary mesh (this -> mesh).
         * @return Compensation potential, supersampling applied.
         * @note This function gives inaccurate result. However, I think that was old bugged version. It is not revised, yet.
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > get_fine_comp_potential(
            std::array<double,3> center,
            Teuchos::RCP<const Basis> omesh = Teuchos::null
        );

        /**
         * @brief Interpolates the compensation charge to desired mesh.
         * @param[in] is_fine Requests the values on the fine mesh.
         * @param[in] lmax Maximum angular momentum value.
         * @return Interpolated compensation charge to desired mesh.
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > interpolate_compensation_charge(
                bool is_fine,
                int lmax
        );

        /**
         * @brief Interpolates the gradient of compensation charge to desired mesh.
         * @param[in] omesh Output mesh.
         * @param[in] lmax Maximum angular momentum value.
         * @return Interpolated compensation charge to desired mesh.
         **/
        std::vector< Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > > interpolate_compensation_charge_grad(
                Teuchos::RCP<const Basis> omesh,
                int lmax
        );


        /**
         * @brief Interpolates the compensation potential to desired mesh.
         * @param[in] is_fine Requests the values on the fine mesh.
         * @param[in] lmax Maximum angular momentum value.
         * @return Interpolated compensation potential to desired mesh.
         **/
        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > interpolate_compensation_potential(
                bool is_fine,
                int lmax
        );

        //Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > get_spherical_harmonics_on_grid(Teuchos::RCP<const Basis> omesh, int lmax);

        Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > retrieve_fine_compensation_potential();

        // Data storage
        Teuchos::RCP<Paw_Species> paw_species;
        int spin_size;
        std::array<double,3> position;
        std::vector<int> index_to_l;
        int lmax;

        double ext_core = 0.0;
        std::vector< std::vector<double> > ext_valence;

        //Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > projector_grads;
        //Teuchos::RCP<Epetra_MultiVector> projector_coeffs;
        std::vector< std::vector<int> > proj_inds;
        std::vector< std::vector<double> > proj_coeffs;
        std::vector< std::vector< std::vector<int> > > proj_grad_inds;
        std::vector< std::vector< std::vector<double> > > proj_grad_coeffs;

        Teuchos::Array< Teuchos::RCP< Epetra_MultiVector> > coarse_compensation_potential;
        Teuchos::Array< Teuchos::RCP< Epetra_MultiVector> > compensation_potential;
        //Teuchos::Array< Teuchos::RCP< Epetra_MultiVector> > compensation_charge;
        std::vector< Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > > compensation_charge_grad;

        //std::vector< std::vector< std::vector<double> > > fine_comp_vals;
        //std::vector< std::vector< std::vector<int> > > fine_comp_inds;

        Teuchos::RCP<Epetra_MultiVector> fine_projector_coeffs;
        //Teuchos::RCP<Epetra_Vector> coarse_core_density_potential;
        //Teuchos::RCP<Epetra_Vector> fine_core_density_potential;

        Teuchos::RCP<const Basis> mesh;
        Teuchos::RCP<const Basis> fine_mesh;

        Teuchos::RCP<Paw_XC> paw_xc_new;

        //Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > fine_Ylm;
        Teuchos::RCP<Atoms> myatoms;
        double fine_beta;
        std::string fine_filter_type;
        int fine_proj_degree;
        int fine_pot_degree;
        Teuchos::RCP<const Basis> proj_mesh;
        Teuchos::RCP<const Basis> comp_mesh;

        int store_comp_potential = 0;
        int occupancy_output = 2;
        std::string id;
        std::vector<std::string> comp_pot_fname;
};
