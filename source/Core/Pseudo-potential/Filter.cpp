#include "Filter.hpp"
#include "../../Util/ACE_Config.hpp"

#include <cmath>
#include <iostream>

#include "../../Io/Periodic_table.hpp"
#include "../../Basis/Basis_Function/Basis_Function.hpp"
#include "../../Basis/Grid_Setting/Grid_Setting.hpp"
#include "../../Util/Integration.hpp"
#include "../../Util/String_Util.hpp"
#include "../../Util/Verbose.hpp"

using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

Filter::Filter(RCP<const Basis> mesh, RCP<const Atoms> atoms, vector<double> Zvals){
    this->mesh = mesh;
    this->Zvals = Zvals;
    this->atoms = atoms;

    comp_charge_exps.clear();
}
/*
int Filter::compute_comp_charge_and_potential(){
    int NumMyElements = mesh->get_map()->NumMyElements();
    //int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
    if(comp_charge_exps.size()==0){
        return -2;
    }

    vector<std::array<double,3> > positions =atoms->get_positions();
    const double** scaled_grid = mesh->get_scaled_grid();

    int i_x,i_y,i_z;
    double x,y,z;
    double r2,scaled_r2,r_gaus;
    comp_charge = Teuchos::rcp(new Epetra_MultiVector(*mesh->get_map(), atoms->get_size())); //Initialize all values as 0
    comp_potential = Teuchos::rcp(new Epetra_MultiVector(*mesh->get_map(), atoms->get_size()));

    for(int t=0; t<atoms->get_size(); t++){
        int itype = atoms->get_atom_type(t);

        r_gaus = comp_charge_exps[itype];
//        #ifdef ACE_HAVE_OMP
//        #pragma omp parallel for private(x,y,z,r2,scaled_r2,i_x,i_y,i_z)
//        #endif
        for(int i=0; i<NumMyElements; i++){
            mesh->decompose(i, &i_x, &i_y, &i_z);
            std::cout << i << "\t" << i_x << "," << i_y << ","<<i_z <<std::endl;
            x = scaled_grid[0][i_x] - positions[t][0];
            y = scaled_grid[1][i_y] - positions[t][1];
            z = scaled_grid[2][i_z] - positions[t][2];
            r2 = x*x + y*y + z*z;
            scaled_r2 = r2/(r_gaus*r_gaus);
            comp_charge->operator()(t)->operator[](i)=Zvals[itype]*exp(-scaled_r2 )/pow(sqrt(M_PI)*r_gaus,3);
            if (r2<1.0e-15){
                comp_potential->operator()(t)->operator[](i)=Zvals[itype]*2/(sqrt(M_PI)*r_gaus);
            }
            else{
                comp_potential->operator()(t)->operator[](i)=Zvals[itype]*erf(sqrt(scaled_r2))/sqrt(r2);
            }
        }
    }

    return 0;
}
*/
int Filter::filter_local(vector<double> original_f, vector<double> r_g, vector<double> dr_g, double rcut, vector<double>& new_f, vector<double>& new_mesh){
    if (original_f.size()!=r_g.size() or original_f.size()!=dr_g.size() ){
        Verbose::all() << "Error!!!  length of original_f is not equal to length of r_g or dr_g" << std::endl;
        Verbose::all() << "size of r_g: " << r_g.size() << std::endl;
        Verbose::all() << "size of dr_g: " << dr_g.size() << std::endl;
        Verbose::all() << "size of original_f: " << original_f.size() << std::endl;
        return -1;
    }


    if(comp_charge_exps.size()==0){
        return -2;
    }

    //double cutoff_r = 2.0 * rcut_tmp;
    double cutoff_r = rcut*1.25;

    double* scaling = new double[3];

    scaling[0] = mesh->get_scaling()[0];
    scaling[1] = mesh->get_scaling()[1];
    scaling[2] = mesh->get_scaling()[2];

    double h = std::max( std::max( scaling[0],scaling[1] ),scaling[2] ) ;

    delete[] scaling;
    double max_freq = M_PI/h ;

    double q_cut = M_PI / h ;
    double dq = (M_PI / h) * 0.01;
    double dr = h * 0.01;

    /////////////// generate new R mesh  /////////////////////////
    vector<double> new_r_g;
    vector<double> new_dr_g;
    double value = 0.0;
    while(value <= cutoff_r){
        new_r_g.push_back(value);
        new_dr_g.push_back(dr);
        value+=dr;
    }

    new_mesh=new_r_g;

/*
    new_r_g = r_g;
    new_r_g.insert(new_r_g.begin(), 0.0);
    new_dr_g = dr_g;
    new_dr_g.insert(new_dr_g.begin(), new_r_g[1] - new_r_g[0]);
    double value = new_r_g[new_r_g.size()-1] + dr;
    while(value <= cutoff_r){
        new_r_g.push_back(value);
        new_dr_g.push_back(dr);
        value+=dr;
    }
    new_mesh = new_r_g;
*/

    /////////////// generate mesh on frequency Domain  /////////////////////////
    vector<double> q_i;
    value = 0.0;
    while(value<=max_freq){
        q_i.push_back(value);
        value += dq;
    }
    new_f.clear();

    ////////////// Filtering //////////////////////
    compute(h, original_f, r_g, dr_g, 0, new_r_g, new_dr_g, q_i, dq, q_cut, cutoff_r, new_f);
    return 0;
}

int Filter::filter_nonlocal(vector<double>original_f, vector<double>r_g, vector<double>dr_g, int l, double nonlocal_cutoff, vector<double>& new_f, vector<double>& new_mesh){
    if (original_f.size()!=r_g.size() or original_f.size()!=dr_g.size() ){
        Verbose::all() << "Filter nonlocal" << std::endl;
        Verbose::all() << "Error!!!  length of original_f is not equal to length of r_g or dr_g" << std::endl;
        Verbose::all() << "size of r_g: " << r_g.size() << std::endl;
        Verbose::all() << "size of dr_g: " << dr_g.size() << std::endl;
        Verbose::all() << "size of original_f: " << original_f.size() << std::endl;
        exit(-1);
    }

    double cutoff_r = nonlocal_cutoff;
    cutoff_r *= 1.5;


    double* scaling = new double[3];
    scaling[0] = mesh->get_scaling()[0];
    scaling[1] = mesh->get_scaling()[1];
    scaling[2] = mesh->get_scaling()[2];
    double h = std::max( std::max( scaling[0],scaling[1] ),scaling[2] ) ;

    delete[] scaling;

    double max_freq =  M_PI / h;

    double q_cut = M_PI / h;
    double dq = M_PI / h * 0.01;
    double dr = h * 0.01;

    /////////////// generate new R mesh  /////////////////////////
    vector<double> new_r_g;
    vector<double> new_dr_g;
    double value = 0.0;
    while(value <= cutoff_r){
        new_r_g.push_back(value);
        new_dr_g.push_back(dr);
        value+=dr;
    }
    new_mesh = new_r_g;
/*
    new_r_g = r_g;
    new_r_g.insert(new_r_g.begin(), 0.0);
    new_dr_g = dr_g;
    new_dr_g.insert(new_dr_g.begin(), new_r_g[1] - new_r_g[0]);
    double value = new_r_g[new_r_g.size()-1] + dr;
    while(value <= cutoff_r){
        new_r_g.push_back(value);
        new_dr_g.push_back(dr);
        value+=dr;
    }
    new_mesh = new_r_g;
*/

    /////////////// generate mesh on frequency Domain  /////////////////////////
    vector<double> q_i;
    value = 0.0;
    while(value<=max_freq){
        q_i.push_back(value);
        value+=dq;
    }
    new_f.clear();

    ////////////// Filtering //////////////////////
    compute(h, original_f, r_g, dr_g, l, new_r_g, new_dr_g, q_i, dq, q_cut, cutoff_r, new_f);
    return 0;
}

void Filter::compute(double target_scaling, vector<double> original_f, vector<double> r_g, vector<double> dr_g, int l, vector<double> new_r_g, vector<double> new_dr_g, vector<double> q_i, double dq_i, double q_cut, double rc2, vector<double>& new_f){

    if (l>3){
        Verbose::all() << "l is too high " << l<< std::endl;
        exit(-1);
    }

    int r_vector_length = r_g.size();
    int q_vector_length = q_i.size();

    double rc;
    int i_rc;
    for (int g = original_f.size()-1; g>0; g--){
        double tmp = original_f[g];
        if ( fabs(tmp) > 1.0e-20 ){
            rc=r_g[g];
            i_rc = g;
            break;
        }
    }

    const long double h = target_scaling;
    const long double q_max = M_PI/h;  // max frequency that grid can describe
    const int i_max = q_i.size(); //index for max frequency
    int i_cut = 0 ; // index for cutoff frequency
    for (int i=0;i<q_i.size();i++){
        if(q_i[i] >= q_cut){
            i_cut = i;
            break;
        }
    }


    if (i_cut==0){
        i_cut = i_max;
    }

    const long double c = sqrt(2/M_PI);  // constant for Bessel transformation

    int new_r_vector_length = new_r_g.size();

    long double** sinqr_ig = new long double* [q_vector_length];
    long double** cosqr_ig = new long double* [q_vector_length];
    long double** qr_ig = new long double* [q_vector_length];

    long double** new_sinqr_ig = new long double* [q_vector_length];
    long double** new_cosqr_ig = new long double* [q_vector_length];
    long double** new_qr_ig = new long double* [q_vector_length];

    long double* r2_g = new long double [r_vector_length];
    long double* q2_i = new long double [q_vector_length] ;
    long double* new_r2_g = new long double [new_r_vector_length];

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for (int i =0; i<q_vector_length ; i++){
        sinqr_ig[i] = new long double [r_vector_length];
        cosqr_ig[i] = new long double [r_vector_length];
        qr_ig[i] = new long double [r_vector_length];

        new_sinqr_ig[i] = new long double [new_r_vector_length];
        new_cosqr_ig[i] = new long double [new_r_vector_length];
        new_qr_ig[i] = new long double [new_r_vector_length];

        for(int g=0;g<r_vector_length;g++){
            qr_ig[i][g] = q_i[i]*r_g[g];
            cosqr_ig[i][g] = cos(qr_ig[i][g]);
            sinqr_ig[i][g] = sin(qr_ig[i][g]);
        }

        for(int new_g=0;new_g<new_r_vector_length;new_g++){
            new_qr_ig[i][new_g] = q_i[i]*new_r_g[new_g];
            new_cosqr_ig[i][new_g] = cos(new_qr_ig[i][new_g]);
            new_sinqr_ig[i][new_g] = sin(new_qr_ig[i][new_g]);
        }
    }

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int i=0;i<q_vector_length;i++){
        q2_i[i]=q_i[i]*q_i[i];
    }
    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int g=0;g<r_vector_length;g++){
        r2_g[g] = r_g[g]*r_g[g];
    }

    #ifdef ACE_HAVE_OMP
    #pragma omp parallel for
    #endif
    for(int new_g=0;new_g<new_r_vector_length;new_g++){
        new_r2_g[new_g] = new_r_g[new_g]*new_r_g[new_g];
    }

    vector<long double> cut_i;
    for (int i=0;i<q_vector_length;i++){
        cut_i.push_back(1.0);
    }

    vector<long double> fq_i;
    vector<double> fr_g;
    vector<long double> fdrim_g;
    for (int g=0;g<r_vector_length;g++){
        fdrim_g.push_back(original_f[g]*dr_g[g]);
    }

    switch(l){
        case 0:
            for (int i=0; i<q_vector_length;i++){
                long double val=0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:val)
                #endif
                for (int g=0;g<fdrim_g.size();g++){
                    if (fabs(qr_ig[i][g] ) < 1E-5){
                        val+=fdrim_g[g]*r2_g[g];
                    }
                    else{
                        val+=fdrim_g[g]*sinqr_ig[i][g]*r2_g[g]/qr_ig[i][g];
                    }
                }
                fq_i.push_back(c*val*cut_i[i]);
            }
            for (int new_g=0; new_g<new_r_vector_length; new_g++){
                long double val=0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:val)
                #endif
                for (int i=0; i<q_vector_length;i++){
                    if(fabs(new_r_g[new_g]*q_i[i]) < 1.e-10 ){
                        val+=fq_i[i]*q2_i[i];
                    }
                    else{
                        val+=fq_i[i]*new_sinqr_ig[i][new_g]*q2_i[i]/new_qr_ig[i][new_g];
                    }
                }
                fr_g.push_back(c*val*dq_i);
            }
            break;
        case 1:
            for (int i =0;i<q_vector_length;i++){
                long double val=0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:val)
                #endif
                for (int g =0;g<fdrim_g.size();g++){
                    if(fabs(qr_ig[i][g])<1.e-20){
                        val+=fdrim_g[g]*r2_g[g]*qr_ig[i][g]*0.3333333333;
                    }
                    else{
                        val+=(sinqr_ig[i][g]/qr_ig[i][g] - cosqr_ig[i][g]) * fdrim_g[g] * r2_g[g] / qr_ig[i][g];
                    }
                }
                fq_i.push_back(c*val*cut_i[i]);
            }
            for (int new_g =0; new_g<new_r_vector_length; new_g++){
                long double val = 0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:val)
                #endif
                for (int i =0;i<fq_i.size();i++){
                    if(fabs(new_qr_ig[i][new_g])<1.e-20){
                        val+=fq_i[i] * q2_i[i] * new_qr_ig[i][new_g] * 0.3333333333;
                    }
                    else{
                        val+=(new_sinqr_ig[i][new_g]/new_qr_ig[i][new_g] - new_cosqr_ig[i][new_g]) * fq_i[i] * q2_i[i] / new_qr_ig[i][new_g];
                    }
                }
                fr_g.push_back(c*val*dq_i);
            }
            break;
        case 2:
            for (int i=0;i<q_vector_length;i++){
                long double val=0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:val)
                #endif
                for (int g=0;g<fdrim_g.size();g++){
                    if(fabs(qr_ig[i][g])<1.e-20){
                        val+=fdrim_g[g]*r2_g[g]*qr_ig[i][g]*qr_ig[i][g]*0.06666666667;
                    }
                    else{
                        val+=fdrim_g[g]*((3/pow(qr_ig[i][g],3)-1/qr_ig[i][g])*sinqr_ig[i][g]- (3/pow(qr_ig[i][g],2))*cosqr_ig[i][g])*r2_g[g];
                    }
                }
                fq_i.push_back(c*val*cut_i[i]);
            }

            for (int new_g=0; new_g<new_r_vector_length; new_g++){
                long double val=0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:val)
                #endif
                for (int i=0;i<fq_i.size();i++){
                    if(fabs(new_qr_ig[i][new_g])<1.e-20){
                        val+=fq_i[i]*q2_i[i]*new_qr_ig[i][new_g]*new_qr_ig[i][new_g]*0.06666666667;
                    }
                    else{
                        val+=fq_i[i]*((3/pow(new_qr_ig[i][new_g],3)-1/new_qr_ig[i][new_g])*new_sinqr_ig[i][new_g]- (3/pow(new_qr_ig[i][new_g],2))*new_cosqr_ig[i][new_g])*q2_i[i];
                    }
                }
                fr_g.push_back(c*val*dq_i);
            }
            break;
        case 3:
            for (int i=0;i<q_vector_length;i++){
                double val=0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:val)
                #endif
                for (int g=0;g<fdrim_g.size();g++){
                    if(fabs(qr_ig[i][g])<1.e-20){
                        val+=fdrim_g[g]*r2_g[g]*qr_ig[i][g]*qr_ig[i][g]*qr_ig[i][g]*0.009523809524;
                    }
                    else{
                        val+=fdrim_g[g]*((15/pow(qr_ig[i][g],4)-6/pow(qr_ig[i][g],2))*sinqr_ig[i][g] - (15/pow(qr_ig[i][g],3)-1/qr_ig[i][g])*cosqr_ig[i][g])*r2_g[g];
                    }
                }
                fq_i.push_back(c*val*cut_i[i]);
            }
            for (int new_g=0; new_g<new_r_vector_length; new_g++){
                double val=0.0;
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for reduction(+:val)
                #endif
                for (int i=0;i<q_vector_length;i++){
                    if(fabs(new_qr_ig[i][new_g])<1.e-20){
                        val+=fq_i[i]*q2_i[i]*new_qr_ig[i][new_g]*new_qr_ig[i][new_g]*new_qr_ig[i][new_g]*0.009523809524;
                    }
                    else{
                        val+=fq_i[i]*((15/pow(new_qr_ig[i][new_g],4)-6/pow(new_qr_ig[i][new_g],2))*new_sinqr_ig[i][new_g] - (15/pow(new_qr_ig[i][new_g],3)-1/new_qr_ig[i][new_g])*new_cosqr_ig[i][new_g])*q2_i[i];
                    }
                }
                fr_g.push_back(c*val*dq_i);
            }
            break;
    }

    for (int i=0;i<q_vector_length;i++){
        delete[] sinqr_ig[i];
        delete[] cosqr_ig[i];
        delete[] qr_ig[i];

        delete[] new_sinqr_ig[i];
        delete[] new_cosqr_ig[i];
        delete[] new_qr_ig[i];
    }

    delete[] sinqr_ig;
    delete[] cosqr_ig;
    delete[] qr_ig;
    delete[] q2_i;
    delete[] r2_g;

    delete[] new_sinqr_ig;
    delete[] new_cosqr_ig;
    delete[] new_qr_ig;
    delete[] new_r2_g;
    new_f = fr_g;

    return;
}

void Filter::optimize_comp_charge_exps(vector<vector<double> > local_pp_radial,vector<vector<double> > original_mesh){

    double sum=std::numeric_limits<double>::max();
    double sum_tmp;
    double r_gaus,r_gaus_tmp;
    double r,scaled_r;

    vector<double> initial_comp_charge_exps;
    vector<int> types= atoms->get_atom_types();
    for (int i=0;i<types.size();i++){
        initial_comp_charge_exps.push_back(Periodic_atom::comp_charge_exp[types[i] - 1]);
    }

    for(int itype=0;itype<local_pp_radial.size();itype++){
        for(double val=0.25;val<=3.0;val+=0.25){
            r_gaus_tmp=val*initial_comp_charge_exps[itype];
            sum_tmp=0.0;
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for private(r,scaled_r)reduction(+:sum_tmp)
            #endif
            for (int i=0;i< original_mesh.size();i++){
                r=original_mesh[itype][i];
                scaled_r = r/r_gaus_tmp;
                sum_tmp+=r*r*fabs(local_pp_radial[itype][i]+Zvals[itype]*erf(scaled_r)/r);
            }
            if(sum_tmp<sum){
                r_gaus = r_gaus_tmp;
                sum=sum_tmp;
            }
        }
        comp_charge_exps.push_back(r_gaus);
    }

    //compute_comp_charge_and_potential();
    return ;
}

int Filter::get_comp_potential(vector<double> original_mesh, vector<double>& return_val,int itype){

    if(comp_charge_exps.size()==0){
        return -2;
    }

    vector<std::array<double,3> > positions = atoms->get_positions();

    return_val.clear();

    double r,scaled_r,r_gaus;
    r_gaus = comp_charge_exps[itype];
    for (int i=0;i< original_mesh.size();i++){
        r=original_mesh[i];
        scaled_r = r/r_gaus;
        return_val.push_back( Zvals[itype]*erf(scaled_r)/r );
    }

    return 0;
}


void Filter::get_comp_charge_exps(vector<double>& comp_charge_exps2){
    if(comp_charge_exps.size() == 0){
        Verbose::all() << "Filter::get_comp_charge_exps - no compensation charge." << std::endl;
        exit(EXIT_FAILURE);
    }
    else if(comp_charge_exps.size() != comp_charge_exps2.size()){
        Verbose::all() << "Filter::get_comp_charge_exps - the size of compensation charge is different." << std::endl;
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<comp_charge_exps2.size(); i++){
        comp_charge_exps2[i] = comp_charge_exps[i];
    }

    return;
}
