#include "Hgh2KB.hpp"
#include <cmath>
#include "Epetra_Map.h"

#include "Filter.hpp"

#include "../../Basis/Basis_Function/Basis_Function.hpp"
#include "../../Basis/Basis_Function/Sinc.hpp"
#include "../../Basis/Grid_Setting/Grid_Setting.hpp"
#include "../../Basis/Grid_Setting/Grid_Atoms.hpp"

#include "../../Util/Spherical_Harmonics.hpp"
#include "../../Util/Read_Hgh.hpp"
#include "../../Util/Read_Hgh_Internal.hpp"
#include "../../Util/Double_Grid.hpp"
#include "../../Util/Verbose.hpp"
#include "../../Util/Parallel_Util.hpp"

#include "../../Util/Radial_Grid.hpp"

using std::endl;
using std::fixed;
using std::string;
using std::scientific;
using std::abs;
using std::vector;

using Teuchos::Array;
using Teuchos::rcp;
using Teuchos::RCP;

Hgh2KB::Hgh2KB(RCP<const Basis> mesh,RCP<const Atoms> atoms,RCP<Teuchos::ParameterList> parameters){
    this->mesh = mesh;
    this->parameters=Teuchos::sublist(parameters,"BasicInformation");
    this->atoms=atoms;

    initialize_parameters();
    read_pp();  // read information from pseudopotential file; fill out all values related to radial mesh
    initialize_pp();
    this -> make_vectors();
}

void Hgh2KB::initialize_parameters(){
    if(!parameters->sublist("Pseudopotential").isParameter("PSFilenames")){
        Verbose::all() << "Hgh2KB::initialize_parameters - CANNOT find PSFilenames in input file." << std::endl;
        exit(EXIT_FAILURE);
    } else {
        hgh_filenames = parameters->sublist("Pseudopotential").get< Array<string> >("PSFilenames");
    }

    parameters->sublist("Pseudopotential").get<double>("NonlocalThreshold", 1E-6);
    parameters->sublist("Pseudopotential").get<double>("LocalThreshold", 1E-7);
/*
    if(!parameters->sublist("Filter").isParameter("FilterType")){
        parameters->sublist("Filter").set("FilterType","None");
        is_filter = false;
    }
    else if(parameters->sublist("Filter").get<string>("FilterType")=="TS"){
        is_filter = true;
    }
    else{
        parameters->sublist("Filter").set("FilterType","None");
        is_filter = false;
    }
*/
    parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid", 0);

    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1){
        parameters->sublist("Pseudopotential").get<int>("FineDimension", 3);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - FineDimension = " << parameters->sublist("Pseudopotential").get<int>("FineDimension") << std::endl;

        parameters->sublist("Pseudopotential").get<string>("FilterType", "Sinc");
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - FilterType = " << parameters->sublist("Pseudopotential").get<string>("FilterType") << std::endl;

        if(parameters->sublist("Pseudopotential").get<string>("FilterType")=="Lagrange"){
            parameters->sublist("Pseudopotential").get<int>("InterpolationOrder", 8);
            Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - Using Lagrange Filter function of order-" << parameters->sublist("Pseudopotential").get<int>("InterpolationOrder") << std::endl;
        }

        parameters->sublist("Pseudopotential").get<double>("LocalRgauss", 1.0);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - LocalRgauss = " << parameters->sublist("Pseudopotential").get<double>("LocalRgauss") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("ShortDecayTol", 0.0001);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - ShortDecayTol = " << parameters->sublist("Pseudopotential").get<double>("ShortDecayTol") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange", 1.25);
        Verbose::single(Verbose::Normal)<< "Hgh2KB::initialize_parameters - LocalIntegrationRange = " << parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange") << std::endl;

        /*
        if(!parameters->sublist("Pseudopotential").isParameter("NonlocalRmax")){
            parameters->sublist("Pseudopotential").set<double>("NonlocalRmax", 2.0);
        }
        Verbose::single(Verbose::Normal)<< "Hgh2KB::initialize_parameters - NonlocalRmax = " << parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") << std::endl;
        */
    }

    this -> type = parameters->sublist("Pseudopotential").get<string>("HghFormat", "Internal");
    Verbose::single(Verbose::Normal) << "\n#------------------------------------------- Hgh2KB::initialize_parameters\n";
    if(type == "HGH"){
        Verbose::single(Verbose::Normal) << " HGH pseudopotential format specified in Phys. Rev. B 58, 3641 is used.\n";
    } else if(type == "Willand"){
        Verbose::single(Verbose::Normal) << " HGH pseudopotential format specified in J. Chem. Phys. 138, 104109 is used.\n";
    } else if(type == "Internal"){
        Verbose::single(Verbose::Normal) << " Hard coded HGH pseudopotential is used.\n";
        this -> xc_type = parameters -> sublist("Pseudopotential").get<string>("XCType");
    } else{
        Verbose::all() << "Wrong pseudopotential format: Pseudopotential.HghFormat is wrong\n";
        exit(EXIT_FAILURE);
    }
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

    /*
    if(!parameters->sublist("Pseudopotential").isParameter("LocalIntegration")){
        parameters->sublist("Pseudopotential").set<string>("LocalIntegration", "coarse");
    }
    */

    parameters->sublist("Pseudopotential").get<string>("NonlocalIntegration", "coarse");

    if(parameters->sublist("Pseudopotential").get<string>("NonlocalIntegration") == "fine"){
        parameters->sublist("Pseudopotential").get<double>("NonlocalScaling", 5.0);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - Scaling = " << parameters->sublist("Pseudopotential").get<double>("NonlocalScaling") << '\n';

        parameters->sublist("Pseudopotential").get<double>("NonlocalRmax", 5.0);
        Verbose::single(Verbose::Normal) << "Hgh2KB::initialize_parameters - NonlocalRmax = " << parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") << '\n';
    }

    return;
}

void Hgh2KB::read_pp(){
    for(int itype=0; itype<atoms->get_num_types(); itype++){
        double Zval_tmp = 0.0;
        int number_vps = 0;
        bool core_correction = false;

        if(this -> type == "Internal"){
            Read::Hgh_Internal::read_header(hgh_filenames[itype], Zval_tmp, xc_type, core_correction);
        } else {
            Read::Hgh::read_header(hgh_filenames[itype], Zval_tmp, type, core_correction);
        }

        Zvals.push_back(Zval_tmp);

        if(this -> type == "Internal"){
            Read::Hgh_Internal::read_local(hgh_filenames[itype], xc_type, rloc, nloc, c_i);
        } else {
            Read::Hgh::read_local(hgh_filenames[itype], type, rloc, nloc, c_i);
        }

        vector<int> oamom_tmp;
        if(this -> type == "Internal"){
            Read::Hgh_Internal::read_nonlocal(hgh_filenames[itype], xc_type, h_ij_total, k_ij_total, r_l, projector_number, number_vps_file, oamom_tmp, itype);
        } else {
            Read::Hgh::read_nonlocal(hgh_filenames[itype], type, h_ij_total, k_ij_total, r_l, projector_number, number_vps_file, oamom_tmp, itype);
        }

        oamom.push_back(oamom_tmp);

        // Read nonlinear core correction
        double rcore_tmp = 0.0, ccore_tmp = 0.0;
        if(core_correction == true){
            if(this -> type == "Internal"){
                Read::Hgh_Internal::read_core_correction(hgh_filenames[itype], xc_type, rcore_tmp, ccore_tmp);
            } else {
                Read::Hgh::read_core_correction(hgh_filenames[itype], rcore_tmp, ccore_tmp);
            }

            nonlinear_core_correction.push_back(true);
        }
        else{
            nonlinear_core_correction.push_back(false);
        }
        rcore.push_back(rcore_tmp);
        ccore.push_back(ccore_tmp);
        // end

        pp_information(itype);
    }

    return;
}

void Hgh2KB::compute_nonlinear_core_correction(){
    int *MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements = mesh->get_map()->NumMyElements();

    core_density = Teuchos::rcp(new Epetra_Vector(*mesh->get_map()));
    core_density_grad = rcp(new Epetra_MultiVector(*mesh->get_map(),3) );

    const double** scaled_grid = mesh->get_scaled_grid();
    vector<std::array<double,3> > positions = atoms->get_positions();

    double analytic_core_charge = 0.0;

    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        double Z = atoms->get_atomic_numbers()[iatom];
        if(nonlinear_core_correction[itype] == true){

            analytic_core_charge += (Z - Zvals[itype]) * ccore[itype];

            for(int i=0; i<NumMyElements; i++){
                int i_x=0, i_y=0, i_z=0;
                mesh->decompose(MyGlobalElements[i], &i_x, &i_y, &i_z);

                double x = scaled_grid[0][i_x] - positions[iatom][0];
                double y = scaled_grid[1][i_y] - positions[iatom][1];
                double z = scaled_grid[2][i_z] - positions[iatom][2];
                double r = sqrt(x*x + y*y + z*z);

                double tmp = ccore[itype] * (Z - Zvals[itype]) / pow(sqrt(2.0*M_PI)*rcore[itype], 3) * exp(-0.5*r*r/rcore[itype]/rcore[itype]);

                core_density->SumIntoMyValue(i, 0, tmp);
                core_density_grad->SumIntoMyValue(i, 0, -1.0/rcore[itype]/rcore[itype] * x * tmp); // x-axis
                core_density_grad->SumIntoMyValue(i, 1, -1.0/rcore[itype]/rcore[itype] * y * tmp); // y-axis
                core_density_grad->SumIntoMyValue(i, 2, -1.0/rcore[itype]/rcore[itype] * z * tmp); // z-axis
            }
        }
    }
    double numerical_core_charge = 0.0;
    core_density->Norm1(&numerical_core_charge);
    numerical_core_charge *= mesh->get_scaling()[0] * mesh->get_scaling()[1] * mesh->get_scaling()[2];

    Verbose::single().precision(12);
    Verbose::single() << fixed;
    Verbose::single(Verbose::Normal) << endl;
    Verbose::single(Verbose::Normal) << "#------------------------------- Hgh2KB::compute_nonlinear_core_correction" << endl;
    Verbose::single(Verbose::Normal) << " Analytic core charge  = " << analytic_core_charge << endl;
    Verbose::single(Verbose::Normal) << " Numerical core charge = " << numerical_core_charge << endl;
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << endl;
    Verbose::single().precision(6);
    Verbose::single() << scientific;

    return;
}
/*
void Hgh2KB::calculate_local(Teuchos::RCP<Epetra_Vector>& local_potential){
    int *MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements = mesh->get_map()->NumMyElements();

    const double** scaled_grid = mesh->get_scaled_grid();
    vector<std::array<double,3> > positions = atoms->get_positions();
    double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");

    local_potential->PutScalar(0.0);

    Verbose::single(Verbose::Normal) << "\n#------------------------------------------------- Hgh2KB::calculate_local\n";
    Verbose::single(Verbose::Normal) << " Local pseudopotential integration: Gauss quadrature\n";
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        // Calculate local part of pseudopotential_matrix
        for(int i=0; i<NumMyElements; i++){
            double x,y,z,r;
            int i_x=0,i_y=0,i_z=0;
            mesh->decompose(MyGlobalElements[i], &i_x, &i_y, &i_z);

            // Distance between the grid point & the atom
            x = scaled_grid[0][i_x] - positions[iatom][0];
            y = scaled_grid[1][i_y] - positions[iatom][1];
            z = scaled_grid[2][i_z] - positions[iatom][2];
            r = sqrt(x*x + y*y + z*z);

            local_potential->operator[](i) += compute_Vlocal(itype, r);
            //local[i] = -Zval[itype] * tmp2 + exp( -0.5 * (r*r/(rloc[itype]*rloc[itype])) ) * tmp;
        }
        // end
    }

    return;
}*/

void Hgh2KB::get_comp_potential(vector<double> radial_mesh, int itype, vector<double>& comp_potential){
    vector<std::array<double,3>> positions =atoms->get_positions();
    const double** scaled_grid = mesh->get_scaled_grid();
    const double* scaling = mesh->get_scaling();

    comp_potential.clear();

    double r,scaled_r, r_gauss;
    r_gauss = parameters->sublist("Pseudopotential").get<double>("LocalRgauss");
    double comp_norm = 0.0;
    for (int i=0;i< radial_mesh.size();i++){
        r=radial_mesh[i];
        scaled_r = r/r_gauss;
        comp_norm += Zvals[itype]/pow(sqrt(M_PI)*r_gauss,3)*exp(-scaled_r*scaled_r);
        comp_potential.push_back( Zvals[itype]*erf(scaled_r)/r );
    }

    comp_norm = 0.0;
    for (int j=0; j<mesh->get_original_size(); j++){
        int j_x, j_y, j_z=0;
        double x, y, z, r;
        mesh->decompose(j, &j_x, &j_y, &j_z);
        x = scaled_grid[0][j_x];
        y = scaled_grid[1][j_y];
        z = scaled_grid[2][j_z];
        r = sqrt(x*x + y*y + z*z);
        scaled_r = r/r_gauss;
        if(r<=local_cutoff[itype]){
            scaled_r = r/r_gauss;
            comp_norm += Zvals[itype]/pow(sqrt(M_PI)*r_gauss,3)*exp(-scaled_r*scaled_r);
        }
    }
    comp_norm*=scaling[0]*scaling[1]*scaling[2];

    return;
}

/*
void Hgh2KB::calculate_local_DG(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian){
    int *MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements = mesh->get_map()->NumMyElements();

    Verbose::single(Verbose::Normal) << "\n#------------------------------------------- Hgh2KB::calculate_local_DG\n";
    Verbose::single(Verbose::Normal) << " Using double-grid to represent pseudopotential\n";
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

    int size = mesh->get_original_size();
    vector<double*> positions = atoms->get_positions();

    double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");

    local_pp.resize(atoms->get_original_size());
    local_pp_dev.resize(atoms->get_original_size());

    for(int iatom=0; iatom<atoms->get_original_size(); iatom++){
        local_pp[iatom].resize(size);
        local_pp_dev[iatom].resize(size);
        for(int i=0; i<size; i++){
            local_pp[iatom][i] = 0.0;
            local_pp_dev[iatom][i] = 0.0;
        }
    }

    const double* scaling = mesh->get_scaling();
    const double** scaled_grid = mesh->get_scaled_grid();
    double* fine_scaling = new double[3];
    double fine_dimension = parameters->sublist("Pseudopotential").get<int>("FineDimension");
    fine_scaling[0] = scaling[0] / fine_dimension;
    fine_scaling[1] = scaling[1] / fine_dimension;
    fine_scaling[2] = scaling[2] / fine_dimension;

    double length_for_x = (scaled_grid[0][mesh->get_points()[0]-1]-scaled_grid[0][0]);
    double length_for_y = (scaled_grid[1][mesh->get_points()[1]-1]-scaled_grid[1][0]);
    double length_for_z = (scaled_grid[2][mesh->get_points()[2]-1]-scaled_grid[2][0]);

    int numx = static_cast<int>(length_for_x / fine_scaling[0]) + 1;
    int numy = static_cast<int>(length_for_y / fine_scaling[1]) + 1;
    int numz = static_cast<int>(length_for_z / fine_scaling[2]) + 1;

    int* fine_points = new int[3];
    fine_points[0] = numx;
    fine_points[1] = numy;
    fine_points[2] = numz;

    Basis_Function* fine_mesh = new Sinc(fine_points, fine_scaling);
    const double** fine_scaled_grid = fine_mesh->get_scaled_grid();

    vector<double> rcut;

    for(int itype=0; itype<atoms->get_atom_types().size(); itype++){
        // Find rcut
        double rcut_tmp = 0.0;
        for(int i=0; i<11; i++){
            double r = 10.0 - static_cast<double>(i);
            double tmp = compute_Vshort(itype, r);
            if(abs(tmp) < 1.0e-4) rcut_tmp = r;
            else break;
        }
        for(int i=0; i<11; i++){
            double r = rcut_tmp - 0.1 * static_cast<double>(i);
            double tmp = compute_Vshort(itype, r);
            if(abs(tmp) < 1.0e-4) rcut_tmp = r;
            else break;
        }
        for(int i=0; i<11; i++){
            double r = rcut_tmp - 0.01 * static_cast<double>(i);
            double tmp = compute_Vshort(itype, r);
            if(abs(tmp) < 1.0e-4) rcut_tmp = r;
            else break;
        }
        for(int i=0; i<11; i++){
            double r = rcut_tmp - 0.001 * static_cast<double>(i);
            double tmp = compute_Vshort(itype, r);
            if(abs(tmp) < 1.0e-4) rcut_tmp = r;
            else break;
        }
        rcut_tmp *= parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange");
        Verbose::single() << " \"rcut\" for atom " << itype << ": " << rcut_tmp << " Bohr\n";
        rcut.push_back(rcut_tmp);
        // end
    }

    for(int iatom=0; iatom<atoms->get_original_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        RCP<Atoms> new_atom = rcp(new Atoms());
        Atom tmp_atom = Atom(atoms->operator[](iatom));
        new_atom->push(tmp_atom);

        vector<double> radius;
        radius.push_back(rcut[itype]);
        Grid_Setting* fine_grid = new Grid_Atoms(fine_points, fine_mesh, new_atom.get(), radius);

        Epetra_Map fine_map(fine_grid->get_original_size(), 0, mesh->get_map()->Comm());
        int fine_NumMyElements = fine_map.NumMyElements();
        int* fine_GlobalMyElements = fine_map.MyGlobalElements();

        for(int j=0; j<size; j++){
            double X = 0.0, Y = 0.0, Z = 0.0, R = 0.0;
            int j_x = 0, j_y = 0, j_z = 0;
            mesh->decompose(j, &j_x, &j_y, &j_z);
            X = scaled_grid[0][j_x] - positions[iatom][0];
            Y = scaled_grid[1][j_y] - positions[iatom][1];
            Z = scaled_grid[2][j_z] - positions[iatom][2];
            R = sqrt( X*X + Y*Y + Z*Z );

            double Vshort_tmp = 0.0;
            double total_Vshort_tmp = 0.0;
            if(R < rcut[itype]){
                for(int fine_q=0; fine_q<fine_NumMyElements; fine_q++){
                    int fine_qx = 0;
                    int fine_qy = 0;
                    int fine_qz = 0;

                    fine_grid->decompose(fine_GlobalMyElements[fine_q], fine_mesh->get_points(), &fine_qx, &fine_qy, &fine_qz);

                    double x = fine_scaled_grid[0][fine_qx] - positions[iatom][0];
                    double y = fine_scaled_grid[1][fine_qy] - positions[iatom][1];
                    double z = fine_scaled_grid[2][fine_qz] - positions[iatom][2];
                    double r = sqrt(x*x + y*y + z*z);

                    Vshort_tmp += compute_Vshort(itype, r) * sinc((X-x)/scaling[0]) * sinc((Y-y)/scaling[1]) * sinc((Z-z)/scaling[2]);
                }
                fine_map.Comm().SumAll(&Vshort_tmp, &total_Vshort_tmp, 1);
                total_Vshort_tmp *= 1.0 / pow(fine_dimension,3.0);
            }

            double Vlocal_tmp = 0.0;
            if(R < 1.0E-15){
                Vlocal_tmp = sqrt(2.0/M_PI) / rloc[itype];
            }
            else{
                Vlocal_tmp = erf( R/(sqrt(2.0)*rloc[itype]) ) / R;
            }
            Vlocal_tmp *= -Zvals[itype];

            double Vlocal = Vlocal_tmp + total_Vshort_tmp;
            local_pp[iatom][j] += Vlocal;

            int ierr;
            if(abs(Vlocal) > threshold){
                ierr = core_hamiltonian->InsertGlobalValues(j, 1, &Vlocal, &j);
            }
        }
        delete fine_grid;
    }

    delete[] fine_points;
    delete[] fine_scaling;
    delete fine_mesh;

    return;
}
*/

void Hgh2KB::calculate_nonlocal(int iatom, std::array<double,3> position, vector<vector<vector<vector<double> > > >& nonlocal_pp, vector<vector<vector<vector<double> > > >& nonlocal_dev_x, vector<vector<vector<vector<double> > > >& nonlocal_dev_y, vector<vector<vector<vector<double> > > >& nonlocal_dev_z, vector<vector<vector<vector<int> > > >& nonlocal_pp_index){

    int size = mesh->get_original_size();
    const double** scaled_grid = mesh->get_scaled_grid();
    double threshold = parameters->sublist("Pseudopotential").get<double>("NonlocalThreshold");

    int itype = atoms->get_atom_type(iatom);

    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1
            or (parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0
            and parameters->sublist("Pseudopotential").get<string>("NonlocalIntegration") == "coarse")){

        if(iatom == 0){
            Verbose::single(Verbose::Normal) << "\n#---------------------------------------------- Hgh2KB::calculate_nonlocal\n";
            Verbose::single(Verbose::Normal) << " Non-local pseudopotential integration: Gauss quadrature\n";
            Verbose::single(Verbose::Normal) << " Nonlocal threshold = " << threshold << endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";
        }

        for(int i=0; i<number_vps_file[itype]; i++){
            int l = oamom[itype][i];
            vector< vector< vector<double> > > nonlocal_pp_tmp;
            vector< vector< vector<int> > > nonlocal_pp_index_tmp;
            for(int m=0; m<2*l+1; m++){
                vector< vector<double> > nonlocal_pp_tmp_tmp;
                vector< vector<int> > nonlocal_pp_index_tmp_tmp;
                for(int p=0; p<projector_number[itype][i]; p++){
                    vector<double> nonlocal_pp_tmp_tmp_tmp;
                    vector<int> nonlocal_pp_index_tmp_tmp_tmp;
                    for(int j=0; j<size; j++){
                        int j_x=0, j_y=0, j_z=0;
                        mesh->decompose(j, &j_x, &j_y, &j_z);
                        double x = scaled_grid[0][j_x] - position[0];
                        double y = scaled_grid[1][j_y] - position[1];
                        double z = scaled_grid[2][j_z] - position[2];
                        double r = sqrt( x*x + y*y + z*z );

                        double value = sqrt(2) * pow(r, l+2*p) * exp(-0.5*r*r/(r_l[itype][i]*r_l[itype][i])) / (pow(r_l[itype][i], l+(4*(p+1)-1)*0.5) * sqrt(tgamma(l + (4*(p+1)-1)*0.5))) * Spherical_Harmonics::Ylm(l,m-l,x,y,z) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) );

                        if (fabs(value) > threshold){
                            nonlocal_pp_tmp_tmp_tmp.push_back(value);
                            nonlocal_pp_index_tmp_tmp_tmp.push_back(j);
                        }
                    }
                    nonlocal_pp_tmp_tmp.push_back(nonlocal_pp_tmp_tmp_tmp);
                    nonlocal_pp_index_tmp_tmp.push_back(nonlocal_pp_index_tmp_tmp_tmp);
                }
                nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp);
                nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp);
            }
            nonlocal_pp.push_back(nonlocal_pp_tmp);
            nonlocal_pp_index.push_back(nonlocal_pp_index_tmp);
        }
    }
    /*
    else if(parameters->sublist("Pseudopotential").get<string>("NonlocalIntegration") == "fine"){
        if(MyPID == 0 and iatom == 0){
            cout << endl;
            cout << "#---------------------------------------------- Hgh2KB::calculate_nonlocal" << endl;
            cout << " Non-local pseudopotential integration: Numerical integration" << endl;
            cout << " Nonlocal threshold = " << threshold << endl;
            cout << "#---------------------------------------------------------------------------" << endl;
        }

        vector<double> I_x0, I_y0, I_z0;
        vector<double> I_x1, I_y1, I_z1;
        vector<double> I_x2, I_y2, I_z2;
        vector<double> I_x3, I_y3, I_z3;
        vector<double> I_x4, I_y4, I_z4;

        for(int i=0;i<number_vps_file[itype];i++){
            int l = oamom[itype][i];
            I_x0.clear(); I_y0.clear(); I_z0.clear();
            I_x1.clear(); I_y1.clear(); I_z1.clear();
            I_x2.clear(); I_y2.clear(); I_z2.clear();
            I_x3.clear(); I_y3.clear(); I_z3.clear();

            vector< vector< vector<double> > > nonlocal_pp_tmp;
            vector< vector< vector<int> > > nonlocal_pp_index_tmp;

            switch(l){
                case(0):{
                    vector< vector<double> > nonlocal_pp_tmp_tmp;
                    vector< vector<int> > nonlocal_pp_index_tmp_tmp;
                    for(int j=1; j<=projector_number[itype][i]; j++){
                        if(abs(h_ij_total[itype][i][j-1]) > 1.0E-30){
                            if(j == 1){
                                vector<double> nonlocal_pp_tmp_tmp_tmp;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp;

                                I_x0 = integrate(r_l[itype][i], 0, 0, position[0]);
                                I_y0 = integrate(r_l[itype][i], 0, 1, position[1]);
                                I_z0 = integrate(r_l[itype][i], 0, 2, position[2]);

                                double constant = 1.0 / (pow(M_PI,0.75) * pow(r_l[itype][i],1.5));

                                int k_x=0, k_y=0, k_z=0;
                                for(int k=0; k<size; k++){
                                    grid_setting->decompose(k, basis->get_points(), &k_x, &k_y, &k_z);
                                    double tmp = constant * I_x0[k_x] * I_y0[k_y] * I_z0[k_z];

                                    if(abs(tmp) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp.push_back(tmp);
                                        nonlocal_pp_index_tmp_tmp_tmp.push_back(k);
                                    }
                                }
                                nonlocal_pp_tmp_tmp.push_back(nonlocal_pp_tmp_tmp_tmp);
                                nonlocal_pp_index_tmp_tmp.push_back(nonlocal_pp_index_tmp_tmp_tmp);
                            }
                            else if(j == 2){
                                vector<double> nonlocal_pp_tmp_tmp_tmp;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp;

                                I_x2 = integrate(r_l[itype][i], 2, 0, position[0]);
                                I_y2 = integrate(r_l[itype][i], 2, 1, position[1]);
                                I_z2 = integrate(r_l[itype][i], 2, 2, position[2]);

                                double constant = 2.0 / ( sqrt(15) * pow(M_PI,0.75) * pow(r_l[itype][i],3.5) );

                                int k_x=0, k_y=0, k_z=0;
                                for(int k=0; k<size; k++){
                                    grid_setting->decompose(k, basis->get_points(), &k_x, &k_y, &k_z);
                                    double tmp = constant * ( I_x2[k_x] * I_y0[k_y] * I_z0[k_z] + I_x0[k_x] * I_y2[k_y] * I_z0[k_z] + I_x0[k_x] * I_y0[k_y] * I_z2[k_z] );
                                    if(abs(tmp) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp.push_back(tmp);
                                        nonlocal_pp_index_tmp_tmp_tmp.push_back(k);
                                    }
                                }
                                nonlocal_pp_tmp_tmp.push_back(nonlocal_pp_tmp_tmp_tmp);
                                nonlocal_pp_index_tmp_tmp.push_back(nonlocal_pp_index_tmp_tmp_tmp);
                            }
                            else{
                                cout << "Not implemeneted." << endl;
                                exit(EXIT_FAILURE);
                            }
                        }
                        else{
                            nonlocal_pp_tmp_tmp.push_back(vector<double>());
                            nonlocal_pp_index_tmp_tmp.push_back(vector<int>());
                        }
                    }
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp);
                } break;

                case(1):{
                    vector< vector<double> > nonlocal_pp_tmp_tmp1;
                    vector< vector<double> > nonlocal_pp_tmp_tmp2;
                    vector< vector<double> > nonlocal_pp_tmp_tmp3;
                    vector< vector<int> > nonlocal_pp_index_tmp_tmp1;
                    vector< vector<int> > nonlocal_pp_index_tmp_tmp2;
                    vector< vector<int> > nonlocal_pp_index_tmp_tmp3;
                    for(int j=1; j<=projector_number[itype][i]; j++){
                        if(abs(h_ij_total[itype][i][j-1]) > 1.0E-30){
                            if(j == 1){
                                vector<double> nonlocal_pp_tmp_tmp_tmp1;
                                vector<double> nonlocal_pp_tmp_tmp_tmp2;
                                vector<double> nonlocal_pp_tmp_tmp_tmp3;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp1;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp2;
                                vector<int> nonlocal_pp_index_tmp_tmp_tmp3;

                                I_x0 = integrate(r_l[itype][i],0,0,position[0]);
                                I_y0 = integrate(r_l[itype][i],0,1,position[1]);
                                I_z0 = integrate(r_l[itype][i],0,2,position[2]);
                                I_x1 = integrate(r_l[itype][i],1,0,position[0]);
                                I_y1 = integrate(r_l[itype][i],1,1,position[1]);
                                I_z1 = integrate(r_l[itype][i],1,2,position[2]);

                                double constant = sqrt(2) / ( pow(M_PI,0.75) * pow(r_l[itype][i],2.5) );

                                int k_x=0, k_y=0, k_z=0;
                                for(int k=0; k<size; k++){
                                    grid_setting->decompose(k, basis->get_points(), &k_x, &k_y, &k_z);
                                    double tmp1 = constant * ( I_x0[k_x] * I_y1[k_y] * I_z0[k_z] );
                                    double tmp2 = constant * ( I_x0[k_x] * I_y0[k_y] * I_z1[k_z] );
                                    double tmp3 = constant * ( I_x1[k_x] * I_y0[k_y] * I_z0[k_z] );

                                    if(abs(tmp1) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp1.push_back(tmp1);
                                        nonlocal_pp_index_tmp_tmp_tmp1.push_back(k);
                                    }
                                    if(abs(tmp2) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp2.push_back(tmp2);
                                        nonlocal_pp_index_tmp_tmp_tmp2.push_back(k);
                                    }
                                    if(abs(tmp3) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp3.push_back(tmp3);
                                        nonlocal_pp_index_tmp_tmp_tmp3.push_back(k);
                                    }
                                }
                                nonlocal_pp_tmp_tmp1.push_back(nonlocal_pp_tmp_tmp_tmp1);
                                nonlocal_pp_tmp_tmp2.push_back(nonlocal_pp_tmp_tmp_tmp2);
                                nonlocal_pp_tmp_tmp3.push_back(nonlocal_pp_tmp_tmp_tmp3);
                                nonlocal_pp_index_tmp_tmp1.push_back(nonlocal_pp_index_tmp_tmp_tmp1);
                                nonlocal_pp_index_tmp_tmp2.push_back(nonlocal_pp_index_tmp_tmp_tmp2);
                                nonlocal_pp_index_tmp_tmp3.push_back(nonlocal_pp_index_tmp_tmp_tmp3);
                            }
                            else{
                                cout << "Not implemented." << endl;
                                exit(EXIT_FAILURE);
                            }
                        }
                        else{
                            nonlocal_pp_tmp_tmp1.push_back(vector<double>());
                            nonlocal_pp_tmp_tmp2.push_back(vector<double>());
                            nonlocal_pp_tmp_tmp3.push_back(vector<double>());
                            nonlocal_pp_index_tmp_tmp1.push_back(vector<int>());
                            nonlocal_pp_index_tmp_tmp2.push_back(vector<int>());
                            nonlocal_pp_index_tmp_tmp3.push_back(vector<int>());
                        }
                    }
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp1);
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp2);
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp3);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp1);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp2);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp3);

                } break;

                default:{
                    cout << "Not implemented." << endl;
                    exit(EXIT_FAILURE);
                }

            } // switch
            nonlocal_pp.push_back(nonlocal_pp_tmp);
            nonlocal_pp_index.push_back(nonlocal_pp_index_tmp);
        } // for(int i=0;i<number_vps_file[itype];i++){
    }
    */
    else{
        Verbose::all() << "Hgh2KB::calculate_nonlocal - not supported." << endl;
        exit(EXIT_FAILURE);
    }

    return;
}

vector<double> Hgh2KB::integrate(double r_l, int exponent, int axis, double position){
    vector<double> value;

    // points = 500, max = 50.0 : tested for O atom of one H2O molecule
    double max = 7.0 * r_l; // In this case, the exponential term in the integral kernal is smaller than 2.3E-11

    // Adaptive scheme
    for(int i=0;i<mesh->get_points()[axis];i++){

        double h = 0.01 * mesh->get_scaling()[axis];
        //double h = 0.5 * basis->get_scaling()[axis]; // convergence = 1.0E-10
        int points = 2.0 * max / h;
        h = 2.0 * max / double(points-1);

        double convergence = 0.0;
        double before_val;
        double sum_val = 0.0;

        int j;
        for(j=0; j<2 || convergence > 1.0E-10; j++){
            before_val = sum_val;
            sum_val = 0.0;

            for(int k=0;k<points;k++){
                double sampling_point = - max + double(k) * h;
                sum_val += pow(sampling_point,exponent) * exp( -0.5*sampling_point*sampling_point / (r_l*r_l) ) * mesh->compute_basis(i,sampling_point+position,axis) * h;
            }
            convergence = abs(sum_val - before_val);
            points *= 2;
            h = 2.0 * max / double(points-1);
        }
        //cout << "No. of iterations = " << j << endl;
        //cout << "Integration spacing = " << h << endl;
        value.push_back(sum_val);
    }
    // end


/*
  for(int i=0;i<grid_setting->get_points()[axis];i++){
    double value_tmp = 0.0;
    for(int j=0;j<points;j++){
      double sampling_point = - max + double(j) * h;
      value_tmp += pow(sampling_point,exponent) * exp( -0.5*sampling_point*sampling_point / (r_l*r_l) ) * basis->compute_basis(i,sampling_point+position,axis) * h;
    }
    value.push_back(value_tmp);
  }
*/

    Verbose::single(Verbose::Detail) << "\n=============================================\n";
    Verbose::single(Verbose::Detail) << "Hgh pseudopotential integration\n";
    Verbose::single(Verbose::Detail) << "---------------------------------------------\n";
    Verbose::single(Verbose::Detail) << "                            r_l = " << r_l << endl;
    Verbose::single(Verbose::Detail) << "                       exponent = " << exponent << endl;
    Verbose::single(Verbose::Detail) << "                           axis = " << axis << endl;
    Verbose::single(Verbose::Detail) << "                atomic position = " << position << endl;
  //cout << "        # of integration points = " << points << endl;
    Verbose::single(Verbose::Detail) << "      maximum integration range = " << max << endl;
  //cout << "            integration spacing = " << h << endl;
    Verbose::single(Verbose::Detail) << "=============================================\n";

    return value;
}

/*
void Hgh2KB::construct_matrix(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian){
    int ierr;

    Epetra_Map Map = core_hamiltonian->RowMap();

    vector<double*> positions = atoms->get_positions();

    for(int iatom=0; iatom<atoms->get_original_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        vector< vector< vector< vector<double> > > > nonlocal_pp;
        vector< vector< vector< vector<double> > > > nonlocal_dev_x;
        vector< vector< vector< vector<double> > > > nonlocal_dev_y;
        vector< vector< vector< vector<double> > > > nonlocal_dev_z;
        vector< vector< vector< vector<int> > > > nonlocal_pp_index;
        calculate_nonlocal(iatom,positions[iatom],nonlocal_pp,nonlocal_dev_x,nonlocal_dev_y,nonlocal_dev_z,nonlocal_pp_index);  // calculate nonlocal potential
        V_vector.push_back(nonlocal_pp);
        V_dev_x.push_back(nonlocal_dev_x);
        V_dev_y.push_back(nonlocal_dev_y);
        V_dev_z.push_back(nonlocal_dev_z);
        V_index.push_back(nonlocal_pp_index);

        for(int k=0; k<nonlocal_pp.size(); k++){
            int l = oamom[itype][k];
            for(int m=0; m<nonlocal_pp[k].size(); m++){ // spin quantum number
                for(int a=0; a<nonlocal_pp[k][m].size(); a++){ // projector index a
                    int index_size_a = nonlocal_pp_index[k][m][a].size();
                    Verbose::single() << "iatom = " << iatom << ", l = " << l << ", m = " << m-l << ", projector = " << a << ", vector size = " << index_size_a << endl;
                    for(int b=0;b<nonlocal_pp[k][m].size();b++){ // projector index b
                        int index_size_b = nonlocal_pp_index[k][m][b].size();
                        int index = a * nonlocal_pp[k][m].size() + b;

                        for(int i=0;i<index_size_a;i++){
                            vector<double> values;
                            int i_index = nonlocal_pp_index[k][m][a][i];
                            if(Map.MyGID(i_index)){
                                for(int j=0;j<index_size_b;j++){
                                    values.push_back(h_ij_total[itype][k][index] * nonlocal_pp[k][m][a][i] * nonlocal_pp[k][m][b][j]);
                                }
                                ierr = core_hamiltonian->InsertGlobalValues(i_index, values.size(), &values[0], &nonlocal_pp_index[k][m][b][0]);
                                if(ierr < 0){ // If the allocated length of the row has to be expanded, a positive warning code will be returned.
                                    cout << iatom <<  "ERROR: core_hamiltonian->InsertGlobalValues(V_index[k][m][a][i], values.size(), &values[0], &V_index[k][m][a][0]) - ierr = " << ierr << endl;
                                }
                            }
                        }
                    }
                }
            }
        }
    } //for(int iatom=0;iatom<number_atom;iatom++){

    return;
}
*/

int Hgh2KB::read_pao(int itype){
  return 0;
}

void Hgh2KB::pp_information(int itype){
    Verbose::single(Verbose::Simple) << "\n==========================================================\n";
    Verbose::single(Verbose::Simple) << "Pseudopotential file for this atom  : " << hgh_filenames[itype] << endl;
    Verbose::single(Verbose::Simple) << "Valence charge                      : " << Zvals[itype] << endl;
    Verbose::single(Verbose::Simple) << "Number of projector types           : " << number_vps_file[itype] << endl;
    for(int i=0;i<number_vps_file[itype];i++){
        Verbose::single(Verbose::Simple) << "     Orbital angular momentum       : " << oamom[itype][i] << endl;
        Verbose::single(Verbose::Simple) << "              # of projectors       : " << projector_number[itype][i] << endl;
    }
    Verbose::single(Verbose::Simple) << "Nonlinear core correction           : " << nonlinear_core_correction[itype] << endl;
    Verbose::single(Verbose::Simple) << "==========================================================\n";

    return;
}

double Hgh2KB::compute_Vlocal(int itype, double r){
    double retval = 0.0;

    double tmp = 0.0;

    // Gaussian term exponential part
    for(int j=0; j<c_i[itype].size(); j++){
        if(j==0) tmp += c_i[itype][j];
        else tmp += c_i[itype][j] * pow(r/rloc[itype], j*2.0);
    }

    // Erf term.
    double tmp2 = 0.0;
    if(r < 1.0E-15){
        tmp2 = sqrt(2.0/M_PI) / rloc[itype];
    }
    else{
        tmp2 = erf( r/(sqrt(2)*rloc[itype]) ) / r;
    }

    retval = -Zvals[itype] * tmp2 + exp( -0.5 * (r*r/(rloc[itype]*rloc[itype])) ) * tmp;

    return retval;
}

double Hgh2KB::compute_Vshort(int itype, double r){
    double retval = 0.0;

    for(int j=0; j<c_i[itype].size(); j++){
        if(j==0) retval += c_i[itype][j];
        else retval += c_i[itype][j] * pow(r/rloc[itype], j*2.0);
    }
    retval *= exp( -0.5 * (r*r/(rloc[itype]*rloc[itype])) );

    return retval;
}

double Hgh2KB::sinc(double x){
    if(x==0) return 1.0;
    else return sin(M_PI*x)/(M_PI*x);
}

vector< vector<int> > Hgh2KB::get_oamom(){
    return oamom;
}

vector<double> Hgh2KB::get_local_cutoff(){
  return local_cutoff;
}

vector< vector<double> > Hgh2KB::get_input_mesh(){
  return local_mesh;
}

vector< vector<double> > Hgh2KB::get_input_local(){
  return short_potential_radial;
}

vector< vector<double> > Hgh2KB::get_input_EKB(){
    return EKB;
}

vector< vector< vector< vector< vector<double> > > > > Hgh2KB::get_V_nl(){
    if(V_vector.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_vector;
}

vector< vector< vector< vector< vector<double> > > > > Hgh2KB::get_V_nl_dev_x(){
    if(V_dev_x.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_x;
}

vector< vector< vector< vector< vector<double> > > > > Hgh2KB::get_V_nl_dev_y(){
    if(V_dev_y.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_y;
}

vector< vector< vector< vector< vector<double> > > > > Hgh2KB::get_V_nl_dev_z(){
    if(V_dev_z.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_z;
}

vector< vector< vector< vector < vector<int> > > > > Hgh2KB::get_V_index(){
    if(V_index.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_index;
}

vector< vector<double> > Hgh2KB::get_V_local(){
    if(local_pp.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed local vectors" << std::endl;
        this -> make_local_pp();
    }
    return local_pp;
}

vector< vector<double> > Hgh2KB::get_V_local_dev(){
    return local_pp_dev;
}

void Hgh2KB::make_vectors(){
    make_local_pp();
    make_nonlocal_vectors();
}

void Hgh2KB::make_local_pp(){
    /*
    local_pp.resize(size);
    for(int i=0; i<size; i++){
        local_pp[i] = 0.0;
    }
    */

    local_pp.resize(atoms->get_size());
    int size = mesh->get_original_size();
    for(int iatom=0; iatom<atoms->get_size(); iatom++){
         local_pp[iatom].resize(size);
         for(int i=0; i<size; i++){
             local_pp[iatom][i] = 0.0;
         }
    }

    if( parameters -> sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0 ){
        int size = mesh -> get_original_size();
        int *MyGlobalElements = mesh->get_map()->MyGlobalElements();
        int NumMyElements = mesh->get_map()->NumMyElements();
        const double** scaled_grid = mesh->get_scaled_grid();
        vector<std::array<double,3> > positions = atoms->get_positions();
        double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");

        Verbose::single(Verbose::Normal) << "\n#------------------------------------------------- Hgh2KB::calculate_local\n";
        Verbose::single(Verbose::Normal) << " Local pseudopotential integration: Gauss quadrature\n";
        Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

        /*
        local_pp.resize(atoms->get_original_size());
        for(int iatom=0; iatom<atoms->get_original_size(); iatom++){
            local_pp[iatom].resize(size);
            for(int i=0; i<size; i++){
                local_pp[iatom][i] = 0.0;
            }
        }
        */

        for(int iatom=0; iatom<atoms->get_size(); iatom++){
            int itype = atoms->get_atom_type(iatom);

            // Calculate local part of pseudopotential_matrix
            #ifdef ACE_HAVE_OMP
            #pragma omp parallel for
            #endif
            for(int i=0; i<NumMyElements; i++){
                double x,y,z,r;
                int i_x=0,i_y=0,i_z=0;
                mesh->decompose(MyGlobalElements[i], &i_x, &i_y, &i_z);

                // Distance between the grid point & the atom
                x = scaled_grid[0][i_x] - positions[iatom][0];
                y = scaled_grid[1][i_y] - positions[iatom][1];
                z = scaled_grid[2][i_z] - positions[iatom][2];
                r = sqrt(x*x + y*y + z*z);

                local_pp[iatom][MyGlobalElements[i]] += compute_Vlocal(itype, r);
                //local_pp[MyGlobalElements[i]] += compute_Vlocal(itype, r);
            }
            // end
        }
    }
    else{
        // use double grid
        int *MyGlobalElements = mesh->get_map()->MyGlobalElements();
        int NumMyElements = mesh->get_map()->NumMyElements();

        Verbose::single(Verbose::Normal) << "\n#------------------------------------------- Hgh2KB::calculate_local_DG\n";
        Verbose::single(Verbose::Normal) << " Using double-grid to represent pseudopotential\n";
        Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------\n";

        int size = mesh->get_original_size();
        vector<std::array<double,3> > positions = atoms->get_positions();

        double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");

        const double* scaling = mesh->get_scaling();
        const double** scaled_grid = mesh->get_scaled_grid();
        double* fine_scaling = new double[3];
        double fine_dimension = parameters->sublist("Pseudopotential").get<int>("FineDimension");
        fine_scaling[0] = scaling[0] / fine_dimension;
        fine_scaling[1] = scaling[1] / fine_dimension;
        fine_scaling[2] = scaling[2] / fine_dimension;

        double length_for_x = (scaled_grid[0][mesh->get_points()[0]-1]-scaled_grid[0][0]);
        double length_for_y = (scaled_grid[1][mesh->get_points()[1]-1]-scaled_grid[1][0]);
        double length_for_z = (scaled_grid[2][mesh->get_points()[2]-1]-scaled_grid[2][0]);

        int numx = static_cast<int>(length_for_x / fine_scaling[0]) + 1;
        int numy = static_cast<int>(length_for_y / fine_scaling[1]) + 1;
        int numz = static_cast<int>(length_for_z / fine_scaling[2]) + 1;

        int* fine_points = new int[3];
        fine_points[0] = numx;
        fine_points[1] = numy;
        fine_points[2] = numz;

        RCP<Basis_Function> fine_basis = rcp( new Sinc(fine_points, fine_scaling) );
        const double** fine_scaled_grid = fine_basis->get_scaled_grid();

        vector<double> rcut;

        for(int itype=0; itype<atoms->get_atom_types().size(); itype++){
            // Find rcut
            double rcut_tmp = 0.0;
            for(int i=0; i<11; i++){
                double r = 10.0 - static_cast<double>(i);
                double tmp = compute_Vshort(itype, r);
                if(abs(tmp) < 1.0e-4) rcut_tmp = r;
                else break;
            }
            for(int i=0; i<11; i++){
                double r = rcut_tmp - 0.1 * static_cast<double>(i);
                double tmp = compute_Vshort(itype, r);
                if(abs(tmp) < 1.0e-4) rcut_tmp = r;
                else break;
            }
            for(int i=0; i<11; i++){
                double r = rcut_tmp - 0.01 * static_cast<double>(i);
                double tmp = compute_Vshort(itype, r);
                if(abs(tmp) < 1.0e-4) rcut_tmp = r;
                else break;
            }
            for(int i=0; i<11; i++){
                double r = rcut_tmp - 0.001 * static_cast<double>(i);
                double tmp = compute_Vshort(itype, r);
                if(abs(tmp) < 1.0e-4) rcut_tmp = r;
                else break;
            }
            rcut_tmp *= parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange");
            Verbose::single(Verbose::Normal) << " \"rcut\" for atom " << itype << ": " << rcut_tmp << " Bohr\n";
            rcut.push_back(rcut_tmp);
            // end
        }

        for(int iatom=0; iatom<atoms->get_size(); iatom++){
            int itype = atoms->get_atom_type(iatom);

            RCP<Atoms> new_atom = rcp(new Atoms());
            Atom tmp_atom = Atom(atoms->operator[](iatom));
            new_atom->push(tmp_atom);

            vector<double> radius;
            radius.push_back(rcut[itype]);
            Grid_Setting* fine_grid = new Grid_Atoms(fine_points, fine_basis, new_atom.get(), radius);

            Epetra_Map fine_map(fine_grid->get_size(), 0, mesh->get_map()->Comm());
            int fine_NumMyElements = fine_map.NumMyElements();
            int* fine_GlobalMyElements = fine_map.MyGlobalElements();

            for(int j=0; j<size; j++){
                double X = 0.0, Y = 0.0, Z = 0.0, R = 0.0;
                int j_x = 0, j_y = 0, j_z = 0;
                mesh->decompose(j, &j_x, &j_y, &j_z);
                X = scaled_grid[0][j_x] - positions[iatom][0];
                Y = scaled_grid[1][j_y] - positions[iatom][1];
                Z = scaled_grid[2][j_z] - positions[iatom][2];
                R = sqrt( X*X + Y*Y + Z*Z );

                double Vshort_tmp = 0.0;
                double total_Vshort_tmp = 0.0;
                if(R < rcut[itype]){
                    for(int fine_q=0; fine_q<fine_NumMyElements; fine_q++){
                        int fine_qx = 0;
                        int fine_qy = 0;
                        int fine_qz = 0;

                        fine_grid->decompose(fine_GlobalMyElements[fine_q], fine_basis->get_points(), &fine_qx, &fine_qy, &fine_qz);

                        double x = fine_scaled_grid[0][fine_qx] - positions[iatom][0];
                        double y = fine_scaled_grid[1][fine_qy] - positions[iatom][1];
                        double z = fine_scaled_grid[2][fine_qz] - positions[iatom][2];
                        double r = sqrt(x*x + y*y + z*z);

                        Vshort_tmp += compute_Vshort(itype, r) * sinc((X-x)/scaling[0]) * sinc((Y-y)/scaling[1]) * sinc((Z-z)/scaling[2]);
                    }
                    Parallel_Util::group_sum(&Vshort_tmp, &total_Vshort_tmp, 1);
                    total_Vshort_tmp *= 1.0 / pow(fine_dimension,3.0);
                }

                double Vlocal_tmp = 0.0;
                if(R < 1.0E-15){
                    Vlocal_tmp = sqrt(2.0/M_PI) / rloc[itype];
                }
                else{
                    Vlocal_tmp = erf( R/(sqrt(2.0)*rloc[itype]) ) / R;
                }
                Vlocal_tmp *= -Zvals[itype];

                double Vlocal = Vlocal_tmp + total_Vshort_tmp;
                local_pp[iatom][j] += Vlocal;
                //local_pp[j] += Vlocal;
            }
            delete fine_grid;
        }

        delete[] fine_points;
        delete[] fine_scaling;
        //delete fine_mesh;
    }
    return;
}

void Hgh2KB::make_nonlocal_vectors(){
    //////////////Nonlocal potential ///////////////////
    vector<std::array<double,3> > positions = atoms->get_positions();
    for(int iatom=0;iatom<atoms->get_size();iatom++){
        int itype = atoms->get_atom_type(iatom);
        vector<vector<vector<vector<double> > > > nonlocal_pp;
        vector<vector<vector<vector<double> > > > nonlocal_dev_x;
        vector<vector<vector<vector<double> > > > nonlocal_dev_y;
        vector<vector<vector<vector<double> > > > nonlocal_dev_z;
        vector<vector<vector<vector<int> > > > nonlocal_pp_index;
        calculate_nonlocal(iatom,positions[iatom],nonlocal_pp,nonlocal_dev_x,nonlocal_dev_y,nonlocal_dev_z,nonlocal_pp_index);  //calculate nonlocal potential
        V_vector.push_back(nonlocal_pp);
        V_dev_x.push_back(nonlocal_dev_x);
        V_dev_y.push_back(nonlocal_dev_y);
        V_dev_z.push_back(nonlocal_dev_z);
        V_index.push_back(nonlocal_pp_index);

        nonlocal_pp.clear();
        nonlocal_dev_x.clear();
        nonlocal_dev_y.clear();
        nonlocal_dev_z.clear();
        nonlocal_pp_index.clear();
    }
    return;
}

void Hgh2KB::initialize_pp(){
    return;
}

vector< vector< vector<double> > > Hgh2KB::get_h_ij_total(){
    return this -> h_ij_total;
}

void Hgh2KB::get_vector_address(vector< vector<double> >& local_pp, vector<vector<vector<vector<vector<double> > > > >& V_vector, vector<vector<vector<vector<vector<int> > > > >& V_index, vector< vector<double> >& EKB, vector< vector<int> >& oamom, vector<int>& number_vps_file){
//void Hgh2KB::get_vector_address(vector<double>& local_pp, vector<vector<vector<vector<vector<double> > > > >& V_vector, vector<vector<vector<vector<vector<int> > > > >& V_index, vector< vector<double> >& EKB, vector< vector<int> >& oamom, vector<int>& number_vps_file){
    local_pp = this->local_pp;
    V_vector = this->V_vector;
    V_index = this->V_index;
    EKB = this->EKB;
    oamom = this->oamom;
    number_vps_file = this->number_vps_file;
}

void Hgh2KB::clear_vector(){
    this -> local_pp.clear();
    //this -> local_pp_dev.clear();
    this -> V_index.clear();
    this -> V_vector.clear();
    this -> V_dev_x.clear();
    this -> V_dev_y.clear();
    this -> V_dev_z.clear();
}
