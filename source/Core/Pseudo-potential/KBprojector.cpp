#include "KBprojector.hpp"
#include <iostream>

#include "Epetra_Map.h"

#include "../../Util/Spherical_Harmonics.hpp"
#include "../../Util/Verbose.hpp"

using std::string;
using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

// NLCC
int KBprojector::get_core_electron_info(Array< RCP<Epetra_Vector> >& density, Array< RCP<Epetra_MultiVector> >& density_grad){
    if(density.size() == 1){
        density[0]->Update(1.0, *this->core_density, 0.0);
        density_grad[0]->Update(1.0, *this->core_density_grad, 0.0);
    }
    else if(density.size() == 2){
        for(int i_spin=0; i_spin<density.size(); i_spin++){
            density[i_spin]->Update(0.5, *this->core_density, 0.0);
            density_grad[i_spin]->Update(0.5, *this->core_density_grad, 0.0);
        }
    }
    else{
        Verbose::all() << "KBprojector::get_core_electron_info - ERROR.\n";
        Verbose::all() << density.size() << '\n';
        exit(EXIT_FAILURE);
    }

    return 0;
}


vector<double> KBprojector::get_Zvals(){
  return Zvals;
}
vector<int> KBprojector::get_projector_numbers(){
    return number_vps_file;
}
/*
void KBprojector::calculate_KB(RCP<Epetra_CrsMatrix>& core_hamiltonian){
    clock_t start_time,end_time;
    start_time=clock();

    int ierr;
    int size = mesh->get_original_size();
    Epetra_Map Map = core_hamiltonian->RowMap();

    int number_atom = atoms->get_size();
    int NumMyElements = core_hamiltonian->Map().NumMyElements();
    int* MyGlobalElements = core_hamiltonian->Map().MyGlobalElements();
    //////////// local potential ///////////////////////
    if(parameters->sublist("Pseudopotential").get<string>("UsingDoubleGrid") == "No"){
        Teuchos::RCP<Epetra_Vector> local_potential = Teuchos::rcp(new Epetra_Vector(Map));
        calculate_local(local_potential);
        for (int i=0; i<NumMyElements; i++)
            core_hamiltonian->InsertGlobalValues(MyGlobalElements[i], 1, &local_potential->operator[](i), &MyGlobalElements[i]);{
        }
    }
    else if(parameters->sublist("Pseudopotential").get<string>("UsingDoubleGrid")=="Yes"){
        //calculate_local_DG(core_hamiltonian);
    }
    ////////////// end /////////////////////////////////

    //construct_matrix(core_hamiltonian);

    end_time =clock();
    Verbose::single()<< std::endl;
    Verbose::single()<< "#------------------------------------------------- KBprojector::calculate_KB" << std::endl;
    Verbose::single()<< " Time to perform outer product for pseudopotential_matrix: " << double(end_time-start_time)/CLOCKS_PER_SEC << " s" << std::endl;
    Verbose::single()<< "#---------------------------------------------------------------------------" << std::endl;

    return;
}
*/
