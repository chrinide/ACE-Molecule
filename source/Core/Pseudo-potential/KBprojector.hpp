#pragma once
#include <string>
#include <vector>

#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"
#include "Epetra_CrsMatrix.h"
#include "Teuchos_RCP.hpp"
#include "Teuchos_ParameterList.hpp"

#include "../../Io/Atoms.hpp"
#include "../../Basis/Basis.hpp"
#include "Filter.hpp"
#include "../../Util/Time_Measure.hpp"

class KBprojector{
    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~KBprojector(){};

        virtual void read_pp()=0;
/**
 * @brief Construct pseudopotential matrix and add it to the core Hamiltonian matrix.
 * @author Kwangwoo Hong,Sunghwan Choi
 * @date 2014-08-14
 * @param core_hamiltonian The core Hamiltonian which is updated
 * */
//        void calculate_KB(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian);
        double get_ion_ion_correction();
        int get_core_electron_info(
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& density, 
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> >& density_grad
        );
        virtual void compute_nonlinear_core_correction() = 0;
        std::vector<double> get_Zvals();
        std::vector<int> get_projector_numbers();

//        void get_comp_charge_exps(std::vector<double>& comp_charge_exps);

        virtual std::vector< std::vector<int> > get_oamom()=0;
        virtual std::vector< std::vector<double> > get_input_EKB()=0;
        virtual std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl()=0;
        virtual std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_x()=0;
        virtual std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_y()=0;
        virtual std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_z()=0;
        virtual std::vector< std::vector< std::vector <std::vector < std::vector<int> > > > > get_V_index()=0;
        virtual std::vector< std::vector< std::vector<double> > > get_h_ij_total()=0;

        virtual std::vector< std::vector<double> > get_V_local()=0;
        //virtual std::vector<double> get_V_local()=0;
        virtual std::vector< std::vector<double> > get_V_local_dev()=0;
        virtual std::vector<double> get_local_cutoff()=0;
        virtual std::vector< std::vector<double> > get_input_mesh()=0;
        virtual std::vector< std::vector<double> > get_input_local()=0;
        virtual void get_vector_address(std::vector< std::vector<double> >& local_pp, std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > >& V_vector, std::vector<std::vector<std::vector<std::vector<std::vector<int> > > > >& V_index, std::vector< std::vector<double> >& EKB, std::vector< std::vector<int> >& oamom, std::vector<int>& number_vps_file) = 0;
        virtual void clear_vector()=0;

    protected:
        virtual void calculate_nonlocal(int iatom,std::array<double,3> position,
                                        std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_pp,
                                        std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_x,
                                        std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_y,
                                        std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_z,
                                        std::vector<std::vector<std::vector<std::vector<int> > > >& nonlocal_pp_index ) = 0;
        //virtual void calculate_local(Teuchos::RCP<Epetra_Vector>& local_potential) = 0;
        //virtual void calculate_local_DG(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian) = 0;

        //virtual void construct_matrix(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian) = 0;

        std::vector< std::vector<double> > local_pp_radial;  // itype, value
        std::vector< std::vector< std::vector< std::vector<double> > > >  nonlocal_pp_radial;   // itype, l, projector, value

        std::vector< std::vector< std::vector<double> > > nonlocal_cutoff;  //itype, l, projector

        std::vector<std::vector<double> > local_mesh; // radial mesh; itype, value
        std::vector<std::vector <double> > local_d_mesh; //radial mesh; itype, value
        std::vector<std::vector<std::vector<std::vector<double> > > > nonlocal_mesh; //radial mesh;  itype, l, projector, value
        std::vector<std::vector<std::vector<std::vector<double> > > > nonlocal_d_mesh; //radial mesh; itype, l, projector, value

        std::vector<std::vector<std::vector<std::vector<double> > > > new_nonlocal_mesh; //radial mesh;  itype, l, projector, value    
        std::vector< std::vector< std::vector< std::vector<double> > > >  new_nonlocal_pp_radial;   // itype, l, projector, value
        std::vector< std::vector< std::vector<double> > > new_nonlocal_cutoff;  //itype, l, projector

        std::vector<std::vector<int> >  projector_number; //itype, l

        std::vector< std::vector<int> > oamom;
        std::vector< std::vector<double> > EKB;
        std::vector< std::vector< std::vector<double> > > h_ij_total;

        Filter* filter;
        Teuchos::RCP<const Basis> mesh ;
        Teuchos::RCP<Teuchos::ParameterList> parameters;
        Teuchos::RCP<const Atoms> atoms;

        std::vector<int> number_vps_file,number_pao_file;
        std::vector<double> Zvals;
    
        std::vector< std::vector<double> > original_mesh;
        std::vector< std::vector<double> > original_d_mesh;
  
        Teuchos::RCP<Epetra_MultiVector> short_potential;
 
        Teuchos::RCP<Epetra_Vector> core_density;
        Teuchos::RCP<Epetra_MultiVector> core_density_grad;

        std::vector< std::vector<double> > local_pp;
        std::vector< std::vector<double> > local_pp_dev;
        Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure() );
};


