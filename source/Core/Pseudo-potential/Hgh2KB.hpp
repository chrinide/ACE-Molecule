#pragma once
#include <vector>
#include <string>
#include <algorithm>

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"

#include "KBprojector.hpp"


class Hgh2KB: public KBprojector {
  public:
    Hgh2KB(Teuchos::RCP<const Basis> mesh,Teuchos::RCP<const Atoms> atoms, Teuchos::RCP<Teuchos::ParameterList> parameters);

    void read_pp();

    void compute_nonlinear_core_correction();

    std::vector< std::vector<int> > get_oamom();
    std::vector< std::vector<double> > get_input_EKB();
    std::vector< std::vector<double> > get_V_local();
    std::vector< std::vector<double> > get_V_local_dev();
    std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl();
    std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_x();
    std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_y();
    std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_z();
    std::vector< std::vector< std::vector <std::vector < std::vector<int> > > > > get_V_index();
    //void get_vector_address(std::vector<double>& local_pp, std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > >& V_vector, std::vector<std::vector<std::vector<std::vector<std::vector<int> > > > >& V_index, std::vector< std::vector<double> >& EKB, std::vector< std::vector<int> >& oamom, std::vector<int>& number_vps_file) ;
    void get_vector_address(std::vector< std::vector<double> >& local_pp, std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > >& V_vector, std::vector<std::vector<std::vector<std::vector<std::vector<int> > > > >& V_index, std::vector< std::vector<double> >& EKB, std::vector< std::vector<int> >& oamom, std::vector<int>& number_vps_file) ;

    std::vector<double> get_local_cutoff();
    std::vector< std::vector<double> > get_input_mesh();
    std::vector< std::vector<double> > get_input_local();
    std::vector< std::vector< std::vector<double> > > get_h_ij_total();
    void clear_vector();

  protected:
    Teuchos::Array<std::string> hgh_filenames;
    std::string hgh_filename;
    //int** oamom;
    std::string type;
    std::string xc_type;

    // Variables for local pseudopotentials
    std::vector<double> rloc;
    std::vector<int> nloc;
    std::vector< std::vector<double> > c_i;

    std::vector<double> local_cutoff;
    std::vector< std::vector<double> > local_mesh;
    std::vector< std::vector<double> > short_potential_radial;

    // Variables for non-local pseudopotentials
    std::vector< std::vector<double> > r_l;
    std::vector< std::vector < std::vector<double> > > h_ij_total;
    std::vector< std::vector < std::vector<double> > > k_ij_total;

    std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > > V_vector;
    std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > > V_dev_x;
    std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > > V_dev_y;
    std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > > V_dev_z;
    std::vector<std::vector<std::vector<std::vector<std::vector<int> > > > > V_index;

    // Variables for nlcc
    std::vector<bool> nonlinear_core_correction;
    std::vector<double> rcore;
    std::vector<double> ccore;

    int read_pao(int itype);
    void pp_information(int itype);

    void initialize_pp();
    void initialize_parameters();

    std::vector<double> integrate(double r_l, int exponent, int axis, double position);
    //void calculate_local(Teuchos::RCP<Epetra_Vector>& local_potential);
    //void calculate_local_DG(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian);
    void calculate_nonlocal(int iatom,std::array<double,3> position,std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_pp,std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_x,std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_y,std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_z,std::vector<std::vector<std::vector<std::vector<int> > > >& nonlocal_pp_index );
    void construct_matrix(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian);

    void get_comp_potential(std::vector<double> mesh, int itype, std::vector<double>& comp_potential);
    double compute_Vlocal(int itype, double r);
    double compute_Vshort(int itype, double r);
    double sinc(double x);
    //std::vector<double> local_pp;

    //std::vector< std::vector<double> > local_pp;
    //std::vector< std::vector<double> > local_pp_dev;

    //from here, this part is added by jaechang
    void make_vectors();
    void make_local_pp();
    //void make_local_nonlocal_pp();
    void make_nonlocal_vectors();
    //void arrange_local_pp();
};
