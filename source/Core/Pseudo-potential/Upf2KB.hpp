#pragma once
#include <string>
#include <vector>
//#include <time.h>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_ParameterList.hpp"

#include "Epetra_Vector.h"
#include "KBprojector.hpp"


class Upf2KB: public KBprojector {
  public:
/**
@brief Read information from pseudopotential file(s).
@author Kwangwoo Hong
@date 2013-12-24
*/
    void read_pp();

/**
@brief Calculate local pseudopotential.
@author Kwangwoo Hong, Sunghwan Choi
@date 2013-12-24
@param local Local part of pseudopotential (The diagonal elements of the Hamiltonian matrix)
@param iatom Atom index for finding corresponding pseudopotential file
@param position The position of the atom
*/
    void local_potential(double* local,int iatom,double* position);

/**
@brief Calculate nonlocal pseudopotential.
@author Kwangwoo Hong
@date 2013-12-24
@param V_nonlocal Nonlocal part of pseudopotential (The diagonal and off-diagonal elements of the Hamiltonian matrix)
@param iatom Atom index for finding corresponding pseudopotential file
@param position The position of the atom
*/
//    void calculate_nonlocal(double**** V_nonlocal,int iatom,double* position);
    void nonlocal_potential(double**** V_nonlocal,int iatom,double* position);

/* // *
@brief Construct pseudopotential matrix and add it to the core Hamiltonian matrix.
@author Kwangwoo Hong
@date 2013-12-24
@param core_hamiltonian The core Hamiltonian which is updated
@param local Local part of pseudopotential (The diagonal elements of the Hamiltonian matrix)
@param V_nonlocal Nonlocal part of pseudopotential (The diagonal and off-diagonal elements of the Hamiltonian matrix)
@param number_vps_total The number of KB projectors per atom
*/
    //std::vector< std::vector<int> > get_oamom();
    //std::vector< std::vector<int> > get_n_l();

    std::vector< std::vector<int> > get_oamom();
    std::vector <int> get_number_vps_file();
    std::vector<double> get_local_cutoff();
    std::vector< std::vector<double> > get_input_mesh();
    std::vector< std::vector<double> > get_input_local();
    std::vector< std::vector<double> > get_input_EKB();
    std::vector< std::vector<double> > get_V_local();
    //std::vector<double> get_V_local();
    std::vector< std::vector<double> > get_V_local_dev();
    std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl();
    std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_x();
    std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_y();
    std::vector< std::vector< std::vector <std::vector < std::vector<double> > > > > get_V_nl_dev_z();
    std::vector< std::vector< std::vector <std::vector < std::vector<int> > > > > get_V_index();
    std::vector< std::vector< std::vector<double> > > get_h_ij_total();
    void get_vector_address(std::vector< std::vector <double> >& local_pp, std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > >& V_vector, std::vector<std::vector<std::vector<std::vector<std::vector<int> > > > >& V_index, std::vector< std::vector<double> >& EKB, std::vector< std::vector<int> >& oamom, std::vector<int>& number_vps_file) ;
    void clear_vector();

    //Upf2KB(Basis* basis,Grid_Setting* grid_setting,Atoms* atoms,Epetra_Map* map, Teuchos::Teuchos::RCP<Teuchos::ParameterList> parameters);
    
    Upf2KB(Teuchos::RCP<const Basis> mesh,Teuchos::RCP<const Atoms> atoms, Teuchos::RCP<Teuchos::ParameterList> parameters);

    void compute_nonlinear_core_correction();

  protected:
    std::string word;
    std::vector<int> mesh_size;
    void initialize_parameters();
    void initialize_pp();
    std::vector< std::vector< std::vector<double> > > pao;

    std::vector<double> local_cutoff;
    std::vector< std::vector<double> > local_mesh;
    std::vector< std::vector<int> > oamom;

//    std::vector< std::vector< double > > local_pp;
//    std::vector< std::vector<double> > local_pp_dev;

    std::vector< std::vector<double> > short_potential_radial;
    std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > > V_vector;
    std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > > V_dev_x;
    std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > > V_dev_y;
    std::vector<std::vector<std::vector<std::vector<std::vector<double> > > > > V_dev_z;
    std::vector<std::vector<std::vector<std::vector<std::vector<int> > > > > V_index;

    Teuchos::Array<std::string> upf_filenames;
    std::string upf_filename;
    double calculate_basis(int iatom,int i,int pp,int l,int m,double* position);
    bool type_new; // Upf pseudopotential type
    bool is_cal_local_dev = true; //not calculate local_pp_dev, when we want geometry optimization, this part should be changed
    bool is_cal_nonlocal_dev = true; //not calculate local_pp_dev, when we want geometry optimization, this part should be changed

    int read_pao(int itype);
    void pp_information(int itype);

    std::vector< std::vector<int> > n_l;
    std::vector< std::vector< std::vector<double> > > fitting_parameters;

    std::vector< std::vector<double> > nonlinear_core_correction_mesh; // radial mesh; itype, value
    std::vector< std::vector<double> > nonlinear_core_correction_radial; // radial mesh; itype, value
    std::vector<bool> nonlinear_core_correction;

    void get_comp_potential(std::vector<double> mesh, int itype, std::vector<double>& comp_potential);
    //void calculate_local(Teuchos::RCP<Epetra_Vector>& local_potential);
    //void calculate_local_DG(Teuchos::Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian);
    //void calculate_local_DG(Teuchos::Teuchos::RCP<Epetra_CrsMatrix> core_hamiltonian);
    void calculate_nonlocal(int iatom,std::array<double,3> position,
                                std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_pp,
                                std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_x,
                                std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_y,
                                std::vector<std::vector<std::vector<std::vector<double> > > >& nonlocal_dev_z,
                                std::vector<std::vector<std::vector<std::vector<int> > > >& nonlocal_pp_index );
    //void construct_matrix(Teuchos::Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian);

    //from here, this part is added by jaechang
    void make_vectors(); 
    void make_local_pp(); 
    //void make_local_nonlocal_pp(); 
    void make_nonlocal_vectors();
    //void arrange_local_pp();
    int fine_dimension;
};

