//#include <chrono>
#include "Paw.hpp"
#include <iostream>
#include <cmath>

#include "../../Basis/Create_Basis.hpp"
#include "../../Util/Verbose.hpp"
#include "../../Util/Parallel_Manager.hpp"
#include "../../Util/Parallel_Util.hpp"
#include "../../Util/Tricubic_Interpolation.hpp"
#include "../../Util/String_Util.hpp"
//#include "../../Util/Trilinear_Interpolation.hpp"
//#include "../../Compute/Create_Compute.hpp"

using std::vector;
using std::string;
using std::abs;
using Teuchos::Array;
using Teuchos::ParameterList;
using Teuchos::RCP;
using Teuchos::rcp;

Paw::Paw(RCP<const Basis> mesh, RCP<const Atoms> atoms, RCP<ParameterList> parameters){
    this -> mesh = mesh;
    this -> parameters = parameters;
    this -> atoms=atoms;
    this -> initialize_parameters();
    this -> initialize_finegrid();
    this -> read_all();
}

RCP<Paw_Atom> Paw::get_paw_atom(int iatom){
    return this -> paw_atoms.at(iatom);
}

RCP<Paw_Species> Paw::get_paw_species(int itype){
    return this -> paw_species.at(itype);
}

void Paw::initialize_parameters(){
    if(!parameters->sublist("BasicInformation").sublist("Pseudopotential").isParameter("PSFilenames")){
        Verbose::all() << "There are no PSFilenames in input file." << std::endl;
        exit(EXIT_FAILURE);
    }else{
        if(parameters->sublist("BasicInformation").sublist("Pseudopotential").get<string>("Format") == "xml"){
            Verbose::single(Verbose::Normal) << "GPAW-setup xml type of paw dataset is used" << std::endl;
            type_xml=true;
        } else if(parameters->sublist("BasicInformation").sublist("Pseudopotential").get<string>("Format") == "gz") {
            Verbose::all() << "If you use gzip compressed dataset, please decompress it." << std::endl;
            Verbose::all() << "Decompression not implemented yet." << std::endl;
            type_xml=false;
            exit(EXIT_FAILURE);
        } else {
            Verbose::all() << "Wrong PAW dataset file format: Pseudopotential.Format is wrong" << std::endl;
            exit(EXIT_FAILURE);
        }
        paw_filenames=parameters->sublist("BasicInformation").sublist("Pseudopotential").get< Array<std::string> >("PSFilenames");
    }

    parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<double>("OverlapCutoff", 1.0E-7);

    this -> spin_size = parameters -> sublist("BasicInformation").get<int>("Polarize")+1;
    this -> num_electrons = (int)parameters -> sublist("BasicInformation").get<double>("NumElectrons");
    //this -> new_hamiltonian_structure = Teuchos::null;
    return;
}

void Paw::initialize_finegrid(){
    const int * cpoints = this -> mesh -> get_points();
    const double * cscaling = this -> mesh -> get_scaling();
    int* points = new int[3];
    double * scaling = new double[3];
    int multipliers = parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("FineGrid", 2);

    for(int i = 0; i < 3; ++i){
        scaling[i] = cscaling[i]/multipliers;
        points[i] = multipliers*(cpoints[i]-1)+1;
    }

    //*
    RCP<ParameterList> fine_param = rcp( new ParameterList( parameters -> sublist("BasicInformation") ) );
    //RCP<ParameterList> fine_param = rcp( new ParameterList() );
    fine_param -> sublist("BasicInformation") = parameters -> sublist("BasicInformation");
    fine_param -> set("ScalingX", scaling[0]);
    fine_param -> set("ScalingY", scaling[1]);
    fine_param -> set("ScalingZ", scaling[2]);
    fine_param -> set("PointX", points[0]);
    fine_param -> set("PointY", points[1]);
    fine_param -> set("PointZ", points[2]);
    fine_param -> set("AllowOddPoints", 1);
    // */

    //this -> fine_mesh = rcp( new Basis(basis, basis, grid_setting, this->mesh -> get_map()) );
    Verbose::set_numformat(Verbose::Pos);
    this -> fine_mesh = Create_Basis::Create_Basis( *this -> mesh -> get_map() -> Comm().Clone(), fine_param, rcp( new Atoms(*this -> atoms) ) );
    Verbose::single(Verbose::Normal) << "============== PAW fine mesh ==============" << std::endl;
    Verbose::single(Verbose::Normal) << *this -> fine_mesh;
    Verbose::single(Verbose::Normal) << "===========================================" << std::endl << std::endl;

    //RCP<Teuchos::ParameterList> hartree_param = rcp( new Teuchos::ParameterList() );
    //this -> fine_poisson = Create_Compute::Create_Poisson(this -> fine_mesh, hartree_param);

    delete[] points;
    delete[] scaling;
}

void Paw::read_all(){
    this -> paw_species.resize(atoms->get_num_types());
    for(int itype=0;itype < this -> atoms->get_num_types();itype++){
        this -> paw_species.at(itype) = rcp( new Paw_Species( this -> paw_filenames[itype], this -> type_xml ) );
    }
    this -> set_paw_element_types();
    this -> atom_to_element_list = this -> map_atom_to_element();

    this -> check_sphere_overlap();

    int fine_proj_degree = 1;
    if( this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").isParameter("FineProj") ){
        fine_proj_degree = this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("FineProj");
        if( fine_proj_degree < 1 ) fine_proj_degree = 1;
    }
    /*
    int lmax = -1;
    if( this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").isParameter("Lmax") ){
        lmax = this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential").get<int>("Lmax");
    }
    */

    RCP<ParameterList> addi_params = rcp( new ParameterList( this -> parameters -> sublist("BasicInformation").sublist("Pseudopotential") ) );

    this -> paw_atoms.resize(atoms->get_size());
    for(int itype=0;itype < this -> atoms->get_size();itype++){
        Verbose::single(Verbose::Detail) << "PAW_ATOM #" << itype << " initialization:" << std::endl;
        addi_params -> set<string>("ID", this -> parameters -> sublist("BasicInformation").get<string>("InpName") + "." + String_Util::to_string(itype));
        this -> paw_atoms.at(itype) = rcp(new Paw_Atom(
            this -> paw_species[this->atom_to_element_list[itype]],
            this -> atoms -> get_positions()[itype],
            this -> spin_size,
            this -> mesh, this -> fine_mesh, addi_params
        ) );
        Verbose::single(Verbose::Detail) << "PAW_ATOM #" << itype << " initialization END" << std::endl;
    }

    Verbose::single(Verbose::Detail) << "Paw_Atom construction complete" << std::endl;

    this -> PAW_D_matrixes.clear();
    this -> PAW_D_matrixes.resize( this -> atoms -> get_positions().size() );

    RCP<Time_Measure> timer = rcp(new Time_Measure());
    timer -> start("PAW::Initial fine hartree potential");
    this -> fine_hartree_vector = rcp( new Epetra_Vector( *this -> fine_mesh -> get_map() ) );
    for(int i = 0; i < this -> atoms -> get_size(); ++i){
        this -> fine_hartree_vector -> Update( 1.0, *this -> paw_atoms[i] -> get_initial_hartree_potential(true), 1.0 );
    }
    timer -> end("PAW::Initial fine hartree potential");

    Verbose::single(Verbose::Detail) << "Calculating overlap operator" << std::endl;
    this -> calculate_overlap_matrix();
    Verbose::single(Verbose::Normal) << "PAW ready" << std::endl;
    this -> print_reference_energy();
    Verbose::set_numformat(Verbose::Time);
    timer -> print(Verbose::single(Verbose::Simple));
    return;
}

void Paw::set_paw_element_types(){
    this -> paw_element_types.clear();
    for(int i = 0; i < this -> paw_species.size(); ++i){
        this -> paw_element_types.push_back( this -> paw_species[i] -> get_atomic_number() );
    }
}

vector<int> Paw::map_atom_to_element(){
    vector<int> atom_list = this -> atoms -> get_atomic_numbers();
    vector<int> paw_list = this -> paw_element_types;
    vector<int> pawmap;
    vector<int>::iterator it;

    for(int i = 0; i < atom_list.size(); ++i ){
        it = find( paw_list.begin(), paw_list.end(), atom_list[i] );
        if( it == paw_list.end() ){
            Verbose::all() << atom_list[0] << ", " << paw_list[0] << std::endl;
            Verbose::all() << "PAW list and atom type list not match!" << std::endl;
            exit(EXIT_FAILURE);
        }
        pawmap.push_back(it-paw_list.begin());
    }
    return pawmap;
}

int Paw::get_overlap_matrix( RCP<Epetra_CrsMatrix> &overlap_matrix ){
    overlap_matrix = this -> overlap_matrix;
    return 0;
}

int Paw::get_energy_correction(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > orbitals,
    RCP<Epetra_Vector> &hartree_vector,
    double &hartree_energy,
    double &int_n_vxc,
    double &x_energy,
    double &c_energy,
    double &kinetic_energy,
    std::vector<double> &kinetic_energies,
    double &zero_energy,
    double &external_energy
){
    vector<int> atom_list = this -> atoms -> get_atomic_numbers();

    double local_hartree_energy = 0.0;
    double scaling = this -> mesh -> get_scaling()[0] * this -> mesh -> get_scaling()[1] * this -> mesh -> get_scaling()[2];
    int NumMyElements = this -> mesh -> get_map() -> NumMyElements();
    Array< RCP<Epetra_Vector> > core_density = this -> get_atomcenter_core_density(false);

    for(int s = 0; s < occupations.size(); ++s){
        for(int j = 0; j < NumMyElements; ++j){
            for(int i = 0; i < orbitals[s] -> NumVectors(); ++i){
                local_hartree_energy += 0.5 * occupations[s]->operator[](i) * orbitals[s]->operator[](i)[j] * orbitals[s]->operator[](i)[j] * hartree_vector->operator[](j);
            }
        }
    }
    Parallel_Util::group_sum(&local_hartree_energy, &hartree_energy, 1);
    for(int s = 0; s < occupations.size(); ++s){
        local_hartree_energy = 0.0;
        core_density[s] -> Dot( *hartree_vector, &local_hartree_energy );
        hartree_energy += 0.5 * scaling * local_hartree_energy;
    }

    hartree_energy += this -> get_Hartree_energy_correction();

    Verbose::single(Verbose::Simple) << "PAW smooth energy contribution" << std::endl;
    Verbose::single(Verbose::Simple) << "----------------------------------------" << std::endl;
    Verbose::single(Verbose::Simple) << " PS Kinetic           = " << std::setprecision(10) << kinetic_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS Electrostatic     = " << std::setprecision(10) << hartree_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS XC                = " << std::setprecision(10) << x_energy+c_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS X                 = " << std::setprecision(10) << x_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS C                 = " << std::setprecision(10) << c_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " PS external          = " << std::setprecision(10) << external_energy << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << "----------------------------------------" << std::endl;

    for(int i = 0; i < atom_list.size(); ++i){
        double atom_hartree_energy = 0, atom_int_n_vxc = 0, atom_x_energy = 0;
        double atom_c_energy = 0, atom_zero_energy = 0, atom_external_energy = 0;
        std::vector<double> atom_kinetic_energies(occupations.size());
        this -> paw_atoms[i] -> get_energy_correction(
            this -> PAW_D_matrixes[i],
            atom_hartree_energy, atom_int_n_vxc, atom_x_energy,
            atom_c_energy, atom_kinetic_energies, atom_zero_energy,
            atom_external_energy
        );
        hartree_energy += atom_hartree_energy;
        int_n_vxc += atom_int_n_vxc;
        x_energy += atom_x_energy;
        c_energy += atom_c_energy;
        for(int s = 0; s < occupations.size(); ++s){
            kinetic_energy += atom_kinetic_energies[s];
            kinetic_energies[s] += atom_kinetic_energies[s];
        }
        zero_energy += atom_zero_energy;
        external_energy += atom_external_energy;
    }

    return 3;
}

int Paw::get_density_correction(Array< RCP<Epetra_Vector> > &density){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_Vector> > atom_density_corr = this -> paw_atoms[i] -> get_density_correction( this -> PAW_D_matrixes[i] );
        for(int s = 0; s < spin_size; ++s){
            density[s] -> Update( 1.0, *atom_density_corr[s], 1.0 );
        }
    }

    return 3;
}

int Paw::get_orbital_correction(Array< RCP<Epetra_MultiVector> > &orbitals){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_MultiVector> > atom_orbital_corr;
        this -> paw_atoms[i] -> get_orbital_correction( orbitals, atom_orbital_corr );
        for(int s = 0; s < spin_size; ++s){
            orbitals[s] -> Update( 1.0, *atom_orbital_corr[s], 1.0 );
        }
    }

    return 3;
}

void Paw::calculate_hamiltonian_correction_coeff(
    vector< vector< vector< vector<double> > > > &dH_coeff
){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    Verbose::single(Verbose::Normal) << "Updating Hamiltonian correction" << std::endl;
    if( this -> fine_hartree_vector == Teuchos::null ){
        Verbose::all() << "calculate_hamiltonian_correction: hartree vector is not updated" << std::endl;
        exit(EXIT_FAILURE);
        return;
    }

    dH_coeff.clear();
    dH_coeff.resize(this -> spin_size);
    for(int i = 0; i < atom_pos.size(); ++i){
        vector< vector< vector<double> > > dH_coeff_atom = this -> paw_atoms[i] -> get_Hamiltonian_correction_matrix( this -> fine_hartree_vector, this -> PAW_D_matrixes[i] );
        for(int s = 0; s < this -> spin_size; ++s){
            for(int j1 = 0; j1 < dH_coeff_atom[s].size(); ++j1){
                for(int j2 = 0; j2 < dH_coeff_atom[s][j1].size(); ++j2){
                    if(!std::isfinite(dH_coeff_atom[s][j1][j2])){
                        Verbose::all() << "dH_coeff of spin " << s << ", atom " << i << ", indicies ("<< j1 << "," << j2 << ") = " << dH_coeff_atom[s][j1][j2] << std::endl;
                        Verbose::all() << "PAW atomcenter density matrix of spin " << s << ", atom " << i << ", indicies ("<< j1 << "," << j2 << ") = " << this -> PAW_D_matrixes[i][s](j1,j2) << std::endl;
                        throw std::runtime_error("Hamotonian correction coefficient is strange!");
                    }
                }
            }
            dH_coeff[s].push_back( dH_coeff_atom[s] );
        }
    }
    return;
}

void Paw::update_PAW_D_matrix(
    Array< RCP<const Occupation> > occupations,
    Array< RCP<const Epetra_MultiVector> > wavefunctions
){
    vector<std::array<double,3> > positions = this -> atoms -> get_positions();
    this -> PAW_D_matrixes.clear();
    this -> PAW_D_matrixes.resize( positions.size() );

    Verbose::single(Verbose::Detail) << "Updating PAW atomcenter density matrix" << std::endl;
    for(int i = 0; i < positions.size(); ++i ){
        this -> PAW_D_matrixes[i] = this -> paw_atoms[i] -> get_one_center_density_matrix( occupations, wavefunctions );
        Verbose::single(Verbose::Simple) << "PAW Atom " << i << " orbital occupancy" << std::endl;
        this -> paw_atoms[i] -> print_orbital_occupancy(occupations, wavefunctions);
    }
}

void Paw::initialize_PAW_D_matrix( ){
    vector<std::array<double,3> > positions = this -> atoms -> get_positions();
    this -> PAW_D_matrixes.clear();
    this -> PAW_D_matrixes.resize( positions.size() );

    Verbose::single(Verbose::Detail) << "Initializing PAW atomcenter density matrix" << std::endl;
    vector<int> val_electrons;
    int num_e = this -> num_electrons;
    int current_tot_num_e = 0;
    for(int i = 0; i < positions.size(); ++i ){
        val_electrons.push_back( this -> paw_atoms[i] -> get_paw_species() -> get_read_paw() -> get_valence_electron_number() );
        current_tot_num_e += val_electrons.back();
    }
    /*
    vector<int> neutral_val_electrons( val_electrons );
    while( num_e < current_tot_num_e ){
        --std::max_element( val_electrons.begin(), val_electrons.end() );
        --current_tot_num_e;
    }
    while( num_e > current_tot_num_e ){
        ++std::max_element( val_electrons.begin(), val_electrons.end() );
        ++current_tot_num_e;
    }
    // */
    for(int i = 0; i < positions.size(); ++i ){
        double charge = static_cast<double>(current_tot_num_e - num_e) / positions.size();
        this -> PAW_D_matrixes[i] = this -> paw_atoms[i] -> get_one_center_initial_density_matrix(charge);
    }
    this -> PAW_D_matrixes_history_in.push_back( this -> PAW_D_matrixes );
}

RCP<Epetra_Vector> Paw::get_zero_potential(){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    RCP<Epetra_Vector> zero_potential = rcp( new Epetra_Vector( * this -> mesh -> get_map(), true) );

    for(int i = 0; i < atom_pos.size(); ++i){
        zero_potential -> Update( 1.0, *this -> paw_atoms[i] -> get_zero_potential(), 1.0 );
    }
    return zero_potential;
}

Array< RCP<Epetra_Vector> > Paw::get_zero_potential_gradient(bool is_fine/* = false*/){
    RCP<const Basis> omesh;
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    if(is_fine){
        omesh = this -> fine_mesh;
    } else {
        omesh = this -> mesh;
    }

    Array< RCP<Epetra_Vector> > zero_potential_grad;
    for(int d = 0; d < 3; ++d){
        zero_potential_grad.append(rcp( new Epetra_Vector( *omesh -> get_map(), true) ));
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_Vector> > atomic_zero_pot_grad = this -> paw_atoms[i] -> get_zero_potential_gradient();
        for(int d = 0; d < 3; ++d){
            zero_potential_grad[d] -> Update( 1.0, *atomic_zero_pot_grad[d], 1.0 );
        }
    }
    return zero_potential_grad;
}

RCP<Epetra_Vector> Paw::get_compensation_charge(
        bool is_fine/* = false*/
){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    RCP<Epetra_Vector> comp_charge;
    if( is_fine ){
        comp_charge = rcp( new Epetra_Vector( * this -> fine_mesh -> get_map(), true) );
    } else {
        comp_charge = rcp( new Epetra_Vector( * this -> mesh -> get_map(), true) );
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        if( is_fine ){
            comp_charge -> Update( 1.0, *this -> paw_atoms[i] -> get_compensation_charge( this -> PAW_D_matrixes[i], true), 1.0 );
        } else {
            comp_charge -> Update( 1.0, *this -> paw_atoms[i] -> get_compensation_charge( this -> PAW_D_matrixes[i] ), 1.0 );
        }
    }
    // ~0.06s

    return comp_charge;
}

void Paw::calculate_overlap_matrix(){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    int size = this -> mesh -> get_map() -> NumGlobalElements();
    double tolerance=this->parameters->sublist("BasicInformation").sublist("Pseudopotential").get<double>("OverlapCutoff") ;
    Verbose::set_numformat(Verbose::Scientific);
    Verbose::single(Verbose::Normal) << "Overlap tolerance = " << tolerance << std::endl;

    RCP<Epetra_CrsGraph> S_structure = rcp( new Epetra_CrsGraph( Copy, *this -> mesh -> get_map(), 0 ) );

    for(int i = 0; i < size; ++i){
        int indices = i;
        S_structure -> InsertGlobalIndices(i, 1, &indices);
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        Teuchos::SerialDenseMatrix<int,double> S_correction = this -> paw_atoms.at(i) -> get_overlap_matrix();
        vector< vector<double> > proj_coeffs;
        vector< vector<int> > proj_inds;
        this -> paw_atoms.at(i) -> get_projector_coeffs_and_inds(proj_coeffs, proj_inds);
        int stateno = proj_inds.size();

        vector<double> proj_coeff_max;
        for(int ind = 0; ind < stateno; ++ind){
            double max_val = *std::max_element(proj_coeffs[ind].begin(), proj_coeffs[ind].end());
            double min_val = *std::min_element(proj_coeffs[ind].begin(), proj_coeffs[ind].end());
            proj_coeff_max.push_back( std::max(abs(max_val), abs(min_val) ) );
        }

        for(int index1 = 0; index1 < stateno; ++index1){
            for(int index2 = 0; index2 < stateno; ++index2){
                if( abs(S_correction(index1,index2)) * proj_coeff_max[index1]*proj_coeff_max[index2] > tolerance ){
                    for(int ind1 = 0; ind1 < proj_inds[index1].size(); ++ind1 ){
                        int a_index = proj_inds[index1][ind1];
                        if( this -> mesh -> get_map() -> MyGID(a_index) ){
                            vector<int> index;
                            index.reserve(proj_inds[index2].size());

                            for(int ind2 = 0; ind2 < proj_inds[index2].size(); ++ind2 ){
                                double calcval = S_correction(index1,index2) * proj_coeffs[index1][ind1] * proj_coeffs[index2][ind2];
                                if( abs(calcval) > tolerance ){
                                    index.push_back( proj_inds[index2][ind2] );
                                }
                            }
                            int ierr = S_structure -> InsertGlobalIndices(a_index, index.size(), &index[0]);
                            if( ierr != 0 ){
                                Verbose::all() << "ERROR: Paw CrsGraph construction " << ierr << std::endl;
                                exit(EXIT_FAILURE);
                            }
                        }// if( map -> MyGID(a_index) )
                    }
                }
            }// index2
        }// index1
//        */
    }

    S_structure -> FillComplete();

    overlap_matrix = rcp( new Epetra_CrsMatrix( Copy, *S_structure) );
    //int dim = overlap_matrix -> NumGlobalRows();

    for(int j = 0; j < this -> mesh -> get_map() -> NumMyElements(); ++j){
        double val = 1.0;
        int ind = this -> mesh -> get_map() -> MyGlobalElements()[j];
        int ierr = overlap_matrix -> SumIntoGlobalValues( ind, 1.0, &val, &ind );
        if( ierr != 0 ){
            Verbose::all() << "calc overlap matrix InsertGlobalValues error: " << ierr << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        Teuchos::SerialDenseMatrix<int,double> S_correction = this -> paw_atoms.at(i) -> get_overlap_matrix();
        vector< vector<double> > proj_coeffs;
        vector< vector<int> > proj_inds;
        this -> paw_atoms.at(i) -> get_projector_coeffs_and_inds(proj_coeffs, proj_inds);
        int stateno = proj_inds.size();

        for(int index1 = 0; index1 < stateno; ++index1){
            for(int index2 = 0; index2 < stateno; ++index2){
                for(int ind1 = 0; ind1 < proj_inds[index1].size(); ++ind1 ){
                    int a_index = proj_inds[index1][ind1];
                    if( this -> mesh -> get_map() -> MyGID(a_index) ){
                        vector<double> value;
                        vector<int> index;
                        value.reserve(proj_inds[index2].size());
                        index.reserve(proj_inds[index2].size());

                        for(int ind2 = 0; ind2 < proj_inds[index2].size(); ++ind2 ){
                            double calcval = S_correction(index1,index2) * proj_coeffs[index1][ind1] * proj_coeffs[index2][ind2];
                            if( abs(calcval) > tolerance ){
                                value.push_back( calcval );
                                index.push_back( proj_inds[index2][ind2] );
                            }
                        }
                        int ierr = overlap_matrix -> SumIntoGlobalValues(a_index, value.size(), &value[0], &index[0]);
                        //if( ierr < 0 ){
                        if( ierr != 0 ){
                            Verbose::all() << "ERROR: Paw_Atom S matrix: " << ierr << std::endl;
                            exit(EXIT_FAILURE);
                        }
                    }// if( map -> MyGID(a_index) )
                }
            }// index2
        }// index1
    }

    overlap_matrix -> FillComplete();
    overlap_matrix -> OptimizeStorage();

    Verbose::set_numformat(Verbose::Scientific);
    Verbose::single(Verbose::Normal) << "PAW overlap matrix sparsity: "
             << static_cast<double>(overlap_matrix->NumGlobalNonzeros())/static_cast<double>(this->mesh->get_original_size())/this->mesh->get_original_size()
             << ", identity matrix sparsity: "
             << 1.0/static_cast<double>(this->mesh->get_original_size())
             << " (" << static_cast<double>(overlap_matrix->NumGlobalNonzeros())/static_cast<double>(this->mesh->get_original_size()) << " times)"
             << std::endl;
}

double Paw::get_Hartree_energy_correction(){
    double scaling = this -> fine_mesh -> get_scaling()[0] * this -> fine_mesh -> get_scaling()[1] * this -> fine_mesh -> get_scaling()[2];
    double energy = 0.0;
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    RCP<Epetra_Vector> comp_charge = this -> get_compensation_charge(true);
    this -> fine_hartree_vector -> Dot( *comp_charge, &energy );
    energy *= scaling;

    energy *= 0.5;

    return energy;// Most scaling-dependent part.
}

void Paw::hartree_vector_update(
    //Array< RCP<Epetra_Vector> > ps_density,
    RCP<Epetra_Vector> &hartree_vector
){
    clock_t st, et;
    Parallel_Manager::info().all_barrier();
    st = clock();
    this -> fine_hartree_vector = rcp( new Epetra_Vector( *this -> fine_mesh -> get_map() ) );

    Interpolation::Tricubic::interpolate(mesh, hartree_vector, fine_mesh, this->fine_hartree_vector);
    //Interpolation::Trilinear::interpolate(mesh, hartree_vector, fine_mesh, this->fine_hartree_vector);
    /*
    Array< RCP<Epetra_Vector> > fine_ps_density;
    for(int s = 0; s < ps_density.size(); ++s){
        fine_ps_density.push_back( rcp( new Epetra_Vector( *this -> fine_mesh -> get_map() ) ) );
        Trilinear_Interpolation::interpolate(mesh, ps_density[s], fine_mesh, fine_ps_density[s]);
    }
    double hartree_energy;
    this -> fine_poisson -> compute(fine_ps_density, this -> fine_hartree_vector, hartree_energy);
    */
    this -> fine_hartree_vector -> Update( 1.0, *this->get_Hartree_potential_from_comp_charge(true), 1.0 );

    for(int i = 0; i < this -> atoms -> get_positions().size(); ++i){
        Array< RCP< Epetra_Vector> > core_pot = this -> paw_atoms[i] -> get_smooth_core_potential( true );
        for(int s = 0; s < core_pot.size(); ++s){
            this -> fine_hartree_vector -> Update( 1.0, *core_pot[s], 1.0 );
        }

        core_pot = this -> paw_atoms[i] -> get_smooth_core_potential( false );
        for(int s = 0; s < core_pot.size(); ++s){
            hartree_vector -> Update( 1.0, *core_pot[s], 1.0 );
        }
    }

    hartree_vector -> Update( 1.0, *this -> get_Hartree_potential_from_comp_charge(false), 1.0);
    Parallel_Manager::info().all_barrier();
    et = clock();
    Verbose::single(Verbose::Simple) << "PAW: hartree_vector_update time = " << static_cast<double>(et-st)/CLOCKS_PER_SEC << "s" << std::endl;
}

RCP<Epetra_Vector> Paw::get_Hartree_potential_from_comp_charge(
        bool is_fine/* = false*/
){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    RCP<Epetra_Vector> hartree_potential;
    if( is_fine ){
        hartree_potential = rcp( new Epetra_Vector( * this -> fine_mesh -> get_map(), true) );
    } else {
        hartree_potential = rcp( new Epetra_Vector( * this -> mesh -> get_map(), true) );
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        if( is_fine ){
            hartree_potential -> Update( 1.0, *this -> paw_atoms[i] -> get_compensation_charge_Hartree_potential( this -> PAW_D_matrixes[i], true ), 1.0 );
        } else {
            hartree_potential -> Update( 1.0, *this -> paw_atoms[i] -> get_compensation_charge_Hartree_potential( this -> PAW_D_matrixes[i], false ), 1.0 );
        }
    }

    return hartree_potential;
}

std::vector<double> Paw::get_total_compensation_charges(){
    size_t atom_size = this -> atoms -> get_size();
    vector<double> charge(atom_size);
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    for(int i = 0; i < atom_size; ++i){
        charge[i]= this -> paw_atoms[i] -> get_total_compensation_charge(this -> PAW_D_matrixes[i]);
    }

    return charge;
}

Array< RCP<Epetra_Vector> > Paw::get_atomcenter_core_density(
    bool is_ae
){
    Array< RCP<Epetra_Vector> > core_density;
    for(int s = 0; s < spin_size; ++s){
        core_density.push_back( rcp( new Epetra_Vector( *this -> mesh -> get_map(), true ) ) );
    }
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_Vector> > atom_core_density = this -> paw_atoms[i] -> get_core_density_vector(is_ae);
        for(int s = 0; s < spin_size; ++s){
            core_density[s] -> Update( 1.0, *atom_core_density[s], 1.0 );
        }
    }

    return core_density;
}

Array< RCP<Epetra_Vector> > Paw::get_atomcenter_density(bool is_ae ){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    Array< RCP<Epetra_Vector> > density;
    for(int s = 0; s < spin_size; ++s){
        density.push_back(rcp( new Epetra_Vector( * this -> mesh -> get_map() ) ) );
    }

    for(int i = 0; i < atom_pos.size(); ++i){
        Array< RCP<Epetra_Vector> > atom_density = this -> paw_atoms[i] -> get_atomcenter_density_vector( is_ae, this -> PAW_D_matrixes[i] );
        for(int s = 0; s < spin_size; ++s){
            density[s] -> Update( 1.0, *atom_density[s], 1.0 );
        }
    }

    return density;
}

void Paw::check_sphere_overlap(){
    vector<std::array<double,3> > atom_pos = this -> atoms -> get_positions();
    for(int itype = 0; itype < this -> atoms->get_size(); itype++){
        for(int jtype = itype+1; jtype < this -> atoms->get_size(); jtype++){
            double distance = this -> atoms -> distance(itype, jtype);
            //double rc1 = this -> paw_species[this->atom_to_element_list[itype]] -> get_projector_cutoff();
            //double rc2 = this -> paw_species[this->atom_to_element_list[jtype]] -> get_projector_cutoff();
            //double rc1 = this -> paw_species[this->atom_to_element_list[itype]] -> get_cutoff_radius();
            //double rc2 = this -> paw_species[this->atom_to_element_list[jtype]] -> get_cutoff_radius();
            double rc1 = this -> paw_species[this->atom_to_element_list[itype]] -> get_partial_wave_cutoff();
            double rc2 = this -> paw_species[this->atom_to_element_list[jtype]] -> get_partial_wave_cutoff();
            if( distance < rc1 + rc2 ){
                Verbose::single(Verbose::Simple) << "!!!!!!!!!!!! PAW spheres are overlapping !!!!!!!!!!!!" << std::endl;
                Verbose::single(Verbose::Simple) << "distance = " << distance << std::endl;
                Verbose::single(Verbose::Simple) << "atom " << itype << " rc = " << rc1 << std::endl;
                Verbose::single(Verbose::Simple) << "atom " << jtype << " rc = " << rc2 << std::endl;
            }
        }
    }
}

void Paw::density_matrix_mixing(
    std::vector<double> mixing_coeffs,
    double mixing_alpha,
    int ispin
){
    vector<std::array<double,3> > positions = this -> atoms -> get_positions();

    //Verbose::single(Verbose::Detail) << "Matrix history before = " << this -> PAW_D_matrixes_history_in.size() << " and " << this -> PAW_D_matrixes_history_out.size() << " (spin " << ispin << ")" << std::endl;
    if( ispin == 0 ){
        this -> PAW_D_matrixes_history_in.push_back( this -> PAW_D_matrixes );
        this -> PAW_D_matrixes_history_out.push_back( this -> PAW_D_matrixes );
    }

    if( this -> PAW_D_matrixes_history_in.size() > 0 ){
        for(int a = 0; a < positions.size(); ++a ){
            this -> PAW_D_matrixes[a][ispin].putScalar(0.0);
        }
        for(int a = 0; a < positions.size(); ++a ){
            for(int i = 0; i < mixing_coeffs.size(); ++i){
                int index = this -> PAW_D_matrixes_history_in.size() - mixing_coeffs.size()+i-1;
                //Verbose::single(Verbose::Detail) << "index = " << index << std::endl;
                //int index = i;
                Teuchos::SerialDenseMatrix<int,double> temp = this -> PAW_D_matrixes_history_in[index][a][ispin];
                temp.scale( mixing_coeffs[i]*mixing_alpha);
                this -> PAW_D_matrixes[a][ispin] += temp;
                Teuchos::SerialDenseMatrix<int,double> temp2 = this -> PAW_D_matrixes_history_out[index][a][ispin];
                temp2.scale( mixing_coeffs[i]*(1-mixing_alpha));
                this -> PAW_D_matrixes[a][ispin] += temp2;
            }
            this -> PAW_D_matrixes_history_in.back()[a][ispin] = this -> PAW_D_matrixes[a][ispin];
        }
    }
    //Verbose::single(Verbose::Detail) << "Matrix history = " << this -> PAW_D_matrixes_history_in.size() << " and " << this -> PAW_D_matrixes_history_out.size() << " (spin " << ispin << ")" << std::endl;

}

void Paw::print_PAW_D_matrix(){
    vector<std::array<double,3> > positions = this -> atoms -> get_positions();
    if( Parallel_Manager::info().get_mpi_rank() == 0 ){
        std::ios oldstate(nullptr);
        oldstate.copyfmt(Verbose::all());
        Verbose::all() << "D MATRIX" << std::endl;
        for(int a = 0; a < positions.size(); ++a){
            Verbose::all() << "atom " << a << std::endl;
            for(int s = 0; s < PAW_D_matrixes[a].size(); ++s){
                Verbose::all() << "spin " << s << std::endl;
                for(int i = 0; i < PAW_D_matrixes[a][s].numRows(); ++i){
                    for(int j = 0; j < PAW_D_matrixes[a][s].numCols(); ++j){
                        Verbose::all() << std::fixed << std::setw(8) << std::setprecision(5) << this -> PAW_D_matrixes[a][s](i,j) << "\t";
                    }
                    Verbose::all() << std::endl;
                }
                Verbose::all() << std::endl;
            }
            Verbose::all() << std::endl;
        }
        Verbose::all() << std::endl;
        Verbose::all().copyfmt(oldstate);
    }
}

vector< Array< Teuchos::SerialDenseMatrix<int,double> > > Paw::get_density_matrixes(){
    return this -> PAW_D_matrixes;
}

void Paw::set_density_matrixes( vector< Array< Teuchos::SerialDenseMatrix<int, double> > > D_matrixes ){
    this -> PAW_D_matrixes = D_matrixes;
}

void Paw::print_reference_energy(){
    double ref_kinetic = 0.0;
    double ref_potential = 0.0;
    //double ref_external = 0.0; // Does not support, yet.
    double ref_xc = 0.0;
    //double ref_local = 0.0;
    for(int itype=0;itype < this -> atoms->get_size();itype++){
        ref_kinetic += this -> paw_species[this->atom_to_element_list[itype]] -> get_read_paw() -> get_ae_kinetic_energy();
        ref_potential += this -> paw_species[this->atom_to_element_list[itype]] -> get_read_paw() -> get_ae_electrostatic_energy();
        //ref_external += this -> paw_species[this->atom_to_element_list[itype]] -> get_external_correction_scalar();
        ref_xc += this -> paw_species[this->atom_to_element_list[itype]] -> get_read_paw() -> get_ae_xc_energy();
        //ref_local += this -> paw_species[this->atom_to_element_list[itype]] -> get_read_paw() -> get_ae_kinetic_energy();
    }
    Verbose::set_numformat(Verbose::Energy);
    Verbose::single(Verbose::Simple) << "----------------------------------------" << std::endl;
    Verbose::single(Verbose::Simple) << " Reference Kinetic       = " << std::setprecision(10) << ref_kinetic << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " Reference Electrostatic = " << std::setprecision(10) << ref_potential << " Ha" << std::endl;
    //Verbose::single(Verbose::Simple) << " Reference External = " << ref_external << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " Reference XC            = " << std::setprecision(10) << ref_xc << " Ha" << std::endl;
    //Verbose::single(Verbose::Simple) << " Reference Local = " << ref_local << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << " Reference Total         = " << std::setprecision(10) << ref_kinetic+ref_potential+ref_xc << " Ha" << std::endl;
    Verbose::single(Verbose::Simple) << "----------------------------------------" << std::endl;
}

RCP<Epetra_Vector> Paw::get_fine_hartree_potential() const{
    return rcp(new Epetra_Vector(*this -> fine_hartree_vector));
}

RCP<const Basis> Paw::get_fine_basis(){
    return this -> fine_mesh;
}
