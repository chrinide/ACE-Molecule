#include "Paw_Util.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include "../../Util/Verbose.hpp"
#include "../../Util/String_Util.hpp"

using std::vector;
using std::endl;
using std::ifstream;
using std::make_pair;
using std::string;
using Teuchos::Array;

Array< Teuchos::SerialDenseMatrix<int,double> > Paw_Util::read_matrix( string filename, int spin_size, bool double_off_diagonal ){
    Verbose::all() << "Reading " << filename << endl;
    ifstream input;
    input.open( filename.c_str() );
    string line;
    vector<double> val_list;

    while( getline(input, line) ){
        vector<string> word = String_Util::strSplit( line, " " );
        for(int i = 0; i < word.size(); ++i){
            val_list.push_back( ::atof( word[i].c_str() ) );
        }
    }

    int size = static_cast<int>( sqrt(1+8*val_list.size()/spin_size)-1 )/2;
    Array< Teuchos::SerialDenseMatrix<int,double> > matrixes;
    for(int s = 0; s < spin_size; ++s){
        Teuchos::SerialDenseMatrix<int,double> matrix(size, size);
        for(int i1 = 0; i1 < val_list.size()/spin_size; ++i1){
            int i = s * val_list.size()/2 + i1;
            std::pair<int,int> index = Paw_Util::unpack_index(size, i1);
            if( index.first == index.second ){
                matrix(index.first, index.second) = val_list[i];
            } else {
                if( double_off_diagonal){
                    matrix(index.first, index.second) = val_list[i]/2;
                    matrix(index.second, index.first) = val_list[i]/2;
                } else {
                    matrix(index.first, index.second) = val_list[i];
                    matrix(index.second, index.first) = val_list[i];
                }
            }
        }
        matrixes.push_back( matrix );
    }
    input.close();

    return matrixes;
}

int Paw_Util::pack_index(int n, int i, int j){
    if( j >= i ){
        return j + n * i - i*(i+1)/2;
    } else {
        return i + n * j - j*(j+1)/2;
    }
}

std::pair<int, int> Paw_Util::unpack_index(int n, int ind){
    int i = floor( ( 2*n+1 - sqrt( (2*n+1)*(2*n+1) - 8*ind ) ) / 2 );
    int j = ind - n*i + i*(i+1)/2;

    return make_pair(i, j);
}
