#pragma once
#include <vector>

#include "Teuchos_RCP.hpp"
#include "Epetra_MultiVector.h"

#include "../../Io/Atoms.hpp"
#include "../../Basis/Basis.hpp"

class Filter {

    public:
        /**
        @breif Constructor of filter class

        */
        Filter(Teuchos::RCP<const Basis> mesh, Teuchos::RCP<const Atoms> atoms, std::vector<double> Zvals);
        /**
            filter function for local potential 
        */
        int filter_local(std::vector<double>original_f,std::vector<double>r_g,std::vector<double>dr_g, double rcut, std::vector<double>& new_f,std::vector<double>& new_mesh);

        /**
            filter function for non-local potentials, which 
        */
        int filter_nonlocal(std::vector<double>original_f,std::vector<double>r_g,std::vector<double>dr_g,int l,double nonlocal_cutoff,std::vector<double>& new_f,std::vector<double>& new_mesh);

        /**
            @param local_pp_radial
        */
        void optimize_comp_charge_exps(std::vector<std::vector<double> > local_pp_radial,std::vector<std::vector<double> > original_mesh);
        int get_comp_potential(std::vector<double> mesh, std::vector<double>& return_val,int itype);
        void get_comp_charge_exps(std::vector<double>& comp_charge_exps2);

    protected:
        std::vector<double> Zvals;

        void compute(double target_scaling, std::vector<double> original_f, std::vector<double> r_g, std::vector<double> dr_g, int l, std::vector<double> new_r_g, std::vector<double> new_dr_g,std::vector<double> q_i,double dq_i,double q_cut, double rc2, std::vector<double>& new_f);

//        Teuchos::RCP<Epetra_MultiVector> comp_charge;
//        Teuchos::RCP<Epetra_MultiVector> comp_potential;
        Teuchos::RCP<Epetra_MultiVector> local_potential;
//        int compute_comp_charge_and_potential();
        Teuchos::RCP<const Basis> mesh;
        Teuchos::RCP<const Atoms> atoms;
        std::vector<double> comp_charge_exps;
};


