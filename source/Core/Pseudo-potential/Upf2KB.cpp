#include "Upf2KB.hpp"
#include <cmath>
#include "Filter.hpp"
#include "EpetraExt_MatrixMatrix.h"
#include "Epetra_Comm.h"
#include "Epetra_Map.h"

#include "../../Basis/Basis_Function/Basis_Function.hpp"
#include "../../Basis/Basis_Function/Sinc.hpp"
#include "../../Basis/Grid_Setting/Grid_Setting.hpp"
#include "../../Basis/Grid_Setting/Grid_Atoms.hpp"

#include "../../Util/String_Util.hpp"
#include "../../Util/Spherical_Harmonics.hpp"
#include "../../Util/Spherical_Harmonics_Derivative.hpp"
#include "../../Util/Read_Upf.hpp"
#include "../../Util/Double_Grid.hpp"
#include "../../Util/Linear_Interpolation.hpp"
#include "../../Util/Spline_Interpolation.hpp"
#include "../../Util/Numerical_Derivatives.hpp"
#include "../../Util/Verbose.hpp"
#include "../../Util/Parallel_Manager.hpp"
#include "../../Util/Parallel_Util.hpp"
#include "../../Util/LoopUtil.hpp"

#ifdef ACE_HAVE_OMP
#include "omp.h"
#endif

using std::abs;
using std::string;
using std::vector;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
/*
#include <ctime>
void pause(int dur)
{
int temp = time(NULL) + dur;
while(temp > time(NULL));
}
*/
using Interpolation::Spline::splint;
using Numerical_Derivatives::numerical_derivatives;

Upf2KB::Upf2KB(RCP<const Basis> mesh,RCP<const Atoms> atoms, RCP<Teuchos::ParameterList> parameters){
    this->mesh = mesh;
    this->parameters=Teuchos::sublist(parameters,"BasicInformation");
    this->atoms=atoms;

    initialize_parameters();
    read_pp();  // read information from pseudopotential file; fill out all values related to radial mesh
    initialize_pp();
    make_vectors();
}

void Upf2KB::initialize_parameters(){
    upf_filenames=parameters->sublist("Pseudopotential").get< Array<string> >("PSFilenames");

    parameters->sublist("Pseudopotential").get<double>("NonlocalThreshold",1E-6);
    parameters->sublist("Pseudopotential").get<double>("LocalThreshold",1E-7);
    parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid", 0);
    parameters->sublist("Pseudopotential").get<int>("UsingFiltering", 0);

    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1){
        this -> fine_dimension = parameters->sublist("Pseudopotential").get<int>("FineDimension", 3);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - FineDimension = " << this -> fine_dimension << std::endl;

        parameters->sublist("Pseudopotential").get<string>("FilterType", "Sinc");
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - FilterType = " << parameters->sublist("Pseudopotential").get<string>("FilterType") << std::endl;

        if(parameters->sublist("Pseudopotential").get<string>("FilterType")=="Lagrange"){
            parameters->sublist("Pseudopotential").get<int>("InterpolationOrder", 8);
            Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - Using Lagrange Filter function of order-" << parameters->sublist("Pseudopotential").get<int>("InterpolationOrder") << std::endl;
        }

        parameters->sublist("Pseudopotential").get<double>("LocalRgauss", 1.0);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - LocalRgauss = " << parameters->sublist("Pseudopotential").get<double>("LocalRgauss") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("ShortDecayTol", 0.0001);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - ShortDecayTol = " << parameters->sublist("Pseudopotential").get<double>("ShortDecayTol") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange", 1.25);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - LocalIntegrationRange = " << parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange") << std::endl;

        parameters->sublist("Pseudopotential").get<double>("NonlocalRmax", 2.0);
        Verbose::single(Verbose::Normal) << "Upf2KB::initialize_parameters - NonlocalRmax = " << parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") << std::endl;
    }
    if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
        parameters->sublist("Pseudopotential").get<double>("NonlocalRmax", 2.0);
    }

    return;
}

void Upf2KB::read_pp(){

    //    local_mesh.clear();
    //    short_potential_radial.clear();
    for(int itype=0;itype<atoms->get_num_types();itype++){
        // Read header
        double Zval_tmp = 0.0;
        int number_vps = 0, number_pao = 0, mesh_size_tmp = 0;
        bool core_correction = false;

        //Read_Upf::read_header(upf_filenames[itype], &Zval_tmp, &number_vps, &number_pao, &mesh_size_tmp);
        Read::Upf::read_header(upf_filenames[itype], &Zval_tmp, &number_vps, &number_pao, &mesh_size_tmp, &core_correction);

        Zvals.push_back(Zval_tmp);
        number_vps_file.push_back(number_vps);
        number_pao_file.push_back(number_pao);
        mesh_size.push_back(mesh_size_tmp);
        // end

        // Read mesh points
        vector<double> original_mesh_tmp;
        vector<double> original_d_mesh_tmp;

        //shchoi
        original_mesh_tmp = Read::Upf::read_upf(upf_filenames[itype], "PP_R");
        original_d_mesh_tmp = Read::Upf::read_upf(upf_filenames[itype], "PP_RAB");

        if(mesh_size[itype] != original_mesh_tmp.size()){
            Verbose::all()<< "Upf2KB::read_pp - The number of mesh points from PP_HEADER & PP_R is different." << std::endl;
            Verbose::all()<< mesh_size[itype] << "\t!=\t" << original_mesh_tmp.size() << std::endl;
            Verbose::all()<< "Upf2KB::original mesh " <<std::endl;
            for (int i=0;original_mesh_tmp.size();i++ ){
                Verbose::all()<< "r\t" <<original_mesh_tmp[i] <<std::endl;
            }
            exit(-1);
        }

        original_mesh.push_back(original_mesh_tmp);
        original_d_mesh.push_back(original_d_mesh_tmp);
        Verbose::single(Verbose::Detail) << "original_mesh_tmp size: " << original_mesh_tmp.size() <<std::endl;
        local_mesh.push_back(original_mesh_tmp);
        Verbose::single(Verbose::Detail) << "local_mesh size: " << local_mesh.size() <<std::endl;
        local_d_mesh.push_back(original_d_mesh_tmp);
        /*
           for (int k=0;k<original_mesh_tmp.size();k++){
           Verbose::single(Verbose::Detail)<< original_mesh_tmp[k] <<std::endl;
           }
           */
        // Read local pseudopotential
        vector<double> local_pp_radial_tmp;

        //int local_pp_radial_tmp_size = 0;

        local_pp_radial_tmp = Read::Upf::read_upf(upf_filenames[itype], "PP_LOCAL");
        // Unit conversion from Ryd to Hartree
        for(int t=0;t<local_pp_radial_tmp.size();t++){
            local_pp_radial_tmp[t] *= 0.5;
        }
        //end

        //local_pp_radial_tmp_size = local_pp_radial_tmp.size();
        local_pp_radial.push_back(local_pp_radial_tmp);

        if(local_pp_radial[itype].size()!=local_mesh[itype].size()){
            Verbose::all() <<"size of local term is not equal to the size of local mesh" << std::endl;
            Verbose::all() << "size of original_mesh_tmp: " << original_mesh_tmp.size() << std::endl;
            Verbose::all() << "size of local_pp_radial: " << local_pp_radial[itype].size() << std::endl;
            Verbose::all() << "size of local_mesh: " <<local_mesh[itype].size() << std::endl;
            Verbose::all() << "itype " << itype  << std::endl;
            for (int k=0;k<local_mesh[itype].size();k++){
                Verbose::all() << local_mesh[itype][k] << std::endl;
            }
            exit(-1);
        }
        // end

        // Read nonlinear core correction
        vector<double> nonlinear_core_correction_mesh_tmp;
        vector<double> nonlinear_core_correction_radial_tmp;

        if(core_correction == true){
            Verbose::single(Verbose::Normal) << "Upf2KB: found nonlinear core correction for atom type " << itype << std::endl;
            nonlinear_core_correction_radial_tmp = Read::Upf::read_upf(upf_filenames[itype], "PP_NLCC");
            for(int i=0; i<nonlinear_core_correction_radial_tmp.size(); i++){
                nonlinear_core_correction_mesh_tmp.push_back(original_mesh_tmp[i]);
            }

            // Linear extrapolation at r=0
            double tmp = (nonlinear_core_correction_radial_tmp[1] - nonlinear_core_correction_radial_tmp[0]) / (nonlinear_core_correction_mesh_tmp[1] - nonlinear_core_correction_mesh_tmp[0]) * (0.0 - nonlinear_core_correction_mesh_tmp[0]) + nonlinear_core_correction_radial_tmp[0];

            nonlinear_core_correction_mesh_tmp.insert(nonlinear_core_correction_mesh_tmp.begin(), 0.0);
            nonlinear_core_correction_radial_tmp.insert(nonlinear_core_correction_radial_tmp.begin(), tmp);
            // end
            /*
               for(int i=0; i<nonlinear_core_correction_radial_tmp.size(); i++){
               cout << nonlinear_core_correction_mesh_tmp[i] << " " << nonlinear_core_correction_radial_tmp[i] << endl;
               }
               exit(-1);
               */
            nonlinear_core_correction.push_back(true);
        }
        else{
            Verbose::single(Verbose::Normal) << "Upf2KB: no nonlinear core correction for atom type " << itype << std::endl;
            nonlinear_core_correction.push_back(false);
        }

        nonlinear_core_correction_mesh.push_back(nonlinear_core_correction_mesh_tmp);
        nonlinear_core_correction_radial.push_back(nonlinear_core_correction_radial_tmp);
        // end

        // Read EKB
        vector<double> EKB_tmp;

        //int EKB_tmp_size = 0;

        EKB_tmp = Read::Upf::read_ekb(upf_filenames[itype], number_vps);

        // Unit conversion from Ryd^-1 to Hartree^-1
        for(int i=0;i<EKB_tmp.size();i++){
            EKB_tmp[i] *= 2.0;
        }
        // end

        //EKB_tmp_size = EKB_tmp.size();

        EKB.push_back(EKB_tmp);
        // end

        // Read nonlocal pseudopotential
        vector< vector < vector<double> > > nonlocal_mesh_tmp;
        vector< vector < vector<double> > > nonlocal_d_mesh_tmp;
        if(number_vps > 0){
            vector< vector< vector<double> > > nonlocal_pp_radial_tmp;
            vector< vector<double> > nonlocal_cutoff_tmp;
            vector<int> oamom_tmp;

            nonlocal_pp_radial_tmp = Read::Upf::read_nonlocal(upf_filenames[itype], number_vps, oamom_tmp);
            oamom.push_back(oamom_tmp);

            for(int i=0;i<number_vps;i++){
                vector< vector<double> > nonlocal_mesh_tmp_tmp;
                vector< vector<double> > nonlocal_d_mesh_tmp_tmp;
                vector<double> nonlocal_mesh_tmp_tmp_tmp;
                vector<double> nonlocal_d_mesh_tmp_tmp_tmp;
                vector<double> nonlocal_cutoff_tmp_tmp;
                // 1. Change the value that I want to.
                for(int t=0;t<nonlocal_pp_radial_tmp[i][0].size();t++){
                    nonlocal_pp_radial_tmp[i][0][t] = 0.5*nonlocal_pp_radial_tmp[i][0][t] / original_mesh[itype][t];// Unit conversion from Ryd to Hartree
                    nonlocal_mesh_tmp_tmp_tmp.push_back(original_mesh[itype][t]);
                    nonlocal_d_mesh_tmp_tmp_tmp.push_back(original_d_mesh[itype][t]);
                }

                // 3. Cut the small or weird values at the end of the array.
                while(fabs(nonlocal_pp_radial_tmp[i][0][nonlocal_pp_radial_tmp[i][0].size()-1]) < 1.0E-30){
                    nonlocal_pp_radial_tmp[i][0].pop_back();
                    nonlocal_mesh_tmp_tmp_tmp.pop_back();
                    nonlocal_d_mesh_tmp_tmp_tmp.pop_back();
                }

                nonlocal_cutoff_tmp_tmp.push_back(nonlocal_mesh_tmp_tmp_tmp[nonlocal_mesh_tmp_tmp_tmp.size()-1]);
                nonlocal_mesh_tmp_tmp.push_back(nonlocal_mesh_tmp_tmp_tmp);
                nonlocal_d_mesh_tmp_tmp.push_back(nonlocal_d_mesh_tmp_tmp_tmp);

                nonlocal_mesh_tmp.push_back(nonlocal_mesh_tmp_tmp);
                nonlocal_d_mesh_tmp.push_back(nonlocal_d_mesh_tmp_tmp);
                nonlocal_cutoff_tmp.push_back(nonlocal_cutoff_tmp_tmp);
            }
            nonlocal_pp_radial.push_back(nonlocal_pp_radial_tmp);
            nonlocal_cutoff.push_back(nonlocal_cutoff_tmp);
        }
        else{
            nonlocal_pp_radial.push_back( vector< vector< vector<double> > >() );
            nonlocal_cutoff.push_back( vector< vector<double> >() );
            oamom.push_back(vector<int>());
        }
        // end
        nonlocal_mesh.push_back(nonlocal_mesh_tmp);
        nonlocal_d_mesh.push_back(nonlocal_d_mesh_tmp);

        ///////////// calcualte projector number ////////////
        vector<int> projector_number_tmp;
        for(int j=0;j<number_vps;j++){
            projector_number_tmp.push_back(1);
        }
        projector_number.push_back(projector_number_tmp);
        ////////////          end        /////////////////////

        pp_information(itype);
    }
    return;
}

void Upf2KB::compute_nonlinear_core_correction(){
    int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements =mesh->get_map()->NumMyElements();

    core_density = rcp(new Epetra_Vector(*mesh->get_map()));
    core_density_grad = rcp(new Epetra_MultiVector(*mesh->get_map(),3) );

    core_density -> PutScalar(0.0);
    core_density_grad -> PutScalar(0.0);

    const double** scaled_grid = mesh->get_scaled_grid();
    vector<std::array<double,3> > positions = atoms->get_positions();

    for(int iatom=0; iatom<atoms->get_size(); iatom++){
        int itype = atoms->get_atom_type(iatom);

        if(nonlinear_core_correction[itype] == true){
            double rcut = nonlinear_core_correction_mesh[itype][nonlinear_core_correction_mesh[itype].size() - 1];
            /*
               if(MyPID == 0){
               cout.precision(8);
               cout << scientific;
               for(int i=0; i<nonlinear_core_correction_radial[itype].size(); i++){
               cout << nonlinear_core_correction_mesh[itype][i] << "  " << nonlinear_core_correction_radial[itype][i] << endl;
               }
               exit(-1);
               }
               */
            // Spline interpolation
            int core_correction_size = nonlinear_core_correction_radial[itype].size();
            vector<double> y2;
            for(int i=0; i<nonlinear_core_correction_radial[itype].size(); i++){
                double tmp = Numerical_Derivatives::numerical_derivatives(nonlinear_core_correction_mesh[itype][i], nonlinear_core_correction_mesh[itype], nonlinear_core_correction_radial[itype], 2, 5);
                y2.push_back(tmp);
            }

            for(int i=0; i<NumMyElements; i++){
                int i_x=0, i_y=0, i_z=0;
                mesh->decompose(MyGlobalElements[i], &i_x, &i_y, &i_z);

                double x = scaled_grid[0][i_x] - positions[iatom][0];
                double y = scaled_grid[1][i_y] - positions[iatom][1];
                double z = scaled_grid[2][i_z] - positions[iatom][2];
                double r = sqrt(x*x + y*y + z*z);

                if(r < rcut){
                    double tmp = splint(nonlinear_core_correction_mesh[itype], nonlinear_core_correction_radial[itype], y2, core_correction_size, r);
                    double tmp_grad = numerical_derivatives(r, nonlinear_core_correction_mesh[itype], nonlinear_core_correction_radial[itype], 1, 5);

                    core_density->SumIntoMyValue(i, 0, tmp);
                    core_density_grad->SumIntoMyValue(i, 0, tmp_grad * x / r ); // x-axis
                    core_density_grad->SumIntoMyValue(i, 1, tmp_grad * y / r ); // y-axis
                    core_density_grad->SumIntoMyValue(i, 2, tmp_grad * z / r ); // z-axis
                }
            }
        }
    }

    double numerical_core_charge = 0.0;
    core_density->Norm1(&numerical_core_charge);
    numerical_core_charge *= mesh->get_scaling()[0] * mesh->get_scaling()[1] * mesh->get_scaling()[2];

    Verbose::single().precision(12);
    Verbose::single() << std::fixed ;
    Verbose::single(Verbose::Normal) << std::endl;
    Verbose::single(Verbose::Normal) << "#------------------------------- Upf2KB::compute_nonlinear_core_correction" << std::endl;
    //cout << " Analytic core charge  = " << analytic_core_charge << endl;
    Verbose::single(Verbose::Normal) << " Numerical core charge = " << numerical_core_charge << std::endl;
    Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
    Verbose::single().precision(6);
    Verbose::single() << std::scientific;

    return;
}
/*
void Upf2KB::calculate_local(Teuchos::RCP<Epetra_Vector>& local_potential){
    timer->start("Upf2KB::calculate_local");
    int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements = mesh->get_map()->NumMyElements();
    double threshold=parameters->sublist("Pseudopotential").get<double>("LocalThreshold");
    for (int iatom=0; iatom<atoms->get_size(); iatom++){
        for (int i=0; i < NumMyElements; i++){
            //local_pp[iatom][i] += total_value;
            local_potential->operator[](i) = local_pp[iatom][MyGlobalElements[i]];
        }
    }
    //cout << *core_hamiltonian << endl;
    //exit(-1);
    timer->end("Upf2KB::calculate_local");
    Verbose::single(Verbose::Normal)<< std::endl;
    Verbose::single(Verbose::Normal)<< "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
    Verbose::single(Verbose::Normal)<< " Time to compute double-grid integration: " << timer->get_elapsed_time("Upf2KB::calculate_local",-1) << " s" << std::endl;
    Verbose::single(Verbose::Normal)<< "#---------------------------------------------------------------------------" << std::endl;
    return;
}
*/
void Upf2KB::get_comp_potential(vector<double> radial_mesh, int itype, vector<double>& comp_potential){
    vector<std::array<double,3> > positions =atoms->get_positions();
    const double** scaled_grid = mesh->get_scaled_grid();
    const double* scaling = mesh->get_scaling();

    comp_potential.clear();

    double r,scaled_r, r_gauss;
    r_gauss = parameters->sublist("Pseudopotential").get<double>("LocalRgauss");
    double comp_norm = 0.0;
    for (int i=0;i< radial_mesh.size();i++){
        r=radial_mesh[i];
        scaled_r = r/r_gauss;
        comp_norm += Zvals[itype]/pow(sqrt(M_PI)*r_gauss,3)*exp(-scaled_r*scaled_r);
        comp_potential.push_back( Zvals[itype]*erf(scaled_r)/r );
    }

    comp_norm = 0.0;
    for (int j=0; j<mesh->get_original_size(); j++){
        int j_x, j_y, j_z=0;
        double x, y, z, r;
        mesh->decompose(j, &j_x, &j_y, &j_z);
        x = scaled_grid[0][j_x];
        y = scaled_grid[1][j_y];
        z = scaled_grid[2][j_z];
        r = sqrt(x*x + y*y + z*z);
        scaled_r = r/r_gauss;
        if(r<=local_cutoff[itype]){
            scaled_r = r/r_gauss;
            comp_norm += Zvals[itype]/pow(sqrt(M_PI)*r_gauss,3)*exp(-scaled_r*scaled_r);
        }
    }
    comp_norm*=scaling[0]*scaling[1]*scaling[2];

    return;
}

void Upf2KB::calculate_nonlocal(int iatom,std::array<double,3> position,
                                    vector<vector<vector<vector<double> > > >& nonlocal_pp,
                                    vector<vector<vector<vector<double> > > >& nonlocal_dev_x,
                                    vector<vector<vector<vector<double> > > >& nonlocal_dev_y,
                                    vector<vector<vector<vector<double> > > >& nonlocal_dev_z,
                                    vector<vector<vector<vector<int> > > >& nonlocal_pp_index )
{

    int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
    int NumMyElements = mesh->get_map()->NumMyElements();
    int size = mesh->get_original_size();
    const double** scaled_grid = mesh->get_scaled_grid();
    int itype = atoms->get_atom_type(iatom);
    double threshold=parameters->sublist("Pseudopotential").get<double>("NonlocalThreshold");

    auto check_xyzr = [this,&MyGlobalElements,&scaled_grid,&position]
                                (int i, double& x, double& y, double& z, double& r)->void {
                                int i_x; int i_y; int i_z;
                                mesh->decompose(MyGlobalElements[i], &i_x, &i_y,&i_z);
                                x = scaled_grid[0][i_x] - position[0];
                                y = scaled_grid[1][i_y] - position[1];
                                z = scaled_grid[2][i_z] - position[2];
                                r = std::sqrt( x*x + y*y +z*z);
                                return;
                                };


    {//initialize vectors
        nonlocal_pp = vector< vector< vector<vector<double> > > >(number_vps_file[itype]  );
        nonlocal_pp_index = vector< vector< vector<vector<int> > > >(number_vps_file[itype]  );
        if(is_cal_nonlocal_dev){
            nonlocal_dev_x = vector< vector< vector<vector<double> > > >(number_vps_file[itype] );
            nonlocal_dev_y = vector< vector< vector<vector<double> > > >(number_vps_file[itype] );
            nonlocal_dev_z = vector< vector< vector<vector<double> > > >(number_vps_file[itype] );
        }
    }


    /*  initialize case and factor
        case_ == 0 : UsingDoubleGrid=0  & UsingFiltering = 1
                     factor =1
        case_ == 1 : UsingDoubleGrid=0  & UsingFiltering = 1
                     factor = NonlocalRmax
        case_ == 2 : UsingDoubleGrid=1
                     factor = NonlocalRmax
    */
    double factor = 1.0;
    int case_=0;

    Verbose::single(Verbose::Simple) << std::endl;
    Verbose::single(Verbose::Simple) << "#---------------------------------------------- Upf2KB::calculate_nonlocal" << std::endl;
    if (parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0){
        if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
            factor=parameters->sublist("Pseudopotential").get<double>("NonlocalRmax");
            case_=1;
            Verbose::single(Verbose::Simple) << " Using filtering to represent pseudopotential" << std::endl;
        }
        else{
            Verbose::single(Verbose::Simple) << " Using single-grid to represent pseudopotential" << std::endl;
        }
    }
    else if (parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1) {
        factor  = parameters->sublist("Pseudopotential").get<double>("NonlocalRmax");
        case_=2;
        Verbose::single(Verbose::Simple) << " Using double-grid to represent pseudopotential" << std::endl;
    }
    else{
        Verbose::all() << "Invalid UsingDoubleGrid value " << parameters -> sublist("Pseudopotential").get<int>("UsingDoubleGrid") << std::endl;
        exit(-1);
    }
    Verbose::single(Verbose::Simple) << " Nonlocal threshold = " << threshold << std::endl;
    Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" <<std::endl;
    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0){
        timer->start("Upf2KB::shchoi");

        vector< std::function<bool(int)> > filter_functions;
        for(int i =0;i<number_vps_file[itype];i++){ // angular momentum index
            int l = oamom[itype][i]; //angular momentum
            nonlocal_pp[i].resize(2*l+1);
            nonlocal_pp_index[i].resize(2*l+1);
            if(is_cal_nonlocal_dev){
                nonlocal_dev_x[i].resize(2*l+1);
                nonlocal_dev_y[i].resize(2*l+1);
                nonlocal_dev_z[i].resize(2*l+1);
            }
            for(int m=0;m<2*l+1;m++){ //  magnetic momentum index
                nonlocal_pp[i][m].resize(projector_number[itype][i]);
                nonlocal_pp_index[i][m].resize(projector_number[itype][i]);
                if(is_cal_nonlocal_dev){
                    nonlocal_dev_x[i][m].resize(projector_number[itype][i]);
                    nonlocal_dev_y[i][m].resize(projector_number[itype][i]);
                    nonlocal_dev_z[i][m].resize(projector_number[itype][i]);
                }
                for (int p=0; p< projector_number[itype][i]; p++){ // number of projector index
                    double rcut = new_nonlocal_cutoff[itype][i][p]*factor;
                    auto check_distance = [rcut,check_xyzr](int i)->bool {
                                                double x; double y; double z; double r;
                                                check_xyzr(i,x,y,z,r);
                                                return rcut>r;
                                                };
                    filter_functions.push_back(check_distance);
                }
            }
        }

        std::vector<std::vector<int> > indices(filter_functions.size());
        LoopUtil::filter(NumMyElements,filter_functions,indices);
        int loop_index=0;
        for(int i =0;i<number_vps_file[itype];i++){ // angular momentum index
            int l = oamom[itype][i]; //angular momentum
            for(int m=0;m<2*l+1;m++){ //  magnetic momentum index
                for (int p=0; p< projector_number[itype][i]; p++){ // number of projector index
                    vector<double> input_radial,input_radial_mesh;
                    switch(case_){
                        case (0):
                            // No filtering, no doublegrid.
                            input_radial = new_nonlocal_pp_radial[itype][i][p];
                            input_radial_mesh = new_nonlocal_mesh[itype][i][p];
                            { // insert value at r=0.0
                            double value_for_origin = (new_nonlocal_pp_radial[itype][i][p][1] - new_nonlocal_pp_radial[itype][i][p][0]) / (new_nonlocal_mesh[itype][i][p][1] - new_nonlocal_mesh[itype][i][p][0]) * (0.0 - new_nonlocal_mesh[itype][i][p][0]) + new_nonlocal_pp_radial[itype][i][p][0];
                            input_radial_mesh.insert(input_radial_mesh.begin(), 0.0);
                            input_radial.insert(input_radial.begin(), value_for_origin);
                            }

                        break;
                        case(1):
                            /// @todo Check that we should not add origin values.
                            filter->filter_nonlocal(new_nonlocal_pp_radial[itype][i][p],
                                                    new_nonlocal_mesh[itype][i][p],
                                                    nonlocal_d_mesh[itype][i][p],
                                                    l, new_nonlocal_cutoff[itype][i][p]*factor,
                                                    input_radial, input_radial_mesh);

                        break;
                        case(2):
                            // Doublegrid. But since if(..."UsingDoubleGrid"...) here, it does not work.
                        default:
                            Verbose::all() << "Filtering/doublegrid option case_ = " << case_ << std::endl;
                            throw std::logic_error("Invalid filtering/doublegrid option!");
                        break;
                    }
                    //std::cout << "514514 1 new_nonlocal_pp_mesh" << itype << i << p << ": " << new_nonlocal_mesh[itype][i][p][0] << std::endl;
                    const int mesh_size = input_radial_mesh.size();
                    vector<double> y2(mesh_size);

                    // Spline interpolation
                    #ifdef ACE_HAVE_OMP
                    #pragma omp parallel for
                    #endif
                    for(int j=0; j<mesh_size; j++){
                        y2[j] = numerical_derivatives(input_radial_mesh[j], input_radial_mesh, input_radial, 2, 5);
                    }

                    const int loop_size = indices[loop_index].size();

                    nonlocal_pp[i][m][p].resize(loop_size);
                    nonlocal_pp_index[i][m][p].resize(loop_size);
                    if(is_cal_nonlocal_dev){
                        nonlocal_dev_x[i][m][p].resize(loop_size);
                        nonlocal_dev_y[i][m][p].resize(loop_size);
                        nonlocal_dev_z[i][m][p].resize(loop_size);
                    }

                    Parallel_Manager::info().all_barrier();
                    #ifdef ACE_HAVE_OMP
                    #pragma omp parallel for
                    #endif
                    for(int j=0; j<loop_size; j++){
                        double x,y,z,r;
                        int j_x,j_y,j_z;
                        double dev_spherical_value_x; double dev_spherical_value_y; double dev_spherical_value_z;
                        double value;
                        double value_x; double value_y; double value_z;
                        int original_j = MyGlobalElements[indices[loop_index][j]];
                        mesh->decompose(original_j,&j_x,&j_y,&j_z);
                        check_xyzr(indices[loop_index][j],x,y,z,r);
                        double spherical_value = Spherical_Harmonics::Ylm(l, m-l, x, y, z);
                        double denominator = mesh->compute_basis(j_x,scaled_grid[0][j_x],0)
                                        * mesh->compute_basis(j_y,scaled_grid[1][j_y],1)
                                        * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ;
                        if(is_cal_nonlocal_dev){
                            dev_spherical_value_x = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,0);
                            dev_spherical_value_y = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,1);
                            dev_spherical_value_z = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,2);
                        }
                        if(r < 1.0E-10){
                            value = spherical_value * input_radial[0] / denominator ;
                            if(is_cal_nonlocal_dev){
                                value_x = dev_spherical_value_x * input_radial[0] / denominator ;
                                value_y = dev_spherical_value_y * input_radial[0] / denominator ;
                                value_z = dev_spherical_value_z * input_radial[0] / denominator ;
                            }
                        }
                        else {
                            double radial_value = splint(input_radial_mesh, input_radial, y2, mesh_size, r);
                            value = spherical_value * radial_value / denominator ;
                            if(is_cal_nonlocal_dev){
                                double der_value = numerical_derivatives(r,input_radial_mesh, input_radial, 1, 3);
                                value_x = (dev_spherical_value_x * radial_value + spherical_value * der_value*x/r) / denominator ;
                                value_y = (dev_spherical_value_y * radial_value + spherical_value * der_value*y/r) / denominator ;
                                value_z = (dev_spherical_value_z * radial_value + spherical_value * der_value*z/r) / denominator ;
                            }
                        }
                        nonlocal_pp[i][m][p][j] =value;
                        nonlocal_pp_index[i][m][p][j] = original_j;
                        if(is_cal_nonlocal_dev){
                            nonlocal_dev_x[i][m][p][j] =value_x;
                            nonlocal_dev_y[i][m][p][j] =value_y;
                            nonlocal_dev_z[i][m][p][j] =value_z;
                        }
                    }
                    Parallel_Util::group_gather_vector(nonlocal_pp_index[i][m][p]);
                    Parallel_Util::group_gather_vector(nonlocal_pp[i][m][p]);
                    if(is_cal_nonlocal_dev){
                        Parallel_Util::group_gather_vector(nonlocal_dev_x[i][m][p]);
                        Parallel_Util::group_gather_vector(nonlocal_dev_y[i][m][p]);
                        Parallel_Util::group_gather_vector(nonlocal_dev_z[i][m][p]);
                    }
                    loop_index++;
                }
            }
        }
        timer->end("Upf2KB::shchoi");
        Verbose::single(Verbose::Normal) << "Upf2KB time(s) : " << timer->get_elapsed_time("Upf2KB::shchoi",-1) <<std::endl;
    }
/*
    if(parameters->sublist("Pseudopotential").get<string>("UsingDoubleGrid") == "No"){
        Verbose::single(Verbose::Normal)<< std::endl;
        Verbose::single(Verbose::Normal)<< "#---------------------------------------------- Upf2KB::calculate_nonlocal" << std::endl;
        Verbose::single(Verbose::Normal)<< " Using single-grid to represent pseudopotential" << std::endl;
        Verbose::single(Verbose::Normal)<< " Nonlocal threshold = " << threshold << std::endl;
        Verbose::single(Verbose::Normal)<< "#---------------------------------------------------------------------------" <<std::endl;
        // Option 1. Calculation with Gauss quadrature
        for(int i =0;i<number_vps_file[itype];i++){ // angular momentum index
            int l = oamom[itype][i]; //angular momentum
            vector<vector<vector<double > > > nonlocal_pp_tmp;
            vector<vector<vector<double > > > nonlocal_dev_x_tmp;
            vector<vector<vector<double > > > nonlocal_dev_y_tmp;
            vector<vector<vector<double > > > nonlocal_dev_z_tmp;
            vector<vector<vector<int > > > nonlocal_pp_index_tmp;
            for(int m=0;m<2*l+1;m++){ //  magnetic momentum index
                vector<vector<double> > nonlocal_pp_tmp_tmp;
                vector<vector<double> > nonlocal_dev_x_tmp_tmp;
                vector<vector<double> > nonlocal_dev_y_tmp_tmp;
                vector<vector<double> > nonlocal_dev_z_tmp_tmp;
                vector<vector<int> > nonlocal_pp_index_tmp_tmp;
                for (int p=0; p< projector_number[itype][i]; p++){ // number of projector index
                    double rcut = new_nonlocal_cutoff[itype][i][p];

                    vector<double> nonlocal_pp_tmp_tmp_tmp;
                    vector<double> nonlocal_dev_x_tmp_tmp_tmp;
                    vector<double> nonlocal_dev_y_tmp_tmp_tmp;
                    vector<double> nonlocal_dev_z_tmp_tmp_tmp;
                    vector<int> nonlocal_pp_index_tmp_tmp_tmp;

                    if(parameters->sublist("Pseudopotential").get<string>("UsingFiltering") == "No"){
                        double tmp = (nonlocal_pp_radial[itype][i][p][1] - nonlocal_pp_radial[itype][i][p][0]) / (nonlocal_mesh[itype][i][p][1] - nonlocal_mesh[itype][i][p][0]) * (0.0 - nonlocal_mesh[itype][i][p][0]) + nonlocal_pp_radial[itype][i][p][0];
                        new_nonlocal_mesh[itype][i][p].insert(new_nonlocal_mesh[itype][i][p].begin(), 0.0);
                        new_nonlocal_pp_radial[itype][i][p].insert(new_nonlocal_pp_radial[itype][i][p].begin(), tmp);
                        // Spline interpolation
                        int mesh_size = new_nonlocal_pp_radial[itype][i][p].size();
                        vector<double> y2(mesh_size);
                        #ifdef ACE_HAVE_OMP
                        #pragma omp parallel for
                        #endif
                        for(int j=0; j<mesh_size; j++){
                            y2[j] = Numerical_Derivatives::numerical_derivatives(new_nonlocal_mesh[itype][i][p][j], new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], 2, 5);
                        }

#ifdef ACE_HAVE_OMP

                        vector< vector<double> > nonlocal_pp_tmp_tmp_tmp_tmp;
                        vector< vector<double> > nonlocal_dev_x_tmp_tmp_tmp_tmp;
                        vector< vector<double> > nonlocal_dev_y_tmp_tmp_tmp_tmp;
                        vector< vector<double> > nonlocal_dev_z_tmp_tmp_tmp_tmp;
                        vector< vector<int> > nonlocal_pp_index_tmp_tmp_tmp_tmp;
#pragma omp parallel
                        {
#pragma omp single
                            {
                                nonlocal_pp_tmp_tmp_tmp_tmp.resize(omp_get_num_threads());
                                nonlocal_dev_x_tmp_tmp_tmp_tmp.resize(omp_get_num_threads());
                                nonlocal_dev_y_tmp_tmp_tmp_tmp.resize(omp_get_num_threads());
                                nonlocal_dev_z_tmp_tmp_tmp_tmp.resize(omp_get_num_threads());
                                nonlocal_pp_index_tmp_tmp_tmp_tmp.resize(omp_get_num_threads());
                            }
                            int start = omp_get_thread_num()*(NumMyElements/omp_get_num_threads());
                            int end = omp_get_thread_num()==omp_get_num_threads()-1? NumMyElements: (omp_get_thread_num()+1)* (NumMyElements/omp_get_num_threads());
                            for(int j=start;j<end;j++){
                                int j_x=0,j_y=0,j_z=0;
                                mesh->decompose(MyGlobalElements[j],&j_x,&j_y,&j_z);
                                double x = scaled_grid[0][j_x] - position[0];
                                double y = scaled_grid[1][j_y] - position[1];
                                double z = scaled_grid[2][j_z] - position[2];
                                double r = sqrt( x*x + y*y + z*z );
                                if(r < 1.0E-30){
                                    double value = Spherical_Harmonics::Ylm(l, m-l, x, y, z) * new_nonlocal_pp_radial[itype][i][p][0] / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;

                                    if (fabs(value) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(value);
                                        nonlocal_pp_index_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(MyGlobalElements[j]);
                                        if(is_cal_nonlocal_dev){
                                            double value_x = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z, 0) * new_nonlocal_pp_radial[itype][i][p][0] / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                            double value_y = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z, 1) * new_nonlocal_pp_radial[itype][i][p][0] / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                            double value_z = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z, 2) * new_nonlocal_pp_radial[itype][i][p][0] / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                            nonlocal_dev_x_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(value_x);
                                            nonlocal_dev_y_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(value_y);
                                            nonlocal_dev_z_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(value_z);
                                        }
                                    }
                                }
                                else if(r < rcut){
                                    double value = Spherical_Harmonics::Ylm(l,m-l,x,y,z) * Interpolation::Spline::splint(new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], y2, mesh_size, r) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;

                                    if (fabs(value) > threshold){
                                        nonlocal_pp_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(value);
                                        nonlocal_pp_index_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(MyGlobalElements[j]);
                                        if(is_cal_nonlocal_dev){
                                            double value_x = (Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,0) * Interpolation::Spline::splint(new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], y2, mesh_size, r) + Spherical_Harmonics::Ylm(l,m-l,x,y,z) * Numerical_Derivatives::numerical_derivatives(r,new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], 1, 3)*x/r) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                            double value_y = (Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,1) * Interpolation::Spline::splint(new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], y2, mesh_size, r) + Spherical_Harmonics::Ylm(l,m-l,x,y,z) * Numerical_Derivatives::numerical_derivatives(r,new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], 1, 3)*y/r) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                            double value_z = (Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,2) * Interpolation::Spline::splint(new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], y2, mesh_size, r) + Spherical_Harmonics::Ylm(l,m-l,x,y,z) * Numerical_Derivatives::numerical_derivatives(r,new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], 1, 3)*z/r) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                            nonlocal_dev_x_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(value_x);
                                            nonlocal_dev_y_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(value_y);
                                            nonlocal_dev_z_tmp_tmp_tmp_tmp[omp_get_thread_num()].push_back(value_z);
                                        }
                                    }
                                }
                            }
#pragma omp barrier
#pragma omp single nowait
                            {
                                for(int i=0; i<omp_get_num_threads(); i++){
                                    nonlocal_pp_tmp_tmp_tmp.insert(nonlocal_pp_tmp_tmp_tmp.end(), nonlocal_pp_tmp_tmp_tmp_tmp[i].begin(), nonlocal_pp_tmp_tmp_tmp_tmp[i].end());
                                    nonlocal_pp_index_tmp_tmp_tmp.insert(nonlocal_pp_index_tmp_tmp_tmp.end(), nonlocal_pp_index_tmp_tmp_tmp_tmp[i].begin(), nonlocal_pp_index_tmp_tmp_tmp_tmp[i].end());

                                    if(is_cal_nonlocal_dev){
                                        nonlocal_dev_x_tmp_tmp_tmp.insert(nonlocal_dev_x_tmp_tmp_tmp.end(), nonlocal_dev_x_tmp_tmp_tmp_tmp[i].begin(), nonlocal_dev_x_tmp_tmp_tmp_tmp[i].end());
                                        nonlocal_dev_y_tmp_tmp_tmp.insert(nonlocal_dev_y_tmp_tmp_tmp.end(), nonlocal_dev_y_tmp_tmp_tmp_tmp[i].begin(), nonlocal_dev_y_tmp_tmp_tmp_tmp[i].end());
                                        nonlocal_dev_z_tmp_tmp_tmp.insert(nonlocal_dev_z_tmp_tmp_tmp.end(), nonlocal_dev_z_tmp_tmp_tmp_tmp[i].begin(), nonlocal_dev_z_tmp_tmp_tmp_tmp[i].end());
                                    }
                                }
                            }
#pragma omp barrier
#pragma omp single nowait
                            {
                                Parallel_Util::group_gather_vector(nonlocal_pp_tmp_tmp_tmp);
                                Parallel_Util::group_gather_vector(nonlocal_pp_index_tmp_tmp_tmp);
                                nonlocal_pp_tmp_tmp.push_back(nonlocal_pp_tmp_tmp_tmp);
                                nonlocal_pp_index_tmp_tmp.push_back(nonlocal_pp_index_tmp_tmp_tmp);
                                if(is_cal_nonlocal_dev){
                                    Parallel_Util::group_gather_vector(nonlocal_dev_x_tmp_tmp_tmp);
                                    Parallel_Util::group_gather_vector(nonlocal_dev_y_tmp_tmp_tmp);
                                    Parallel_Util::group_gather_vector(nonlocal_dev_z_tmp_tmp_tmp);
                                    nonlocal_dev_x_tmp_tmp.push_back(nonlocal_dev_x_tmp_tmp_tmp);
                                    nonlocal_dev_y_tmp_tmp.push_back(nonlocal_dev_y_tmp_tmp_tmp);
                                    nonlocal_dev_z_tmp_tmp.push_back(nonlocal_dev_z_tmp_tmp_tmp);
                                }
                            }
                        }

#else
                        for(int j=0;j<NumMyElements;j++){
                            int j_x=0,j_y=0,j_z=0;
                            mesh->decompose(MyGlobalElements[j],&j_x,&j_y,&j_z);
                            double x = scaled_grid[0][j_x] - position[0];
                            double y = scaled_grid[1][j_y] - position[1];
                            double z = scaled_grid[2][j_z] - position[2];
                            double r = sqrt( x*x + y*y + z*z );
                            if(r < 1.0E-30){
                                double value = Spherical_Harmonics::Ylm(l, m-l, x, y, z) * new_nonlocal_pp_radial[itype][i][p][0] / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;

                                if (fabs(value) > threshold){
                                    nonlocal_pp_tmp_tmp_tmp.push_back(value);
                                    nonlocal_pp_index_tmp_tmp_tmp.push_back(MyGlobalElements[j]);
                                    if(is_cal_nonlocal_dev){
                                        double value_x = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z, 0) * new_nonlocal_pp_radial[itype][i][p][0] / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                        double value_y = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z, 1) * new_nonlocal_pp_radial[itype][i][p][0] / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                        double value_z = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z, 2) * new_nonlocal_pp_radial[itype][i][p][0] / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                        nonlocal_dev_x_tmp_tmp_tmp.push_back(value_x);
                                        nonlocal_dev_y_tmp_tmp_tmp.push_back(value_y);
                                        nonlocal_dev_z_tmp_tmp_tmp.push_back(value_z);
                                    }
                                }
                            }
                            else if(r < rcut){
                                double value = Spherical_Harmonics::Ylm(l,m-l,x,y,z) * Interpolation::Spline::splint(new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], y2, mesh_size, r) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;

                                if (fabs(value) > threshold){
                                    nonlocal_pp_tmp_tmp_tmp.push_back(value);

                                    nonlocal_pp_index_tmp_tmp_tmp.push_back(MyGlobalElements[j]);
                                    if(is_cal_nonlocal_dev){
                                        double value_x = (Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,0) * Interpolation::Spline::splint(new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], y2, mesh_size, r) + Spherical_Harmonics::Ylm(l,m-l,x,y,z) * Numerical_Derivatives::numerical_derivatives(r,new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], 1, 3)*x/r) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                        double value_y = (Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,1) * Interpolation::Spline::splint(new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], y2, mesh_size, r) + Spherical_Harmonics::Ylm(l,m-l,x,y,z) * Numerical_Derivatives::numerical_derivatives(r,new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], 1, 3)*y/r) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                        double value_z = (Spherical_Harmonics::Derivative::dYlm(l,m-l,x,y,z,2) * Interpolation::Spline::splint(new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], y2, mesh_size, r) + Spherical_Harmonics::Ylm(l,m-l,x,y,z) * Numerical_Derivatives::numerical_derivatives(r,new_nonlocal_mesh[itype][i][p], new_nonlocal_pp_radial[itype][i][p], 1, 3)*z/r) / ( mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ) ;
                                        nonlocal_dev_x_tmp_tmp_tmp.push_back(value_x);
                                        nonlocal_dev_y_tmp_tmp_tmp.push_back(value_y);
                                        nonlocal_dev_z_tmp_tmp_tmp.push_back(value_z);
                                    }
                                }
                            }
                        }
                        Parallel_Util::group_gather_vector(nonlocal_pp_tmp_tmp_tmp);
                        Parallel_Util::group_gather_vector(nonlocal_pp_index_tmp_tmp_tmp);
                        nonlocal_pp_tmp_tmp.push_back(nonlocal_pp_tmp_tmp_tmp);
                        nonlocal_pp_index_tmp_tmp.push_back(nonlocal_pp_index_tmp_tmp_tmp);
                        if(is_cal_nonlocal_dev){
                            Parallel_Util::group_gather_vector(nonlocal_dev_x_tmp_tmp_tmp);
                            Parallel_Util::group_gather_vector(nonlocal_dev_y_tmp_tmp_tmp);
                            Parallel_Util::group_gather_vector(nonlocal_dev_z_tmp_tmp_tmp);
                            nonlocal_dev_x_tmp_tmp.push_back(nonlocal_dev_x_tmp_tmp_tmp);
                            nonlocal_dev_y_tmp_tmp.push_back(nonlocal_dev_y_tmp_tmp_tmp);
                            nonlocal_dev_z_tmp_tmp.push_back(nonlocal_dev_z_tmp_tmp_tmp);
                        }
#endif
                    }

                    else if(parameters->sublist("Pseudopotential").get<string>("UsingFiltering") == "Yes"){
                        timer->start("Upf2KB::filtering yes shchoi");
                        // Fourier Filtering
                        vector<double> filtered_nonlocal_pp_radial;
                        vector<double> filtered_nonlocal_mesh;
                        double rmax = parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") * rcut;
                        filter->filter_nonlocal(new_nonlocal_pp_radial[itype][i][p], new_nonlocal_mesh[itype][i][p], nonlocal_d_mesh[itype][i][p], l, rmax, filtered_nonlocal_pp_radial, filtered_nonlocal_mesh);

                        int mesh_size = filtered_nonlocal_mesh.size();
                        vector<double> y2(mesh_size);
                        #ifdef ACE_HAVE_OMP
                        #pragma omp parallel for
                        #endif
                        for(int j=0; j<mesh_size; j++){
                            //Verbose::all() << "r : " << filtered_nonlocal_mesh[j] << std::endl;
                            y2[j] = Numerical_Derivatives::numerical_derivatives(filtered_nonlocal_mesh[j], filtered_nonlocal_mesh, filtered_nonlocal_pp_radial, 2, 5);
                        }
                        double x,y,z,r;
                        double value,value_x,value_y,value_z;
                        double spherical_value;
                        double dev_spherical_value_x; double dev_spherical_value_y; double dev_spherical_value_z;
                        double denominator;
                        int j_x=0,j_y=0,j_z=0;
//                        #ifdef ACE_HVAE_OMP
//                        #pragma omp parallel for ordered schedule(static,1) private(x,y,z,r,j_x,j_y,j_z,value,value_x,value_y,value_z,dev_spherical_value_x,dev_spherical_value_y,dev_spherical_value_z,spherical_value,denominator)
//                        #endif
                        Verbose::single()<< "now original start" <<std::endl;
                        int index_test=0;
                        for(int j=0; j<NumMyElements; j++){
                            mesh->decompose(MyGlobalElements[j],&j_x,&j_y,&j_z);
                            x = scaled_grid[0][j_x] - position[0];
                            y = scaled_grid[1][j_y] - position[1];
                            z = scaled_grid[2][j_z] - position[2];
                            r = sqrt( x*x + y*y + z*z );
                            spherical_value = Spherical_Harmonics::Ylm(l, m-l, x, y, z);
                            denominator = mesh->compute_basis(j_x,scaled_grid[0][j_x],0) * mesh->compute_basis(j_y,scaled_grid[1][j_y],1) * mesh->compute_basis(j_z,scaled_grid[2][j_z],2) ;
                            if(is_cal_nonlocal_dev){
                            dev_spherical_value_x = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,0);
                            dev_spherical_value_y = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,1);
                            dev_spherical_value_z = Spherical_Harmonics::Derivative::dYlm(l, m-l, x, y, z ,2);
                            }
                            if(r < 1.0E-30){
                                value = spherical_value * filtered_nonlocal_pp_radial[0] / denominator ;
                                if(is_cal_nonlocal_dev){
                                value_x = dev_spherical_value_x * filtered_nonlocal_pp_radial[0] / denominator ;
                                value_y = dev_spherical_value_y * filtered_nonlocal_pp_radial[0] / denominator ;
                                value_z = dev_spherical_value_z * filtered_nonlocal_pp_radial[0] / denominator ;
                                }
                            }
                            else if(r < rmax){
                                value = spherical_value * Interpolation::Spline::splint(filtered_nonlocal_mesh, filtered_nonlocal_pp_radial, y2, mesh_size, r) / denominator ;
                                if(is_cal_nonlocal_dev){
                                value_x = (dev_spherical_value_x * Interpolation::Spline::splint(filtered_nonlocal_mesh, filtered_nonlocal_pp_radial, y2, mesh_size, r) + spherical_value * Numerical_Derivatives::numerical_derivatives(r,filtered_nonlocal_mesh, filtered_nonlocal_pp_radial, 1, 3)*x/r) / denominator ;
                                value_y = (dev_spherical_value_y * Interpolation::Spline::splint(filtered_nonlocal_mesh, filtered_nonlocal_pp_radial, y2, mesh_size, r) + spherical_value * Numerical_Derivatives::numerical_derivatives(r,filtered_nonlocal_mesh, filtered_nonlocal_pp_radial, 1, 3)*y/r) / denominator ;
                                value_z = (dev_spherical_value_z * Interpolation::Spline::splint(filtered_nonlocal_mesh, filtered_nonlocal_pp_radial, y2, mesh_size, r) + spherical_value * Numerical_Derivatives::numerical_derivatives(r,filtered_nonlocal_mesh, filtered_nonlocal_pp_radial, 1, 3)*z/r) / denominator ;
                                }
                            }
                            else{
                                continue;
                            }
//                            #ifdef ACE_HAVE_OMP
//                            #pragma omp ordered
//                            {
//                            #endif
                            if (fabs(value) > threshold){
                                nonlocal_pp_tmp_tmp_tmp.push_back(value);
                                nonlocal_pp_index_tmp_tmp_tmp.push_back(MyGlobalElements[j]);
                                if(is_cal_nonlocal_dev){
                                nonlocal_dev_x_tmp_tmp_tmp.push_back(value_x);
                                nonlocal_dev_y_tmp_tmp_tmp.push_back(value_y);
                                nonlocal_dev_z_tmp_tmp_tmp.push_back(value_z);
                                }
                            }
//                            #ifdef ACE_HAVE_OMP
//                            }
//                            #endif
                        }
                        Parallel_Util::group_gather_vector(nonlocal_pp_tmp_tmp_tmp);
                        Parallel_Util::group_gather_vector(nonlocal_pp_index_tmp_tmp_tmp);
                        nonlocal_pp_tmp_tmp.push_back(nonlocal_pp_tmp_tmp_tmp);
                        nonlocal_pp_index_tmp_tmp.push_back(nonlocal_pp_index_tmp_tmp_tmp);
                        if(is_cal_nonlocal_dev){
                            Parallel_Util::group_gather_vector(nonlocal_dev_x_tmp_tmp_tmp);
                            Parallel_Util::group_gather_vector(nonlocal_dev_y_tmp_tmp_tmp);
                            Parallel_Util::group_gather_vector(nonlocal_dev_z_tmp_tmp_tmp);
                            nonlocal_dev_x_tmp_tmp.push_back(nonlocal_dev_x_tmp_tmp_tmp);
                            nonlocal_dev_y_tmp_tmp.push_back(nonlocal_dev_y_tmp_tmp_tmp);
                            nonlocal_dev_z_tmp_tmp.push_back(nonlocal_dev_z_tmp_tmp_tmp);
                        }
                        }
                        timer->end("Upf2KB::filtering yes shchoi");
                        Verbose::single(Verbose::Detail) << "filtering shchoi: " << timer->get_elapsed_time("Upf2KB::filtering yes shchoi",-1) <<std::endl;
                        //index_++;
                    }
                    nonlocal_pp_tmp.push_back(nonlocal_pp_tmp_tmp);
                    nonlocal_pp_index_tmp.push_back(nonlocal_pp_index_tmp_tmp);
                    if(is_cal_nonlocal_dev){
                    nonlocal_dev_x_tmp.push_back(nonlocal_dev_x_tmp_tmp);
                    nonlocal_dev_y_tmp.push_back(nonlocal_dev_y_tmp_tmp);
                    nonlocal_dev_z_tmp.push_back(nonlocal_dev_z_tmp_tmp);
                    }
                }
                nonlocal_pp.push_back(nonlocal_pp_tmp);
                nonlocal_pp_index.push_back(nonlocal_pp_index_tmp);
                if(is_cal_nonlocal_dev){
                nonlocal_dev_x.push_back(nonlocal_dev_x_tmp);
                nonlocal_dev_y.push_back(nonlocal_dev_y_tmp);
                nonlocal_dev_z.push_back(nonlocal_dev_z_tmp);
                }
            }
        }
        */
        else{
            timer->start("Upf2KB::calculate_nonlocal with double grid");
            RCP<Atoms> new_atom = rcp(new Atoms());
            Atom tmp_atom =  Atom(atoms->operator[](iatom));
            new_atom->push(  tmp_atom );

            const double* scaling = mesh->get_scaling();

            Verbose::single(Verbose::Simple) << std::endl;
            Verbose::single(Verbose::Simple) <<"#---------------------------------------------- Upf2KB::calculate_nonlocal" << std::endl;
            Verbose::single(Verbose::Simple) << " Using double-grid to represent pseduopotential " << std::endl;
            Verbose::single(Verbose::Simple) << " Non-local pseudopotential integration Rmax : " << parameters->sublist("Pseudopotential").get<double>("NonlocalRmax") << std::endl;
            Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" << std::endl;

            vector<vector<vector<vector<double> > > > my_nonlocal_pp;
            vector<vector<vector<vector<double> > > > my_nonlocal_dev_x;
            vector<vector<vector<vector<double> > > > my_nonlocal_dev_y;
            vector<vector<vector<vector<double> > > > my_nonlocal_dev_z;

            double* fine_scaling = new double[3];
            fine_scaling[0] = scaling[0] / this -> fine_dimension;
            fine_scaling[1] = scaling[1] / this -> fine_dimension;
            fine_scaling[2] = scaling[2] / this -> fine_dimension;

            int* fine_points = new int[3];
            double length_for_x = (scaled_grid[0][mesh->get_points()[0]-1]-scaled_grid[0][0]);
            double length_for_y = (scaled_grid[1][mesh->get_points()[1]-1]-scaled_grid[1][0]);
            double length_for_z = (scaled_grid[2][mesh->get_points()[2]-1]-scaled_grid[2][0]);

            int numx = static_cast<int>(length_for_x / fine_scaling[0]) + 1;
            int numy = static_cast<int>(length_for_y / fine_scaling[1]) + 1;
            int numz = static_cast<int>(length_for_z / fine_scaling[2]) + 1;

            fine_points[0] = numx;
            fine_points[1] = numy;
            fine_points[2] = numz;

            RCP<Basis_Function> fine_basis = rcp( new Sinc(fine_points, fine_scaling) );
            //vector<double> sampling_coefficients = Double_Grid::get_sampling_coefficients(parameters);
            vector<double> sampling_coefficients = Double_Grid::get_sampling_coefficients( parameters -> sublist("Pseudopotential").get<string>("FilterType"), this -> fine_dimension );

            for(int i=0; i<number_vps_file[itype]; i++){
                int l = oamom[itype][i];

                my_nonlocal_pp.push_back( vector<vector<vector<double> > >() );
                my_nonlocal_dev_x.push_back( vector<vector<vector<double> > >() );
                my_nonlocal_dev_y.push_back( vector<vector<vector<double> > >() );
                my_nonlocal_dev_z.push_back( vector<vector<vector<double> > >() );

                for(int m=0; m<2*l+1; m++){
                    my_nonlocal_pp[i].push_back(vector<vector<double> >() );
                    my_nonlocal_dev_x[i].push_back(vector<vector<double> >() );
                    my_nonlocal_dev_y[i].push_back(vector<vector<double> >() );
                    my_nonlocal_dev_z[i].push_back(vector<vector<double> >() );

                    nonlocal_pp[i].push_back(vector<vector<double> >() );
                    nonlocal_dev_x[i].push_back(vector<vector<double> >() );
                    nonlocal_dev_y[i].push_back(vector<vector<double> >() );
                    nonlocal_dev_z[i].push_back(vector<vector<double> >() );

                    nonlocal_pp_index[i].push_back( vector<vector<int> > () );

                    for(int p=0; p<projector_number[itype][i]; p++){
                        vector<double> input_radial = new_nonlocal_pp_radial[itype][i][p];
                        vector<double> input_radial_mesh = new_nonlocal_mesh[itype][i][p];
                        { // insert value at r=0.0
                            double value_for_origin = (new_nonlocal_pp_radial[itype][i][p][1] - new_nonlocal_pp_radial[itype][i][p][0]) / (new_nonlocal_mesh[itype][i][p][1] - new_nonlocal_mesh[itype][i][p][0]) * (0.0 - new_nonlocal_mesh[itype][i][p][0]) + new_nonlocal_pp_radial[itype][i][p][0];
                            input_radial_mesh.insert(input_radial_mesh.begin(), 0.0);
                            input_radial.insert(input_radial.begin(), value_for_origin);
                        }

                        int mesh_size = input_radial.size();
                        vector<double> y2;

                        for(int q=0; q<mesh_size; q++){
                            double tmp = Numerical_Derivatives::numerical_derivatives(input_radial_mesh[q], input_radial_mesh, input_radial, 2, 5);
                            y2.push_back(tmp);
                        }

                        double rcut = new_nonlocal_cutoff[itype][i][p];
                        Verbose::single(Verbose::Detail) << "l : " << l << "\t m : " << m << "\trcut : " << rcut << std::endl;
                        vector<double> radius;
                        radius.push_back(rcut);

                        Grid_Setting* fine_grid = new Grid_Atoms(fine_points, fine_basis, new_atom.get(), radius);
                        Epetra_Map fine_map(fine_grid->get_size(), 0, mesh->get_map()->Comm());
                        int NumFineElements = fine_map.NumMyElements();
                        int* GlobalFineElements = fine_map.MyGlobalElements();

                        my_nonlocal_pp[i][m].push_back(vector<double> () );
                        my_nonlocal_dev_x[i][m].push_back(vector<double> () );
                        my_nonlocal_dev_y[i][m].push_back(vector<double> () );
                        my_nonlocal_dev_z[i][m].push_back(vector<double> () );

                        nonlocal_pp[i][m].push_back(vector<double> () );
                        nonlocal_dev_x[i][m].push_back(vector<double> () );
                        nonlocal_dev_y[i][m].push_back(vector<double> () );
                        nonlocal_dev_z[i][m].push_back(vector<double> () );
                        nonlocal_pp_index[i][m].push_back( vector<int>  () );

                        for(int j=0; j<size; j++){
                            double rmax = rcut*parameters->sublist("Pseudopotential").get<double>("NonlocalRmax"); // This value should be decided.
                            int j_x=0, j_y=0, j_z=0;
                            mesh->decompose(j,  &j_x, &j_y, &j_z);
                            double x = scaled_grid[0][j_x] - position[0];
                            double y = scaled_grid[1][j_y] - position[1];
                            double z = scaled_grid[2][j_z] - position[2];
                            double r = sqrt( x*x + y*y + z*z );

                            if(r < rmax){
                                double value_tmp=0.0, value_tmp_x=0.0, value_tmp_y=0.0, value_tmp_z = 0.0;
                                double spherical_value,dev_spherical_value_x,dev_spherical_value_y,dev_spherical_value_z;
                                #ifdef ACE_HAVE_OMP
                                #pragma omp parallel for private(spherical_value,dev_spherical_value_x,dev_spherical_value_y,dev_spherical_value_z) reduction(+:value_tmp),reduction(+:value_tmp_x),reduction(+:value_tmp_y),reduction(+:value_tmp_z)
                                #endif
                                for(int fine_j=0; fine_j<NumFineElements; fine_j++){
                                    int fine_jx=0, fine_jy=0, fine_jz=0;
                                    const double** fine_scaled_grid = fine_basis->get_scaled_grid();
                                    fine_grid->decompose(GlobalFineElements[fine_j], fine_basis->get_points(), &fine_jx, &fine_jy, &fine_jz);

                                    double xa = fine_scaled_grid[0][fine_jx] - position[0];
                                    double yb = fine_scaled_grid[1][fine_jy] - position[1];
                                    double zc = fine_scaled_grid[2][fine_jz] - position[2];
                                    double fine_r = sqrt((xa)*(xa) + (yb)*(yb) + (zc)*(zc));

                                    int zx = round(abs(scaled_grid[0][j_x] - fine_scaled_grid[0][fine_jx])/fine_scaling[0]);
                                    int zy = round(abs(scaled_grid[1][j_y] - fine_scaled_grid[1][fine_jy])/fine_scaling[1]);
                                    int zz = round(abs(scaled_grid[2][j_z] - fine_scaled_grid[2][fine_jz])/fine_scaling[2]);

                                    double sinc_x = sampling_coefficients[zx];
                                    double sinc_y = sampling_coefficients[zy];
                                    double sinc_z = sampling_coefficients[zz];

                                    double pre_factor = sinc_x*sinc_y*sinc_z;
                                    spherical_value = Spherical_Harmonics::Ylm(l, m-l, xa, yb, zc);
                                    dev_spherical_value_x = Spherical_Harmonics::Derivative::dYlm(l, m-l, xa, yb, zc,0);
                                    dev_spherical_value_y = Spherical_Harmonics::Derivative::dYlm(l, m-l, xa, yb, zc,1);
                                    dev_spherical_value_z = Spherical_Harmonics::Derivative::dYlm(l, m-l, xa, yb, zc,2);
                                    if(fine_r > 1.0E-30){
                                        value_tmp+= pre_factor * spherical_value * Interpolation::Spline::splint(input_radial_mesh, input_radial, y2, mesh_size, fine_r);
                                        value_tmp_x += pre_factor * (spherical_value * Numerical_Derivatives::numerical_derivatives(fine_r, input_radial_mesh, input_radial, 1, 3)*xa/fine_r + dev_spherical_value_x * Interpolation::Spline::splint(input_radial_mesh, input_radial, y2, mesh_size, fine_r));
                                        value_tmp_y += pre_factor * (spherical_value * Numerical_Derivatives::numerical_derivatives(fine_r, input_radial_mesh, input_radial, 1, 3)*yb/fine_r + dev_spherical_value_y * Interpolation::Spline::splint(input_radial_mesh, input_radial, y2, mesh_size, fine_r));
                                        value_tmp_z += pre_factor * (spherical_value * Numerical_Derivatives::numerical_derivatives(fine_r, input_radial_mesh, input_radial, 1, 3)*zc/fine_r + dev_spherical_value_z * Interpolation::Spline::splint(input_radial_mesh, input_radial, y2, mesh_size, fine_r));
                                    }
                                    else{
                                        value_tmp+= pre_factor * spherical_value * input_radial[0];
                                    }
                                }

                                value_tmp   /= pow(this -> fine_dimension,3)/sqrt(scaling[0]*scaling[1]*scaling[2]);
                                value_tmp_x /= pow(this -> fine_dimension,3)/sqrt(scaling[0]*scaling[1]*scaling[2]);
                                value_tmp_y /= pow(this -> fine_dimension,3)/sqrt(scaling[0]*scaling[1]*scaling[2]);
                                value_tmp_z /= pow(this -> fine_dimension,3)/sqrt(scaling[0]*scaling[1]*scaling[2]);

                                nonlocal_pp[i][m][p].push_back(0.0);
                                nonlocal_dev_x[i][m][p].push_back(0.0);
                                nonlocal_dev_y[i][m][p].push_back(0.0);
                                nonlocal_dev_z[i][m][p].push_back(0.0);

                                my_nonlocal_pp[i][m][p].push_back(value_tmp );
                                my_nonlocal_dev_x[i][m][p].push_back(value_tmp_x );
                                my_nonlocal_dev_y[i][m][p].push_back(value_tmp_y );
                                my_nonlocal_dev_z[i][m][p].push_back(value_tmp_z );
                                nonlocal_pp_index[i][m][p].push_back(j);

                            }
                        }
                        delete fine_grid;
                    }
                }
            }

            //delete fine_mesh;
            delete[] fine_scaling;
            delete[] fine_points;

            for(int i=0; i<number_vps_file[itype]; i++){
                int l = oamom[itype][i];
                for(int m=0; m<2*l+1; m++){
                    for(int p=0; p<projector_number[itype][i]; p++){
                        Parallel_Util::group_sum(&my_nonlocal_pp[i][m][p][0], &nonlocal_pp[i][m][p][0], nonlocal_pp_index[i][m][p].size() );
                        Parallel_Util::group_sum(&my_nonlocal_dev_x[i][m][p][0], &nonlocal_dev_x[i][m][p][0], nonlocal_pp_index[i][m][p].size());
                        Parallel_Util::group_sum(&my_nonlocal_dev_y[i][m][p][0], &nonlocal_dev_y[i][m][p][0], nonlocal_pp_index[i][m][p].size());
                        Parallel_Util::group_sum(&my_nonlocal_dev_z[i][m][p][0], &nonlocal_dev_z[i][m][p][0], nonlocal_pp_index[i][m][p].size());
                        Verbose::single(Verbose::Detail) << "Upf2KB::index size : " << nonlocal_pp_index[i][m][p].size() << std::endl;
                        for (int q=0;q<nonlocal_pp_index[i][m][p].size();q++){
                            if (fabs(nonlocal_pp[i][m][p][q])<threshold){
                                nonlocal_pp_index[i][m][p].erase(nonlocal_pp_index[i][m][p].begin()+q);
                                nonlocal_pp[i][m][p].erase(nonlocal_pp[i][m][p].begin()+q);
                                nonlocal_dev_x[i][m][p].erase(nonlocal_dev_x[i][m][p].begin()+q);
                                nonlocal_dev_y[i][m][p].erase(nonlocal_dev_y[i][m][p].begin()+q);
                                nonlocal_dev_z[i][m][p].erase(nonlocal_dev_z[i][m][p].begin()+q);

                                q--;
                            }
                        }

                        Verbose::single(Verbose::Detail) << "index size : " << nonlocal_pp_index[i][m][p].size() << std::endl;
                    }
                }
            }
            timer->end("Upf2KB::calculate_nonlocal with double grid");
            Verbose::single(Verbose::Simple) << std::endl;
            Verbose::single(Verbose::Simple) <<"#---------------------------------------------- Upf2KB::calculate_nonlocal" << std::endl;
            Verbose::single(Verbose::Simple) << " Time to compute numerical integration: " << timer->get_elapsed_time("Upf2KB::calculate_nonlocal with double grid",-1) << " s" << std::endl;
            Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" << std::endl;
        }
        return;
    }
    /*
       void Upf2KB::construct_matrix(Teuchos::RCP<Epetra_CrsMatrix>& core_hamiltonian){
       int ierr;
       Epetra_Map Map = core_hamiltonian->RowMap();
//////////////Nonlocal potential ///////////////////
vector<double *> positions = atoms->get_positions();
for(int iatom=0;iatom<atoms->get_original_size();iatom++){
int itype = atoms->get_atom_type(iatom);
for(int a=0; a<oamom.size();a++){
for(int b=0;b<oamom[0].size();b++){
}
}
for(int i=0;i<V_vector[iatom].size();i++){ //i : number_vps
int l = oamom[itype][i];
int EKB_index = i*(number_vps_file[itype]+1);
double EKB_value = EKB[itype][EKB_index];
for(int m=0;m<2*l+1;m++){
for (int p=0;p< V_vector[iatom][i][m].size();p++){
int index_size = V_index[iatom][i][m][p].size();
Verbose::instance()<< "index size : " << V_index[iatom][i][m][p].size() << Endl::endl;
Verbose::instance()<< "iatom = " << iatom << ", type = " << atoms->get_atomic_numbers()[iatom] << ", l = " << l << ", m = " << m-l << ", vector size = " << index_size << Endl::endl;
for(int a=0; a<index_size; a++){
vector<double> values;
int a_index = V_index[iatom][i][m][p][a];
if(Map.MyGID(a_index)){
for(int b=0; b<index_size; b++){
double tmp = EKB_value * V_vector[iatom][i][m][p][a] * V_vector[iatom][i][m][p][b] ;
values.push_back(tmp);
}
ierr = core_hamiltonian->InsertGlobalValues(a_index, values.size(), &values[0], &V_index[iatom][i][m][p][0]);
if(ierr < 0){ // If the allocated length of the row has to be expanded, a positive warning code will be returned.
Verbose::instance()<<iatom <<  "ERROR: core_hamiltonian->InsertGlobalValues(i_index, values.size(), &values[0], &V_index[k][m][0]) - ierr = " << ierr << Endl::endl;
}
}
}
}
}
}
}

return;
}
*/

void Upf2KB::pp_information(int itype){
    // Printing pseudopotential information
    Verbose::single(Verbose::Simple) <<std::endl;
    Verbose::single(Verbose::Simple) << "==========================================================" << std::endl;
    Verbose::single(Verbose::Simple) << "Pseudopotential file for this atom  : " << upf_filenames[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "Valence charge                      : " << Zvals[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "Number of projectors                : " << number_vps_file[itype] << std::endl;
    for(int i=0;i<number_vps_file[itype];i++){
        Verbose::single(Verbose::Simple) << "     Orbital angular momentum       : " << oamom[itype][i] << std::endl;
    }
    Verbose::single(Verbose::Simple) << "Number of atomic pseudo orbitals    : " << number_pao_file[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "Number of points in mesh            : " << mesh_size[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "Nonlinear core correction           : " << nonlinear_core_correction[itype] << std::endl;
    Verbose::single(Verbose::Simple) << "==========================================================" << std::endl;
    Verbose::single(Verbose::Simple) << std::endl;
    // end

    return;
}

vector< vector<int> > Upf2KB::get_oamom(){
    return oamom;
}

vector<double> Upf2KB::get_local_cutoff(){
    return local_cutoff;
}

vector< vector<double> > Upf2KB::get_input_mesh(){
    return local_mesh;
}

vector< vector<double> > Upf2KB::get_input_local(){
    return short_potential_radial;
}

vector< vector<double> > Upf2KB::get_V_local(){
    if(local_pp.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed local vectors" << std::endl;
        this -> make_local_pp();
    }
    return local_pp;
}

void Upf2KB::clear_vector(){
    this -> local_pp.clear();
    this -> local_pp_dev.clear();
    this -> V_index.clear();
    this -> V_vector.clear();
    this -> V_dev_x.clear();
    this -> V_dev_y.clear();
    this -> V_dev_z.clear();
    Verbose::single(Verbose::Detail) << "Input value for UPF pseudopotential was deleted "<<std::endl;
}

vector< vector<double> > Upf2KB::get_V_local_dev(){
    if(local_pp_dev.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed local vectors" << std::endl;
        this -> make_local_pp();
    }
    return local_pp_dev;
}

vector< vector<double> > Upf2KB::get_input_EKB(){
    return EKB;
}

vector< vector< vector< vector< vector<double> > > > > Upf2KB::get_V_nl(){
    if(V_vector.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_vector;
}

vector< vector< vector< vector< vector<double> > > > > Upf2KB::get_V_nl_dev_x(){
    if(V_dev_x.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_x;
}

vector< vector< vector< vector< vector<double> > > > > Upf2KB::get_V_nl_dev_y(){
    if(V_dev_y.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_y;
}

vector< vector< vector< vector< vector<double> > > > > Upf2KB::get_V_nl_dev_z(){
    if(V_dev_z.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_dev_z;
}

vector< vector< vector< vector < vector<int> > > > > Upf2KB::get_V_index(){
    if(V_index.size() == 0){
        Verbose::single(Verbose::Detail) << "Re-calculating removed nonlocal vectors" << std::endl;
        this -> make_nonlocal_vectors();
    }
    return V_index;
}

void Upf2KB::make_vectors(){

    if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
        this->filter = new Filter(mesh, atoms, Zvals);
    }
    make_local_pp();
    make_nonlocal_vectors();

    if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
        delete filter;
    }
}

void Upf2KB::make_local_pp(){
     int size = mesh->get_original_size();
     local_pp.resize(atoms->get_size() );

     /*
     for(int i=0; i<size; i++){
         local_pp[i] = 0.0;
     }
     */
     for(int iatom=0; iatom<atoms->get_size(); iatom++){
         local_pp[iatom].resize(size);
#ifdef ACE_HAVE_OMP
         #pragma omp parallel for
#endif
         for(int i=0; i<size; i++){
             local_pp[iatom][i] = 0.0;
         }
     }

     if(is_cal_local_dev)
     {
         local_pp_dev.resize(atoms->get_size());
         for(int iatom=0; iatom<atoms->get_size(); iatom++){
             local_pp_dev[iatom].resize(size);
#ifdef ACE_HAVE_OMP
             #pragma omp parallel for
#endif
             for(int i=0; i<size; i++){
                 local_pp_dev[iatom][i] = 0.0;
             }
         }
     }

    if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 1){
        timer->start("Upf2KB::double-grid integration");
        Verbose::single(Verbose::Normal) <<std::endl;
        Verbose::single(Verbose::Normal) <<"#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
        Verbose::single(Verbose::Normal) << "Using double-grid to represent pseudopotential : make local_pp vector" << std::endl;
        Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;

        //int ierr;
        int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
        int NumMyElements = mesh->get_map()->NumMyElements();
        double threshold = parameters->sublist("Pseudopotential").get<double>("LocalThreshold");

        vector< vector <double> > comp_pp_radial;
        vector< vector <double> > y2_short;
        vector< vector <double> > y2_comp;
        vector< vector <double> > y2_local;
        vector<double> rcut;

        //////////////////////////////////////////////////////////////////////////////////////
        const double* scaling = mesh->get_scaling();
        const double **scaled_grid=mesh->get_scaled_grid();
        double* fine_scaling = new double[3];
        double fine_dimension = parameters->sublist("Pseudopotential").get<int>("FineDimension");
        fine_scaling[0] = scaling[0] / fine_dimension;
        fine_scaling[1] = scaling[1] / fine_dimension;
        fine_scaling[2] = scaling[2] / fine_dimension;


        double length_for_x = (scaled_grid[0][mesh->get_points()[0]-1]-scaled_grid[0][0]);
        double length_for_y = (scaled_grid[1][mesh->get_points()[1]-1]-scaled_grid[1][0]);
        double length_for_z = (scaled_grid[2][mesh->get_points()[2]-1]-scaled_grid[2][0]);

        int numx = static_cast<int>(length_for_x / fine_scaling[0]) + 1;
        int numy = static_cast<int>(length_for_y / fine_scaling[1]) + 1;
        int numz = static_cast<int>(length_for_z / fine_scaling[2]) + 1;

        int* fine_points = new int[3];
        fine_points[0] = numx;
        fine_points[1] = numy;
        fine_points[2] = numz;

        RCP<Basis_Function> fine_basis = rcp( new Sinc(fine_points, fine_scaling) );
        const double** fine_scaled_grid = fine_basis->get_scaled_grid();
        vector<double> sampling_coefficients = Double_Grid::get_sampling_coefficients(parameters->sublist("Pseudopotential").get<string>("FilterType"), fine_dimension);

        for(int itype=0; itype<atoms->get_atom_types().size(); itype++){
            // Local mesh and Short & Comp potential //
            local_mesh.push_back( original_mesh[itype]);
            local_cutoff.push_back( original_mesh[itype][local_pp_radial[itype].size()-1]);

            vector<double> comp_potential;
            vector<double> short_potential_radial_tmp;
            this->get_comp_potential(original_mesh[itype],itype,comp_potential);
            comp_pp_radial.push_back(comp_potential);

            for (int i =0;i<original_mesh[itype].size(); i++) {
                double short_potential_radial_tmp_tmp;
                short_potential_radial_tmp_tmp = comp_pp_radial[itype][i] + local_pp_radial[itype][i];
                short_potential_radial_tmp.push_back(short_potential_radial_tmp_tmp);
                //          cout << " short_potential_radial_tmp_tmp " << comp_pp_radial[itype][i] << " " << local_pp_radial[itype][i] << endl;
            }
            /*
               cout << "local pp radial : " << local_pp_radial[0].size() << endl;
               for(int k=0; k<local_pp_radial[0].size(); k++){
               cout << local_pp_radial[0][k] << endl;
               }
               */
            short_potential_radial.push_back(short_potential_radial_tmp);

            // Linear extrapolation at r=0
            double local_0_tmp = (local_pp_radial[itype][1] - local_pp_radial[itype][0]) / (local_mesh[itype][1] - local_mesh[itype][0]) * (0.0 - local_mesh[itype][0]) + local_pp_radial[itype][0];
            double short_0_tmp = (short_potential_radial[itype][1] - short_potential_radial[itype][0]) / (local_mesh[itype][1] - local_mesh[itype][0]) * (0.0 - local_mesh[itype][0]) + short_potential_radial[itype][0];
            double comp_0_tmp = (comp_pp_radial[itype][1] - comp_pp_radial[itype][0]) / (local_mesh[itype][1] - local_mesh[itype][0]) * (0.0 - local_mesh[itype][0]) + comp_pp_radial[itype][0];

            local_mesh[itype].insert(local_mesh[itype].begin(), 0.0);
            short_potential_radial[itype].insert(short_potential_radial[itype].begin(), short_0_tmp);
            comp_pp_radial[itype].insert(comp_pp_radial[itype].begin(), comp_0_tmp);
            local_pp_radial[itype].insert(local_pp_radial[itype].begin(), local_0_tmp);

            //Gather cutoff radius information//
            double rcut_tmp = 0.0;
            for(int i=0; i<local_mesh[itype].size(); i++){
                if(abs(short_potential_radial[itype][i]) < parameters->sublist("Pseudopotential").get<double>("ShortDecayTol") ){
                    rcut_tmp = local_mesh[itype][i];
                    break;
                }
            }
            rcut.push_back(rcut_tmp);

            // Make second derivative terms for SplInt. //
            int mesh_size = local_mesh[itype].size();
            vector<double> y2_short_tmp;
            vector<double> y2_comp_tmp;
            vector<double> y2_local_tmp;
            for(int i=0; i<mesh_size; i++){
                double y2_short_tmp_tmp = Numerical_Derivatives::numerical_derivatives(local_mesh[itype][i], local_mesh[itype], short_potential_radial[itype], 2, 5);
                double y2_local_tmp_tmp = Numerical_Derivatives::numerical_derivatives(local_mesh[itype][i], local_mesh[itype], local_pp_radial[itype], 2, 5);
                double y2_comp_tmp_tmp = Numerical_Derivatives::numerical_derivatives(local_mesh[itype][i], local_mesh[itype], comp_pp_radial[itype], 2, 5);
                y2_short_tmp.push_back(y2_short_tmp_tmp);
                y2_local_tmp.push_back(y2_local_tmp_tmp);
                y2_comp_tmp.push_back(y2_comp_tmp_tmp);
            }
            y2_short.push_back(y2_short_tmp);
            y2_comp.push_back(y2_comp_tmp);
            y2_local.push_back(y2_local_tmp);
            // Make second derivative terms for SplInt. //

        }
        ///////////////////////////////////////////////
        vector<std::array<double,3> > positions = atoms->get_positions();
        for (int iatom=0;iatom<atoms->get_size();iatom++){
            RCP<Atoms> new_atom = rcp(new Atoms());
            Atom tmp_atom =  Atom(atoms->operator[](iatom));
            new_atom->push(  tmp_atom );

            vector<double> local_pp_index_out;
            vector<double> local_pp_index_in;

            ////////////////////         Spline interpolation           ///////////////////////////
            int itype = atoms->get_atom_type(iatom);
            int mesh_size = local_mesh[itype].size();

            ////////////////////         Check in or out           /////////////////////////////
            for(int j=0; j<size; j++){
                double x1 = 0.0, y1=0.0, z1=0.0, r1=0.0;
                int j_x=0,j_y=0,j_z=0;
                mesh->decompose(j,&j_x,&j_y,&j_z);
                x1 = scaled_grid[0][j_x] - positions[iatom][0];
                y1 = scaled_grid[1][j_y] - positions[iatom][1];
                z1 = scaled_grid[2][j_z] - positions[iatom][2];
                r1 = sqrt( x1*x1 + y1*y1 + z1*z1 );

                if(r1 < rcut[itype]){
                    local_pp_index_in.push_back(j);
                }
                else{
                    local_pp_index_out.push_back(j);
                }
            }
            int in_size = local_pp_index_in.size();

            vector<double> radius;
            double rcut_in = rcut[itype]*parameters->sublist("Pseudopotential").get<double>("LocalIntegrationRange");
            radius.push_back(rcut_in);
            //Grid_Setting* fine_grid = new Grid_Atoms(fine_points, fine_mesh, new_atom.get(), radius);
            //jaechang
            int* tmp_fine_points = new int[3];
            tmp_fine_points[0] = fine_points[0];
            tmp_fine_points[1] = fine_points[1];
            tmp_fine_points[2] = fine_points[2];
            Atoms* tmp_new_atom = new Atoms();
            Atom tmp_tmp_atom =  Atom(atoms->operator[](iatom));
            tmp_new_atom->push(  tmp_tmp_atom );
            //jaechang
            Grid_Setting* fine_grid = new Grid_Atoms(tmp_fine_points, fine_basis, tmp_new_atom, radius);

            delete[] tmp_fine_points;
            delete tmp_new_atom;

            Epetra_Map fine_map(fine_grid->get_size(), 0, mesh->get_map()->Comm());
            int fine_NumMyElements = fine_map.NumMyElements();
            int* fine_GlobalMyElements = fine_map.MyGlobalElements();

            for (int local_k=0; local_k < in_size; local_k++){
                int i= local_pp_index_in[local_k];
                double xi = 0.0, yi = 0.0, zi = 0.0, ri = 0.0;
                int i_x=0,i_y=0,i_z=0;

                mesh->decompose(i,&i_x,&i_y,&i_z);
                xi = scaled_grid[0][i_x] - positions[iatom][0];
                yi = scaled_grid[1][i_y] - positions[iatom][1];
                zi = scaled_grid[2][i_z] - positions[iatom][2];
                ri = sqrt( xi*xi + yi*yi + zi*zi );

                ////////////////////////   Integration     //////////////////////////////////
                double total_value = 0.0, total_dev = 0.0, value_tmp = 0.0, dev_tmp = 0.0;
                for(int fine_q=0; fine_q<fine_NumMyElements; fine_q++){
                    int fine_qx=0, fine_qy=0, fine_qz=0;
                    //fine_grid->decompose(fine_map.GID(fine_q),&fine_qx, &fine_qy, &fine_qz);
                    fine_grid->decompose(fine_GlobalMyElements[fine_q], fine_basis->get_points(), &fine_qx, &fine_qy, &fine_qz);

                    double qx = fine_scaled_grid[0][fine_qx] - positions[iatom][0];
                    double qy = fine_scaled_grid[1][fine_qy] - positions[iatom][1];
                    double qz = fine_scaled_grid[2][fine_qz] - positions[iatom][2];
                    double fine_r = sqrt((qx)*(qx) + (qy)*(qy) + (qz)*(qz));

                    int zx = round(abs(scaled_grid[0][i_x] - fine_scaled_grid[0][fine_qx])/fine_scaling[0]);
                    int zy = round(abs(scaled_grid[1][i_y] - fine_scaled_grid[1][fine_qy])/fine_scaling[1]);
                    int zz = round(abs(scaled_grid[2][i_z] - fine_scaled_grid[2][fine_qz])/fine_scaling[2]);

                    double sinc_x = sampling_coefficients[zx];
                    double sinc_y = sampling_coefficients[zy];
                    double sinc_z = sampling_coefficients[zz];


                    value_tmp +=  sinc_x * sinc_y * sinc_z * Interpolation::Spline::splint(local_mesh[itype], short_potential_radial[itype], y2_short[itype], mesh_size, fine_r);
                    if(is_cal_local_dev){
                        dev_tmp +=  sinc_x * sinc_y * sinc_z * Numerical_Derivatives::numerical_derivatives(fine_r, local_mesh[itype], short_potential_radial[itype], 1, 3);
                    }
                }
                if(is_cal_local_dev){
                    dev_tmp /= pow(fine_dimension,3);
                }
                value_tmp /= pow(fine_dimension,3);


                Parallel_Util::group_sum(&value_tmp, &total_value, 1);
                if(is_cal_local_dev){
                    Parallel_Util::group_sum(&dev_tmp, &total_dev, 1);
                }
                local_pp[iatom][i] += total_value;
                //local_pp[i] += total_value;
                if(is_cal_local_dev){
                    local_pp_dev[iatom][i] += total_dev;
                }
            }
            delete fine_grid;
            //fine_map.~Epetra_Map();// This line cause error on gcc compiler.

            ////////////////////////// calcualte rest of points (only for diagonal values )   //////////////
            for(int local_j=0; local_j<NumMyElements; local_j++){
                int j = MyGlobalElements[local_j];
                bool is_in = false;
                for (int p=0;p<local_pp_index_in.size();p++){
                    if(local_pp_index_in[p]==j ){
                        //local_pp_index_in.erase(local_pp_index_in.begin() + p);
                        is_in=true;
                        break;
                    }
                }

                double x=0.0, y=0.0, z=0.0, r=0.0, tmp=0.0, dev_tmp=0.0;
                int j_x=0,j_y=0,j_z=0;
                mesh->decompose(j, &j_x, &j_y, &j_z);
                x = scaled_grid[0][j_x] - positions[iatom][0];
                y = scaled_grid[1][j_y] - positions[iatom][1];
                z = scaled_grid[2][j_z] - positions[iatom][2];
                r = sqrt( x*x + y*y + z*z );
                if(is_in==true){
                    if(r <= local_cutoff[itype]){
                        tmp= -Interpolation::Spline::splint(local_mesh[itype], comp_pp_radial[itype], y2_comp[itype], mesh_size, r);
                        if(is_cal_local_dev){
                            dev_tmp = -Numerical_Derivatives::numerical_derivatives(r, local_mesh[itype], comp_pp_radial[itype], 1, 3);
                        }

                    }
                }
                else{
                    if(r <= local_cutoff[itype]){
                        tmp = Interpolation::Spline::splint(local_mesh[itype], local_pp_radial[itype], y2_local[itype], mesh_size, r);
                        if(is_cal_local_dev){
                            dev_tmp = Numerical_Derivatives::numerical_derivatives(r, local_mesh[itype], local_pp_radial[itype], 1, 3);
                        }
                    }
                }

                local_pp[iatom][j] += tmp;
                //local_pp[j] += tmp;
                if(is_cal_local_dev){
                    local_pp_dev[iatom][j] += dev_tmp;
                }
                /*
                   if(ierr < 0){ // If the allocated length of the row has to be expanded, a positive warning code will be returned.
                   Verbose::all() << iatom <<  "ERROR::core_hamiltonian->InsertGlobalValues(j, 1, &tmp, &j) - ierr = " << ierr << std::endl;
                   }
                   */

            }
            //delete new_atom;
        }// for(iatom)

        delete[] fine_points;
        delete[] fine_scaling;
        //delete fine_mesh;
        //jaechang
        //local_mesh.clear();
        //local_cutoff.clear();
        //short_potential_radial.clear();
        //local_pp.clear();
        //local_pp_dev.clear();
        for(int k=0; k<local_pp_radial.size(); k++){
            local_pp_radial[k].erase(local_pp_radial[k].begin());
        }
        /*
           cout << "local pp radial2 : " << local_pp_radial[0].size() << endl;
           for(int k=0; k<local_pp_radial[0].size(); k++){
           cout << local_pp_radial[0][k] << endl;
           }
           */
        //
        timer->end("Upf2KB::double-grid integration");
        Verbose::single(Verbose::Normal) << std::endl;
        Verbose::single(Verbose::Normal) << "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
        Verbose::single(Verbose::Normal) << " Time to compute double-grid integration: " << timer->get_elapsed_time("Upf2KB::double-grid integration",-1) << " s" << std::endl;
        Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
        /*
           for(int i=0; i<local_pp.size(); i++){
           cout << "check local pp" << endl;
           for(int j=0; j<local_pp[i].size(); j++){
           cout <<j << " " << local_pp[i][j] << endl;
           }
           }
           for(int i=0; i<local_pp_index_in.size(); i++){
           cout << "check local pp_index_in" << endl;
           for(int j=0; j<local_pp_index_in[i].size(); j++){
           cout <<j << " " << local_pp_index_in[i][j] << endl;
           }
           }
           */
    }
    else if(parameters->sublist("Pseudopotential").get<int>("UsingDoubleGrid") == 0){
        timer->start("single-grid integration");
        int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
        int NumMyElements = mesh->get_map()->NumMyElements();
        if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 0){
            Verbose::single(Verbose::Normal) << std::endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
            Verbose::single(Verbose::Normal) << "Using single-grid to represent pseudopotential without filtering" << std::endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
            for (int itype=0; itype<atoms->get_atom_types().size(); itype++){
                short_potential_radial.push_back(local_pp_radial[itype]);
                local_mesh.push_back( original_mesh[itype]);
                local_cutoff.push_back( original_mesh[itype][short_potential_radial[itype].size()-1]);

                // Linear extrapolation at r=0
                double tmp = (short_potential_radial[itype][1] - short_potential_radial[itype][0]) / (local_mesh[itype][1] - local_mesh[itype][0]) * (0.0 - local_mesh[itype][0]) + short_potential_radial[itype][0];

                local_mesh[itype].insert(local_mesh[itype].begin(), 0.0);
                short_potential_radial[itype].insert(short_potential_radial[itype].begin(), tmp);
            }

            vector<std::array<double,3> > positions = atoms->get_positions();

            /*for(int i=0;i<NumMyElements;i++){
              local_pp[MyGlobalElements[i]] = 0.0;
              }*/

            // Why this???
            for (int iatom=0;iatom<atoms->get_size();iatom++){
                int itype = atoms->get_atom_type(iatom);
                // Spline interpolation
                int mesh_size = local_mesh[itype].size();
                /*
                   vector<double> y2;
                   for(int i=0; i<mesh_size; i++){
                   double tmp = Numerical_Derivatives::numerical_derivatives(local_mesh[itype][i], local_mesh[itype], short_potential_radial[itype], 2, 5);
                   y2.push_back(tmp);
                   }
                   */
                const double **scaled_grid=mesh->get_scaled_grid();
                double x,y,z,r;
                int i_x=0,i_y=0,i_z=0;
                //double tmp;
#ifdef ACE_HAVE_OMP
#pragma omp parallel for private(x,y,z,r,i_x,i_y,i_z)
#endif
                for(int i=0;i<NumMyElements;i++){
                    mesh->decompose(MyGlobalElements[i],&i_x,&i_y,&i_z);
                    x = scaled_grid[0][i_x] - positions[iatom][0];
                    y = scaled_grid[1][i_y] - positions[iatom][1];
                    z = scaled_grid[2][i_z] - positions[iatom][2];
                    r = sqrt( x*x + y*y + z*z );
                    if(r <= local_cutoff[itype]){
                        //double tmp = Spline_Interpolation::splint(local_mesh[itype], short_potential_radial[itype], y2, mesh_size, r);
                        double tmp = Interpolation::Linear::linear_interpolate(r, local_mesh[itype], short_potential_radial[itype]);
                        double dev_tmp = Numerical_Derivatives::numerical_derivatives(r,local_mesh[itype], short_potential_radial[itype], 1,3);
                        //local_pp[MyGlobalElements[i]] += tmp;
                        local_pp[iatom][MyGlobalElements[i]] += tmp;
                        local_pp_dev[iatom][MyGlobalElements[i]] = dev_tmp;
                        //local_potential->operator[](i) += tmp;
                        //short_potential->operator[](iatom)[i] = tmp;
                        //cout << "tmp : " << tmp << ", r :  " << r << endl;
                    }
                }
            }
        }
        //by jaechang
        else if(parameters->sublist("Pseudopotential").get<int>("UsingFiltering") == 1){
            Verbose::single(Verbose::Normal) << std::endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
            Verbose::single(Verbose::Normal) << "Using single-grid to represent pseudopotential with filtering" << std::endl;
            Verbose::single(Verbose::Normal) << "#---------------------------------------------------------------------------" << std::endl;
            /*
               for(int i=0;i<NumMyElements;i++){
               local_pp[MyGlobalElements[i]] = 0.0;
               }
               */

            vector <vector <double> > short_mesh;
            vector <vector <double> > comp_potential_radial;
            vector <double> short_rcut;

            timer->start("filtering timer-1");
            filter->optimize_comp_charge_exps(local_pp_radial, original_mesh);
            timer->end("filtering timer-1");
            Verbose::single(Verbose::Detail)<< " filtering timer-1: " << timer->get_elapsed_time("filtering timer-1",-1) << " s" << std::endl;

            timer->start("filtering timer0");
            for (int itype=0; itype<atoms->get_atom_types().size(); itype++){
                vector<double> new_short_potential_radial_tmp;
                vector<double> short_potential_radial_tmp(original_mesh[itype].size());
                vector<double> comp_potential_radial_tmp;
                vector<double> short_mesh_tmp;

                filter->get_comp_potential(original_mesh[itype],comp_potential_radial_tmp,itype);

                //this->get_comp_potential(original_mesh[itype],itype,comp_potential_radial_tmp);
                local_mesh.push_back( original_mesh[itype]);
                local_cutoff.push_back( original_mesh[itype][original_mesh[itype].size()-1]);

                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for
                #endif
                for (int i =0;i<original_mesh[itype].size(); i++){
                    short_potential_radial_tmp[i] = (local_pp_radial[itype][i] + comp_potential_radial_tmp[i]);
                }

                double rcut_tmp = 0.0;
                for(int i=0; i<short_potential_radial_tmp.size(); i++){
                    if(abs(short_potential_radial_tmp[i]) < 1.0E-4 ){
                        rcut_tmp = original_mesh[itype][i];
                        break;
                    }
                }
                double short_rcut_tmp = rcut_tmp*1.25;
                short_rcut.push_back(short_rcut_tmp);



                timer->start("filtering local");
                filter->filter_local(short_potential_radial_tmp,original_mesh[itype],original_d_mesh[itype], short_rcut_tmp, new_short_potential_radial_tmp, short_mesh_tmp);
                timer->end("filtering local");
                Verbose::single(Verbose::Detail) << " filtering local("<<itype<<")" << timer->get_elapsed_time("filtering local",-1) << " s" << std::endl;
                // Linear extrapolation at r=0
                double tmp = (comp_potential_radial_tmp[1] - comp_potential_radial_tmp[0]) / (original_mesh[itype][1] - original_mesh[itype][0]) * (0.0 - original_mesh[itype][0]) + comp_potential_radial_tmp[0];
                comp_potential_radial_tmp.insert(comp_potential_radial_tmp.begin(), tmp);
                local_mesh[itype].insert(local_mesh[itype].begin(), 0.0);

                short_mesh.push_back(short_mesh_tmp);
                short_potential_radial.push_back(new_short_potential_radial_tmp);
                comp_potential_radial.push_back(comp_potential_radial_tmp);
            }
            timer->end("filtering timer0");
            Verbose::single(Verbose::Detail) << " filtering timer0: " << timer->get_elapsed_time("filtering timer0",-1) << " s" << std::endl;

            vector<std::array<double,3> > positions = atoms->get_positions();

            int itype;
            int i_x=0,i_y=0,i_z=0;
            double x,y,z,r;
            const double **scaled_grid=mesh->get_scaled_grid();
            timer->start("filtering timer1");
//            #ifdef ACE_HAVE_OMP
//            #pragma omp parallel for private(x,y,z,r,i_x,i_y,i_z,itype) collapse(2)
//            #endif
            for (int iatom=0;iatom<atoms->get_size();iatom++){
                // Spline interpolation
                /*int mesh_size = local_mesh[itype].size();
                  vector<double> y2;
                  for(int i=0; i<mesh_size; i++){
                  double tmp = Numerical_Derivatives::numerical_derivatives(local_mesh[itype][i], local_mesh[itype], short_potential_radial[itype], 2, 5);
                  y2.push_back(tmp);
                  }*/
                itype = atoms->get_atom_type(iatom);
                #ifdef ACE_HAVE_OMP
                #pragma omp parallel for private(x,y,z,r,i_x,i_y,i_z)
                #endif
                for(int i=0;i<NumMyElements;i++){
                    mesh->decompose(MyGlobalElements[i],&i_x,&i_y,&i_z);
                    x = scaled_grid[0][i_x] - positions[iatom][0];
                    y = scaled_grid[1][i_y] - positions[iatom][1];
                    z = scaled_grid[2][i_z] - positions[iatom][2];
                    r = sqrt( x*x + y*y + z*z );
                    double tmp = 0.0;
                    double dev_tmp = 0.0;
                    if(r < 1.0E-30){
                        tmp = short_potential_radial[itype][0] - comp_potential_radial[itype][0];
                        //dev_tmp = Numerical_Derivatives::numerical_derivatives(r,local_mesh[itype], short_potential_radial[itype], 1,3);
                    }
                    else if(r <= local_cutoff[itype]){
                        if(r < short_rcut[itype]){
                            tmp = Interpolation::Linear::linear_interpolate(r, short_mesh[itype], short_potential_radial[itype]) - Interpolation::Linear::linear_interpolate(r, local_mesh[itype], comp_potential_radial[itype]);
                            dev_tmp = Numerical_Derivatives::numerical_derivatives(r,short_mesh[itype], short_potential_radial[itype], 1,3) - Numerical_Derivatives::numerical_derivatives(r,local_mesh[itype], comp_potential_radial[itype], 1,3);
                        }
                        else{
                            tmp = -Interpolation::Linear::linear_interpolate(r, local_mesh[itype], comp_potential_radial[itype]);
                            dev_tmp = -Numerical_Derivatives::numerical_derivatives(r,local_mesh[itype], comp_potential_radial[itype], 1,3);
                        }
                    }
                    //local_pp[MyGlobalElements[i]] += tmp;
                    local_pp[iatom][MyGlobalElements[i]] += tmp;
                    local_pp_dev[iatom][MyGlobalElements[i]] += dev_tmp;
                }
            }
            timer->end("filtering timer1");
            Verbose::single(Verbose::Detail) << " filtering timer1: " << timer->get_elapsed_time("filtering timer1",-1) << " s" << std::endl;
        }

        //short_potential_radial.clear();
        //local_mesh.clear();
        //local_cutoff.clear();
        timer->end("single-grid integration");
        Verbose::single(Verbose::Simple) << std::endl;
        Verbose::single(Verbose::Simple) << "#---------------------------------------------- Upf2KB::calculate_local" << std::endl;
        Verbose::single(Verbose::Simple) << " Time to compute single-grid integration: " << timer->get_elapsed_time("single-grid integration",-1) << " s" << std::endl;
        Verbose::single(Verbose::Simple) << "#---------------------------------------------------------------------------" << std::endl;


    }
    //arrange_local_pp();
    return;
}

void Upf2KB::make_nonlocal_vectors(){
    //////////////Nonlocal potential ///////////////////
    vector<std::array<double,3> > positions = atoms->get_positions();
    for(int iatom=0;iatom<atoms->get_size();iatom++){
        //int itype = atoms->get_atom_type(iatom);
        vector<vector<vector<vector<double> > > > nonlocal_pp;
        vector<vector<vector<vector<double> > > > nonlocal_dev_x;
        vector<vector<vector<vector<double> > > > nonlocal_dev_y;
        vector<vector<vector<vector<double> > > > nonlocal_dev_z;
        vector<vector<vector<vector<int> > > > nonlocal_pp_index;
        calculate_nonlocal(iatom,positions[iatom],nonlocal_pp,nonlocal_dev_x,nonlocal_dev_y,nonlocal_dev_z,nonlocal_pp_index);  //calculate nonlocal potential
        V_vector.push_back(nonlocal_pp);
        V_dev_x.push_back(nonlocal_dev_x);
        V_dev_y.push_back(nonlocal_dev_y);
        V_dev_z.push_back(nonlocal_dev_z);
        V_index.push_back(nonlocal_pp_index);

        nonlocal_pp.clear();
        nonlocal_dev_x.clear();
        nonlocal_dev_y.clear();
        nonlocal_dev_z.clear();
        nonlocal_pp_index.clear();
    }
    return;
}

    /*
       void Upf2KB::arrange_local_pp(){
       int * MyGlobalElements = mesh->get_map()->MyGlobalElements();
       int NumMyElements =mesh->get_map()->NumMyElements();
       double ** tmp1 = new double* [local_pp.size()];
       for(int i=0; i<local_pp.size(); i++){
       tmp1[i] = new double [local_pp[i].size()];
       double* tmp2 = new double[local_pp[i].size()];
       for(int k=0; k<local_pp[i].size();k++)
       tmp2[k] = local_pp[i][k];
//Parallel_Util::group_sum(&local_pp[i][0] , tmp1[i], local_pp[i].size());
Parallel_Util::group_sum(tmp2 , tmp1[i], local_pp[i].size());
delete tmp2;
}
for(int i=0; i<local_pp[0].size(); i++)
cout << i << " " << local_pp[0][i] << " " << local_pp[1][i] << endl;
local_pp.clear();
local_pp.resize(atoms->get_original_size());
int size = mesh->get_original_size();
for(int iatom=0; iatom<atoms->get_original_size(); iatom++){
local_pp[iatom].resize(size);
for(int i=0; i<size; i++){
local_pp[iatom][i] = 0.0;
}
}
for(int i=0; i<local_pp.size(); i++){
for(int j=0; j<NumMyElements; j++){
local_pp[i][MyGlobalElements[j]] = tmp1[i][MyGlobalElements[j]];
}
}
for(int i=0; i< local_pp.size(); i++){
delete [] tmp1[i];
}
delete tmp1;
cout << "check" << endl;

return ;
}
*/
void Upf2KB::initialize_pp(){
    // Filtering non-local pseudopotentials
    for(int itype=0; itype<atoms->get_atom_types().size(); itype++){

        vector<vector<vector<double> > > new_nonlocal_mesh_tmp;
        vector< vector< vector<double> > > new_nonlocal_pp_radial_tmp;
        vector< vector<double> >  new_nonlocal_cutoff_tmp;

        for(int i=0; i<number_vps_file[itype]; i++){
            int l = oamom[itype][i];

            vector<vector<double> > new_nonlocal_mesh_tmp_tmp;
            vector<vector<double> > new_nonlocal_pp_radial_tmp_tmp;
            vector<double>   new_nonlocal_cutoff_tmp_tmp;

            for(int p=0; p<projector_number[itype][i]; p++){
                vector<double> new_nonlocal_mesh_tmp_tmp_tmp;
                vector<double> new_nonlocal_pp_radial_tmp_tmp_tmp;
                for(int t=0; t<nonlocal_mesh[itype][i][p].size(); t++){
                    new_nonlocal_mesh_tmp_tmp_tmp.push_back(nonlocal_mesh[itype][i][p][t]);
                }
                for(int t=0; t<nonlocal_pp_radial[itype][i][p].size(); t++){
                    new_nonlocal_pp_radial_tmp_tmp_tmp.push_back(nonlocal_pp_radial[itype][i][p][t]);
                }
                //vector<double> new_nonlocal_mesh_tmp_tmp_tmp = nonlocal_mesh[itype][i][p];
                //vector<double> new_nonlocal_pp_radial_tmp_tmp_tmp = nonlocal_pp_radial[itype][i][p];
                //double tmp = (nonlocal_pp_radial[itype][i][p][1] - nonlocal_pp_radial[itype][i][p][0]) / (nonlocal_mesh[itype][i][p][1] - nonlocal_mesh[itype][i][p][0]) * (0.0 - nonlocal_mesh[itype][i][p][0]) + nonlocal_pp_radial[itype][i][p][0];
                //new_nonlocal_mesh_tmp_tmp_tmp.insert(new_nonlocal_mesh_tmp_tmp_tmp.begin(), 0.0);
                //new_nonlocal_pp_radial_tmp_tmp_tmp.insert(new_nonlocal_pp_radial_tmp_tmp_tmp.begin(), tmp);
                new_nonlocal_cutoff_tmp_tmp.push_back(nonlocal_cutoff[itype][i][p]);
                new_nonlocal_mesh_tmp_tmp.push_back(new_nonlocal_mesh_tmp_tmp_tmp);
                new_nonlocal_pp_radial_tmp_tmp.push_back(new_nonlocal_pp_radial_tmp_tmp_tmp);
            }
            new_nonlocal_mesh_tmp.push_back(new_nonlocal_mesh_tmp_tmp);
            new_nonlocal_pp_radial_tmp.push_back(new_nonlocal_pp_radial_tmp_tmp);
            new_nonlocal_cutoff_tmp.push_back(new_nonlocal_cutoff_tmp_tmp);
        }
        new_nonlocal_mesh.push_back(new_nonlocal_mesh_tmp);
        new_nonlocal_pp_radial.push_back(new_nonlocal_pp_radial_tmp);
        new_nonlocal_cutoff.push_back(new_nonlocal_cutoff_tmp);
    }
    // end
}

vector< vector< vector<double> > > Upf2KB::get_h_ij_total(){
    return this -> h_ij_total;
}

void Upf2KB::get_vector_address(vector< vector <double> >& local_pp, vector<vector<vector<vector<vector<double> > > > >& V_vector, vector<vector<vector<vector<vector<int> > > > >& V_index, vector< vector<double> >& EKB, vector< vector<int> >& oamom, vector<int>& number_vps_file){
    local_pp = this->local_pp;
    V_vector = this->V_vector;
    V_index = this->V_index;
    EKB = this->EKB;
    oamom = this->oamom;
    number_vps_file = this->number_vps_file;
}
