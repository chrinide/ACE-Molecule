#include "PCMSolver.hpp"
#include <vector>
#include <array>
#include <string>
#include <cmath>
#include <algorithm>
#include <cmath>
#include "Epetra_Map.h"

#include "../Io/Atoms.hpp"
#include "../Util/Tricubic_Interpolation.hpp"
//#include "../Util/Trilinear_Interpolation.hpp"
#include "../Util/Verbose.hpp"
#include "../Util/Parallel_Manager.hpp"

using std::vector;
using std::array;
using std::string;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::Array;

#ifdef ACE_PCMSOLVER
void host_writer(const char * message){Verbose::single(Verbose::Simple) << message << std::endl;}

PCMSolver::PCMSolver(Teuchos::RCP<const Basis> mesh, string solver_type, PCMInput pcm_input, double charge_width/* = 1.0*/, bool is_neq/* = false*/){
    this -> mesh = mesh;
    this -> host_input = pcm_input;
    this -> input_filename = "";
    this -> is_neq = is_neq;
    this -> alpha = charge_width;
    this -> timer = rcp(new Time_Measure());
}

PCMSolver::PCMSolver(Teuchos::RCP<const Basis> mesh, string input_filename, double charge_width/* = 1.0*/, bool is_neq/* = false*/){
    this -> mesh = mesh;
    this -> input_filename = "";
    this -> is_neq = is_neq;
    this -> alpha = charge_width;
    this -> timer = rcp(new Time_Measure());
}

PCMSolver::~PCMSolver(){
    delete[] this -> grid;
    delete[] this -> areas;

    Parallel_Manager::info().all_barrier();
    if(Parallel_Manager::info().get_mpi_rank()==0){
        pcmsolver_delete(this -> pcm_context);
    }
    this -> timer -> print(Verbose::single(Verbose::Normal));
}

void PCMSolver::initialize(RCP<const Atoms> atoms, std::vector<double> mol_charges){
    this -> pseudo_charges = vector<double>(atoms -> get_size());
    for(int i = 0; i < atoms -> get_size(); i++){
        pseudo_charges[i] = mol_charges.at(i);
    }

    vector< array<double,3> > mol_coordinates = atoms -> get_positions();

    const int nuc_size = atoms -> get_size();
    double * atom_numbers = new double[nuc_size];
    double * charges = new double[nuc_size];
    double * coordinates = new double[nuc_size*3];

    for(int i = 0; i < nuc_size; ++i){
        atom_numbers[i] = atoms -> get_atomic_numbers()[i];
        charges[i] = this -> pseudo_charges[i];
        for(int j = 0; j < 3; ++j){
            coordinates[3*i+j] = mol_coordinates[i][j];
        }
    }

    // C1 is fine.
    int symmetry_info[4] = {0,0,0,0};

    Parallel_Manager::info().all_barrier();
    this -> timer -> start("PCMSolver::init::grid initialization");
    if(Parallel_Manager::info().get_mpi_rank()==0){
        if (!pcmsolver_is_compatible_library()) {
            Verbose::all() << "PCMSolver library not compatible" << std::endl;
            exit(EXIT_FAILURE);
        }
        if(this -> input_filename.size() == 0){
            this -> pcm_context = pcmsolver_new(PCMSOLVER_READER_HOST, nuc_size, atom_numbers, coordinates, symmetry_info, &this -> host_input, host_writer);
        } else {
            this -> pcm_context = pcmsolver_new_v1112(PCMSOLVER_READER_OWN, nuc_size, atom_numbers, coordinates, symmetry_info, this -> input_filename.c_str(), NULL, host_writer);
        }
        this -> grid_size = pcmsolver_get_cavity_size(pcm_context);
        pcmsolver_print(this -> pcm_context);
    }

    delete[] charges;
    delete[] coordinates;
    delete[] atom_numbers;

    Parallel_Manager::info().all_barrier();
    Parallel_Manager::info().group_bcast(&this -> grid_size, 1, 0);

    this -> grid = new double[3 * this -> grid_size]();
    this -> areas = new double[this -> grid_size]();

    if(Parallel_Manager::info().get_mpi_rank() == 0){
        pcmsolver_get_centers(this -> pcm_context, this -> grid);
        pcmsolver_get_areas(this -> pcm_context, this -> areas);
    }

    Parallel_Manager::info().all_barrier();
    Parallel_Manager::info().group_bcast(this -> areas, this -> grid_size, 0);
    Parallel_Manager::info().group_bcast(this -> grid, 3*this -> grid_size, 0);

    this -> timer -> end("PCMSolver::init::grid initialization");
}

RCP<const Epetra_Vector> PCMSolver::get_PCM_local_potential(){
    return PCM_local_potential;
}

double * PCMSolver::nuclear_mep(std::vector<double> charges, std::vector< std::array<double,3> > coordinates,
    int grid_size, double * grid
){
    double * mep = new double[grid_size]();
    int nr_nuclei = charges.size();
    for (int i = 0; i < nr_nuclei; i++) {
        for (int j = 0; j < grid_size; j++) {
            // Column-major ordering. Offsets: col_idx * nr_rows + row_idx
            double dist = pow((coordinates[i][0] - grid[j * 3]), 2) +
                pow((coordinates[i][1] - grid[j * 3 + 1]), 2) +
                pow((coordinates[i][2] - grid[j * 3 + 2]), 2);
            mep[j] += charges[i] / sqrt(dist);
        }
    }

    return mep;
}

double * PCMSolver::electron_mep(RCP<const Basis> mesh, RCP<Epetra_Vector> hartree_potential, int grid_size, double* grid){
    vector< std::array<double,3> > PCM_grid_positions;
    std::vector<double> PCM_potential;
    double* e_mep = new double[grid_size]();
    for (int i=0; i<grid_size; i++){
        std::array<double,3> PCM_grid;
        std::copy(&grid[3*i], &grid[3*i]+3, PCM_grid.begin());
        PCM_grid_positions.push_back(PCM_grid);
    }

    PCM_potential = Interpolation::Tricubic::interpolate(mesh,hartree_potential,PCM_grid_positions);
    //PCM_potential = Interpolation::Trilinear::interpolate(mesh, hartree_potential, PCM_grid_positions);

    for(int i = 0; i < grid_size; i++){
        e_mep[i]=PCM_potential.at(i);
    }

    return e_mep;
}

Teuchos::RCP<Epetra_Vector> PCMSolver::calculate_polarization_potential(RCP<const Basis> mesh, double alpha, int grid_size, double* areas, double* grid_charge, double* grid){
    const double** scaled_grid = mesh ->get_scaled_grid();
    double p1 = 0.119763;
    double p2 = 0.205117;
    double q1 = 0.137546;
    double q2 = 0.434344;

    int NumMyElements = mesh -> get_map() -> NumMyElements();

    //double* potential_array= new double[mesh->get_original_size()]();
    double* potential_array= new double[NumMyElements]();
    int* MyGlobalElements = mesh->get_map()->MyGlobalElements();
#pragma omp parallel for
    for(int jl = 0; jl < NumMyElements; jl++){
        int j = MyGlobalElements[jl];
        int j_x, j_y, j_z;
        mesh -> decompose(j,&j_x, &j_y, &j_z);
        double dot_potential = 0;
        double constant_value = 2./pow(M_PI * alpha, 0.5);
        for(int i = 0; i < grid_size; i++){
            double r2 = pow(grid[3*i]-scaled_grid[0][j_x],2) + pow(grid[3*i+1]-scaled_grid[1][j_y],2) + pow(grid[3*i+2]-scaled_grid[2][j_z],2);
            double x2 = r2/(alpha*areas[i]);
            double x = sqrt(x2);
            dot_potential -= (grid_charge[i]*constant_value/sqrt(areas[i])*(1+p1*x+p2*x2))/(1+q1*x+q2*x2+p2*x2*x);
        }
        //potential_array[j] = dot_potential;
        potential_array[jl] = dot_potential;
    }

    int * indices = new int[NumMyElements];
    for(int i = 0; i < NumMyElements;i++){
        indices[i] = i;
    }
    RCP<Epetra_Vector> retval = rcp(new Epetra_Vector(*mesh -> get_map(), false));
    //retval -> ReplaceMyValues(NumMyElements, &potential_array[mesh -> get_map() -> MinMyGID()], indices);
    retval -> ReplaceMyValues(NumMyElements, potential_array, indices);
    delete[] indices;
    delete[] potential_array;
    return retval;
}

double PCMSolver::compute( RCP<State> state, RCP<Epetra_Vector> hartree_potential){
    double energy = 0.0;

    if(Parallel_Manager::info().get_mpi_rank()==0 and this -> pcm_context == NULL){
        Verbose::all() << "PCMSolver::initialize is not called!" << std::endl;
        throw std::logic_error("PCMSolver::initialize is not called! (wrong code)");
    }

    this -> timer -> start("PCMSolver::compute");

    double * asc_Ag = new double[grid_size]();
    this -> timer -> start("PCMSolver::compute::MEP");
    double * e_mep = electron_mep(mesh, hartree_potential, this -> grid_size, this -> grid);
    double * mep = nuclear_mep(this -> pseudo_charges, state -> get_atoms() -> get_positions(), this -> grid_size, this -> grid);

    for(int i = 0; i < this -> grid_size; i++){
        mep[i] -= e_mep[i];
    }

    this -> timer -> end("PCMSolver::compute::MEP");
    this -> timer -> start("PCMSolver::compute::ASC");
    if(Parallel_Manager::info().get_mpi_rank() == 0){
        const char * mep_lbl = {"NucMEP"};
        // Set MEP into the PCMSolver.
        pcmsolver_set_surface_function(this -> pcm_context, this -> grid_size, mep, mep_lbl);

        const char * asc_lbl = {"NucASC"};
        int irrep = 0;
        if(this->is_neq){
            pcmsolver_compute_asc(this -> pcm_context, mep_lbl, asc_lbl, irrep);
        } else {
            pcmsolver_compute_response_asc(this -> pcm_context, mep_lbl, asc_lbl, irrep);
        }
        pcmsolver_get_surface_function(this -> pcm_context, this -> grid_size, asc_Ag, asc_lbl);
        energy = pcmsolver_compute_polarization_energy(this -> pcm_context, mep_lbl, asc_lbl);
    }


    delete[] mep;
    delete[] e_mep;
    this -> timer -> end("PCMSolver::compute::ASC");

    Parallel_Manager::info().group_bcast(asc_Ag, this -> grid_size, 0);
    Parallel_Manager::info().group_bcast(&energy, 1, 0);

    this -> timer -> start("PCMSolver::compute::polarization potential");

    //double * potential_array = calculate_polarization_potential(mesh, this -> alpha, this -> grid_size, this -> areas, asc_Ag, this -> grid);
    this -> PCM_local_potential = calculate_polarization_potential(mesh, this -> alpha, this -> grid_size, this -> areas, asc_Ag, this -> grid);
    delete[] asc_Ag;

    this -> timer -> end("PCMSolver::compute::polarization potential");

    this -> timer -> end("PCMSolver::compute");
    this -> timer -> print(Verbose::single(Verbose::Simple), -1);
    return energy;
}

#endif
