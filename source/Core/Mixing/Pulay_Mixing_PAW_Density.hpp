#pragma once
//#include <string>
#include <vector>
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"

#include "../../State/Scf_State.hpp"
#include "Pulay_Mixing.hpp"
#include "Pulay_Mixing_Density.hpp"

/**
 * @brief Performs Pulay mixing for PAW density matrix and density.
 * @note See Pulay_Mixing class.
 * @note This class owns Pulay_Mixing_Density to update both PAW density matrix and PS density.
 **/
class Pulay_Mixing_PAW_Density: public Pulay_Mixing {
    public:
        /**
         * @brief Constructor.
         * @param start Before this value, perform linear mixing instead. If this value is less than history+1, then it is set to the history+1.
         * @param history Number of previous PAW density matrix to mix.
         * @param alpha Mixing coefficient for initial linear mixing part.
         * @param pulay_alpha Mixing coefficient for Pulay mixing part.
         **/
        Pulay_Mixing_PAW_Density(
            int start, int history, int max_history,
            double alpha, double pulay_alpha
        );
        /**
         * @brief Inherited from Mixing. See Mixing.
         * @return Always 0.
         **/
        int update(
            Teuchos::RCP<const Basis> mesh, 
            Teuchos::Array< Teuchos::RCP<Scf_State> > states, 
            int i_spin, 
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
        );

    private:
        std::vector< std::vector< std::vector<double> > > np_residue;
        std::vector< std::vector< std::vector<double> > > np_descriptor_in;
        Teuchos::RCP<Pulay_Mixing_Density> pulay_mdensity;
};
