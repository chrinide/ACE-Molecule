#include "Pulay_Mixing.hpp"
#include "Teuchos_LAPACK.hpp"

using std::vector;
using std::complex;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

Pulay_Mixing::Pulay_Mixing(int start, int history, int max_history, double alpha, double pulay_alpha, bool update_paw/* = false*/){
    this -> start = start;
    this -> history = history;

    if( history > start ){
        this->start = history;
    }
    this -> alpha=alpha;
    this -> pulay_alpha = pulay_alpha;
    this -> max_history = max_history;
    this -> used_step_number = history;
    this -> update_paw = update_paw;
}

int Pulay_Mixing::compute(
    Array< RCP<Epetra_Vector> > residue,
    Array< RCP<Epetra_Vector> > density_ins,
    double pulay_alpha, int i_spin,
    RCP<Epetra_Vector>& output,
    RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/
){
    // See Kresse, Phys. Rev. B 54, 11169 (1996)
    // See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html

    int dim  = residue.size();
    Teuchos::SerialDenseMatrix<int, double> pulay_matrix (dim, dim);

    for( int i=0; i<dim; i++){
        for( int j=0; j<dim; j++){
            double result = 0.0;
            residue[i] -> Dot(*residue[j], &result);
            pulay_matrix(i, j) = result;
        }
    }

    Teuchos::LAPACK<int, double> lapack;
    int info;
    int* IPIV=new int[dim]();
    int LWORK = dim;
    double* WORK = new double[LWORK]();

    // Since POTRI can be applied to symetric positive definite matrix, it cannot be used here.
    // GETRI(obtaining inverse matrix) requres GETRF(PLU decomposition) first
    lapack.GETRF(dim, dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, &info);
    lapack.GETRI(dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, WORK, LWORK, &info);
    delete[] IPIV;
    delete[] WORK;
    if(info != 0){
        //residue.remove(0);
        //density_ins.remove(0);
        //Verbose::single(Verbose::Detail) << "residue.size " << residue.size() << std::endl;
        //compute(residue, density_ins, pulay_alpha, output);
        return -1;
    }
    //lapack.GETRI(dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, WORK, -1, &info);

    /*
    double* b = new double [dim];
    for(int i=0; i<dim; i++){
        b[i] = 0.0;
    }
    */
    vector<double> b(dim, 0.0);

    for(int i=0; i<dim; i++){
        for(int j=0; j<dim; j++){
            b[i] += pulay_matrix(j, i);
        }
    }

    double b_norm = 0.0;
    for(int i=0; i<dim; i++){
        b_norm += b[i];
    }

    for(int i=0; i<dim; i++){
        b[i] /= b_norm;
    }

    // This old version, uses
    // rho_in(n+1) = sum_i^n b_i ( pulay_alpha*rho_out(i) + (1-pulay_alpha)*rho_in(i))
    // which follows the reference
//    /*
    output->PutScalar(0.0);
    for(int i=0;i<dim;i++){
        RCP<Epetra_Vector> temp_density = rcp(new Epetra_Vector( *density_ins[i] ));
//        temp_density -> Update(this->pulay_alpha, *residue[i], 1.0);
        temp_density -> Update(pulay_alpha, *residue[i], 1.0);
        output -> Update(b[i], *temp_density, 1.0);
    }
//    */
    if(nuclear_potential != Teuchos::null and this -> update_paw){
        nuclear_potential -> mix_nuclear_potential( b, 1.0, i_spin );
    }

    // I think this new version should
    // rho_in(n+1) = pulay_alpha * [sum_i^n b_i ( rho_out(i) - rho_in(i))] + (1-pulay_alpha) * rho_in(n)
    // It respects just previous input only, and does not count of ancient density.
    // Also easier to code here, because output already contains rho_in(n)
    // But confirmed to be worse than previous one.
    /*
    for(int i = 0; i < dim; ++i){
        output -> Update( b[i]*this->pulay_alpha, *residue[i], 1.0 );
    }
    */

    //delete[] b;

    return 0;
}

int Pulay_Mixing::compute(
    vector< vector<double> > residue,
    vector< vector<double> > density_ins,
    double pulay_alpha, int i_spin,
    vector<double> &output
){
    // See Kresse, Phys. Rev. B 54, 11169 (1996)
    // See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html

    int dim = residue.size();
    int vec_size = residue[0].size();
    Teuchos::SerialDenseMatrix<int, double> pulay_matrix (dim, dim);

    for( int i=0; i<dim; i++){
        for( int j=0; j<dim; j++){
            double result = 0.0;
            for(int k = 0; k < vec_size; ++k){
                result += residue[i][k] * residue[j][k];
            }
            pulay_matrix(i, j) = result;
        }
    }

    Teuchos::LAPACK<int, double> lapack;
    int info;
    int* IPIV=new int[dim]();
    int LWORK = dim;
    double* WORK = new double[LWORK]();

    // Since POTRI can be applied to symetric positive definite matrix, it cannot be used here.
    // GETRI(obtaining inverse matrix) requres GETRF(PLU decomposition) first
    lapack.GETRF(dim, dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, &info);
    lapack.GETRI(dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, WORK, LWORK, &info);
    delete[] IPIV;
    delete[] WORK;
    if(info != 0){
        return -1;
    }

    vector<double> b(dim, 0.0);
    for(int i=0; i<dim; i++){
        for(int j=0; j<dim; j++){
            b[i] += pulay_matrix(j, i);
        }
    }

    double b_norm = 0.0;
    for(int i=0; i<dim; i++){
        b_norm += b[i];
    }

    for(int i=0; i<dim; i++){
        b[i] /= b_norm;
    }

    // This old version, uses
    // rho_in(n+1) = sum_i^n b_i ( pulay_alpha*rho_out(i) + (1-pulay_alpha)*rho_in(i))
    // which follows the reference
    output.clear();
    output.resize(vec_size);
    for(int i = 0; i < dim; ++i){
        for(int j = 0; j < vec_size; ++j){
            output[j] += b[i] * (density_ins[i][j] + pulay_alpha * residue[i][j]);
        }
    }
    return 0;
}
int Pulay_Mixing::compute(
        std::vector< std::vector<std::complex<float> > > residue,
        std::vector< std::vector<std::complex<float> > > pol_ins,
        double pulay_alpha,
        std::vector<std::complex<float> >  &output
        ){
    // See Kresse, Phys. Rev. B 54, 11169 (1996)
    // See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html
    int dim = residue.size();
    int vec_size = residue[0].size();
    Teuchos::SerialDenseMatrix<int, complex<float> > pulay_matrix (dim, dim);

    for( int i=0; i<dim; i++){
        for( int j=0; j<dim; j++){
            complex<float> result = 0.0;
            for(int k = 0; k < vec_size; ++k){
                result += residue[i][k] * residue[j][k];
            }
            pulay_matrix(i, j) = result;
        }
    }
    Teuchos::LAPACK<int, complex<float> > lapack;
    int info;
    int* IPIV=new int[dim]();
    int LWORK = dim;
    complex<float>* WORK = new complex<float>[LWORK]();

    // Since POTRI can be applied to symetric positive definite matrix, it cannot be used here.
    // GETRI(obtaining inverse matrix) requres GETRF(PLU decomposition) first
    lapack.GETRF(dim, dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, &info);
    lapack.GETRI(dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, WORK, LWORK, &info);
    delete[] IPIV;
    delete[] WORK;
    if(info != 0){
        return -1;
    }
    vector<complex< float> >  b(dim, 0.0);
    for(int i=0; i<dim; i++){
        for(int j=0; j<dim; j++){
            b[i] += pulay_matrix(j, i);
        }
    }

    complex<float> b_norm = 0.0;
    for(int i=0; i<dim; i++){
        b_norm += b[i];
    }

    for(int i=0; i<dim; i++){
        b[i] /= b_norm;
    }

    // This old version, uses
    // rho_in(n+1) = sum_i^n b_i ( pulay_alpha*rho_out(i) + (1-pulay_alpha)*rho_in(i))
    // which follows the reference
    for(int i = 0; i < dim; ++i){
        for(int j = 0; j < vec_size; ++j){
            output[j] += b[i] * (pol_ins[i][j] + (float) pulay_alpha * residue[i][j]);
        }
    }
    return 0;
}

int Pulay_Mixing::compute(
        std::vector< std::vector<std::complex<float> > > residue,
        std::vector< std::vector<std::complex<float> > > pol_ins,
        double pulay_alpha,
        std::vector<std::complex<float> >  &output ,
        Teuchos::SerialDenseMatrix<int, complex<float> > pulay_matrix
        ){
    // See Kresse, Phys. Rev. B 54, 11169 (1996)
    // See http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html
    int dim = residue.size();
    int vec_size = residue[0].size();
    Teuchos::LAPACK<int, complex<float> > lapack;
    int info;
    int* IPIV=new int[dim]();
    int LWORK = dim;
    complex<float>* WORK = new complex<float>[LWORK]();

    // Since POTRI can be applied to symetric positive definite matrix, it cannot be used here.
    // GETRI(obtaining inverse matrix) requres GETRF(PLU decomposition) first
    lapack.GETRF(dim, dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, &info);
    lapack.GETRI(dim, pulay_matrix.values(), pulay_matrix.stride(), IPIV, WORK, LWORK, &info);
    delete[] IPIV;
    delete[] WORK;
    if(info != 0){
        return -1;
    }
    vector<complex< float> >  b(dim, 0.0);
    for(int i=0; i<dim; i++){
        for(int j=0; j<dim; j++){
            b[i] += pulay_matrix(j, i);
        }
    }

    complex<float> b_norm = 0.0;
    for(int i=0; i<dim; i++){
        b_norm += b[i];
        //b_norm += std::fabs(b[i]);
    }

    for(int i=0; i<dim; i++){
        b[i] /= b_norm;
    }

    // This old version, uses
    // rho_in(n+1) = sum_i^n b_i ( pulay_alpha*rho_out(i) + (1-pulay_alpha)*rho_in(i))
    // which follows the reference
    for(int i = 0; i < dim; ++i){
        for(int j = 0; j < vec_size; ++j){
            output[j] += b[i] * (pol_ins[i][j] + (float) pulay_alpha * residue[i][j]);
        }
    }
    return 0;
}
