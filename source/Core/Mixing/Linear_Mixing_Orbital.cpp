#include "Linear_Mixing_Orbital.hpp"
#include <vector>
#include "Epetra_Vector.h"
#include "Epetra_MultiVector.h"

using Teuchos::Array;
using Teuchos::RCP;

Linear_Mixing_Orbital::Linear_Mixing_Orbital(double alpha)
    :Linear_Mixing(alpha){
    this->mixing_type = "Orbital";
}

int Linear_Mixing_Orbital::update(RCP<const Basis> mesh, Array<RCP<Scf_State> > states, int i_spin, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    //http://ftp.abinit.org/ws07/Liege_Torrent2_SC_Mixing.pdf
    //3rd page
    //states[states.size()-1] = states.size()-2-th out_state
    //states[states.size()-2] = states.size()-2-th in_state

    //equivalent condition orginated from older version of this code
    //Do not mix anything for first step
    if(states.size()==1 || states.size()==2) return 0;

    RCP<Epetra_MultiVector> out_state_orb = states[states.size()-1]->orbitals[i_spin];
    RCP<Epetra_MultiVector> in_state_orb = states[states.size()-2]->orbitals[i_spin];

    RCP<Epetra_MultiVector> output = Teuchos::rcp(new Epetra_MultiVector(*out_state_orb));
    output->Update(1-alpha,*in_state_orb,alpha);

    if( nuclear_potential != Teuchos::null ){
        std::vector<double> coeffs(1, 1.0);
        nuclear_potential -> mix_nuclear_potential( coeffs, alpha, i_spin );
    }
    if(states[states.size()-1]->density.size()>0){
        states[states.size()-1]->density.clear();
    }
    const int size = states[states.size()-1]->orbitals.size();
    if (i_spin<size){
        states[states.size()-1]->orbitals[i_spin] = output;
    }
    else if ((i_spin==0 and size ==0) or (i_spin==1 and size ==1) ){
        states[states.size()-1]->orbitals.append( output );
    }
    else{
        Verbose::all() << "Linear_Mixing_Orbitals: i_spin: " << i_spin << std::endl
                       << "Linear_Mixing_Orbitals: size:" << size << std::endl
                       << "Linear_Mixing_Orbitals: invalid access" << std::endl;
        exit(-1);
    }
    return 0;
}

/*
void Linear_Mixing_Density::update(Teuchos::RCP<Epetra_MultiVector> eigenvectors, Teuchos::Array<Occupation* > occupations, Teuchos::RCP<Epetra_MultiVector> new_density, int sp_polarize){
    Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > orbitals;
    orbitals.push_back( Teuchos::rcp(new Epetra_MultiVector(*eigenvectors)) );

    update(orbitals, occupations, new_density, sp_polarize);

    orbitals.clear();

    return;
}
*/
