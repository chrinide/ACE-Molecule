#pragma once
#include <vector>
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"
#include "Mixing.hpp"

/**
 * @brief Abstract class that includes Broyden mixing computation routine.
 * @author Sungwoo Kang
 * @note See PRC 78 014318 (2008) Section II.B
 * @date 2015
 **/
class Broyden_Mixing: public Mixing{
    public:
        /**
         * @brief Default constructor of Broyden_Mixing
         * @param start Before this value, perform linear mixing instead. If this value is less than history+1, then it is set to the history+1.
         * @param history Number of previous PAW density matrix to mix.
         * @param alpha Mixing coefficient for initial linear mixing part.
         * @param broyden_alpha Mixing coefficient for Broyden mixing part.
         * @param w_0 See PRC 78 014318 (2008) Section II.B, default = 0.01
         * @param w_m See PRC 78 014318 (2008) Section II.B, default = NULL
         */
        Broyden_Mixing(
            int start,
            int history,
            double alpha,
            double broyden_alpha,
            double w_0 = 0.01, double *w_m = NULL);
        /**
         * @brief Inherited from Mixing, but still pure virtual.
         **/
        virtual int update(
            Teuchos::RCP<const Basis> mesh, 
            Teuchos::Array< Teuchos::RCP<Scf_State> > states, 
            int i_spin, 
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
        ) = 0;
    protected:
        /**
         * @brief Performs Broyden mixing for Epetra_Vector.
         * @param residue Array of f_out-f_in.
         * @param density_ins Array of f_in.
         * @param alpha Mixing coeff. (1-alpha)*f_in+alpha*f_mixed.
         * @param i_spin Spin index (exists for PAW density matrix support).
         * @param w_0 See reference paper in note.
         * @param w_m See reference paper in note.
         * @param output Mixed vector.
         * @param nuclear_potential PAW density matrix mixing support.
         * @note See PRC 78 014318 (2008) Section II.B
         **/
        int compute(
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > residue, 
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density_ins, 
            double alpha, int i_spin, double w_0, std::vector<double> w_m, 
            Teuchos::RCP<Epetra_Vector>& output, 
            Teuchos::RCP<Nuclear_Potential> nuclear_potential = Teuchos::null
        );
        /**
         * @brief Performs Broyden mixing for std::vector. Used for PAW_Density
         * @param residue Array of f_out-f_in.
         * @param density_ins Array of f_in.
         * @param alpha Mixing coeff. (1-alpha)*f_in+alpha*f_mixed.
         * @param i_spin Spin index (exists for PAW density matrix support).
         * @param w_0 See reference paper in note.
         * @param w_m See reference paper in note.
         * @param output Mixed vector.
         * @note See PRC 78 014318 (2008) Section II.B
         **/
        int compute(
            std::vector< std::vector<double> > residue, 
            std::vector< std::vector<double> > density_ins, 
            double alpha, int i_spin, double w_0, std::vector<double> w_m, 
            std::vector<double> &output 
        );
        double alpha, broyden_alpha;
        int start, history;
        double w_0;
        std::vector<double> w_m;
        Teuchos::Array< Teuchos::Array< Teuchos::RCP<Epetra_Vector> > > residue; // residue[i_spin][history_index]
};
