#include "Broyden_Mixing_Density.hpp"
#include <vector>

using std::vector;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

Broyden_Mixing_Density::Broyden_Mixing_Density(int start, int history, double alpha, double broyden_alpha, double w_0/* = 0.01;*/, double* w_m/* = NULL*/)
    : Broyden_Mixing(start, history, alpha, broyden_alpha, w_0, w_m){
    this->mixing_type = "Density";
}

int Broyden_Mixing_Density::update( RCP<const Basis> mesh, Array< RCP<Scf_State> > states, int i_spin, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    const int size = states[states.size()-1]->density.size();

    RCP<Epetra_Vector> output = rcp( new Epetra_Vector(*states[states.size()-1]->density[i_spin]) );

    //when states.size()==2
    //states[0] = state_in(0)
    //states[1] = state_out(0)
    this -> residue.resize(size);
    if(states.size()==2){
        if(i_spin==0){
            Array<RCP<Epetra_Vector> > tmp;
            tmp.append( rcp(new Epetra_Vector(output->Map())) );
            tmp[0]->Update(1.0, *states[1]->density[i_spin], -1.0, *states[0]->density[i_spin], 0.0);
            residue.append(tmp);
        } else {
            residue[i_spin].append( rcp(new Epetra_Vector(output->Map())) );
            residue[i_spin][0]->Update(1.0, *states[1]->density[i_spin], -1.0, *states[0]->density[i_spin], 0.0);
        }
    }

    if(states.size()-1 < start){
        RCP<const Epetra_Vector> density_in = states[states.size()-2]->density[i_spin];
        output->Update(1-this->alpha, *density_in, this->alpha);
        if(states.size()>2){
            residue[i_spin].append( rcp(new Epetra_Vector(output->Map())));
            residue[i_spin][residue[i_spin].size()-1]->Update(1.0, *states[states.size()-1]->density[i_spin], -1.0, *states[states.size()-2]->density[i_spin], 0.0);
        }
        if( nuclear_potential != Teuchos::null ){
            vector<double> coeffs(1, 1.0);
            nuclear_potential -> mix_nuclear_potential( coeffs, alpha, i_spin );
        }
    }
    else{
        Array< RCP<Epetra_Vector> > density_ins;
        for(int i=0;i<history+1;i++){
            // Appends multivector object having polarize+1 vectors to residue
            density_ins.append( rcp(new Epetra_Vector(*states[states.size()-history+i-2]->density[i_spin])) );// NEED history+1 density_in

            // Assign value to the residue. value: save_densities[i]-save_densities[i-1]
            // residue = rho_out - rho_in
        }
        residue[i_spin].append( rcp(new Epetra_Vector(output->Map())));
        residue[i_spin][residue[i_spin].size()-1]->Update(1.0, *states[states.size()-1]->density[i_spin], -1.0, *states[states.size()-2]->density[i_spin], 0.0);

        while(residue[i_spin].size()>history+1){// NEED history+1 residue
            residue[i_spin].remove(0);
        }
//        compute(residue, density_ins, output);
        if( nuclear_potential != Teuchos::null ){
            Broyden_Mixing::compute(residue[i_spin], density_ins, broyden_alpha, i_spin, w_0, w_m, output, nuclear_potential);
        } else {
            Broyden_Mixing::compute(residue[i_spin], density_ins, broyden_alpha, i_spin, w_0, w_m, output);
        }
    }

    if (i_spin<size){
        states[states.size()-1]->density[i_spin] = output;
        //state->density[i_spin] = output;
    }
    else if ((i_spin==0 and size ==0) or (i_spin==1 and size ==1) ){
        //state->density.append( output );
        states[states.size()-1]->density.append( output );
    }
    else{
        Verbose::all() << "Broyden_Mixing_Density: i_spin: " << i_spin << std::endl
                       << "Broyden_Mixing_Density: size:" << size << std::endl
                       << "Broyden_Mixing_Density: invalid access" << std::endl;
        exit(-1);
    }
    return 0;
}
