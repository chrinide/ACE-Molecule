#include "Pulay_Mixing_Polarizability.hpp"
#include "Epetra_Vector.h"

using std::complex;
using std::vector;
using std::array;
using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

Pulay_Mixing_Polarizability::Pulay_Mixing_Polarizability(int start, int history, int max_history, double alpha, double pulay_alpha)
    :Pulay_Mixing(start, history, max_history, alpha, pulay_alpha){
        this->mixing_type = "Polarizability";
    }

int Pulay_Mixing_Polarizability::update( RCP<const Basis> mesh, Array< RCP<Scf_State> > states, int wavelength, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    update1(mesh, states, wavelength, nuclear_potential);
    //update2(mesh, states, wavelength, nuclear_potential);
    return 0;
}

int Pulay_Mixing_Polarizability::update1( RCP<const Basis> mesh, Array< RCP<Scf_State> > states, int wavelength, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    // 2015. 06. 25. - Jaewook Kim
    // NEW VERSION is wrong! (not converged, density_in(n-1) = density_mix(n) != density_out(n) )
    // now this code follows old version.
    // Storing "residue" = density_out - density_in
    vector<std::array<complex<float>,9 > > output = states[states.size()-1]->polarizability[wavelength];

     
    int num_atom = output.size();
    if(states.size()-1 < start){
        vector<std::array<complex<float>,9 > > in = states[states.size()-2]->polarizability[wavelength];
        for(int i = 0; i < num_atom; i++){
            for(int j = 0; j < 9; j++){
                output[i][j] = output[i][j]*((float)alpha) + in[i][j]*(1-(float)alpha);
            }
        }
        std::vector< std::complex<float> > tmp_residue (output.size()*9);
        for(int i = 0; i < output.size(); ++i){
            for(int j = 0; j < 9; j++){
                tmp_residue[i*9+j] =  output[i][j]- in[i][j];
            }
        }
        pol_residue.push_back(tmp_residue);
        states[states.size()-1]->polarizability[wavelength] = output;
    } else {
        std::vector< std::vector< std::complex<float> > > pol_ins;
        for(int his=0; his<history; his++){
            std::vector< std::complex<float> > tmp(9*num_atom);
            for(int i=0; i<num_atom; i++){
                for(int j=0; j<9; j++){
                    tmp[i*9+j] = states[states.size()-history+his-1]->polarizability[wavelength][i][j];
                }
            }
            pol_ins.push_back(tmp );
        }
        // Assign value to the residue. value: save_pol[i]-save_pol[i-1]
        // residue = rho_out - rho_in
        pol_residue.push_back(std::vector<std::complex<float> > (num_atom*9));
        for(int iatom = 0; iatom < num_atom; ++iatom){
            for(int j=0; j<9; j++)
                pol_residue[pol_residue.size()-1][iatom*9+j] = states[states.size()-1]->polarizability[wavelength][iatom][j] - states[states.size()-2]->polarizability[wavelength][iatom][j];
        }

        while(this -> pol_residue.size()>history){
            this -> pol_residue.erase(pol_residue.begin());
        }


        //        compute(residue, density_ins, output);

        std::vector<std::complex<float> > conti_output(num_atom*9);
        int dim = pol_residue.size();
        int vec_size = pol_residue[0].size();
        Teuchos::SerialDenseMatrix<int, std::complex<float> > pulay_matrix (dim, dim);
        //if(saved_pulay_matrix==Teuchos::null){
        if(!is_saved_pulay_matrix){
            is_saved_pulay_matrix = true;
            for( int i=0; i<dim; i++){
                for( int j=0; j<dim; j++){
                    complex<float> result = 0.0;
                    for(int k = 0; k < vec_size; ++k){
                        result += pol_residue[i][k] * pol_residue[j][k];
                    }
                    pulay_matrix(i, j) = result;
                }
            }
            saved_pulay_matrix = pulay_matrix;
        }
        else{
            if(size_of_saved_pulay_matrix!=dim){
                size_of_saved_pulay_matrix=dim;
                for( int i=0; i<pol_residue.size()-1; i++){
                    for( int j=0; j<pol_residue.size()-1; j++){
                        pulay_matrix(i, j) = saved_pulay_matrix(i,j);
                    }
                }
                for( int i=0; i<dim; i++){
                    complex<float> result = 0.0;
                    for(int k = 0; k < vec_size; ++k){
                        result += pol_residue[i][k] * pol_residue[dim-1][k];
                    }
                    pulay_matrix(i, dim-1) = result;
                    pulay_matrix(dim-1, i) = result;
                }
            }
            else{
                for( int i=1; i<pol_residue.size(); i++){
                    for( int j=1; j<pol_residue.size(); j++){
                        pulay_matrix(i-1, j-1) = saved_pulay_matrix(i,j);
                    }
                }
                for( int i=0; i<dim; i++){
                    complex<float> result = 0.0;
                    for(int k = 0; k < vec_size; ++k){
                        result += pol_residue[i][k] * pol_residue[dim-1][k];
                    }
                    pulay_matrix(i, dim-1) = result;
                    pulay_matrix(dim-1, i) = result;
                }
            }
            saved_pulay_matrix = pulay_matrix;
        }

        //int ierr = Pulay_Mixing::compute(pol_residue, pol_ins, pulay_alpha, conti_output);
        int ierr = Pulay_Mixing::compute(pol_residue, pol_ins, pulay_alpha, conti_output, pulay_matrix);
        //int ierr = 0;
        //exit(-1);
        if( ierr < 0 ){
            vector<std::array<complex<float>,9 > > in = states[states.size()-2]->polarizability[wavelength];
            for(int i = 0; i < num_atom; i++){
                for(int j = 0; j < 9; j++){
                    conti_output[i*9+j] = output[i][j]*((float) alpha)+ in[i][j]*(1-(float) alpha);
                }
            }
        }

        for(int i = 0; i < num_atom; i++){
            for(int j = 0; j < 9; j++){
                states[states.size()-1]->polarizability[wavelength][i][j] = conti_output[i*9+j];
            }
        }
        if(history<max_history)
            history++;
    }
    return 0;
}

int Pulay_Mixing_Polarizability::update2( RCP<const Basis> mesh, Array< RCP<Scf_State> > states, int wavelength, RCP<Nuclear_Potential> nuclear_potential/* = Teuchos::null*/){
    /*
    // 2015. 06. 25. - Jaewook Kim
    // NEW VERSION is wrong! (not converged, density_in(n-1) = density_mix(n) != density_out(n) )
    // now this code follows old version.
    // Storing "residue" = density_out - density_in
    int num_atom = states[states.size()-1]->polarizability[wavelength].size();
    if(pol_residue2.size()==0)
        pol_residue2.resize(num_atom);
    for(int iatom = 0; iatom < num_atom; iatom++){
        if(states.size()-1 < start){
            std::array<complex<float>,9 > in = states[states.size()-2]->polarizability[wavelength][iatom];
            std::array<complex<float>,9 > output = states[states.size()-1]->polarizability[wavelength][iatom];
            for(int j = 0; j < 9; j++){
                output[j] = output[j]*((float)alpha) + in[j]*(1-(float)alpha);
            }

            std::vector< std::complex<float> > tmp_residue (9);
            for(int j = 0; j < 9; j++){
                tmp_residue[j] =  output[j]- in[j];
            }
            pol_residue2[iatom].push_back(tmp_residue);
            states[states.size()-1]->polarizability[wavelength][iatom] = output;
        } else {
            std::vector< std::vector< std::complex<float> > > pol_ins;
            for(int his=0; his<history; his++){
                std::vector< std::complex<float> > tmp_his(9);
                for(int j=0; j<9; j++)
                    tmp_his[j] = states[states.size()-history+his-1]->polarizability[wavelength][iatom][j];
                pol_ins.push_back(tmp_his);
            }
            // Assign value to the residue. value: save_pol[i]-save_pol[i-1]
            // residue = rho_out - rho_in
            
            pol_residue2[iatom].push_back(std::vector<std::complex<float> > (9));
            for(int j=0; j<9; j++)
                pol_residue2[iatom][pol_residue2[iatom].size()-1][j] = states[states.size()-1]->polarizability[wavelength][iatom][j] - states[states.size()-2]->polarizability[wavelength][iatom][j];
            while(this -> pol_residue2[iatom].size()>history){
                this -> pol_residue2[iatom].erase(pol_residue2[iatom].begin());
            }

            //        compute(residue, density_ins, output);

            std::vector<std::complex<float> > output(9);
            std::array<complex<float>,9 > in = states[states.size()-2]->polarizability[wavelength][iatom];
            int dim = pol_residue2[iatom].size();
            int vec_size = 9;
            int ierr = Pulay_Mixing::compute(pol_residue2[iatom], pol_ins, pulay_alpha, output);
            //int ierr = 0;
            //exit(-1);
            if( ierr < 0 ){
                for(int j = 0; j < 9; j++){
                    output[j] = output[j]*((float) alpha)+ in[j]*(1-(float) alpha);
                }
            }
            for(int j=0; j<9; j++)
                states[states.size()-1]->polarizability[wavelength][iatom][j] = output[j];
            //history++;

        }
        */
    return 0;
}
