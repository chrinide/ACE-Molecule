#include "Create_Mixing.hpp"
#include <iostream>

#include "Linear_Mixing_Density.hpp"
#include "Linear_Mixing_Potential.hpp"
#include "Linear_Mixing_Orbital.hpp"
#include "Pulay_Mixing_Density.hpp"
#include "Pulay_Mixing_Potential.hpp"
#include "Pulay_Mixing_Polarizability.hpp"
#include "Broyden_Mixing_Density.hpp"
#include "Broyden_Mixing_Potential.hpp"

#ifdef USE_PAW
#include "Linear_Mixing_PAW_Density.hpp"
#include "Pulay_Mixing_PAW_Density.hpp"
#include "Broyden_Mixing_PAW_Density.hpp"
#endif

using Teuchos::RCP;
using Teuchos::rcp;

RCP<Mixing> Create_Mixing::Create_Mixing(RCP<Teuchos::ParameterList> parameters){
    int mixing_method = parameters->sublist("Mixing").get<int>("MixingMethod", 1);
    std::string mixing_type = parameters->sublist("Mixing").get<std::string>("MixingType", "Potential");
    parameters -> sublist("Mixing").get<double>("MixingParameter", 0.3);

    bool update_paw = true;
    if(mixing_type == "Density"){
        update_paw = (parameters -> sublist("Mixing").get<int>("UpdatePawMatrix", 1) == 1);
    } else {
        update_paw = (parameters -> sublist("Mixing").get<int>("UpdatePawMatrix", 0) == 1);
    }


//MixingMethod # 0: linear mixing | 1: Pulay mixing | 2: Broyden mixing
    RCP<Mixing> mixing;
//    if (mixing_type=="Density" and mixing_method==0 ){
    if (mixing_method==0){
        double alpha=parameters->sublist("Mixing").get<double>("MixingParameter");
        if(mixing_type=="Density"){
            mixing=rcp(new Linear_Mixing_Density(alpha, update_paw) );
        }
        else if(mixing_type=="Potential"){
            mixing=rcp(new Linear_Mixing_Potential(alpha, update_paw) );
        }
        else if(mixing_type=="Orbital"){
            mixing=rcp(new Linear_Mixing_Orbital(alpha) );
        }
#ifdef USE_PAW
        else if(mixing_type=="PAW_Density"){
            mixing=rcp(new Linear_Mixing_PAW_Density(alpha) );
        }
#endif
        else{
            Verbose::all() << "Create_Mixing::unknown mixing_type" << std::endl;
            exit(-1);
        }
    }
//    else if (mixing_type=="Density" and mixing_method==1 ){
    else if (mixing_method==1){  //< Pulay case
        double alpha=parameters->sublist("Mixing").get<double>("MixingParameter");
        double pulay_alpha = parameters->sublist("Mixing").get<double>("PulayMixingParameter", alpha);
        int start = parameters ->sublist("Mixing").get<int>("MixingStart", 6);
        int history = parameters ->sublist("Mixing").get<int>("MixingHistory", 4);
        int max_history = parameters ->sublist("Mixing").get<int>("MaxHistory", 0);

        if(start<history){
            Verbose::all() << "MixingStart has to be equal or greater than Mixing_History!"<<std::endl;
            std::cout << "history : " << history << std::endl;
            std::cout << "start : " << start << std::endl;

            exit(EXIT_FAILURE);
        }
        if(mixing_type=="Density"){  //<Pulay case
            mixing=rcp(new Pulay_Mixing_Density(start, history, max_history, alpha, pulay_alpha, update_paw) );
        }
        else if(mixing_type=="Potential"){
            mixing=rcp(new Pulay_Mixing_Potential(start, history, max_history, alpha, pulay_alpha, update_paw) );
        }
//        else if(mixing_type=="Orbital"){
//            mixing=rcp(new Pulay_Mixing_Orbital(start, history, max_history, alpha, pulay_alpha) );
//        }
#ifdef USE_PAW
        else if(mixing_type=="PAW_Density"){
            mixing=rcp(new Pulay_Mixing_PAW_Density(start, history, max_history, alpha, pulay_alpha) );
        }
#endif
        else if(mixing_type=="Polarizability"){
            mixing=rcp(new Pulay_Mixing_Polarizability(start, history, max_history, alpha, pulay_alpha) );
        }
        else{
            Verbose::all() << "Create_Mixing::unknown mixing_type" <<std::endl;
            exit(-1);
        }
    }
    else if (mixing_method==2){  //< Broyden case
        double alpha=parameters->sublist("Mixing").get<double>("MixingParameter");
        double broyden_alpha = parameters->sublist("Mixing").get<double>("PulayMixingParameter", alpha);
        int start = parameters ->sublist("Mixing").get<int>("MixingStart", 6);
        int history = parameters ->sublist("Mixing").get<int>("MixingHistory", 4);

        if(start<=history){
            Verbose::all() << "MixingStart has to be greater than Mixing_History!"<<std::endl;
            exit(EXIT_FAILURE);
        }
        if(mixing_type=="Density"){
            mixing=rcp(new Broyden_Mixing_Density(start, history, alpha, broyden_alpha) );
        }
        else if(mixing_type=="Potential"){
            mixing=rcp(new Broyden_Mixing_Potential(start, history, alpha, broyden_alpha) );
        }
//        else if(mixing_type=="Orbital"){
//            mixing=rcp(new Broyden_Mixing_Orbital(start, history, alpha, broyden_alpha) );
//        }
#ifdef USE_PAW
        else if(mixing_type=="PAW_Density"){
            mixing=rcp(new Broyden_Mixing_PAW_Density(start, history, alpha, broyden_alpha) );
        }
#endif
        else{
            Verbose::all() << "Create_Mixing::unknown mixing_type" <<std::endl;
            exit(-1);
        }
    }
    else{
        Verbose::all() << "Create_Mixing::unknown mixing_method" <<std::endl;
        exit(-1);
    }
    switch (mixing_method){
        case 0:
            Verbose::single(Verbose::Normal) << "Linear mixing with ";
            break;
        case 1:
            Verbose::single(Verbose::Normal) << "DIIS mixing with ";
            break;
        case 2:
            Verbose::single(Verbose::Normal) << "Broyden mixing with ";
            break;
    }
    Verbose::single(Verbose::Normal) << mixing_type << " is used" <<std::endl;
    return mixing;
}
