#include "External_Field_Cube.hpp"
#include <fstream>
#include "Teuchos_ParameterList.hpp"

#include "../../Basis/Create_Basis.hpp"
#include "../../Util/Parallel_Manager.hpp"
#include "../../Util/Parallel_Util.hpp"
#include "../../Util/Verbose.hpp"
#include "../../Util/Tricubic_Interpolation.hpp"
#include "../../Util/Trilinear_Interpolation.hpp"
#include "../../Util/Time_Measure.hpp"
#include "../../Util/Read_Cube.hpp"

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::ParameterList;
using Teuchos::NO_TRANS;
using std::vector;
using std::array;
using std::string;

External_Field_Cube::External_Field_Cube(
        Teuchos::RCP<const Basis> mesh,
        std::string cube_filename,
        int interpolation,
        double scaling_factor
): External_Field(std::string("Cube"),mesh){
    this -> cube_filename = cube_filename;
    this -> interpolation_scheme = interpolation;
    this-> scaling_factor = scaling_factor;

    calculate_potential();
    have_potential = true;
}

void External_Field_Cube::calculate_potential(){

    std::array<int,3> cube_points;
    std::array<std::array<double,3>,3> cube_lattice_vec;

    Verbose::single(Verbose::Detail)<< "External_Field_Cube:: checking validity of filename (" << cube_filename << ") " <<std::endl;
    //Teuchos::RCP<Epetra_Vector> potential ;
    Teuchos::RCP<const Atoms> atoms = Teuchos::rcp(new Atoms(Read::Cube::read_cube_atoms(cube_filename) ));
    Read::Cube::read_cube_mesh(cube_filename, cube_points, cube_lattice_vec); //unit is bohr

//    for debug
//    std::cout << cube_lattice_vec[0][0] << "\t" << cube_lattice_vec[0][1] <<"\t" << cube_lattice_vec[0][2] <<std::endl;
//    std::cout << cube_lattice_vec[1][0] << "\t" << cube_lattice_vec[1][1] <<"\t" << cube_lattice_vec[1][2] <<std::endl;
//    std::cout << cube_lattice_vec[2][0] << "\t" << cube_lattice_vec[2][1] <<"\t" << cube_lattice_vec[2][2] <<std::endl;

    // check cube contains orthogonal (diagonal) lattice vectors
    for(int i = 0; i < 3; ++i){
        int j = (i+1)%3; int k = (i+2)%3;
        if (fabs(cube_lattice_vec[i][j]) > 1e-8 or
                fabs(cube_lattice_vec[i][k]) > 1e-8){
            Verbose::all() << "lattice in cube file should be orthogonal" <<std::endl;
            Verbose::all() << "cube filename:" << cube_filename <<std::endl;
            exit(-1);
        }
    }

    RCP<Time_Measure> timer = rcp(new Time_Measure());

    RCP<ParameterList> cube_mesh_param = rcp(new ParameterList("cube_mesh"));
//    cube_mesh_param -> sublist("BasicInformation").set<string>("Grid", "Basic");
//    cube_mesh_param -> sublist("BasicInformation").set<string>("Basis", "Sinc");
//    cube_mesh_param -> sublist("BasicInformation").set<int>("AllowOddPoints", 1);
//    cube_mesh_param -> sublist("BasicInformation").set<int>("PointX", cube_points[0]);
//    cube_mesh_param -> sublist("BasicInformation").set<int>("PointY", cube_points[1]);
//    cube_mesh_param -> sublist("BasicInformation").set<int>("PointZ", cube_points[2]);
//    cube_mesh_param -> sublist("BasicInformation").set<double>("ScalingX", cube_lattice_vec[0][0]);
//    cube_mesh_param -> sublist("BasicInformation").set<double>("ScalingY", cube_lattice_vec[1][1]);
//    cube_mesh_param -> sublist("BasicInformation").set<double>("ScalingZ", cube_lattice_vec[2][2]);

    cube_mesh_param ->set<string>("Grid", "Basic");
    cube_mesh_param ->set<string>("Basis", "Sinc");
    cube_mesh_param ->set<int>("AllowOddPoints", 1);
    cube_mesh_param ->set<int>("PointX", cube_points[0]);
    cube_mesh_param ->set<int>("PointY", cube_points[1]);
    cube_mesh_param ->set<int>("PointZ", cube_points[2]);
    cube_mesh_param ->set<double>("ScalingX", cube_lattice_vec[0][0]);
    cube_mesh_param ->set<double>("ScalingY", cube_lattice_vec[1][1]);
    cube_mesh_param ->set<double>("ScalingZ", cube_lattice_vec[2][2]);

    RCP<Basis> cube_mesh = Create_Basis::Create_Basis(*this -> mesh -> get_map() -> Comm().Clone(), cube_mesh_param);
    RCP<Epetra_Vector> original_values = rcp(new Epetra_Vector(*cube_mesh->get_map()));

    //std::cout << *cube_mesh <<std::endl;
    //exit(-1);

    cube_mesh->read_cube(cube_filename, original_values, true);
    int NumMyElements = mesh -> get_map() -> NumMyElements();
    int* MyGlobalElements = mesh -> get_map() -> MyGlobalElements();
    vector<double> val(NumMyElements);
    int j_x,j_y,j_z;

    this -> potential = rcp( new Epetra_Vector( *mesh->get_map()));
    timer -> start("Interpolation");
    if(this -> interpolation_scheme == 0){
       Interpolation::Trilinear::interpolate(cube_mesh, original_values, mesh, potential );
    } else if(this -> interpolation_scheme == 1){
        Interpolation::Tricubic::interpolate(cube_mesh, original_values, mesh, potential );
    }
    // scaling
    potential->Scale(scaling_factor);
    timer -> end("Interpolation");
    timer -> print(Verbose::single(Verbose::Simple), -1, "Interpolation");

    return;
}
