#pragma once
#include <string>
#include "../../Basis/Basis.hpp"
#include "../../State/State.hpp"

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Epetra_Vector.h"

class External_Field{
  friend std::ostream& operator <<(std::ostream &c, const External_Field& external_field);
  friend std::ostream& operator <<(std::ostream &c, const External_Field* pexternal_field);
  public:
//    External_Field(RCP<const Basis> mesh,RCP<ParameterList> parameters);
    External_Field(
        std::string type,
        Teuchos::RCP<const Basis> mesh
    );

    /**
     * @brief potential generation function 
     * @param [out] diagonal, which contains potential values on every grid points. Teuchos::Array is related to spin index
     * @return error code [not activated yet]
     */
    // Array<spin_index> diagonal
    int get_potential(Teuchos::Array< Teuchos::RCP<Epetra_Vector> >& diagonal);
    std::vector<double> get_force(Teuchos::RCP<const State> states );
    /// 

    /**
     * @brief calculate interaction energy cuased by external field
     * @param[in] state  mesh Basis.
     * @param[in] alpha spin index.
     * @return    energy due to field 
     */
    double get_energy(Teuchos::RCP<const State> states,int alpha);
    bool is_field_applied();
    std::string type;
  protected:
    Teuchos::RCP<const Basis> mesh;
    //Teuchos::RCP<Teuchos::ParameterList> parameters;

    virtual void calculate_potential() = 0;
    Teuchos::RCP<Epetra_Vector> potential;
    bool have_potential = false;
};

