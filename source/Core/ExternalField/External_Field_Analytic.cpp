#include "External_Field_Analytic.hpp"
#include "../../Util/Parallel_Manager.hpp"
#include "../../Util/Parallel_Util.hpp"
#include "../../Util/Verbose.hpp"

using Teuchos::rcp;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::ParameterList;
using std::string;

External_Field_Analytic::External_Field_Analytic(RCP<const Basis> mesh,string field, string type, string field_direction,double field_strength): External_Field(std::string("analytic"),mesh){
    this->field=field;
    this->type=type;
    this->field_direction=field_direction;
    this->field_strength=field_strength; 

    if(field == "Electric"){
        if(type != "Static"){
            Verbose::all() << "Wrong external field type - only static field is supported" << std::endl;
            exit(EXIT_FAILURE);
        }

        if(field_direction != "x" && field_direction != "y" && field_direction != "z"){
            Verbose::all() << "Wrong external field direction - choose x, y, or z" << std::endl;
            exit(EXIT_FAILURE);
        }
        calculate_potential();
        have_potential = true;
    }
    else if (field ==""){
        have_potential = false;
    }
    else{
        Verbose::all() << "Wrong external field - only electric field is supported" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void External_Field_Analytic::calculate_potential(){
    const double** scaled_grid = mesh->get_scaled_grid();
    double* electric_field = new double[3]();
    //const int* points = mesh->get_points();
    int size = mesh->get_map()->NumMyElements();
    int* MyGlobalElements = mesh->get_map()->MyGlobalElements();
    potential = rcp( new Epetra_Vector( *mesh->get_map() )) ;

    if(type == "Static"){
        // set electric field vector
        if(field_direction == "x"){
            electric_field[0] = field_strength;
        }else if(field_direction == "y"){
            electric_field[1] = field_strength;
        }else if(field_direction == "z"){
            electric_field[2] = field_strength;
        }
        // end

        int i_x=0,i_y=0,i_z=0;
        for(int i=0;i<size;i++){
            mesh->decompose(MyGlobalElements[i],&i_x,&i_y,&i_z);
            double* position = new double[3]; 
            position[0] = scaled_grid[0][i_x];
            position[1] = scaled_grid[1][i_y];
            position[2] = scaled_grid[2][i_z];
            double tmp = 0.0;
            for(int j=0;j<3;j++){
                tmp += electric_field[j] * position[j]; 
            }
            potential->ReplaceGlobalValue(MyGlobalElements[i],0, tmp);
            delete[] position;
        }
    }

    delete[] electric_field;
    return;
}

/*
double External_Field::numeric_integration(int j,int index,double* electric_field,double min,double max){
    double return_val=0.0;
    const int* points= basis->get_points();
    double scaling = (max-min)/(num_integrate_point-1);
    double point=min;
    for (int i=0;i <num_integrate_point ;i++){
        return_val+=  basis->compute_basis(j,point,index)* basis->compute_basis(j,point,index)*electric_field[index]*scaling ;
        point += scaling;
    }

    return return_val;
}
*/
