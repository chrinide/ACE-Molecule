#pragma once
#include <string>
#include "../../Basis/Basis.hpp"
#include "External_Field.hpp"

#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"

class External_Field_Analytic: public External_Field{
  public:
    External_Field_Analytic(
        Teuchos::RCP<const Basis> mesh,
        std::string field, std::string type, std::string field_direction,
        double field_strength
    );
  protected:
    //Teuchos::RCP<const Basis> mesh;
    Teuchos::RCP<Teuchos::ParameterList> parameters;

    std::string field,type,field_direction;
    double field_strength;
    void calculate_potential();
};
