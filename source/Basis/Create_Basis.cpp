#include <iostream>
#include <stdexcept>
#include "../Util/Verbose.hpp"
#include "Create_Basis.hpp"

using std::string;
using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::null;
using Teuchos::ParameterList;
using std::endl;

RCP<Basis> Create_Basis::Create_Basis(const Epetra_Comm& comm, RCP<ParameterList> parameters, RCP<Atoms> atoms){
    int* points = new int[3];
    double* scaling = new double[3];
    const int MyPID=comm.MyPID();

    scaling[0]=parameters->get<double>("ScalingX");
    scaling[1]=parameters->get<double>("ScalingY");
    scaling[2]=parameters->get<double>("ScalingZ");

    bool is_even_only = true;
    if( parameters -> isParameter("AllowOddPoints") ){
        if( parameters -> get<int>("AllowOddPoints") != 0 ){
            is_even_only = false;
        }
    }

    if(parameters->get<string>("Grid") == "Atoms"){
        Verbose::single() <<"Warning:: Grid_Atoms ignores input Points and CellDimension" <<std::endl;
        vector<double> double_radious;

        bool is_relative_radius = true;
        if(parameters->get<int>("AbsoluteRadius",1)!=0){
            is_relative_radius = false;
            Verbose::single(Verbose::Normal) << "AbsoluteRadius is set" << endl;
        }
        else{
            Verbose::single(Verbose::Normal) << "RelativeRadius is set" << endl;
        }
        Teuchos::Array<string>  str_radius = parameters->get<Teuchos::Array<string> >("Radius");
        Verbose::single(Verbose::Normal) << " Grid radius for atoms in angstrom: \n";
        Verbose::single(Verbose::Detail) << "size of radius" << str_radius.size() << endl;

        if(str_radius.size()!=atoms->get_num_types() and str_radius.size()!=1){
            throw std::invalid_argument("You have to provide BasicInformation.Radius variable for each atom types or should provide only 1!");
        }

        if(str_radius.size()==1){
            for(int i =0; i<atoms->get_num_types(); i++){
                double_radious.push_back( atof(str_radius[0].c_str()) );
            }
        }

        for(int i =0; i<str_radius.size(); i++){
             Verbose::single(Verbose::Normal) << str_radius[i] <<", ";
             double_radious.push_back( atof(str_radius[i].c_str()) );
        }
        Verbose::single(Verbose::Detail) << std::endl;

        //atoms->move_center_of_mass(0.0,0.0,0.0);
        Verbose::set_numformat(Verbose::Pos);
        Verbose::single(Verbose::Detail)<< *atoms <<std::endl;
        Verbose::set_numformat();
        int index1,index2,index3,index4,index5,index6;
        double min_x,max_x,min_y,max_y,min_z,max_z;
        double* cell = new double[3];
        //atoms->get_vertex_info(index1,min_x,index2,max_x,index3,min_y,index4,max_y,index5,min_z,index6,max_z, double_radious, is_relative_radius);
        atoms->get_vertex_info(index1,min_x,index2,max_x,index3,min_y,index4,max_y,index5,min_z,index6,max_z, double_radious, is_relative_radius, cell);
        Verbose::single(Verbose::Detail) << endl << min_x <<"\t" << max_x << "\t" << min_y << "\t" << max_y << "\t" << min_z << max_z <<std::endl;
        auto positions = atoms->get_positions();

        points[0] = floor(2.0*cell[0]/scaling[0]) + 1;
        points[1] = floor(2.0*cell[1]/scaling[1]) + 1;
        points[2] = floor(2.0*cell[2]/scaling[2]) + 1;

        if( is_even_only ){
            for(int i=0; i<3; i++){
                if(points[0]%2==1)
                    points[0] = points[0]-1;
                if(points[1]%2==1)
                    points[1] = points[1]-1;
                if(points[2]%2==1)
                    points[2] = points[2]-1;
            }
        } else {
            Verbose::single(Verbose::Normal) << "Allowing odd points for Grid_Atoms" << std::endl;
        }
        parameters->set("CellDimensionX",cell[0]);
        parameters->set("CellDimensionY",cell[1]);
        parameters->set("CellDimensionZ",cell[2]);

        parameters->set("PointX",points[0]);
        parameters->set("PointY",points[1]);
        parameters->set("PointZ",points[2]);
        delete[] cell;
        Verbose::single(Verbose::Detail) << "Grid_Atoms parameters" << std::endl;
        Verbose::single(Verbose::Detail) << *parameters <<std::endl;
    }

    points[0] = parameters->get<int>("PointX");
    points[1] = parameters->get<int>("PointY");
    points[2] = parameters->get<int>("PointZ");

    bool is_cubic = false;
    if(parameters->isParameter("UseCubicBaseOnly")){
        is_cubic = (parameters->get<int>("UseCubicBaseOnly")!=0);
    }
    if (is_cubic){
        int max = std::max(std::max(points[0],points[1]),points[2]);
        points[0] = max;
        points[1] = max;
        points[2] = max;
    }
    for(int i = 0; i < 3; ++i){
        if(points[i] < 1){
            points[i] = 1;
        }
    }
    RCP<Basis_Function> basis;
    RCP<Basis_Function> kinetic_basis;
    //Verbose::single() <<  points[0] << "\t" << points[1] << "\t"<< points[2] << std::endl;
    //Verbose::single() <<  scaling[0] << "\t" << scaling[1] << "\t"<< scaling[2] << std::endl;
    if(parameters->get<string>("Basis") == "Sinc"){
        basis = rcp( new Sinc(points, scaling) );
        kinetic_basis = rcp( new Sinc(points, scaling) );
        if(parameters->isParameter("KineticMatrix")){
            if(parameters->get<string>("KineticMatrix") == "Finite_Difference"){
                Verbose::single(Verbose::Normal) << "Compute kinetic energy matrix with FD method.\n";
                kinetic_basis = rcp( new Finite_Difference(points, scaling, parameters->get<int>("DerivativesOrder")) );
            }
        }
    }
    else if(parameters->get<string>("Basis") == "Finite_Difference"){
        parameters -> get<int>("DerivativesOrder", 9);
        Verbose::single(Verbose::Normal) << " Finite difference derivative approximation: " << parameters->get<int>("DerivativesOrder") << "-point stencil\n";
        basis = rcp( new Finite_Difference(points, scaling, parameters->get<int>("DerivativesOrder")) );
        kinetic_basis = rcp( new Finite_Difference(points, scaling, parameters->get<int>("DerivativesOrder")) );
    }
    else{
        if(MyPID == 0) Verbose::all() << "main - Wrong basis function." << std::endl;
        exit(EXIT_FAILURE);
    }
    if( is_even_only ){
        if(points[0]%2==1 or points[1]%2==1 or points[2]%2==1){
            Verbose::all() << "Points should be even number" << endl;
            Verbose::all() << *parameters << endl;
            exit(-1);
        }
    }
    for(int k=0; k<3; k++){
        Verbose::set_numformat(Verbose::Pos);
        if(k==0)
            Verbose::single()<<"Grid points on x-axis \n" ;
        else if(k==1)
            Verbose::single()<<"Grid points on y-axis \n" ;
        else
            Verbose::single()<<"Grid points on z-axis \n" ;
        if (points[k]<10){
            for(int i=0;i<points[k];i++){
                Verbose::single()<< basis->get_scaled_grid()[k][i] <<"\t";
            }
        }
        else{
            for(int i=0;i<5;i++){
                Verbose::single()<< basis->get_scaled_grid()[k][i] << "\t";
            }
            Verbose::single()<<"...\t" ;

            for(int i=points[k]-5;i<points[k];i++){
                Verbose::single()<< basis->get_scaled_grid()[k][i] << "\t";
            }
        }
        Verbose::single()<<"\n";
    }
    Verbose::single()<<"\n";

    RCP<Grid_Setting> grid_setting;
    if(parameters->get<string>("Grid") == "Sphere"){
        grid_setting = rcp( new Grid_Sphere(points,basis) );
    }
    else if(parameters->get<string>("Grid") == "Basic"){
        grid_setting = rcp( new Grid_Basic(points) );
    }
    else if(parameters->get<string>("Grid") == "Atoms"){
        if (atoms.is_null()){
            Verbose::single()<< "Create_Basis::error there is no atoms \n" ;
            exit(-1);
        }
        bool is_relative_radius = true;
        if(parameters->get<int>("AbsoluteRadius",1)!=0){
            is_relative_radius = false;
            Verbose::single(Verbose::Normal) << "AbsoluteRadius is set" << endl;
        }
        else{
            Verbose::single(Verbose::Normal) << "RelativeRadius is set" << endl;
        }
        Teuchos::Array<string> str_radius = parameters->get<Teuchos::Array<string> >("Radius");

        vector<double> double_radious;

        Verbose::single(Verbose::Normal) << " Grid radius for atoms (Angstroms): \n";
        Verbose::set_numformat(Verbose::Pos);
        for(int i =0; i<str_radius.size(); i++){
             Verbose::single(Verbose::Normal) << str_radius[i] <<", ";
             double_radious.push_back( atof(str_radius[i].c_str()) );
        }
        Verbose::single(Verbose::Normal) <<std::endl;
        for(int k=0; k<double_radious.size(); k++){
            double_radious[k] *= 1.889725989;  //Angstrom to Bohr
        }
        if (double_radious.size()==1){
            Verbose::single(Verbose::Detail)<< double_radious[0] <<std::endl;
            grid_setting = rcp( new Grid_Atoms(points, basis, atoms.get(), double_radious[0], is_relative_radius) );

        }
        else if(double_radious.size()!=atoms->get_num_types()){
            throw std::invalid_argument("You have to provide BasicInformation.Radius variable for each atom types or should provide only 1!");
        }
        else{
            grid_setting = rcp( new Grid_Atoms(points, basis, atoms.get(), double_radious) );
        }

    }
    else{
        Verbose::single() << "main - Wrong grid type. \n" ;
        exit(EXIT_FAILURE);
    }
    RCP<Epetra_Map>  map = rcp(new Epetra_Map(grid_setting->get_size(),0,comm) );
    delete [] points;  //shchoi 16.09.05
    delete [] scaling; //shchoi 16.09.05
    //Verbose::all() << comm <<std::endl;
    Verbose::set_numformat();
    return rcp( new Basis(basis, kinetic_basis, grid_setting,map) );
};
