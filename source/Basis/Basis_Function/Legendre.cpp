#include "../Basis_Function/Legendre.hpp"

#include <cmath>

Legendre::Legendre(int *_point, double *_scaling){
    this->points = new int [3];
    this->points[0] = _point[0];
    this->points[1] = _point[1];
    this->points[2] = _point[2];

    size = points[0] * points[1] * points[2];
    //compute_grid_and_weight_factors();

    this->scaling = new double [3];
    this->scaling[0] = _scaling[0];
    this->scaling[1] = _scaling[1];
    this->scaling[2] = _scaling[2];

    //compute_scaled_grid();
    this -> is_computed_weight = false;
}

Legendre::~Legendre(){
    delete[] this -> points;
    delete[] this -> scaling;
    if( is_computed_weight ){
        for(int i = 0; i < 3; ++i){
            delete[] this -> grid[i];
            delete[] this -> weight_factor[i];
        }
        delete[] this -> grid;
        delete[] this -> weight_factor;
    }
}

void Legendre::compute_grid_and_weight_factors(){
    double** grid = new double* [3];
    double** weight_factor = new double* [3];

    for(int i=0;i<3;i++){
        grid[i] = new double [points[i]];
        weight_factor[i] = new double [points[i]];
        gauleg(-scaling[i], scaling[i], grid[i], weight_factor[i], points[i]);
    }

    this->grid = grid;
    this->weight_factor = weight_factor;
    this -> is_computed_weight = true;

    return;
}

// Given the lower and upper limits of integration x1 and x2, and given n, this routine returns
// arrays x[1..n] and w[1..n] of length n, containing the abscissas and weights of the Gauss-
// Legendre n-point quadrature formula.
void Legendre::gauleg(double x1, double x2, double* x, double* w, int n){
    double EPS = 3.0E-11; // Relative precision
    int m, j, i;
    double z1, z, xm, xl, pp, p3, p2, p1; // High precision is a good idea for this routine.
    m = (n+1)/2;      // The roots are symmetric in the interval, so
    xm = 0.5*(x2+x1); // we only have to find half of them.
    xl = 0.5*(x2-x1);
    for(i=1;i<=m;i++){ // Loop over the desired roots.
        z = cos(M_PI*(i-0.25)/(n+0.5));
        // Starting with the above approximation to the ith root, we enter the main loop of
        // refinement by Newton’s method.
        do{
            p1 = 1.0;
            p2 = 0.0;
            for(j=1;j<=n;j++){ // Loop up the recurrence relation to get the
                p3 = p2;       // Legendre polynomial evaluated at z.
                p2 = p1;
                p1 = ((2.0*j-1.0)*z*p2-(j-1.0)*p3)/j;
            }
            // p1 is now the desired Legendre polynomial. We next compute pp, its derivative,
            // by a standard relation involving also p2, the polynomial of one lower order.
            pp = n*(z*p1-p2)/(z*z-1.0);
            z1 = z;
            z = z1-p1/pp; // Newton’s method.
        } while (fabs(z-z1) > EPS);
        x[i-1] = xm-xl*z;                  // Scale the root to the desired interval,
        x[n-i] = xm+xl*z;              // and put in its symmetric counterpart.
        w[i-1] = 2.0*xl/((1.0-z*z)*pp*pp); // Compute the weight
        w[n-i] = w[i-1];                 // and its symmetric counterpart.
    }
    return;
}

int Legendre::set_scaling(double* scaling){
  this->scaling = scaling;
  //compute_scaled_grid();
  return 0;
}

const int* Legendre::get_points(){
    return points;
}

double** Legendre::get_grid(){
    return grid;
}

const double* Legendre::get_scaling(){
    return scaling;
}

double Legendre::get_weight_function(double point, int axis){
    return 1.0;
}

//////////////
double** Legendre::get_scaled_grid(){
    return scaled_grid;
} 
double Legendre::get_weight_factor(int j, int axis){
    return weight_factor[axis][j];
}
/*
void Legendre::get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points){

}
*/
