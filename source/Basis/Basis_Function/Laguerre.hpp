#pragma once

/**
 * @brief This class and its children should be hidden behined Basis class, except for special propose.
 **/
class Laguerre{
    public:
        Laguerre(int *_point, double *_scaling);
        ~Laguerre();

        const double* get_scaling();
        double** get_grid();
        const int* get_points();
        double get_weight_factor(int j, int axis);
        double get_weight_function(double point, int axis);

        double** get_scaled_grid();
        int set_scaling(double* scaling);
        int* get_neighboring_points(int axis, int axis_points, int* neighboring_points);    

    private:
        int* points;
        double* scaling;
        double** grid;
        double** scaled_grid;
        int size;

        double **weight_factor;

        void compute_grid_and_weight_factors();
        void compute_scaled_grid();

        void gaulag(double *x, double *w, int n);
        double gammln(double xx);

};

