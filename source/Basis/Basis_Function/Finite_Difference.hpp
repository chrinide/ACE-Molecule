#pragma once 
#include <string>
#include <vector>

#include "../Basis_Function/Basis_Function.hpp"

/**
 * @brief This class and its children should be hidden behined Basis class, except for special propose.
 **/
class Finite_Difference : public Basis_Function {
    public:
        Finite_Difference(int* points, double* scaling,int der_order=9);
        ~Finite_Difference();
        /// This function set scaling factor. This function call compute_scaled_grid. Thus you don't need to call explicity.
        int set_scaling(double* scaling);



        /// This returns whether it is uniform grid basis or not
        bool is_uniform();
        /// This computes the element of kinetic matrix
        double compute_kinetic(int i, int j, int axis);
        /// This returns the value of basis function at given point
        double compute_basis(int j, double point, int axis);
        /// This function computes the second derivative value of j th Lagrange function at given point
        double compute_second_der(int j, double point, int axis);
        /// This returns first derivative of j th Lagrange function at given point
        double compute_first_der(int j, double point, int axis);
        double get_weight_factor(int j,int axis);    
        double get_weight_function(double point,int axis);    
        void get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points);    
        int get_order(){return der_order;};
    protected:
        void compute_coef();
        void compute_grid();
        int der_order;
        std::vector<double> second_center_coef;// = { {1,-2,1},{-1.0/12.0,4.0/3.0,-5.0/2.0,4.0/3.0,-1.0/12.0}  };
        std::vector<double> second_forward_coef;
        std::vector<double> first_center_coef;
        std::vector<double> first_forward_coef;
};
