#pragma once 
#include <vector>
#include "Epetra_Vector.h"
#include "Epetra_CrsMatrix.h"

#include "../Basis_Function/Basis_Function.hpp"

/**
 * @brief This class and its children should be hidden behined Basis class, except for special propose.
 **/
class Sinc: public Basis_Function {
    private:
        ///This computes sinc(x) = sin(PI*x)/(PI*x)
        double sinc(double x);
        ///This computes the grid, which is uniform grid between [-N/2,+N/2] 
        void compute_grid();
    public:
        ///This is constructor 
        Sinc (int * _point, double * _scaling);
        ~Sinc();

        bool is_uniform();

        double compute_kinetic(int i, int j, int axis);
        double compute_basis(int j, double point, int axis);
        double compute_second_der(int j, double point, int axis);
        double compute_first_der(int j,double point, int axis);
        double get_weight_factor(int j,int axis);    
        double get_weight_function(double point,int axis);    
           int set_scaling(double* scaling);
        void get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points);    
};
