#pragma once

#include "Teuchos_ParameterList.hpp"

#include "Basis.hpp"
#include "Basis_Function/Basis_Function.hpp"
#include "Basis_Function/Finite_Difference.hpp"
#include "Basis_Function/Sinc.hpp"
#include "Grid_Setting/Grid_Setting.hpp"
#include "Grid_Setting/Grid_Basic.hpp"
#include "Grid_Setting/Grid_Sphere.hpp"
#include "Grid_Setting/Grid_Atoms.hpp"



namespace Create_Basis{
    /**
     * @brief Create Basis class. See input manual for details.
     * @brief Comm Epetr communicator.
     * @brief parameters Parameter, should be subsection BasicInformation.
     * @brief atoms Atoms object required for Grid_Atoms.
     **/
    Teuchos::RCP<Basis> Create_Basis(const Epetra_Comm& Comm, Teuchos::RCP<Teuchos::ParameterList> parameters, Teuchos::RCP<Atoms> atoms = Teuchos::null);

}
