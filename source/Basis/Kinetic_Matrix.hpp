#pragma once
#include "Epetra_CrsMatrix.h"
#include "Teuchos_RCP.hpp"
#include "Teuchos_TimeMonitor.hpp"
#include "Epetra_MultiVector.h"
#include "AnasaziOperator.hpp"
#include "AnasaziTypes.hpp"
#include "Basis.hpp"

class Basis;
/**
 * @brief Kinetic matrix according to the given Basis.
 **/
class Kinetic_Matrix{
    public:
        ~Kinetic_Matrix();
        Kinetic_Matrix( Teuchos::RCP< const Basis> mesh); 
        /**
         * @brief output = Kinetic_Matrix * input. Use own implementations.
         **/
        int multiply(Teuchos::RCP<Epetra_MultiVector> input, Teuchos::RCP<Epetra_MultiVector> &output) const;
        /**
         * @brief Does nothing.
         **/
        int multiply(const Anasazi::MultiVec<double>& X, 
        Anasazi::MultiVec<double>& Y ) const;
        /**
         * @brief output = Kinetic_Matrix * input. Use Epetra_CrsMatrix multiplication routine. Generate Epetra_CrsMatrix when called.
         **/
        int multiply2(Teuchos::RCP<Epetra_MultiVector> input, Teuchos::RCP<Epetra_MultiVector> &output);
        /**
         * @brief output = Kinetic_Matrix * input. Use Epetra_CrsMatrix multiplication routine. Stores the Epetra_CrsMatrix when first called.
         **/
        int multiply3(Teuchos::RCP<Epetra_MultiVector> input, Teuchos::RCP<Epetra_MultiVector> &output);

        /**
         * output = scalar * this + matrix. Interface to other methods.
         **/
        int add(Teuchos::RCP<Epetra_CrsMatrix> matrix, Teuchos::RCP<Epetra_CrsMatrix> &output, double scalar);
        /**
         * matrix += scalar * this.
         **/
        int add(Teuchos::RCP<Epetra_CrsMatrix> &matrix, double scalar);
    protected:
        /**
         * matrix = scalar * this + matrix. Speed up using SumIntoGlobalVlues for already filled matrix. If desired index not exists, throw an error.
         **/
        int add_filled(Teuchos::RCP<Epetra_CrsMatrix> &matrix, double scalar);

        void make_kinetic_vector();
        void make_row(int row_index, std::vector<double>& value, std::vector<int>& index) const;
        std::vector< std::vector<double> > kinetic_vector;
        double** neighboring_list;
        bool hold_kinetic_crsmatrix=false;
        Teuchos::RCP<Epetra_CrsMatrix> kinetic_crsmatrix;
        Teuchos::RCP<const Basis> mesh;
};
