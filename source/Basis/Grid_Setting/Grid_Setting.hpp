#pragma once 
//#include <ostream>
#include <iostream>
#include <string>
#include <vector>
#ifdef USE_CUDA
#include "cuda_runtime_api.h"
#include "device_launch_parameters.h"
#endif 

/**
 * @brief This class should be hidden behind Basis class and its method should not be called explicitly.
 **/
class Grid_Setting {
    friend std::ostream &operator <<(std::ostream &c, const Grid_Setting &grid_setting);
    friend std::ostream &operator <<(std::ostream &c, const Grid_Setting* pgrid_setting);

    public:
        /**
         * @brief Virtual destructor for abstract class.
         **/
        virtual ~Grid_Setting(){};
        /**
         * @brief See Basis::decompose.
         * @details Implementation of Basis::decompose. 
         * @todo Probably change to protected / friend of Basis class?
         **/
        virtual void decompose(int index, const int* points, int* i_x, int* i_y, int* i_z)=0;
        //virtual int* decompose(int index, int* points)=0;
        /**
         * @brief See Basis::combine.
         * @details Implementation of Basis::combine. 
         * @todo Probably change to protected / friend of Basis class?
         **/
        virtual int  combine(int i_x,int i_y,int i_z,const int* points)=0;
        bool operator== (Grid_Setting* grid_setting) const;

        /**
         * @brief Change index from Grid_Basic index to own index.
         **/
        virtual int from_basic(int basic_grid_index)=0;
        /**
         * @brief Change index to Grid_Basic index from own index.
         **/
        virtual int to_basic(int current_grid_index)=0;

        std::string get_type() const;
        int get_size() const;
        const int* get_points() const;
        std::vector<int*> get_boundary();

    protected:
        std::string grid_type;
        int size;
        int* points;
        std::vector<int*> boundary;
};
