#pragma once 
#include "Teuchos_RCP.hpp"
#include "Grid_Setting.hpp"
#include "../Basis_Function/Basis_Function.hpp"
#include <vector>

/**
 * @brief This class should be hidden behind Basis class and its method should not be called explicitly.
 **/
class Grid_Sphere: public Grid_Setting{
    public:
        void decompose(int index, const int* points, int* i_x, int* i_y, int* i_z);
        int  combine(int i_x,int i_y,int i_z, const int* points);
        int from_basic(int basic_grid_index);
        int to_basic(int current_grid_index);

        ///This is constructor that specifies radius of total grid.
        //Grid_Sphere(int* points, Teuchos::RCP<Basis_Function> basis, double radius);  //shchoi 17.03.17
        //Grid_Sphere(const int* points, Teuchos::RCP<Basis_Function> basis, double radius);//shchoi 17.03.17
        ///This is constructor. The radius is set as maximum of given values.
        //Grid_Sphere(int* points, Teuchos::RCP<Basis_Function> basis);//shchoi 17.03.17
        Grid_Sphere(const int* points, Teuchos::RCP<Basis_Function> basis);
        ~Grid_Sphere();


    protected:
        void scan_boundary(Teuchos::RCP<Basis_Function> basis,int v_x,int v_y, int v_z);
        Grid_Setting* grid_basic;
        std::vector<int> match_sphere_to_basic;
        std::vector<int> match_basic_to_sphere;
        //double* get_all_points(double* scaling,double** grid);
};

