#include "Grid_Atoms.hpp"
#include "../../Util/Parallel_Manager.hpp"
#include <cmath>
#include "../../Util/Verbose.hpp"
#include "Epetra_Map.h"
#include "mpi.h"
#include "Epetra_MpiComm.h"

#ifdef ACE_HAVE_OMP
#include "omp.h"
#endif

using std::vector;
using Teuchos::RCP;
using Teuchos::rcp;


Grid_Atoms::Grid_Atoms(const int* points, Teuchos::RCP<Basis_Function> basis,Atoms* atoms, double factor, bool is_relative){
    timer = Teuchos::rcp(new Time_Measure());
    timer->start("Constructor2");
    Verbose::single(Verbose::Normal) << "Grid_Atoms::Grid indices are parallely generated" << std::endl;
    grid_type="Atoms";
    grid_basic = new Grid_Basic(points);


    size = points[0]*points[1]*points[2];
    this->points = new int[3];
    this->points[0] = points[0];
    this->points[1] = points[1];
    this->points[2] = points[2];
    this->atoms=new Atoms(*atoms);
    const double** scaled_grid = basis->get_scaled_grid();
    const double* scaling = basis->get_scaling();
    vector<std::array<double,3> > positions = atoms->get_positions();
    const int atom_size = positions.size();
    //factor*=1.889725989; //angstrom to bohr
    if(is_relative){
        for(int j=0; j < atom_size ;j++){
            int atomic_number = atoms->get_atomic_number_from_type(atoms->get_atom_type(j));
            Verbose::single(Verbose::Normal) << "real radius in bohr("<<j<<","<< atomic_number<<") : " << factor*Periodic_atom::covalent_radii[atomic_number] << std::endl;
        }
    }
    else{
        for(int j=0; j < atom_size ;j++){
            int atomic_number = atoms->get_atomic_number_from_type(atoms->get_atom_type(j));
            Verbose::single(Verbose::Normal) << "real radius in bohr("<<j<<","<< atomic_number<<") : " << factor << std::endl;
        }
    }
    std::vector<int> tmp(points[0]*points[1]*points[2]);
    memset(&tmp[0], 0, sizeof(int)*points[0]*points[1]*points[2]);
    for(int i=0; i<atom_size; i++){
        int atom_ix = (positions[i][0]-scaled_grid[0][0])/scaling[0]+1;
        int atom_iy = (positions[i][1]-scaled_grid[1][0])/scaling[1]+1;
        int atom_iz = (positions[i][2]-scaled_grid[2][0])/scaling[2]+1;
        int itype = atoms->get_atom_type(i);
        int point_x, point_y, point_z;
        int atomic_number = atoms->get_atomic_number_from_type(atoms->get_atom_type(i));
        if(is_relative){
            point_x = factor*Periodic_atom::covalent_radii[atomic_number]/scaling[0]+2;
            point_y = factor*Periodic_atom::covalent_radii[atomic_number]/scaling[1]+2;
            point_z = factor*Periodic_atom::covalent_radii[atomic_number]/scaling[2]+2;
        }
        else{
            point_x = factor/scaling[0]+2;
            point_y = factor/scaling[1]+2;
            point_z = factor/scaling[2]+2;
        }

        for(int x=-point_x; x<point_x; x++){
            int new_x = x+atom_ix;
            for(int y=-point_y; y<point_y; y++){
                int new_y = y+atom_iy;
                for(int z=-point_z; z<point_z; z++){
                    int new_z = z+atom_iz;
                    int new_index = grid_basic->combine(new_x,new_y,new_z, points);

                    if(new_index>0){
                        double r = sqrt((positions[i][0]-scaled_grid[0][new_x])*(positions[i][0]-scaled_grid[0][new_x])
                                       +(positions[i][1]-scaled_grid[1][new_y])*(positions[i][1]-scaled_grid[1][new_y])
                                       +(positions[i][2]-scaled_grid[2][new_z])*(positions[i][2]-scaled_grid[2][new_z]));

                        if(is_relative){

                            if(r<factor*Periodic_atom::covalent_radii[atomic_number]){
                                tmp[new_index]=1;
                            }
                        }
                        else{
                            if(r<factor){
                                tmp[new_index]=1;
                            }
                        }
                    }
                }
            }
        }
    }
    for(int i=0; i<tmp.size(); i++){
        if(tmp[i] == 1){
            match_atoms_to_basic.push_back(i);
            match_basic_to_atoms.push_back(match_atoms_to_basic.size()-1);
  //          std::cout << i << std::endl;
        }
        else{
            match_basic_to_atoms.push_back(-1);
        }
    }
    timer->end("Constructor2");
    Verbose::single(Verbose::Detail) << "Constructor2 of Grid Atoms consumed : \t" << timer->get_elapsed_time("Constructor2",-1) << " s" << std::endl;
    Verbose::single(Verbose::Normal) << "Grid_Atoms::size is reduced to " << match_atoms_to_basic.size() << "/" << size <<std::endl;
    size = match_atoms_to_basic.size();

}

Grid_Atoms::Grid_Atoms(int* points, RCP<Basis_Function> basis,Atoms* atoms, vector<double> double_radious){

    Teuchos::RCP<Time_Measure> timer = Teuchos::rcp(new Time_Measure());
    timer->start("Grid Atom");

    Verbose::single(Verbose::Normal) << "Grid_Atoms::Grid indices are generated...." << std::endl;
    grid_type = "Atoms";
    grid_basic = new Grid_Basic(points);



    const double ** scaled_grid = basis->get_scaled_grid();
    const double* scaling = basis->get_scaling();
    size = points[0] * points[1] * points[2];
    //cout << size << endl;
    this->points = new int [3];
    this->points[0] = points[0];
    this->points[1] = points[1];
    this->points[2] = points[2];
    this->atoms=new Atoms(*atoms);
    /*
    //bohr to angstrom
    for(int k=0; k<double_radious.size(); k++){
    double_radious[k]*=1.889725989;
    }
    */

    vector<std::array<double,3> > positions = atoms->get_positions();
    const int atom_size = positions.size();
    double factor=1.889725989; //angstrom to bohr
    int i_x=0, i_y=0, i_z=0;
    double x, y, z, r;
    bool is_contained = false;
    match_basic_to_atoms.clear();
    match_atoms_to_basic.clear();
    std::vector<int> tmp(points[0]*points[1]*points[2]);
    memset(&tmp[0], 0, sizeof(int)*points[0]*points[1]*points[2]);
    for(int i=0; i<atom_size; i++){
        int atom_ix = (positions[i][0]-scaled_grid[0][0])/scaling[0]+1;
        int atom_iy = (positions[i][1]-scaled_grid[1][0])/scaling[1]+1;
        int atom_iz = (positions[i][2]-scaled_grid[2][0])/scaling[2]+1;
        int itype = atoms->get_atom_type(i);
        int point_x = double_radious[itype]/scaling[0]+2;
        int point_y = double_radious[itype]/scaling[1]+2;
        int point_z = double_radious[itype]/scaling[2]+2;
        for(int x=-point_x; x<point_x; x++){
            int new_x = x+atom_ix;
            for(int y=-point_y; y<point_y; y++){
                int new_y = y+atom_iy;
                for(int z=-point_z; z<point_z; z++){
                    int new_z = z+atom_iz;
                    int new_index = grid_basic->combine(new_x,new_y,new_z, points);

                    if(new_index>0){
                        double r = sqrt((positions[i][0]-scaled_grid[0][new_x])*(positions[i][0]-scaled_grid[0][new_x])
                                        +(positions[i][1]-scaled_grid[1][new_y])*(positions[i][1]-scaled_grid[1][new_y])
                                        +(positions[i][2]-scaled_grid[2][new_z])*(positions[i][2]-scaled_grid[2][new_z]));
                        if(r<double_radious[itype]){
                            tmp[new_index]=1;
                        }
                    }
                }
            }
        }
    }
    for(int i=0; i<tmp.size(); i++){
        if(tmp[i] == 1){
            match_atoms_to_basic.push_back(i);
            match_basic_to_atoms.push_back(match_atoms_to_basic.size()-1);
  //          std::cout << i << std::endl;
        }
        else{
            match_basic_to_atoms.push_back(-1);
        }
    }
    for(int j=0; j < atom_size ;j++){
        int atomic_number = atoms->get_atomic_number_from_type(atoms->get_atom_type(j));
        Verbose::single(Verbose::Normal) << "real radius("<< atomic_number<<") : " << double_radious[atoms->get_atom_type(j)]<<std::endl;
    }

    timer->end("Grid Atom");
    Verbose::single(Verbose::Detail) << "Grid_Atoms time: " << timer->get_elapsed_time("Grid Atom", -1) << std::endl;
    Verbose::single(Verbose::Normal) << "Grid_Atoms::size is reduced to " << match_atoms_to_basic.size() << "/" << size <<std::endl;
    size = match_atoms_to_basic.size();
//    exit(-1);
}
void Grid_Atoms::decompose(int index, const int* points, int* i_x, int* i_y, int* i_z){
    int index_in_basic_grid_setting = match_atoms_to_basic[index];
    grid_basic->decompose(index_in_basic_grid_setting,points,i_x,i_y,i_z);
    return;
}

int Grid_Atoms::combine(int i_x,int i_y, int i_z,const int * points){
    int index_in_basic_grid_setting =  grid_basic->combine(i_x,i_y,i_z, points);
    if(index_in_basic_grid_setting<0){
        return index_in_basic_grid_setting;  // return error number!!
    }
    return match_basic_to_atoms[index_in_basic_grid_setting];
}

int Grid_Atoms::from_basic(int basic_grid_index){
    return match_basic_to_atoms[basic_grid_index];
}
int Grid_Atoms::to_basic(int current_grid_index){
    return match_atoms_to_basic[current_grid_index];
}
Grid_Atoms::~Grid_Atoms(){
    delete atoms;
    delete grid_basic;
    delete [] points;
}

std::vector<int> Grid_Atoms::get_match_basic_to_atoms(){
    return match_basic_to_atoms;
}
std::vector<int> Grid_Atoms::get_match_atoms_to_basic(){
    return match_atoms_to_basic;
}
