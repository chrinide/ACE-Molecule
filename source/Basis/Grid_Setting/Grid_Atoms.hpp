#pragma once 
#include "Teuchos_RCP.hpp"
#include "Grid_Setting.hpp"
#include "Grid_Basic.hpp"
#include "../Basis_Function/Basis_Function.hpp"
#include "../../Io/Atoms.hpp"
#include "../../Io/Periodic_table.hpp"
#include "Epetra_Comm.h"
#include "Epetra_Map.h"
#include <vector>
#include "../../Util/Time_Measure.hpp"

/**
 * @brief This class should be hidden behind Basis class and its method should not be called explicitly.
 **/
class Grid_Atoms: public Grid_Setting{
    public:
        void decompose(int index, const int* points, int* i_x, int* i_y, int* i_z);
        int  combine(int i_x,int i_y,int i_z, const int* points);
        int from_basic(int basic_grid_index);
        int to_basic(int current_grid_index);

        std::vector<int> get_match_atoms_to_basic();
        std::vector<int> get_match_basic_to_atoms();
        ///This is constructor that specifies radius of total grid.
        //Grid_Atoms(int* points, Basis_Function* basis,Atoms* atoms, double radius);
        Grid_Atoms(const int* points, Teuchos::RCP<Basis_Function> basis,Atoms* atoms, double factor, bool is_relative);
        ///This is constructor that specifies radius of total grid.
        Grid_Atoms(int* points, Teuchos::RCP<Basis_Function> basis,Atoms* atoms, std::vector<double> radius);
        //Grid_Atoms(const int* points, Teuchos::RCP<Basis_Function> basis,Atoms* atoms, std::vector<double> radius);
        ~Grid_Atoms();

    protected:
        Atoms* atoms;
        Grid_Setting* grid_basic;
        std::vector<int> match_atoms_to_basic;
        std::vector<int> match_basic_to_atoms;
        Teuchos::RCP<Time_Measure> timer;
};
