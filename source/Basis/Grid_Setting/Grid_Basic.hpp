#pragma once 
#include "Grid_Setting.hpp"

/**
 * @brief This class should be hidden behind Basis class and its method should not be called explicitly.
 **/
class Grid_Basic: public Grid_Setting{
    public:

        void decompose(int index,const int* points, int* i_x, int* i_y, int* i_z);
        int  combine(int i_x,int i_y,int i_z,const int* points);

        int from_basic(int basic_grid_index);
        int to_basic(int current_grid_index);

        Grid_Basic(int* points);
        Grid_Basic(const int* points);
        ~Grid_Basic();
};
