#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"
#include "Epetra_Map.h"
#include "Epetra_CrsMatrix.h"
#include "Epetra_Comm.h"// Need to include this.

#include "../Io/Atoms.hpp"
#include "Basis_Function/Basis_Function.hpp"
#include "Kinetic_Matrix.hpp"
#include "Grid_Setting/Grid_Setting.hpp"

class Kinetic_Matrix;

/**
 * @brief Abstract class containing all informations related with simulation box.
 * @author Sunghwan Choi
 * @date 2016.1

*/
class Basis{
    friend std::ostream& operator <<(std::ostream &c, const Basis &pmesh);
    friend std::ostream& operator <<(std::ostream &c, const Basis* pmesh);
    friend bool operator==(const Basis& mesh1, const Basis& mesh2);
    friend bool operator!=(const Basis& mesh1, const Basis& mesh2);

    friend class Kinetic_Matrix;

    public:
        /**
        @brief Constructor. Do not call it directly unless you are knowing what you are doing.
               Use Create_Basis if you want to create Basis class.
        @param basis Basis function class.
        @param kinetic_basis Basis function class used for kinetic energy calculation.
        @param grid_setting Simulation box specification class.
        @param map Epetra_Map corresponds to our simulation grid.
        */
        Basis(
            Teuchos::RCP<Basis_Function> basis,
            Teuchos::RCP<Basis_Function> kinetic_basis,
            Teuchos::RCP<Grid_Setting> grid_setting,
            Teuchos::RCP<Epetra_Map> map
        );
        //~Basis();

        /**
         * @return Number of points in x, y, z-axis in length 3 C-array.
         * @note Do not delete returned pointer.
         * @todo Remove pointer usage.
         **/
        const int* get_points() const;
        /**
         * @brief Used for Grid_Atom construction?
         * @note Can this replace get_neighbors in Tricubic_Interpolation?
         **/
        void get_neighboring_points(int axis, int axis_points, std::vector<int>& neighboring_points) const;
        /**
         * @return Scaling factor(grid spacing) along x, y, z-axis in length 3 C-array.
         **/
        const double* get_scaling() const;
        /**
         * @return Total number of points in the grid.
         **/
        int get_original_size() const;
        /**
         * @return Basis type (Sinc, Laguree, etc.).
         **/
        std::string get_basis_type() const;
        /**
         * @return Grid type (Grid_Basic, Grid_Sphere, Grid_Atom).
         **/
        std::string get_grid_type() const;
        /**
         * @return x-, y-, z- direction grid coordinates.
         * @note Do not delete retuned pointer.
         * @todo Remove pointer usage.
         **/
        const double** get_scaled_grid() const;
        /**
         * @return Distance of input point from origin.
         **/
        double compute_distance(int i_x,int i_y, int i_z) const;
        /**
         * @return Distance between input points.
         **/
        double compute_distance(int i_x,int i_y, int i_z, int j_x, int j_y, int j_z) const;
        double find_closest_point(double x, double y, double z, int* i_x, int* i_y, int* i_z) const;

//        bool operator== (const Basis mesh) const;
//        bool operator!= (const Basis mesh) const;
        //bool operator== (const RCP<Basis> basis);
        /**
         * @return Epetra_Map corresponds to this basis.
         **/
        Teuchos::RCP<const Epetra_Map> get_map() const;
        /**
         * @return Kinetic operator matrix of this grid.
         **/
        Teuchos::RCP<const Epetra_CrsMatrix> get_kinetic_matrix() const;
        //RCP<const Epetra_CrsMatrix> get_kinetic_matrix();
        double compute_basis(int j, double point, int axis) const;
        double compute_second_der(int j, double point, int axis) const;
        double compute_first_der(int j, double point, int axis) const;
        std::vector< std::vector< double> > make_kinetic_vector();

//        virtual std::vector< int > get_shell_point(Basis_Function* basis, double depth) =0;
//        virtual std::vector< int > get_shell_point(Basis_Function* basis) =0;

        /**
         * @brief Decompose 1-D form index into the indices of x-, y-, z-coordinates.
         * @param index 1-D form index.
         * @param i_x Output. Pointer to x-axis index.
         * @param i_y Output. Pointer to y-axis index.
         * @param i_z Output. Pointer to z-axis index.
         * @todo Probabily should change to int&.
         **/
        void decompose(int index, int* i_x, int* i_y, int* i_z) const;
        /**
         * @brief Combine x-, y-, z-direction indices into 1-D form index.
         * @param i_x x-axis index.
         * @param i_y y-axis index.
         * @param i_z z-axis index.
         * @return 1-D form index.
         **/
        int combine(int i_x,int i_y,int i_z) const;

//        bool operator== (RCP<Basis> mesh);
//        RCP<Grid_Setting> get_grid_setting();
//        RCP<Basis_Function> get_basis();

//        virtual int from_basic(int basic_grid_index)=0;
//        virtual int to_basic(int current_grid_index)=0;
        void write_cube(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Epetra_Vector> entity,
            int PID=0
        ) const;
        void write_cube(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::RCP<Epetra_MultiVector> entity,
            int index_offset = 0
        ) const;
        void write_cube(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > entity
        ) const;
        void write_cube(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > entity,
            int index_offset = 0
        ) const;

        void read_cube(
            std::string name,
            Teuchos::RCP<Epetra_Vector>& entity,
            bool is_bohr = true
        ) const;
        void read_cube(
            Teuchos::Array<std::string> names,
            Teuchos::RCP<Epetra_MultiVector>& entity,
            bool is_bohr = true
        ) const;
        void parallel_read_cube(
            Teuchos::Array<std::string> names,
            Teuchos::RCP<Epetra_MultiVector>& entity,
            bool is_bohr = true
        ) const;

        void read_cube(std::string name, double* retval, bool is_bohr = true) const;

        void write_along_axis(
            std::string name, int axis,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > entity,
            std::vector<double> offset = std::vector<double>(),
            int index_offset = 0
        ) const;
        void write_along_axis(
            std::string name, int axis,
            Teuchos::RCP<Epetra_MultiVector> entity,
            std::vector<double> offset = std::vector<double>(),
            int index_offset = 0
        ) const;
        void write_along_axis(
            std::string name, int axis,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > entity,
            std::vector<double> offset = std::vector<double>()
        ) const;
        void write_along_axis(
            std::string name, int axis,
            Teuchos::RCP<Epetra_Vector> entity,
            std::vector<double> offset = std::vector<double>()
        ) const;

        void write_along_axis(
            std::string name,
            std::vector< std::array<double,3> > positions,
            Teuchos::Array< Teuchos::RCP<Epetra_MultiVector> > entity,
            int index_offset = 0
        ) const;
        void write_along_axis(
            std::string name,
            std::vector< std::array<double,3> > positions,
            Teuchos::RCP<Epetra_MultiVector> entity,
            int index_offset = 0
        ) const;
        void write_along_axis(
            std::string name,
            std::vector< std::array<double,3> > positions,
            Teuchos::Array< Teuchos::RCP<Epetra_Vector> > entity
        ) const;
        void write_along_axis(
            std::string name,
            std::vector< std::array<double,3> > positions,
            Teuchos::RCP<Epetra_Vector> entity
        ) const;

        void parallel_write_cube(
                   std::string name,
                Teuchos::RCP<const Atoms> atoms,
                   Teuchos::RCP<Epetra_MultiVector> entity,
                int index_offset=0
        ) const;

        void write_polarizability(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            std::vector<double> wavelength,
            std::vector<std::complex<float> > polarizability
        ) const;

        void write_dipole(
            std::string name,
            Teuchos::RCP<const Atoms> atoms,
            std::vector<double> wavelength,
            std::vector<std::vector< std::vector<std::complex<float> > > > dipole
        ) const;

        void get_stencil(int* max_stencil_x,
                int* max_stencil_y,
                int* max_stencil_z) const ;

    protected:
        double compute_kinetic(int i, int j, int axis) const;
        //int compute_kinetic_matrix();
        Teuchos::RCP<Grid_Setting> grid_setting;
        Teuchos::RCP<Basis_Function> basis;
        Teuchos::RCP<Basis_Function> kinetic_basis;
        Teuchos::RCP<Epetra_Map> map;
};
