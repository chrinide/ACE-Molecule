#pragma once
#include <iostream>
#include <vector>
#include "Atom.hpp"
/**
 * @brief Class for handle atoms information.
  */
class Atoms
{
    friend std::ostream &operator <<(std::ostream &c, const Atoms &atoms);
    friend std::ostream &operator <<(std::ostream &c, Atoms* patoms);

  public :
    Atoms();//< @brief Constructor.
    Atoms(std::vector<Atom> atom_list);//< @brief Construct from Atom classes.
    Atoms(const Atoms& atoms);//< @brief Copy constructor.
    ~Atoms();
    /**
     * @return Vector of atomic numbers of molecule. Ordered.
     **/
    std::vector<int> get_atomic_numbers() const ;
    /**
     * @return Vector of x, y, z coordinates.
     **/
    std::vector<std::array<double ,3> > get_positions() const;
    std::vector<double> get_radii() const;
    std::vector<double> get_charges() const;
    std::array<double,3> get_center_of_mass() const;
    /**
     * @brief works like std::vector.
     **/
    void pop();
//    void push(Atom atom);
    /**
     * @brief works like std::vector.
     **/
    void push(Atom& atom, bool make_connection_table=true);
    /**
     * @brief works like std::vector.
     **/
    Atom& operator[] (int i);
    /**
     * @brief works like std::vector.
     **/
    Atom operator[] (int i) const;
    /**
     * @return Total number of atoms.
     **/
    int get_size() const;
    double distance(int i, int j) const;
    void make_connection();
    std::vector< std::vector<int> > get_connection() const;
    bool operator==( Atoms new_atoms);
    bool operator!=( Atoms new_atoms);
    Atoms& operator=(const Atoms& new_atoms);
    /**
     * @return Atom type index. Atom type index is ordered in ascending way.
     **/
    std::vector<int> get_atom_types() const;
    /**
     * @param Atom index.
     * @return Atom type index. Atom type index is ordered in ascending way.
     **/
    int get_atom_type(int index) const;
    /**
     * @param Atom type index.
     * @return Atomic number.
     **/
    int get_atomic_number_from_type(int i_type) const;
    std::vector<double> get_comp_charge_exps();
    /**
     * @brief Number of elements in molecule. Ex) For C2H4, returns 2 and for C2H2O2, returns 3.
     * @note Useful for pseudopotential file reading, etc.
     * @return Number of elements in molecule. Ex) For C2H4, returns 2 and for C2H2O2, returns 3.
     **/
    int get_num_types() const;
    void move_center_of_mass(double new_center_of_mass_x, double new_center_of_mass_y,double new_center_of_mass_z);
    void move_positions(double x, double y,double z);
//    void move_center_of_mass_to_origin();
    void get_vertex_info(int& index1, double& min_x, int& index2, double& max_x,
                         int& index3, double& min_y, int& index4, double& max_y,
                         int& index5, double& min_z, int& index6, double& max_z, std::vector<double> double_radious, bool is_relative_radius) const;

    void get_vertex_info(int& index1, double& min_x,
            int& index2, double& max_x,
            int& index3, double& min_y,
            int& index4, double& max_y,
            int& index5, double& min_z,
            int& index6, double& max_z,
            std::vector<double> double_radious,
            bool is_relative_radius,
            double* cell ) ;
    void reset_connection_table();
  private :
    std::vector<Atom> atom_list;
    std::vector< std::vector<int> > connection_table;

};
