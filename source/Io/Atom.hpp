#pragma once
#include <iostream>
#include <string>
#include <array>
class Atom
{
    friend std::ostream &operator<<(std::ostream &c, Atom &atom);
    friend std::ostream &operator<<(std::ostream &c, Atom* patom);
  public : 
    Atom(); 
    ~Atom();
    //Atom(Atom& atom); 
    Atom(int atomic_number, std::array<double,3> position, double charge);
    Atom(std::string symbol, std::array<double,3> position, double charge);
    Atom(Atom& atom);
    Atom(const Atom& atom);
    int get_atomic_number();
    int get_atomic_number() const;

    double get_valence_charge() const;
    void set_valence_charge(double valence_charge);

    std::array<double,3> get_position() const;
//    double * get_position() const;
    double get_charge();
    double get_charge() const;
    double get_radius();

    void set_position(std::array<double,3> position);

    std::string get_symbol();
    double get_comp_charge_exp();
    bool operator==(Atom new_atom);
    bool operator!=(Atom new_atom);
    Atom& operator=(const Atom& original_atom);
    Atom& operator=(Atom& original_atom);
    
  private : 
    double charge;
    double valence_charge;
    std::array<double,3> position;
    int atomic_number;
    std::string symbol;

};

