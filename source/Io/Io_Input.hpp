#pragma once
#include <ctype.h>
#include <fstream>
#include <string>
#include <algorithm>
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"
//#include "Atoms.hpp"

/**
 * @brief Read input parameters and process.
 * @author Sungwoo Kang
 * @date 2015
 * @version 0.0.1
 **/
class Io_Input {
    public:
        /**
         * @brief Read tree-structured input file and put it as ParameterList.
         * @param filename Input file to read.
         * @param parameters Output parameter.
         * @note See read_sublist(): Actual reading routine.
         **/
        void read_input(std::string filename, Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > &parameters);
        /**
         * @brief Split input parameters into computation order.
         * @details Split input parameter sublists into BasicInformation + Compute Interface:
         *      BasicInformation
         *      Guess              ->   BasicInformation + Guess
         *      Scf                ->   BasicInformation + Scf
         * Name of returning ParameterList follows that of computation routine
         * @param total_parameter ParameterList to split
         * @return Array of split parameters.
         **/
        Teuchos::Array< Teuchos::RCP< Teuchos::ParameterList> > split( Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> > total_parameter );

    private:
        /**
         * @brief Read given sublist. Returns if it meets corresponding End.
         * @details Read sublist. Recursively call itself if it meets new sublist. Returns if it meets End.
         * @param input_fstream Input file stream to read.
         * @param list_name Sublist name to read.
         * @return Resulting ParameterList for the sublist.
         * @note See read_line(), read_multiline(): Reading line.
         **/
        Teuchos::RCP<Teuchos::ParameterList> read_sublist(
                std::ifstream &input_fstream, std::string list_name, int& depth
        );
        /**
         * @brief Read one line and put it into ParameterList.
         * @details Read a line. It is putted into ParameterList, or putted into returning map.
         * @param paramlist Output parameterlist.
         * @param input_line Line to process.
         * @param inputs_list
         *        If parameter name is in list_input_list, put it in this variable instead of putting this in paramlist.
         *        Key == parameter name, val == list of variables.
         *        Values in inputs_list should be inserted to ParameterList at the end of sublist reading.
         * @note Since some variables should be inserted to ParameterList afterwards and others are inserted here, this seems to be bad design.
         *       Maybe we should migrate ParameterList processing to read_sublist().
         *       See tokenize(), also.
         * @todo If C++ support is enabled or we can use regex, we should change number/string identification using regex "^(\\-|\\+)?[0-9]*(\\.[0-9]+)?".
         **/
        void read_line(
                Teuchos::RCP<Teuchos::ParameterList> &paramlist,
                std::string input_line,
                std::map< std::string, std::vector<std::string> > &inputs_list
        );
        /**
         * @brief Read several lines and put it into ParameterList.
         * @param paramlist Output parameterlist.
         * @param input_fstream Stream of the file to process.
         * @param input_name Name of the input.
         * @note This is included to read geometry file at input, not other file.
         *       This is bad design, and I do not think this function is used...
         **/
        void read_multiline(
                Teuchos::RCP<Teuchos::ParameterList> &paramlist,
                std::ifstream &input_fstream,
                std::string input_name
        );

        /**
         * @brief Process string and put it into vector. Only first two values are treated.
         * @details Split string using whitespace as tokens, respecting quoted values with whitespace.
         * @param line string to process.
         * @return Resulting strings, with size zero or two.
         * @note It seems that this function should be in String_Util.
         * @todo This function may need to process more-than-two values.
         **/
        std::vector<std::string> tokenize( std::string line );

        /**
         * @brief If a parameter name is in this vector, put it as vector<string>, and does not ignore repeating value. If not, repeating value will be ignored, except the last one.
         **/
        std::vector<std::string> list_input_list;
        /**
         * @brief If a parameter name is in this vector, %% will be treated as multiline input.
         **/
        std::vector<std::string> multiline_input_list;

        /**
         * @brief Prints warning if input key has underbar.
         **/
        bool check_depreciation = true;
        int depreciation_count = 0;
        std::string depreciation_msg = "";
};
