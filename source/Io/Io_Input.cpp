#include "Io_Input.hpp"
#include <iostream>
#include <stdexcept>
#include "Teuchos_TestForException.hpp"
#include "../Util/String_Util.hpp"
#include "../Util/ParamList_Util.hpp"
//#include "../Util/Verbose.hpp"

using std::vector;
using std::map;
using std::string;
using std::ifstream;
using std::istringstream;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::ParameterList;
using Teuchos::Array;

void Io_Input::read_input(string filename, Array< RCP<ParameterList> > &parameters){
    this->list_input_list.push_back("PSFilenames");
    this->list_input_list.push_back("InitialFilenames");
    this->list_input_list.push_back("XFunctional");
    this->list_input_list.push_back("CFunctional");
    this->list_input_list.push_back("XCFunctional");
    this->list_input_list.push_back("GaussianPotential");
    this->list_input_list.push_back("InputEigenvalues");
    this->list_input_list.push_back("Radius");
    this->list_input_list.push_back("Gaussian");
    this->list_input_list.push_back("Txt");
    this->list_input_list.push_back("ACE");
    this->list_input_list.push_back("AtomicPolarizability");
    this -> list_input_list.push_back("OccupationList");
    this -> multiline_input_list.push_back("Geometry");

    ifstream inputfile(filename.c_str());
    if( inputfile.fail() ){
        throw std::invalid_argument("Cannot read input file "+ filename+"!");
    }
    int depth = 0;
    //parameters = rcp( new ParameterList( *this->read_sublist( inputfile, "main", depth ) ) );
    parameters.clear();
    string line, sublist_name;
    while( inputfile.eof() == false ){
        getline( inputfile, line );
        line = String_Util::trim(line);
        if(line.find("#") != string::npos){
            line = String_Util::trim(String_Util::strSplit(line, "#", true)[0]);
        }
        if(line.size() < 1){
            continue;
        }

        if(line.find("%%") != string::npos){
            vector<string> tokens = this -> tokenize(line);
            if( tokens.size() == 0 ){
                continue;
            }

            sublist_name = tokens[1];

            if( sublist_name == "End" ){
                sublist_name.clear();
                --depth;
                break;
            } else {
                ++depth;
                //paramlist -> sublist(sublist_name) = ParameterList( *this -> read_sublist( input_fstream, sublist_name, depth ) );
                parameters.append(rcp(new ParameterList(*this -> read_sublist( inputfile, sublist_name, depth ))));
                sublist_name.clear();
            }
        }
    }
    /*
    if( depth != 0 and input_fstream.eof() ){
        std::cout << "File ended while scanning use of %% " << list_name << "!" << std::endl;
        std::cout << *paramlist << std::endl;
        throw std::invalid_argument("File ended while scannign use of %%  "+ list_name+"!");
    }
    */

    inputfile.close();
    TEUCHOS_TEST_FOR_EXCEPTION(
        ParamList_Util::get_subparameter(parameters, "BasicInformation") == Teuchos::null,
        std::invalid_argument, "There is no BasicInformation section!"
    );
    ParamList_Util::get_subparameter(parameters, "BasicInformation") -> set<string>("InpName", filename);
    if(this -> check_depreciation and this -> depreciation_count > 0){
        std::cout << this -> depreciation_msg << std::endl;
        std::cerr << this -> depreciation_msg << std::endl;
        //throw std::invalid_argument(this -> depreciation_msg);
    }

    return;
}

RCP<ParameterList> Io_Input::read_sublist( ifstream &input_fstream, string list_name, int &depth){
    string line, sublist_name;
    map< string, vector<string> > inputs_list;
    RCP<ParameterList> paramlist = rcp( new ParameterList( list_name ) );

    while( input_fstream.eof() == false ){
        getline( input_fstream, line );
        line = String_Util::trim(line);
        if(line.find("#") != string::npos){
            line = String_Util::trim(String_Util::strSplit(line, "#", true)[0]);
        }
        if(line.size() < 1){
            continue;
        }

        if(line.find("%%") != string::npos){
            vector<string> tokens = this -> tokenize(line);
            if( tokens.size() == 0 ){
                continue;
            }

            if( find(this->multiline_input_list.begin(), this->multiline_input_list.end(), tokens[1]) != this->multiline_input_list.end() ){
                this -> read_multiline( paramlist, input_fstream, tokens[1] );
                continue;
            }

            sublist_name = tokens[1];
            if(this -> check_depreciation){
                std::size_t found = sublist_name.find_first_of('_');
                if(found != std::string::npos and found != 0){
                    ++this -> depreciation_count;
                    this -> depreciation_msg += "Input convention forbids _ in section name (Detected " + sublist_name + ")!\n";
                }
            }

            if( sublist_name == "End" ){
                sublist_name.clear();
                --depth;
                break;
            } else {
                ++depth;
                paramlist -> sublist(sublist_name) = ParameterList( *this -> read_sublist( input_fstream, sublist_name, depth ) );
                sublist_name.clear();
            }
        } else {
            this -> read_line( paramlist, line, inputs_list );
        }
    }
    if( depth != 0 and input_fstream.eof() ){
        std::cout << "File ended while scanning use of %% " << list_name << "!" << std::endl;
        std::cout << *paramlist << std::endl;
        throw std::invalid_argument("File ended while scannign use of %%  "+ list_name+"!");
    }

    for( map< string, vector<string> >::iterator it = inputs_list.begin(); it != inputs_list.end(); ++it ){
        Array<string> values (it->second.begin(),it->second.end());
        paramlist->set(it->first,values);
    }
    return paramlist;
}

void Io_Input::read_line(
        RCP<ParameterList> &paramlist,
        std::string input_line,
        std::map< std::string, std::vector<std::string> > &inputs_list
){
    vector<string> tokens = this -> tokenize(input_line);
    if( tokens.size() == 0 ){
        return;
    }
    string input_name = tokens[0];
    string input_val = tokens[1];
    if(this -> check_depreciation){
        std::size_t found = input_name.find_first_of('_');
        if(found != std::string::npos and found != 0){
            ++this -> depreciation_count;
            this -> depreciation_msg += "Input convention forbids _ in input key (Detected " + input_name + ")!\n";
        }
    }

    if( input_name == "%%" ){
        return;
    }

    if( find(this->list_input_list.begin(), this->list_input_list.end(), input_name) != this->list_input_list.end() ){
        inputs_list[input_name].push_back(input_val);
    } else {
        //if(!isdigit(input_val[0]) and input_val.find_first_of("-") != 0){
        if(input_val.find_first_not_of("-.0123456789Ee") != std::string::npos){
        // This line can be changed to regex using "^(\\-|\\+)?[0-9]*(\\.[0-9]+)?" with C++11.
            paramlist -> set(input_name,input_val);
        }
        else{
            if(input_val.find_first_of(".eE") == string::npos){
                int ival_int=atoi(input_val.c_str());
                paramlist -> set(input_name,ival_int);
            }
            else{
                double ival_double=atof(input_val.c_str());
                paramlist -> set(input_name,ival_double);
            }
        }
    }
}

void Io_Input::read_multiline(
        RCP<ParameterList> &paramlist,
        ifstream &input_fstream,
        string input_name
){
    string data;
    while( input_fstream.eof() == false ){
        string input_line;
        getline( input_fstream, input_line );
        vector<string> tokens = this -> tokenize(input_line);

        if( tokens.size() == 0 ){
            break;
        }
        if( tokens[0] == "%%" ){
            break;
        }
        data += input_line;
        data += "\n";
    }
    paramlist -> set(input_name, data);
}

std::vector<std::string> Io_Input::tokenize( std::string line ){
    vector<string> tokens;

    string::size_type commentPos = line.find_first_of("#",0);
    if( commentPos != string::npos ){
        string tmp;
        tmp = line.substr(0, commentPos);
        line = tmp;
    }

    // Pass empty lines only with space
    string::size_type lastPos = line.find_first_not_of(" \t",0);
    if(lastPos == string::npos){
        return tokens;
    }

    string::size_type sublistPos = line.find_first_of("%%", 0);
    if( sublistPos != string::npos ){
        line.replace( sublistPos, 2, "%% " );
    }

    if( line.size() == 0 ){
        return tokens;
    }

    string tmp1, tmp2, tmp3;
    istringstream iss(line);
    iss >> tmp1;
    iss >> tmp2;

    if (std::all_of(tmp2.begin(),tmp2.end(),isspace)!=0){
        std::cout << "Key should have a value (" << tmp1 << ")" <<std::endl;
#ifdef EDISON
        if(tmp1 == "AbsoluteRadius"){
            tmp2 = "1";
        } else {
            exit(-1);
        }
#else
        exit(-1);
#endif
    }

    // This if statement handles quoted values with whitespace inside
    if(tmp2.find("'")!=string::npos || tmp2.find("\"")!=string::npos) {
        tmp2.erase(tmp2.begin());
        while (true){
            iss >> tmp3;
            if (tmp3.find("'")!=string::npos || tmp3.find("\"")!=string::npos){
                tmp3.erase(tmp3.end()-1);
                tmp2.append(" ");
                tmp2.append(tmp3);
                break;
            }
            if (tmp3==""){
                break;
            }
            tmp2.append(" ");
            tmp2.append(tmp3);

        }
    }
    tokens.push_back(tmp1);
    tokens.push_back(tmp2);
    return tokens;
}

Teuchos::Array< RCP< ParameterList> > Io_Input::split( Teuchos::Array< RCP<ParameterList> > total_parameter ){
    Teuchos::Array< RCP< ParameterList> > retval;
    //vector<string> param_names;
    std::map<std::string, int> param_counter;
    RCP<Teuchos::ParameterList> parameters_basic = ParamList_Util::get_subparameter(total_parameter, "BasicInformation");
    for( Array< RCP<ParameterList> >::iterator it = total_parameter.begin(); it != total_parameter.end(); ++it){
        std::string param_name((*it) -> name());
        if( param_name != "BasicInformation" ){
            if( param_counter.find(param_name) == param_counter.end() ){
                param_counter[param_name] = 0;
            }
            RCP<ParameterList> sub_params = rcp( new ParameterList( param_name ) );
            sub_params -> sublist("BasicInformation") = *parameters_basic;
            sub_params -> sublist( param_name ) = *ParamList_Util::get_subparameter(total_parameter, param_name, param_counter[param_name]);
            ++param_counter[param_name];
            retval.push_back( sub_params );
        }
        //param_names.push_back(param_name);
    }
    if( retval.size() == 0 ){
        std::cout << "No Input parameter group detected: Carefully revise %% End." << std::endl;
        throw std::invalid_argument("No Input parameter group detected: Carefully revise %% End.");
    }
    return retval;
}
