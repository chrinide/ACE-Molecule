#pragma once
#include "Atoms.hpp"
#include <string>
#include <fstream>

/**
 * @brief Read geometry file name and returns Atoms object.
 **/
class Io_Atoms
{
    public :
        Atoms read_atoms(std::string filename, std::string format = "xyz");
        void write_atoms(std::string filename, Atoms atoms, std::string format = "xyz");
        Atoms read_xyz(std::ifstream &input);
        Atoms read_pdb(std::ifstream &input);
        Atoms read_coord(std::ifstream &input);
};
