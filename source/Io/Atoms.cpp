#include "Atoms.hpp"
#include <iomanip>
#include <algorithm>
#include <stdexcept>
#include <cmath>
#include "../Util/Verbose.hpp"
#include "Periodic_table.hpp"
#include "../Util/String_Util.hpp"

using std::abs;
using std::max;
using std::min;
using std::setw;
using std::ostream;
using std::vector;

Atoms::Atoms()
{
    connection_table= vector< vector<int> >(1);
    for(int i = 0; i < 1; i++){
        connection_table[i] = vector<int>(1);
    }
}
Atoms::Atoms(vector<Atom> atom_list)
{
    this->atom_list = atom_list;
    this->connection_table = vector< vector<int> >(atom_list.size());
    for(int i = 0; i < atom_list.size(); i++){
        connection_table[i] = vector<int>(atom_list.size());
        for(int j = 0; j < atom_list.size(); j++){
            connection_table[i][j] = 0;
        }
    }
}
Atoms::Atoms(const Atoms& atoms)
{
    this->operator=(atoms);
}
Atoms::~Atoms(){
    atom_list.clear();
    /*
    for(int i = 0; i < atom_list.size(); i++){
        delete [] connection_table[i];
    }
    delete[] connection_table;
    */
}
vector<int> Atoms::get_atomic_numbers() const
{
    vector<int> retval;
    for(int i = 0; i < atom_list.size(); i++){
        retval.push_back(atom_list.at(i).get_atomic_number());
    }

    return retval;
}
vector<std::array<double,3> > Atoms::get_positions() const
{
    vector<std::array<double,3> > retval;
    for(int i = 0; i < atom_list.size(); i++){
        retval.push_back(atom_list.at(i).get_position());
    }
    return retval;
}
vector<double> Atoms::get_radii() const
{
    vector<double> retval;
    for(int i=0; i< atom_list.size(); i++){
        retval.push_back(Periodic_atom::covalent_radii[atom_list.at(i).get_atomic_number()]);
    }
    return retval;
}
vector<double> Atoms::get_charges() const
{
    vector<double> retval;

    for(int i = 0; i < atom_list.size(); i++){
        retval.push_back(atom_list.at(i).get_charge());
    }

    return retval;
}
std::array<double,3> Atoms::get_center_of_mass() const
{
    std::array<double,3> retval;

    for(int i = 0; i < 3; i++){
        retval[i] = 0;
    }

    double tw = 0;

    for(int i = 0; i < this->atom_list.size(); i++){
        tw += Periodic_atom::mass_table[this->atom_list.at(i).get_atomic_number() - 1];
    }

    for(int i = 0; i < this->atom_list.size(); i++){
        retval[0] += Periodic_atom::mass_table[atom_list.at(i).get_atomic_number() - 1] * ((atom_list.at(i).get_position()[0]));
        retval[1] += Periodic_atom::mass_table[atom_list.at(i).get_atomic_number() - 1] * ((atom_list.at(i).get_position()[1]));
        retval[2] += Periodic_atom::mass_table[atom_list.at(i).get_atomic_number() - 1] * ((atom_list.at(i).get_position()[2]));
    }

    for(int i = 0; i < 3; i++){
        retval[i] = retval[i]/tw;
    }

    return retval;
}
void Atoms::pop()
{
    /*
    for(int i = 0; i < atom_list.size(); i++){
        delete connection_table[i];
    }
    delete[] connection_table;
    */

    this->atom_list.pop_back();

    this->connection_table = vector< vector<int> >(atom_list.size());
    for(int i = 0; i < atom_list.size(); i++){
        connection_table[i] = vector<int>(atom_list.size());
        for(int j = 0; j < atom_list.size(); j++){
            connection_table[i][j] = 0;
        }
    }
}
/*
   void Atoms::push(Atom atom)
   {
   this->atom_list.push_back(atom);

   this->connection_table = new int * [atom_list.size()];
   for(int i = 0; i < atom_list.size(); i++){
   connection_table[i] = new int [atom_list.size()];
   for(int j = 0; j < atom_list.size(); j++){
   connection_table[i][j] = 0;
   }
   }
   }
   */
void Atoms::push(Atom& atom, bool make_connection_table )
{
    /*
    for(int i = 0; i < this->atom_list.size(); i++){
        delete connection_table[i];
    }
    delete[] connection_table;
    */

    Atom new_atom(atom);
    this->atom_list.push_back(new_atom);
    if(make_connection_table){
        this->connection_table = vector< vector<int> > (atom_list.size());
        for(int i = 0; i < atom_list.size(); i++){
            connection_table[i] = vector<int> (atom_list.size(), 0.0);
        }
    }
}
/*
   int Atoms::get_size()
   {
   return this->atom_list.size();
   }
   */
int Atoms::get_size() const
{
    return this->atom_list.size();
}

   Atom& Atoms::operator[] (int i)
   {
   return this->atom_list.at(i);
   }

Atom Atoms::operator[] (int i) const
{
    return this->atom_list.at(i);
}

double Atoms::distance(int i, int j) const
{
    double retval;
    double x2 = pow((atom_list.at(i).get_position()[0]
                - this->atom_list.at(j).get_position()[0]), 2);
    double y2 = pow((atom_list.at(i).get_position()[1]
                - this->atom_list.at(j).get_position()[1]), 2);
    double z2 = pow((atom_list.at(i).get_position()[2]
                - this->atom_list.at(j).get_position()[2]), 2);

    retval = sqrt(x2+y2+z2);

    return retval;
}
void Atoms::make_connection()
{
    for(int i = 0; i < atom_list.size(); i++){
        for(int j = 0; j < atom_list.size(); j++){
            int number1 = atom_list.at(i).get_atomic_number() - 1;
            int number2 = atom_list.at(j).get_atomic_number() - 1;
            if((distance(i,j)<=((Periodic_atom::covalent_radii[number1]
                                + Periodic_atom::covalent_radii[number2]) * 1.1)) & (i != j)){
                this->connection_table[i][j]++;
            }
        }
    }
}

vector< vector<int> > Atoms::get_connection() const
{
    /*
    int ** retval = new int * [atom_list.size()];
    for(int i = 0; i < atom_list.size(); i++){
        retval[i] = new int[atom_list.size()];
        for(int j = 0; j< atom_list.size(); j++){
            retval[i][j] = connection_table[i][j];
        }
    }
    return retval;
    */
    return this -> connection_table;
}
bool Atoms::operator==( Atoms new_atoms){
    if (this->get_size()!=new_atoms.get_size() ){
        return false;
    }
    for (int i =0;i < get_size(); i++){
        if (this->operator[](i)!=new_atoms[i]){
            return false;
        }
    }

    return true;
}

bool Atoms::operator!=( Atoms new_atoms){
    if (this->operator==(new_atoms)){
        return false;
    }
    return true;
}

Atoms& Atoms::operator=( const Atoms& original_atoms){
    for (int i =0;i <original_atoms.get_size();i++){
        atom_list.push_back(Atom(original_atoms[i]) );
    }
    vector< vector<int> > original_connection = original_atoms.get_connection();

    /*
    this->connection_table = new int * [atom_list.size()];
    for(int i = 0; i < atom_list.size(); i++){
        connection_table[i] = new int [atom_list.size()];
        for(int j = 0; j < atom_list.size(); j++){
            connection_table[i][j] = original_connection[i][j];
        }
    }
    */
    this -> connection_table = original_connection;
    return *this;
}

ostream &operator <<(ostream &c, const Atoms &atoms){
    c << "====================  Atoms  =====================" <<std::endl;
    vector< std::array<double,3> > positions = atoms.get_positions();
    vector<int> atomic_numbers = atoms.get_atomic_numbers();

    for (int i =0;i < atoms.get_size();i++ ){
        c << setw(2) << atomic_numbers.at(i)  << setw(15) << positions.at(i)[0] << setw(15) << positions.at(i)[1] << setw(15) << positions.at(i)[2] << std::endl;
    }
    c << "==================================================" <<std::endl;
    return c;
}
ostream &operator <<(ostream &c, Atoms* patoms){
    Verbose::all() << *patoms ;
    return c;
}
int Atoms::get_atom_type(int index) const
{
    int return_val;
    vector<int> atom_types = get_atom_types();
    for (int i =0;i<atom_types.size();i++){
        if(atom_types[i] == atom_list[index].get_atomic_number() ){
            return_val = i;
            break;
        }
    }
    return return_val;
}
vector<int> Atoms::get_atom_types() const
{
    vector<int> atom_type;
    if(this -> atom_list.size() > 0){
        atom_type.push_back(this->atom_list.at(0).get_atomic_number());
        int temp = 0;
        for(int i = 1; i < this->atom_list.size(); i++){
            for(int j = 0; j < atom_type.size(); j++){
                if(this->atom_list.at(i).get_atomic_number() != atom_type.at(j))
                    temp++;
            }
            if(temp == atom_type.size()){
                atom_type.push_back(this->atom_list.at(i).get_atomic_number());
            }
            temp = 0;
        }
    }

    sort(atom_type.begin(), atom_type.end());
    return atom_type;
}

int Atoms::get_atomic_number_from_type(int i_type) const {
    return get_atom_types()[i_type];
}

vector<double> Atoms::get_comp_charge_exps(){
    vector<int> atom_types = get_atom_types();
    vector<double> return_val;
    for (int i=0;i<atom_types.size();i++){
        return_val.push_back(Periodic_atom::comp_charge_exp[atom_types[i] - 1]);
    }
    return return_val;
}

int Atoms::get_num_types() const
{
    vector<int> atom_type;
    if(this -> atom_list.size() > 0){
        atom_type.push_back(this->atom_list.at(0).get_atomic_number());
        for(int i = 1; i < this->atom_list.size(); i++){
            int temp = 0;
            for(int j = 0; j < atom_type.size(); j++){
                if(this->atom_list.at(i).get_atomic_number() != atom_type.at(j))
                    temp++;
            }
            if(temp == atom_type.size()){
                atom_type.push_back(this->atom_list.at(i).get_atomic_number());
            }
            temp = 0;
        }
    }
    return atom_type.size();
}

void Atoms::move_positions(double x, double y,double z){
    std::array<double,3> new_position ;
    for (int i=0;i<get_size();i++){
        auto old_position = atom_list.at(i).get_position();
        new_position[0] = old_position[0]+x;
        new_position[1] = old_position[1]+y;
        new_position[2] = old_position[2]+z;
        atom_list.at(i).set_position(new_position);
    }
    return;
}
void Atoms::move_center_of_mass(double new_center_of_mass_x, double new_center_of_mass_y,double new_center_of_mass_z){
    auto center_of_mass = get_center_of_mass();
    std::array<double,3> new_position;
    for (int i=0;i<get_size();i++){
        auto old_position = atom_list.at(i).get_position();
        new_position[0] = old_position[0]-center_of_mass[0]+new_center_of_mass_x;
        new_position[1] = old_position[1]-center_of_mass[1]+new_center_of_mass_y;
        new_position[2] = old_position[2]-center_of_mass[2]+new_center_of_mass_z;
        for(int j=0; j<3; j++){
            if(fabs(new_position[j])<1.e-10){
                new_position[j]=0.0;
            }
        }
        atom_list.at(i).set_position(new_position);
    }
    return;
}
/*
   void Atoms::move_center_of_mass_to_origin(){
   double* center_of_mass = get_center_of_mass();
   if ((center_of_mass[0]==0.0&&center_of_mass[1]==0.0)&&center_of_mass[2]==0.0){
   delete center_of_mass;
   return;
   }

   center_of_mass[0]

   delete center_of_mass;
   return ;
   }*/
void Atoms::get_vertex_info(int& index1, double& min_x,
                         int& index2, double& max_x,
                         int& index3, double& min_y,
                         int& index4, double& max_y,
                         int& index5, double& min_z,
                         int& index6, double& max_z,vector<double> double_radious, bool is_relative_radius ) const
{

    auto radii = get_radii();
    vector<std::array<double,3> > positions = get_positions();
    index1=0;
    index2=0;
    index3=0;
    index4=0;
    index5=0;
    index6=0;
    min_x=positions[0][0];
    max_x=positions[0][0];
    min_y=positions[0][1];
    max_y=positions[0][1];
    min_z=positions[0][2];
    max_z=positions[0][2];
    if(is_relative_radius){
        for(int i =1; i<positions.size(); i++){
            if(min_x<positions[i][0]-radii[i]*double_radious[get_atom_type(i)]*1.889725989){
                min_x=positions[i][0];
                index1=i;
            }
            if(max_x>positions[i][0] + radii[i]*double_radious[get_atom_type(i)]*1.889725989){
                max_x=positions[i][0];
                index2=i;
            }
            if(min_y<positions[i][1]- radii[i]*double_radious[get_atom_type(i)]*1.889725989){
                min_y=positions[i][1];
                index3=i;
            }
            if(max_y>positions[i][1] + radii[i]*double_radious[get_atom_type(i)]*1.889725989){
                max_y=positions[i][1];
                index4=i;
            }
            if(min_z<positions[i][2] - radii[i]*double_radious[get_atom_type(i)]*1.889725989){
                min_z=positions[i][2];
                index5=i;
            }
            if(max_z>positions[i][2] + radii[i]*double_radious[get_atom_type(i)]*1.889725989){
                max_z=positions[i][2];
                index6=i;
            }
        }
    }
    else{
        for(int i =1; i<positions.size(); i++){
            if(min_x<positions[i][0]-double_radious[get_atom_type(i)]*1.889725989){
                min_x=positions[i][0];
                index1=i;
            }
            if(max_x>positions[i][0] + double_radious[get_atom_type(i)]*1.889725989){
                max_x=positions[i][0];
                index2=i;
            }
            if(min_y<positions[i][1]- double_radious[get_atom_type(i)]*1.889725989){
                min_y=positions[i][1];
                index3=i;
            }
            if(max_y>positions[i][1] + double_radious[get_atom_type(i)]*1.889725989){
                max_y=positions[i][1];
                index4=i;
            }
            if(min_z<positions[i][2] - double_radious[get_atom_type(i)]*1.889725989){
                min_z=positions[i][2];
                index5=i;
            }
            if(max_z>positions[i][2] + double_radious[get_atom_type(i)]*1.889725989){
                max_z=positions[i][2];
                index6=i;
            }
        }
    }
    return;
}
void Atoms::get_vertex_info(int& index1, double& min_x,
                         int& index2, double& max_x,
                         int& index3, double& min_y,
                         int& index4, double& max_y,
                         int& index5, double& min_z,
                         int& index6, double& max_z,
                         vector<double> double_radious,
                         bool is_relative_radius,
                         double* cell )
{

    auto radii = get_radii();
    vector<std::array<double,3> > positions = get_positions();
    index1=0;
    index2=0;
    index3=0;
    index4=0;
    index5=0;
    index6=0;

    //Verbose::all() << "double radius " << double_radious[0] << std::endl;
    if(is_relative_radius){
        min_x=positions[0][0] - radii[0]*double_radious[0]*1.889725989;
        max_x=positions[0][0] + radii[0]*double_radious[0]*1.889725989;
        min_y=positions[0][1] - radii[0]*double_radious[0]*1.889725989;
        max_y=positions[0][1] + radii[0]*double_radious[0]*1.889725989;
        min_z=positions[0][2] - radii[0]*double_radious[0]*1.889725989;
        max_z=positions[0][2] + radii[0]*double_radious[0]*1.889725989;
        for(int i =1; i<positions.size(); i++){
            //cout << "minx " << min_x << " " << positions[i][0] << " " << radii[i]*double_radious[0]*1.889725989 <<  " " << i <<  " " << (positions[i][0]-radii[i]*double_radious[0]*1.889725989) <<  " " << radii[i] << endl;
            if(min_x > (positions[i][0]-radii[i]*double_radious[0]*1.889725989)){
                min_x=positions[i][0]-radii[i]*double_radious[0]*1.889725989;
                index1=i;
            }
            if(max_x < positions[i][0] + radii[i]*double_radious[0]*1.889725989){
                max_x=fabs(positions[i][0] + radii[i]*double_radious[0]*1.889725989);
                index2=i;
            }
            if(min_y > positions[i][1]- radii[i]*double_radious[0]*1.889725989){
                min_y=positions[i][1]- radii[i]*double_radious[0]*1.889725989;
                index3=i;
            }
            if(max_y < positions[i][1] + radii[i]*double_radious[0]*1.889725989){
                max_y=fabs(positions[i][1] + radii[i]*double_radious[0]*1.889725989);
                index4=i;
            }
            if(min_z > positions[i][2] - radii[i]*double_radious[0]*1.889725989){
                min_z=positions[i][2] - radii[i]*double_radious[0]*1.889725989;
                index5=i;
            }
            if(max_z < positions[i][2] + radii[i]*double_radious[0]*1.889725989){
                max_z=fabs(positions[i][2] + radii[i]*double_radious[0]*1.889725989);
                index6=i;
            }
        }
        cell[0]=max(fabs(min_x), max_x) + 0.01;
        cell[1]=max(fabs(min_y), max_y) + 0.01;
        cell[2]=max(fabs(min_z), max_z) + 0.01;
        Verbose::single(Verbose::Detail) << "x : " << min_x << " " << max_x << std::endl;
        Verbose::single(Verbose::Detail) << "y : " << min_y << " " << max_y << std::endl;
        Verbose::single(Verbose::Detail) << "z : " << min_z << " " << max_z << std::endl;
        /*
        cell[0] = (max_x-min_x)/2;
        cell[1] = (max_y-min_y)/2;
        cell[2] = (max_z-min_z)/2;
        //double move_x = -1*(positions[index2][0] + radii[index2] * double_radious[0] * 1.889725989 - positions[index1][0] + radii[index1] * double_radious[0] * 1.889725989)/2;
        //double move_y = -1*(positions[index4][1] + radii[index4] * double_radious[0] * 1.889725989 - positions[index3][1] + radii[index3] * double_radious[0] * 1.889725989)/2;
        //double move_z = -1*(positions[index6][2] + radii[index6] * double_radious[0] * 1.889725989 - positions[index5][2] + radii[index5] * double_radious[0] * 1.889725989)/2;
        move_positions(move_x, move_y, move_z);
        */
        //cout << *this << endl;
        //cout << "aaaa" << endl;
        //cout << cell[0] << endl;
        //cout << cell[1] << endl;
        //cout << cell[2] << endl;

    }
    else{
        min_x=positions[0][0] - double_radious[get_atom_type(0)]*1.889725989;
        max_x=positions[0][0] + double_radious[get_atom_type(0)]*1.889725989;
        min_y=positions[0][1] - double_radious[get_atom_type(0)]*1.889725989;
        max_y=positions[0][1] + double_radious[get_atom_type(0)]*1.889725989;
        min_z=positions[0][2] - double_radious[get_atom_type(0)]*1.889725989;
        max_z=positions[0][2] + double_radious[get_atom_type(0)]*1.889725989;
        for(int i =1; i<positions.size(); i++){
            //cout << "minx " << min_x << " " << positions[i][0] << " " << radii[i]*double_radious[0]*1.889725989 <<  " " << i <<  " " << (positions[i][0]-radii[i]*double_radious[0]*1.889725989) <<  " " << radii[i] << endl;
            if(min_x > (positions[i][0]-double_radious[get_atom_type(i)]*1.889725989)){
                min_x=positions[i][0]-double_radious[get_atom_type(i)]*1.889725989;
                index1=i;
            }
            if(max_x < positions[i][0] + double_radious[get_atom_type(i)]*1.889725989){
                max_x=fabs(positions[i][0] + double_radious[get_atom_type(i)]*1.889725989);
                index2=i;
            }
            if(min_y > positions[i][1]- double_radious[get_atom_type(i)]*1.889725989){
                min_y=positions[i][1]- double_radious[get_atom_type(i)]*1.889725989;
                index3=i;
            }
            if(max_y < positions[i][1] + double_radious[get_atom_type(i)]*1.889725989){
                max_y=fabs(positions[i][1] + double_radious[get_atom_type(i)]*1.889725989);
                index4=i;
            }
            if(min_z > positions[i][2] - double_radious[get_atom_type(i)]*1.889725989){
                min_z=positions[i][2] - double_radious[get_atom_type(i)]*1.889725989;
                index5=i;
            }
            if(max_z < positions[i][2] + double_radious[get_atom_type(i)]*1.889725989){
                max_z=fabs(positions[i][2] + double_radious[get_atom_type(i)]*1.889725989);
                index6=i;
            }
            if (double_radious.size()<=get_atom_type(i)){
                Verbose::all() << "Atom type " << i <<"\t" <<get_atom_type(i)  << " but Grid_Atom radius size is " << double_radious.size() <<std::endl;
                throw std::range_error("Atom type " + String_Util::to_string(i) + "\t" + String_Util::to_string(get_atom_type(i)) + " but Grid_Atom radius size is " + String_Util::to_string((int)double_radious.size()));
            }
        }
        cell[0]=max(fabs(min_x), max_x) + 0.01;
        cell[1]=max(fabs(min_y), max_y) + 0.01;
        cell[2]=max(fabs(min_z), max_z) + 0.01;
        Verbose::set_numformat(Verbose::Pos);
        Verbose::single(Verbose::Detail) << "x : " << min_x << " " << max_x << std::endl;
        Verbose::single(Verbose::Detail) << "y : " << min_y << " " << max_y << std::endl;
        Verbose::single(Verbose::Detail) << "z : " << min_z << " " << max_z << std::endl;

    }
    return;
}
void Atoms::reset_connection_table(){
    this->connection_table = vector< vector<int> > (atom_list.size());
    for(int i = 0; i < atom_list.size(); i++){
        connection_table[i] = vector<int> (atom_list.size(), 0.0);
    }
}
