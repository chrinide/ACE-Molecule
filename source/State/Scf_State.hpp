#pragma once
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
//#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"
//#include "AnasaziTypes.hpp"
//#include "Epetra_Map.h"
//#include "../Io/Atoms.hpp"

#include "State.hpp"

/**
 * @brief This class is inherited from State to store detailed potential/energy breakdown during SCF run.
 *        See State for details.
 **/
class Scf_State: public State{
    public:        
        Scf_State();
        Scf_State(const Scf_State& state);
        Scf_State(const State& state);
        Scf_State& operator=(const Scf_State& state);
        Scf_State& operator=(const State& state);

        //Teuchos::RCP<Epetra_Vector> hartree_potential;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > x_potential;
        Teuchos::Array< Teuchos::RCP<Epetra_Vector> > c_potential;
//        Array< RCP<Epetra_Vector> > residue;
        
        double hartree_energy;
        double kinetic_energy;
        double external_energy;
        double x_energy;
        double c_energy;

};
