#include "Dynamic_Accessor.hpp"
#include <map>
#include <utility>

/*
if you want to add value names, which are  dynamically accesssed,
add key to point the value name into valName_typeNames_map  ex) Orbitals denot orbitals->orbitals
and also possible tpyes. If more than two types are possible, you can just add them.
The types should have inheritance relation in order to allow more than two types to get value.
ex) Epetra_Vector inherits Epetra_MultiVector thus, density could be obtained by both Epetra_Vector and Epetra_MultiVector since original density type is Epetra_Vector
and finally you should change init function. add the cases in the function.

*/
namespace Dynamic_Accessor{
    //do not need to call externally below functions.
    int init_double(RCP<State> state);
    int init_multivector(RCP<State> state);
    int init_vector(RCP<State> state);
    int check_type(RCP<State> state, string value_name, string type_name);

    map<string, double*> double_map;
    map<string, Array<RCP<Epetra_MultiVector> > > array_multivector_map;
    map<string, Array<RCP<Epetra_Vector> > >  array_vector_map;
    typedef map<string, vector<string> >  Val_Type_Map;
    Val_Type_Map valName_typeNames_map = {
        std::make_pair("orbitals",vector<string> {"MultiVector" } ),
        std::make_pair("Orbitals",vector<string> {"MultiVector" } ),
        std::make_pair("density",vector<string> {"MultiVector","Vector"} ),
        std::make_pair("Density",vector<string> {"MultiVector","Vector"} ),
        std::make_pair("Total_Energy",vector<string>{"Double"}),
        std::make_pair("total_energy",vector<string>{"Double"}),
        std::make_pair("Nuclear_Nuclear_Repulsion",vector<string>{"Double"}),
        std::make_pair("nuclear_nuclear_repulsion",vector<string>{"Double"})
    };
}
int Dynamic_Accessor::get(RCP<State> state,string s ,double& output){
    check_type(state,s,"Double");
    output = *double_map[s];
    return 0;
}

int Dynamic_Accessor::get(RCP<State> state, string s, int i_spin, RCP<Epetra_MultiVector>& output){
    check_type(state,s,"MultiVector");
    Array< RCP<Epetra_MultiVector> > original_val = array_multivector_map[s];
    int size = original_val.size();
    if(i_spin< size ){
        output = rcp(new Epetra_MultiVector( *original_val[i_spin]));
    }
    else{
        Verbose::all() << "Dynamic_Accessor:: Error" << s<< " is too short" <<std::endl;
        Verbose::all() << "Dynamic_Accessor:: size: "<< size <<std::endl;
        Verbose::all() << "Dynamic_Accessor:: i_spin: "<< i_spin <<std::endl;
        exit(-1);
    }

    return 0;
}
int Dynamic_Accessor::get(RCP<State> state,string s, int i_spin, RCP<Epetra_Vector>& output){
    check_type(state,s,"Vector");
    Array< RCP<Epetra_Vector> > original_val = array_vector_map[s];
    int size = original_val.size();
    if(i_spin< size ){
        output = rcp( new Epetra_Vector( *original_val[i_spin]));
    }
    else{
        Verbose::all() << "Dynamic_Accessor:: Error" << s << " is too short" << std::endl;
        Verbose::all() << "Dynamic_Accessor:: size: " << size << std::endl;
        Verbose::all() << "Dynamic_Accessor:: i_spin: " << i_spin << std::endl;
        exit(-1);
    }
    return 0;
}
int Dynamic_Accessor::set(RCP<State> state,string s ,double input){
    check_type(state,s,"Double");
    *double_map[s]=input;
    return 0;
}
int Dynamic_Accessor::set(RCP<State> state,string s, int i_spin, RCP<Epetra_MultiVector> input){
    check_type(state,s,"MultiVector");
    Array< RCP<Epetra_MultiVector> > original_val = array_multivector_map[s];
    int size = original_val.size();
    if(i_spin < size){
        original_val[i_spin] = rcp(new Epetra_MultiVector(*input));
    }
    else if (size ==1 and i_spin ==1){
        original_val.append( rcp ( new Epetra_MultiVector(*input)  ) );
    }
    else{
        Verbose::all()<< "Dynamic_Accessor:: Error "<<std::endl;
        exit(-1);
    }

    return 0;
}
int Dynamic_Accessor::set(RCP<State> state,string s, int i_spin,RCP<Epetra_Vector> input){
    check_type(state,s,"Vector");
    Array< RCP<Epetra_MultiVector> > original_val = array_multivector_map[s];
    int size = original_val.size();
    if(i_spin < size){
        original_val[i_spin] =  rcp ( new Epetra_Vector(*input)  );
    }
    else if (size ==1 and i_spin ==1){
        original_val.append(rcp ( new Epetra_Vector(*input)  ) );
    }
    else{
        Verbose::all()<< "Dynamic_Accessor:: Error "<<std::endl;
        exit(-1);
    }

    return 0;
}

int Dynamic_Accessor::check_type(RCP<State> state, string value_name, string type_name){
    try{
        if (std::find(valName_typeNames_map[value_name].begin(),valName_typeNames_map[value_name].end(),type_name)==valName_typeNames_map[value_name].end() ){
            // given type is not matched with pre-defined type.
            Verbose::all() << "Dynamic_Accessor::value name is correct but type is wrong" << std::endl;
            Verbose::all() << "Dynamic_Accessor::given value name: " << value_name << std::endl;
            Verbose::all() << "Dynamic_Accessor::given type name: " << type_name << std::endl;
            exit(-1);
        }
        else{

            if(type_name =="MultiVector"){
                return init_multivector(state);
            }
            else if(type_name=="Vector"){
                return init_vector(state);
            }
            else if (type_name=="Double"){
                return init_double(state);
            }
        }

    }
    catch(std::exception& e ){
        // value name is not in the list.
        Verbose::all() << "Dynamic_Accessor::Wrong value_name!!" <<std::endl;
        Verbose::all() << "Dynamic_Accessor::given value name: " << value_name <<std::endl;
        exit(-1);
    }
    return 0;
}
int Dynamic_Accessor::init_double(RCP<State> state){
    double_map["total_energy"] = &state->total_energy;
    double_map["Total_Energy"] = &state->total_energy;
    double_map["nuclear_nuclear_repulsion"]= &state->nuclear_nuclear_repulsion;
    double_map["Nuclear_Nuclear_Repulsion"]= &state->nuclear_nuclear_repulsion;
    return 0;
}

int Dynamic_Accessor::init_multivector(RCP<State> state){
    array_multivector_map["orbitals"] = state->orbitals;
    array_multivector_map["Orbitals"] = state->orbitals;
    array_multivector_map["density"] = state->density;
    array_multivector_map["Density"] = state->density;

    return 0;
}

int Dynamic_Accessor::init_vector(RCP<State> state){
    array_vector_map["density"] = state->density;
    array_vector_map["Density"] = state->density;

    return 0;
}
