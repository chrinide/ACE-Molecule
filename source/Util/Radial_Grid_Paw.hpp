#pragma once
#include <vector>
#include "Epetra_Vector.h"
#include "Teuchos_RCP.hpp"

#include "../Basis/Basis.hpp"

/**
 * @brief Utility for atom-centered radial grid.
 * @details Mainly used for manipulating data on atom-centered radial grid for PAW dataset.
 * @author Sungwoo Kang
 * @date 2015 
 * @version 0.0.2
 * @note Changed back to Spline interpolation because it gives better energy.
 * @todo Should check spline interpolation symmetry breaking, remove unused codes, 
 *       and remove cutoffs if possible.
 **/
namespace Radial_Grid{
namespace Paw{
    /**
     * @brief Add data1 and data2
     * @param data1 Value to add 1.
     * @param data2 Value to add 2.
     * @return data1+data2. Size follow shortest between data1 and data2.
     **/
    std::vector<double> sum( std::vector<double> data1, std::vector<double> data2 );

    /**
     * @brief Calculate radial integration \f$ \int r^2 dr [data1]*[data2] \f$ using trapezoidal rule.
     * @param data1 Value to integrate 1.
     * @param data2 Value to integrate 2.
     * @param grid_val Radial mesh to calculate on.
     * @param cutoff Upper bound for calculation. Upper bound of grid_val if less then zero.
     * @return \f$ \int r^2 dr [data1]*[data2] \f$.
     **/
    double radial_integrate( 
        std::vector<double> data1, std::vector<double> data2, 
        std::vector<double> grid_val, double cutoff = -1.0 
    );

    /**
     * @brief Calculate \f$ \int dr \f$ [data1]*[data2] using trapezoidal rule.
     * @param data1 Value to integrate 1.
     * @param data2 Value to integrate 2.
     * @param grid_val Radial mesh to calculate on.
     * @param cutoff Upper bound for calculation. Upper bound of grid_val if less then zero.
     * @return \f$ \int dr [data1]*[data2] \f$.
     **/
    double linear_integrate( 
        std::vector<double> data1, std::vector<double> data2, 
        std::vector<double> grid_val, double cutoff = -1.0 
    );

    /**
     * @brief Calculate derivative using backward finite difference, accuracy 1.
     * @param data1: Value to differentiate.
     * @param grid_val: Radial mesh to calculate on.
     * @return \f$ \frac{data[i]-data[i-1]}{grid_val[i]-grid_val[i-1]} \f$. Zero for first element.
     **/
    std::vector<double> linear_derivative( std::vector<double> data1, std::vector<double> grid_val );

    /**
     * @brief Calculate radial integration \f$ \int r^2 dr [data1]*[data2] \f$ using analytic grid derivative. (dr = [grid_derivative_val])
     * @param data1 Value to integrate 1.
     * @param data2 Value to integrate 2.
     * @param grid_val Radial mesh to calculate on.
     * @param grid_derivative_val Analytic derivative of radial mesh.
     * @param cutoff Upper bound for calculation. Upper bound of grid_val if less then zero.
     * @return \f$ \int r^2 dr [data1]*[data2] \f$.
     **/
    double radial_integrate( 
        std::vector<double> data1, std::vector<double> data2, 
        std::vector<double> grid_val, std::vector<double> grid_derivative_val, 
        double cutoff = -1.0 
    );

    /**
     * @brief Calculate \f$ \int dr [data1]*[data2] \f$ using analytic grid derivative. (dr = [grid_derivative_val])
     * @param data1 Value to integrate 1.
     * @param data2 Value to integrate 2.
     * @param grid_val Radial mesh to calculate on.
     * @param grid_derivative_val Analytic derivative of radial mesh.
     * @param cutoff Upper bound for calculation. Upper bound of grid_val if less then zero.
     * @return \f$ \int dr [data1]*[data2] \f$.
     **/
    double linear_integrate( 
        std::vector<double> data1, std::vector<double> data2, 
        std::vector<double> grid_val, std::vector<double> grid_derivative_val, 
        double cutoff = -1.0 
    );

    /**
     * @brief Calculate linear derivative using analytic grid derivative.
     * @details Similar to central difference coefficient. Forward/Backward difference coefficient-like calculations for first and last element.
     * @param data Value to differentiate.
     * @param grid_val Radial mesh to calculate on.
     * @param grid_derivative Analytic derivative of radial mesh.
     * @return \f$ \frac{data[i+1]-data[i-1]}{2 \times dr} \f$.
     **/
    std::vector<double> linear_derivative( std::vector<double> data, std::vector<double> grid_val, std::vector<double> grid_derivative );

    //void get_gradient_from_radial_grid( int l, int m, vector<double> src, vector<double> src_grid, double * src_center, RCP<const Basis> dest_mesh, Teuchos::Array< RCP<Epetra_Vector> > &retval, double rc = -1.0 );
    
    /**
     * @brief Calculate gradient of properties on radial grid and put it onto Basis.
     * @details Calculate gradient of [src]*Ylm. Put all value larger than RGD2GD_CUTOFF to all cores.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param m Magnetic moment of spherical harmonics function that is to be multiplied.
     * @param src Value to differentiate, after multiplying spherical harmonics.
     * @param src_grid Source radial mesh.
     * @param src_center Center of src_grid will be positioned here on dest_mesh.
     * @param dest_mesh Destination mesh.
     * @param retval Resulting gradient value. MultiVector index means differentiating positions (x:0, y:1, z:2).
     * @param rc Radial mesh cutoff. Upper bound of src_grid if less then zero.
     * @todo Change retval to Array< RCP<Epetra_Vector> >.
     **/
    void get_gradient_from_radial_grid( 
        int l, int m, std::vector<double> src, std::vector<double> src_grid, std::array<double,3> src_center, 
        Teuchos::RCP<const Basis> dest_mesh, Teuchos::RCP<Epetra_MultiVector> &retval, double rc = -1.0 
    );

    /**
     * @brief Interpolate properties on radial grid onto Basis. 
     * @details Interpolate [src]*Ylm to Basis. Put all values larger than RGD2GD_CUTOFF to all cores.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param m Magnetic moment of spherical harmonics function that is to be multiplied.
     * @param src Value to interpolate, after multiplying spherical harmonics.
     * @param src_grid Source radial mesh.
     * @param src_center Center of src_grid will be positioned here on dest_mesh.
     * @param dest_mesh Destination mesh.
     * @param dest_val Interpolated value.
     * @param dest_ind Index correspond to the interpolated value.
     * @param rc Radial mesh cutoff. Upper bound of src_grid if less then zero.
     * @note Is this function used?
     * @todo Parallelize this.
     **/
    void Interpolate_from_radial_grid( 
        int l, int m, std::vector<double> src, std::vector<double> src_grid, std::array<double,3> src_center, 
        Teuchos::RCP<const Basis> dest_mesh, std::vector<double> &dest_val, std::vector<int> &dest_ind, double rc = -1.0 
    );

    /**
     * @brief Interpolate properties on radial grid onto Basis and change it to basis coefficient. 
     * @details Interpolate [src]*Ylm to Basis and change it to basis coefficient. Put all values larger than RGD2GD_CUTOFF to all cores.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param m Magnetic moment of spherical harmonics function that is to be multiplied.
     * @param src Value to interpolate, after multiplying spherical harmonics.
     * @param src_grid Source radial mesh.
     * @param src_center Center of src_grid will be positioned here on dest_mesh.
     * @param dest_mesh Destination mesh.
     * @param dest_val Interpolated value.
     * @param dest_ind Index correspond to the interpolated value.
     * @param rc Radial mesh cutoff. Upper bound of src_grid if less then zero.
     * @note Is this function used? 
     *       Repeating this code is faster (one less loop), but bad for maintanence.
     *       What will be better?
     * @todo Parallelize this.
     **/
    void get_basis_coeff_from_radial_grid( 
        int l, int m, std::vector<double> src, std::vector<double> src_grid, std::array<double,3> src_center, 
        Teuchos::RCP<const Basis> dest_mesh, std::vector<double> &dest_val, std::vector<int> &dest_ind, double rc = -1.0 
    );

    /**
     * @brief Interpolate properties on radial grid onto Basis.
     * @details Interpolate [src]*Ylm to Basis. Put all values larger than RGD2GD_CUTOFF, with parallelized.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param m Magnetic moment of spherical harmonics function that is to be multiplied.
     * @param src Value to interpolate, after multiplying spherical harmonics.
     * @param src_grid Source radial mesh.
     * @param src_center Center of src_grid will be positioned here on dest_mesh.
     * @param dest_mesh Destination mesh.
     * @param retval Resulting interpolated value. Input and output. Need to be initialized.
     * @param rc Radial mesh cutoff. Upper bound of src_grid if less then zero.
     **/
    int Interpolate_from_radial_grid( 
        int l, int m, std::vector<double> src, std::vector<double> src_grid, std::array<double,3> src_center, 
        Teuchos::RCP<const Basis> dest_mesh, Teuchos::RCP<Epetra_Vector> retval, double rc = -1.0 
    );

    /**
     * @brief Interpolate properties on radial grid onto Basis and change it to basis coefficient. 
     * @details Interpolate [src]*Ylm to Basis and change it to basis coefficient. Put all values larger than RGD2GD_CUTOFF.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param m Magnetic moment of spherical harmonics function that is to be multiplied.
     * @param src Value to interpolate, after multiplying spherical harmonics.
     * @param src_grid Source radial mesh.
     * @param src_center Center of src_grid will be positioned here on dest_mesh.
     * @param dest_mesh Destination mesh.
     * @param retval Resulting coefficient of interpolated value. Input and output. Need to be initialized.
     * @param rc Radial mesh cutoff. Upper bound of src_grid if less then zero.
     * @note Repeating this code is faster (one less loop), but bad for maintanence.
     *       What will be better?
     **/
    int get_basis_coeff_from_radial_grid( 
        int l, int m, std::vector<double> src, std::vector<double> src_grid, std::array<double,3> src_center, 
        Teuchos::RCP<const Basis> dest_mesh, Teuchos::RCP<Epetra_Vector> retval, double rc = -1.0 
    );

    /**
     * @brief Change radial grid to other grid.
     * @param src Value to interpolate.
     * @param src_grid Source radial mesh.
     * @param dest_grid Destination radial mesh.
     * @return Interpolated value.
     **/
    std::vector<double> change_radial_grid( std::vector<double> src, std::vector<double> src_grid, std::vector<double> dest_grid );

    // Utilities, probabily this will be used in Paw_Calc only.
    //std::vector<double> calculate_Hartree_potential_r( std::vector<double> radial_func, int l, std::vector<double> grid );
    // Calculates radial part only.
    // [Full Input Func.] = [Radial Part (input of this)] * [Angular part (Spherical Harmonics)].
    // Return value should be multiplied by Yml, which is same with spherical harmonics of input function.

    // Analytic versions.
    //std::vector<double> calculate_Hartree_potential_r( std::vector<double> radial_func, int l, std::vector<double> grid, std::vector<double> grid_derivative, double cutoff = -1.0 );
    /**
     * @brief Calculate Hartree potential at radial grid.
     * @details See arXiv:0910.1921v2 eq.(22) (section 2.2)
     * @param radial_func Value to compute Hartree potential.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param grid Radial mesh that radial_func represented on.
     * @param grid_derivative Analytic derivative of radial mesh.
     * @return Hartree potential.
     **/
    std::vector<double> calculate_Hartree_potential_r( 
        std::vector<double> radial_func, int l, std::vector<double> grid, std::vector<double> grid_derivative 
    );

    /**
     * @brief Calculate Hartree potential at radial grid.
     * @details See arXiv:0910.1921v2 eq.(22) (section 2.2)
     * @param radial_func Value to compute Hartree potential.
     * @param r Radial point to get result.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param grid Radial mesh that radial_func represented on.
     * @param grid_derivative Analytic derivative of radial mesh.
     * @return Hartree potential.
     **/
    double calculate_Hartree_potential_r( 
        std::vector<double> radial_func, double r, int l, std::vector<double> grid, std::vector<double> grid_derivative 
    );

    /**
     * @brief Calculate coulomb integral of two values.
     * @details \f$ \int r_1^2 dr_1 \int r_2^2 dr_2 \frac{f_1(r_1)*f_2(r_2)}{|r_1-r_2|} \f$.
     * @param f1 Value 1 on radial mesh (grid).
     * @param f2 Value 2 on radial mesh (grid).
     * @param l1 Angular momentum of Value 1.
     * @param l2 Angular momentum of Value 2.
     * @param m1 Magnetic momentum of Value 1.
     * @param m2 Magnetic momentum of Value 2.
     * @param grid Radial mesh that supports f1 and f2.
     * @param grid_derivative Analytic derivative of [grid].
     * @param cutoff Radial mesh cutoff. Upper bound of grid if less then zero.
     * @return Coulomb integral between f1 and f2.
     **/
    double calculate_coulomb_integral( 
        std::vector<double> f1, std::vector<double> f2, int l1, int l2, int m1, int m2, 
        std::vector<double> grid, std::vector<double> grid_derivative, double cutoff = -1.0 
    );
    //double calculate_quadruple_coulomb_integral( std::vector<double> f1, std::vector<double> f2, std::vector<double> f3, std::vector<double> f4, int l1, int l2, int l3, int l4, int m1, int m2, int m3, int m4, std::vector<double> grid, std::vector<double> grid_derivative, double cutoff = -1.0 );
}
}

