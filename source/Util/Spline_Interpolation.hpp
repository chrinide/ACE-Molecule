#pragma once
#include <vector>

namespace Interpolation{
namespace Spline{
    /**
     * @brief Produce y2a value for splint.
     * @param x X-value.
     * @param y Y-value.
     * @param n Size of x and y.
     * @param yp1 Gradient at index = 0.
     * @param ypn Gradient at index = n.
     **/
    std::vector<double> spline(std::vector<double> x, std::vector<double> y, int n, double yp1, double ypn);
    /**
     * @brief Interpolate.
     * @param xa X-value.
     * @param ya Y-value.
     * @param y2a Return value of spline.
     * @param n Size of xa and ya.
     * @param x Desired point.
     **/
    double splint(std::vector<double> xa, std::vector<double> ya, std::vector<double> y2a, int n, double x);
};
};
