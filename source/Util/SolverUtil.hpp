#pragma once
#include "Teuchos_RCP.hpp"
#include "Teuchos_SerialDenseMatrix.hpp"

#include "AnasaziConfigDefs.hpp"
#include "AnasaziMultiVecTraits.hpp"
#include "AnasaziOperatorTraits.hpp"
#include "Teuchos_ScalarTraits.hpp"

#include "AnasaziOutputManager.hpp"
#include "Teuchos_BLAS.hpp"
#include "Teuchos_LAPACK.hpp"

class MAGMA {
    public:
        typedef typename Teuchos::ScalarTraits<double>::magnitudeType MagnitudeType;
        typedef typename Teuchos::ScalarTraits<double>  SCT;
        static int directSolver(int size, const Teuchos::SerialDenseMatrix<int,double> &KK,
                Teuchos::RCP<const Teuchos::SerialDenseMatrix<int,double> > MM,
                Teuchos::SerialDenseMatrix<int,double> &EV,
                std::vector< typename Teuchos::ScalarTraits<double>::magnitudeType > &theta,
                int &nev, int esType = 0);        
    private:
};

class SCALAPACK {
    public:
        typedef typename Teuchos::ScalarTraits<double>::magnitudeType MagnitudeType;
        typedef typename Teuchos::ScalarTraits<double>  SCT;
        static int directSolver(int size, const Teuchos::SerialDenseMatrix<int,double> &KK,
                Teuchos::RCP<const Teuchos::SerialDenseMatrix<int,double> > MM,
                Teuchos::SerialDenseMatrix<int,double> &EV,
                std::vector< typename Teuchos::ScalarTraits<double>::magnitudeType > &theta,
                int &nev, int esType = 0);        
    private:
        /*
        int iam, nprocs;
        int myrank_mpi, nprocs_mpi;
        int ictxt, nprow, npcol, myrow, mycol;
        int np, nq, nb, n;
        int mpA, nqA;
        int info, itemp, seed, lwork, min_mn;
        int descA[9], descZ[9], descB[9];
        double *A, *B, *Z, *work, *W;
        int izero=0,ione=1;
        double mone=(-1.0e0),pone=(1.0e0),dzero=(0.0e0);
        char jobz, uplo;
        */
        //static void initializer(int n, int& iam, int& nprocs, int& myrank_mpi, int& nprocs_mpi, int& ictxt, int& nprow, int& npcol, int& myrow, int& mycol, int& np, int& nq, int& nb, int& mpA, int& nqA, int* descA, int* descZ, int* descB);
        static void scatter(double* local, double* global, int n, int nb, int nprow, int npcol, int myrow, int mycol, int nprocs, int mpA, int nqA);
        static void make_upper(double* local, int n, int nb, int nprow, int npcol, int myrow, int mycol, int nprocs, int mpA, int nqA);
        static void gather(double* local, double* global, int n, int nb, int nprow, int npcol, int myrow, int mycol, int nprocs, int mpA, int nqA, int ictxt);
        void divisor(int* nprow, int* npcol);
};

