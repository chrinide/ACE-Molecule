#pragma once
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"

#include "Set_Default_Values.hpp"

namespace Default_Values{
    class Set_Scf :public Set_Default_Values{
        public:
            Set_Scf();
            int set_values(Teuchos::RCP<Teuchos::ParameterList>& total_parameters);

            //~Set_Default_Values();
            //void check_given_parameters();
    };
}
