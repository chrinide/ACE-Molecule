#include "Set_Basic_Information.hpp"
#include "Teuchos_StandardParameterEntryValidators.hpp"
#include "../Read_Cube.hpp"
#include "../Verbose.hpp"

using std::string;
using std::array;
using Teuchos::RCP;
using Teuchos::ParameterList;

Default_Values::Set_Basic_Information::Set_Basic_Information(){
}

int Default_Values::Set_Basic_Information::set_parameters(RCP<ParameterList>& total_parameters){
    RCP<ParameterList> parameters;
    if(total_parameters -> name() == "BasicInformation"){
        parameters = total_parameters;
    /*
    // Backward compatability.
    } else if(total_parameters -> isSublist("BasicInformation")){
        parameters = Teuchos::sublist(total_parameters, "BasicInformation");
    */
    } else {
        return 1;
    }
    /////////////  check Use DefaultValues ///////////////
    use_default=parameters->get<bool>("UseDefaultValues", true);
    if(use_default == false){
        return 1;
    }

    /////////////  Calculation Mode  ///////////
#ifndef EDISON
    parameters->get<string>("Mode","Auto");
#endif
    /////////////  Basis & Grid      ///////////
    parameters->get<string>("Basis","Sinc");
    parameters->get<string>("Grid","Atoms");

    /////////////  Points or Scaling ///////////
    parameters->get<string>("Type","Scaling");

    /////////////  Type Cube        ////////////////
    if(parameters -> get<string>("Type") == "Cube"){
        parameters -> set<int>("AllowOddPoints", 1);
        array<int, 3> points;
        array<double, 3> scaling;
        array< array<double, 3>, 3> uvec;
        Read::Cube::read_cube_mesh(parameters -> get<string>("BasisCubeFilename"), points, uvec);
        for(int i = 0; i < 3; ++i){
            bool empty = true;
            for(int j = 0; j < 3; ++j){
                if(std::abs(uvec[i][j]) > 0){
                    if(empty){
                        scaling[i] = uvec[i][j];
                        empty = false;
                    } else {
                        string errmsg = "Unit vector from BasisCubeFilename should be parallel to x, y, z directions!";
                        std::cout << errmsg << std::endl;
                        throw std::invalid_argument(errmsg);
                    }
                }
            }
        }
        for(int i = 0; i < 3; ++i){
            string pos = (i == 0)? "X": (i == 1)? "Y": "Z";
            double cell = points[i]*scaling[i]/2. + 0.1*scaling[i];
            parameters -> set((string("CellDimension")+pos).c_str(), cell);
            parameters -> set((string("Point")+pos).c_str(), points[i]);
            parameters -> set((string("Scaling")+pos).c_str(), scaling[i]);
        }
        if(parameters->get<string>("Grid") != "Basic"){
            string warmsg = "Grid should be basic for Type == Cube!";
            std::cout << warmsg << std::endl;
            std::cerr << warmsg << std::endl;
            parameters -> set<string>("Grid", "Basic");
        }
    }

    bool is_even_only = true;
    if( parameters -> isParameter("AllowOddPoints") ){
        if( parameters -> get<int>("AllowOddPoints") > 0 ){
            is_even_only = false;
        }
    }

    /////////////  Cell Dimension    ///////////
    if(parameters->get<string>("Grid")!="Atoms"){
        const std::string errmsg_nocell = "Given Input does not contain 'Cell' value";
        if(!parameters->isParameter("CellDimensionX")){
            TEUCHOS_TEST_FOR_EXCEPTION(
            !parameters->isParameter("Cell"),
            std::invalid_argument, errmsg_nocell);

            parameters->set("CellDimensionX",parameters->get<double>("Cell"));
        }
        if(!parameters->isParameter("CellDimensionY")){
            TEUCHOS_TEST_FOR_EXCEPTION(
            !parameters->isParameter("Cell"),
            std::invalid_argument, errmsg_nocell);

            parameters->set("CellDimensionY",parameters->get<double>("Cell"));
        }
        if(!parameters->isParameter("CellDimensionZ")){
            TEUCHOS_TEST_FOR_EXCEPTION(
            !parameters->isParameter("Cell"),
            std::invalid_argument, errmsg_nocell);

            parameters->set("CellDimensionZ",parameters->get<double>("Cell"));
        }

        if(parameters->get<string>("Type")=="Points"){
            const std::string errmsg_nopts = "Given Input does not contain 'Points' value";
            double* scaling = new double[3];

            if(!parameters->isParameter("PointX")){
                TEUCHOS_TEST_FOR_EXCEPTION(
                !parameters->isParameter("Points"),
                std::invalid_argument, errmsg_nopts);

                parameters->set("PointX",parameters->get<int>("Points"));
            }
            if(parameters -> get<int>("PointX") < 2){
                scaling[0] = parameters -> get<double>("CellDimensionX");
            } else {
                scaling[0] = 2*parameters->get<double>("CellDimensionX")/(parameters->get<int>("PointX")-1);
            }

            if(!parameters->isParameter("PointY")){
                TEUCHOS_TEST_FOR_EXCEPTION(
                !parameters->isParameter("Points"),
                std::invalid_argument, errmsg_nopts);

                parameters->set("PointY",parameters->get<int>("Points"));
            }
            if(parameters -> get<int>("PointY") < 2){
                scaling[1] = parameters -> get<double>("CellDimensionY");
            } else {
                scaling[1] = 2*parameters->get<double>("CellDimensionY")/(parameters->get<int>("PointY")-1);
            }

            if(!parameters->isParameter("PointZ")){
                TEUCHOS_TEST_FOR_EXCEPTION(
                !parameters->isParameter("Points"),
                std::invalid_argument, errmsg_nopts);

                parameters->set("PointZ",parameters->get<int>("Points"));
            }
            if(parameters -> get<int>("PointZ") < 2){
                scaling[2] = parameters -> get<double>("CellDimensionZ");
            } else {
                scaling[2] = 2*parameters->get<double>("CellDimensionZ")/(parameters->get<int>("PointZ")-1);
            }

            parameters->set("ScalingX",scaling[0]);
            parameters->set("ScalingY",scaling[1]);
            parameters->set("ScalingZ",scaling[2]);

            delete[] scaling;
        }
    }
    else{
        TEUCHOS_TEST_FOR_EXCEPTION(
        parameters->get<string>("Type")!="Scaling",
        std::invalid_argument,
        "Type should be Scaling when Grid_Atoms is applied ");
    }
    if (parameters->get<string>("Type")=="Scaling"){
        const std::string errmsg_noh = "Given Input does not contain 'Scaling' value";
        int* points = new int[3];
        double* scaling = new double[3];
        if(!parameters->isParameter("ScalingX")){
            TEUCHOS_TEST_FOR_EXCEPTION(
            !parameters->isParameter("Scaling"),
            std::invalid_argument, errmsg_noh);

            parameters->set("ScalingX",parameters->get<double>("Scaling"));
        }

        if(!parameters->isParameter("ScalingY")){
            TEUCHOS_TEST_FOR_EXCEPTION(
            !parameters->isParameter("Scaling"),
            std::invalid_argument, errmsg_noh);

            parameters->set("ScalingY",parameters->get<double>("Scaling"));
        }

        if(!parameters->isParameter("ScalingZ")){
            TEUCHOS_TEST_FOR_EXCEPTION(
            !parameters->isParameter("Scaling"),
            std::invalid_argument, errmsg_noh);

            parameters->set("ScalingZ",parameters->get<double>("Scaling"));
        }

        scaling[0]=parameters->get<double>("ScalingX");
        scaling[1]=parameters->get<double>("ScalingY");
        scaling[2]=parameters->get<double>("ScalingZ");

        if(parameters->get<string>("Grid")!="Atoms"){
            //points[0] = 2.0 * floor(parameters->get<double>("CellDimensionX")/(scaling[0])) + 1;
            points[0] = floor(2.0 * parameters->get<double>("CellDimensionX")/(scaling[0])) + 1;
            points[1] = floor(2.0*parameters->get<double>("CellDimensionY")/(scaling[1])) + 1;
            points[2] = floor(2.0*parameters->get<double>("CellDimensionZ")/(scaling[2])) + 1;

            if( is_even_only ){
                for(int i=0; i<3; i++){
                    if(points[0]%2==1)
                        points[0] = points[0]-1;
                    if(points[1]%2==1)
                        points[1] = points[1]-1;
                    if(points[2]%2==1)
                        points[2] = points[2]-1;
                }
            } else {
                Verbose::single(Verbose::Normal) << "Allowing odd grid points for non Grid_Atoms" << std::endl;
            }
            parameters->set<double>("CellDimensionX", 0.5*(points[0]-1)*scaling[0]);
            parameters->set<double>("CellDimensionY", 0.5*(points[1]-1)*scaling[1]);
            parameters->set<double>("CellDimensionZ", 0.5*(points[2]-1)*scaling[2]);
            //points[0]=floor(2.0*parameters->get<double>("CellDimensionX")/(scaling[0]))+1.0;

            //points[1] = 2.0 * floor(parameters->get<double>("CellDimensionY")/(scaling[1])) + 1;


            //points[1]=floor(2.0*parameters->get<double>("CellDimensionY")/(scaling[1]))+1.0;


            //points[2] = 2.0 * floor(parameters->get<double>("CellDimensionZ")/(scaling[2])) + 1;

            //points[2]=floor(2.0*parameters->get<double>("CellDimensionZ")/(scaling[2]))+1.0;

            parameters->set("PointX",points[0]);
            parameters->set("PointY",points[1]);
            parameters->set("PointZ",points[2]);
        }
        delete [] points;
        delete[] scaling;
    }

    ////////// Geometry File     /////////////////////////
#ifndef EDISON
    parameters->get<string>("GeometryFormat", "xyz");
    TEUCHOS_TEST_FOR_EXCEPTION(
    !parameters->isParameter("GeometryFilename"),
    std::invalid_argument,
    "Given Input does not contain 'GeometryFilename' value ");
#endif

    ////////// Pseudopotential   ///////////////////////
    int pp_type = parameters -> sublist("Pseudopotential").get<int>("Pseudopotential", 1);
    if(pp_type == 1){
        string type = parameters -> sublist("Pseudopotential").get<string>("Format", "hgh");
        if(type == "hgh"){
            string format = parameters -> sublist("Pseudopotential").get<string>("HghFormat", "Internal");
            (format == "Internal")? parameters -> sublist("Pseudopotential").get<string>("XCType", "LDA"): "";
        }
    } else if (pp_type == 3){
        parameters -> sublist("Pseudopotential").get<string>("Format", "xml");
    }

    //////////////// Int to double cast /////////////////////////////
    if(parameters -> isParameter("NumElectrons")){
        parameters -> set<double>("NumElectrons", Teuchos::getDoubleParameter(*parameters, "NumElectrons"));
    }
    if(parameters -> isParameter("Charge")){
        parameters -> set<double>("Charge", Teuchos::getDoubleParameter(*parameters, "Charge"));
    } else {
        if(!parameters -> isParameter("NumElectrons")){
            parameters -> get<double>("Charge", 0.0);
        }
    }
    if(parameters -> isParameter("SpinMultiplicity")){
        parameters -> set<double>("SpinMultiplicity", Teuchos::getDoubleParameter(*parameters, "SpinMultiplicity"));
    }

    /////////////////////////////////////////////////////////
    return 1+call_components(parameters);
}

/*
Set_Basic_Information* Default_Values::BasicInformation_Factory::create(){
    return new Set_Basic_Information();
}
*/
