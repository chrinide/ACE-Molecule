#include"Set_EDISON.hpp"
#include <stdexcept>
#include "../ParamList_Util.hpp"

using std::string;
using Teuchos::Array;
using Teuchos::RCP;
using Teuchos::rcp;
using Teuchos::ParameterList;

Default_Values::Set_EDISON::Set_EDISON(){
}


int Default_Values::Set_EDISON::set_parameters(Teuchos::Array< Teuchos::RCP<Teuchos::ParameterList> >& total_parameters){
    int retval = 0;
    string functional;
    RCP<ParameterList> parameters;
    for(int i = 0; i < total_parameters.size(); ++i){
        parameters = total_parameters[i];
        if(parameters -> name() == "Scf"){
            retval += this -> set_parameters_scf(parameters);
            functional = parameters -> get<string>("Functional_Name");
        /*
        } else if(total_parameters -> isSublist("Scf")){
            retval += this -> set_parameters_scf(Teuchos::sublist(parameters, "Scf"));
            functional = parameters.sublist("Scf").get<string>("Functional_Name");
        */
        }
    }
    RCP<ParameterList> parameters_basic = ParamList_Util::get_subparameter(total_parameters, "BasicInformation");
    if(parameters_basic -> name() == "BasicInformation"){
        retval += this -> set_parameters_basic(parameters_basic, functional);
    }

    for(int i = 0; i < total_parameters.size(); ++i){
        parameters = total_parameters[i];
        if(parameters -> name() == "Guess"){
            retval += this -> set_parameters_guess(parameters, Teuchos::sublist(parameters_basic, "Pseudopotential"));
        /*
        } else if(total_parameters -> isSublist("Guess")){
            return this -> set_parameters_guess(Teuchos::sublist(parameters, "Guess"));
        */
        }
        if(parameters -> name() == "TDDFT"){
            retval += this -> set_parameters_tddft(parameters);
        /*
        } else if(total_parameters -> isSublist("TDDFT")){
            retval += this -> set_parameters_tddft(Teuchos::sublist(parameters, "TDDFT"));
        */
        }
    }
    if(ParamList_Util::get_subparameter(total_parameters, "Guess") == Teuchos::null){
        total_parameters.append(rcp( new ParameterList("Guess")));
        retval += this -> set_parameters_guess(total_parameters[total_parameters.size()-1], Teuchos::sublist(parameters_basic, "Pseudopotential"));
    }
    return retval;
}

int Default_Values::Set_EDISON::set_parameters(Teuchos::RCP<Teuchos::ParameterList>& parameters){
    throw std::logic_error("Set_EDISON::set_parameters should not be called!");
}

int Default_Values::Set_EDISON::set_parameters_basic(Teuchos::RCP<Teuchos::ParameterList>& parameters, std::string functional){
    /////////////  Default Setting Suggestion    ///////////
    parameters->get<string>("Mode", "Sp");
    parameters->get<string>("Type", "Scaling");
    parameters->get<double>("Scaling", 0.3);
    parameters->get<string>("Grid", "Atoms");
    parameters->get< Array<string> >("Radius", Array<string>(1, "1.3"));
    parameters->get<string>("KineticMatrix", "Finite_Difference");
    parameters->get<int>("DerivativesOrder", 5);


    /////////////  BasicInformation   ///////////
    parameters->set("Geometry_Format", "xyz");

    /////////////  Pseudopotential      ///////////
    parameters->sublist("Pseudopotential").get<int>("Pseudopotential",1);
    if(parameters->sublist("Pseudopotential").get<int>("Pseudopotential") == 1){
        parameters->sublist("Pseudopotential").get<string>("Format","upf");
        parameters->sublist("Pseudopotential").get<string>("PSFilePath","libraries/PSEUDOPOTENTIALS_NC");
        if(!parameters->sublist("Pseudopotential").isParameter("PSFileSuffix")){
            string functional = parameters -> sublist("Scf").sublist("ExchangeCorrelation").get<string>("Functional_Name");
            if( functional == "PZ" ){
                parameters->sublist("Pseudopotential").get<string>("PSFileSuffix",".pz-n-nc.UPF");
            } else if( functional == "PBE" || functional == "EXX" ){
                parameters->sublist("Pseudopotential").get<string>("PSFileSuffix",".pbe-n-nc.UPF");
            }
        }
    }
    else if(parameters->sublist("Pseudopotential").get<int>("Pseudopotential") == 3){
        parameters->sublist("Pseudopotential").get<string>("Format","xml");
        parameters->sublist("Pseudopotential").get<string>("PSFilePath","libraries/PAW");
        if(!parameters->sublist("Pseudopotential").isParameter("PSFileSuffix")){
            if( functional == "PW" ){
                parameters->sublist("Pseudopotential").get<string>("PSFileSuffix",".LDA");
            } else if( functional == "PBE" ){
                parameters->sublist("Pseudopotential").get<string>("PSFileSuffix",".PBE");
            }
        }
    }
    return 1+call_components(parameters);
}

int Default_Values::Set_EDISON::set_parameters_guess(Teuchos::RCP<Teuchos::ParameterList>& parameters, Teuchos::RCP<Teuchos::ParameterList> pp_param){
    /////////////  Guess    ///////////
    if(pp_param -> get<int>("Pseudopotential") == 1){
        parameters->get<int>("InitialGuess", 1);
    }
    else if(pp_param -> get<int>("Pseudopotential") == 3){
        parameters->get<int>("InitialGuess", 4);
    }
    parameters->get<string>("InitialFilePath",
            pp_param -> get<string>("PSFilePath"));
    parameters->get<string>("InitialFileSuffix",
            pp_param -> get<string>("PSFileSuffix"));
    if(parameters -> get<int>("InitialGuess") == 2){
        parameters->get<string>("EHT_Filename", "libraries/eht_params.dat");
    }
    if(parameters->isSublist("Output")){
        if(!parameters->sublist("Output").isParameter("Prefix")){
            parameters->sublist("Output").get<string>("Prefix", "result/");
        } else {
            parameters -> sublist("Output").get<string>("Prefix",
                    std::string("result/")+parameters -> sublist("Output").get<string>("Prefix"));
        }
    }
    return 1+call_components(parameters);
}

int Default_Values::Set_EDISON::set_parameters_scf(Teuchos::RCP<Teuchos::ParameterList>& parameters){
    /////////////  XC      ///////////
    if(!parameters->sublist("ExchangeCorrelation").isParameter("Functional_Name")){
        if(!parameters->sublist("ExchangeCorrelation").isParameter("XCFunctional")){
            TEUCHOS_TEST_FOR_EXCEPTION(
            !parameters->sublist("ExchangeCorrelation").isParameter("XFunctional"),
            std::invalid_argument,
            "Given Input does not contain XFunctional value ");
            TEUCHOS_TEST_FOR_EXCEPTION(
            !parameters->sublist("ExchangeCorrelation").isParameter("CFunctional"),
            std::invalid_argument,
            "Given Input does not contain CFunctional value ");
        }

        if( parameters -> sublist("ExchangeCorrelation").get< Array<string> >("XFunctional")[0] == "1"
            and parameters -> sublist("ExchangeCorrelation").get< Array<string> >("CFunctional")[0] == "9" ){
            parameters -> sublist("ExchangeCorrelation").set("Functional_Name", "PZ");
        }
        if( parameters -> sublist("ExchangeCorrelation").get< Array<string> >("XFunctional")[0] == "101"
            and parameters -> sublist("ExchangeCorrelation").get< Array<string> >("CFunctional")[0] == "130" ){
            parameters -> sublist("ExchangeCorrelation").set("Functional_Name", "PBE");
        }
    }

    //parameters->set("ConvergenceType", "Energy");
    parameters->get<string>("ConvergenceType", "Density");
    if(!parameters->isParameter("ConvergenceTolerance")){
        if(parameters->get<string>("ConvergenceType") == "Energy"){
            parameters->set("ConvergenceTolerance", 0.000001);
            if(!parameters -> isParameter("EnergyDecomposition")){
                parameters -> set("EnergyDecomposition", 1);
            }
        }
        else if(parameters->get<string>("ConvergenceType") == "Density"){
            parameters->set("ConvergenceTolerance", 0.001);
            if(!parameters -> isParameter("EnergyDecomposition")){
                parameters -> set("EnergyDecomposition", 0);
            }
        }
    }
    if(!parameters->sublist("Diagonalize").isParameter("Tolerance")){
        parameters->sublist("Diagonalize").set("Tolerance", 0.00000001);
    }

    if(!parameters->sublist("Output").isParameter("Prefix")){
        parameters->sublist("Output").get<string>("Prefix", "result/");
    } else {
        parameters -> sublist("Output").set("Prefix",
                std::string("result/")+parameters -> sublist("Output").get<string>("Prefix"));
    }
    parameters->sublist("Output").get<string>("Orbitals", "cube");
    //}
    parameters -> sublist("Mixing").get<string>("MixingType", "Potential");
    return 1+call_components(parameters);
}

int Default_Values::Set_EDISON::set_parameters_tddft(Teuchos::RCP<Teuchos::ParameterList>& parameters){
    /////////////  TDDFT    ///////////
    if(parameters->isSublist("Output")){
        if(!parameters->sublist("Output").isParameter("Prefix")){
            parameters->sublist("Output").get<string>("Prefix", "result/");
        } else {
            parameters -> sublist("Output").get<string>("Prefix",
                    std::string("result/")+parameters -> sublist("Output").get<string>("Prefix"));
        }
    }
    parameters -> get<string>("ExchangeKernel", "HF");

    /////////////////////////////////////////////////////////
    return 1+call_components(parameters);
}
