#pragma once
#include "Teuchos_ParameterList.hpp"
#include "Teuchos_RCP.hpp"

#include "Set_Default_Values.hpp"

namespace Default_Values{
    class Set_External_Field: public: Set_Default_Values{
        public:
            Set_External_Field(Teuchos::RCP<Set_Default_Values> set_default_values);
            int set_parameters(Teuchos::RCP<Teuchos::ParameterList>& total_parameters);
            //void check_given_parameters();

            Set_External_Field();
            //~Set_Default_Values();
    };
}
