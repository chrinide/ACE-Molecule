#include "Integration.hpp"

using Teuchos::RCP;
using Teuchos::rcp;

double Integration::integrate(RCP<const Basis> mesh, RCP<Epetra_MultiVector> vector1, bool is_value1, int index1, RCP<Epetra_MultiVector> vector2, bool is_value2, int index2){
    double return_val=0.0;
    RCP<Epetra_MultiVector> new_vector1 = rcp ( new Epetra_MultiVector(*vector1) );
    RCP<Epetra_MultiVector> new_vector2 = rcp ( new Epetra_MultiVector(*vector2) );
    
    if(is_value1==true)
        Value_Coef::Value_Coef(mesh, vector1, true, false, new_vector1);
    if(is_value2==true)
        Value_Coef::Value_Coef(mesh, vector2, true, false, new_vector2);

    new_vector1->operator()(index1)->Dot(*new_vector2->operator()(index2),&return_val);

    new_vector1 = Teuchos::null;
    new_vector2 = Teuchos::null;

    return return_val;
}

double Integration::integrate(RCP<const Basis> mesh, RCP<Epetra_Vector> vector1, bool is_value1, RCP<Epetra_Vector> vector2, bool is_value2){
    double return_val=0.0;
    if(vector1->GlobalLength()!=vector2->GlobalLength() ){
        throw (-1);
    }

    RCP<Epetra_Vector> new_vector1 = rcp ( new Epetra_Vector(*vector1) );
    RCP<Epetra_Vector> new_vector2 = rcp ( new Epetra_Vector(*vector2) );
    if(is_value1==true)
        Value_Coef::Value_Coef(mesh, vector1, true, false, new_vector1);
    if(is_value2==true)
        Value_Coef::Value_Coef(mesh, vector2, true, false, new_vector2);

    new_vector1->Dot(*new_vector2,&return_val);

    new_vector1=Teuchos::null;
    new_vector2=Teuchos::null;
    
    return return_val;
}

double Integration::integrate(RCP<const Basis> mesh, double* vector1, bool is_value1, double* vector2, bool is_value2){
    double return_val=0.0;
    int total_size = mesh->get_original_size();

    double* new_vector1 = new double[total_size];
    double* new_vector2 = new double[total_size];
    memcpy(new_vector1, vector1, sizeof(double)*total_size);
    memcpy(new_vector2, vector2, sizeof(double)*total_size);
    if(is_value1==true)
        Value_Coef::Value_Coef(mesh, vector1, true, false, new_vector1, total_size);
    if(is_value2==true)
        Value_Coef::Value_Coef(mesh, vector2, true, false, new_vector2, total_size);

//    new_vector1->Dot(*new_vector2,&return_val);
    for(int i=0; i<total_size; i++)
        return_val +=new_vector1[i]*new_vector2[i];
    delete [] new_vector1;
    delete [] new_vector2;
    return return_val;
}



