#pragma once
#include <vector>
#include "../Util/Spherical_Harmonics.hpp"

/**
 * @brief Lebedev quadrature calculations.
 * @details 50-points version. 
 *          See USSR Comp. Math. Math. Phys. 1975, 15, 44.
 *          http://people.sc.fsu.edu/~jburkardt/cpp_src/sphere_lebedev_rule/sphere_lebedev_rule.html
 * @author Sungwoo Kang
 * @date 2015
 * @version 0.0.1
 **/
namespace Lebedev_Quadrature {
    /**
     * @brief Get Lebedev quadrature points on unit sphere.
     * @return Lebedev quadrature points.
     **/
    std::vector< std::vector<double> > get_points_on_unit_sphere();
    /**
     * @brief Get Lebedev quadrature weight correspond to the point.
     * @return Weight correspond to the Lebedev quadrature points.
     **/
    std::vector<double> get_weight();
    /**
     * @brief Integrate using Lebedev quadrature points.
     * @details \f$ \int d\Omega [vals]*[Ylm] \f$.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param m Magnetic moment of spherical harmonics function that is to be multiplied.
     * @param val Value to be computed.
     * @return Integrated value.
     **/
    double integrate( int l, int m, double val );
    /**
     * @brief Integrate using Lebedev quadrature points.
     * @details \f$ \int d\Omega [vals]*[Ylm] \f$.
     * @param l Angular momentum of spherical harmonics function that is to be multiplied.
     * @param m Magnetic moment of spherical harmonics function that is to be multiplied.
     * @param vals Radial value.
     * @return Integrated value.
     * @note See double integrate() also: Actual computation routine.
     **/
    std::vector<double> integrate( int l, int m, std::vector<double> vals );
    /**
     * @brief Print Lebedev quadrature point and corresponding weight.
     **/
    void print_quadruture();
};


