#pragma once
#include <vector>

namespace Interpolation{
namespace Linear {
    ///< @brief Interpolate the value at x on mesh using linear interpolation.
    double linear_interpolate(double x, std::vector<double> mesh, std::vector<double> value);
    ///< @brief Interpolate the value at x on mesh using bisection.
    double linear_interpolate_bisection(double x, std::vector<double> mesh, std::vector<double> value);
};
};

