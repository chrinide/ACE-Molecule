#pragma once
#include <string>
#include <vector>

#include "Teuchos_Array.hpp"
#include "AnasaziTypes.hpp"
//#include "Teuchos_SerialDenseMatrix.hpp"

namespace Read{
namespace Octopus {
    /**
     * @brief Read Octopus output (static/info) and get eigenvalues.
     **/
    void read_octopus(std::string filename, Teuchos::Array< std::vector<double > >& eigenvalues);
};
namespace ACE {
    /**
     * @brief Read ACE-Molecule ground state output and get eigenvalues.
     **/
    void read_ACE(std::string filename, Teuchos::Array< std::vector<double > >& eigenvalues);
    /**
     * @brief Read ACE-Molecule ground state and excited state output and get eigenvalues and excitation info.
     **/
    void read_ACE(std::string filename,
            std::vector<double>& energy,
            std::vector<double>& wavelength,
            std::vector<double>& oscillating_strength );
};

namespace Gaussian{
    /**
     * @brief Read G09 ground state and excited state output and get eigenvalues and excitation info.
     **/
    void read_gaussian(std::string filename,
            std::vector<double>& energy,
            std::vector<double>& wavelength,
            std::vector<double>& oscillating_strength );
};

namespace Txt{
    void read_txt(std::string filename,
            std::vector<double>& wavelength,
            std::vector< std::complex<double> >& polarizability );
};

std::vector<std::string> split(const std::string &s, char delim);
};

