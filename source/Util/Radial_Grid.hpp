#pragma once
#include <vector>
#include "Linear_Interpolation.hpp"
//#include <limits>
//std::numeric_limits<double>::max()
//template<typename T>

class RadialGrid {
    friend bool operator==(const RadialGrid& grid1, const RadialGrid& grid2 ){
        
        if ( !grid1.compatible(grid2) ){
            return false;
        }

        const int mesh_size = grid1.mesh.size();

        for (int i =0; i< mesh_size; i++ ){
            if( fabs( grid1.mesh[i] -  grid2.mesh[i]) > tolerance_mesh){
                return false;
            }
        }

        for (int i =0; i< mesh_size; i++ ){
            if( fabs( grid1.value[i] -  grid2.value[i]) > tolerance_val){
                return false;
            }
        }
        return true;

    };

    friend RadialGrid operator+(const RadialGrid& grid1, const RadialGrid& grid2){
        if ( !grid1.compatible(grid2) ){
            exit(-1);
        }
        int size = grid1.mesh.size();
        std::vector<double> result_value( grid2.value.begin(), grid2.value.end() );

        for (int i=0; i< size; i++){
            result_value[i]+=grid1.value[i];
        }

        return RadialGrid(result_value, grid1.mesh);        
    };

    public:
        RadialGrid(std::vector<double> value, std::vector<double> mesh):value(value),mesh(mesh){
            if (value.size() != mesh.size()){
                exit(-1);
            }
        }

        inline double get_value(int index) const {return value[index];} 
        inline double get_mesh(int index) const {return mesh[index];} 
        inline double interpolated( double r ) const{
            if(r<mesh[0]){ 
                return (value[1] - value[0]) / ( mesh[1] - mesh[0]) * (r - mesh[0]) + mesh[0];
            }
            
            return Interpolation::Linear::linear_interpolate(r, value, mesh);
        }

        bool compatible(const RadialGrid& grid, double tolerance = 1e-6) const {
            if ( mesh.size() !=grid.mesh.size() ){
                return false;
            }
            return true;
        }
    protected:
        std::vector<double> value;
        std::vector<double> mesh;
        constexpr static double tolerance_mesh = 1e-6;
        constexpr static double tolerance_val = 1e-6;
};
