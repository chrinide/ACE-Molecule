#pragma once
#include <string>
#include <vector>

class EHT_parameter{
    public:
        EHT_parameter(std::string filename);
        double get_orbital_energy(int atomic_number, int orbital_number);
        double get_cutoff(int atomic_number);
        double atomic_orbital(int atomic_number, int orbital_number, double* x);
        double get_number_of_basis_function(int atomic_number);
        bool is_orbital_index_ended(int atomic_number, int orbital_number);
        
        int get_number_of_valence_electron(int atomic_number);
        double get_zeta1(int atomic_number, int angular_momentum);

        std::string get_orbital_name(int atomic_number, int orbital_number);
    private:
        int number_of_atoms; // total number of atom
        std::vector<int> parameter_index;
        std::vector<int> number_of_valence_electron;
        std::vector<int> number_of_exponent;
        std::vector<int> principle_quantum_number;
        std::vector<int> angular_quantum_number;
        std::vector<double> Hii;
        std::vector<double> zeta1;
        std::vector<double> zeta2;
        std::vector<double> coeff1;
        std::vector<double> coeff2;
        std::vector<double> cutoff;
        std::vector<std::string> atom_label;

        void read_EHT_param(std::string filename);
        void calculate_cutoff();
};
