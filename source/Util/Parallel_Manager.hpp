#pragma once
#include "ACE_Config.hpp"  
#include "Teuchos_RCP.hpp"
#include "Epetra_Comm.h"
#ifdef ACE_HAVE_MPI
    #include "mpi.h"
#endif
#ifdef ACE_HAVE_OMP
    #include "omp.h"
#endif
#include <iostream>
#include <string>

class Parallel_Manager{

    friend std::ostream& operator<<(std::ostream &out, const Parallel_Manager& manager);
    friend int main(int argc, char* argv[]);
    public:

        enum operation {Max, Min, Sum, Prod};
        static Parallel_Manager& info();
        static int finalize(){ if(instance!=NULL) delete instance; return 0; };

        void change_group_size(int group_size=1);

        Teuchos::RCP<Epetra_Comm> get_group_comm();
        Teuchos::RCP<Epetra_Comm> get_all_comm();
        Teuchos::RCP<Epetra_Comm> get_rank_comm();

        int get_group_size(){ return group_size; };
        int get_group_rank(){ return group_rank; };
        int get_mpi_size(){ return mpi_size; };
        int get_mpi_rank(){ return mpi_rank; };
        int get_total_mpi_size(){ return total_mpi_size; };
        int get_total_mpi_rank(){ return total_mpi_rank; };
        int get_openmp_num_threads(){ return openmp_num_threads;};

        int get_openmp_thread_num(){ 
            #ifdef ACE_HAVE_OMP
            return omp_get_thread_num();
            #else
            return 0;
            #endif 
        };
        //int get_openmp_thread_num(){ return openmp_thread_num;};

        /**
         * @brief MPI_Gather, for each group or each rank.
         * @details Gather send_values from each processes and store to recv_data at root processor in rank order.
         * @note All pointers should be initialized.
         **/
        int group_gather(double* send_values, int send_count, double* recv_data, int recv_counts, int root);
        int group_gather(int* send_values, int send_count, int* recv_data, int recv_counts,int root);
        int rank_gather(int* send_values, int send_count, int* recv_data, int recv_counts,int root);
        int rank_gather(double* send_values, int send_count, double* recv_data, int recv_counts,int root);

        /**
         * @brief MPI_Gatherv, for each group or each rank.
         * @details Same with MPI_Gather but supports different send_values size.
         * @note All pointers should be initialized.
         **/
        int rank_gatherv(double* send_values, int send_count, double* recv_data, const int recv_counts[], const int displs[], int root );
        int group_gatherv(int* send_values, int send_count, int* recv_data, const int recv_counts[], const int displs[], int root );
        int group_gatherv(double* send_values, int send_count, double* recv_data, const int recv_counts[], const int displs[], int root );

        /**
         * @brief MPI_AllGatherv, for each group or each rank.
         * @details Same with MPI_Gatherv but stores to all processors instead of specified rank.
         * @note All pointers should be initialized.
         **/
        int group_allgatherv(double* send_values, int send_count, double* recv_data, const int* recv_counts, const int *displs);
        int group_allgatherv(int* send_values, int send_count, int* recv_data, const int* recv_counts, const int *displs);
        int group_allgather(int* send_values, int send_count, int* recv_data, int recv_counts);
        int group_allgather(double* send_values, int send_count, double* recv_data, int recv_counts);

        /**
         * @brief MPI_Scatter(v), for each group or each rank.
         * @details Divide and send send_data at root to recv_data at all processors.
         * @note All pointers should be initialized.
         **/
        int group_scatter(double* send_data, int send_count, double* recv_data, int recv_count, int root);
        int group_scatterv(double* send_data, int* send_count, double* recv_data, int recv_count, const int displs[], int root);
        int rank_scatter(double* send_data, int send_count, double* recv_data, int recv_count, int root);

        /**
         * @brief MPI_Barrier, for each group or each rank.
         * @details Pauses until all/group/rank processors reach this point.
         **/
        void all_barrier();
        void group_barrier();
        void rank_barrier();

        /**
         * @brief MPI_Bcast, for each group or each rank.
         * @details Send buffer at root processor to buffer at all/group/rank processors.
         * @note All pointers should be initialized.
         **/
        int all_bcast(int* buffer, int count, int root);
        int all_bcast(double* buffer, int count, int root);
        int group_bcast(int* buffer, int count, int root);        
        int group_bcast(double* buffer, int count, int root);
        int group_bcast(bool* buffer, int count, int root);
        int rank_bcast(int* buffer, int count, int root);
        int rank_bcast(double* buffer, int count, int root);

        /**
         * @brief MPI_Allreduce, for each group or each rank.
         * @details Retrieve send_data from all processors and apply op then store to recv_data at all processors.
         * @note All pointers should be initialized.
         **/
        void rank_allreduce(double* send_data, double* recv_data, int count, operation op);
        void rank_allreduce(int* send_data, int* recv_data, int count, operation op);
        void group_allreduce(double* send_data, double* recv_data, int count, operation op);
        void group_allreduce(int* send_data, int* recv_data, int count, operation op);
        void group_allreduce(std::complex<float>* send_data, std::complex<float>* recv_data, int count, operation op);
        void all_allreduce(double* send_data, double* recv_data, int count, operation op);
        void all_allreduce(int* send_data, int* recv_data, int count, operation op);
        // this is gpu tag 
        enum gputag {Diag,Exx,Kernal_Int,TAGEND,All}; 
        #ifdef USE_CUDA
        void assign_gpus(const int ngpu,gputag tag);
        #endif 
        bool get_gpu_enable(gputag tag=All);
        int get_ngpu();

        std::string print_error(int err_no);
    private:
        static Parallel_Manager* instance;

        static void construct(int argc, char* argv[], int group_size=1) ;

        Parallel_Manager(int argc, char* argv[], int group_size);
        ~Parallel_Manager();

        int total_mpi_size; // total number of processors
        int total_mpi_rank; // rank for all processors
        int group_size;  // total number of group
        int group_rank;  // group index
        int mpi_size;    // total number of processors belonging to a single group
        int mpi_rank;    // rank within a group
        int openmp_num_threads; // num thread 
        std::vector<gputag> vec_gpu_enables;
        int ngpu=0;
        //int openmp_thread_num;  // thread num

#ifdef ACE_HAVE_MPI
        MPI_Comm group_comm = MPI_COMM_NULL; // communicator among processors in the same group
        MPI_Comm rank_comm = MPI_COMM_NULL;  // communicator among processors which share the same rank 
#endif 
        //Teuchos::RCP<Epetra_Comm> epetra_group_comm;
        
};
