#pragma once 
#include "Verbose.hpp"
#include <stdexcept>
#include <string>

class Warning{
    public:
        static void print_warn(std::string& message ){
            Verbose::all() <<message << std::flush;
        }
        static void throw_error(const char* message,int type=0 ){
            auto tmp_message = std::string(message);
            return throw_error(tmp_message,type);
        }
        static void throw_error(std::string& message,int type=0 ){
            switch (type){
                case 0:
                    throw std::runtime_error(message);
                    break;
                default:
                    Verbose::all() <<message << std::flush;
            }
        };
};
