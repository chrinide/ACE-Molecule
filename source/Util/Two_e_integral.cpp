#include "Two_e_integral.hpp"

#include "Value_Coef.hpp"
#include "Integration.hpp"

using Teuchos::rcp;
using Teuchos::RCP;
using Teuchos::Array;

//mulliken symbol!
//(ij|kl) = intint i*(r1)j(r1)k*(r2)l(r2)/|r1-r2| dr1 dr2
double Two_e_integral::compute_two_center_integral(RCP<const Basis> mesh, RCP<const Epetra_MultiVector> orbital_coeff, int i, int j, int k, int l, RCP<Poisson_Solver> poisson_solver){
    auto map = mesh->get_map();
    RCP<Epetra_Vector> ii = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> jj = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> kk = rcp(new Epetra_Vector(*map));
    RCP<Epetra_Vector> ll = rcp(new Epetra_Vector(*map));
    ii->Scale(1.0,*orbital_coeff->operator()(i));
    jj->Scale(1.0,*orbital_coeff->operator()(j));
    kk->Scale(1.0,*orbital_coeff->operator()(k));
    ll->Scale(1.0,*orbital_coeff->operator()(l));
//    Value_Coef::Value_Coef(mesh,rcp(orbital_coeff->operator()(i)),false,true,ii);
//    Value_Coef::Value_Coef(mesh,rcp(orbital_coeff->operator()(j)),false,true,jj);
//    Value_Coef::Value_Coef(mesh,rcp(orbital_coeff->operator()(k)),false,true,kk);
//    Value_Coef::Value_Coef(mesh,rcp(orbital_coeff->operator()(l)),false,true,ll);
    Value_Coef::Value_Coef(mesh,ii,false,true,ii);
    Value_Coef::Value_Coef(mesh,jj,false,true,jj);
    Value_Coef::Value_Coef(mesh,kk,false,true,kk);
    Value_Coef::Value_Coef(mesh,ll,false,true,ll);
    return Two_e_integral::compute_two_center_integral(mesh,ii,jj,kk,ll,poisson_solver);
}

double Two_e_integral::compute_two_center_integral(RCP<const Basis> mesh, double** orbital_coeff, int i, int j, int k, int l, RCP<Poisson_Solver> poisson_solver){
    int total_size = mesh->get_original_size();
    double* ii = new double[total_size];
    double* jj = new double[total_size];
    double* kk = new double[total_size];
    double* ll = new double[total_size];
    Value_Coef::Value_Coef(mesh,orbital_coeff[i],false,true,ii, total_size);
    Value_Coef::Value_Coef(mesh,orbital_coeff[j],false,true,jj, total_size);
    Value_Coef::Value_Coef(mesh,orbital_coeff[k],false,true,kk, total_size);
    Value_Coef::Value_Coef(mesh,orbital_coeff[l],false,true,ll, total_size);
    double retval = Two_e_integral::compute_two_center_integral(mesh,ii,jj,kk,ll,poisson_solver);
    delete [] ii;
    delete [] jj;
    delete [] kk;
    delete [] ll;
    return retval;
}

//RCP<Epetra_Vector> i, j, k, and l are orbital value vectors
double Two_e_integral::compute_two_center_integral(RCP<const Basis> mesh, RCP<Epetra_Vector> i, RCP<Epetra_Vector> j, RCP<Epetra_Vector> k, RCP<Epetra_Vector> l, RCP<Poisson_Solver> poisson_solver){
    double return_val = 0.0;
    RCP<Epetra_Vector> kl = rcp(new Epetra_Vector(*(mesh->get_map())));
    kl->Multiply(1.0, *(k), *(l), 0.0);
    RCP<Epetra_Vector> Kji;
    Two_e_integral::compute_Kji(mesh,j,i,poisson_solver,Kji);
    return_val = Integration::integrate(mesh,Kji,true,kl,true);
    return return_val;
}
double Two_e_integral::compute_two_center_integral(RCP<const Basis> mesh, double* i, double* j, double* k, double* l, RCP<Poisson_Solver> poisson_solver){
    double return_val = 0.0;
    int total_size = mesh->get_original_size();
    double* kl = new double[total_size];
    for(int n=0;n <total_size; n++) kl[n] = k[n]*l[n];
    double* Kji = new double[total_size];
    Two_e_integral::compute_Kji(j,i,poisson_solver,Kji, total_size);
    return_val = Integration::integrate(mesh,Kji,true,kl,true);
    //for(int ni=0; n<total_size; n++) return_val += Kji[n]*kl[n];

    delete [] Kji;
    delete [] kl;
    return return_val;
}

//Epetra_Vector * i, j, k, and l are orbital value vectors
double Two_e_integral::compute_two_center_integral(RCP<const Basis> mesh, Epetra_Vector * i, Epetra_Vector * j, Epetra_Vector * k, Epetra_Vector * l, RCP<Poisson_Solver> poisson_solver){
    double return_val = 0.0;
    RCP<Epetra_Vector> kl = rcp(new Epetra_Vector(*(mesh->get_map())));
    kl->Multiply(1.0, *(k), *(l), 0.0);
    RCP<Epetra_Vector> Kji;
    Two_e_integral::compute_Kji(mesh,j,i,poisson_solver,Kji);
    return_val = Integration::integrate(mesh,Kji,true,kl,true);
    return return_val;
}

//RCP<Epetra_Vector>  k and l are orbital value vectors
double Two_e_integral::compute_two_center_integral(RCP<const Basis> mesh, RCP<Epetra_Vector> Kji, Epetra_Vector* k, Epetra_Vector* l){
    double return_val = 0.0;

    RCP<Epetra_Vector> kl = rcp(new Epetra_Vector(*(mesh->get_map())));
    kl->Multiply(1.0, *(k), *(l), 0.0);
    return_val = Integration::integrate(mesh, Kji, true, kl, true);

    return return_val;
}

double Two_e_integral::compute_two_center_integral(RCP<const Basis> mesh, double* Kji, double* k, double* l, int size){
    double return_val = 0.0;

    double* kl = new double[size];
    for(int i=0; i<size; i++) kl[i] = k[i]*l[i];
    return_val = Integration::integrate(mesh, Kji, true, kl, true);
    delete [] kl;

    return return_val;
}

int Two_e_integral::compute_Kji(RCP<const Basis> mesh, RCP<const Epetra_MultiVector> orbital_coeff, int j, int i, RCP<Poisson_Solver> poisson_solver, RCP<Epetra_Vector>& output){
    RCP<Epetra_Vector> jj;
    RCP<Epetra_Vector> ii;
    Value_Coef::Value_Coef(mesh,rcp(orbital_coeff->operator()(i)),false,true,ii);
    Value_Coef::Value_Coef(mesh,rcp(orbital_coeff->operator()(j)),false,true,jj);
    return Two_e_integral::compute_Kji(mesh,jj,ii,poisson_solver,output);
}

//RCP<Epetra_Vector> i and j are orbital value vectors
int Two_e_integral::compute_Kji(RCP<const Basis> mesh, RCP<Epetra_Vector> j, RCP<Epetra_Vector> i, RCP<Poisson_Solver> poisson_solver, RCP<Epetra_Vector>& output){
    Array<RCP<Epetra_Vector> > ji;
    ji.push_back( rcp(new Epetra_Vector(*(mesh->get_map()))));
    ji[0]->Multiply(1.0, *(j), *(i), 0.0);
    if(output == Teuchos::null){
        output = rcp(new Epetra_Vector(*(mesh->get_map())));
    }
    double tmp_val;
    RCP<Epetra_Vector> tmp_out;
    poisson_solver->compute(ji,tmp_out,tmp_val);
    output->Update(1.0, *tmp_out, 0.0);
    return 0;
}

//Epetra_Vector * i and j are orbital value vectors
int Two_e_integral::compute_Kji(RCP<const Basis> mesh, Epetra_Vector * j, Epetra_Vector * i, RCP<Poisson_Solver> poisson_solver, RCP<Epetra_Vector>& output){
    Array<RCP<Epetra_Vector> > ji;
    ji.push_back( rcp(new Epetra_Vector(*(mesh->get_map()))));
    ji[0]->Multiply(1.0, *(j), *(i), 0.0);
    if(output == Teuchos::null){
        output = rcp(new Epetra_Vector(*(mesh->get_map())));
    }
    double tmp_val;
    RCP<Epetra_Vector> tmp_out;
    poisson_solver->compute(ji,tmp_out,tmp_val);
    output->Update(1.0, *tmp_out, 0.0);
    return 0;
}

int Two_e_integral::compute_Kji(double* j, double * i, RCP<Poisson_Solver> poisson_solver, double* output, int size){
    double* density = new double[size];
    for(int k=0; k<size; ++k)
        density[k] = i[k]*j[k];
    double energy = 0;
    poisson_solver->compute(density, output, energy);
    delete [] density;
    return 0;
}
//RCP<Epetra_Vector> i and j are orbital value vectors
int Two_e_integral::compute_Kji(RCP<const Basis> mesh, Epetra_Vector* ji, RCP<Poisson_Solver> poisson_solver, RCP<Epetra_Vector>& output){
    Array<RCP<Epetra_Vector> > input;
    input.push_back( rcp(ji) );
    if(output == Teuchos::null){
        output = rcp(new Epetra_Vector(*(mesh->get_map())));
    }
    double tmp_val;
    RCP<Epetra_Vector> tmp_out;
    poisson_solver->compute(input,tmp_out,tmp_val);
    output->Update(1.0, *tmp_out, 0.0);
    return 0;
}

double Two_e_integral::compute_coulomb_integral(RCP<const Basis> mesh, RCP<const Epetra_MultiVector> orbital_coeff, int i, int j, RCP<Poisson_Solver> poisson_solver){
    return Two_e_integral::compute_two_center_integral(mesh, orbital_coeff, i, i, j, j, poisson_solver);
}

//RCP<Epetra_Vector> i and j are orbital value vectors
double Two_e_integral::compute_coulomb_integral(RCP<const Basis> mesh, RCP<Epetra_Vector> i, RCP<Epetra_Vector> j, RCP<Poisson_Solver> poisson_solver){
    return Two_e_integral::compute_two_center_integral(mesh, i, i, j, j, poisson_solver);
}

double Two_e_integral::compute_exchange_integral(RCP<const Basis> mesh, RCP<const Epetra_MultiVector> orbital_coeff, int i, int j, RCP<Poisson_Solver> poisson_solver){
    return Two_e_integral::compute_two_center_integral(mesh, orbital_coeff, i, j, i, j, poisson_solver);
}

//RCP<Epetra_Vector> i and j are orbital value vectors
double Two_e_integral::compute_exchange_integral(RCP<const Basis> mesh, RCP<Epetra_Vector> i, RCP<Epetra_Vector> j, RCP<Poisson_Solver> poisson_solver){
    return Two_e_integral::compute_two_center_integral(mesh, i, j, i, j, poisson_solver);
}
