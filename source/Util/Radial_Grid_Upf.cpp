#include "Radial_Grid_Upf.hpp"
#include <iostream>
#include <cmath>
#include "Read_Upf.hpp"
#include "../Util/Linear_Interpolation.hpp"
#include "../Util/Spherical_Harmonics.hpp"
#include "../Util/Spherical_Harmonics_Derivative.hpp"
#include "Verbose.hpp"

#define RGD2GD_CUTOFF 1.0E-20
#define HARTREE_CUTOFF 1.0E-50

using std::vector;
using std::string;
using std::abs;
using Teuchos::RCP;
using Spherical_Harmonics::Ylm;

vector<double> Radial_Grid::Upf::read_radial_mesh( string upf_filename ){
    vector<double> radial_mesh = Read::Upf::read_upf(upf_filename, "PP_R");

    while(abs(radial_mesh.back()) < 1.0E-10){
        radial_mesh.pop_back();
    }

    if( radial_mesh.begin() > 0.0 ){
        radial_mesh.insert(radial_mesh.begin(),0.0);
    }
    return radial_mesh;
}

vector<double> Radial_Grid::Upf::read_rhoatom( string upf_filename, vector<double> radial_mesh ){
    vector<double> rho;
    rho = Read::Upf::read_upf(upf_filename, "PP_RHOATOM");

    while(abs(rho[rho.size()-1]) < 1.0E-10){
        rho.pop_back();
    }

    // From the manual of Quantum Espresso, PP_RHOATOM in *.upf is
    // radial atomic (pseudo-)charge. This is 4*pi*r^2 times the true charge.
    // Therefore, this code saves the value divided by 4*pi*r^2.
    for(int i=0;i<rho.size();i++){
        rho[i]=rho[i]/(4*M_PI*radial_mesh[i+1]*radial_mesh[i+1]);
    }

    // linear extrapolation at r=0
    double tmp;
    tmp=(rho[1]-rho[0])/(radial_mesh[2]-radial_mesh[1])*(radial_mesh[0]-radial_mesh[1])+rho[0];
    rho.insert(rho.begin(),tmp);

    Verbose::single(Verbose::Detail) << "RHOATOM cutoff= " << radial_mesh[rho.size()-1]<<"\n";

    return rho;
}

vector< vector<double> > Radial_Grid::Upf::read_pao( string upf_filename, vector<double> radial_mesh, int atom_number ){
    int num_pao = 0;
    if(atom_number<=2){
        num_pao = 1;
    }
    else if(atom_number<=10 and atom_number>2){
        num_pao = 2;
    }
    else if(atom_number<=20 and atom_number>10){
        num_pao = 2;
    }

    vector< vector<double> > input_wavefunctions=Read::Upf::read_pao(upf_filename, num_pao);

    for(int i=0; i<num_pao; i++){
        while(abs(input_wavefunctions[i][input_wavefunctions[i].size()-1]) < 1.0E-10){
            input_wavefunctions[i].pop_back();
        }

        for(int j=0; j<input_wavefunctions[i].size(); j++){
            input_wavefunctions[i][j] = input_wavefunctions[i][j]/radial_mesh[j+1];

        }
    }

    double tmp;
    for(int i=0; i<num_pao; i++){
        tmp=(input_wavefunctions[i][1]-input_wavefunctions[i][0])/(input_wavefunctions[i][2]-input_wavefunctions[i][1])*(radial_mesh[0]-radial_mesh[1])+input_wavefunctions[i][0];
        input_wavefunctions[i].insert(input_wavefunctions[i].begin(),tmp);
    }

    return input_wavefunctions;
}

// dest size == grid_setting -> get_original_size
void Radial_Grid::Upf::get_basis_coeff_from_radial_grid( int l, int m, vector<double> src, vector<double> src_grid, double * src_center, RCP<const Basis> dest_mesh, RCP<Epetra_Vector> retval){

    retval -> PutScalar(0.0);

    int dim = dest_mesh -> get_original_size();
    const double ** scaled_grid = dest_mesh -> get_scaled_grid();

    for(int i = 0; i < dim; ++i ){
        if( retval -> Map().MyGID(i) ){
            double xi, yi, zi, r;
            int i_x = 0, i_y = 0, i_z = 0;
            dest_mesh -> decompose(i, &i_x, &i_y, &i_z );
            xi = scaled_grid[0][i_x] - src_center[0];
            yi = scaled_grid[1][i_y] - src_center[1];
            zi = scaled_grid[2][i_z] - src_center[2];
            r = sqrt(xi*xi+yi*yi+zi*zi);

            if( r <= src_grid.back() ) {
                //double val = Interpolation::Spline::splint( src_grid, src, y2, src.size(), r);
                double val = Interpolation::Linear::linear_interpolate( r, src_grid, src);
                val /= ( dest_mesh -> compute_basis(i_x, scaled_grid[0][i_x], 0) * dest_mesh -> compute_basis(i_y, scaled_grid[1][i_y], 1) * dest_mesh -> compute_basis(i_z, scaled_grid[2][i_z], 2) );
                val *= Ylm(l, m, xi, yi, zi);

                if( abs(val) > RGD2GD_CUTOFF ){
                    retval -> ReplaceGlobalValues(1, &val, &i);
                }
            }
        }
    }
}
