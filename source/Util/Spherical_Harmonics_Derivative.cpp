#include "Spherical_Harmonics_Derivative.hpp"
#include <iostream>
#include <stdexcept>
#include <cmath>
#include <cstdlib>
#include "Spherical_Harmonics.hpp"
#include "String_Util.hpp"
#include "Verbose.hpp"

#define __YLM_CUTOFF__ 1.0E-30

using std::sqrt;

/*
Spherical_Harmonics_Derivative::Spherical_Harmonics_Derivative(){
}
*/

// real spherical harmonics
double Spherical_Harmonics::Derivative::dYlm(int l,int m,double x,double y,double z, int d){ 
    //d=0 : x, d=1 : y, d=2 : z
    double r=sqrt(x*x+y*y+z*z);
    double Ylm = Spherical_Harmonics::Ylm(l,m,x,y,z);
    if( r < __YLM_CUTOFF__ ){
        return 0.0;
    }
    double dYlm = Spherical_Harmonics::Derivative::drlYlm(l,m,x,y,z,d);
    for(int i = 0; i < l; ++i){
        dYlm /= r;
    }
    if( d == 0 ) dYlm -= Ylm * l * x/r/r;
    if( d == 1 ) dYlm -= Ylm * l * y/r/r;
    if( d == 2 ) dYlm -= Ylm * l * z/r/r;

    return dYlm;
}

// r^l * real spherical harmonics
double Spherical_Harmonics::Derivative::drlYlm(int l,int m,double x,double y,double z,int d){
    double r=sqrt(x*x+y*y+z*z);
    double drlYlm;
    if( d < 0 and d > 2 ){
        throw std::invalid_argument("Wrong derivative direction!");
    }
    if (l==0){
        if (m==0){
            drlYlm = 0.0;
        }else{
            Verbose::all() << "Wrong spin angular momentum" << std::endl;
            exit(EXIT_FAILURE);
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==1){
        if(m==-1){
            if( d == 1 ) drlYlm=0.4886025119029199;
            else drlYlm = 0.0;
        }else if(m==0){
            if( d == 2 ) drlYlm=0.4886025119029199;
            else drlYlm = 0.0;
        }else if(m==1){
            if( d == 0 ) drlYlm=0.4886025119029199;
            else drlYlm = 0.0;
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==2){
        if(m==-2){
            if( d == 0 ) drlYlm=1.0925484305920792*y;
            if( d == 1 ) drlYlm=1.0925484305920792*x;
            if( d == 2 ) drlYlm=0.0;
        }else if(m==-1){
            if( d == 0 ) drlYlm=0.0;
            if( d == 1 ) drlYlm=1.0925484305920792*z;
            if( d == 2 ) drlYlm=1.0925484305920792*y;
        }else if(m==0){
            if( d == 0 ) drlYlm=0.31539156525252005*(-2*x);
            if( d == 1 ) drlYlm=0.31539156525252005*(-2*y);
            if( d == 2 ) drlYlm=0.31539156525252005*(4.0*z);
        }else if(m==1){
            if( d == 0 ) drlYlm=1.0925484305920792*z;
            if( d == 1 ) drlYlm=0.0;
            if( d == 2 ) drlYlm=1.0925484305920792*x;
        }else if(m==2){
            if( d == 0 ) drlYlm=0.5462742152960396*2*x;
            if( d == 1 ) drlYlm=0.5462742152960396*(-2*y);
            if( d == 2 ) drlYlm=0.0;
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==3){
        if(m==-3){
            if( d == 0 ) drlYlm=0.5900435899266435*6.0*x*y;
            if( d == 1 ) drlYlm=0.5900435899266435*3*(x*x-y*y);
            if( d == 2 ) drlYlm=0.0;
        }else if(m==-2){
            if( d == 0 ) drlYlm=2.890611442640554*y*z;
            if( d == 1 ) drlYlm=2.890611442640554*x*z;
            if( d == 2 ) drlYlm=2.890611442640554*x*y;
        }else if(m==-1){
            if( d == 0 ) drlYlm=0.4570457994644658*(-2*x*y);
            if( d == 1 ) drlYlm=0.4570457994644658*(4.0*z*z-x*x-3*y*y);
            if( d == 2 ) drlYlm=0.4570457994644658*8.0*z*y;
        }else if(m==0){
            if( d == 0 ) drlYlm=0.3731763325901154*z*(-6.0*x);
            if( d == 1 ) drlYlm=0.3731763325901154*z*(-6.0*y);
            if( d == 2 ) drlYlm=0.3731763325901154*3*(2.0*z*z-x*x-y*y);
        }else if(m==1){
            if( d == 0 ) drlYlm=0.4570457994644658*(4.0*z*z-3*x*x-y*y);
            if( d == 1 ) drlYlm=0.4570457994644658*x*(-2*y);
            if( d == 2 ) drlYlm=0.4570457994644658*x*(8.0*z);
        }else if(m==2){
            if( d == 0 ) drlYlm=1.445305721320277*2*x*z;
            if( d == 1 ) drlYlm=1.445305721320277*(-2*y*z);
            if( d == 2 ) drlYlm=1.445305721320277*(x*x-y*y);
        }else if(m==3){
            if( d == 0 ) drlYlm=0.5900435899266435*3*(x*x-y*y);
            if( d == 1 ) drlYlm=0.5900435899266435*(-6.0*y*x);
            if( d == 2 ) drlYlm=0.0;
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==4){
        if(m==-4){
            if( d == 0 ) drlYlm=2.5033429417967046*y*(3*x*x-y*y);
            if( d == 1 ) drlYlm=2.5033429417967046*x*(x*x-3*y*y);
            if( d == 2 ) drlYlm=0.0;
        }else if(m==-3){
            if( d == 0 ) drlYlm=1.7701307697799307*(6.0*x-y*y)*y*z;
            if( d == 1 ) drlYlm=1.7701307697799307*3*(x*x-y*y)*z;
            if( d == 2 ) drlYlm=1.7701307697799307*(3.0*x*x-y*y)*y;
        }else if(m==-2){
            if( d == 0 ) drlYlm=0.9461746957575601*y*(6.0*z*z-3*x*x-y*y);
            if( d == 1 ) drlYlm=0.9461746957575601*x*(6.0*z*z-x*x-3*y*y);
            if( d == 2 ) drlYlm=0.9461746957575601*12*x*y*z;
        }else if(m==-1){
            if( d == 0 ) drlYlm=0.6690465435572892*y*z*(-6*x);
            if( d == 1 ) drlYlm=0.6690465435572892*z*(4*z*z-3*x*x-9*y*y);
            if( d == 2 ) drlYlm=0.6690465435572892*3*y*(4*z*z-x*x-y*y);
        }else if(m==0){
            if( d == 0 ) drlYlm=0.10578554691520431*12*(-5*z*z+r*r)*x;
            if( d == 1 ) drlYlm=0.10578554691520431*12*(-5*z*z+r*r)*y;
            if( d == 2 ) drlYlm=0.10578554691520431*4*(35*z*z*z-30*(z*r*r+z*z*z)+3*r*r*z);
        }else if(m==1){
            if( d == 0 ) drlYlm=0.6690465435572892*z*(4*z*z-9*x*x-3*y*y);
            if( d == 1 ) drlYlm=0.6690465435572892*x*z*(-6*y);
            if( d == 2 ) drlYlm=0.6690465435572892*3*x*(4*z*z-x*x-y*y);
        }else if(m==2){
            if( d == 0 ) drlYlm=0.47308734787878004*2*x*(6.0*z*z-2*x*x);
            if( d == 1 ) drlYlm=0.47308734787878004*2*y*(6.0*z*z-2*x*x);
            if( d == 2 ) drlYlm=0.47308734787878004*(x*x-y*y)*(12.0*z-x*x-y*y);
        }else if(m==3){
            if( d == 0 ) drlYlm=1.7701307697799307*3*(x*x-y*y)*z;
            if( d == 1 ) drlYlm=1.7701307697799307*(-6*y)*x*z;
            if( d == 2 ) drlYlm=1.7701307697799307*(x*x-3*y*y)*x;
        }else if(m==4){
            //if( d == 0 ) drlYlm=0.6258357354491761*2*(2*x*x*x-3*x*y*y-3*y*y*x);
            if( d == 0 ) drlYlm=0.6258357354491761*4*(x*x-3*y*y)*x;
            if( d == 1 ) drlYlm=0.6258357354491761*4*(y*y-3*x*x)*y;
            if( d == 2 ) drlYlm=0.0;
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==5){
        if(m==-5){
            if( d == 0 ) drlYlm=0.6563820568401701*20*x*y*(x*x-y*y);
            if( d == 1 ) drlYlm=0.6563820568401701*5*(y*y*y*y+x*x*x*x-6*x*x*y*y);
            if( d == 2 ) drlYlm=0.0;
        }else if(m==-4){
            if( d == 0 ) drlYlm=8.302649259524165*y*z*(3*x*x-y*y);
            if( d == 1 ) drlYlm=8.302649259524165*x*z*(x*x-3*y*y);
            if( d == 2 ) drlYlm=8.302649259524165*x*y*(x*x-y*y);
        }else if(m==-3){
            if( d == 0 ) drlYlm=0.4892382994352504*x*y*(2*y*y-6*r*r-6*x*x+54*z*z);
            if( d == 1 ) drlYlm=0.4892382994352504*(3*y*y*r*r+2*y*y*y*y-27*y*y*z*z-3*x*x*r*r-6*x*x*y*y+27*x*x*z*z);
            if( d == 2 ) drlYlm=0.4892382994352504*y*(y*y*2*z-9*y*y*2*z-3*x*x*2*z+27*x*x*2*z);
        }else if(m==-2){
            if( d == 0 ) drlYlm=4.793536784973324*y*z*(2*z*z-y*y-3*x*x);
            if( d == 1 ) drlYlm=4.793536784973324*x*z*(2*z*z-3*y*y-x*x);
            if( d == 2 ) drlYlm=4.793536784973324*x*y*z*(3*z*z-r*r);
        }else if(m==-1){
            if( d == 0 ) drlYlm=0.45294665119569694*y*(-14*z*z*2*x+4*x*r*r);
            if( d == 1 ) drlYlm=0.45294665119569694*( (-14*z*z*r*r+r*r*r*r+21*z*z*z*z) + y*(-14*z*z*2*y+4*y*r*r) );
            if( d == 2 ) drlYlm=0.45294665119569694*y*(-14*2*z*r*r-14*z*z*2*z+4*z*r*r+21*4*z*z*z);
        }else if(m==0){
            if( d == 0 ) drlYlm=0.1169503224534236*z*(15*4*x*r*r-70*z*z*2*x);
            if( d == 1 ) drlYlm=0.1169503224534236*z*(15*4*y*r*r-70*z*z*2*y);
            if( d == 2 ) drlYlm=0.1169503224534236*((63*z*z*z*z+15*r*r*r*r-70*z*z*r*r) + z*(63*4*z*z*z+15*4*z*r*r-70*2*z*r*r-70*z*z*2*z));
        }else if(m==1){
            if( d == 0 ) drlYlm=0.45294665119569694*((r*r*r*r-14*z*z*r*r+21*z*z*z*z) + x*(4*x*r*r-14*z*z*2*x));
            if( d == 1 ) drlYlm=0.45294665119569694*x*(4*y*r*r-14*z*z*2*y);
            if( d == 2 ) drlYlm=0.45294665119569694*x*(4*z*r*r-14*2*z*r*r-14*z*z*2*z+21*4*z*z*z);
        }else if(m==2){
            if( d == 0 ) drlYlm=2.396768392486662*z*(y*y*2*x+3*2*x*z*z-2*x*r*r-x*x*2*x);
            if( d == 1 ) drlYlm=2.396768392486662*z*(-3*y*z*z+y*r*r+y*y*2*y-x*x*2*y);
            if( d == 2 ) drlYlm=2.396768392486662*( (-3*y*y*z*z+y*y*r*r+3*x*x*z*z-x*x*r*r) + z*(-3*y*y*2*z+y*y*2*+3*x*x*2*z-x*x*2*z) );
        }else if(m==3){
            if( d == 0 ) drlYlm=0.4892382994352504*((9*x*x*z*z-27*y*y*z*z-x*x*r*r+3*y*y*r*r) + x*(9*2*x*z*z-2*x*r*r-x*x*2*x+3*y*y*2*x));
            if( d == 1 ) drlYlm=0.4892382994352504*x*(-27*2*y*z*z-x*x*2*y+3*2*y*r*r+3*y*y*2*y);
            if( d == 2 ) drlYlm=0.4892382994352504*x*(9*x*x*2*z-27*y*y*2*z-x*x*2*z+3*y*y*2*z);
        }else if(m==4){
            if( d == 0 ) drlYlm=2.075662314881041*z*(-6*2*x*y*y+4*x*x*x);
            if( d == 1 ) drlYlm=2.075662314881041*z*(4*y*y*y-6*x*x*2*y);
            if( d == 2 ) drlYlm=2.075662314881041*( (y*y*y*y-6*x*x*y*y+x*x*x*x));
        }else if(m==5){
            if( d == 0 ) drlYlm=0.6563820568401701*((-10*x*x*y*y+5*y*y*y*y+x*x*x*x) + x*(-10*2*x*y*y+4*x*x*x));
            if( d == 1 ) drlYlm=0.6563820568401701*x*(-10*x*x*2*y+5*4*y*y*y);
            if( d == 2 ) drlYlm=0.0;
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l==6){
        if(m==-6){
            if( d == 0 ) drlYlm=1.3663682103838286*(y*(-10*x*x*y*y+3*x*x*x*x+3*y*y*y*y) + x*y*(-10*2*x*y*y+3*4*x*x*x) );
            if( d == 1 ) drlYlm=1.3663682103838286*(x*(-10*x*x*y*y+3*x*x*x*x+3*y*y*y*y) + x*y*(-10*x*x*2*y+3*4*y*y*y));
            if( d == 2 ) drlYlm=0.0;
        }else if(m==-5){
            if( d == 0 ) drlYlm=2.366619162231752*y*z*(-10*2*x*y*y+5*4*x*x*x);
            if( d == 1 ) drlYlm=2.366619162231752*(z*(y*y*y*y-10*x*x*y*y+5*x*x*x*x) + y*z*(4*y*y*y-10*x*x*2*y));
            if( d == 2 ) drlYlm=0.0;
        }else if(m==-4){
            if( d == 0 ) drlYlm=2.0182596029148967*(y*(-x*x*r*r+y*y*r*r-11*y*y*z*z+11*x*x*z*z) + x*y*(-2*x*r*r-x*x*2*x+y*y*2*x+11*x*x*2*z));
            if( d == 1 ) drlYlm=2.0182596029148967*(x*(-x*x*r*r+y*y*r*r-11*y*y*z*z+11*x*x*z*z) + x*y*(-x*x*2*y+2*y*r*r+y*y*2*y-11*2*y*z*z));
            if( d == 2 ) drlYlm=2.0182596029148967*x*y*(-x*x*2*z+y*y*2*z-11*y*y*2*z+11*x*x*2*z);
        }else if(m==-3){
            if( d == 0 ) drlYlm=0.9212052595149235*y*z*(-9*2*x*r*r-9*x*x*2*r+33*2*x*z*z+3*y*y*2*x);
            if( d == 1 ) drlYlm=0.9212052595149235*(z*(-11*y*y*z*z-9*x*x*r*r+33*x*x*z*z+3*y*y*r*r) + y*z*(-11*2*y*z*z-9*x*x*2*y+3*2*y*r*r+3*y*y*2*y));
            if( d == 2 ) drlYlm=0.9212052595149235*(y*(-11*y*y*z*z-9*x*x*r*r+33*x*x*z*z+3*y*y*r*r) + y*z*(-11*y*y*2*z-9*x*x*2*z+33*x*x*2*z+3*y*y*2*z));
        }else if(m==-2){
            if( d == 0 ) drlYlm=0.9212052595149235*(y*(r*r*r*r+33*z*z*z*z-18*z*z*r*r) + x*y*(4*x*r*r-18*z*z*2*x));
            if( d == 1 ) drlYlm=0.9212052595149235*(x*(r*r*r*r+33*z*z*z*z-18*z*z*r*r) + x*y*(4*y*r*r-18*z*z*2*y));
            if( d == 2 ) drlYlm=0.9212052595149235*x*y*(4*z*r*r+33*4*z*z*z-18*2*z*r*r-18*z*z*2*z);
        }else if(m==-1){
            if( d == 0 ) drlYlm=0.5826213625187314*y*z*(5*4*x*r*r-30*z*z*2*x);
            if( d == 1 ) drlYlm=0.5826213625187314*(z*(5*r*r*r*r+33*z*z*z*z-30*z*z*r*r) + y*z*(5*4*y*r*r-30*z*z*2*y));
            if( d == 2 ) drlYlm=0.5826213625187314*(y*(5*r*r*r*r+33*z*z*z*z-30*z*z*r*r) + y*z*(5*4*z*r*r+33*4*z*z*z-30*2*z*r*r-30*z*z*2*z));
        }else if(m==0){
            if( d == 0 ) drlYlm=0.06356920226762842*(-5*6*x*r*r*r*r+105*z*z*4*x*r*r-315*z*z*z*z*2*x);
            if( d == 1 ) drlYlm=0.06356920226762842*(-5*6*y*r*r*r*r+105*z*z*4*y*r*r-315*z*z*z*z*2*y);
            if( d == 2 ) drlYlm=0.06356920226762842*(231*6*z*z*z*z*z-5*6*z*r*r*r*r+105*(2*z*r*r*r*r+z*z*4*z*r*r)-315*(4*z*z*z*r*r+z*z*z*z*2*z));
        }else if(m==1){
            if( d == 0 ) drlYlm=0.5826213625187314*(z*(-30*z*z*r*r+33*z*z*z*z+5*r*r*r*r) + x*z*(-30*z*z*2*x+5*4*x*r*r));
            if( d == 1 ) drlYlm=0.5826213625187314*x*z*(-30*z*z*2*y+5*4*y*r*r);
            if( d == 2 ) drlYlm=0.5826213625187314*(x*(-30*z*z*r*r+33*z*z*z*z+5*r*r*r*r) + x*z*(-30*(2*z*r*r+z*z*2*z)+33*4*z*z*z+5*4*z*r*r));
        }else if(m==2){
            if( d == 0 ) drlYlm=0.46060262975746175*(33*2*x*z*z*z*z+2*x*r*r*r*r+x*x*4*x*r*r-y*y*4*x*r*r-18*(2*x*z*z*r*r+x*x*z*z*2*x)+18*y*y*z*z*2*x);
            if( d == 1 ) drlYlm=0.46060262975746175*(x*x*4*y*r*r-2*y*r*r*r*r-y*y*4*y*r*r-18*x*x*z*z*2*y+18*(2*y*z*z*r*r+y*y*z*z*2*y)-33*2*y*z*z*z*z);
            if( d == 2 ) drlYlm=0.46060262975746175*(33*x*x*4*z*z*z+x*x*4*z*r*r-y*y*4*z*r*r-18*(x*x*2*z*r*r+x*x*z*z*2*z)+18*(y*y*2*z*r*r+y*y*z*z*2*z)-33*y*y*4*z*z*z);
        }else if(m==3){
            if( d == 0 ) drlYlm=0.9212052595149235*(z*(-3*x*x*r*r-33*y*y*z*z+9*y*y*r*r+11*x*x*z*z) + x*z*(-3*(2*x*r*r+x*x*2*x)+9*y*y*2*x+11*2*x*z*z));
            if( d == 1 ) drlYlm=0.9212052595149235*x*z*(-3*x*x*2*y-33*2*y*z*z+9*(2*y*r*r+y*y*2*y));
            if( d == 2 ) drlYlm=0.9212052595149235*(x*z*(-3*x*x*2*z-33*y*y*2*z+9*y*y*2*z+11*x*x*2*z) + x*(-3*x*x*r*r-33*y*y*z*z+9*y*y*r*r+11*x*x*z*z));
        }else if(m==4){
            if( d == 0 ) drlYlm=0.5045649007287242*(-66*2*x*y*y*z*z-4*x*x*x*r*r-x*x*x*x*2*x+6*(2*x*y*y*r*r+x*x*y*y*2*x)+11*4*x*x*x*z*z-y*y*y*y*2*x);
            if( d == 1 ) drlYlm=0.5045649007287242*(11*4*y*y*y*z*z-66*x*x*2*y*z*z-x*x*x*x*2*y+6*(x*x*2*y*r*r+x*x*y*y*2*y)-4*y*y*y*r*r-y*y*y*y*2*y);
            if( d == 2 ) drlYlm=0.5045649007287242*(11*y*y*y*y*2*z-66*x*x*y*y*2*z-x*x*x*x*2*z+6*x*x*y*y*2*z+11*x*x*x*x*2*z-y*y*y*y*2*z);
        }else if(m==5){
            if( d == 0 ) drlYlm=2.366619162231752*(z*(5*y*y*y*y+x*x*x*x-10*x*x*y*y) + x*z*(4*x*x*x-10*2*x*y*y));
            if( d == 1 ) drlYlm=2.366619162231752*x*z*(5*4*y*y*y-10*x*x*2*y);
            if( d == 2 ) drlYlm=2.366619162231752*x*(5*y*y*y*y+x*x*x*x-10*x*x*y*y);
        }else if(m==6){
            if( d == 0 ) drlYlm=0.6831841051919143*(6*x*x*x*x*x+15*2*x*y*y*y*y-15*4*x*x*x*y*y);
            if( d == 1 ) drlYlm=0.6831841051919143*(15*x*x*4*y*y*y-15*x*x*x*x*2*y-6*y*y*y*y*y);
            if( d == 2 ) drlYlm=0.0;
        }else{
            throw std::invalid_argument("Wrong spin angular momentum " + String_Util::to_string(m) + " for l = " + String_Util::to_string(l) +"!");
        }
    }else if(l>6){
        throw std::invalid_argument("Orbital angular momentum l > 6 is not supported yet.");
    }
    return drlYlm;
}

/*
// Calculate associated Legendre function
double Spherical_Harmonics_Derivative::Plm(int l,int m,double x){
  double P00=1.0;
  double P10=x;
  double P11=-sqrt(1-x*x);
  double P1_1=-0.5*P11;

  double Plm=0.0;

  if(l==0){
    if(m==0){
      Plm=P00;
    }else{
      cout << "Wrong spin angular momentum" << endl;
      exit(EXIT_FAILURE);
    }
  }else if(l==1){
    if(m==-1){
      Plm=P1_1;
    }else if(m==0){
      Plm=P00;
    }else if(m==1){
      Plm=P11;
    }else{
      cout << "Wrong spin angular momentum" << endl;
      exit(EXIT_FAILURE);
    }
  }else if(l>1){
    double Plm=P11;
    // First, calculate Pll
    for(int i=1;i<l;i++){
      Plm = -(2.0*i+1.0) * sqrt(1.0-x*x) * Plm;
    }
    // Second, calculate Plm
    
    // end
  }
  return Plm;
}
*/
