#pragma once
#include <string>
#include <vector>

namespace Read{
namespace Upf {
    //< @return True if the UPF file version is 2.0.
    bool get_type(std::string filename);
    //void read_header(std::string filename, double* Zval, int* number_vps, int* number_pao, int* mesh_size);
    /**
     * @brief Read UPF header and parse the data.
     * @param filename Input filename.
     * @param Zval Number of valence electrons.
     * @param number_vps
     * @param number_pao
     * @param mesh_size UPF file mesh size.
     * @param core_correction Availablity of NLCC.
     **/ 
    void read_header(std::string filename, double* Zval, int* number_vps, int* number_pao, int* mesh_size, bool* core_correction);
    /**
     * @brief Read requested field, described on the mesh.
     * @param filename UPF filename
     * @param field Database field name.
     **/
    std::vector<double> read_upf(std::string filename, std::string field); 
    /**
     * @brief Read nonlocal potential part.
     **/
    std::vector< std::vector< std::vector<double> > > read_nonlocal(std::string filename, int number_vps, std::vector<int>& oamom);
    /**
     * @brief Read E_KB (Coefficient).
     **/
    std::vector<double> read_ekb(std::string filename, int number_vps);
    std::vector< std::vector<double> > read_pao(std::string filename, int number_pao);
};
};
