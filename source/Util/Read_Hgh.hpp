#pragma once
#include <string>
#include <vector>

namespace Read{
/**
 * @brief HGH pseudopotential reader.
 * @details Read two formats of HGH pseudopotential. (Requires HghFormat).
 *             1. HghFormat is hgh
 *                Format from Phys. Rev. B 58, p.3641 (1998), Table I.
 *                Zion    rloc    C1        C2        C3        C4
 *                        r0        h^0_1,1    h^0_2,2    h^0_3,3
 *                        r1        h^1_1,1    h^1_2,2    h^1_3,3
 *                                  k^1_1,1    k^1_2,2    k^1_3,3
 *                        r2        h^2_1,1    h^2_2,2    h^2_3,3
 *                                  k^2_1,1    k^2_2,2    k^2_3,3
 *             2. HghFormat is Willand
 *                Format from arXiv:1212.6011.v2, Table V.
 *                This format is used for abinit.
 *                Comment line
 *                Zatom    Zion    construnction date
 *                pspcod    pspxc    lmax    lloc    mmax    r2well [Note: this line is not read]
 *                rloc        Nloc    C1        C2
 *                Nnonloc
 *                rs        h^s_11    h^s_21    ...
 *                                    h^s_22    ...
 *                                            ...
 *                rp        h^p_11    h^p_21    ...
 *                                    h^p_22    ...
 *                                            ...
 *                ...
 *                rcore    ccore
 * @note While this namespace is written by Kwangwoo and Sunghwan, the function API doc is written by Sungwoo.
 *      And I am curious about some implementations, like unused output and some outputs being appended.
 **/
namespace Hgh {
    /**
     * @param [in] filename HGH pseudopotential file name.
     * @param [out] Zval Number of valence electrons.
     * @param [in] type HGH pseudopotential file type. HGH or Willand.
     * @param [out] core_correction If NLCC exists, true.
     **/
    void read_header(std::string filename, double& Zval, std::string type, bool& core_correction);
    /**
     * @param [in] filename HGH pseudopotential file name.
     * @param [in] type HGH pseudopotential file type. HGH or Willand.
     * @param [out] rloc r_loc value of HGH pseudopotential specification. Value is appended.
     * @param [out] nloc Length of c_i. Value is appended.
     * @param [out] c_i ith value corresponds to c_(i+1) value of HGH pseudopotential specification. Value is appended. Zero values are trucated.
     **/
    void read_local(std::string filename, std::string type, std::vector<double>& rloc, std::vector<int>& nloc, std::vector< std::vector<double> >& c_i);
    /**
     * @param [in] filename HGH pseudopotential file name.
     * @param [in] type HGH pseudopotential file type. HGH or Willand.
     * @param [out] h_ij_total h_ij value of HGH pseudopotential specification. Value is appended.
     * @param [out] k_ij_total Meant to be k_ij value of HGH pseudopotential specification. This value is untouched in current implementations.
     * @param [out] r_l List of r_l values (r_0, r_1, ...) of HGH pseudopotential specification. (Corresponds to angular momentum part of projector).
     * @param [out] n_l Number of nonzero elements of h_ii. If h_11 only, n_l = 1. If h_ij exists up to h_22, n_l = 2. If h_ij exists up to h_33, n_l = 3.
     * @param [out] number_vps_file Effectively, size of r_l.
     * @param [out] oamom_tmp List of projector's orbital angular momentums. r_0 -> 0, r_1 -> 1, r_2 -> 2, ... Value is appended.
     * @param [out] itype Not used. why?
     * @details Detailed indexing of h_ij_total: h_ij_total[-1] is the value read in this function.
     * Second index corresponds to angular momentum (and r_l).
     * Third index differs by corresponding n_l values.
     * If n_l[i] == 1, h_ij_total[-1][l] has size of 1 and contains h11 only.
     * If n_l[i] == 2, h_ij_total[-1][l] has size of 4 and contains h11 h12 h21 h22.
     * If n_l[i] == 3, h_ij_total[-1][l] has size of 6 and contains h11 h12 h13 h21 h22 h23 h31 h32 h33.
     **/
    void read_nonlocal(std::string filename, std::string type, std::vector< std::vector<std:: vector<double> > >& h_ij_total, std::vector< std::vector< std::vector<double> > >& k_ij_total, std::vector< std::vector<double> >& r_l, std::vector< std::vector<int> >& n_l, std::vector<int>& number_vps_file, std::vector<int>& oamom_tmp, int itype);
    /**
     * @param [in] filename HGH pseudopotential file name.
     * @param [out] rcore r_core of HGH pseudopotential specification.
     * @param [out] ccore c_core of HGH pseudopotential specification.
     **/
    void read_core_correction(std::string filename, double& rcore, double& ccore);
};
};
