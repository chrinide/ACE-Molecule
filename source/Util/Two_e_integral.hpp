#pragma once
#include "Teuchos_Array.hpp"
#include "Teuchos_RCP.hpp"

#include "Epetra_MultiVector.h"
#include "Epetra_Vector.h"

#include "../Compute/Poisson.hpp"
#include "../Basis/Basis.hpp"
//#include "Verbose.hpp"

namespace Two_e_integral{
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> mesh, 
        Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, 
        int i, int j, int k, int l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> mesh, 
        double** orbital_coeff, 
        int i, int j, int k, int l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    double compute_two_center_integral(
            Teuchos::RCP<const Basis> mesh, 
            double* Kji,
            double* k,
            double* l,
            int size
    );

    int compute_Kji(
        Teuchos::RCP<const Basis> mesh, 
        Teuchos::RCP<const Epetra_MultiVector> orbital_coeff, 
        int j, int i, 
        Teuchos::RCP<Poisson_Solver> poisson_solver, 
        Teuchos::RCP<Epetra_Vector>& output
    );
    //RCP<Epetra_Vector> k and l are orbital value vectors
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> mesh, 
        Teuchos::RCP<Epetra_Vector> Kji, 
        Epetra_Vector* k, Epetra_Vector* l
    );


    //RCP<Epetra_Vector> i, j, k, and l are orbital value vectors
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> mesh, 
        Teuchos::RCP<Epetra_Vector> i, Teuchos::RCP<Epetra_Vector> j, 
        Teuchos::RCP<Epetra_Vector> k, Teuchos::RCP<Epetra_Vector> l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> mesh, 
        double* i, double* j, 
        double* k, double* l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );

    int compute_Kji(
        Teuchos::RCP<const Basis> mesh, 
        Teuchos::RCP<Epetra_Vector> j, Teuchos::RCP<Epetra_Vector> i, 
        Teuchos::RCP<Poisson_Solver> poisson_solver, 
        Teuchos::RCP<Epetra_Vector>& output
    );

    //Epetra_Vector* i, j, k, and l are orbital value vectors
    double compute_two_center_integral(
        Teuchos::RCP<const Basis> mesh, 
        Epetra_Vector * i, Epetra_Vector * j, 
        Epetra_Vector * k, Epetra_Vector * l, 
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    int compute_Kji(
        Teuchos::RCP<const Basis> mesh, 
        Epetra_Vector * j, Epetra_Vector * i, 
        Teuchos::RCP<Poisson_Solver> poisson_solver, 
        Teuchos::RCP<Epetra_Vector>& output
    );
    int compute_Kji(
        Teuchos::RCP<const Basis> mesh,
        Epetra_Vector* ji,
        Teuchos::RCP<Poisson_Solver> poisson_solver, 
        Teuchos::RCP<Epetra_Vector>& output
    );
    int compute_Kji(
        double* j,
        double * i,
        Teuchos::RCP<Poisson_Solver> poisson_solver,
        double* output,
        int size
    );

    double compute_coulomb_integral(
        Teuchos::RCP<const Basis> mesh,
        Teuchos::RCP<const Epetra_MultiVector> orbital_coeff,
        int i, int j,
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    double compute_coulomb_integral(
        Teuchos::RCP<const Basis> mesh,
        Teuchos::RCP<Epetra_Vector> i, Teuchos::RCP<Epetra_Vector> j,
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );

    double compute_exchange_integral(
        Teuchos::RCP<const Basis> mesh,
        Teuchos::RCP<const Epetra_MultiVector> orbital_coeff,
        int i, int j,
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
    double compute_exchange_integral(
        Teuchos::RCP<const Basis> mesh,
        Teuchos::RCP<Epetra_Vector> i, Teuchos::RCP<Epetra_Vector> j,
        Teuchos::RCP<Poisson_Solver> poisson_solver
    );
}
