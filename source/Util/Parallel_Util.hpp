#pragma once
#include "Teuchos_RCP.hpp"
#include "Epetra_Vector.h"
#include "Teuchos_SerialDenseMatrix.hpp"

/**
 * @brief Parallel routine that wraps Parallel_Manager.
 */
namespace Parallel_Util{

    /**
     * @brief Summation over rank_comm. Double version.
     * @param send_data Datas to sum over processors.
     * @param recv_data Summation results. Should be allocated before the call.
     * @param count Size of send_data and recv_data.
     **/
    void rank_sum(double* send_data, double* recv_data, int count);
    /**
     * @brief Summation over group_comm. Double version.
     * @param send_data Datas to sum over processors.
     * @param recv_data Summation results. Should be allocated before the call.
     * @param count Size of send_data and recv_data.
     **/
    void group_sum(double* send_data, double* recv_data, int count);
    /**
     * @brief Summation over MPI_COMM_WORLD. Double version.
     * @param send_data Datas to sum over processors.
     * @param recv_data Summation results. Should be allocated before the call.
     * @param count Size of send_data and recv_data.
     **/
    void all_sum(double* send_data, double* recv_data, int count);
    /**
     * @brief Summation over rank_comm. Int version.
     * @param send_data Datas to sum over processors.
     * @param recv_data Summation results. Should be allocated before the call.
     * @param count Size of send_data and recv_data.
     **/
    void rank_sum(int* send_data, int* recv_data, int count);
    /**
     * @brief Summation over group_comm. Int version.
     * @param send_data Datas to sum over processors.
     * @param recv_data Summation results. Should be allocated before the call.
     * @param count Size of send_data and recv_data.
     **/
    void group_sum(int* send_data, int* recv_data, int count);
    /**
     * @brief Summation over MPI_COMM_WORLD. Int version.
     * @param send_data Datas to sum over processors.
     * @param recv_data Summation results. Should be allocated before the call.
     * @param count Size of send_data and recv_data.
     **/
    void all_sum(int* send_data, int* recv_data, int count);

    /**
     * @brief Broadcast Epetra_MultiVector to each process.
     * @param send_data Epetra_MultiVector to send.
     * @param recv_data Reciving data array. Should be allocated before the call.
     */
    void group_allgather_multivector(Teuchos::RCP<const Epetra_MultiVector> send_data, double** recv_data);
    /**
     * @brief Broadcast Epetra_Vector to each process.
     * @param send_data Epetra_Vector to send.
     * @param recv_data Reciving data array. Should be allocated before the call.
     */
    void group_allgather_vector(Teuchos::RCP<const Epetra_Vector> send_data, double* recv_data);
    void group_allgather_vector(Epetra_Vector* send_data, double* recv_data);
    /**
     * @brief Broadcast Epetra_MultiVector to a process.
     * @param send_data Epetra_MultiVector to send.
     * @param recv_data Reciving data array. Should be allocated before the call.
     * @param root Processor number that the values to be gathered.
     */
    void group_gather_multivector(Teuchos::RCP<Epetra_MultiVector> send_data, double** recv_data, int root);
    /**
     * @brief Broadcast Epetra_Vector to a process.
     * @param send_data Epetra_Vector to send.
     * @param recv_data Reciving data array. Should be allocated before the call.
     * @param root Processor number that the values to be gathered.
     */
    void group_gather_vector(Teuchos::RCP<Epetra_Vector> send_data, double* recv_data, int root);
    void group_gather_vector(Epetra_Vector* send_data, double* recv_data, int root);

    /**
     * @brief Extract nonzero values from Epetra_MultiVector and put its values and indices to std::vector and distribute those to all processors.
     * @param entity Epetra_MultiVector to extract.
     * @param vals Output value array.
     * @param inds Output index array.
     * @param cutoff Anything that is smaller than cutoff is treated as zero..
     */
    void group_extract_nonzero_from_multivector(
            Teuchos::RCP<Epetra_MultiVector> entity,
            std::vector< std::vector<double> >& vals, std::vector< std::vector<int> >& inds, double cutoff = 1.0E-30);

    /**
     * @brief Extract nonzero values from Epetra_Vector and put its values and indices to std::vector and distribute those to all processors.
     * @param entity Epetra_MultiVector to extract.
     * @param vals Output value array.
     * @param inds Output index array.
     * @param cutoff Anything that is smaller than cutoff is treated as zero..
     */
    void group_extract_nonzero_from_vector(
            Teuchos::RCP<Epetra_Vector> entity,
            std::vector<double>& vals, std::vector<int>& inds, double cutoff = 1.0E-30);

    /**
     * @brief Extract nonzero values from Epetra_MultiVector and put its values and indices to std::vector. Each processors hold their own values only.
     * @param entity Epetra_MultiVector to extract.
     * @param vals Output value array.
     * @param inds Output index array.
     * @param cutoff Anything that is smaller than cutoff is treated as zero..
     */
    void extract_nonzero_from_multivector_locally(
            Teuchos::RCP<Epetra_MultiVector> entity,
            std::vector< std::vector<double> >& vals, std::vector< std::vector<int> >& inds, double cutoff = 1.0E-30);

    /**
     * @brief Extract nonzero values from Epetra_Vector and put its values and indices to std::vector. Each processors hold their own values only.
     * @param entity Epetra_MultiVector to extract.
     * @param vals Output value array.
     * @param inds Output index array.
     * @param cutoff Anything that is smaller than cutoff is treated as zero..
     */
    void extract_nonzero_from_vector_locally(
            Teuchos::RCP<Epetra_Vector> entity,
            std::vector<double>& vals, std::vector<int>& inds, double cutoff = 1.0E-30);

    //Teuchos::RCP<Epetra_CrsMatrix> matrix_redisitribution(Teuchos::RCP<Epetra_CrsMatrix> matrix);
    Teuchos::RCP<Epetra_CrsMatrix> matrix_redisitribution(Teuchos::RCP<Epetra_CrsMatrix> input, int*& tar_row_indices, int*& tar_col_indices, double*& tar_values);
    Teuchos::RCP<Epetra_CrsMatrix> matrix_redisitribution(Teuchos::RCP<Epetra_CrsMatrix> matrix, bool is_copy=true);
    //Teuchos::RCP<Epetra_CrsMatrix> matrix_redisitribution(Teuchos::RCP<Epetra_CrsMatrix> input, int*& tar_row_indices, int*& tar_col_indices, double*& tar_values);


    /**
     * @brief convert Teuchos::RCP<Epetra_CrsMatrix> into Teuchos::SerialDenseMatrix<int,double>
     */
    void matrix_serialize(Teuchos::RCP<Epetra_CrsMatrix> input, Teuchos::SerialDenseMatrix<int,double>& Matrix);

    void group_gather_vector(std::vector<double>& vector); //this routine is possible only for nonlocal_pp of single grid
    void group_gather_vector(std::vector<int>& vector); //this routine is possible only for nonlocal_pp of single grid
}
