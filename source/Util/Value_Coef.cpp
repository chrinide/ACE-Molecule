#include "Value_Coef.hpp"
#include "Teuchos_BLAS.hpp"
using Teuchos::RCP;
using Teuchos::rcp;
namespace Value_Coef{
    RCP<Teuchos::BLAS<int,double> > blas = rcp(new Teuchos::BLAS<int,double>());
}
int Value_Coef::Value_Coef(RCP<const Basis> mesh, RCP<const Epetra_MultiVector> input, bool is_val_input, bool is_val_output, RCP<Epetra_MultiVector>& output){

    output = rcp( new Epetra_MultiVector(*input) );
    const double* scaling  = mesh->get_scaling();

    if (is_val_input==true and is_val_output==false){
        output->Scale(sqrt(scaling[0]*scaling[1]*scaling[2]));
    }
    else if (is_val_input==false and is_val_output==true){
        output->Scale(1/sqrt(scaling[0]*scaling[1]*scaling[2]));
    }
    return 0;
}

int Value_Coef::Value_Coef(RCP<const Basis> mesh, RCP<const Epetra_Vector> input, bool is_val_input, bool is_val_output, RCP<Epetra_Vector>& output){

    output = rcp( new Epetra_Vector(*input) );
    const double* scaling  = mesh->get_scaling();

    if (is_val_input==true and is_val_output==false){
        output->Scale(sqrt(scaling[0]*scaling[1]*scaling[2]));
    }
    else if (is_val_input==false and is_val_output==true){
        output->Scale(1/sqrt(scaling[0]*scaling[1]*scaling[2]));
    }
    return 0;
}

int Value_Coef::Value_Coef(RCP<const Basis> mesh, double* input, bool is_val_input, bool is_val_output, double* output, int size){

    const double* scaling  = mesh->get_scaling();

    blas->COPY(size, input,1,output,1);
    if (is_val_input==true and is_val_output==false){
        blas->SCAL(size,sqrt(scaling[0]*scaling[1]*scaling[2]),output,1);
//        for(int i=0; i<size; i++)
//            output[i] = input[i]*sqrt(scaling[0]*scaling[1]*scaling[2]);
    }
    else if (is_val_input==false and is_val_output==true){
        blas->SCAL(size,1.0/sqrt(scaling[0]*scaling[1]*scaling[2]),output,1);
//        for(int i=0; i<size; i++)
//            output[i] = input[i]/sqrt(scaling[0]*scaling[1]*scaling[2]);
    }
    return 0;
}
