#pragma once

#include <string>
#include <vector>
#include <utility>
#include <map>

extern "C" {
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlversion.h>
}

namespace Read{
/**
 * @brief Structure containing informations about a single PAW partial wave state.
 * @author Sungwoo Kang
 * @date 2015
 * @version 0.0.1
 **/
typedef struct _Paw_wave_state{
    /**
     * @brief State id.
     **/
    std::string state;
    /**
     * @brief State main quantum number.
     * @note Maybe change to in to avoid unsigned-signed comparison.
     **/
    unsigned int n;
    /**
     * @brief State angular momentum.
     * @note Maybe change to in to avoid unsigned-signed comparison.
     **/
    unsigned int l;
    /**
     * @brief State occupation number.
     * @note Maybe change to in to avoid unsigned-signed comparison.
     **/
    unsigned int occupation_number;
    /**
     * @brief Cutoff radius of state.
     * @note Maybe change to in to avoid unsigned-signed comparison.
     **/
    double cutoff_radius;
    /**
     * @brief Energy of the state that are calculated.
     **/
    double energy;
} PAW_STATE;

/**
 * @brief Read GPAW-XML type PAW dataset and contain the data.
 * @author Sungwoo Kang
 * @date 2015
 * @version 0.0.1
 * @todo Change this to namespace, so that not all informations are read.
 *       Essentially, separate data containing part from here.
 **/
class Read_Paw{
    public:
        //Read_Paw( Read_Paw &src );// Should be const, with using functions
        /**
         * @brief Constructor.
         * @param filename Filename to read.
         * @param is_xml Specifies dataset type. Original intention is to make it able to read gz-compressed files.
         * @note Actually this just call set_pawfile_type() and read_file(). This is unnecessary.
         **/
        Read_Paw(std::string filename, bool is_xml);
        //Read_Paw operator=( Read_Paw &src ); shallow 

        /**
         * @brief Overall read routine.
         * @details This function reads following int/string data:
         *  Atom symbol, Z, core elec #, valence elec #, XC functional, AE kinetic / XC / electrostatic energy (not needed), core kinetic energy, paw_radius (for paw_setup version 0.7), EXX core-core part contribution (unimplemented).
         *          This function reads following array data:
         *  Radial grid info, AE core density, PS core density, PS valence density (unused), AE partial wave, PS partial wave, proejctor function, zero potential.
         *          This function analyze the shape function input and store it as array form.
         *          This function reads following matrix data:
         *  AE core kinetic energy density, PS core kinetic energy density, EXX X-matrix, which all are unimplemented for now.
         *          This function reads valence state info and store as a PAW_STATE variable.
         *
         * @param paw_filename File to read.
         * @return Always zero.
         **/
        int read_file(std::string paw_filename);

        /**
         * @brief Data return: List of radial grid informations specified.
         * @return List of grid ID.
         * @note Change to namespace to remove this.
         **/
        std::vector<std::string> get_grid_types();
        /**
         * @brief Data return: Radial grid information.
         * @param grid_id Grid ID
         * @return Grid r value.
         * @note Change to namespace to remove this.
         **/
        std::vector<double> get_grid( std::string grid_id );
        /**
         * @brief Data return: Analytic derivative of radial grid.
         * @param grid_id Grid ID
         * @return Analytic grid derivative at r value.
         * @note Change to namespace to remove this.
         **/
        std::vector<double> get_grid_derivative( std::string grid_id );

        /**
         * @brief Data return: AE core density.
         * @return Grid ID, AE core density.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_all_electron_core_density_r();
        /**
         * @brief Data return: PS core density.
         * @return Grid ID, PS core density.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_smooth_core_density_r();
        /**
         * @brief Data return: PS valence density.
         * @return Grid ID, PS valence density.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_smooth_valence_density_r();
        /**
         * @brief Data return: Zero potential.
         * @return Grid ID, zero potential.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_zero_potential_r();

        /**
         * @brief Data return: Partial wave ID list.
         * @return List of partial wave ID.
         * @note Change to namespace to remove this.
         **/
        std::vector<std::string> get_partial_wave_types();
        /**
         * @brief Data return: AE partial wave.
         * @param wave_state ID of PW desired.
         * @return Grid ID, AE partial wave.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_all_electron_partial_wave_r( std::string wave_state );
        /**
         * @brief Data return: PS partial wave.
         * @param wave_state ID of PW desired.
         * @return Grid ID, PS partial wave.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_smooth_partial_wave_r( std::string wave_state );
        /**
         * @brief Data return: Projector function.
         * @param wave_state ID of PW desired.
         * @return Grid ID, projector function.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_projector_function_r( std::string wave_state );
        /**
         * @brief Data return: AE partial wave.
         * @param wave_state Index of PW desired.
         * @return Grid ID, AE partial wave.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_all_electron_partial_wave_r( int wave_state );
        /**
         * @brief Data return: AE partial wave.
         * @param wave_state Index of PW desired.
         * @return Grid ID, PS partial wave.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_smooth_partial_wave_r( int wave_state );
        /**
         * @brief Data return: Projector function.
         * @param wave_state Index of PW desired.
         * @return Grid ID, projector function.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_projector_function_r( int wave_state );
        /**
         * @brief Data return: PAW state.
         * @param wave_state ID of PW desired.
         * @return See PAW_STATE.
         * @note Change to namespace to remove this.
         **/
        PAW_STATE get_partial_wave_state( std::string wave_state );
        /**
         * @brief Data return: PAW state.
         * @param wave_state Index of PW desired.
         * @return See PAW_STATE.
         * @note Change to namespace to remove this.
         **/
        PAW_STATE get_partial_wave_state( int wave_state );

        /**
         * @brief Data return: Kinetic energy differnece matrix
         * @details \f$ <AE_i|T|AE_j>-<PS_i|T|PS_j> \f$.
         * @return KE difference matrix.
         * @note Change to namespace to remove this.
         **/
        std::vector< std::vector<double> > get_kinetic_energy_difference_matrix();

        /**
         * @brief Data return: AE core kinetic energy density.
         * @details \f$ <AE_{core}|T|AE_{core}> \f$.
         * @return pair<string, vector<double> >: Grid ID, AE core KE density.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_all_electron_core_KE_density_r();
        /**
         * @brief Data return: PS core kinetic energy density.
         * @details \f$ <PS_{core}|T|PS_{core}> \f$.
         * @return Grid ID, PS core KE density.
         * @note Change to namespace to remove this.
         **/
        std::pair< std::string, std::vector<double> > get_smooth_core_KE_density_r();

        /**
         * @brief Data return: Core EXX energy.
         * @return Core EXX energy.
         * @note Change to namespace to remove this.
         **/
        double get_EXX_core();
        /**
         * @brief Data return: Valence EXX energy.
         * @return Valence EXX energy between i and jth partial waves.
         * @note Change to namespace to remove this.
         **/
        std::vector< std::vector<double> > get_EXX_matrix();

        /**
         * @brief Data return: Core kinetic energy.
         * @return Core kinetic energy.
         * @note Change to namespace to remove this.
         **/
        double get_core_kinetic_energy();
        /**
         * @brief Data return: AE kinetic energy.
         * @return AE kinetic energy.
         * @note Change to namespace to remove this.
         **/
        double get_ae_kinetic_energy();
        /**
         * @brief Data return: AE XC energy.
         * @return AE XC energy.
         * @note Change to namespace to remove this.
         **/
        double get_ae_xc_energy();
        /**
         * @brief Data return: AE hartree energy.
         * @return AE hartree energy.
         * @note Change to namespace to remove this.
         **/
        double get_ae_electrostatic_energy();
        /**
         * @brief Data return: AE total energy.
         * @return AE total energy.
         * @note Change to namespace to remove this.
         **/
        double get_ae_total_energy();

        /**
         * @brief Data return: Atom symbol from dataset.
         * @return Atom symbol from dataset.
         * @note Change to namespace to remove this.
         **/
        std::string get_atom_symbol();
        /**
         * @brief Data return: Atom number.
         * @return Atom number.
         * @note Change to namespace to remove this.
         **/
        int get_atom_number();
        /**
         * @brief Data return: Number of core electrons.
         * @return Number of core electrons.
         * @note Change to namespace to remove this.
         **/
        int get_core_electron_number();
        /**
         * @brief Data return: Number of valence electrons.
         * @return Number of valence electrons.
         * @note Change to namespace to remove this.
         **/
        int get_valence_electron_number();

        /**
         * @brief Set pawfile type variable.
         * @note Change to namespace to remove this.
         *       This is unneccesary.
         **/
        void set_pawfile_type(bool is_xml);
        /**
         * @brief Get pawfile type variable.
         * @return is xml or not.
         * @note Change to namespace to remove this.
         *       This is unneccesary.
         **/
        bool get_pawfile_type();

        /**
         * @brief Data return: Get shape function type.
         * @return Type of shape function. 1: Gaussian, 2: Sinc, 3: Exponential, 4: Bessel (unimplemented)
         * @note Change to namespace to remove this.
         *       See https://wiki.fysik.dtu.dk/gpaw/setups/pawxml.html#shape-function-for-the-compensation-charge
         *       May change to int to avoid int-uint comparison.
         **/
        unsigned int get_shape_function_type();
        /**
         * @brief Data return: Get shape function rc.
         * @return shape function rc.
         * @note Change to namespace to remove this.
         *       See https://wiki.fysik.dtu.dk/gpaw/setups/pawxml.html#shape-function-for-the-compensation-charge
         **/
        double get_shape_function_rc();
        /**
         * @brief Data return: Get shape function lambda.
         * @return lambda value of shape function for exponential type.
         * @note Change to namespace to remove this.
         *       See https://wiki.fysik.dtu.dk/gpaw/setups/pawxml.html#shape-function-for-the-compensation-charge
         **/
        double get_shape_function_lambda();

        /**
         * @brief Data return: Get XC type.
         * @return 1: LDA, 2: GGA, 3:MGGA, 4:HYBrid.
         * @note Change to namespace to remove this.
         *       This seems unnecessary. Even if this is necessary, string would be better.
         **/
        int get_xc_type();
        /**
         * @brief Data return: Get XC functionals.
         * @return X, C functional LIBXC no.
         **/
        std::vector<int> get_xc_functionals();

        /**
         * @brief Data return: Get PAW cutoff radius.
         * @return PAW cutoff radius.
         * @note Change to namespace to remove this.
         **/
        double get_cutoff_radius();
        /**
         * @brief Data return: Get projector function cutoff.
         * @return Largest projector function cutoff.
         * @note Maybe move to Paw_Species.
         **/
        double get_projector_cutoff();
        /**
         * @brief Data return: Get PAW core density cutoff radius.
         * @return PAW core density cutoff radius.
         * @note Maybe move to Paw_Species.
         **/
        double get_core_density_cutoff();
        /**
         * @brief Data return: Get PAW partial wave cutoff radius.
         * @return Largest partial wave cutoff radius.
         * @note Maybe move to Paw_Species.
         **/
        double get_partial_wave_cutoff();
        /**
         * @brief Data return: Get paw_radius.
         * @return paw_radius variable.
         * @note Change to namespace to remove this.
         **/
        double get_paw_radius();

        /**
         * @brief Process compensation charge infomation and put onto grid.
         * @param l Angular momentum input.
         * @param grid_id ID of grid to put compensation charge on.
         * @return Corresponding compensation charge..
         * @note Maybe move to Paw_Species.
         **/
        std::vector<double> get_compensation_charge_on_grid( int l, std::string grid_id );

        /**
         * @brief Calculate compensation charge radial part.
         * @param r Radial position.
         * @param l Angular momentum input.
         * @return Corresponding compensation charge..
         * @note Maybe move to Paw_Species.
         **/
        double get_radial_compensation_charge(double r, int l);

        std::string filename;
    private:
        /**
         * @brief Read data on grid.
         * @param a_node current node that contains grid-formatted data.
         * @return Grid ID, Data.
         **/
        std::pair< std::string, std::vector<double> > read_data_on_grid( xmlNode *a_node );
        /**
         * @brief Read one grid data and store it to this -> grid_map, this -> grid_derivative_map.
         * @param grid_setting_node Current node that contains grid data.
         **/
        void read_grid_setting( xmlNode* grid_setting_node );
        /**
         * @brief Read all PAW states info and store it to this -> paw_states_map.
         * @param state_node Current node that the name is valence_states.
         **/
        void read_state_elements( xmlNode* state_node );
        /**
         * @brief Read matrix data.
         * @param a_node current node that contains matrix data.
         * @return Resulting matrix data.
         **/
        std::vector< std::vector<double> > read_matrix( xmlNode* a_node );

        /**
         * @brief Interprete XC data string and put libxc numbers to xc_functionals.
         * @param xc_name current node that contains XC data.
         **/
        void read_xc_info( char* xc_name );
        //std::vector< std::vector<double> > read_xc_info( const char* xc_name );

        /**
         * @brief Calculate cutoff radius.
         * @return Cutoff radius calculated from zero potential.
         * @note Is this necessary?
         **/
        double calc_cutoff_radius();

        std::string atom_symbol;
        int atom_number;
        int core_electron;
        int val_electron;

        double core_kinetic_energy;
        double ae_kinetic_energy, ae_xc_energy, ae_electrostatic_energy, ae_total_energy;

        int xc_type;
        std::vector<int> xc_functionals;

        unsigned int compensation_charge_shape_type;
        double compensation_charge_rc;
        double compensation_charge_lambda;
        double cutoff_radius;
        double paw_radius;

        bool type_xml;
        std::map< std::string, std::vector<double> > grid_map;
        std::map< std::string, std::vector<double> > grid_derivative_map;

        std::pair< std::string, std::vector<double> > all_electron_density_core;
        std::pair< std::string, std::vector<double> > smooth_density_core;
        std::pair< std::string, std::vector<double> > smooth_density_valence;
        std::pair< std::string, std::vector<double> > zero_potential;
        std::vector< std::vector<double> > kinetic_energy_difference;

        std::pair< std::string, std::vector<double> > all_electron_KE_density_core;
        std::pair< std::string, std::vector<double> > smooth_KE_density_core;

        double EXX_core;
        std::vector< std::vector<double> > EXX_matrix;

        std::map<std::string, PAW_STATE> paw_states_map;
        std::map< std::string, std::pair< std::string, std::vector<double> > > all_electron_partial_waves;
        std::map< std::string, std::pair< std::string, std::vector<double> > > smooth_partial_waves;
        std::map< std::string, std::pair< std::string, std::vector<double> > > projector_functions;

        std::vector<std::string> paw_states_list;

};
};
