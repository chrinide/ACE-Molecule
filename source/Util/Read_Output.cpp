#include "Read_Output.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include "Verbose.hpp"

using std::endl;
using std::string;
using std::vector;
using std::ifstream;
using std::istringstream;

void Read::Octopus::read_octopus(string filename, Teuchos::Array< vector<double > >& eigenvalues){
    vector<double> evals;
    evals.clear();

    string word;

    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all() << "Read::Octopus::read_octopus - CANNOT find " << filename << endl;
        exit(EXIT_FAILURE);
    }

    int sp_polarize = eigenvalues.size();

    bool end1 = false;
    bool end2 = false;
    while(inputfile.eof() == false){
        getline(inputfile, word);
        if(word.find("Eigenvalue",0) != string::npos and word.find("Occupation",0) != string::npos){
            while(true){
                getline(inputfile, word);
                istringstream iss (word);
                string str;
                iss >> str >> str >> str;

                if(std::abs(atof(str.c_str())) > 1.0E-10){
                    evals.push_back(atof(str.c_str()));
                }
                else{
                    end1 = true;
                    break;
                }
            }
        }
/*
        if(word.find("External",0) != string::npos){
            istringstream iss (word);
            string str;
            iss >> str >> str >> str;

            octopus_external_energy = atof(str.c_str());
            end2 = true;
        }
*/
//        if(end1 and end2) break;
        if(end1) break;
    }

    inputfile.close();

    int eigenvalues_size = 0;
    for(int ispin=0; ispin<eigenvalues.size(); ispin++){
        for(int i=0; i<eigenvalues[ispin].size(); i++){
            eigenvalues_size++;
        }
    }

    if(eigenvalues_size > evals.size()){
        Verbose::all() << "Read::Octopus::read_octopus - Wrong input file: " << filename << endl;
        Verbose::all() << "eigenvalues_size = " << eigenvalues_size << " > " << evals.size() << " = evals.size()" << endl;
        exit(EXIT_FAILURE);
    }

    if(sp_polarize == 2){
        vector<double> spin_up;
        vector<double> spin_down;
        spin_up.clear();
        spin_down.clear();

        for(int i=0; i<evals.size(); i++){
            if(i%2 == 0) spin_up.push_back(evals[i]);
            else spin_down.push_back(evals[i]);
        }

        for(int i=0; i<eigenvalues[0].size(); i++){
            eigenvalues[0][i] = spin_up[i];
        }

        for(int i=0; i<eigenvalues[1].size(); i++){
            eigenvalues[1][i] = spin_down[i];
        }
    }
    else{
        for(int i=0; i<eigenvalues[0].size(); i++){
            eigenvalues[0][i] = evals[i];
        }
    }

    return;
}

void Read::ACE::read_ACE(string filename, Teuchos::Array< vector<double > >& eigenvalues){
    vector<double> evals;
    evals.clear();

    string word;

    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all() << "Read::ACE::read_ACE - Wrong input file: " << filename << endl;
        exit(EXIT_FAILURE);
    }

    int sp_polarize = eigenvalues.size();

    bool end1 = false;
    bool end2 = false;
    while(inputfile.eof() == false){
        getline(inputfile, word);
        if(word.find("SPIN",0) != string::npos){
            while(true){
                getline(inputfile, word);
                istringstream iss (word);
                string str;
                iss >> str >> str;

                if(std::abs(atof(str.c_str())) > 1.0E-10){
                    evals.push_back(atof(str.c_str()));
                }
                else{
                    end1 = true;
                    break;
                }
            }
        }
/*
        if(word.find("External energy",0) != string::npos){
            istringstream iss(word);
            string str;
            iss >> str >> str >> str >> str;
            for(int i=0;i<5;i++) getline(inputfile, word);
            bool is_it_final=false;
            if(word.find("#",0)) is_it_final=true;
            else{
                for(int i=0;i<4;i++) getline(inputfile, word);
                if(word.find("#",0)) is_it_final=true;
            }
            for(int i=0;i<2;i++) getline(inputfile, word);
            if(word.find("converged")){
                octopus_external_energy = atof(str.c_str());
                end2 = true;
            }
        }
*/
        if(end1) break;
    }

    inputfile.close();

    int eigenvalues_size = 0;
    for(int ispin=0; ispin<eigenvalues.size(); ispin++){
        for(int i=0; i<eigenvalues[ispin].size(); i++){
            eigenvalues_size++;
        }
    }

    if(eigenvalues_size > evals.size()){
        Verbose::all() << "Read::ACE::read_ACE - Wrong input file: " << filename << endl;
        Verbose::all() << "eigenvalues_size = " << eigenvalues_size << " > " << evals.size() << " = evals.size()" << endl;
        exit(EXIT_FAILURE);
    }

    if(sp_polarize == 2){
        Verbose::all() << "Read::ACE::read_ACE - spin unrestricted is not supported " << filename << endl;
        exit(EXIT_FAILURE);
    }
    else{
        for(int i=0; i<eigenvalues[0].size(); i++){
            eigenvalues[0][i] = evals[i];
        }
    }

    return;
}

void Read::Gaussian::read_gaussian(string filename,
                                   std::vector<double>& energy,
                                   std::vector<double>& wavelength,
                                   std::vector<double>& oscillating_strength ){

    string word;
    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all() << "Read::Gaussian::read_Gaussian - Wrong input file: " << filename << endl;
        exit(EXIT_FAILURE);
    }


    while(inputfile.eof() == false){
        getline(inputfile, word);
        std::vector<std::string> splited = Read::split(word,' ');
        if(splited.size()>3){
            if(splited[1]=="Excited" and splited[2]=="State"){
                for(int i=0; i<splited.size(); i++){
                    if(splited[i]=="eV"){
                        energy.push_back(atof(splited[i-1].c_str()));
                    }
                    if(splited[i]=="nm"){
                        wavelength.push_back(atof(splited[i-1].c_str()));
                        std::vector<std::string> splited2 = Read::split(splited[i+2],'=');
                        oscillating_strength.push_back(atof(splited2[1].c_str()));
                    }

                }
            }
        }
    }
}
void Read::Txt::read_txt(std::string filename,
        std::vector<double>& wavelength,
        std::vector< std::complex<double> >& polarizability ){
    string word;
    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all() << "Read::Txt::read_txt - Wrong input file: " << filename << endl;
        exit(EXIT_FAILURE);
    }

    getline(inputfile, word);
    while(inputfile.eof() == false){
        getline(inputfile, word);
        std::vector<std::string> splited = Read::split(word,' ');
        if(splited.size()>=3){
            wavelength.push_back(atof(splited[0].c_str()));
            double real = atof(splited[1].c_str());
            double imag = atof(splited[2].c_str());
            std::complex<double> tmp (real, imag);
            polarizability.push_back(tmp);
        }
    }
}

std::vector<std::string> Read::split(const std::string &s, char delim) {
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

void Read::ACE::read_ACE(string filename,
                                   std::vector<double>& energy,
                                   std::vector<double>& wavelength,
                                   std::vector<double>& oscillating_strength ){

    string word;
    ifstream inputfile(filename.c_str());

    if(inputfile.fail()){
        Verbose::all() << "Read::Gaussian::read_ACE - Wrong input file: " << filename << endl;
        exit(EXIT_FAILURE);
    }


    while(inputfile.eof() == false){
        getline(inputfile, word);
        std::vector<std::string> splited = Read::split(word,' ');
        if(splited.size()>3){
            if(splited[2]=="Excitation" and splited[3] == "energy"){
                double E = atof(splited[8].c_str());
                getline(inputfile, word);
                getline(inputfile, word);
                getline(inputfile, word);
                getline(inputfile, word);
                std::vector<std::string> splited2 = Read::split(word,' ');
                double OS = atof(splited2[8].c_str());
                if(OS > 0.00001){
                    energy.push_back(E);
                    oscillating_strength.push_back(OS);
                    wavelength.push_back((1239.84191277/E));
                }
            }
        }
    }
}
