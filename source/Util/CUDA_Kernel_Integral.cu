#include "CUDA_Kernel_Integral.hpp"
#include "../Util/Verbose.hpp"
//#include "../Util/Parallel_Manager.hpp"
#include "../Util/String_Util.hpp"

#include <cublas_v2.h>
#include "cuda_runtime_api.h"
#include <cstdio>
#include "nvToolsExt.h"

#include <time.h>
//#include "Time_Measure.hpp"


const char* cublasGetErrorString(cublasStatus_t status)
{
    switch(status)
    {
        case CUBLAS_STATUS_SUCCESS: return "CUBLAS_STATUS_SUCCESS";
        case CUBLAS_STATUS_NOT_INITIALIZED: return "CUBLAS_STATUS_NOT_INITIALIZED";
        case CUBLAS_STATUS_ALLOC_FAILED: return "CUBLAS_STATUS_ALLOC_FAILED";
        case CUBLAS_STATUS_INVALID_VALUE: return "CUBLAS_STATUS_INVALID_VALUE"; 
        case CUBLAS_STATUS_ARCH_MISMATCH: return "CUBLAS_STATUS_ARCH_MISMATCH"; 
        case CUBLAS_STATUS_MAPPING_ERROR: return "CUBLAS_STATUS_MAPPING_ERROR";
        case CUBLAS_STATUS_EXECUTION_FAILED: return "CUBLAS_STATUS_EXECUTION_FAILED"; 
        case CUBLAS_STATUS_INTERNAL_ERROR: return "CUBLAS_STATUS_INTERNAL_ERROR"; 
    }
    return "unknown error";
}


namespace CUDA_Kernel_Integral{
    int num_threads;
    int num_blocks;

    /// temporary memory space for convolution
    double** dT_gamma;
    /// temporary memory space for convolution
    double** dT_gamma2;
    /// temporary memory space for convolution
    double** dT_beta;
    /// temporary memory space for convolution
    double** dT_beta2;
    /// temporary memory space for convolution
    double** drho;
    // pointer of pointer in Device
    double** dFx; double** dFy; double** dFz;
    // pointer of pointer in Device
    double** dFx_dev; double** dFy_dev; double** dFz_dev;
    
    double* d_potential;
    double* d_density;
    // pointer of pointer in Device
    double** dT_gamma_dev; double** dT_gamma2_dev;
    double** dT_beta_dev; double** dT_beta2_dev;
    double** drho_dev;
    int* di_xs; int* di_ys; int* di_zs;
    int* d_from_basic;
    bool gpu_memory_allocated=false;

}
int CUDA_Kernel_Integral::compute_gpu(const int total_size,
            const int* points, const double* scaling,
            double* density, double*& hartree_potential,
            std::vector<double> w_t,
            std::vector<double*> F_xs,
            std::vector<double*> F_ys,
            std::vector<double*> F_zs){

    nvtxRangePushA("CUDA_ISF");

    //nvtxRangeStartA("CUDA_ISF");

    Verbose::single(Verbose::Detail)<< std::endl << "#--------------------------------------------------- CUDA_Kernel_Integral::compute2" << std::endl;

    cublasStatus_t check;
    num_threads=points[0];
    num_blocks = points[1]*points[2];

    /////////////////////////////
/*
    cudaStream_t stream;
    cudaError_t check2 = cudaStreamCreate( &stream );
    if (cudaSuccess!=check2) {
        std::cout << "1 "<<std::endl;
        exit(-1);
    }
*/
    cublasHandle_t handle;
    check = cublasCreate(&handle);
    if (CUBLAS_STATUS_SUCCESS!=check) {
        std::cout << "2 "<<std::endl;
        exit(-1);
    }

/*
    check = cublasSetStream( handle, stream  );
    if (CUBLAS_STATUS_SUCCESS!=check) {
        std::cout << "3 "<<std::endl;
        exit(-1);
    }
*/
    const double factor = std::sqrt(scaling[0]*scaling[1]*scaling[2]);
    
    cudaError_t check2 = cudaMemcpy(d_density, density, total_size*sizeof(double), cudaMemcpyHostToDevice);
    if(check2!=  cudaSuccess){
        std::cout << "check1 "<<cudaGetErrorString(check2) <<std::endl ;
        exit(-1);
    }
    
    cublasStatus_t check3 = cublasDscal(handle, total_size, &factor,  d_density, 1);  //value to coeff 
    if(check3!=  CUBLAS_STATUS_SUCCESS){
        std::cout << "check1 "<<cublasGetErrorString(check3) <<std::endl ;
        exit(-1);
    }

    // From here, notations same as [J.Juselius; D.Sundholm, JCP, 126, 094101 (2007)]
    // See eq.(20) of the paper.

    float dgemm_time=0, transpose_time=0, unfold_time=0, dscal_time=0;
    clock_t st = clock();
    // Construct d^gamma'_{alpha',beta'}
    fold <<< num_blocks, num_threads>>>(points[0],points[1],points[2],di_xs,di_ys,di_zs,d_from_basic,d_density,drho_dev);
    clock_t et = clock();
    //std::cout << "fold:  "<< ((float)(et-st))/CLOCKS_PER_SEC <<std::endl;
    //fold <<< points[1], num_threads, 0, stream >>>(points[0],points[1],points[2],d_density,d_from_basic,drho_dev);

//    cudaDeviceSynchronize();

    // alloc  potential in gpu memory
    cudaMalloc((void**)& d_potential,  total_size*sizeof(double));
    cudaMemset( d_potential, 0,  total_size*sizeof(double));


    double one =1.0; double zero = 0.0;
    if (cudaSuccess!=cudaGetLastError()){
        std::cout << cudaGetErrorString(cudaGetLastError()) ;
        exit(-1);
    }
    cublasStatus_t stat1;
    const size_t t_size = w_t.size();
    for(size_t alpha=0; alpha<t_size; alpha++){
        cublasSetMatrix(points[0], points[0], sizeof(double), F_xs[alpha], points[0], dFx[0], points[0] );        
        st = clock();
        stat1 =cublasDgemmBatched(handle, CUBLAS_OP_N, CUBLAS_OP_N, points[0], points[1], points[0], &one, (const double**) dFx_dev, points[0], (const double**) drho_dev, points[0], &zero, dT_gamma_dev, points[0], points[2] );
        et = clock();
        dgemm_time += ((float)(et-st))/CLOCKS_PER_SEC ;
//        cublasStatus_t stat1 =cublasDgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, points[0], points[1], points[0], &one, (const double*) dFx[0], points[0], (const double*) drho[0], points[0], &zero, dT_gamma[0], points[0] );


        cublasSetMatrix(points[1], points[1], sizeof(double), F_ys[alpha], points[1], dFy[0], points[1] );
        st = clock();
        stat1 = cublasDgemmBatched(handle, CUBLAS_OP_N, /*CUBLAS_OP_N*/CUBLAS_OP_T, points[0], points[1], points[1], &one, (const double**) dT_gamma_dev, points[0], (const double**) dFy_dev, points[1], &zero, dT_gamma2_dev, points[0], points[2] );
        et = clock();
        dgemm_time += ((float)(et-st))/CLOCKS_PER_SEC ;

        
        st = clock();
        transpose<<< num_blocks, num_threads >>> (points[0],points[1],points[2],dT_gamma2_dev, dT_beta_dev);
        et = clock();
        //transpose<<< num_blocks, num_threads, 0, stream >>> (points[0],points[1],points[2],dT_gamma2_dev, dT_beta_dev);
        transpose_time+=((float)(et-st))/CLOCKS_PER_SEC ;
        cublasSetMatrix(points[2], points[2], sizeof(double), F_zs[alpha], points[2], dFz[0], points[2] );

        st = clock();
        stat1 = cublasDgemmBatched(handle, CUBLAS_OP_N, /*CUBLAS_OP_N*/CUBLAS_OP_T, points[0], points[2], points[2], &one, (const double**) dT_beta_dev, points[0], (const double**) dFz_dev, points[2], &zero, dT_beta2_dev, points[0], points[1] );
        et = clock();
        dgemm_time += ((float)(et-st))/CLOCKS_PER_SEC ;

        st = clock();
        unfold <<< num_blocks, num_threads >>> ( w_t[alpha], points[0], points[1], points[2],di_xs,di_ys,di_zs, d_from_basic, (const double**) dT_beta2_dev, d_potential );
        et = clock();
        unfold_time += ((float) (et-st))/CLOCKS_PER_SEC;

        //unfold <<< num_blocks, num_threads, 0, stream >>> ( w_t[alpha], points[0], points[1], points[2],di_xs,di_ys,di_zs, d_from_basic, (const double**) dT_beta2_dev, d_potential );
    }
    const double coef = 2.0/sqrt(M_PI);
    st = clock();
    stat1 = cublasDscal(handle, total_size, &coef, d_potential,1 );
    et = clock();
    dscal_time += ((float) (et-st))/CLOCKS_PER_SEC;

    cudaMemcpy(hartree_potential, d_potential, total_size*sizeof(double), cudaMemcpyDeviceToHost);
    cudaFree(d_potential);
    //cudaStreamDestroy(stream);
    cublasStatus_t checkblas = cublasDestroy(handle);
    if (CUBLAS_STATUS_SUCCESS!= checkblas){
        std::cout << "last error: "<<cublasGetErrorString(checkblas) <<std::endl;
        exit(-1);
    }
    nvtxRangePop();
//    std::cout << "dgemm_time\ttranspose_time\tunfold_time\tdscal_time" <<std::endl;
//    std::cout << dgemm_time  << "\t"  << transpose_time <<"\t" << unfold_time <<"\t" <<dscal_time <<std::endl;
    //nvtxRangeEnd("CUDA_ISF");
    return 0;
}
__global__ void CUDA_Kernel_Integral::fold (const int points_x,const int points_y,const int points_z,
                                            const int* di_xs,const int* di_ys,const int* di_zs,
                                            const int* d_from_basic,
                                            const double* d_density, double** d_rho_dev)
{
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = points_x*points_y*points_z;
    const int stride = blockDim.x*gridDim.x;

    int i_x,i_y,i_z;
    int ind;
    for(int i=idx; i<total_size ; i+=stride){
        ind = d_from_basic[i];
        if (ind>=0){    
            i_x = di_xs[ind];
            i_y = di_ys[ind];
            i_z = di_zs[ind];
            d_rho_dev[i_z][points_x*i_y+i_x] = d_density[ind];
        }
    }

    
    return;
}
__global__ void CUDA_Kernel_Integral::transpose(const int points_x,const int points_y,const int points_z,
                                double** original_matrices, double** matrices ){
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = points_x*points_y*points_z;
    const int stride = blockDim.x*gridDim.x;
    int x,y,z;

    for(int i=idx; i<total_size ; i+=stride){
        z = i/(points_x*points_y);
        y = (i-z*points_y*points_x)/points_x;
        x = i%points_x;

        matrices[y][points_x*z+x] = original_matrices[z][points_x*y+x];
    }

    return;
}

__global__ void CUDA_Kernel_Integral::unfold( const double weight, const int points_x,const int points_y,const int points_z,
                                const int* di_xs,const int* di_ys,const int* di_zs,
                                const int* d_from_basic,
                                double const** __restrict__ original_matrices, double* values )
{
    const int idx = blockDim.x*blockIdx.x + threadIdx.x;
    const int total_size = points_x*points_y*points_z;
    const int stride = blockDim.x*gridDim.x;

    int i_x,i_y,i_z;
    int ind;
    for(int i=idx; i<total_size ; i+=stride){
        ind = d_from_basic[i];
        if (ind>=0){
            i_x = di_xs[ind];
            i_y = di_ys[ind];
            i_z = di_zs[ind];
            values[ind] += original_matrices[i_y][points_x*i_z+i_x]*weight;
        }
    }

    return;
}

