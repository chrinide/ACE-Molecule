#include "Numerical_Derivatives.hpp"
#include <stdexcept>
#include <cstdlib>
#include "Verbose.hpp"

double Numerical_Derivatives::numerical_derivatives(double x, std::vector<double> mesh, std::vector<double> value, int order, int accuracy){
    double retval;

    //vector<double> alpha; // alpha_i = x_i - x
    //vector<double> f; // f_i = f(x_i)
    if(x < mesh[0] or x > mesh[mesh.size() - 1]){
        Verbose::set_numformat(Verbose::Default);
        Verbose::all()<< "Numerical_Derivatives::numerical_derivatives - ERROR: wrong x value \n";
        Verbose::all() << "x = " << x << ", mesh start = " << mesh[0] << ", mesh end =" <<  mesh[mesh.size()-1] << std::endl;
        throw std::logic_error("x value outside of mesh");
    }

    int index = 0;
    for(int i=0; i<mesh.size(); i++){
        if(x <= mesh[i]){
            index = i;
            break;
        }
    }

    if(accuracy == 2){
        if(order == 1){
            double a1, a2;
            double v1, v2;
            if(index == 0){
                a1 = mesh[index] - x;
                a2 = mesh[index+1] - x;
                v1 = value[index];
                v2 = value[index+1];
            }
            else{
                a1 = mesh[index-1] - x;
                a2 = mesh[index] - x;
                v1 = value[index-1];
                v2 = value[index];
            }

            retval = v1/(a1-a2) + v2/(a2-a1);
        }
        else{
            Verbose::all()<< "Numerical_Derivatives::numerical_derivatives - not supported. \n" ;
            exit(EXIT_FAILURE);
        }
    }
    else if(accuracy == 3){
        double a1, a2, a3, v1, v2, v3;
        if(index == 0){
            a1 = mesh[index] - x;
            a2 = mesh[index+1] - x;
            a3 = mesh[index+2] - x;
            v1 = value[index];
            v2 = value[index+1];
            v3 = value[index+2];
        }
        else if(index == 1){
            a1 = mesh[index-1] - x;
            a2 = mesh[index] - x;
            a3 = mesh[index+1] - x;
            v1 = value[index-1];
            v2 = value[index];
            v3 = value[index+1];
        }
        else{
            a1 = mesh[index-2] - x;
            a2 = mesh[index-1] - x;
            a3 = mesh[index] - x;
            v1 = value[index-2];
            v2 = value[index-1];
            v3 = value[index];
        }

        if(order == 1){
            retval = -(a2+a3)*v1/(a1-a2)/(a1-a3) - (a1+a3)*v2/(a2-a1)/(a2-a3) - (a1+a2)*v3/(a3-a1)/(a3-a2);
        }
        else if(order == 2){
            retval = 2*v1/(a1-a2)/(a1-a3) + 2*v2/(a2-a1)/(a2-a3) + 2*v3/(a3-a1)/(a3-a2);
        }
        else{
            Verbose::all()<< "Numerical_Derivatives::numerical_derivatives - not supported. \n";
            exit(EXIT_FAILURE);
        }
    }
    else if(accuracy == 4){
        double a1, a2, a3, a4, v1, v2, v3, v4;
        if(index == 0){
            a1 = mesh[index] - x;
            a2 = mesh[index+1] - x;
            a3 = mesh[index+2] - x;
            a4 = mesh[index+3] - x;
            v1 = value[index];
            v2 = value[index+1];
            v3 = value[index+2];
            v4 = value[index+3];
        }
        else if(index == 1){
            a1 = mesh[index-1] - x;
            a2 = mesh[index] - x;
            a3 = mesh[index+1] - x;
            a4 = mesh[index+2] - x;
            v1 = value[index-1];
            v2 = value[index];
            v3 = value[index+1];
            v4 = value[index+2];
        }
        else if(index == mesh.size()-1){
            a1 = mesh[index-3] - x;
            a2 = mesh[index-2] - x;
            a3 = mesh[index-1] - x;
            a4 = mesh[index] - x;
            v1 = value[index-3];
            v2 = value[index-2];
            v3 = value[index-1];
            v4 = value[index];
        }
        else{
            a1 = mesh[index-2] - x;
            a2 = mesh[index-1] - x;
            a3 = mesh[index] - x;
            a4 = mesh[index+1] - x;
            v1 = value[index-2];
            v2 = value[index-1];
            v3 = value[index];
            v4 = value[index+1];
        }

        if(order == 1){
            retval = (a2*a3 + a2*a4 + a3*a4)*v1/(a1-a2)/(a1-a3)/(a1-a4) + (a1*a3 + a1*a4 + a3*a4)*v2/(a2-a1)/(a2-a3)/(a2-a4) + (a1*a2 + a1*a4 + a2*a4)*v3/(a3-a1)/(a3-a2)/(a3-a4) + (a1*a2 + a1*a3 + a2*a3)*v4/(a4-a1)/(a4-a2)/(a4-a3);
        }
        else if(order == 2){
            retval = -2*(a2+a3+a4)*v1/(a1-a2)/(a1-a3)/(a1-a4) - 2*(a1+a3+a4)*v2/(a2-a1)/(a2-a3)/(a2-a4) - 2*(a1+a2+a4)*v3/(a3-a1)/(a3-a2)/(a3-a4) - 2*(a1+a2+a3)*v4/(a4-a1)/(a4-a2)/(a4-a3);
        }
        else{
            Verbose::all()<< "Numerical_Derivatives::numerical_derivatives - not supported. \n" ;
            exit(EXIT_FAILURE);
        }
    }
    else if(accuracy == 5){
        double a1, a2, a3, a4, a5, v1, v2, v3, v4, v5;
        if(index == 0){
            a1 = mesh[index] - x;
            a2 = mesh[index+1] - x;
            a3 = mesh[index+2] - x;
            a4 = mesh[index+3] - x;
            a5 = mesh[index+4] - x;
            v1 = value[index];
            v2 = value[index+1];
            v3 = value[index+2];
            v4 = value[index+3];
            v5 = value[index+4];
        }
        else if(index == 1){
            a1 = mesh[index-1] - x;
            a2 = mesh[index] - x;
            a3 = mesh[index+1] - x;
            a4 = mesh[index+2] - x;
            a5 = mesh[index+3] - x;
            v1 = value[index-1];
            v2 = value[index];
            v3 = value[index+1];
            v4 = value[index+2];
            v5 = value[index+3];
        }
        else if(index == mesh.size()-1){
            a1 = mesh[index-4] - x;
            a2 = mesh[index-3] - x;
            a3 = mesh[index-2] - x;
            a4 = mesh[index-1] - x;
            a5 = mesh[index] - x;
            v1 = value[index-4];
            v2 = value[index-3];
            v3 = value[index-2];
            v4 = value[index-1];
            v5 = value[index];
        }
        else if(index == mesh.size()-2){
            a1 = mesh[index-3] - x;
            a2 = mesh[index-2] - x;
            a3 = mesh[index-1] - x;
            a4 = mesh[index] - x;
            a5 = mesh[index+1] - x;
            v1 = value[index-3];
            v2 = value[index-2];
            v3 = value[index-1];
            v4 = value[index];
            v5 = value[index+1];
        }
        else{
            a1 = mesh[index-2] - x;
            a2 = mesh[index-1] - x;
            a3 = mesh[index] - x;
            a4 = mesh[index+1] - x;
            a5 = mesh[index+2] - x;
            v1 = value[index-2];
            v2 = value[index-1];
            v3 = value[index];
            v4 = value[index+1];
            v5 = value[index+2];
        }

        if(order == 1){
            retval = -(a2*a3*a4 + a2*a3*a5 + a2*a4*a5 + a3*a4*a5)*v1/(a1-a2)/(a1-a3)/(a1-a4)/(a1-a5) - (a1*a3*a4 + a1*a3*a5 + a1*a4*a5 + a3*a4*a5)*v2/(a2-a1)/(a2-a3)/(a2-a4)/(a2-a5) - (a1*a2*a4 + a1*a2*a5 + a1*a4*a5 + a2*a4*a5)*v3/(a3-a1)/(a3-a2)/(a3-a4)/(a3-a5) - (a1*a2*a3 + a1*a2*a5 + a1*a3*a5 + a2*a3*a5)*v4/(a4-a1)/(a4-a2)/(a4-a3)/(a4-a5) - (a1*a2*a3 + a1*a2*a4 + a1*a3*a4 + a2*a3*a4)*v5/(a5-a1)/(a5-a2)/(a5-a3)/(a5-a4);
        }
        else if(order == 2){
            retval = 2*(a2*a3 + a2*a4 + a2*a5 + a3*a4 + a3*a5 + a4*a5)*v1/(a1-a2)/(a1-a3)/(a1-a4)/(a1-a5) + 2*(a1*a3 + a1*a4 + a1*a5 + a3*a4 + a3*a5 + a4*a5)*v2/(a2-a1)/(a2-a3)/(a2-a4)/(a2-a5) + 2*(a1*a2 + a1*a4 + a1*a5 + a2*a4 + a2*a5 + a4*a5)*v3/(a3-a1)/(a3-a2)/(a3-a4)/(a3-a5) + 2*(a1*a2 + a1*a3 + a1*a5 + a2*a3 + a2*a5 + a3*a5)*v4/(a4-a1)/(a4-a2)/(a4-a3)/(a4-a5) + 2*(a1*a2 + a1*a3 + a1*a4 + a2*a3 + a2*a4 + a3*a4)*v5/(a5-a1)/(a5-a2)/(a5-a3)/(a5-a4);
        }
        else{
            Verbose::all()<< "Numerical_Derivatives::numerical_derivatives - not supported. \n" ;
            exit(EXIT_FAILURE);
        }
    }
    else{
        Verbose::all()<< "Numerical_Derivatives::numerical_derivatives - not supported.\n";
        exit(EXIT_FAILURE);
    }

    return retval;
}
