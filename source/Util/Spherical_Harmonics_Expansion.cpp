#include "Spherical_Harmonics_Expansion.hpp"
#include <iostream>
#include <cmath>

#include "../Util/Tricubic_Interpolation.hpp"
#include "../Util/Lebedev_Quadrature.hpp"

using std::vector;
using Teuchos::RCP;

vector<double> Spherical_Harmonics::Expansion::expand(
        int l, int m, 
        RCP<const Basis> mesh, 
        Teuchos::RCP<Epetra_MultiVector> values, 
        int index, 
        vector<double> dest_grid,
        double * expand_center/* = NULL*/
){
    vector<double> expand_val(dest_grid.size(), 0.0);

    double center[3];
    if( expand_center == NULL ){
        for( int i = 0; i < 3; ++i){
            center[i] = 0.0;
        }
    } else {
        for( int i = 0; i < 3; ++i){
            center[i] = expand_center[i];
        }
    }

    vector<double> weight = Lebedev_Quadrature::get_weight();
    vector< vector<double> > pts = Lebedev_Quadrature::get_points_on_unit_sphere();

    for(int n = 0; n < weight.size(); ++n){
        double Y = Spherical_Harmonics::Ylm(l,m,pts[n][0],pts[n][1],pts[n][2]);
        vector< std::array<double,3> > pos;
        for(int r = 0; r < dest_grid.size(); ++r){
            std::array<double,3> tmp;
            for(int i = 0; i < 3; ++i){
                tmp[i] =dest_grid[r]*pts[n][i]+center[i];
            }
            pos.push_back(tmp);
        }

        vector<double> radial_val = Interpolation::Tricubic::interpolate( mesh, values, index, pos);

        for(int r = 0; r < dest_grid.size(); ++r){
            expand_val[r] += radial_val[r]*Y*weight[n]*4*M_PI;
        }
    }// for n

    return expand_val;
}

/*
std::vector<double> Spherical_Harmonics_Expansion::integrate_ring( 
        Basis* basis, 
        Grid_Setting* grid_setting, 
        Teuchos::RCP<Epetra_MultiVector> values, 
        int index, 
        std::vector<double> rcut,
        double * expand_center/  = NULL /
){
    vector<double> retval (0.0, rcut.size()-1);

    int NumMyElements = values -> Map().NumMyElements();
    int* MyGlobalElements = values -> Map().MyGlobalElements();
    double scaling = basis -> get_scaling()[0] * basis -> get_scaling()[1] * basis -> get_scaling()[2];

    double center[3];
    if( expand_center == NULL ){
        for( int i = 0; i < 3; ++i){
            center[i] = 0.0;
        }
    } else {
        for( int i = 0; i < 3; ++i){
            center[i] = expand_center[i];
        }
    }

    for(int i = 0; i < NumMyElements; ++i){
        int i_x, i_y, i_z;
        grid_setting -> decompose( MyGlobalElements[i], basis -> get_points(), &i_x, &i_y, &i_z );

        double x, y, z, r;
        x = after_scaled_grid[0][j_x] - center[0];
        y = after_scaled_grid[1][j_y] - center[1];
        z = after_scaled_grid[2][j_z] - center[2];

        r = sqrt(x*x+y*y+z*z);
        for(int j = 0; j < rcut.size()-1; ++j){
            if( rcut[j] <= r && r < rcut[j+1] ){
                retval[j] += values -> operator()(index) -> operator[](i) * scaling;
            }
        }
    }
    return retval;
}
*/

/*
double Spherical_Harmonics_Expansion::integrate_ring( 
        Basis* basis, 
        Grid_Setting* grid_setting, 
        Teuchos::RCP<Epetra_MultiVector> values, 
        int index, 
        double rmin,
        double rmax, 
        double * expand_center/  = NULL /
){
    double retval =0.0;

    int NumMyElements = values -> Map().NumMyElements();
    int* MyGlobalElements = values -> Map().MyGlobalElements();
    double scaling = basis -> get_scaling()[0] * basis -> get_scaling()[1] * basis -> get_scaling()[2];

    double center[3];
    if( expand_center == NULL ){
        for( int i = 0; i < 3; ++i){
            center[i] = 0.0;
        }
    } else {
        for( int i = 0; i < 3; ++i){
            center[i] = expand_center[i];
        }
    }

    for(int i = 0; i < NumMyElements; ++i){
        int i_x, i_y, i_z;
        grid_setting -> decompose( MyGlobalElements[i], basis -> get_points(), &i_x, &i_y, &i_z );

        double x, y, z, r;
        x = after_scaled_grid[0][j_x] - center[0];
        y = after_scaled_grid[1][j_y] - center[1];
        z = after_scaled_grid[2][j_z] - center[2];

        r = sqrt(x*x+y*y+z*z);
        if( rmin <= r && r < rmax ){
            retval += values -> operator()(index) -> operator[](i) * scaling;
        }
    }
    return retval;
}
*/

/*
void Spherical_Harmonics_Expansion::get_Ylm_on_mesh( 
        int l, int m, 
        Basis* basis, 
        Grid_Setting* grid_setting, 
        Teuchos::RCP<Epetra_Vector> output, 
        double * expand_center/  = NULL /
){
    if( l == 0 ){
        output -> PutScalar( 1/sqrt(4*M_PI) );
        return;
    }

    int NumMyElements = values -> Map().NumMyElements();
    int* MyGlobalElements = values -> Map().MyGlobalElements();
    double scaling = basis -> get_scaling()[0] * basis -> get_scaling()[1] * basis -> get_scaling()[2];

    double center[3];
    if( expand_center == NULL ){
        for( int i = 0; i < 3; ++i){
            center[i] = 0.0;
        }
    } else {
        for( int i = 0; i < 3; ++i){
            center[i] = expand_center[i];
        }
    }

    for(int i = 0; i < NumMyElements; ++i){
        int i_x, i_y, i_z;
        grid_setting -> decompose( MyGlobalElements[i], basis -> get_points(), &i_x, &i_y, &i_z );

        double x, y, z, r;
        x = after_scaled_grid[0][j_x] - center[0];
        y = after_scaled_grid[1][j_y] - center[1];
        z = after_scaled_grid[2][j_z] - center[2];

        r = sqrt(x*x+y*y+z*z);
        output -> ReplaceGlobalValues( MyGlobalElements[i], 0, Spherical_Harmonics::Ylm(l, m, x, y, z) );
    }
}
*/
