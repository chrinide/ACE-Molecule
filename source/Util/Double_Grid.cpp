#include "Double_Grid.hpp"
#include <iostream>
#include <cmath>
#include "Parallel_Manager.hpp"
#include "Verbose.hpp"

using std::vector;
using std::sin;
using Teuchos::RCP;

vector<double> Double_Grid::get_sampling_coefficients( std::string FilterType, int FineDimension ){
    vector<double> sampling_coefficients;
    //Coefficients for supersampling of target function//
    //RCP<ParameterList> parameters = Teuchos::sublist(parameters,"BasicInformation");

    if( FineDimension < 1 ){
        return sampling_coefficients;
    }

    if( FilterType == "Sinc" ){
    //Using Filter function as Sinc function//
        int sin_N = 60;
        for(int i = 0; i < sin_N*FineDimension; ++i){
            double new_x = static_cast<double>(i)/FineDimension;
            double value = 0.0;   
            if(i==0){
                value = 1.0;
            } else if( i % FineDimension == 0 ){
                value = 0.0;
            } else{
                value = sin(M_PI*new_x)/(M_PI*new_x);
            }
            sampling_coefficients.push_back(value);
        } 
    }

    else if( FilterType == "Lagrange"){
    //Using Filter function as Lagrange function of order 8//
        for(int i=0; i<8*FineDimension; i++){
            double value = 1.0;
            int kmin = -int(floor(17.0/2.0));
            int kmax = int(ceil(17.0/2.0));
            double new_x = static_cast<double>(i)/FineDimension;
            int jx = int(floor(new_x));

            for(int ix=kmin; ix<-jx; ix++){
                value *= (new_x-jx-ix)/(-jx-ix);
            } 
            for(int ix=-jx+1; ix<kmax+1; ix++){
                value *= (new_x-jx-ix)/(-jx-ix);
            } 
            sampling_coefficients.push_back(value);
        }
    }

    return sampling_coefficients;
}


RCP<Epetra_Vector> Double_Grid::sample(
        vector<double> fine_vals,
        vector<int> fine_inds,
        RCP<const Basis> fine_mesh,
        RCP<const Basis> out_mesh,
        std::string filter_type,
        int FineDimension, double beta,
        vector<double> center, double cutoff
){
    int * MyGlobalElements = out_mesh -> get_map() -> MyGlobalElements();
    int NumMyElements = out_mesh -> get_map() -> NumMyElements();
    const double ** scaled_grid = out_mesh -> get_scaled_grid();
    const double ** fine_scaled_grid = fine_mesh -> get_scaled_grid();
    const double * fine_scaling = fine_mesh -> get_scaling();

    // Interpolate proj
    double rc_in = cutoff;
    double rc_out = rc_in * beta;      // rmax = beta * rcut by SO.Ryu
    vector<double> sampling_coefficients = Double_Grid::get_sampling_coefficients(filter_type, FineDimension);
    RCP<Epetra_Vector> retval = Teuchos::rcp( new Epetra_Vector( *out_mesh -> get_map(), true ) );

    // k: FILTER FUNCTION
    for(int kl = 0; kl < NumMyElements; ++kl){
        int k = MyGlobalElements[kl];
        int k_x, k_y, k_z;
        out_mesh -> decompose(k, &k_x, &k_y, &k_z);
        if( pow(scaled_grid[0][k_x]-center[0],2)+pow(scaled_grid[1][k_y]-center[1],2)+pow(scaled_grid[2][k_z]-center[2],2) > rc_out*rc_out ){
            continue;
        }
        // j: finer mesh
        for(int j = 0; j < fine_inds.size(); ++j){
            int j_x, j_y, j_z;
            fine_mesh -> decompose(fine_inds[j], &j_x, &j_y, &j_z);

            int zx = round(abs(scaled_grid[0][k_x] - fine_scaled_grid[0][j_x])/fine_scaling[0]);
            int zy = round(abs(scaled_grid[1][k_y] - fine_scaled_grid[1][j_y])/fine_scaling[1]);
            int zz = round(abs(scaled_grid[2][k_z] - fine_scaled_grid[2][j_z])/fine_scaling[2]);

            double fin_val = fine_vals[j] * sampling_coefficients[zx]
                             * sampling_coefficients[zy]  * sampling_coefficients[zz];

            fin_val /= pow(FineDimension,1.5);
            int ierr = retval -> SumIntoGlobalValue( k, 0, fin_val );
            if( ierr != 0 ){
                Verbose::all() << "fine proj SumIntoGlobalValue error" << std::endl;
            }
        }
    } // for kl

    Parallel_Manager::info().all_barrier();
    return retval;
}
// */
