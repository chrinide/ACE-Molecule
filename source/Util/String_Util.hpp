#pragma once
#include <string>
#include <vector>

namespace String_Util{
    ///< @brief Check if the string is double.
    bool isStringDouble(const char *s);
    ///< @brief Check if the string is integer.
    bool isStringInteger(const char *s);
    //string* strSplit(string strOrigin,string strTok);
    /**
     * @brief Split the string.
     * @param strOrigin String to split.
     * @param strTok Delimiter.
     * @param preserve_none If true then zero length string is included in the return value.
     * @return Splitted string.
     **/
    std::vector<std::string> strSplit(std::string strOrigin, std::string strTok, bool preserve_none = false);
    /**
     * @brief Split the string.
     * @param strOrigin String to split.
     * @param strTok Delimiter.
     * @param preserve_none If true then zero length string is included in the return value.
     * @return Splitted string.
     **/
    std::vector<std::string> strSplit(std::string strOrigin, std::vector<std::string> strTok, bool preserve_none = false);

    /**
     * @brief Split the string by whitespace characters(" ", "\f", "\n", "\r", "\t", "\v").
     * @param strOrigin String to split.
     * @param preserve_none If true then zero length string is included in the return value.
     * @return Splitted string.
     **/
    std::vector<std::string> Split_ws(std::string strOrigin, bool preserve_none = false);

    /**
     * @brief Calculates the factorial.
     * @todo Move this from String_Util.
     **/
    int factorial(int x, int result = 1);
    /**
     * @brief Calculates the factorial for large x like x = 13.
     * @todo Move this from String_Util.
     **/
    long long int factorial(long long int x, long long int result = 1);
    /**
     * @brief Calculates the double factorial.
     * @todo Move this from String_Util.
     **/
    int double_factorial(int x);
    /**
     * @brief Calculates the double factorial for large x.
     * @todo Move this from String_Util.
     **/
    long long int double_factorial(long long int x);
    /**
     * @brief Trim whitespaces on the left.
     **/
    std::string ltrim( std::string s );
    /**
     * @brief Trim whitespaces on the right.
     **/
    std::string rtrim( std::string s );
    /**
     * @brief Trim whitespaces on the both side.
     **/
    std::string trim( std::string s );
    /**
     * @brief Fill 0 in front of i to make N-length string.
     **/
    std::string fill0(int i, int N);

    /**
     * @brief Convert string to int. Ignore non-digits.
     **/
    int string_to_int(std::string word);
    /**
     * @brief Convert string to int. Dies when met non-digits.
     * @todo replace this to c++11 std::stoi
     **/
    int         stoi( std::string s);
    /**
     * @brief Convert string to float actually.
     * @todo replace this to c++11 std::stod
     **/
    double      stod( std::string s);
    /**
     * @brief Converts number to the string. Workaround for incomplete intel15 std::to_string implementation.
     **/
    std::string to_string(long long value);
    /**
     * @brief Converts number to the string. Workaround for incomplete intel15 std::to_string implementation.
     **/
    std::string to_string(int value);
    /**
     * @brief Converts number to the string. Workaround for incomplete intel15 std::to_string implementation.
     **/
    std::string to_string(double value);
};
