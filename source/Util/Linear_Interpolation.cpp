#include "Linear_Interpolation.hpp"
#include <stdexcept>
#include <cmath>
#include <cstdlib>
#include "Verbose.hpp"

using std::vector;
using std::abs;

double Interpolation::Linear::linear_interpolate(double x, vector<double> mesh, vector<double> value){
    double y;

    if(x < mesh[0] || x > mesh[mesh.size()-1]){
        throw std::invalid_argument("x value is outside of provided mesh");
    }
    else if(std::fabs(x-mesh[0]) < 1.0E-10){
        y = value[0];
    }
    else{
        for(int i=0;i<mesh.size();i++){
            if(x < mesh[i]){
                y = (value[i] - value[i-1]) / (mesh[i] - mesh[i-1]) * (x - mesh[i-1]) + value[i-1];
                break;
            }
        }
    }

    return y;
}

double Interpolation::Linear::linear_interpolate_bisection(double x, vector<double> mesh, vector<double> value){
    //double y;
    int a,b,c;

    a=0;
    b=value.size()-1;

    while(true){
        c=(a+b)/2;
        if(mesh[c] > x) b=c;
        else a=c;

        if(b-a==1) break;
    }

    double y = (value[b]-value[a]) / (mesh[b]-mesh[a]) * (x-mesh[a]) + value[a];

    return y;
}
