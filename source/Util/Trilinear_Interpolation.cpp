#include "Trilinear_Interpolation.hpp"
#include <stdexcept>
#include <cmath>
#include "Verbose.hpp"
#include "Parallel_Util.hpp"

using std::vector;
using Teuchos::RCP;
using Teuchos::rcp_implicit_cast;
using Teuchos::rcp;

void Interpolation::Trilinear::interpolate(
    RCP<const Basis> before_mesh, 
    RCP<Epetra_MultiVector> before_values, 
    RCP<const Basis> after_mesh, 
    RCP<Epetra_MultiVector> &after_values, 
    double * after_center/* = NULL*/ 
){
    after_values -> PutScalar(0.0);
    const double ** after_scaled_grid = after_mesh -> get_scaled_grid();

    int size = before_mesh -> get_original_size();
    int beforeNumMyElements = before_values -> Map().NumMyElements();
    int* beforeMyGlobalElements = before_values -> Map().MyGlobalElements();
    int afterNumMyElements = after_values -> Map().NumMyElements();
    int* afterMyGlobalElements = after_values -> Map().MyGlobalElements();

    double center[3];
    if( after_center == NULL ){
        for( int i = 0; i < 3; ++i){
            center[i] = 0.0;
        }
    } else {
        for( int i = 0; i < 3; ++i){
            center[i] = after_center[i];
            center[i] = 0.0;
        }
    }

    for (int k=0;k<before_values->NumVectors();++k){
        double *tmpval = new double [size]();
        double *srcval = new double [size]();
        //memset(tmpval, 0.0, sizeof(double)*size);
        //memset(srcval, 0.0, sizeof(double)*size);

        for(int j=0; j<beforeNumMyElements; j++){
            tmpval[beforeMyGlobalElements[j]] = before_values->operator[](k)[j];
        }
        Parallel_Util::group_sum(tmpval, srcval, size);
        delete[] tmpval;

        for (int j = 0; j < afterNumMyElements; ++j ){
            int j_x=0,j_y=0,j_z=0;

            after_mesh->decompose( afterMyGlobalElements[j],&j_x,&j_y,&j_z);
            double x, y, z;
            x = after_scaled_grid[0][j_x] - center[0];
            y = after_scaled_grid[1][j_y] - center[1];
            z = after_scaled_grid[2][j_z] - center[2];

            double interpolated = Interpolation::Trilinear::calculate(before_mesh, srcval, x, y, z);
            after_values -> operator()(k) -> operator[](j) = interpolated;
        }
        delete[] srcval;
    }
}

void Interpolation::Trilinear::interpolate(
    Teuchos::RCP<const Basis> before_mesh, 
    Teuchos::RCP<Epetra_Vector> before_values, 
    Teuchos::RCP<const Basis> after_mesh, 
    Teuchos::RCP<Epetra_Vector> &after_values, 
    double * after_center/* = NULL */
){
    RCP<Epetra_MultiVector> after_values2 = rcp_implicit_cast<Epetra_MultiVector>(after_values);
    Interpolation::Trilinear::interpolate( 
        before_mesh, rcp_implicit_cast<Epetra_MultiVector>(before_values), 
        after_mesh, after_values2, after_center );
    if( after_values2 != after_values ){
        Verbose::all() << "Interpolation::Trilinear::interpolate Epetra_Vector to Epetra_Vector check!" << std::endl;
        after_values -> Update( 1.0, *after_values2 -> operator()(0), 0.0 );
    }
}

std::vector<double> Interpolation::Trilinear::interpolate(
    Teuchos::RCP<const Basis> before_mesh, 
    Teuchos::RCP<Epetra_MultiVector> before_values, 
    int row,
    std::vector< std::array<double,3> > positions
){
    int size = before_mesh -> get_original_size();
    int beforeNumMyElements = before_values -> Map().NumMyElements();
    int* beforeMyGlobalElements = before_values -> Map().MyGlobalElements();

    double *tmpval = new double [size]();
    double *srcval = new double [size]();
    //memset(tmpval, 0.0, sizeof(double)*size);
    //memset(srcval, 0.0, sizeof(double)*size);

    for(int j=0; j<beforeNumMyElements; j++){
        tmpval[beforeMyGlobalElements[j]] = before_values->operator[](row)[j];
    }
    Parallel_Util::group_sum(tmpval, srcval, size);
    delete[] tmpval;

    vector<double> retvals;
    for(int n = 0; n < positions.size(); ++n){
        double x = positions[n][0];
        double y = positions[n][1];
        double z = positions[n][2];
        double interpolated = Interpolation::Trilinear::calculate(before_mesh, srcval, x, y, z);
        retvals.push_back(interpolated);
    }

    delete[] srcval;
    return retvals;
}

std::vector<double> Interpolation::Trilinear::interpolate(
    Teuchos::RCP<const Basis> before_mesh, 
    Teuchos::RCP<Epetra_Vector> before_values, 
    std::vector< std::array<double,3> > positions
){
    return Interpolation::Trilinear::interpolate( before_mesh, rcp_implicit_cast<Epetra_MultiVector>(before_values), 0, positions );
}

double Interpolation::Trilinear::calculate(
    RCP<const Basis> before_mesh, 
    double * srcval, 
    double x, double y, double z
){
    const double ** before_scaled_grid = before_mesh -> get_scaled_grid();

    vector< vector< vector<int> > > combined_neighbors;
    vector< vector<int> > decomposed_neighbors;
    Interpolation::Trilinear::find_nearest_points( before_mesh, x, y, z, decomposed_neighbors, combined_neighbors);

    /*
    if( decomposed_neighbors[0][0] < 0 || decomposed_neighbors[0][1] < 0 || decomposed_neighbors[0][2] < 0 ){
        Verbose::all() << "point " << x << ", " << y << ", " << z << ": out of before_grid" << std::endl;
        return 0.0;
    }
    */

    double zd;
    if( decomposed_neighbors[1][2] >= 0 ){
        if( decomposed_neighbors[0][2] >= 0 ){
            zd = (z-before_scaled_grid[2][decomposed_neighbors[0][2]])/(before_scaled_grid[2][decomposed_neighbors[1][2]]-before_scaled_grid[2][decomposed_neighbors[0][2]]);
        } else {
            zd = -(z-before_scaled_grid[2][decomposed_neighbors[1][2]])/(before_mesh -> get_scaling()[2]);
        }
    } else {
        if( decomposed_neighbors[0][2] >= 0 ){
            zd = (z-before_scaled_grid[2][decomposed_neighbors[0][2]])/(before_mesh -> get_scaling()[2]);
        } else {
            Verbose::all() << "point " << x << ", " << y << ", " << z << ": completely out of before_grid" << std::endl;
            return 0.0;
        }
    }
    vector< vector<double> > z_interpolated;
    z_interpolated.resize(2);
    for( int a = 0; a < 2; ++a ){
        z_interpolated[a].resize(2);
    }
    for( int a = 0; a < 2; ++a ){
        for( int b = 0; b < 2; ++b ){
            double vals[2];
            for( int c = 0; c < 2; ++c ){
                if( combined_neighbors[a][b][c] < 0 || combined_neighbors[a][b][c] > before_mesh->get_original_size() ){
                    vals[c] = 0.0;
                } else {
                    vals[c] = srcval[combined_neighbors[a][b][c]];
                }
            }
            /*
            if( combined_neighbors[a][b][0] < 0 || combined_neighbors[a][b][0] > before_mesh->get_original_size() ){
                if( combined_neighbors[a][b][1] >= 0 and combined_neighbors[a][b][1] < before_mesh -> get_original_size() ){
                    vals[0] = 0.0;
                } else {
                    vals[0] = 0.0;
                }
            } else {
                vals[0] = srcval[combined_neighbors[a][b][0]];
            }
            if( combined_neighbors[a][b][1] < 0 || combined_neighbors[a][b][1] > before_mesh->get_original_size() ){
                if( combined_neighbors[a][b][0] >= 0 and combined_neighbors[a][b][0] < before_mesh -> get_original_size() ){
                    vals[1] = 0.0;
                } else {
                    vals[1] = 0.0;
                }
            } else {
                vals[1] = srcval[combined_neighbors[a][b][1]];
            }
            */
            z_interpolated[a][b] = vals[0]*(1.0-zd)+vals[1]*zd;
        }
    }

    double yd;
    /*
    if( decomposed_neighbors[1][1] > 0 ){
        yd = (y-before_scaled_grid[1][decomposed_neighbors[0][1]])/(before_scaled_grid[1][decomposed_neighbors[1][1]]-before_scaled_grid[1][decomposed_neighbors[0][1]]);
    } else {
        yd = (y-before_scaled_grid[1][decomposed_neighbors[0][1]])/(before_mesh -> get_scaling()[1]);
    }
    */
    if( decomposed_neighbors[1][1] >= 0 ){
        if( decomposed_neighbors[0][1] >= 0 ){
            yd = (y-before_scaled_grid[1][decomposed_neighbors[0][1]])/(before_scaled_grid[1][decomposed_neighbors[1][1]]-before_scaled_grid[1][decomposed_neighbors[0][1]]);
        } else {
            yd = -(y-before_scaled_grid[1][decomposed_neighbors[1][1]])/(before_mesh -> get_scaling()[1]);
        }
    } else {
        if( decomposed_neighbors[0][1] >= 0 ){
            yd = (y-before_scaled_grid[1][decomposed_neighbors[0][1]])/(before_mesh -> get_scaling()[1]);
        } else {
            //Verbose::all() << "point " << x << ", " << y << ", " << z << ": completely out of before_grid" << std::endl;
            return 0.0;
        }
    }
    vector<double> yz_interpolated;
    yz_interpolated.resize(2);
    for( int a = 0; a < 2; ++a ){
        yz_interpolated[a] = z_interpolated[a][0]*(1.0-yd)+z_interpolated[a][1]*yd;
    }

    double xd;
    /*
    if( decomposed_neighbors[1][0] > 0 ){
        xd = (x-before_scaled_grid[0][decomposed_neighbors[0][0]])/(before_scaled_grid[0][decomposed_neighbors[1][0]]-before_scaled_grid[0][decomposed_neighbors[0][0]]);
    } else {
        xd = (x-before_scaled_grid[0][decomposed_neighbors[0][0]])/(before_mesh -> get_scaling()[0]);
    }
    */
    if( decomposed_neighbors[1][0] >= 0 ){
        if( decomposed_neighbors[0][0] >= 0 ){
            xd = (x-before_scaled_grid[0][decomposed_neighbors[0][0]])/(before_scaled_grid[0][decomposed_neighbors[1][0]]-before_scaled_grid[0][decomposed_neighbors[0][0]]);
        } else {
            xd = -(x-before_scaled_grid[0][decomposed_neighbors[1][0]])/(before_mesh -> get_scaling()[0]);
        }
    } else {
        if( decomposed_neighbors[0][0] >= 0 ){
            xd = (x-before_scaled_grid[0][decomposed_neighbors[0][0]])/(before_mesh -> get_scaling()[0]);
        } else {
            Verbose::all() << "point " << x << ", " << y << ", " << z << ": completely out of before_grid" << std::endl;
            return 0.0;
        }
    }
    double interpolated;
    interpolated = yz_interpolated[0]*(1.0-xd)+yz_interpolated[1]*xd;

    return interpolated;
}

/*
 * decomposed index: input of scaled_grid[i][axis]
 * combined_index: index for Vector = [x_i][y_i][z_i]
 *
 * i, x_i, y_i, z_i info
 * .: grid point, @: desired point
 * .@ .
 * 0  1
 * @  .
 * 0  1
 * (If desired point is on grid point, it gets index 0.
 */

void Interpolation::Trilinear::find_nearest_points( RCP<const Basis> mesh, double x, double y, double z, std::vector< std::vector<int> > &decomposed_index, std::vector< std::vector< std::vector<int> > > &combined_index ){
    const int * grid_size = mesh -> get_points();
    const double ** grid = mesh -> get_scaled_grid();
    const double * grid_scaling = mesh -> get_scaling();

    decomposed_index.clear();
    decomposed_index.resize(2);
    for(int i = 0; i < 2; ++i){
        decomposed_index[i].resize(3, -1);
    }

    double r[3];
    r[0] = x;
    r[1] = y;
    r[2] = z;

    for(int i = 0; i < 3; ++i){
        for(int j = 0; j < grid_size[i]-1; ++j){
            if( grid[i][j] <= r[i] && grid[i][j+1] > r[i] ){
                decomposed_index[0][i] = j;
                decomposed_index[1][i] = j+1;
                break;
            }
        }
        /*
        if( grid[i][grid_size[i]-1] == r[i] ){
            decomposed_index[0][i] = grid_size[i]-1;
        }
        */
        if( r[i] >= grid[i][grid_size[i]-1] and r[i]-grid_scaling[i] < grid[i][grid_size[i]-1] ){
            decomposed_index[0][i] = grid_size[i] - 1;
        }
        if( r[i] < grid[i][0] and r[i]+grid_scaling[i] >= grid[i][0] ){
            decomposed_index[1][i] = 0;
        }
    }

//    vector< vector< vector<int> > > index;
    combined_index.resize(2);
    for(int i = 0; i < 2; ++i){
        combined_index[i].resize(2);
        for(int j = 0; j < 2; ++j){
            combined_index[i][j].resize(2, -1);
        }
    }

    for(int i = 0; i < 2; ++i){
        for(int j = 0; j < 2; ++j){
            for(int k = 0; k < 2; ++k){
                combined_index[i][j][k] = mesh -> combine( decomposed_index[i][0], decomposed_index[j][1], decomposed_index[k][2] );
            }
        }
    }
//    return index;
}

