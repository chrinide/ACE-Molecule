#pragma once

#include "Epetra_Vector.h"

#include "Teuchos_SerialDenseMatrix.hpp"
#include "Teuchos_RCP.hpp"
#include "Teuchos_Array.hpp"
#include "Teuchos_BLAS.hpp"

#include "../Basis/Basis.hpp"
#include "../State/State.hpp"
#include "Time_Measure.hpp"
/**
@breif This class performs kernel integration.
@author Sunghwan Choi

@details
Here, kernel function is linear combination of Gaussian functions to represent 1/r.
The method, performed here was proposed by Sundholm (JCP 132, 024102, 2010).
Within two-divided regions ([t_i,t_l], [t_l,t_f]), num_points1, num_points2 quadrature points are made, respectively.
Poisson_Solver inherits this class
*/

class Kernel_Integral{
    public:
        /**
         * @breif NEVER DELETE THIS VARIABLE BECAUSE THIS SOMEONE IS CONSTANTLY BREAKING POISSON_NGAU CLASS!
         * @details NEVER DELETE THIS VARIABLE BECAUSE THIS SOMEONE IS CONSTANTLY BREAKING POISSON_NGAU CLASS!
         **/
        std::string which_t_sampling_used = "";
        std::string type;
        /**
        @breif This function is a wapper function for a distributed input
        @param density (In) - density vector, distributed all processors
        @param hartree_potential (out) - potential vector, which is the result of this calculations
        @param hartree_energy (out) - self-interaction energy for coulomb interaction
        @details This function executes with  t-level parallelization
        */
        int compute(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density, Teuchos::RCP<Epetra_Vector>& hartree_potential,double& hartree_energy);

        /**
        @breif This function is a wapper function for a gathered input.
        @param density (In) - density vector, which is contained on a single processors
        @param hartree_potential (out) - potential vector, which is the result of this calculations
        @param hartree_energy (out) - self-interaction energy for coulomb interaction
        @details Each processor computes whole calculations. Therefore, you can use different inputs simultaneously
        */
        int compute(double* density, double* hartree_potential, double& hartree_energy);




        /**
        @breif This function is a wapper function for a gathered input and gives only energy .
        @param density [In]  density vector, which is contained on a single processors
        */
        double get_energy(Teuchos::Array< Teuchos::RCP<Epetra_Vector> > density);

        /**
        @brief Constructor
        @author Kwangwoo Hong, Sunghwan Choi
        @date 2013-12-24
        @param [in] mesh  mesh information
        @param [in] t_i The starting point for t sampling range
        @param [in] t_l The point that seperate the first and second range on t space
        @param [in] t_f The endpoint for t sampling range
        @param [in] num_points1  The number of sampling points on the first range [t_i, t_l]
        @param [in] num_points2  The number of sampling points on the second range [t_l, t_f]
        @param [in] asymtotic_correction  Use asymtotic correction term by Sundholm (JCP 132, 024102, 2010)
        */
        Kernel_Integral(Teuchos::RCP<const Basis> mesh,double t_i, double t_l, double t_f, int num_points1, int num_points2, bool asymtotic_correction = true,int ngpus = 0);

        /**
        @brief Destructor
        @date 2016-03-22
        @author Sunghwan Choi
        */
        ~Kernel_Integral();
        //Kernel_Integral(RCP<const Basis> mesh, int initial_integrate_numgrid, double integrate_max,double tmin, double tmax, int num_points, int num_box, string input, string output);
        /**
        @brief This function returns information related to this class
        @author Sungwoo Kang
        */
        virtual std::string get_info_string() const = 0;
        int get_ngpus(){return ngpus;};
    protected:

        /**
            @breif doing 3D convolution with given data
            @author Kangwoo Hong, Sunghwan Choi
            @param [in] density  Input data on 3D grid
            @param [out] hartree_potential  The pointer of memory space for ouput data.
            @param [in] F_xs   A set of convolution matrices on x-axis
            @param [in] F_ys   A set of convolution matrices on y-axis
            @param [in] F_zs   A set of convolution matrices on z-axis

        */
        int compute_cpu(double* density, double* hartree_potential,
                        std::vector<double> w_t,
                        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_xs,
                        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_ys,
                        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_zs);
        /**
        @brief The function that calculate elements of F matrix
        @author Sunghwan Choi
        @date 2016-03-06
        @param [in] t  t value
        @param [in] i  The index for bra
        @param [in] j  The index for ket
        @param [in] axis The index for axis
        */
        double compute_F(double t, int i, int j, int axis);
        /**
        @brief  F_xs, F_ys, and F_zs are filled out in this function
        @author Kwangwoo Hong
        @date 201???
        */
        void construct_matrix();
        void construct_matrix2();
        /**
        @brief This function perform sampling t values on given range
        @author Kwangwoo Hong
        */
        virtual void t_sampling()=0;

        /// minimum in sampling range
        double t_i;
        /// value for partitioning of t range
        double t_l;
        /// maximum in sampling range
        double t_f;
        /// number of samping points in [t_f, t_l]
        int num_points1;
        /// number of samping points in [t_l, t_f]
        int num_points2;

        /// sampled t values
        std::vector<double> t_total;
        /// sampled weights of samping points
        std::vector<double> w_total;
        /// 3D mesh information
        Teuchos::RCP<const Basis> mesh ;
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_xs;
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_ys;
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_zs;

        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_xs2;
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_ys2;
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > F_zs2;

        /// t values for this processors
        std::vector<double> t_proc;
        std::vector<double> w_proc;
        std::vector<int> i_proc;

        bool fill_complete;
        /// This value determines whether asymtotic correction will be performed
        bool asymtotic_correction;
        bool fill_complete2;
        bool is_t_sampled = false;
        /// This value determines whether computation is done GPU or not
        int ngpus = 0;
        bool gpu_enable = false;

        /// temporary memory space for convolution
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > T_gamma;
        /// temporary memory space for convolution
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > T_gamma2;
        /// temporary memory space for convolution
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > T_beta;
        /// temporary memory space for convolution
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > T_beta2;
        /// temporary memory space for convolution
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > d_gamma;
        /// temporary memory space for convolution
        Teuchos::Array< Teuchos::SerialDenseMatrix<int, double> > v_hartree;

        Teuchos::RCP<Teuchos::BLAS<int,double> > blas;

        /// internal timer
        Teuchos::RCP<Time_Measure> timer;

};
