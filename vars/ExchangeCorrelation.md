## **ExchangeCorrelation**

 This section controls the exchange correlation functional for the DFT or TDDFT calculations.<br>
 Some major exchange-correlation functional can be specified using by *FunctionalName*.<br>
 Otherwise, you can specify exchange-correlation functional using *XCFunctional* keyword, or you can specify exchange and correlation functional seperately using *XFunctional* and *CFunctional* keyword.<br>
 The hybridization of XC functional can be done with special input. See the *XFunctional* and *CFunctional* keywords.<br>

#### **Subsection List**


### **XCLibrary**
> #### **Description**
> This is a keyword to select library set of exchange correlation functional.
> #### **Type**
> string
> #### **Possible Options**
> + Libxc: Using xc library of http://www.tddft.org/programs/octopus/wiki/index.php/Libxc <br>
> The list of available functionals can be found in http://www.tddft.org/programs/octopus/wiki/index.php/Libxc_functionals or the list in http://bigdft.org/Wiki/index.php?title=XC_codes.<br>
> Meta-GGA functionals are not implemented yet.<br>
> Reference :  Miguel A. L. Marques, Micael J. T. Oliveira, and Tobias Burnus, *Comput. Phys. Commun.* **183**, 2272 (2012) [default]


### **XCFunctional**
> #### **Description**
> This is a keyword to specify exchange-correlation functional. See the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a> or <a href="http://www.tddft.org/programs/libxc/functionals/">libxc website</a>.<br>
> We accept the Libxc functional codes in string or integer, such as LDA_X (or 1), GGA_X_PBE (or 101), etc.<br>
> This option accepts functionals including both exchange and correlations, such as HYB_GGA_XC_B3LYP.<br>
> These functionals have **\_XC\_** in their functional code.
>
> XCFucntional with integer higher than 10000 invokes custom exchange-correlation module. Such values are listed below.<br>
> To use the range-separated hybrid, *GaussianPotential* option should be supplied. Otherwise, the program adds 100% EXX instead.<br>
> CAM-type hybrids, using both global and range-separated exact exchange, are not supported.
> #### **Type**
> int or string
> #### **Possible Options**
> + 10478: Use LC-wPBE(2Gau) exchange-correlation. This option should be used in conjunction of GaussianPotential or ErfNGaussianPreset input. See Song, J.-W., and Hirao, K., *J. Chem. Phys.* **143**, 144112 (2015).

> + Any libxc functional code or id.

### **XFunctional**
> #### **Description**
> This is a keyword to specify exchange-correlation functional. See the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a> or <a href="http://www.tddft.org/programs/libxc/functionals/">libxc website</a>. <br>
 We accept the Libxc functional codes in string or integer, such as LDA_X (or 1), GGA_X_PBE (or 101), etc.<br>
 > This option accepts functionals including only exchange, such as GGA_X_PBE.<br>
 > These functionals have **\_X\_** in their functional code.
 >
 > XFucntional with negative integer invokes custom exchange module. Such values are listed below.<br>
 > You may specify scaling factor, which is useful for tuning exact exchange portion. See example below for format.<br>
> #### **Type**
> int or string
> #### **Possible Options**
> + -12: Use KLI exchange.
> + "GGA_X_PBE 0.4": PBE exchange, scaled by 0.4.
> + "101 0.4": PBE exchange, scaled by 0.4.
> + Any libxc functional code or id.

### **CFunctional**
> #### **Description**
> This is a keyword to specify exchange-correlation functional. See the list in <a href="http://bigdft.org/Wiki/index.php?title=XC_codes">BigDFT website</a> or <a href="http://www.tddft.org/programs/libxc/functionals/">libxc website</a>. <br>
 We accept the Libxc functional codes in string or integer, such as LDA_X (or 1), GGA_X_PBE (or 101), etc.<br>
 > This option accepts functionals including only correlation, such as GGA_C_PBE.<br>
 > These functionals have **\_C\_** in their functional code.<br>
 > You may specify scaling factor, in quotation mark with space-separated argument. See *XFunctional*.
> #### **Type**
> int or string
> + Any libxc functional code or id.

### **GaussianPotential**
> #### **Description**
> Sets range-separated exact exchange kernel using Gaussians. This option should be supplied if range-separated hybrids.<br>
> Input should be a string, consisting two real numbers separated by space.<br>
> First number is Gaussian coefficient, and second number is Gaussian exponent. See examples below.<br>
> Multiple arguments are summed to represent short or long-range exact exchange.<br>
> #### **Type**
> string
> #### **Possible Options**
> + "0.27 0.075": Sets EXX potential to 0.27 exp(-0.075 r<sup>2</sup>).
> + "0.18 0.006": Sets EXX potential to 0.18 exp(-0.006 r<sup>2</sup>).
> + Note: Using both "0.27 0.075" and "0.18 0.006" is the setting used in Song, J.-W., and Hirao, K., *J. Chem. Phys.* **143**, 144112 (2015) to mimic erf(0.4r)/r potential.

### **ErfNGaussianPreset**
> #### **Description**
> Expand erf(wr)/r type exact exchange into N Gaussians. This option sets *GaussianPotential* from range-separation parameter for error function coefficient and the number of Gaussians.<br>
> Supply a string containing range-separation parameter and the number of Gaussians, separated by space.<br>
> Currently 2 Gaussians are supported only.
> #### **Type**
> string
> #### **Possible Options**
> + "0.4 2": Expand erf(0.4r)/r into 2 Gaussians and use it as range-separated exact exchange kernel.<br>
>           This option sets two *GaussianPotential* input, with "0.27 0.075" and "0.18 0.006".
