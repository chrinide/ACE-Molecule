## DDA

This section controls parameters related to DDA (ADA)

(Sci. Rep. 2017, 7, 15775. Jaechang Lim, Sungwoo Kang, Jaewook Kim, Woo Youn Kim,* and Seol Ryu*)

#### **Subsection List**


### **InitialFilenames**
> #### **Description**
> Pseudopotential file name which will be used to calculate atomic density. This parameter is used when *ScaleUp* is minus value
> #### **Type**
> string

### **Gaussian**
> #### **Description**
> Output file of gaussian TDDFT calculation for calculation of atomic polarizability
> #### **Type**
> string

### **ACE**
> #### **Description**
> Output file of ACE-Molecule TDDFT calculation for calculation of atomic polarizability. 
> #### **Type**
> string

### **Txt**
> #### **Description**
> Text file of atomic polarizability.This file must have 3 columns for wavelength (in um), real, and imaginary value of polarizability respectively.
> #### **Type**
> string

### **StartWavelength**
> #### **Description**
> Minimum value of wavelength.
> #### **Type**
> float
> #### **Possible Options**
> + 300.0: minimum value of wavelength is 300.0nm [default]

### **EndWavelength**
> #### **Description**
> Maximum value of wavelength.
> #### **Type**
> float
> #### **Possible Options**
> + 700.0: maximum value of wavelength is 700.0nm [default]

### **ScaleUp**
> #### **Description**
> Scaling factor for atomic polarizability. The atomic polzability is multiplied by this value
> #### **Type**
> float
> #### **Possible Options**
> + 1.0: nothing change on atomic polarizability [default]
> + -1.0: atomic polarizability is scaled using local electron density, In this case, you must provide occupied molecular orbitals in Guess section.

### **Damping**
> #### **Description**
> Damping value
> #### **Type**
> float
> #### **Possible Options**
> + 1000.0: 1000nm is used for damping [default]

### **PDDamping**
> #### **Description**
> Position dependent damping value
> #### **Type**
> int
> #### **Possible Options**
> + 0: not using position dependent damping [default]

### **PDDampingMax**
> #### **Description**
> maximum value of position dependent damping value
> #### **Type**
> float
> #### **Possible Options**
> + 10000.0: 10000 nm [default]

### **PDDampingMin**
> #### **Description**
> minimum value of position dependent damping value
> #### **Type**
> float
> #### **Possible Options**
> + 1000.0: 1000 nm [default]

### **OutputBefore**
> #### **Description**
> Write atomic polarizbility of each atom without dipole-dipole interaction in file 
> #### **Type**
> int
> #### **Possible Options**
> + 0 : no write [default]
> + otherwise : write [default]

### **OutputAfter**
> #### **Description**
> Write atomic polarizbility of each atom with dipole-dipole interaction in file 
> #### **Type**
> int
> #### **Possible Options**
> + 0 : no write [default]
> + otherwise : write [default]

### **OutputDipole**
> #### **Description**
> Write dipole moment of each atom in file 
> #### **Type**
> int
> #### **Possible Options**
> + 0 : no write [default]
> + otherwise : write [default]


