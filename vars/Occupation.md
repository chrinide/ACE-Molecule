## **Occupation**

This section governes occupation of electron. This section is defined in Scf or Guess section.

### **VarName**
> #### **Description**
> Description blah blah
> #### **Type**
> string / int / (positive) float
> #### **Possible Options**
> + key: description

### **OccupationMethod**
> #### **Description**
> Controls how to occupy electrons.
> #### **Type**
> string 
> #### **Possible Options**
> + ZeroTemp: This option will occupy electrons from orbitals with low energy.
> + Fermi: Fermi-Dirac distribution is applied.
> + Input: Get occupation from Occupation_List[_Alpha/_Beta]

### **Temperature**
> #### **Description**
> This parameter works only when *OccupationMethod* is set to Fermi. Excit some electrons near the chemical potential to make occupation number to follow Fermi-Dirac distribution.
> Note that you need to set OccupationSize to sufficiently large value.
> #### **Type**
> (non-negative) float


### **OccupationList**
> #### **Description**
> Option for OccupationMethod Input. Should supply two inputs for spin polarized calculations. \ 
 Example: OccupationList \"2.0 x4 1.5 0.5 0.0\"
> #### **Type**
> string

### **OccupationSize**
> #### **Description**
> Sets the maximum size for the electron occupation.
> For OccupationMethod is ZeroTemp, if this variable is not set or set to small values, it will fallback to the minimum occupation suitable for given NumElectrons and NumberOfEigenvalues.
> For OccupationMethod is Fermi, you need to set this value to sufficiently large value.
> For OccupationMethod is Input, this variable is ignored and can be controlled by OccupationList.
> #### **Type**
> int

### **NumberOfVirtualStates**
> #### **Description**
> Adds some unoccupied orbitals to the occupation when it is automatically determined, so it can be calculated.
> This option is ignored when OccupationSize is provided.
> #### **Type**
> int
