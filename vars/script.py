from glob import glob 
import re
from functools import reduce

list_md_files = glob("*.md")
lines = [
"""
**Input data description**
======

ACE-Molecule is quantum chemistry package. This document contains a description of input parameter.
ACE-Molecule requires structural input format. All input parameters should be contained in a certain section. A section can be obtained in another section.

----

"""
]
for md_file in list_md_files:
    print(md_file)
    f = open(md_file)
    lines += f.readlines()
    f.close()

f=open("VarInfo.md","w")
f.write( reduce(lambda x,y: x+y, lines ) )
f.close()


