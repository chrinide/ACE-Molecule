## **Scf**

 This section governs variables related to Scf

#### **Subsection List**
  +  Diagonalize (Optional)
  +  Exchange_Correlation (Mandatory)
  +  ISF (Optional)
  +  Mixing (Optional)
  +  ExternalField (Optional)
  +  SolvationModel (Optional)
  +  Output (Optional)


### **VarName**
> #### **Description**
> Description blah blah
> #### **Type**
> string / int / (positive) float
> #### **Possible Options**
> + key: description


### **IterateMaxCycle**
> #### **Description**
> Number of max iteration of Scf.
> #### **Type**
> (positive) int

### **ConvergenceType**
> #### **Description**
> Type to be used to calculate convergence
> #### **Possible Options**
> + Energy:  Energy difference will be used for checking convergence
> + Density: difference will be used for checking convergence [Default]
> + Potential: Potential difference will be used for checking convergence
> + HOMO: HOMO energy difference will be used for checking convergence
> + EigenvalueSum: Sum of eigenvalue difference will be used for checking convergence

### **ConvergenceTolerance**
> #### **Description**
> Scf will be end if difference of *ConvergenceType* is smaller than *ConvergenceTolerance*.
> #### **Type**
> (positive) float 

### **EnergyDecomposition**
> #### **Description**
> Calculates energy per value of *EnergyDecomposition* steps.
> If the value is 0, the program does not calculate energy at each steps; if it is 3, the program calculates energy for every 3 steps. 
> The converged total energy and its components are always calculated and printed, regardless of this option.
> Defaults to 1 if ConvergenceType is Energy, 0 otherwise.
> #### **Type**
> non-negative int


### **NumberOfEigenvalues**
>#### **Description**
> It decides number of eigenvalues to be calculated in Scf. If this option does not exists or non-positive, it defaults to the eigenvalue number of previous routine.
>#### **Type**
> int

### **DiagonalizeShouldNotBeConverged**
>#### **Description**
> This parameter decides whether diagonalization if fully converged or not. If this option is not specified, diagonalization will be fully converged.
>#### **Type**
> int
> #### **Possible Options**
> + 0: Diagonalization will be fully converged.
> + Otherwise: Diagonalization will not be fully converged. [default]

### **aking_Hamiltonian_Matrix**
> #### **Description**
> it decides whether diagonalization with making hamiltonian matrix or not. \
> For default, diagonalization will be done with making hamiltonian matrix.
>#### **Type**
> int 
> #### **Possible Options**
> + 0:  Diagonalization will be done without making hamiltonian matrix.
> + Otherwise:  Diagonalization will be done with making hamiltonian matrix. [default]

### **ConstructKineticMatrix**
> #### **Description**
> It decides whether calculating kinetic energy using matrix or not.
> #### **Type**
> int
> #### **Possible Options**
> + 0 Calculate kinetic energy without explicit construction of kinetic matrix.
> + Otherwise: Calculate kinetic energy using kinetic matrix. [default]



### **ComputeInitialEnergy**
> #### **Description**
> Decides whether the energy of initial state is computed or not
> #### **Type**
> int
> #### **Possible Options**
> + 0: Do not compute initial energy
> + Otherwise: Compute initial energy [default]

### **IgnoreInternalInit**
> #### **Description**
> Controls the method to obtain initial PAW atomcenter density matrix and hartree potential for PAW calculations.
> #### **Type**
> int 
> #### **Possible Options**
> 0: Construct initial PAW atomcenter density matrix from PAW dataset file. Good if the system is similar with the combination of the free atoms. Default value.
> 1: Construct initial PAW atomcenter density matrix from initial guess orbitals. Good if cube guess is used


### **Making_Hamiltonian_Matrix**
> #### **Description**
> Optional value, it decides whether diagonalization with making hamiltonian matrix or not. \
> For default, diagonalization will be done with making hamiltonian matrix.
> #### **Type**
> int
>+ 0: Diagonalization will be done without making hamiltonian matrix.
>+ Otherwise: Diagonalization will be done with making hamiltonian matrix. [default]

