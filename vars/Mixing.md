## Mixing
 This section governes parameters related to mixing methods. 
 Generates guess Hamiltonian for the next iteration from the output of previous iterations.

### **MixingType
> #### **Description**
> Decides the field that is to be mixed.
> See the following pdf file (page 8) for the difference of thd density mixing and potential mixing.
> https://www.abinit.org/sites/default/files/oldsites/workshop_07/Presentations/Liege_Torrent2_SC_Mixing.pdf
> #### **Type**
> string
> #### **Possible Options**
>+ Density:\
> Mix density to generate guess Hamiltonian for the next step.\
> For PAW calculations, this mixes the PAW atom-centered density matrix.\
> We recommend this option for the PAW calculations.
>+ Potential:\
> Mix potential to generate guess Hamiltonian for the next step.\
> We recommend this option for the KLI calculations. [default]


### **MixingMethod**
> #### **Description**
> Decides the mixing algorithm.
> #### **Type**
> int
> #### **Possible Options**
>+ 0: Linear mixing. \
> $f^{n+1}_{in} = \alpha f^{n}_{out} + (1-\alpha) f^n_{in}$.
>+ 1 Pulay mixing (DIIS).\
> See Kresse, Phys. Rev. B 54, 11169 (1996), or http://www.hector.ac.uk/cse/distributedcse/reports/conquest/conquest/node3.html. [default]
>+ 2 Broyden mixing. \
> See Phys. Rev. C 78, 014318 (2008), section II.B, or Phys. Rev. B 38, 12807 (1998). Not recommended for PAW.


### **MixingParameter**
> #### **Description**
> Decides the mixing coefficient for the linear mixing.\
> As this variable approaches to 0, the mixing gives conservative guess. \
> If this variable is set to 0, the guess for the next iteration will be the same as the current iteration. \
> Setting this variable close to 1 will converge the calculation fast if the initial state is far from the converged state.
> However, the calculation may oscillate near the converged state, if this variable is too high. \
> Note that setting this value outside of 0-1 can work, but it slowdown convergence. \
> Defaults to 0.3.
> #### **Type** 
> float


### **PulayMixingParameter**
> #### **Description**
> Decides the mixing coefficient for the Pulay mixing.\
> This option works pretty much the same as MixingParameter. \
> Only difference is that this works for Pulay mixing.
> Defaults to MixingParameter.
> #### **Type** 
> float


### **BroydenMixingParameter**
> #### **Description**
> Decides the mixing coefficient for the Broyden mixing.\
> This option works pretty much the same as MixingParameter. \
> Only difference is that this works for Broyden mixing.
> Defaults to MixingParameter.
> #### **Type** 
> (positive) float


### **MixingStart**
> #### **Description**
> Irrelevant to the linear mixing algorithm. \
> Pulay, or Broyden mixing will start when the iterations are performed for MixingStart times. \
> Mixing algorithm is linear before this value. \
> If this variable is smaller than MixingHistory, it will be adjusted to corresponding MixingHistory. \
> Defaults to 6.
> #### **Type** 
> (positive) int

>
### **MixingHistory**
> #### **Description**
> Irrelevant to the linear mixing algorithm. \
> Pulay, or Broyden mixing will use this amount of previous iteration informations. \
> Defaults to 4.
> #### **Type** 
> (positive) int


### **UpdatePawMatrix**
> #### **Description**
> Controls whether PAW atomcenter density matrix mixing. See PAW_Density mixing also.\
> Only works with density or potential mixing.
>+ 0
> Do not update PAW atomcenter density matrix when mixing is done. Default for potential mixing. Only option for non-density non-potential mixing.
> #### **Type** 
> int
> #### **Possible Options**
>+ 1
> Update PAW atomcenter density matrix with same coefficient used to update mixing field. Default for density mixing.\
> In order to use same mixing scheme but different coefficient, use PAW_Density as mixing field.

>
