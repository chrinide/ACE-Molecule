import os
os.system("find -iname '*cmake*' -not -name CMakeLists.txt -not -name Find_Trilinos.cmake -not -name Version.cmake -not -name FileLists.cmake -not -name libxc.cmake -exec rm -rf {} \+")
os.system("rm Makefile ace")
os.system("rm -r doxygen")
os.system("rm -rf external_libs")

for path, dirs, files in os.walk("vars"):
    for file in files:
        if ".html" in file and not "index" in file and not "main" in file:
            os.remove(path+"/"+file)

